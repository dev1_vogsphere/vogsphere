/*package com.springboot.ecmu.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="user")

public class User {
 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 public String userid;

 public String alias;
 private String password;
 public String email;
 public String role;
 public String failedAttempt;
 public String account;
 public String status;
 public String approver;

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getUserid() {
	return userid;
}

public void setUserid(String userid) {
	this.userid = userid;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

public String getFailedAttempt() {
	return failedAttempt;
}

public void setFailedAttempt(String failedAttempt) {
	this.failedAttempt = failedAttempt;
}

public String getAccount() {
	return account;
}

public void setAccount(String account) {
	this.account = account;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getApprover() {
	return approver;
}

public void setApprover(String approver) {
	this.approver = approver;
}

public String getAlias() {
	return alias;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public void setAlias(String alias) {
	this.alias = alias;
}
 
}*/
