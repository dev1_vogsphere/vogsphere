package com.springboot.ecmu.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity 
@Table(schema ="debi_check_responses")

public class debi_check_responses {
@Id
@Column (name = "bankresponsecode")
@GeneratedValue(strategy = GenerationType.IDENTITY)
public String bankresponsecode;
public String bankresponsecodedesc;
public String mainstatus;
public String getBankresponsecode() {
	return bankresponsecode;
}
public void setBankresponsecode(String bankresponsecode) {
	this.bankresponsecode = bankresponsecode;
}
public String getBankresponsecodedesc() {
	return bankresponsecodedesc;
}
public void setBankresponsecodedesc(String bankresponsecodedesc) {
	this.bankresponsecodedesc = bankresponsecodedesc;
}
public String getMainstatus() {
	return mainstatus;
}
public void setMainstatus(String mainstatus) {
	this.mainstatus = mainstatus;
}


}
