package com.springboot.ecmu.service.impl;

import org.springframework.stereotype.Service;


import com.springboot.ecmu.model.mandates;
import com.springboot.ecmu.model.terminals;
import com.springboot.ecmu.model.notifypaymentmerchant;
import com.springboot.ecmu.repository.MerchantServicesRespository;
import com.springboot.ecmu.repository.TerminalsRespository;
import com.springboot.ecmu.service.SpeedpointWebhookService;

@Service
public class SpeedpointWebhookImpl implements SpeedpointWebhookService{
	private MerchantServicesRespository merchantServicesRespository;
	private TerminalsRespository terminalsRespository;

	
	private SpeedpointWebhookImpl(MerchantServicesRespository merchantServicesRespository, TerminalsRespository terminalsRespository) {
		super();
		this.merchantServicesRespository = merchantServicesRespository;
		this.terminalsRespository = terminalsRespository;
	}

	
	@Override
	public String notifyPayment(String trans_end_time, String charset, String store_no, String pay_scenario,
			String sign, String trans_fee_c, String discount_bpc, String vat_amount, String order_amount, String app_id,
			String sign_type, String trans_status, String price_currency, String terminal_sn, String trans_type,
			String timestamp, String trans_no, String merchant_no, String pay_method_id,
			String pay_user_account_id, String format, String pay_method_id2, String trans_amount,
			String http_request_id, String version, String pay_channel_trans_no, String paid_amount,
			String discount_bmopc, String cashbackamount, String receivableamount, String settlementtransamount) {
		    String Internalusercode=null;
		    String Usercode=null;
			notifypaymentmerchant transation = new notifypaymentmerchant();
		    boolean transactionexists=merchantServicesRespository.existsBytransno(trans_no);
		    boolean terminalsnexists=terminalsRespository.existsByterminalsn(terminal_sn);
		    System.out.println(trans_no);
		    System.out.println(pay_user_account_id);
		    System.out.println(terminal_sn);
		   		    
			try 
			{
				 //Check if record exist
				if (transactionexists)
				{
					System.out.println("Transaction exist, will not be loaded");
					return "Success";
				}
				else
				{
				//Check if device is assigned Internal UserCode
				System.out.println(terminalsnexists);
			   		    
				if(terminalsnexists) {
					terminals terminal= terminalsRespository.findByterminalsn(terminal_sn);
					Internalusercode=terminal.getInternalusercode();
					Usercode=terminal.getUsercode();
				}else {
						
					Internalusercode="Device not assigned";
					Usercode="Device not assigned";
				}
				transation.setTransno(trans_no);
				transation.setMethod(pay_method_id2);
				transation.setMerchantno(merchant_no);
				transation.setTranstype(trans_type);
				transation.setMerchantorderno(null);
				transation.setPricecurrency(price_currency);
				transation.setOrderamount(order_amount);
				transation.setTransamount(trans_amount);
				transation.setTransfeec(trans_fee_c);
				transation.setAttach(null);
				transation.setStoreno(store_no);
				transation.setTerminalsn(terminal_sn);
				transation.setPayscenario(pay_scenario);
				transation.setTransstatus(trans_status);
				transation.setPaychanneltransno(pay_channel_trans_no);
				transation.setPayuseraccountid(pay_user_account_id);
				transation.setPaidamount(paid_amount);
				transation.setDiscountbmopc(discount_bmopc);
				transation.setDiscountbpc(discount_bpc);
				transation.setTransendtime(trans_end_time);
				transation.setPaymethodid(null);
				transation.setVatamount(vat_amount);
				transation.setTransamountcny(null);
				transation.setExchangerate(null);
				transation.setRefno(null);
				transation.setHttprequestid(http_request_id);
				transation.setVersion(version);
				transation.setInternalusercode(Internalusercode);
				transation.setUsercode(Usercode);
				transation.setCashbackamount(cashbackamount);
				transation.setSettlementtransamount(settlementtransamount);
				transation.setReceivableamount(receivableamount);
				merchantServicesRespository.save(transation);
				System.out.println("Transaction Saved");
				return "Success";
				}
			}
			catch(Exception e) 
			{
				String error="Something went wrong"+e.getMessage()+e.getCause();
				System.out.println(error);
			    return "Something went wrong"+e.getMessage()+e.getCause();
			}
	}
	

}
