package com.springboot.ecmu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.springboot.ecmu.model.terminals;

public interface TerminalsRespository extends JpaRepository<terminals, String>{
	//Mandates save(Mandates mandates);
	terminals findByterminalsn(String terminalsn);
	boolean existsByterminalsn(String terminalsn);
}