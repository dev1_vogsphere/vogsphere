package com.springboot.ecmu.service;

import java.util.List;

import com.springboot.ecmu.model.DebiCheck;
import com.springboot.ecmu.model.mandates;


public interface DebicheckService {

	
	String queryDebichek(String reference, String bSVA_response, String bANK_response_code,
			String mandate_current_status, String pAIN012_response_code);

	String queryDebichek(String reference, String reponse);
	
	

}
