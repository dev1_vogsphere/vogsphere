package com.springboot.ecmu.model;

public class UpdateDebiCheck {

	  public String reference;
	  public String tracking_indicator;
	  public String debtor_auth_code;
	  public String auth_type;
	  public String instalment_occurence;
	  public String frequency;
	  public String  mandate_initiation_date;
	  public String first_collection_date;
	  public String collection_amount_currency;
	  public String collection_amount;
	  public String maximum_collection_currency;
	  public String maximum_collection_amount;
	  public String entry_class;
	  public String debtor_account_name;
	  public String debtor_identification;
	  public String debtor_account_number;
	  public String debtor_account_type;
	  public String debtor_branch_number;
	  public String debtor_contact_number;
	  public String debtor_email;
	  public String collection_date;
	  public String date_adjustment_rule_indicator;
	  public String adjustment_category;
	  public String adjustment_rate;
	  public String adjustment_amount_currency;
	  public String adjustment_amount;
	  public String first_collection_amount_currency;
	  public String first_collection_amount;
	  public String debit_value_type;
	  public String cancellation_reason;
	  public String amended;
	  public String document_type;
	  public String amendment_reason_md16;
	  public String amendment_reason_text;
	  public String tracking_cancellation_indicator;
	  public String collection_date_text;
	  public String changedby;
	  public String changedon;
	  public String changedat;
	  public String IntUserCode;
	  public String rmsIndicator;
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getTracking_indicator() {
		return tracking_indicator;
	}
	public void setTracking_indicator(String tracking_indicator) {
		this.tracking_indicator = tracking_indicator;
	}
	public String getDebtor_auth_code() {
		return debtor_auth_code;
	}
	public void setDebtor_auth_code(String debtor_auth_code) {
		this.debtor_auth_code = debtor_auth_code;
	}
	public String getAuth_type() {
		return auth_type;
	}
	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}
	public String getInstalment_occurence() {
		return instalment_occurence;
	}
	public void setInstalment_occurence(String instalment_occurence) {
		this.instalment_occurence = instalment_occurence;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getMandate_initiation_date() {
		return mandate_initiation_date;
	}
	public void setMandate_initiation_date(String mandate_initiation_date) {
		this.mandate_initiation_date = mandate_initiation_date;
	}
	public String getFirst_collection_date() {
		return first_collection_date;
	}
	public void setFirst_collection_date(String first_collection_date) {
		this.first_collection_date = first_collection_date;
	}
	public String getCollection_amount_currency() {
		return collection_amount_currency;
	}
	public void setCollection_amount_currency(String collection_amount_currency) {
		this.collection_amount_currency = collection_amount_currency;
	}
	public String getCollection_amount() {
		return collection_amount;
	}
	public void setCollection_amount(String collection_amount) {
		this.collection_amount = collection_amount;
	}
	public String getMaximum_collection_currency() {
		return maximum_collection_currency;
	}
	public void setMaximum_collection_currency(String maximum_collection_currency) {
		this.maximum_collection_currency = maximum_collection_currency;
	}
	public String getMaximum_collection_amount() {
		return maximum_collection_amount;
	}
	public void setMaximum_collection_amount(String maximum_collection_amount) {
		this.maximum_collection_amount = maximum_collection_amount;
	}
	public String getEntry_class() {
		return entry_class;
	}
	public void setEntry_class(String entry_class) {
		this.entry_class = entry_class;
	}
	public String getDebtor_account_name() {
		return debtor_account_name;
	}
	public void setDebtor_account_name(String debtor_account_name) {
		this.debtor_account_name = debtor_account_name;
	}
	public String getDebtor_identification() {
		return debtor_identification;
	}
	public void setDebtor_identification(String debtor_identification) {
		this.debtor_identification = debtor_identification;
	}
	public String getDebtor_account_number() {
		return debtor_account_number;
	}
	public void setDebtor_account_number(String debtor_account_number) {
		this.debtor_account_number = debtor_account_number;
	}
	public String getDebtor_account_type() {
		return debtor_account_type;
	}
	public void setDebtor_account_type(String debtor_account_type) {
		this.debtor_account_type = debtor_account_type;
	}
	public String getDebtor_branch_number() {
		return debtor_branch_number;
	}
	public void setDebtor_branch_number(String debtor_branch_number) {
		this.debtor_branch_number = debtor_branch_number;
	}
	public String getDebtor_contact_number() {
		return debtor_contact_number;
	}
	public void setDebtor_contact_number(String debtor_contact_number) {
		this.debtor_contact_number = debtor_contact_number;
	}
	public String getDebtor_email() {
		return debtor_email;
	}
	public void setDebtor_email(String debtor_email) {
		this.debtor_email = debtor_email;
	}
	public String getCollection_date() {
		return collection_date;
	}
	public void setCollection_date(String collection_date) {
		this.collection_date = collection_date;
	}
	public String getDate_adjustment_rule_indicator() {
		return date_adjustment_rule_indicator;
	}
	public void setDate_adjustment_rule_indicator(String date_adjustment_rule_indicator) {
		this.date_adjustment_rule_indicator = date_adjustment_rule_indicator;
	}
	public String getAdjustment_category() {
		return adjustment_category;
	}
	public void setAdjustment_category(String adjustment_category) {
		this.adjustment_category = adjustment_category;
	}
	public String getAdjustment_rate() {
		return adjustment_rate;
	}
	public void setAdjustment_rate(String adjustment_rate) {
		this.adjustment_rate = adjustment_rate;
	}
	public String getAdjustment_amount_currency() {
		return adjustment_amount_currency;
	}
	public void setAdjustment_amount_currency(String adjustment_amount_currency) {
		this.adjustment_amount_currency = adjustment_amount_currency;
	}
	public String getAdjustment_amount() {
		return adjustment_amount;
	}
	public void setAdjustment_amount(String adjustment_amount) {
		this.adjustment_amount = adjustment_amount;
	}
	public String getFirst_collection_amount_currency() {
		return first_collection_amount_currency;
	}
	public void setFirst_collection_amount_currency(String first_collection_amount_currency) {
		this.first_collection_amount_currency = first_collection_amount_currency;
	}
	public String getFirst_collection_amount() {
		return first_collection_amount;
	}
	public void setFirst_collection_amount(String first_collection_amount) {
		this.first_collection_amount = first_collection_amount;
	}
	public String getDebit_value_type() {
		return debit_value_type;
	}
	public void setDebit_value_type(String debit_value_type) {
		this.debit_value_type = debit_value_type;
	}
	public String getCancellation_reason() {
		return cancellation_reason;
	}
	public void setCancellation_reason(String cancellation_reason) {
		this.cancellation_reason = cancellation_reason;
	}
	public String getAmended() {
		return amended;
	}
	public void setAmended(String amended) {
		this.amended = amended;
	}
	public String getDocument_type() {
		return document_type;
	}
	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}
	public String getAmendment_reason_md16() {
		return amendment_reason_md16;
	}
	public void setAmendment_reason_md16(String amendment_reason_md16) {
		this.amendment_reason_md16 = amendment_reason_md16;
	}
	public String getAmendment_reason_text() {
		return amendment_reason_text;
	}
	public void setAmendment_reason_text(String amendment_reason_text) {
		this.amendment_reason_text = amendment_reason_text;
	}
	public String getTracking_cancellation_indicator() {
		return tracking_cancellation_indicator;
	}
	public void setTracking_cancellation_indicator(String tracking_cancellation_indicator) {
		this.tracking_cancellation_indicator = tracking_cancellation_indicator;
	}
	public String getCollection_date_text() {
		return collection_date_text;
	}
	public void setCollection_date_text(String collection_date_text) {
		this.collection_date_text = collection_date_text;
	}
	public String getChangedby() {
		return changedby;
	}
	public void setChangedby(String changedby) {
		this.changedby = changedby;
	}
	public String getChangedon() {
		return changedon;
	}
	public void setChangedon(String changedon) {
		this.changedon = changedon;
	}
	public String getChangedat() {
		return changedat;
	}
	public void setChangedat(String changedat) {
		this.changedat = changedat;
	}
	public String getIntUserCode() {
		return IntUserCode;
	}
	public void setIntUserCode(String intUserCode) {
		IntUserCode = intUserCode;
	}
	public String getRmsIndicator() {
		return rmsIndicator;
	}
	public void setRmsIndicator(String rmsIndicator) {
		this.rmsIndicator = rmsIndicator;
	}
	 

}
