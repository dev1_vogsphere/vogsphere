package com.springboot.ecmu.controller;

import javax.xml.ws.RespectBindingFeature;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.soap.AddressingFeature;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.ws.policy.spring.PolicyEngineBeanDefinitionParser;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.paymentcollections.DownloadResponse;
import com.paymentcollections.IMtomBulk;
import com.paymentcollections.MtomBulk;
import com.paymentcollections.UploadResponse;
import com.querydebicheck.DebicheckOnlineQuery;
import com.querydebicheck.IQueryDebicheck;
import com.querydebicheck.QueryResponse;
import com.springboot.ecmu.fin.model.capturepayment;
import com.springboot.ecmu.model.CancelDebiCheck;
import com.springboot.ecmu.model.DebiCheck;
import com.springboot.ecmu.model.MercantileDownload;
import com.springboot.ecmu.model.MercantileUpload;
import com.springboot.ecmu.model.mandates;
import com.springboot.ecmu.model.RetryDebiCheck;
import com.springboot.ecmu.model.SpeedpointWebhook;
import com.springboot.ecmu.model.UpdateDebiCheck;
//import com.springboot.ecmu.model.User;
import com.springboot.ecmu.repository.MandatesRespository;
import com.springboot.ecmu.service.CancelDebiCheckService;
import com.springboot.ecmu.service.CollectionsDownloadReponseService;
import com.springboot.ecmu.service.CollectionsUploadService;
import com.springboot.ecmu.service.ContributionService;
import com.springboot.ecmu.service.DebiCheckMandateDownloadResponseService;
import com.springboot.ecmu.service.DebiCheckMandateUploadService;
import com.springboot.ecmu.service.DebicheckService;
import com.springboot.ecmu.service.RetryDebiCheckService;
import com.springboot.ecmu.service.SpeedpointWebhookService;
import com.springboot.ecmu.service.UpdateDebiCheckService;
//import com.springboot.ecmu.service.UserService;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



@RestController
public class DebiCheckController {	
	
	public DebicheckService debicheckservice;
	public RetryDebiCheckService retryDebiCheckService;
	public CancelDebiCheckService cancelDebiCheckService;
	public UpdateDebiCheckService updateDebiCheckService;
	public SpeedpointWebhookService speedpointWebhookService;
	public ContributionService contributionService;
	//public CollectionsUploadService collectionsUploadService;
//	public DebiCheckMandateUploadService debiCheckMandateUpoadService;
	public CollectionsDownloadReponseService collectionsDownloadReponseService;
	//public DebiCheckMandateDownloadResponseService debiCheckMandateDownloadResponseService;
    //public UserService userService;

	public DebiCheckController(DebicheckService debicheckservice,RetryDebiCheckService retryDebiCheckService,CancelDebiCheckService cancelDebiCheckService,UpdateDebiCheckService updateDebiCheckService,SpeedpointWebhookService speedpointWebhookService,ContributionService contributionService,CollectionsDownloadReponseService collectionsDownloadReponseService) {
		super();
		this.debicheckservice = debicheckservice;
		this.retryDebiCheckService = retryDebiCheckService;
		this.cancelDebiCheckService = cancelDebiCheckService;
		this.updateDebiCheckService = updateDebiCheckService;
		this.speedpointWebhookService= speedpointWebhookService;
		this.contributionService=contributionService;
		//this.collectionsUploadService=collectionsUploadService;
		//this.debiCheckMandateUpoadService=debiCheckMandateUpoadService;
		this.collectionsDownloadReponseService = collectionsDownloadReponseService;
		//this.debiCheckMandateDownloadResponseService=debiCheckMandateDownloadResponseService;
		//this.userService=userService;
	}


	//Resubmit DebiCheck  Mandate API
	@PostMapping("/api/retrydebicheck")
	public String retryDebiCheckService(@RequestBody RetryDebiCheck retryDebiCheck) {

		return  retryDebiCheckService.ReSubmitDebiCheck(retryDebiCheck.getidmandate(), retryDebiCheck.getChangedby());
	}
	
	//Cancel DebiCheck  Mandate API
	@PostMapping("/api/canceldebicheck")
	public String cancelDebiCheckService(@RequestBody CancelDebiCheck cancelDebiCheck) {
		
		return  cancelDebiCheckService.CancelDebiCheck(cancelDebiCheck.getReference(),cancelDebiCheck.cancellation_reason,cancelDebiCheck.getChangedby());
		
	} 
	
	//Update DebiCheck  Mandate API
	@PostMapping("/api/updatedebicheck")
	public String UpdateDebiCheckService(@RequestBody UpdateDebiCheck updateDebiCheck) {
	return  updateDebiCheckService.UpdateDebiCheck(updateDebiCheck.reference, updateDebiCheck.getCollection_amount(), updateDebiCheck.first_collection_amount,updateDebiCheck.tracking_indicator, updateDebiCheck.mandate_initiation_date, updateDebiCheck.first_collection_date, updateDebiCheck.maximum_collection_amount, updateDebiCheck.debtor_account_name, updateDebiCheck.debtor_account_number, updateDebiCheck.debtor_account_type, updateDebiCheck.debtor_branch_number, updateDebiCheck.debtor_identification, updateDebiCheck.getDebtor_contact_number(), updateDebiCheck.debtor_email, updateDebiCheck.amendment_reason_text, updateDebiCheck.collection_date, updateDebiCheck.date_adjustment_rule_indicator, updateDebiCheck.adjustment_category, updateDebiCheck.adjustment_rate, updateDebiCheck.adjustment_amount, updateDebiCheck.debit_value_type, updateDebiCheck.amended, updateDebiCheck.changedby);
		
	} 
		
	@PostMapping("/api/notifycontribution")
	@ResponseBody 
	public String capturepayment (@RequestBody capturepayment capturepayment){
		String clientreference=capturepayment.getClientreference();
		System.out.println(clientreference);
		String branch=capturepayment.getBranch();
		System.out.println(branch);
		String agent=capturepayment.getAgent();
		System.out.println(agent);
		String paymentdate=capturepayment.getPaymentdate();
		System.out.println(paymentdate);
		String amount=capturepayment.getAmount();
		System.out.println(amount);
		
	    return contributionService.notifycontribution(clientreference, branch, agent, paymentdate, amount);
	    
	
	}
	//Get DebiCheck Status Mandate API
	@PostMapping("/api/debicheck")
	public String queryDebicheck(@RequestBody DebiCheck debiCheck )
	{
		
		String mandates="";
		String mandates_status="";
		String reference ="";
		String BSVA_response="";
		String BANK_response_code="";
		String mandate_current_status="";
		String PAIN012_response_code="";
		//MandatesRespository mandates=new MandatesRespository();
		//Mandates mandate=new Mandates();
		System.out.print(debiCheck.getInMessage());
		System.out.print("/n");
		System.out.print(debiCheck.getUserName());
		System.out.print("/n");
		System.out.print(debiCheck.getUserInst());
		System.out.print("/n");
		System.out.print(debiCheck.getPassWord());
		System.out.print("/n");
		System.out.print(debiCheck.getFootPrint());
		
	
		List<WebServiceFeature> wsFeatures = new ArrayList<WebServiceFeature>();
		wsFeatures.add(new RespectBindingFeature()); 
		wsFeatures.add(new AddressingFeature(true)); 
		WebServiceFeature[] wsFeatureArray = wsFeatures.toArray(new WebServiceFeature[1]);
		// SpringBus springBus = new SpringBus();

		//String originalInput = "MQH2023AugLFD0009";
		Base64 base64 = new Base64();
		//String encodedString = new String(base64.encode(originalInput.getBytes()));
		//String test="TVFIMjAyM0F1Z0xGRDAwMDk=";
				
		IQueryDebicheck queryMandate = new DebicheckOnlineQuery().getWSHttpBindingIQueryDebicheck(wsFeatureArray);
		QueryResponse response =queryMandate.queryMandate(debiCheck.getInMessage(),debiCheck.getUserName(),debiCheck.getUserInst(),debiCheck.getPassWord(),debiCheck.getFootPrint());
		//System.out.print(originalInput.getBytes());
		//String encodedString=(debiCheck.getInMessage(), StandardCharsets.UTF_8);
		//String encodedString=	new String(debiCheck.getInMessage());
		//String reference =encodedString.substring(3);
	
		//get lenght og the response 

		int response_lenght= (response.getReturnVal()).length();
		System.out.print(response);
		if(response_lenght>=372)
		{
		mandates_status=response.getReturnVal();
		reference =(response.getReturnVal()).substring(87, 101);
		BSVA_response=(response.getReturnVal()).substring(294, 300);
		BANK_response_code=(response.getReturnVal()).substring(301, 307);
		mandate_current_status=(response.getReturnVal()).substring(314,349);
		PAIN012_response_code=(response.getReturnVal()).substring(308, 313);
		PAIN012_response_code=(response.getReturnVal()).substring(308, 313);
		
		System.out.print(reference);
		System.out.print(mandates_status);
		System.out.print(BSVA_response);
		System.out.print(BANK_response_code);
		System.out.print(mandate_current_status);
		System.out.print(PAIN012_response_code);
		
		return debicheckservice.queryDebichek(reference,BSVA_response,BANK_response_code,mandate_current_status,PAIN012_response_code);
		}
		else if (response.getReturnVal().contains("Error. No matching mandates found")) {
			String encodedString=	new String(debiCheck.getInMessage());
			reference =encodedString.substring(3);
			return debicheckservice.queryDebichek(reference,response.getReturnVal());
		}
		else {
			return response.getReturnVal();
		}
		
	}
	
	@PostMapping("/api/notifypayment")
	public String SpeedpointWebhook (@RequestBody SpeedpointWebhook speedpointwebhook){
		String trans_end_time=speedpointwebhook.getTrans_end_time();
		String charset=speedpointwebhook.getCharset();
		String store_no=speedpointwebhook.getStore_no();
		String pay_scenario=speedpointwebhook.getPay_scenario();
		String sign=speedpointwebhook.getSign();
		String trans_fee_c=speedpointwebhook.getTrans_fee_c();
		String discount_bpc=speedpointwebhook.getDiscount_bpc();
		String vat_amount=speedpointwebhook.getVat_amount();
		String order_amount=speedpointwebhook.getOrder_amount();
		String app_id=speedpointwebhook.getApp_id();
		String sign_type=speedpointwebhook.getSign_type();
		String trans_status=speedpointwebhook.getTrans_status();
		String price_currency=speedpointwebhook.getPrice_currency();
		String terminal_sn=speedpointwebhook.getTerminal_sn();
		String trans_type=speedpointwebhook.getTrans_type();
		String timestamp=speedpointwebhook.getTimestamp();
		String trans_no=speedpointwebhook.getTrans_no();
		String merchant_no=speedpointwebhook.getMerchant_no();
		String method=speedpointwebhook.getMethod();
		String pay_user_account_id=speedpointwebhook.getPay_user_account_id();
		String format=speedpointwebhook.getFormat();
		String pay_method_id=speedpointwebhook.getPay_method_id();
		String trans_amount=speedpointwebhook.getTrans_amount();
		String http_request_id=speedpointwebhook.getHttp_request_id();
		String version=speedpointwebhook.getVersion();
		String pay_channel_trans_no=speedpointwebhook.getPay_channel_trans_no();
		String paid_amount=speedpointwebhook.getPaid_amount();
		String discount_bmopc=speedpointwebhook.getDiscount_bmopc();
		String cashback_amount=speedpointwebhook.getCashback_amount();
		String receivable_amount=speedpointwebhook.getReceivable_amount();
		String settlement_trans_amount=speedpointwebhook.getSettlement_trans_amount();
		System.out.println(speedpointwebhook.toString());
	    return speedpointWebhookService.notifyPayment(trans_end_time, charset, store_no, pay_scenario, sign, trans_fee_c, discount_bpc, vat_amount, order_amount, app_id, sign_type, trans_status, price_currency, terminal_sn, trans_type, timestamp, trans_no, merchant_no, method, pay_user_account_id, format, pay_method_id, trans_amount, http_request_id, version, pay_channel_trans_no, paid_amount, discount_bmopc,cashback_amount,receivable_amount,settlement_trans_amount);

	}
	
	//DownloadCollectionsResponses
	@PostMapping("/api/mercantiledownloadreponses")
	public String DownloadCollectionsResponses(@RequestBody MercantileDownload mercantileDownload) {
		
		String UserName = "SOAP";
	    String UserInst = "";
	    String PassWord = "";
	    String FootPrint = "";
	    
		try {
			Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ecashpdq_paycollections","ecashpdq_root","nsa{VeIsl)+h");
	    	//Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ecashpdq_paycollections","mlu","Mulu$2625");
			// 2. Get a connection to database 
			Statement myStmt = myConn.createStatement();
			// 3. Execute SQL query
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM ecashpdq_paycollections.client where bank = 'mercantile' ");
			   while(myRs.next())
	    	    {
					UserInst=myRs.getString("usercode");
					PassWord=myRs.getString("password");
					System.out.println(UserName);
					System.out.println(UserInst);
					System.out.println(PassWord);
					System.out.println(FootPrint);
				   
				  
					// set up JAX attributes        
				    List<WebServiceFeature> wsFeatures = new ArrayList<WebServiceFeature>();
		 			wsFeatures.add(new RespectBindingFeature()); 
		 			wsFeatures.add(new AddressingFeature(true)); 
		 			WebServiceFeature[] wsFeatureArray = wsFeatures.toArray(new WebServiceFeature[1]);
		 			//SpringBus springBus = new SpringBus();
		 			
					       
					//IMtomBulk service = new sample.MtomBulk().getWSHttpBindingIMtomBulk(wsFeatureArray);   
				    IMtomBulk service = new MtomBulk().getWSHttpBindingIMtomBulk(wsFeatureArray);
					DownloadResponse response  =service.download(UserName, UserInst, PassWord,FootPrint);
					System.out.println(response.toString());
					FTPClient client = new FTPClient();
					  
					if (response.getFileName()==null || response.getReturnVal().contains("No files in queue") )
					{
						System.out.println(response.getReturnVal());
						return response.getReturnVal();
					}
					else
					{
						
					//Call FTP
					String Outputfilename=response.getFileName();
					//String filename ="C:\\Users\\MuluM\\Documents\\Development\\vogsphere\\Programs\\Mercantile" +Outputfilename;//local
					 String filename ="/var/www/html/Files/Collections/In/" +Outputfilename; //Production
					   //String RemoteFilename = "/public_html/Files/Collections/In/"+Outputfilename;//Production
					String RemoteFilename = "/var/www/html/Files/Collections/In/"+Outputfilename;//Dev
					    System.out.println(Outputfilename);
					    BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
					    FileInputStream bis = new FileInputStream(filename);
					    byte[] responseDownload = response.getFileContents();
					    String output = new String(responseDownload);
					    writer.write(output);
					    writer.close();
					    System.out.println(response.getRemainingFiles());
					     
					try {
						   
						   //client.connect("154.0.164.183", 21); //Production
					   client.connect("154.0.170.80", 21); // Dev
					     
					   // Try to login and return the respective boolean value
					   boolean login = client.login("ecashpdq", "nsa{VeIsl)+h");
					     
					   // If login is true notify user
					    
					   if (login) {
						   
					    System.out.println("Connection established...");
					   System.out.println("Starting to Upload...");
					   client.setFileType(client.BINARY_FILE_TYPE);
					   client.setFileTransferMode(client.BINARY_FILE_TYPE);
					   client.enterLocalPassiveMode();
					  
					   client.storeFile(RemoteFilename, bis);
					   System.out.println("Starting to Uploaded...");
					  // writer1.write(responseDownload);
					   bis.close();
					  //client.writer.write(output);
					       
					  // Try to logout and return the respective boolean value
					   boolean logout = client.logout();
					
					   // If logout is true notify user
					   if (logout) {
					
					 System.out.println("Connection close...");
					    
					       }
					   //  Notify user for failure  
					   } else {
					       System.out.println("Connection fail...");
					   }
					     
					     } catch (IOException e) {
					    
					   e.printStackTrace();
					    
					     } 
					finally 
					{
					    
					   try 
					   {
					       // close connection
					       client.disconnect();
					    
					   } catch (IOException e) 
					   	{					    
					       e.printStackTrace();
					       
					    }
					    
					 }
					
			      	while (response.getRemainingFiles()!=0)
			        {
			        	service = new 	MtomBulk().getWSHttpBindingIMtomBulk();
			        	response  =service.download(UserName, UserInst, PassWord,FootPrint);
			           
			          	//Contract file name 
			          	Outputfilename=response.getFileName();
			          	
			            //filename = "/Users/mulunghisim/Documents/Programs/Mercantile/src/mercantile/"+Outputfilename; 
			           filename ="/var/www/html/Files/Collections/In/" +Outputfilename;
			           // RemoteFilename = "/public_html/Files/Collections/In/"+Outputfilename; //Production
			           // RemoteFilename = "/public_html/Files/Collections/In/"+Outputfilename; // Dev
			            RemoteFilename = "/var/www/html/Files/Collections/In/"+Outputfilename;//Dev
			            System.out.println(Outputfilename);

			            writer = new BufferedWriter(new FileWriter(filename));
			            bis = new FileInputStream(filename);
			            responseDownload = response.getFileContents();
			            output = new String(responseDownload);
			          	writer.write(output);
			          	writer.close(); 
			          	System.out.println(response.getRemainingFiles());
			          	
			          	//Upload file
			          	
			          	
			          	try {
			 			   
			 			   //client.connect("154.0.164.183", 21); //Production
			 			   client.connect("154.0.170.80", 21); // Dev
			 			     
			 			   // Try to login and return the respective boolean value
			 			   boolean login = client.login("ecashpdq", "nsa{VeIsl)+h");
			 			     
			 			   // If login is true notify user
			 			    
			 			   if (login) {
			 				   
			 			       System.out.println("Connection established...");
			 			       System.out.println("Starting to Upload...");
			 			       client.setFileType(client.BINARY_FILE_TYPE);
			 			       client.setFileTransferMode(client.BINARY_FILE_TYPE);
			 			       client.enterLocalPassiveMode();
			 			      
			 			       client.storeFile(RemoteFilename, bis);
			 			       System.out.println("Starting to Uploaded...");
			 			      // writer1.write(responseDownload);
			 			       bis.close();
			 			      //client.writer.write(output);
			 			      
			 			   
			 			     
			 			       // Try to logout and return the respective boolean value
			 			       boolean logout = client.logout();
			 			    
			 			       // If logout is true notify user
			 			       if (logout) {
			 			    
			 			     System.out.println("Connection close...");
			 			    
			 			       }
			 			   //  Notify user for failure  
			 			   } else {
			 			       System.out.println("Connection fail...");
			 			   }
			 			     
			 			     } catch (IOException e) {
			 			    
			 			   e.printStackTrace();
			 			    
			 			     } finally {
			 			    
			 			   try {
			 			       // close connection
			 			    
			 			       client.disconnect();
			 			    
			 			   } catch (IOException e) {
			 			    
			 			       e.printStackTrace();
			 			    
			 			   }
			 			    
			 			     }//end file upload
			    	
			        	}
				     
	    	    }
	    }
	    }
	    catch (Exception exc)
	    {
	    	exc.printStackTrace();
	    }
		return "Completed....";

		//return  collectionsDownloadReponseService.collectionsdownloadreponse(UserName, UserInst, PassWord, FootPrint);
	}

	/*
	 * //UploadCollectionsResponses
	 * 
	 * @PostMapping("/api/mercantileuploadrequest") public String
	 * UploadCollectionsRequest(@RequestBody MercantileUpload mercantileupload) {
	 * 
	 * byte [] inMessage = {}; String userName ="SOAP"; String userInst =""; String
	 * passWord =""; String footPrint =" "; String gennumber =""; //Get Connection
	 * to Database try { // 1. Get a connection to database Connection myConn =
	 * DriverManager.getConnection(
	 * "jdbc:mysql://localhost:3306/ecashpdq_paycollections","ecashpdq",
	 * "nsa{VeIsl)+h"); // 2. Get a connection to database Statement myStmt =
	 * myConn.createStatement(); // 3. Execute SQL query ResultSet myRs = myStmt.
	 * executeQuery("SELECT * FROM ecashpdq_paycollections.bankqueue inner join ecashpdq_paycollections.client on ecashpdq_paycollections.bankqueue.usercode = ecashpdq_paycollections.client.usercode where ecashpdq_paycollections.bankqueue.status='Pending'"
	 * );
	 * 
	 * while(myRs.next()) { userInst=myRs.getString("usercode");
	 * inMessage=myRs.getBytes("encodeddata"); passWord=myRs.getString("password");
	 * gennumber=myRs.getString("gennumber"); //System.out.println(userInst + ", " +
	 * passWord + ", " + inMessage);
	 * 
	 * 
	 * List<WebServiceFeature> wsFeatures = new ArrayList<WebServiceFeature>();
	 * wsFeatures.add(new RespectBindingFeature()); wsFeatures.add(new
	 * AddressingFeature(true)); WebServiceFeature[] wsFeatureArray =
	 * wsFeatures.toArray(new WebServiceFeature[1]);
	 * 
	 * IMtomBulk service = new MtomBulk().getWSHttpBindingIMtomBulk(wsFeatureArray);
	 * UploadResponse response =service.upload(inMessage,
	 * userName,userInst,passWord, ""); System.out.println(response.getReturnVal());
	 * 
	 * //check if requested uploaded Ok
	 * if(response.getReturnVal().contains("uploaded OK")) { Statement myStmtU =
	 * myConn.createStatement(); int myRu = myStmtU.
	 * executeUpdate("UPDATE ecashpdq_paycollections.bankqueue SET ecashpdq_paycollections.bankqueue.status = 'Submitted' WHERE ecashpdq_paycollections.bankqueue.gennumber="
	 * +gennumber); System.out.println(gennumber+" Sumbitted to the Bank"); }
	 * 
	 * Thread.sleep(10000);
	 * 
	 * }
	 * 
	 * } catch (Exception exc) { exc.printStackTrace(); }
	 * System.out.println("No files in the Queue"); return "Completed"; }
	 */
	
	
	/*
	// Validate User REST API
	@PostMapping("/api/users")
	public ResponseEntity<User> validateUser(@RequestBody User user) {
		
		if (userService.existsByEmail(user.getEmail())) {
			return new ResponseEntity<User>(userService.validateUser(user),HttpStatus.BAD_GATEWAY);
		 }else {
			
		return new ResponseEntity<User>(userService.validateUser(user),HttpStatus.CREATED);
		 }
	}
	
	//Get All Users REST API
	@GetMapping("/api/users/getAllUsers")
	public List<User> getAllUsers(){
		return userService.getAllUsers();
	}

	//Get User By Id REST API
	//http://localhost:8080/api/user/1
	@GetMapping("/api/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable ("id") String id){
	return new ResponseEntity<User>(userService.getUserById(id),HttpStatus.OK);

	}

	//Update Rest Password REST API
	//http://localhost:8080/api/user/1
	@PutMapping("/api/users/{id}")
	public ResponseEntity<String> passwordRest(@PathVariable ("id") String id, @RequestBody User user) {
		//return new ResponseEntity<User>(userService.passwordRest(user, id), HttpStatus.OK);
		return new ResponseEntity<String>("Password Successfully Updated", HttpStatus.OK);
	}

	
	//Delete User REST API
	//http://localhost:8080/api/user/1
	@DeleteMapping("/api/users/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable ("id") String id){
		//delete User from DB
		userService.deleteUser(id);
		return new ResponseEntity<String>("User deleted Successfully", HttpStatus.OK);
		
	}*/




}
