package com.springboot.ecmu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.ecmu.model.mandates;


public interface MandatesRespository extends JpaRepository<mandates, String>{
	//Mandates save(Mandates mandates);
	mandates findByReference(String reference);
	mandates findByidmandate(String idmandate);

	
}
