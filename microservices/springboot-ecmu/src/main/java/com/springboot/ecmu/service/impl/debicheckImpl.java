package com.springboot.ecmu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.ws.RespectBindingFeature;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.soap.AddressingFeature;

import org.apache.commons.codec.binary.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.springboot.ecmu.exception.ResourceNotFoundException;
import com.springboot.ecmu.model.DebiCheck;
import com.springboot.ecmu.model.debi_check_responses;
import com.springboot.ecmu.model.mandates;

import com.springboot.ecmu.repository.DebiCheckResponseCodesRespository;
import com.springboot.ecmu.repository.MandatesRespository;

import com.springboot.ecmu.service.DebicheckService;


@Service 
public class debicheckImpl implements DebicheckService{
	private MandatesRespository mandatesRespository;  
	private DebiCheckResponseCodesRespository debiCheckResponseCodesRespository;  
	

	public debicheckImpl(MandatesRespository mandatesRespository, DebiCheckResponseCodesRespository debiCheckResponseCodesRespository) {
		super();
		this.mandatesRespository = mandatesRespository;
		this.debiCheckResponseCodesRespository=debiCheckResponseCodesRespository;
	}

	@Override
	public String queryDebichek(String reference,String reponse) {
		// TODO Auto-generated method stub
		reference=reference.replaceAll("\\n"," ");
		reference=reference.replaceAll("\\r"," ");
		reference=reference.replaceAll("\\s"," ");
		mandates existingUreference = mandatesRespository.findByReference(reference);
		if (existingUreference!=null) 
		{
				try {
				existingUreference.setStatus_desc(reponse);
				// Save Password to database
				mandatesRespository.save(existingUreference);
				return "Record Updated" +reponse;
				}catch(Exception e) {
				return "Something went wrong"+e;
				}
		}else {
			return " "+ reponse;
		}
	}


	@Override
	public String queryDebichek(String reference, String bSVA_response, String bANK_response_code,
			String mandate_current_status, String pAIN012_response_code) {
		
		//Remove leading and trailing spaces
		pAIN012_response_code= pAIN012_response_code.trim();
		mandate_current_status=mandate_current_status.trim();
		bANK_response_code=bANK_response_code.trim();
		bSVA_response=bSVA_response.trim();
		reference.trim();
		String debitorderflag="TRUE";
		String debitorderRMSflag="NRSP";
				
		System.out.print(reference);
	    //Check if the user exist in the database
		mandates existingUreference = mandatesRespository.findByReference(reference.replaceAll("\n"," "));
		//find the bankresponse code description
		//DebiCheckResponses bankresponsedescription =debiCheckResponseCodesRespository.findByResponsecode(bANK_response_code);
		System.out.println("bank code is "+bANK_response_code);
		System.out.println("bankserve code is "+bSVA_response);
		
	    debi_check_responses bankresponsedescription =debiCheckResponseCodesRespository.findBybankresponsecode(bANK_response_code);
	    
	    if(bankresponsedescription==null)
	    {
	    	bankresponsedescription =debiCheckResponseCodesRespository.findBybankresponsecode(bSVA_response);	
	    }
        
		
		//Schedule Debit Order Indicator		
		if(pAIN012_response_code.contains(debitorderflag))
		{
			//existingUreference.setStatus_desc("Mandate Accepted");//Schedule Debit  Order instruction
			existingUreference.setCustomer_approval("Mandate Accepted");//Schedule Debit  Order instruction
		}
		else if(pAIN012_response_code.contains(debitorderRMSflag))
		{
			//existingUreference.setStatus_desc("RMS Mandate Accepted");//Schedule Debit order instruction
			existingUreference.setCustomer_approval("RMS Mandate Accepted");//Schedule Debit  Order instruction
		}
		else{
			existingUreference.setStatus_desc("Mandate not yet accepted by the client");
			//existingUreference.setMandate_current_status(mandate_current_status);
			existingUreference.setCustomer_approval(mandate_current_status);
			mandatesRespository.save(existingUreference);//Do not schedule Debit Order instruction 
			//=return existingUreference.getMandate_current_status();
		}	
		
		//Bank response
		if(bankresponsedescription.getBankresponsecodedesc()!="" )
		{
		existingUreference.setBank_response_code(bankresponsedescription.getBankresponsecodedesc());
		}else{
		existingUreference.setBank_response_code(bANK_response_code);
		}	
		
		//Bankserv Response
		existingUreference.setBsva_response(bSVA_response);
		
		//Bank response
		existingUreference.setMandate_current_status(mandate_current_status);
		
		//find the customer code description 
		//DebiCheckResponses customerresponsedescription =debiCheckResponseCodesRespository.findByResponsecode(pAIN012_response_code);
		debi_check_responses customerresponsedescription =debiCheckResponseCodesRespository.findBybankresponsecode(pAIN012_response_code);
		//Customer response
		if(customerresponsedescription!=null) {
				if(customerresponsedescription.getBankresponsecodedesc()!="")
				{
				existingUreference.setpAIN012_response_code(customerresponsedescription.getBankresponsecodedesc());
				}else{
				existingUreference.setpAIN012_response_code(pAIN012_response_code);
				}	
		}else {
			existingUreference.setpAIN012_response_code("Customer Response code not found "+pAIN012_response_code);
		}
					
		// Save Password to database
		try {
		mandatesRespository.save(existingUreference);
		return existingUreference.getMandate_current_status();
		}catch(Exception e) {
		return "Something went wrong"+e;
		}
		
	}
	
	
}

