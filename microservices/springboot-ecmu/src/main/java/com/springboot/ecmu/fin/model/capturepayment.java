package com.springboot.ecmu.fin.model;

public class capturepayment {
	String clientreference; 
	String branch; 
	String agent; 
	String paymentdate; 
	String amount;
	
	
	public String getClientreference() {
		return clientreference;
	}
	public void setClientreference(String clientreference) {
		this.clientreference = clientreference;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	} 

}
