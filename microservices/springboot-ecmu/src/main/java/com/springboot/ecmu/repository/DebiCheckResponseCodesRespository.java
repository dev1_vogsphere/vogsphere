package com.springboot.ecmu.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.ecmu.model.debi_check_responses;


public interface DebiCheckResponseCodesRespository extends JpaRepository<debi_check_responses, String> {
	//DebiCheckResponses findByResponsecode(String bankresponsecode);
	debi_check_responses findBybankresponsecode(String bankresponsecode);
}
