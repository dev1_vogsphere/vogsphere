/*package com.springboot.ecmu.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;

import com.springboot.ecmu.exception.ResourceNotFoundException;
import com.springboot.ecmu.model.User;
import com.springboot.ecmu.repository.UserRespository;
import com.springboot.ecmu.service.UserService;


@Service 
public class UserServiceImpl implements UserService {

	private UserRespository userRepository;  
	
	public UserServiceImpl(UserRespository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public User validateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public Boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
	 return userRepository.findAll();
	}

	@Override
	public User getUserById(String id) {
		//Optional< User> user = userRepository.findById(id);
	//	if(user.isPresent())
	//	{
	//		return user.get();
	//	}else
	//	{
		//	throw new ResourceNotFoundException("User","Id",id);
		//}
		//Lambda Expresssion,
		return userRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User","Id",id));
	}

	@Override
	public User passwordRest(User user, String id) {
	    //Check if the user exist in the database
		User existingUser = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User","Id",id));
		existingUser.setPassword(user.getPassword());
		// Save Password to database
		userRepository.save(existingUser);
		return existingUser;
	}

	@Override
	public void deleteUser(String id) {
		
		//Check if the User exist in database or not
		userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User","Id",id));
		userRepository.deleteById(id);
		
	}








}*/







