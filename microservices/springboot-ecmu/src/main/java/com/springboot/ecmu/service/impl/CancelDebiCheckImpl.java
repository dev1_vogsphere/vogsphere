package com.springboot.ecmu.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import com.springboot.ecmu.model.mandates;
import com.springboot.ecmu.repository.MandatesRespository;
import com.springboot.ecmu.service.CancelDebiCheckService;


@Service
public class CancelDebiCheckImpl implements CancelDebiCheckService  {
	private MandatesRespository mandatesRespository;  
	
	
	private CancelDebiCheckImpl(MandatesRespository mandatesRespository) {
		super();
		this.mandatesRespository = mandatesRespository;
	}

	@Override
	public String CancelDebiCheck(String reference, String cancellation_reason, String Changeby) {
		
		//Get Current date and time
		LocalDate dateObj = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String currentdatetime = dateObj.format(formatter);
		
		
		// Find Record to update and Resubmit
		mandates Cancelreference = mandatesRespository.findByReference(reference.replaceAll("\n"," "));
		
		// Rescheduled new entry to resubmit
		Cancelreference.setStatus_code("01");
		Cancelreference.setStatus_desc("Pending");
		Cancelreference.setCancellation_reason(cancellation_reason);
		Cancelreference.setChangedby(Changeby);
		Cancelreference.setChangedon(currentdatetime);
		// Save Password to database
		try {
		mandatesRespository.save(Cancelreference);
		return "Mandate cancellation request submitted for processing";
		}catch(Exception e) {
		return "Something went wrong"+e;
		}
	}

}
