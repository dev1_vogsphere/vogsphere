package com.springboot.ecmu.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import com.springboot.ecmu.model.mandates;
import com.springboot.ecmu.repository.MandatesRespository;
import com.springboot.ecmu.service.RetryDebiCheckService;


@Service
public class RetryDebiImpl implements RetryDebiCheckService{
	private MandatesRespository mandatesRespository;  
	
	private RetryDebiImpl(MandatesRespository mandatesRespository) {
		super();
		this.mandatesRespository = mandatesRespository;
	}


	@Override
	public String ReSubmitDebiCheck(String idmandate, String Changeby) {
		//Get Current date and time
		LocalDate dateObj = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String currentdatetime = dateObj.format(formatter);
	    int counter =0;
		// Find Record to update and Resubmit
		mandates Retryreference = mandatesRespository.findByReference(idmandate.replaceAll("\n"," "));
	    // Rescheduled new entry to resubmit
		Retryreference.setStatus_code("01");
		Retryreference.setStatus_desc("Pending");
		
		if(Retryreference.getRetry_counter()==null)
		{
			Retryreference.setRetry_counter(counter);
		}else {
			Retryreference.setRetry_counter(Retryreference.getRetry_counter()+1);
		}
		
		Retryreference.setChangedby(Changeby);
		Retryreference.setChangedon(currentdatetime);
		
		// Save Password to database
		try {
		mandatesRespository.save(Retryreference);
		return "Debi-Check request resubmitted";
		}catch(Exception e) {
		return "Something went wrong"+e;
		}
	}

}
