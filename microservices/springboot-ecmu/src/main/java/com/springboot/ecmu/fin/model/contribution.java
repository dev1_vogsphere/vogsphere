package com.springboot.ecmu.fin.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity 
@Table(schema = "contribution")
public class contribution {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 
	int Contribution_id;
	String Member_Policy_id; 
	String Contribution_date; 
	String Contribution_amount; 
	String Outstanding_amount; 
	String Method_of_contribution_id; 
	String IntUserCode; 
	String agent_name;
	public int getContribution_id() {
		return Contribution_id;
	}
	public void setContribution_id(int contribution_id) {
		Contribution_id = contribution_id;
	}
	public String getMember_Policy_id() {
		return Member_Policy_id;
	}
	public void setMember_Policy_id(String member_Policy_id) {
		Member_Policy_id = member_Policy_id;
	}
	public String getContribution_date() {
		return Contribution_date;
	}
	public void setContribution_date(String contribution_date) {
		Contribution_date = contribution_date;
	}
	public String getContribution_amount() {
		return Contribution_amount;
	}
	public void setContribution_amount(String contribution_amount) {
		Contribution_amount = contribution_amount;
	}
	public String getOutstanding_amount() {
		return Outstanding_amount;
	}
	public void setOutstanding_amount(String outstanding_amount) {
		Outstanding_amount = outstanding_amount;
	}
	public String getMethod_of_contribution_id() {
		return Method_of_contribution_id;
	}
	public void setMethod_of_contribution_id(String method_of_contribution_id) {
		Method_of_contribution_id = method_of_contribution_id;
	}
	public String getIntUserCode() {
		return IntUserCode;
	}
	public void setIntUserCode(String intUserCode) {
		IntUserCode = intUserCode;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	
	
	
	
	


}
