package com.springboot.ecmu.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SpeedpointWebhook {
	
		
	   String trans_end_time;
	   String charset;
	   String store_no;
	   String pay_scenario;
	   String sign;
	   String trans_fee_c;
	   String discount_bpc;
	   String vat_amount;
	   String order_amount;
	   String app_id;
	   String sign_type;
	   String trans_status;
	   String price_currency;
	   String terminal_sn;
	   String trans_type;
	   String timestamp;
	   String trans_no;
	   String merchant_no;
	   String method;
	   String pay_user_account_id;
	   String format;
	   String pay_method_id;
	   String trans_amount;
	   String http_request_id;
	   String version;
	   String pay_channel_trans_no;
	   String paid_amount;
	   String discount_bmopc;
		String receivable_amount;
		String settlement_trans_amount;
		String cashback_amount;
		
		public String getTrans_end_time() {
			return trans_end_time;
		}
		public void setTrans_end_time(String trans_end_time) {
			this.trans_end_time = trans_end_time;
		}
		public String getCharset() {
			return charset;
		}
		public void setCharset(String charset) {
			this.charset = charset;
		}
		public String getStore_no() {
			return store_no;
		}
		public void setStore_no(String store_no) {
			this.store_no = store_no;
		}
		public String getPay_scenario() {
			return pay_scenario;
		}
		public void setPay_scenario(String pay_scenario) {
			this.pay_scenario = pay_scenario;
		}
		public String getSign() {
			return sign;
		}
		public void setSign(String sign) {
			this.sign = sign;
		}
		public String getTrans_fee_c() {
			return trans_fee_c;
		}
		public void setTrans_fee_c(String trans_fee_c) {
			this.trans_fee_c = trans_fee_c;
		}
		public String getDiscount_bpc() {
			return discount_bpc;
		}
		public void setDiscount_bpc(String discount_bpc) {
			this.discount_bpc = discount_bpc;
		}
		public String getVat_amount() {
			return vat_amount;
		}
		public void setVat_amount(String vat_amount) {
			this.vat_amount = vat_amount;
		}
		public String getOrder_amount() {
			return order_amount;
		}
		public void setOrder_amount(String order_amount) {
			this.order_amount = order_amount;
		}
		public String getApp_id() {
			return app_id;
		}
		public void setApp_id(String app_id) {
			this.app_id = app_id;
		}
		public String getSign_type() {
			return sign_type;
		}
		public void setSign_type(String sign_type) {
			this.sign_type = sign_type;
		}
		public String getTrans_status() {
			return trans_status;
		}
		public void setTrans_status(String trans_status) {
			this.trans_status = trans_status;
		}
		public String getPrice_currency() {
			return price_currency;
		}
		public void setPrice_currency(String price_currency) {
			this.price_currency = price_currency;
		}
		public String getTerminal_sn() {
			return terminal_sn;
		}
		public void setTerminal_sn(String terminal_sn) {
			this.terminal_sn = terminal_sn;
		}
		public String getTrans_type() {
			return trans_type;
		}
		public void setTrans_type(String trans_type) {
			this.trans_type = trans_type;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		public String getTrans_no() {
			return trans_no;
		}
		public void setTrans_no(String trans_no) {
			this.trans_no = trans_no;
		}
		public String getMerchant_no() {
			return merchant_no;
		}
		public void setMerchant_no(String merchant_no) {
			this.merchant_no = merchant_no;
		}
		public String getMethod() {
			return method;
		}
		public void setMethod(String method) {
			this.method = method;
		}
		public String getPay_user_account_id() {
			return pay_user_account_id;
		}
		public void setPay_user_account_id(String pay_user_account_id) {
			this.pay_user_account_id = pay_user_account_id;
		}
		public String getFormat() {
			return format;
		}
		public void setFormat(String format) {
			this.format = format;
		}
		public String getPay_method_id() {
			return pay_method_id;
		}
		public void setPay_method_id(String pay_method_id) {
			this.pay_method_id = pay_method_id;
		}
		public String getTrans_amount() {
			return trans_amount;
		}
		public void setTrans_amount(String trans_amount) {
			this.trans_amount = trans_amount;
		}
		public String getHttp_request_id() {
			return http_request_id;
		}
		public void setHttp_request_id(String http_request_id) {
			this.http_request_id = http_request_id;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getPay_channel_trans_no() {
			return pay_channel_trans_no;
		}
		public void setPay_channel_trans_no(String pay_channel_trans_no) {
			this.pay_channel_trans_no = pay_channel_trans_no;
		}
		public String getPaid_amount() {
			return paid_amount;
		}
		public void setPaid_amount(String paid_amount) {
			this.paid_amount = paid_amount;
		}
		public String getDiscount_bmopc() {
			return discount_bmopc;
		}
		public void setDiscount_bmopc(String discount_bmopc) {
			this.discount_bmopc = discount_bmopc;
		}
		public String getReceivable_amount() {
			return receivable_amount;
		}
		public void setReceivable_amount(String receivable_amount) {
			this.receivable_amount = receivable_amount;
		}
		public String getSettlement_trans_amount() {
			return settlement_trans_amount;
		}
		public void setSettlement_trans_amount(String settlement_trans_amount) {
			this.settlement_trans_amount = settlement_trans_amount;
		}
		public String getCashback_amount() {
			return cashback_amount;
		}
		public void setCashback_amount(String cashback_amount) {
			this.cashback_amount = cashback_amount;
		}

		
		
		
		
		
}
