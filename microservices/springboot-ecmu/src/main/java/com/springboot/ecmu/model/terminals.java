package com.springboot.ecmu.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity 
@Table(schema = "terminals")
public class terminals {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	int idterminals; 
	String internalusercode;
	String terminaltype;
	String terminalsn;
	String description;
	String store;
	String usercode;
	public int getIdterminals() {
		return idterminals;
	}
	public void setIdterminals(int idterminals) {
		this.idterminals = idterminals;
	}
	public String getInternalusercode() {
		return internalusercode;
	}
	public void setInternalusercode(String internalusercode) {
		this.internalusercode = internalusercode;
	}
	public String getTerminaltype() {
		return terminaltype;
	}
	public void setTerminaltype(String terminaltype) {
		this.terminaltype = terminaltype;
	}
	public String getTerminalsn() {
		return terminalsn;
	}
	public void setTerminalsn(String terminalsn) {
		this.terminalsn = terminalsn;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
	
	
	
	
}

