package com.springboot.ecmu.service;

public interface UpdateDebiCheckService {
	String UpdateDebiCheck(
			String reference,
			String collection_amount,
			String Changeby,
			String first_collection_amount,
			String tracking_indicator,
			String mandate_initiation_date,
			String first_collection_date,
			String maximum_collection_amount,
			String debtor_account_name,
			String debtor_account_number,
			String debtor_account_type,
			String debtor_branch_number,
			String debtor_identification,
			String debtor_contact_number,
			String debtor_email,
			String amendment_reason_text,
			String collection_date,
			String date_adjustment_rule_indicator,
			String adjustment_category,
			String adjustment_rate,
			String adjustment_amount,
			String debit_value_type,
			String amended);
	
	

}
