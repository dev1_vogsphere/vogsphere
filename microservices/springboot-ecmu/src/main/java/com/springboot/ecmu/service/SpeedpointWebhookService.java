package com.springboot.ecmu.service;

public interface SpeedpointWebhookService {
String notifyPayment(String trans_end_time, String charset, String store_no, String pay_scenario, String sign, String trans_fee_c, String discount_bpc, String vat_amount, String order_amount, String app_id, String sign_type, String trans_status, String price_currency, String terminal_sn, String trans_type, String timestamp, String pay_channel_trans_no, String merchant_no, String pay_method_id, String pay_user_account_id, String format, String pay_method_id2, String trans_amount, String http_request_id, String version, String pay_channel_trans_no2, String paid_amount, String discount_bmopc,String cashbackamount,String receivableamount,String settlementtransamount);

}
