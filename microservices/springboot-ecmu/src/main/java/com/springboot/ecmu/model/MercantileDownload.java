package com.springboot.ecmu.model;

public class MercantileDownload {
	private String UserName ;
	private String UserInst ;
	private String PassWord ;
	private String FootPrint;
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserInst() {
		return UserInst;
	}
	public void setUserInst(String userInst) {
		UserInst = userInst;
	}
	public String getPassWord() {
		return PassWord;
	}
	public void setPassWord(String passWord) {
		PassWord = passWord;
	}
	public String getFootPrint() {
		return FootPrint;
	}
	public void setFootPrint(String footPrint) {
		FootPrint = footPrint;
	}
	
	
}
