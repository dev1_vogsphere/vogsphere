package com.springboot.ecmu.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity 
@Table(schema = "notifypaymentmerchant")
public class notifypaymentmerchant {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 
	String idnotifypaymentmerchant;
	String method;
	String merchantno;
	String transno;
	String transtype;
	String merchantorderno;
	String pricecurrency;
	String orderamount;
	String transamount;
	String transfeec;
	String attach;
	String storeno;
	String terminalsn;
	String payscenario;
	String transstatus;
	String paychanneltransno;
	String payuseraccountid;
	String paidamount;
	String discountbmopc;
	String discountbpc;
	String transendtime;
	String paymethodid;
	String vatamount;
	String transamountcny;
	String exchangerate;
	String refno;
	String httprequestid;
	String version;
	String internalusercode;
	String cashbackamount;
	String receivableamount;
	String settlementtransamount;
	String usercode;
	
	public String getIdnotifypaymentmerchant() {
		return idnotifypaymentmerchant;
	}
	public void setIdnotifypaymentmerchant(String idnotifypaymentmerchant) {
		this.idnotifypaymentmerchant = idnotifypaymentmerchant;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getMerchantno() {
		return merchantno;
	}
	public void setMerchantno(String merchantno) {
		this.merchantno = merchantno;
	}
	public String getTransno() {
		return transno;
	}
	public void setTransno(String transno) {
		this.transno = transno;
	}
	public String getTranstype() {
		return transtype;
	}
	public void setTranstype(String transtype) {
		this.transtype = transtype;
	}
	public String getMerchantorderno() {
		return merchantorderno;
	}
	public void setMerchantorderno(String merchantorderno) {
		this.merchantorderno = merchantorderno;
	}
	public String getPricecurrency() {
		return pricecurrency;
	}
	public void setPricecurrency(String pricecurrency) {
		this.pricecurrency = pricecurrency;
	}
	public String getOrderamount() {
		return orderamount;
	}
	public void setOrderamount(String orderamount) {
		this.orderamount = orderamount;
	}
	public String getTransamount() {
		return transamount;
	}
	public void setTransamount(String transamount) {
		this.transamount = transamount;
	}
	public String getTransfeec() {
		return transfeec;
	}
	public void setTransfeec(String transfeec) {
		this.transfeec = transfeec;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getStoreno() {
		return storeno;
	}
	public void setStoreno(String storeno) {
		this.storeno = storeno;
	}
	public String getTerminalsn() {
		return terminalsn;
	}
	public void setTerminalsn(String terminalsn) {
		this.terminalsn = terminalsn;
	}
	public String getPayscenario() {
		return payscenario;
	}
	public void setPayscenario(String payscenario) {
		this.payscenario = payscenario;
	}
	public String getTransstatus() {
		return transstatus;
	}
	public void setTransstatus(String transstatus) {
		this.transstatus = transstatus;
	}
	public String getPaychanneltransno() {
		return paychanneltransno;
	}
	public void setPaychanneltransno(String paychanneltransno) {
		this.paychanneltransno = paychanneltransno;
	}
	public String getPayuseraccountid() {
		return payuseraccountid;
	}
	public void setPayuseraccountid(String payuseraccountid) {
		this.payuseraccountid = payuseraccountid;
	}
	public String getPaidamount() {
		return paidamount;
	}
	public void setPaidamount(String paidamount) {
		this.paidamount = paidamount;
	}
	public String getDiscountbmopc() {
		return discountbmopc;
	}
	public void setDiscountbmopc(String discountbmopc) {
		this.discountbmopc = discountbmopc;
	}
	public String getDiscountbpc() {
		return discountbpc;
	}
	public void setDiscountbpc(String discountbpc) {
		this.discountbpc = discountbpc;
	}
	public String getTransendtime() {
		return transendtime;
	}
	public void setTransendtime(String transendtime) {
		this.transendtime = transendtime;
	}
	public String getPaymethodid() {
		return paymethodid;
	}
	public void setPaymethodid(String paymethodid) {
		this.paymethodid = paymethodid;
	}
	public String getVatamount() {
		return vatamount;
	}
	public void setVatamount(String vatamount) {
		this.vatamount = vatamount;
	}
	public String getTransamountcny() {
		return transamountcny;
	}
	public void setTransamountcny(String transamountcny) {
		this.transamountcny = transamountcny;
	}
	public String getExchangerate() {
		return exchangerate;
	}
	public void setExchangerate(String exchangerate) {
		this.exchangerate = exchangerate;
	}
	public String getRefno() {
		return refno;
	}
	public void setRefno(String refno) {
		this.refno = refno;
	}
	public String getHttprequestid() {
		return httprequestid;
	}
	public void setHttprequestid(String httprequestid) {
		this.httprequestid = httprequestid;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getInternalusercode() {
		return internalusercode;
	}
	public void setInternalusercode(String internalusercode) {
		this.internalusercode = internalusercode;
	}
	public String getCashbackamount() {
		return cashbackamount;
	}
	public void setCashbackamount(String cashbackamount) {
		this.cashbackamount = cashbackamount;
	}
	public String getReceivableamount() {
		return receivableamount;
	}
	public void setReceivableamount(String receivableamount) {
		this.receivableamount = receivableamount;
	}
	public String getSettlementtransamount() {
		return settlementtransamount;
	}
	public void setSettlementtransamount(String settlementtransamount) {
		this.settlementtransamount = settlementtransamount;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}


	
}
