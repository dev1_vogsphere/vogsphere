package com.springboot.ecmu.model;

public class MercantileUpload {
	byte [] inMessage = {};
	private String userName;
	private String userInst;
	private String passWord;
	private String footPrint;
	private String gennumber;
	public byte[] getInMessage() {
		return inMessage;
	}
	public void setInMessage(byte[] inMessage) {
		this.inMessage = inMessage;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserInst() {
		return userInst;
	}
	public void setUserInst(String userInst) {
		this.userInst = userInst;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getFootPrint() {
		return footPrint;
	}
	public void setFootPrint(String footPrint) {
		this.footPrint = footPrint;
	}
	public String getGennumber() {
		return gennumber;
	}
	public void setGennumber(String gennumber) {
		this.gennumber = gennumber;
	}
	
	
}
