package com.springboot.ecmu.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.springboot.ecmu.model.notifypaymentmerchant;

public interface MerchantServicesRespository extends JpaRepository<notifypaymentmerchant, String>{
	//Mandates save(Mandates mandates);
	notifypaymentmerchant findBytransno(String trans_no);
	boolean existsBytransno(String trans_no);

	
	
}