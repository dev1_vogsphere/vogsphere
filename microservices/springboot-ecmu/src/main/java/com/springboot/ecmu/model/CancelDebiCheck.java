package com.springboot.ecmu.model;

public class CancelDebiCheck {
	public String reference;
	public String changedby;
	public String cancellation_reason;
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getChangedby() {
		return changedby;
	}
	public void setChangedby(String changedby) {
		this.changedby = changedby;
	}
	public String getCancellation_reason() {
		return cancellation_reason;
	}
	public void setCancellation_reason(String cancellation_reason) {
		this.cancellation_reason = cancellation_reason;
	}
	
}
