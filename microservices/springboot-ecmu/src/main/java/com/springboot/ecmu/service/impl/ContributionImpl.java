package com.springboot.ecmu.service.impl;


import org.springframework.stereotype.Service;

import com.springboot.ecmu.fin.repository.ContributionRepository;
import com.springboot.ecmu.model.notifypaymentmerchant;
import com.springboot.ecmu.service.ContributionService;
import com.springboot.ecmu.fin.model.*;

@Service
public class ContributionImpl implements ContributionService {
	private ContributionRepository contributionRepository;
		private ContributionImpl(ContributionRepository contributionRepository) {
		super();
		this.contributionRepository = contributionRepository;
	}
	@Override
	public String notifycontribution(String clientreference, String branch, String agent, String paymentdate,
			String amount) {
		
		System.out.println("Hello");
		contribution payment = new contribution();
		//TODO Auto-generated method stub	
		String myJSON ="{\"response\":\"Success\",\"status\":\"OK\"}";  
		
		try 
		{
			payment.setMember_Policy_id(clientreference);
			System.out.println(clientreference);
			payment.setContribution_date(paymentdate);
			payment.setContribution_amount(amount);
			payment.setAgent_name(agent);
			payment.setIntUserCode(branch);
			contributionRepository.save(payment);
			System.out.println("Transaction Saved");
			return myJSON;
		}
		catch(Exception e) 
		{
			String error="Something went wrong"+e.getMessage()+e.getCause();
			System.out.println(error);
		    return "Something went wrong"+e.getMessage()+e.getCause();
		}
		
	}

}
