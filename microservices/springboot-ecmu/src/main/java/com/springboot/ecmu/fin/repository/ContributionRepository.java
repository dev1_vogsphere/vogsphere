package com.springboot.ecmu.fin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.ecmu.fin.model.contribution;

public interface ContributionRepository extends JpaRepository<contribution, String>{
	//Contribution findByMember_Policy_id(String Member_Policy_id);
}
