package com.springboot.ecmu.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.ecmu.model.Client;

public interface CollectionsDownloadReponseRespository extends JpaRepository<Client, String> {
 Client findByusercode(String bank);
}

