package com.springboot.ecmu.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity 
@Table(schema = "client")

public class Client {
 @Id
 @GeneratedValue(strategy = GenerationType.IDENTITY)
public int ID;
public String bank ;
public String Description;
public String usercode;
public String Intusercode;
public String paymentusercode;
public String debicheckusercode;
public String statementsusercode;
public String username;
public String password;
public String stpassword;
public String statements;
public String aggregate_limits_debits;
public String aggregate_limits_naedo;
public String aggregate_limits_cards;
public String aggregate_limits_credits;
public String item_limits_debits;
public String item_limits_naedo;
public String item_limits_cards;
public String item_limits_credits;
public String changedby;
public String changedon;
public String changedat;
public String lockedforchanges;
public String urlqadebicheck;
public String urlproddebicheck;
public String urlqamercantile;
public String urlprodmercantile;
public String abbreviatedshortname;
public String security;
public String shortname;
public String Internal_account;

public String getDescription() {
	return Description;
}

public void setDescription(String description) {
	Description = description;
}

public String getUsercode() {
	return usercode;
}

public void setUsercode(String usercode) {
	this.usercode = usercode;
}

public String getIntusercode() {
	return Intusercode;
}

public void setIntusercode(String intusercode) {
	Intusercode = intusercode;
}

public String getPaymentusercode() {
	return paymentusercode;
}

public void setPaymentusercode(String paymentusercode) {
	this.paymentusercode = paymentusercode;
}

public String getDebicheckusercode() {
	return debicheckusercode;
}

public void setDebicheckusercode(String debicheckusercode) {
	this.debicheckusercode = debicheckusercode;
}

public String getStatementsusercode() {
	return statementsusercode;
}

public void setStatementsusercode(String statementsusercode) {
	this.statementsusercode = statementsusercode;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getStpassword() {
	return stpassword;
}

public void setStpassword(String stpassword) {
	this.stpassword = stpassword;
}

public String getStatements() {
	return statements;
}

public void setStatements(String statements) {
	this.statements = statements;
}

public String getAggregate_limits_debits() {
	return aggregate_limits_debits;
}

public void setAggregate_limits_debits(String aggregate_limits_debits) {
	this.aggregate_limits_debits = aggregate_limits_debits;
}

public String getAggregate_limits_naedo() {
	return aggregate_limits_naedo;
}

public void setAggregate_limits_naedo(String aggregate_limits_naedo) {
	this.aggregate_limits_naedo = aggregate_limits_naedo;
}

public String getAggregate_limits_cards() {
	return aggregate_limits_cards;
}

public void setAggregate_limits_cards(String aggregate_limits_cards) {
	this.aggregate_limits_cards = aggregate_limits_cards;
}

public String getAggregate_limits_credits() {
	return aggregate_limits_credits;
}

public void setAggregate_limits_credits(String aggregate_limits_credits) {
	this.aggregate_limits_credits = aggregate_limits_credits;
}

public String getItem_limits_debits() {
	return item_limits_debits;
}

public void setItem_limits_debits(String item_limits_debits) {
	this.item_limits_debits = item_limits_debits;
}

public String getItem_limits_naedo() {
	return item_limits_naedo;
}

public void setItem_limits_naedo(String item_limits_naedo) {
	this.item_limits_naedo = item_limits_naedo;
}

public String getItem_limits_cards() {
	return item_limits_cards;
}

public void setItem_limits_cards(String item_limits_cards) {
	this.item_limits_cards = item_limits_cards;
}

public String getItem_limits_credits() {
	return item_limits_credits;
}

public void setItem_limits_credits(String item_limits_credits) {
	this.item_limits_credits = item_limits_credits;
}

public String getChangedby() {
	return changedby;
}

public void setChangedby(String changedby) {
	this.changedby = changedby;
}

public String getChangedon() {
	return changedon;
}

public void setChangedon(String changedon) {
	this.changedon = changedon;
}

public String getChangedat() {
	return changedat;
}

public void setChangedat(String changedat) {
	this.changedat = changedat;
}

public String getLockedforchanges() {
	return lockedforchanges;
}

public void setLockedforchanges(String lockedforchanges) {
	this.lockedforchanges = lockedforchanges;
}

public String getUrlqadebicheck() {
	return urlqadebicheck;
}

public void setUrlqadebicheck(String urlqadebicheck) {
	this.urlqadebicheck = urlqadebicheck;
}

public String getUrlproddebicheck() {
	return urlproddebicheck;
}

public void setUrlproddebicheck(String urlproddebicheck) {
	this.urlproddebicheck = urlproddebicheck;
}

public String getUrlqamercantile() {
	return urlqamercantile;
}

public void setUrlqamercantile(String urlqamercantile) {
	this.urlqamercantile = urlqamercantile;
}

public String getUrlprodmercantile() {
	return urlprodmercantile;
}

public void setUrlprodmercantile(String urlprodmercantile) {
	this.urlprodmercantile = urlprodmercantile;
}

public String getAbbreviatedshortname() {
	return abbreviatedshortname;
}

public void setAbbreviatedshortname(String abbreviatedshortname) {
	this.abbreviatedshortname = abbreviatedshortname;
}

public String getSecurity() {
	return security;
}

public void setSecurity(String security) {
	this.security = security;
}

public String getShortname() {
	return shortname;
}

public void setShortname(String shortname) {
	this.shortname = shortname;
}

public String getInternal_account() {
	return Internal_account;
}

public void setInternal_account(String internal_account) {
	Internal_account = internal_account;
}

public String getBank() {
	return bank;
}

public void setBank(String bank) {
	this.bank = bank;
}


}
