package com.springboot.ecmu.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

import com.springboot.ecmu.model.mandates;
import com.springboot.ecmu.repository.MandatesRespository;
import com.springboot.ecmu.service.UpdateDebiCheckService;

@Service
public class UpdateDebiCheckImpl implements UpdateDebiCheckService  {
	private MandatesRespository mandatesRespository;  

	private UpdateDebiCheckImpl(MandatesRespository mandatesRespository) {
		super();
		this.mandatesRespository = mandatesRespository;
	}

	@Override
	public String UpdateDebiCheck(String reference, String collection_amount,
			String first_collection_amount, String tracking_indicator, String mandate_initiation_date,
			String first_collection_date, String maximum_collection_amount, String debtor_account_name,
			String debtor_account_number, String debtor_account_type, String debtor_branch_number,
			String debtor_identification, String debtor_contact_number, String debtor_email,
			String amendment_reason_text, String collection_date, String date_adjustment_rule_indicator,
			String adjustment_category, String adjustment_rate, String adjustment_amount, String debit_value_type,
			String amended, String Changeby) {
		    
			//Get Current date and time
			LocalDate dateObj = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String currentdatetime = dateObj.format(formatter);
			
			// Find Record to update and Resubmit
			mandates updatereference = mandatesRespository.findByReference(reference.replaceAll("\n"," "));
		
		    //Set Records 
			//updatereference.setCollection_amount(collection_amount);
			//updatereference.setFirst_collection_amount(first_collection_amount);
			//updatereference.setTracking_indicator(tracking_indicator);
			//updatereference.setMandate_initiation_date(mandate_initiation_date);
			//updatereference.setFirst_collection_date(first_collection_date);
			//updatereference.setMaximum_collection_amount(maximum_collection_amount);
			updatereference.setStatus_code("01");
			updatereference.setStatus_desc("Pending");
			updatereference.setDebtor_account_name(debtor_account_name);
			updatereference.setDebtor_account_number(debtor_account_number);
			updatereference.setDebtor_account_type(debtor_account_type);
			updatereference.setDebtor_branch_number(debtor_branch_number);
			updatereference.setDebtor_identification(debtor_identification);
			updatereference.setDebtor_contact_number(debtor_contact_number);
			updatereference.setDebtor_email(debtor_email);
			updatereference.setAmended(amended);
			
			if(first_collection_date.equals(null) || first_collection_date.equals("")){
				System.out.println("EMPTY");
			}else{
			    
			    updatereference.setFirst_collection_date(first_collection_date);//check
			}
			
			
			//updatereference.setCollection_date(collection_date);
			//updatereference.setDate_adjustment_rule_indicator(date_adjustment_rule_indicator);
			//updatereference.setAdjustment_category(adjustment_category);
			//updatereference.setAdjustment_rate(adjustment_rate);
			//updatereference.setAdjustment_amount(adjustment_amount);
			//updatereference.setDebit_value_type(debit_value_type);
			updatereference.setAmended("Y");
			updatereference.setChangedby(Changeby);
			updatereference.setChangedon(currentdatetime);
			// Save Password to database
			try {
			mandatesRespository.save(updatereference);
			return "Mandate amend request submitted for processing";
			}catch(Exception e) {
			return "Something went wrong"+e.getMessage()+e.getCause();
			}
			
	}
	
}
