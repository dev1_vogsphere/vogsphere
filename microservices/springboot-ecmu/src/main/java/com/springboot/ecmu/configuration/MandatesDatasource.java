/*package com.springboot.ecmu.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;



import com.springboot.ecmu.model.Mandates;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.springboot.ecmu.repository.MandatesRespository",
entityManagerFactoryRef = "MandatesEntityManagerFactory")
public class MandatesDatasource {
	
	//Define Datasource properties
		@Bean
		@Primary 
		@ConfigurationProperties("spring.datasource.paycollections")
		public DataSourceProperties mandatesDataSourceProperties()
		{
			return new DataSourceProperties();
		}
		
		//Define Datasource
		@Bean
		@Primary 
		@ConfigurationProperties("spring.datasource.paycollections.configuration")
		public DataSource mandatesDataSource() {
			return mandatesDataSourceProperties()
					.initializeDataSourceBuilder()
					.type(HikariDataSource.class)
					.build();
		}
		
		//Entity Manager and Factory
		@Bean(name= "MandatesEntityManagerFactory")
		@Primary
	
		public LocalContainerEntityManagerFactoryBean  mandateEntityManageFactory(EntityManagerFactoryBuilder builder,@Qualifier("dataSource") DataSource dataSource) {
			return builder
					.dataSource(dataSource).packages(Mandates.class).build();
		}
		
		//Transaction Manager 
		@Bean(name = "MandatesTransactionManager")
	    @Primary
	    public PlatformTransactionManager mandatesTransactionManager(final @Qualifier("MandatesEntityManagerFactory") LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean) {
			return new JpaTransactionManager(localContainerEntityManagerFactoryBean.getObject());
		}	
		
}*/

