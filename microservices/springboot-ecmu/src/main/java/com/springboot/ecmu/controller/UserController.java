/*package com.springboot.ecmu.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.springboot.ecmu.model.DebiCheck;
import com.springboot.ecmu.model.User;
import com.springboot.ecmu.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
//GET HTTP Method
//http://localhost:8080//api/users
	
private UserService userService;
public UserController(UserService userService) {
	super();
	this.userService = userService;
}

// Validate User REST API
@PostMapping()
public ResponseEntity<User> validateUser(@RequestBody User user) {
	
	if (userService.existsByEmail(user.getEmail())) {
		return new ResponseEntity<User>(userService.validateUser(user),HttpStatus.BAD_GATEWAY);
	 }else {
		
	return new ResponseEntity<User>(userService.validateUser(user),HttpStatus.CREATED);
	 }
}

//Get All Users REST API
@GetMapping
public List<User> getAllUsers(){
	return userService.getAllUsers();
}

//Get User By Id REST API
//http://localhost:8080/api/user/1
@GetMapping("{id}")
public ResponseEntity<User> getUserById(@PathVariable ("id") String id){
return new ResponseEntity<User>(userService.getUserById(id),HttpStatus.OK);

}

//Update Rest Password REST API
//http://localhost:8080/api/user/1
@PutMapping("{id}")
public ResponseEntity<String> passwordRest(@PathVariable ("id") String id, @RequestBody User user) {
	//return new ResponseEntity<User>(userService.passwordRest(user, id), HttpStatus.OK);
	return new ResponseEntity<String>("Password Successfully Updated", HttpStatus.OK);
}

/*
//Delete User REST API
//http://localhost:8080/api/user/1
@DeleteMapping("{id}")
public ResponseEntity<String> deleteUser(@PathVariable ("id") String id){
	//delete User from DB
	userService.deleteUser(id);
	return new ResponseEntity<String>("User deleted Successfully", HttpStatus.OK);
	
}



}*/



