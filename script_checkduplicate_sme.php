<?php
require 'database.php';
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_sme_member'))
{
	function get_sme_member($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdoeloan = Database::connect();
		$pdoeloan->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'businessowneinfo';			
		$sql = "select * from $tbl where identificationNumber = ?";
		$q = $pdoeloan->prepare($sql);
		$q->execute(array($dataFilter['Member_id']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
$message = '';
$list    = getpost('member');
$updateList  = getpost('update');

$data_member_Obj = null;
// -- Check member details
if($updateList['update'] == 'display:none')
{$data_member_Obj = get_sme_member($list);}

if(isset($data_member_Obj['Member_id']))
{
	// -- Member warning message.
	$message =  $message.'ERROR: Member ('.$data_member_Obj['Member_id'].') already exist on the system.';
	
	/*$data_member_Obj['Action_Date'].' from bank account number '.
	$databill_single_instructionObj['Account_Number'].' with client reference '.
	$databill_single_instructionObj['Client_Reference_1'];*/			
}

 echo $message; 
?>