<?php 
require 'database.php';
$dbname = "eloan";
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$path = $_POST['path'];

backup_tables($dbname);

/* backup the db OR just a table */
function backup_tables($dbname)
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$tableName = '';
	
	// -- Get all of the tables
		$tablesDrop = array();
		$sql = "SHOW TABLES";
		$q = $pdo->prepare($sql);
		$q->execute();	
		$tablesDrop = $q->fetchAll();  
		
		// -- BOC Get all the Foreign Key Constrait - 01.10.2017.			
		$return = "SET foreign_key_checks = 0;";
		// -- EOC Drop Tables & FOREIGN KEY 01.10.2017.

	// -- Get all of the tables
		$tables = array();
		$sql = "SHOW TABLES";
		$q = $pdo->prepare($sql);
		$q->execute();	
		$tables = $q->fetchAll();
		
	//cycle through
	foreach($tables as $table)
	{		
		foreach($table AS $key => $value) { $table[$key] = stripslashes($value); } 
		$value = "`".$value."`";
		$sql = "SELECT * FROM ".$value;//"SHOW CREATE TABLE loanapp";//"SHOW TABLES";
		$q = $pdo->prepare($sql);
		$q->execute();
		$data = $q->fetchAll();  
		$dataCp = $data;
		
		if(is_array($dataCp))
		{
			//$return = $return.json_encode($dataCp);
		}
		$num_fields = $q->columnCount();
		
		$return = $return.'DROP TABLE IF EXISTS '.$value.';';
		$tableName = $value;
		
		$sql = "SHOW CREATE TABLE ".$value;
		$q = $pdo->prepare($sql);
		$q->execute();	
		$row2 = $q->fetchAll();
						
		//--$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		
		foreach($row2 as $row)
		{ 
			foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
			$return= $return."\n\n".nl2br( $value )."\n\n;";
		}
		$value = '';
		
		//for ($i = 0; $i < $num_fields; $i++) 
		//{
			foreach($data as $row)
			{
				$return.= 'INSERT INTO '.$tableName.' VALUES(';
				for($j=0; $j < $num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = preg_replace("/\n/","/\\n/",$row[$j]);//ereg_replace("\n","\\n",$row[$j]); //
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j < ($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		//}
		$return.="\n\n\n";
	}
	
// -- Replace < br/> with space.
	$return = str_replace('<br />','',$return);
	
	//save file
	global $path;
	$ts = time();
	$db_date = date('Y-m-dH:i:s', $ts);
	$filename = $path.'db-backup-'.$ts.'- eloan.sql';
	$handle = fopen($filename,'w+');

	fwrite($handle,$return);
	fclose($handle);
	
	echo $filename;
}
?>