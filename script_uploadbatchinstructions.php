<?php
////////////////////////////////////////////////////////////////////////
if(!function_exists('getCsvDelimiter'))
{
   function getCsvDelimiter($filePath)
   {
	  $checkLines = 1;
      $delimiters =[',', ';', '\t'];

      $default =',';

       $fileObject = new \SplFileObject($filePath);
       $results = [];
       $counter = 0;
       while ($fileObject->valid() && $counter <= $checkLines) {
           $line = $fileObject->fgets();
           foreach ($delimiters as $delimiter) {
               $fields = explode($delimiter, $line);
               $totalFields = count($fields);
               if ($totalFields > 1) {
                   if (!empty($results[$delimiter])) {
                       $results[$delimiter] += $totalFields;
                   } else {
                       $results[$delimiter] = $totalFields;
                   }
               }
           }
           $counter++;
       }
       if (!empty($results)) {
           $results = array_keys($results, max($results));

           return $results[0];
       }
return $default;
}
}
//If you need a function which converts a string array into a utf8 encoded string array then this function might be useful for you:
if(!function_exists('utf8_string_array_encode'))
{

function utf8_string_array_encode($array){
    $func = function(&$value,&$key){
        if(is_string($value)){
            $value = utf8_encode($value);
        }
        if(is_string($key)){
            $key = utf8_encode($key);
        }
        if(is_array($value)){
            utf8_string_array_encode($value);
        }
    };
    array_walk($array,$func);
    return $array;
}
}
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/script_rule_two_day_service.php'; // -- 18.09.2021 Rule #1 Two Day Service.

if(!function_exists('detectDelimiter'))
{
/* @param string $csvFile Path to the CSV file
* @return string Delimiter
*/
function detectDelimiter($csvFile)
{
    $delimiters = array(
        ';' => 0,
        ',' => 0,
        "\t" => 0,
        "|" => 0
    );

    $handle = fopen($csvFile, "r");
    $firstLine = fgets($handle);
    fclose($handle);
    foreach ($delimiters as $delimiter => &$count) {
        $count = count(str_getcsv($firstLine, $delimiter));
    }

    return array_search(max($delimiters), $delimiters);
}
}
// -- Move Uploaded File -- //
if(!function_exists('moveuploadedfile'))
{
	function moveuploadedfile($name,$tmp_name )
	{
			$today = date('Y-m-d H-i-s');
			$userid = getpost('userid');
			$path = "Files/Collections/Instructions/".$userid."_".$today."_"; // Upload directory
			$count = 0;
		 // No error found! Move uploaded files
		 // Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			$ext = pathinfo($name, PATHINFO_EXTENSION);
			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;

		 // -- Update File Name Directory
	            if(move_uploaded_file($tmp_name , $path.$name))
				{
	            	$count++; // Number of successfully uploaded files
	            }
			$newfilename = $path.$name;
			return 	$tmp_name ;
	}
}
if(!function_exists('uploadcsvfile'))
{
	function uploadcsvfile($branch)
	{
		$output = null;
		$column = null;
		$row_data = null;
		$emailContents = null;
		$contents = null;
		$message = '';
		$tmp_name    = '';
		$separator = '';
		try{
		$tmp_name = $_FILES['csv_file']['tmp_name'];
		$cachedData = getmasterdata();

		if(!empty($tmp_name))
		{

		// -- Backuploaded file for history & tracking
		$name = $_FILES['csv_file']['name'];

		 $separator = getCsvDelimiter($tmp_name);
		 $file_data = fopen($tmp_name, 'r');
		 $column = fgetcsv($file_data,0,$separator);
		 array_unshift($column,'Feedback');//'Feedback');
 		 $colCount    = count($column) - 1;
		 $colCountAft = count($column);
		 array_unshift($column,'Count');
		 $rowCount = 1;
		 while($row = fgetcsv($file_data,0,$separator))
		 {
			// -- Scan the File Convert all record to UTF-8
			for($x=0;$x<$colCount;$x++ )
			{
				 $row[$x] = scanfile_to_UTF8($row[$x]);
			}
			$message = extractcsvfileIinstructions($row,$cachedData,$branch);
			$message = $message;
			array_unshift($row,$message);
			array_unshift($row,$rowCount);
			$rowCount = $rowCount + 1;
		    $row_data[] = $row;
		 }
		 $output = array(
		  'column'  => $column,
		  'row_data'  => $row_data
		 );

		 // -- BOC 29.05.2021
		 // -- Sent a list of uploaded transactions to the client & support team.
		 // -- same concept used in the script_UploadEFTBankUnpaidMecantile.php
		 $contents = report_uploadedbatchInstructions($output);
		 $emailContents = send_uploader_notification($contents);
		 // -- EOC 29.05.2021
		 $filenameEncryp = moveuploadedfile($name,$tmp_name);
		}
	      $output = json_encode($output);
		}
		catch (Exception $e)
			{
				$message = $e->getMessage();
			}

	 return $output;
	}
}
if(!function_exists('extractcsvfileIinstructions'))
{
	function extractcsvfileIinstructions($keydata,$cachedData,$IntUserCode)
	{
			$keydataJson = array();
			$message 	 = '';
			$ruleTwoDayMsg    = ''; // -- 18.09.2021
			$UserCode	 = getpost('UserCode'); // From FormData
//			$IntUserCode = getpost('IntUserCode');
			$userid 	 = getpost('userid');
			$role 		 = getpost('role');
			$Action_Date = '';
			$Reference_1 = '';
			$notificationsList = null;
			$anySuccess = '';

			$cachedBank 	   = $cachedData[0];
			$cachedAccountType = $cachedData[1];
			$cachedClass 	   = $cachedData[2];

			try{
							//$keydata 	 = $csvdata[$x];
							$csvdata = $keydata;
							//$keydata = utf8_string_array_encode($keydata);
							//print($keydata['Reference 1']);
							//Debug Encoding - echo json_encode($keydata)."\n";
							// -- UTF-8
							$keydata = json_encode($keydata,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
							if(empty($keydata))
							{
								// -- ANSI
								$keydata = $csvdata;
							}
							else
							{
								$keydataJson = json_decode($keydata,true);
							}

							$arrayInstruction[1] = $UserCode;//$keydata['Client_Id'];
							$TotalColmn = 17;
							$index = 0;
							// -- Is to avoid unicharacters e.g. \ff
							if(!empty($keydataJson))
							{
							$keydata = array();
							foreach($keydataJson as $item => $v)
							{
							//print_r($v);
							//print('<br/>');
							// -- Trim trailing & leading spaces.
								$v = trim($v);
								if($index == 0){$keydata['Reference 1'] = $v;}
								if($index == 1){$keydata['Reference 2'] = $v;}
								if($index == 2){$keydata['ID Type'] = $v;}
								if($index == 3){$keydata['ID Number'] = $v;}

								if($index == 4){$keydata['Contact Number'] = $v;}
								if($index == 5){$keydata['Initials'] = $v;}
								if($index == 6){$keydata['Account Holder'] = $v;}
								if($index == 7){$keydata['Account Type'] = $v;}

								if($index == 8){$keydata['Account Number'] = $v;}
								if($index == 9){$keydata['Bank Name'] = $v;}
								if($index == 10){$keydata['Number of Payments'] = $v;}
								if($index == 11){$keydata['Service Mode'] = $v;}
								if($index == 12){$keydata['Service Type'] = $v;}

								if($index == 13){$keydata['Frequency'] = $v;}
								if($index == 14){$keydata['Action Date'] = str_replace('/','-',$v); }
								if($index == 15){$keydata['Bank Reference'] = $v;}
								if($index == 16){$keydata['Amount'] = $v;}
								if($index == 17){$keydata['Notify'] = $v;}
								if($index == 18){$keydata['Entry Class'] = $v;}
								$index = $index  + 1;
							}
							}
							// -- EFT
							// -- NAEDO

							// -- UTF-8
							//print_r($keydata);

							echo $keydata['Reference 1'];
							if(isset($keydata['Reference 1'])){$arrayInstruction[2] = $keydata['Reference 1'];}else{$arrayInstruction[2] = '';}
							// -- UTF-8 DOM
							//echo $arrayInstruction[2];
							if(empty($arrayInstruction[2])){if(isset($keydata['\ufeffReference 1'])){$arrayInstruction[2] = $keydata['\ufeffReference 1'];}else{$arrayInstruction[2] = '';}}
							if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';}
							//if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';}
							if(isset($keydata['ID Type'])){$arrayInstruction[4] = $keydata['ID Type'];}else{$arrayInstruction[4] = '';}
							if(isset($keydata['ID Number'])){$arrayInstruction[5] = $keydata['ID Number'];}else{$arrayInstruction[5] = '';}
							if(isset($keydata['Initials'])){$arrayInstruction[6] = $keydata['Initials'];}else{$arrayInstruction[6] = '';}
							if(isset($keydata['Account Holder'])){$arrayInstruction[7] = $keydata['Account Holder'];}else{$arrayInstruction[7] = '';}
							if(isset($keydata['Account Type'])){$arrayInstruction[8] = GetaccounttypebynameCache($keydata['Account Type'],$cachedAccountType);}else{$arrayInstruction[8] = '';}
							if(isset($keydata['Account Number'])){$arrayInstruction[9] = $keydata['Account Number'];}else{$arrayInstruction[9] = '';}
// -- User bank name on the spreadsheet to get the branchcode.
							if(isset($keydata['Bank Name'])){$arrayInstruction[10] = GetbrachcodefromnameCache($keydata['Bank Name'],$cachedBank);}else{$arrayInstruction[10] = '';}//$keydata['Bank Name'];
							if(isset($keydata['Number of Payments'])){$arrayInstruction[11] = $keydata['Number of Payments'];}else{$arrayInstruction[11] = '';}
							if(isset($keydata['Service Type'])){$arrayInstruction[12] = $keydata['Service Type'];}else{$arrayInstruction[12] = '';}
							if(isset($keydata['Service Mode'])){$arrayInstruction[13] = $keydata['Service Mode'];}else{$arrayInstruction[13] = '';}
							if(isset($keydata['Frequency'])){$arrayInstruction[14] = $keydata['Frequency'];}else{$arrayInstruction[14] = '';}
							if(isset($keydata['Action Date'])){$arrayInstruction[15] = $keydata['Action Date'];}else{$arrayInstruction[15] = '';}
// -- Debit Order Date 19.09.2021
							$arrayInstruction[30] = $arrayInstruction[15];
							if(isset($keydata['Bank Reference'])){$arrayInstruction[16] = $keydata['Bank Reference'];}else{$arrayInstruction[16] = '';}
							if(isset($keydata['Amount'])){$arrayInstruction[17] = $keydata['Amount'];}else{$arrayInstruction[17] = '0.00';}
							$arrayInstruction[17] = str_replace(",",".",$arrayInstruction[17]);
							$arrayInstruction[18] = '1';//$keydata['Status_Code'];
							$arrayInstruction[19] = 'pending';//$keydata['Status_Description'];
							$arrayInstruction[20] = '';//$keydata['mandateid'];
							$arrayInstruction[21] = '';//$keydata['mandateitem'];
							$arrayInstruction[22] = $IntUserCode;
							$arrayInstruction[23] = $UserCode;
							if(isset($keydata['Entry Class'])){$arrayInstruction[24] = GetentryclassidCache($keydata['Entry Class'],$cachedClass);}else{$arrayInstruction[24] = 0;}
							if(isset($keydata['Contact Number'])){$arrayInstruction[25] = $keydata['Contact Number'];}else{$arrayInstruction[25] = '';}
							if(isset($keydata['Notify'])){$arrayInstruction[26] = $keydata['Notify'];}else{$arrayInstruction[26] = '';}
							$arrayInstruction[27] = $userid; // -- created by

			// -- Rule #1 - 18.09.2021 = Validate Rule Two Day Services..
              $inside ='blank';
				if($arrayInstruction[12] == 'TWO DAY')
				{
                //$inside ='inside';- Enable for debugging
								// -- Action Date Validation.
								$result = rule_two_day_service($arrayInstruction[15]);

								$results = explode("|", $result);

								if(isset($results[1]))// -- There is a proposed Action Date
								{
									// -- 10/01/2021 avoid confusing message of holiday & weekend for cutoff time.
									if($results[0] == 'Proposed ')
									{$ruleTwoDayMsg = '.<b>Action Date :</b>'.$arrayInstruction[15].' is a weekend or holiday. system auto generated <b>Action Date:</b>'.$results[1];}
									else
									{
										$ruleTwoDayMsg = $results[0];
									}
									$arrayInstruction[15] = $results[1];
									// -- Update the layout
									//$keydata['Action Date'] = $results[1];//str_replace('/','-',$v);
								}
                else
                  {
                      $ruleTwoDayMsg = '.<b> Action Date :</b>'.$arrayInstruction[15].' bumped to next processing date <b> Action Date:</b>'.$results[0];
                      $arrayInstruction[15] = $results[0];
                  }
               }
			   
				// -- Rule #2 - 06.11.2021 = Validate Rule One Day Service..
				if($arrayInstruction[12] == 'SAMEDAY')
				{  // -- stop time.
				   $stoptime = date("h:i",strtotime("12:00"));

				   if(Weekendday($arrayInstruction[15])=="saturday")
				   {
					   $stoptime = date("h:i",strtotime("09:00"));
				   }
					// -- SysTime(st)
					$st = date("H:i");
					// -- Rule #2 : If system_time > 12:00 them suggest the next day then
					if($st > $stoptime)
					{

						$result = rule_sameday_service($arrayInstruction[15]);
						$results = explode("|", $result);
						if(isset($results[1]))// -- There is a proposed Action Date
						{
							$ruleTwoDayMsg = '.<b>The time ('.$st.') is after SAMEDAY stop time ('.$stoptime.'), therefore Action Date :</b>'.$arrayInstruction[15].' is system auto generated to next available <b>Action Date:</b>'.$results[1];
							$arrayInstruction[15] = $results[1];
				   }}
				}
							$message = auto_create_billinstruction($arrayInstruction);
							//$message = $arrayInstruction[8];
							//$message = $st .'>'. $stoptime;
							//$message = $inside.$results[0].$results[1].$arrayInstruction[12].$arrayInstruction[15];//- Enable for debugging
            	//$name = $keydata['Entry Class'];
							//$name = 'NAEDO 5 Day Tracking';
							//$message =  $name.' - '.GetentryclassidCache($name ,$cachedClass);//$keydata['Entry Class'];//implode(" ",$cachedClass[0]);//$arrayInstruction[24];
							// $message = $arrayInstruction[15].$ruleTwoDayMsg;
							if($message == 'success' )
							{
								$anySuccess  = $message ;
								$message = $message.$ruleTwoDayMsg;
							    // -- Build Queue Message to notify client about their debit orders.
								if($arrayInstruction[26] == 'Yes')
								{
									$smstemplatename = 'debitorder_notification';
									$applicationid = 0;
									$Action_Date = $arrayInstruction[15];
									$Reference_1 = $arrayInstruction[2];
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{
										$notification['phone'] 		 = $arrayInstruction[25];
										$notification['content'] 	 = $contents;
										$notification['reference_1'] = $arrayInstruction[2];
										$notification['usercode'] 	 = $UserCode;
										$notification['createdby'] 	 = $userid;
										$notification['IntUserCode'] = $arrayInstruction[22];
										$notification['amount'] 	 = $arrayInstruction[17];
										$notification['Account_Holder'] = $arrayInstruction[7];
										$notificationsList[] = $notification;	// add into list
									}
								}
							}
							// -- one sms/email with notification..
							if($anySuccess == 'success')
							{
								/*$smstemplatename = 'batch_debitorder_uploaded';
								$applicationid = 0;
								$Action_Date = $arrayInstruction[15];
								$Reference_1 = $arrayInstruction[2];
								// -- Sent to the uploader.
								auto_email_collection($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								// -- Sent to info@ecashmeup.com
								auto_email_collection('8707135963082',$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								*/
								// -- notify clients who signed for notifications(queue)
								if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
							}
			}
			catch (Exception $e)
			{
				$message = $e->getMessage();
				if($message == "Could not execute: /var/qmail/bin/sendmail\n")
				{
					$message = 'ERROR:Could not sent an email.';
				}
			}
							return $message;
	}
}
if(!function_exists('report_uploadedbatchInstructions'))
{
function report_uploadedbatchInstructions($data)
{

    $totalColumn = 20;
	$totalRows   = 1;
	$htmlEmail   = array();
	$html 		 = '';
/* -- Building Columns per Row..
	if(isset($data['column']))
    {
	 $totalColumn = count($data['column']);
	 $htmlEmail[] = '<thead><tr>';
	 for($i = 0;$i < $totalColumn;$i++)
	 {
		//$htmlEmail[] = $html."<th><b>".$data['column'][$i]."</b></th>";
	 }
	 $htmlEmail[] = '</thead></tr>';
    }
*/
// -- Building Rows..
	if(isset($data['row_data']))
    {
	 $totalRows = count($data['row_data']);
	 $htmlEmail[] = '<tbody>';
	 for($row = 0;$row < $totalRows;$row++)
     {
		$htmlEmail[] = '<tr>';
		 for($col = 0;$col < $totalColumn;$col++)
			 {
			   $htmlEmail[] = '<td>'.$data['row_data'][$row][$col].'</td>';
			 }
		$htmlEmail[] = '</tr>';
	}
		$htmlEmail[] = '</tbody>';
	}

	$html = implode(" ",$htmlEmail);
	return $html;
}}
if(!function_exists('send_uploader_notification'))
{
// -- Sent to the uploader.
  function send_uploader_notification($contents)
  {
	$userid    = getpost('userid');
	$Title     = getpost('Title');
	$FirstName = getpost('FirstName');
	$LastName  = getpost('LastName');
	$fullname  = $FirstName.' '.$LastName;
	$tablename = 'customer';
	$dbnameobj = 'ecashpdq_eloan';
  	$subject   = 'Uploaded Batch Instructions via csv File - '.date('Y-m-d H-i-s');

	$InterUserCode = getpost('IntUserCode');
	$time         = date('H-i-s');
    $date         = date('Y-m-d');
	$filename 	  = 'emailtemplate_uploadedbillinstructions.html';
	$filecontent  = 'emailtemplate_uploadedbillinstructions.html';
	$filecontent = file_get_contents($filename);
	$filecontent = str_replace("[IntUserCode]",$InterUserCode,$filecontent);
	$filecontent = str_replace("[name]",$fullname,$filecontent);
	$filecontent = str_replace("[date]",$date,$filecontent);
	$filecontent = str_replace("[time]",$time,$filecontent);
	$filecontent = str_replace("[contents]",$contents,$filecontent);

// -- Get Email to reciepients for customer
	$emails = GetEmailRecipients($InterUserCode,$dbnameobj,$tablename);
	// -- Send Email.
	if(!empty($emails))
	{
	  // -- Log Email sent to recipients.
	  foreach($emails as $row)
	  {
		$email = $row['email2'];
		SendEmail($filecontent,$row['email2'],$subject);
	    $phone = $row['phone2'];
	    $status = 'success';
	    AddCommunicatioHistoryGlobal($filecontent,$InterUserCode,$InterUserCode,$InterUserCode,$status,$email,$phone,'email');
	  }
	 }

	 return $emails;
}}
////////////////////////////////////////////////////////////////////////
// ----				 Declarations & Executions					   ---//
////////////////////////////////////////////////////////////////////////
$output1 = null;
$branch = getpost('srUserCode1'); // -- 20/02/2022
ob_start ();
$output1 = uploadcsvfile($branch);
ob_end_clean();
//$output1 = json_encode($output1);
print_r($output1);
//echo $output1;
/*$name = 'NAEDO 5 Day Tracking';
$cached = getmasterdata();
$cachedDataBank = $cached[2];
echo GetentryclassidCache($name ,$cachedDataBank);
var_dump($cachedDataBank);*/
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
?>
