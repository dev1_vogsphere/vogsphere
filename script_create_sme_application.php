<?php
date_default_timezone_set('Africa/Johannesburg');

// * To change this template, choose Tools | Templates
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
$IntUserCodeB=getpost('UserCode');
/* // -- email confirmation 23/5/2023....
if(!function_exists('email_confirmation'))
{
	function email_confirmation($dataConfirmation)
	{
		 $emailFlag     = 'yes';
		 $tenantid      = $dataConfirmation['IntUserCode'];
		 $customerid    = $dataConfirmation['Member_id'];
		 require  $dataConfirmation['filerootpath'].'/effms_confirmation.php';
	}
}
 */
 ////////////////////////////////////////////////////////////////////////
// -- Move Uploaded File -- //
if(!function_exists('moveuploadedfile'))
{
	function moveuploadedfile($name,$tmp_name )
	{
			$today = date('Y-m-d H-i-s');
			$userid = getpost('userid');
			$IntUserCodeB=getpost('UserCode');
			$path = "uploads/".$IntUserCodeB."/".$userid."_".$today."_"; // Upload directory
			$count = 0;
		// No error found! Move uploaded files
		// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			$ext = 'pdf';//pathinfo($name, PATHINFO_EXTENSION);
		// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;

		// -- Update File Name Directory
	            if(move_uploaded_file($tmp_name , $path.$name))
				{
	            	$count++; // Number of successfully uploaded files
	            }
			$newfilename = $path.$name;
			return 	$newfilename ;
	}
} 
 if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}

if(!function_exists('create_sme_application'))
{
	function create_sme_application($member,$memberproxy,$businessInfo,$loanInfo,$businessowneinfo)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		////////////////////////////////////////////////////////////////////////
		$message = 'member';
		$Member_id  = 0;
		$ret = '';
		try
		{
			//get business ID
			//print_r($member);
			$tbl = 'businessowneinfo';
			$tbl2 = 'businessproxy';
			$tbl3 = 'business';
			$tbl4 = 'loanapp';
			
			//Insert Loan info
			$sqlData = sql_associative_array($loanInfo);
			$sql = "INSERT INTO $tbl4
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//error_log(print_r('SQL response from insert'.$q));
			$message = 'success';
			
			if ($message=='success')
			{
			//Insert Business proxy
			$sqlData = sql_associative_array($memberproxy);
			$sql = "INSERT INTO $tbl2
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//error_log(print_r('SQL response from insert'.$q));
			$message = 'success';
			
				if ($message=='success')
				{
				//Insert Business Information 
				$sqlData = sql_associative_array($businessInfo);
				$sql = "INSERT INTO $tbl3
				({$sqlData[0]})
				VALUES ({$sqlData[1]}?)";
				$q   = $pdo->prepare($sql);
				$q->execute($sqlData[2]);
				//error_log(print_r('SQL response from insert'.$q));
				$message = 'success';
				
					if ($message=='success')
					{
					//Insert Business Information 
					$sqlData = sql_associative_array($member);
					$sql = "INSERT INTO $tbl
					({$sqlData[0]})
					VALUES ({$sqlData[1]}?)";
					$q   = $pdo->prepare($sql);
					$q->execute($sqlData[2]);
					//error_log(print_r('SQL response from insert'.$q));
					$message = 'success';
					error_log(json_encode($businessowneinfo));
						
						error_log("inside");
						$size = count($businessowneinfo);
						if($size > 0){
						for($i = 0;$i<$size;$i++)
						{
						if(!empty($businessowneinfo[$i]['identificationNumber']))
						{
						str_replace('<span style="font-size: 12px;">','',$identificationNumber[$i]['identificationNumber']);	
						str_replace('</span>','',$identificationNumber[$i]['identificationNumber']);
						error_log("inside2");
						$sqlData = sql_associative_array($businessowneinfo[$i]);
						$sql = "INSERT INTO $tbl
						({$sqlData[0]})
						VALUES ({$sqlData[1]}?)";
						//print($sqlData[0]);
						//print($sqlData[1]);
						//print_r($sqlData[2]);
						$q   = $pdo->prepare($sql);
						$q->execute($sqlData[2]);
						$message = 'success';
					
						}}}
						else
						{
							//do nothing
						}
						
					
					}
					else
					{
						$message = 'Something went wrong,contact the system Administration';
					}
			
				
				}
				else
				{
					$message = 'Something went wrong,contact the system Administration';
				}
			
			}
			else
			{
				$message = 'Something went wrong,contact the system Administration';
			}
		
	    }
		catch (PDOException $e)
		{
			$message = $message.$e->getMessage();
		
		}
		$ret = $message;
		error_log(print_r('response from insert'.$ret));
		return $ret;	
	}
}

// -- Upload the uploaded files..
$filenameEncryp1 = "";
$filenameEncryp2 = "";
$filenameEncryp3 = "";
$filenameEncryp4 = "";
$filenameEncryp5 = "";
$filenameEncryp6 = "";
$filenameEncryp7 = "";
$filenameEncryp8 = "";
$filenameEncryp9 = "";
$filenameEncryp10 = "";
$filenameEncryp11 = "";
$filenameEncryp12 = "";
$filenameEncryp13 = "";
$filenameEncryp14 = "";
$filenameEncryp15 = "";
$filenameEncryp16 = "";
$filenameEncryp17 = "";
$filenameEncryp18 = "";
$filenameEncryp19 = "";
$filenameEncryp20 = "";
$filenameEncryp21 = "";
$filenameEncryp22 = "";
//print_r($_FILES);
if(isset($_FILES['File1']['tmp_name']))
{
$tmp_name1 		= $_FILES['File1']['tmp_name'];
$name1     		= $_FILES['File1']['name'];
$filenameEncryp1 = moveuploadedfile($tmp_name1,$tmp_name1);
$message = $filenameEncryp1;
}

if(isset($_FILES['File2']['tmp_name']))
{
$tmp_name2 = $_FILES['File2']['tmp_name'];
$name2     = $_FILES['File2']['name'];
$filenameEncryp2 = moveuploadedfile($tmp_name2,$tmp_name2);
}

if(isset($_FILES['File3']['tmp_name']))
{
$tmp_name3 = $_FILES['File3']['tmp_name'];
$name3     = $_FILES['File3']['name'];
$filenameEncryp3 = moveuploadedfile($tmp_name3,$tmp_name3);
}

if(isset($_FILES['File4']['tmp_name']))
{
$tmp_name4 = $_FILES['File4']['tmp_name'];
$name4     = $_FILES['File4']['name'];
$filenameEncryp4 = moveuploadedfile($tmp_name4,$tmp_name4);
}

if(isset($_FILES['File5']['tmp_name']))
{
$tmp_name5 = $_FILES['File5']['tmp_name'];
$name5     = $_FILES['File5']['name'];
$filenameEncryp5 = moveuploadedfile($tmp_name5,$tmp_name5);
}
if(isset($_FILES['File6']['tmp_name']))
{
$tmp_name6 = $_FILES['File6']['tmp_name'];
$name6     = $_FILES['File6']['name'];
$filenameEncryp6 = moveuploadedfile($tmp_name6,$tmp_name6);
}
if(isset($_FILES['File7']['tmp_name']))
{
$tmp_name7 = $_FILES['File7']['tmp_name'];
$name7     = $_FILES['File7']['name'];
$filenameEncryp7 = moveuploadedfile($tmp_name7,$tmp_name7);
}
if(isset($_FILES['File8']['tmp_name']))
{
$tmp_name8 = $_FILES['File8']['tmp_name'];
$name8     = $_FILES['File8']['name'];
$filenameEncryp8 = moveuploadedfile($tmp_name8,$tmp_name8);
}
if(isset($_FILES['File9']['tmp_name']))
{
$tmp_name9 = $_FILES['File9']['tmp_name'];
$name9     = $_FILES['File9']['name'];
$filenameEncryp9 = moveuploadedfile($tmp_name9,$tmp_name9);
}
if(isset($_FILES['File10']['tmp_name']))
{
$tmp_name10 = $_FILES['File10']['tmp_name'];
$name10     = $_FILES['File10']['name'];
$filenameEncryp10 = moveuploadedfile($tmp_name10,$tmp_name10);
}
if(isset($_FILES['File11']['tmp_name']))
{
$tmp_name11 = $_FILES['File11']['tmp_name'];
$name11     = $_FILES['File11']['name'];
$filenameEncryp11 = moveuploadedfile($tmp_name11,$tmp_name11);
}
if(isset($_FILES['File12']['tmp_name']))
{
$tmp_name12 = $_FILES['File12']['tmp_name'];
$name12     = $_FILES['File12']['name'];
$filenameEncryp12 = moveuploadedfile($tmp_name12,$tmp_name12);
}
if(isset($_FILES['File13']['tmp_name']))
{
$tmp_name13 = $_FILES['File13']['tmp_name'];
$name13     = $_FILES['File13']['name'];
$filenameEncryp13 = moveuploadedfile($tmp_name13,$tmp_name13);
}
if(isset($_FILES['File14']['tmp_name']))
{
$tmp_name14 = $_FILES['File14']['tmp_name'];
$name14     = $_FILES['File14']['name'];
$filenameEncryp14 = moveuploadedfile($tmp_name14,$tmp_name14);
}
if(isset($_FILES['File15']['tmp_name']))
{
$tmp_name15 = $_FILES['File15']['tmp_name'];
$name15     = $_FILES['File15']['name'];
$filenameEncryp15 = moveuploadedfile($tmp_name15,$tmp_name15);
}
if(isset($_FILES['File16']['tmp_name']))
{
$tmp_name16 = $_FILES['File16']['tmp_name'];
$name16     = $_FILES['File16']['name'];
$filenameEncryp16 = moveuploadedfile($tmp_name16,$tmp_name16);
}
if(isset($_FILES['File17']['tmp_name']))
{
$tmp_name17 = $_FILES['File17']['tmp_name'];
$name17     = $_FILES['File17']['name'];
$filenameEncryp17 = moveuploadedfile($tmp_name17,$tmp_name17);
}
if(isset($_FILES['File18']['tmp_name']))
{
$tmp_name18 = $_FILES['File18']['tmp_name'];
$name18     = $_FILES['File18']['name'];
$filenameEncryp18 = moveuploadedfile($tmp_name18,$tmp_name18);
}
if(isset($_FILES['File19']['tmp_name']))
{
$tmp_name19 = $_FILES['File19']['tmp_name'];
$name19     = $_FILES['File19']['name'];
$filenameEncryp19 = moveuploadedfile($tmp_name19,$tmp_name19);
}
if(isset($_FILES['File20']['tmp_name']))
{
$tmp_name20 = $_FILES['File20']['tmp_name'];
$name20     = $_FILES['File20']['name'];
$filenameEncryp20 = moveuploadedfile($tmp_name20,$tmp_name20);
}
if(isset($_FILES['File21']['tmp_name']))
{
$tmp_name21 = $_FILES['File21']['tmp_name'];
$name21     = $_FILES['File21']['name'];
$filenameEncryp21 = moveuploadedfile($tmp_name21,$tmp_name21);
}
if(isset($_FILES['File22']['tmp_name']))
{
$tmp_name22 = $_FILES['File22']['tmp_name'];
$name22     = $_FILES['File22']['name'];
$filenameEncryp22 = moveuploadedfile($tmp_name22,$tmp_name22);
}
error_log(print_r('file path'.$filenameEncryp1));

// 1 -- Member	
$member =
array(
'identificationNumber'  =>getpost('Member_id'),	
'idbusiness'			=>getpost('regNumber'),	
'title' 			 	=>getpost('title'),	
'firstname' 			=>getpost('firstname'),	
'surname' 				=>getpost('surname'),
'ownershiptype' 		=>getpost('ownershiptype'),
'shares' 				=>getpost('shares'),
'usercode' 			=>getpost('UserCode'));

// Get total number of rows to be inserted..
$rows = getpost('rows');
$businessowneinfo = null;
error_log(json_encode($rows));

// 8. -- Directors Details ...
for($i = 0;$i < $rows;$i++)
{
$businessowneinfo[] = 
array(
'title'	=>getpost('Title_director'.$i),
'firstname'		=>getpost('firstname_director'.$i),
'surname'=>getpost('surname_director'.$i),
'identificationNumber'		=>getpost('id_director'.$i),
'ownershiptype'		=>getpost('ownershiptype'),
'shares'		=>getpost('Shareholding'.$i),
'usercode' 			=>getpost('UserCode'),
'idbusiness' 			=>getpost('regNumber'));

}

$memberproxy =
array(
'keytitle'  				=>getpost('keytitle'),		
'keyfirstname' 			 	=>getpost('keyfirstname'),	
'keysurname' 				=>getpost('keysurname'),
'keyidentificationNumber' 	=>getpost('keymember_id'),
'keycontactNumber' 			=>getpost('keycontactNumber'),
'idbusiness' 			 	=>getpost('regNumber'),	
'keyemailAddress' 			=>getpost('keyemailAddress'),
);

$businessInfo =
array(
'businessName'  				=>getpost('businessName'),		
'regNumber' 			 		=>getpost('regNumber'),	
'businessType' 					=>getpost('businessType'),	
'productsorServices' 			=>getpost('productsorServices'),
'businesslocation' 				=>getpost('businesslocation'),
'street' 						=>getpost('street'),
'suburb' 						=>getpost('suburb'),
'city' 							=>getpost('city'),
'state' 						=>getpost('state'),
'postalCode' 					=>getpost('postalCode'),
'turnover' 						=>getpost('turnover'),
'stageOfBusiness' 				=>getpost('stageOfBusiness'));


$loanInfo =
array(
'CustomerId'  				=>getpost('userid'),	
'businessrId' 			 		=>getpost('regNumber'),	
'loantypeid' 					=>'2',	
'usercode' 			=>getpost('UserCode'),
'stageOfBusiness' 				=>getpost('stageOfBusiness'),
'accountholdername' 				=>getpost('accountholdername'),
'bankname' 				=>getpost('bankname'),
'accounttype' 				=>getpost('accounttype'),
'accountnumber' 				=>getpost('accountnumber'),
'turnover' 				=>getpost('turnover'),
'turnoverfirewood' 				=>getpost('turnoverfirewood'),
'assetvaluetodate' 				=>getpost('assetvaluetodate'),
'kilns' 				=>getpost('kilns'),
'spades' 				=>getpost('spades'),
'chainsaws' 				=>getpost('chainsaws'),
'wheelbarrows' 				=>getpost('wheelbarrows'),
'scale' 				=>getpost('scale'),
'shovels' 				=>getpost('shovels'),
'brushcutter' 				=>getpost('brushcutter'),
'axes' 				=>getpost('axes'),
'fireExtinguishers' 				=>getpost('fireExtinguishers'),
'lasherbows' 				=>getpost('lasherbows'),
'startupcost' 				=>getpost('startupcost'),
'assetacquisition' 				=>getpost('assetacquisition'),
'workingcapital' 				=>getpost('workingcapital'),
'consentIndicator' 				=>getpost('consentIndicator'),
'workingcapitalamount' 				=>getpost('workingcapitalamount'),
'fundingindicator' 				=>getpost('fundingindicator'),
'fundingamount' 				=>getpost('fundingamount'),
'ownerscontributionindicator' 				=>getpost('ownerscontributionindicator'),
'ownerscontributionnotes' 				=>getpost('ownerscontributionnotes'),
'assetacquisitionindicator' 				=>getpost('assetacquisitionindicator'),
'startupcostindicator' 				=>getpost('startupcostindicator'),
'compensationfundindicator' 				=>getpost('compensationfundindicator'),
'uifindicator' 				=>getpost('uifindicator'),
'vatfindicator' 				=>getpost('vatfindicator'),
'payefindicator' 				=>getpost('payefindicator'),
'incometaxindicator' 				=>getpost('incometaxindicator'),
'incometaxnumber' 				=>getpost('incometaxnumber'),
'accountingofficerindicator' 				=>getpost('accountingofficerindicator'),
'businessplanrindicator' 				=>getpost('businessplanrindicator'),
'managementaccountsindicator' 				=>getpost('managementaccountsindicator'),
'cashflowindicator' 				=>getpost('cashflowindicator'),
'assetvaluetodate' 				=>getpost('assetvaluetodate'),
'businessname' 				=>getpost('businessName'),
'FILEIDDOC'				=>$filenameEncryp1, 		 
'OTHER1'				=>$filenameEncryp2,	 
'OTHER2'				=>$filenameEncryp3,
'OTHER3'				=>$filenameEncryp4, 	 
'OTHER4'				=>$filenameEncryp5,
'OTHER5'				=>$filenameEncryp6,
'OTHER6'				=>$filenameEncryp7,
'OTHER7'				=>$filenameEncryp8,
'OTHER8'				=>$filenameEncryp9,
'OTHER9'				=>$filenameEncryp10,
'OTHER10'				=>$filenameEncryp11,
'OTHER11'				=>$filenameEncryp12,
'OTHER12'				=>$filenameEncryp13,
'OTHER13'				=>$filenameEncryp14,
'OTHER14'				=>$filenameEncryp15,
'OTHER15'				=>$filenameEncryp16,
'OTHER16'				=>$filenameEncryp17,
'OTHER17'				=>$filenameEncryp18,
'OTHER18'				=>$filenameEncryp19,
'OTHER19'				=>$filenameEncryp20,
'OTHER20'				=>$filenameEncryp21,
'OTHER21'				=>$filenameEncryp22,
'prevfundingindicator'				=>getpost('funding_prev'),
'prevfundingindicatordesc'				=>getpost('funding_desc'));

$retMember = null;
// -- Create ffms application.
$retMember= create_sme_application($member,$memberproxy,$businessInfo,$loanInfo,$businessowneinfo);

?>