/*
Date : 2016.09.17
Add File Field
*/
ALTER TABLE `siwapp`.`payment` 
ADD COLUMN `FILE` LONGTEXT NULL AFTER `notes`;

ALTER TABLE `siwapp`.`loanapp` 
ADD COLUMN `accountholdername` VARCHAR(45) NULL AFTER `FILEFICA`,
ADD COLUMN `bankname` VARCHAR(45) NULL AFTER `accountholdername`,
ADD COLUMN `accountnumber` VARCHAR(45) NULL AFTER `bankname`,
ADD COLUMN `branchcode` VARCHAR(45) NULL AFTER `accountnumber`,
ADD COLUMN `accounttype` VARCHAR(45) NULL AFTER `branchcode`;