<?php
//Global Variables
$CurrentDate=date(Ymdhisa);
$Record_Identifier="H04";
$User_Code="UFCT"; // to be added to the database
$Creation_Date=date("ymd");
$Action_Date=date("Y/m/d");
$Filename ='../Files/Collections/Out/'.$User_Code.$CurrentDate.".txt";
//$First_Action_Date=substr((date(ymd)),0,4)."01"; //25th of the Month
//$Last_Action_Date=substr((date(ymd)),0,4)."01"; //25th of the Month

$First_Action_Date=$Creation_Date;
$Last_Action_Date=$Creation_Date;
$Sequence_Number="000001"; //Must contain 000001
$Service_Type="SAMEDAY   ";
$Nominated_Account_Number="02";  //For sameday processing
$Total_Debit_Value=0;

//JDBC query get get sequence number from database Seq_Generator table
//$connect = mysqli_connect("localhost","root","root","PayCollections","8888");
$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL:JDBC query get get sequence number " . mysqli_connect_error();
  }
  $query ="SELECT Next_Seq_Number,Client_Id FROM ecashpdq_paycollections.Seq_Generator WHERE Client_Id='$User_Code'";


$result = mysqli_query($connect,$query);
//$Generation_Number=$result; //Starts 0001 increment by one per Batch Run
if ($result->num_rows > 0)
{

  while($row = $result->fetch_assoc())
  {
          $Previous_Seq_Number=($row["Next_Seq_Number"]);
          $Generation_Number=($row["Next_Seq_Number"]);
 	  $New_Seq_Number= (($row["Next_Seq_Number"])+1);// increment next sequence number
	  $New_Seq_Number=str_pad($New_Seq_Number,4,"0",STR_PAD_LEFT);
          $Client_Id=$row["Client_Id"];
  }
    //Update Seq_Generator
    $sql = "UPDATE ecashpdq_paycollections.Seq_Generator SET Next_Seq_Number='$New_Seq_Number',Previous_Seq_Number='$Previous_Seq_Number' WHERE Client_Id='$User_Code'";
    if ($connect->query($sql) === TRUE) {
      echo "Record updated Generation Number";
    } else {
      echo "Error: " . $sql . "<br>" . $connect->error;
    }

}else {
    echo "Bad";
    echo "0 results";

}
//End

//JDBC query get collection Transactions from database
//$connect = mysqli_connect("localhost","root","root","PayCollections","8888");
$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
//$query ="SELECT  FROM Collection ORDER BY First_Action_Date DESC";
$query ="SELECT * FROM ecashpdq_paycollections.collection WHERE Action_Date='$Action_Date' and Client_Id='$User_Code' and Status_Description='pending'";
$result = mysqli_query($connect,$query);


//Create file
$fileHandle = fopen($Filename, 'w') or die ("Can't open file");

//EFT Header Record
fwrite ($fileHandle, $Record_Identifier);
fwrite ($fileHandle, $User_Code);
fwrite ($fileHandle, $Creation_Date);
fwrite ($fileHandle, $Creation_Date);
fwrite ($fileHandle, $First_Action_Date);
fwrite ($fileHandle, $Last_Action_Date);
fwrite ($fileHandle, $Sequence_Number);
fwrite ($fileHandle, $Generation_Number);
fwrite ($fileHandle, $Service_Type);
fwrite ($fileHandle, $Nominated_Account_Number);
fwrite ($fileHandle, "\n");

//EFT Transaction Record
if ($result->num_rows > 0)
{
  while($row = $result->fetch_assoc())
  {
          //echo str_pad("0", 8, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, "T"); // 1 Transaction Type

          $Branch_Code=str_pad($row["Branch_Code"], 6, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $Branch_Code); // 6 Branch Code

          //Hash Total
          $Hash_Total+=$row["Account_Number"];// Total number of Debit value
          //Set account to 13 length size
          $Account_Number=str_pad($row["Account_Number"], 13, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $Account_Number);// 13 Account Number  Fix account number when you insert; Change the account number to varchar


          fwrite ($fileHandle, $row["Account_Type"]); // 1 Account_Type Modise to make  a change and pass values


          $Total_Debit_Value+=$row["Amount"];// Total number of Debit value

          $Amount=str_replace(".","",strval($row["Amount"]));// remove amount separator
          $Amount=str_pad($Amount, 11, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $Amount); // 11 Amount in Cents

          $Bank_Reference=str_pad($row["Bank_Reference"], 10, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $Bank_Reference); // 10 Company Abbreviated name

          $Client_Reference_1=str_pad($row["Client_Reference_1"], 14, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $Client_Reference_1); // 14 Client Identifier

          $Action_Date=date("ymd",strtotime($row["Action_Date"]));
          fwrite ($fileHandle, $Action_Date); // 6 Action Date YYMMDD

          $Account_Holder=str_pad($row["Initials"]." ".$row["Account_Holder"], 30, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $Account_Holder); // 30 Account Holder Name

          fwrite ($fileHandle, $Action_Date); // 6 Action DateYYMMDD

          fwrite ($fileHandle, "0"); // 1 CDV Check Modise to add a CDV check button
          fwrite ($fileHandle, "44"); // 2 Entry Class Modise to make a change
          fwrite ($fileHandle, "\n");


          //JDBC update status
          $collectionid=$row["collectionid"];
          echo $collectionid;
          $sql = "UPDATE ecashpdq_paycollections.collection SET Status_Description='Transaction Successful',Status_Code='00' WHERE collectionid=$collectionid";
          if ($connect->query($sql) === TRUE) {
            echo "Status updated";
          } else {
            echo "Error: " . $sql . "<br>" . $connect->error;
          }
  }

}else {
    echo "Bad";
    echo "0 results";

}

//EFT Trailer Record
fwrite ($fileHandle, "Z92"); // 3 Record Identifier
fwrite ($fileHandle, $User_Code); //User Code add to database
fwrite ($fileHandle, $Sequence_Number); //Sequence Number add to database

$lines_file=COUNT(FILE($Filename))-1; // Transaction and Header count to calculate
$lines_file_Debit=COUNT(FILE($Filename))-2; //Number of Debit Transaction to calculate
$lines_file=str_pad($lines_file, 6, '0', STR_PAD_LEFT);
$lines_file_Debit=str_pad($lines_file_Debit, 6, '0', STR_PAD_LEFT);
$lines_file_Credit=str_pad("0", 6, '0', STR_PAD_LEFT);

fwrite ($fileHandle, $lines_file); //Transaction and Header count to calculate

fwrite ($fileHandle, $First_Action_Date); //First Action Date
fwrite ($fileHandle, $Last_Action_Date); //Last Action Date

fwrite ($fileHandle, $lines_file_Debit); //Number of Debit Transaction to calculate
fwrite ($fileHandle, $lines_file_Credit); //Number of Credit Transaction to calculate

//Unknown field
fwrite ($fileHandle, $Sequence_Number); //Sequence Number add to database

//echo $Total_Debit_Value;
$Total_Debit_Value=number_format($Total_Debit_Value,2);
$Total_Debit_Value=str_replace(".","",$Total_Debit_Value);// remove amount separator
$Total_Debit_Value=str_replace(",","",$Total_Debit_Value);// remove amount separator
echo $Total_Debit_Value;
$Total_Debit_Value=str_pad($Total_Debit_Value, 12, '0', STR_PAD_LEFT);

fwrite ($fileHandle, $Total_Debit_Value); //Total Debit value to calculate

$Total_Credit_Value=str_pad("0", 12, '0', STR_PAD_LEFT);
fwrite ($fileHandle, $Total_Credit_Value); //Total Credit value to calculate

//Hash total
$Hash_Total=str_pad($Hash_Total, 18, '0', STR_PAD_LEFT);
$Hash_Total=substr($Hash_Total,6,12);
$Hash_Total=str_pad($Hash_Total, 16, '0', STR_PAD_LEFT);
fwrite ($fileHandle, $Hash_Total); //Hash Total to calculate

//close file
fclose($fileHandle);

//Close Database connection
$conn->close();
//$fileHandle = fopen ("/Warehousing/Mercantile.epl", 'r');
//$theInfo = fgets($fileHandle);
//fclose($fileHandle);

//echo $theInfo;
?>
