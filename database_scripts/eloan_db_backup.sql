DROP TABLE IF EXISTS  accounttype;
DROP TABLE IF EXISTS  bankbranch;
DROP TABLE IF EXISTS  bank;
DROP TABLE IF EXISTS  charge;
DROP TABLE IF EXISTS  chargeoption;
DROP TABLE IF EXISTS  chargetype;
DROP TABLE IF EXISTS  funder;
DROP TABLE IF EXISTS  paymentmethod;

DROP TABLE IF EXISTS currency;
DROP TABLE IF EXISTS sourceofincome;
DROP TABLE IF EXISTS transactionhistory;
DROP TABLE IF EXISTS loanfunded;
DROP TABLE IF EXISTS encrypted;

CREATE TABLE `encrypted` (
  `encryptedid` INT NOT NULL AUTO_INCREMENT,
  `ApplicationId` BIGINT(13) NULL,
  `CustomerId` INT(11) NULL,
  `dateEncrypted` DATE NULL,
  `document` VARCHAR(45) NULL,
  PRIMARY KEY (`encryptedid`));

CREATE TABLE `loanfunded` (
  `loanfundedid` INT NOT NULL AUTO_INCREMENT,
  `loanappid` INT(11) NULL,
  `funderid` BIGINT(13) NULL,
  `status` VARCHAR(45) NULL,
  `fundeddate` DATE NULL,
  `statusdate` DATE NULL,
  PRIMARY KEY (`loanfundedid`));
  
INSERT INTO `loanfunded` (`loanappid`, `funderid`, `status`, `fundeddate`, `statusdate`) VALUES ('1', '1', 'APPR', '2014-06-06', '2014-06-06');
  
CREATE TABLE `transactionhistory` 
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(300) NULL,
  `userid` BIGINT(13) NULL,
  `date` DATE NULL,
  `amount` DECIMAL(7,2) NULL,
  PRIMARY KEY (`id`)
  );
  
INSERT INTO `transactionhistory` (`description`, `userid`, `date`, `amount`) VALUES ('Account Balance Brought Forward	', '1', '2014-06-06', '0.00');
  
CREATE TABLE `sourceofincome` (
  `sourceofincomeid` INT NOT NULL AUTO_INCREMENT,
  `sourceofincomename` VARCHAR(45) NULL,
 PRIMARY KEY (`sourceofincomeid`));

INSERT INTO `sourceofincome` (`sourceofincomename`) VALUES ('Salary');
INSERT INTO `sourceofincome` (`sourceofincomename`) VALUES ('Other');

CREATE TABLE `currency` (
  `currencyid` varchar(20) NOT NULL,
  `currencyname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO currency VALUES("$","US Dollar($)");
INSERT INTO currency VALUES("L","Maluti(L)");
INSERT INTO currency VALUES("P","Pula(P)");
INSERT INTO currency VALUES("R","Rand(R)");

CREATE TABLE `funder` (
  `funderid` BIGINT(13) NOT NULL AUTO_INCREMENT,
  `Title` ENUM('Mr', 'Mrs', 'Miss', 'Ms', 'Dr', 'Company') DEFAULT NULL,
  `fundername` varchar(255) DEFAULT NULL,
  `contactname` varchar(255) DEFAULT NULL,
  `phone` varchar(45),
  `email` varchar(255),
  `Street` VARCHAR(45) DEFAULT NULL,
  `Suburb` VARCHAR(45) DEFAULT NULL,
  `City` VARCHAR(45) DEFAULT NULL,
  `State` ENUM('EC', 'FS', 'GP', 'LP', 'MP', 'NW', 'NC', 'WC', 'KZ') DEFAULT NULL,
  `PostCode` CHAR(4) DEFAULT NULL,
  `Dob` DATE DEFAULT NULL,
  `accountholdername` VARCHAR(45),
  `bankname` VARCHAR(45),
  `accountnumber` VARCHAR(45),
  `accounttype` VARCHAR(45),
  `branchcode` VARCHAR(45),
  `FILEIDDOC` VARCHAR(45),
  `FILEPROOFADR` VARCHAR(45),
  `FILECONTRACT` VARCHAR(45),
  `status` VARCHAR(45),
  `applicationdate` DATE DEFAULT NULL,
  `sourceofincome` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`funderid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO  `funder`  VALUES("1","Company","Vogsphere (Pty) Ltd","Mr. Mulunghisi Mahwayi","0791234854","info@vogsphere.co.za","","","","GP","","2014-06-06","","","","","","","","","","2014-06-06","Investments");


DROP TABLE IF EXISTS  globalsettings;



DROP TABLE IF EXISTS  loanappcharges;

DROP TABLE IF EXISTS  payment;

DROP TABLE IF EXISTS  paymentfrequency;



DROP TABLE IF EXISTS  paymentschedule;

DROP TABLE IF EXISTS  province;


DROP TABLE IF EXISTS  series;




DROP TABLE IF EXISTS  common;

DROP TABLE IF EXISTS  loanapp;

DROP TABLE IF EXISTS  customer;
DROP TABLE IF EXISTS  customers;


DROP TABLE IF EXISTS  user;

DROP TABLE IF EXISTS  loantype;


CREATE TABLE `accounttype` (
  `accounttypeid` int(11) NOT NULL AUTO_INCREMENT,
  `accounttypedesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`accounttypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO   `accounttype`  VALUES("1","Cheque");
INSERT INTO   `accounttype`  VALUES("2","Saving");
INSERT INTO   `accounttype`  VALUES("3","Current");
INSERT INTO   `accounttype`  VALUES("4","Test-2");



CREATE TABLE `bank` (
  `bankid` varchar(255) NOT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bankid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `bank`  VALUES("ABSA","ABSA");
INSERT INTO  `bank`  VALUES("bankofathens","Bank of Athens");
INSERT INTO  `bank`  VALUES("Bidvest","Bidvest Bank");
INSERT INTO  `bank`  VALUES("capitec","Capitec");
INSERT INTO  `bank`  VALUES("fnb","First National Bank");
INSERT INTO  `bank`  VALUES("Investec","Investec");
INSERT INTO  `bank`  VALUES("modix","Modise");
INSERT INTO  `bank`  VALUES("nedbank","NedBank");
INSERT INTO  `bank`  VALUES("Postbank","SA Post Bank (Post Office)");
INSERT INTO  `bank`  VALUES("StandardBank","Standard Bank");



CREATE TABLE `bankbranch` (
  `branchcode` varchar(255) NOT NULL,
  `bankid` varchar(255) DEFAULT NULL,
  `branchdesc` varchar(255) NOT NULL,
  PRIMARY KEY (`branchcode`),
  KEY `fk_bankid_idx` (`bankid`),
  CONSTRAINT `fk_bankid` FOREIGN KEY (`bankid`) REFERENCES `bank` (`bankid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `bankbranch` VALUES("	410506","bankofathens","Universal");
INSERT INTO  `bankbranch` VALUES("051001","StandardBank","Universal");
INSERT INTO  `bankbranch` VALUES("198765","nedbank","Universal");
INSERT INTO  `bankbranch` VALUES("250655","fnb","Universal");
INSERT INTO  `bankbranch` VALUES("460005","Postbank","Universal");
INSERT INTO  `bankbranch` VALUES("462005","Bidvest","Universal");
INSERT INTO  `bankbranch` VALUES("470010","capitec","Universal");
INSERT INTO  `bankbranch` VALUES("580105","Investec","Universal");
INSERT INTO  `bankbranch` VALUES("632005","ABSA","Universal-ABSA");



CREATE TABLE `charge` (
  `chargeid` int(11) NOT NULL AUTO_INCREMENT,
  `chargename` varchar(45) DEFAULT NULL,
  `chargetypeid` int(11) DEFAULT NULL,
  `chargeoptionid` int(11) DEFAULT NULL,
  `loantypeid` int(11) DEFAULT NULL,
  `paymentmethodid` varchar(45) DEFAULT NULL,
  `active` varchar(45) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL,
  `amount` decimal(5,2) DEFAULT NULL,
  `min` decimal(5,2) DEFAULT NULL,
  `max` decimal(5,2) DEFAULT NULL,
  `graceperiod` int(11) DEFAULT NULL,
  PRIMARY KEY (`chargeid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO  `charge` VALUES("1","Disbursement - Immediate Transfer","5","1","1","EFT","X","0.00","43.00","0.00","0.00","0");
INSERT INTO  `charge` VALUES("2","Arrears","3","1","3","DTO","X","0.00","2.00","10.00","20.00","0");
INSERT INTO  `charge` VALUES("3","Bounced Debit Order Fee","4","3","3","DTO","X","0.00","100.00","0.00","0.00","0");
INSERT INTO  `charge` VALUES("4","Insurance Fee","3","2","1","DTO","X","0.00","20.00","0.00","1.00","1");
INSERT INTO  `charge` VALUES("6","test","4","1","3","DTO","","0.00","0.00","0.00","0.00","0");



CREATE TABLE `chargeoption` (
  `chargeoptionid` int(11) NOT NULL AUTO_INCREMENT,
  `chargeoptionname` varchar(45) DEFAULT NULL,
  `chargeoptiondesc` varchar(255) DEFAULT NULL,
  `chargeoption` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`chargeoptionid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO  `chargeoption`  VALUES("1","Fee","Fee - Flat Rate Amount","Fixed");
INSERT INTO  `chargeoption`  VALUES("2","Penalty","Penalties - amount due, depending on the grace period and days(min & max)","%");
INSERT INTO  `chargeoption`  VALUES("3","Interest Due","Interest Due on the amount","%");
INSERT INTO  `chargeoption`  VALUES("5","test","test 23","");

CREATE TABLE `chargetype` (
  `chargetypeid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `chargetypedesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`chargetypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO  `chargetype` VALUES("1","Specific Due Date","Select ‘Specified Due Date’ if the charge will be applied manually to a product on a date specified by the user. These charges can be applied more than once throughout a loan terms duration. ");
INSERT INTO  `chargetype` VALUES("2","Disbursement Fee","Select ‘Disbursement’ if the charge is intended to be applied to a loan when it is disbursed. ");
INSERT INTO  `chargetype` VALUES("3","OverDue Installment Fees ","Select ‘Overdue Instalment Amount’ if the charge is a penalty.");
INSERT INTO  `chargetype` VALUES("4","Bounced Debit Order","The amount charged for a bounced debit order.");
INSERT INTO  `chargetype` VALUES("5","Insurance Fee","The amount charged for insurance of the loan amount.");


CREATE TABLE `common` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `series_id` bigint(20) DEFAULT NULL,
  `CustomerId` bigint(20) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_identification` varchar(50) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `invoicing_address` longtext,
  `shipping_address` longtext,
  `contact_person` varchar(100) DEFAULT NULL,
  `terms` longtext,
  `notes` longtext,
  `base_amount` decimal(53,15) DEFAULT NULL,
  `discount_amount` decimal(53,15) DEFAULT NULL,
  `net_amount` decimal(53,15) DEFAULT NULL,
  `gross_amount` decimal(53,15) DEFAULT NULL,
  `paid_amount` decimal(53,15) DEFAULT NULL,
  `tax_amount` decimal(53,15) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `draft` tinyint(1) DEFAULT '1',
  `closed` tinyint(1) DEFAULT '0',
  `sent_by_email` tinyint(1) DEFAULT '0',
  `number` int(11) DEFAULT NULL,
  `recurring_invoice_id` bigint(20) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `days_to_due` mediumint(9) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `max_occurrences` int(11) DEFAULT NULL,
  `must_occurrences` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `period_type` varchar(8) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `finishing_date` date DEFAULT NULL,
  `last_execution_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `common`  VALUES ("1","1","8708035545057","sasas","8708035545057","mpholo@vogsphere.co.za","sasassasasas","Shipping Address","sasasas","","7","-110.000000000000000","0.000000000000000","-110.000000000000000","-110.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2014-06-06","2018-04-13","0","0","0","0","0","","2014-06-06","2014-06-06","2014-06-06","2014-06-06 15:20:41","2015-07-08 22:00:39");
INSERT INTO `common`  VALUES ("2","1","8708035545057","sasas","8708035545057","mpholo@vogsphere.co.za","sasassasasas","Shipping Address","sasasas","","7","-110.000000000000000","0.000000000000000","-110.000000000000000","-110.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2014-06-06","2018-04-13","0","0","0","0","0","","2014-06-06","2014-06-06","2014-06-06","2014-06-06 15:20:41","2015-07-08 22:00:39");
INSERT INTO `common`  VALUES ("3","1","8708035545057","sasas","8708035545057","mpholo@vogsphere.co.za","sasassasasas","Shipping Address","sasasas","","7","-110.000000000000000","0.000000000000000","-110.000000000000000","-110.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2014-06-06","2018-04-13","0","0","0","0","0","","2014-06-06","2014-06-06","2014-06-06","2014-06-06 15:20:41","2015-07-08 22:00:39");
INSERT INTO `common`  VALUES ("4","1","8708035545057","sasas","8708035545057","mpholo@vogsphere.co.za","sasassasasas","Shipping Address","sasasas","","7","-110.000000000000000","0.000000000000000","-110.000000000000000","-110.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2014-06-06","2018-04-13","0","0","0","0","0","","2014-06-06","2014-06-06","2014-06-06","2014-06-06 15:20:41","2015-07-08 22:00:39");
INSERT INTO `common`  VALUES ("5","1","8708035545057","sasas","8708035545057","mpholo@vogsphere.co.za","sasassasasas","Shipping Address","sasasas","","7","-110.000000000000000","0.000000000000000","-110.000000000000000","-110.000000000000000","0.000000000000000","0.000000000000000","2","Invoice","0","0","0","1","0","2014-06-06","2018-04-13","0","0","0","0","0","","2014-06-06","2014-06-06","2014-06-06","2014-06-06 15:20:41","2015-07-08 22:00:39");

CREATE TABLE `user` (
  `userid` bigint(13) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(45) NOT NULL,
  `role` enum('customer','admin') NOT NULL,
  `failedAttempt` INT(11) DEFAULT '0' ,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `user`  VALUES("8708035205087","pepele","tumelomodise4@gmail.com","customer");
INSERT INTO  `user`  VALUES("8708035545057","test","TestExpert5vrr@gmailx.com","customer");
INSERT INTO  `user`  VALUES("8708035545086","pepele","tumelomodise4x@gmail.com","customer");
INSERT INTO  `user`  VALUES("9901019999997","admin7","eloan@vogsphere.co.za","admin");
INSERT INTO  `user`  VALUES("9901019999998","admin8","admin@vogsphere.co.za","admin");
INSERT INTO  `user`  VALUES("9901019999999","admin9","info@vogsphere.co.za","admin");

CREATE TABLE `customer` (
  `CustomerId` bigint(13) NOT NULL,
  `Title` enum('Mr','Mrs','Miss','Ms','Dr') DEFAULT NULL,
  `FirstName` varchar(200) NOT NULL,
  `LastName` varchar(200) NOT NULL,
  `Street` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `State` enum('EC','FS','GP','LP','MP','NW','NC','WC','KZ') NOT NULL,
  `PostCode` char(4) NOT NULL,
  `Dob` date DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CustomerId`),
  CONSTRAINT `userid` FOREIGN KEY (`CustomerId`) REFERENCES `user` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `customer`  VALUES("8708035205087","Mr","Tumelo","Modise","Test","The Reeds","Centurion","LP","0001","2017-04-27","0131238839");
INSERT INTO  `customer`  VALUES("8708035545057","Mr","Test Dummy","Modise","Test","Test - Subs","Cape Town","WC","0001","2017-04-28","0131238839");
INSERT INTO  `customer`  VALUES("9901019999997","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","GP","0157","2017-04-28","0131238839");
INSERT INTO  `customer`  VALUES("9901019999998","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","GP","0157","2017-04-28","0131238839");
INSERT INTO  `customer`  VALUES("9901019999999","Mr","Admin","Admin","43 Mulder, Street","The reeds","Centurion","GP","0157","2017-04-28","0131238839");

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO  `customers`  VALUES("1","Tumelo-Mosala","tumelomodise4@yahoo.com","0891238839");

CREATE TABLE `globalsettings` 
(
  `globalsettingsid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `logo` varchar(600) DEFAULT NULL,
  `legaltext` varchar(600) DEFAULT NULL,
  `currency` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `suburb` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `registrationnumber` varchar(200) DEFAULT NULL,
  `ncr` varchar(100) DEFAULT NULL,
  `terms` varchar(1000) DEFAULT NULL,
  `disclaimer` varchar(1000) DEFAULT NULL,
  `accountholdername` varchar(45) DEFAULT NULL,
  `bankname` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `branchcode` varchar(45) DEFAULT NULL,
  `accounttype` varchar(45) DEFAULT NULL,
  `homepage` varchar(5000) DEFAULT NULL,
  `adminpage` varchar(5000) DEFAULT NULL,
  `customerpage` varchar(5000) DEFAULT NULL,
  `vat` decimal(7,2) DEFAULT '0.00',
  `vatnumber` varchar(100) DEFAULT NULL,
  `mailhost` VARCHAR(45) DEFAULT NULL,
  `port` VARCHAR(45) DEFAULT NULL,
  `username` VARCHAR(45) DEFAULT NULL,
  `password` VARCHAR(45) DEFAULT NULL,
  `SMTPSecure` VARCHAR(45) DEFAULT NULL,
  `dev` VARCHAR(1) DEFAULT NULL,
  `prod` VARCHAR(1) DEFAULT NULL,
  PRIMARY KEY (`globalsettingsid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8

;INSERT INTO globalsettings VALUES("2","Vogsphere Pty Ltd","0131238839","0731238839","info@vogsphere.co.za","http://www.vogsphere.co.za","logo_vogs.png","You must pay to us all amounts that are due and payable in terms of this Agreement, on or before the payment date,
\nwithout any deduction or demand.
\nYou have the right at any time to pay in advance any amounts owed to us without notice or penalty irrespective of
\nwhether or not they are due.
\nBank charges are applicable for payments made via ATM and Internet banking transfers payment are free.","R","43 Mulder Street","The Reeds","Centurion","GP","0001","2012/020274/74/07","NCRCP9272","You must pay to us all amounts that are due and payable in terms of this Agreement, on or before the payment date,
\nwithout any deduction or demand.
\nYou have the right at any time to pay in advance any amounts owed to us without notice or penalty irrespective of
\nwhether or not they are due.
\nBank charges are applicable for payments made via ATM and Internet banking transfers payment are free.","Vogsphere (Pty) Ltd Registration number 2012/020274/74/07. An Authorised Financial Services and Credit Provider (NCRCP9272)","Vogsphere (Pty) Ltd","bankofathens","62341624775","410506","1","<div class=\"col-md-6\">
\n																<h3>Welcome to Vogsphere e-Loan</h3>
\n																<p>E-loan offers flexible, short-term loans from as minimun as R500.00 up to R5000.00 for new Customers.$loantype - Fixed Interest rate of <b>$personalloan%</b>. Loan duration from 1 to 8 months.
\n																<br>
\n																</p><h3 class=\"padding-vert-10\">Required Documents</h3>
\n																<ul class=\"tick animate fadeInRight\">
\n																 <li>ID Document</li>
\n																 <li>Latest 3 months Bank Statement</li>
\n																 <li>Proof of Employment/Income</li>
\n																 <li>Proof of Residence</li>
\n																</ul>
\n																<p></p>
\n																<br>
\n															</div>
\n															
\n															<div class=\"col-md-6\">
\n																<h3 class=\"padding-vert-10\">Key Features</h3>
\n																<ul class=\"tick animate fadeInRight\">
\n																	<li>Maximum of 24 hours to approve you loan</li>
\n																	<li>Apply online 24/7</li>
\n																	<li>No branch visit</li>
\n																</ul>
\n															</div>
\n															
\n															<div class=\"col-md-6\">
\n															<p>New Customers wishing to apply for a loan go to the 
\n																<a href=\"applicationForm\">Application Form</a>
\n																</p>
\n																<p>Existing Customers wishing to see details of their Loan go to
\n																<a href=\"customerLogin\">Customer Loan Information</a> </p>
\n																<p>(NOTE: you will need your Customer ID and password to access this page)
\n																</p>
\n															</div>","<div class=\"col-md-6\"><h3>Welcome $Title $FirstName $LastName to Vogsphere e-Loan Adminstration</h3><p>Adminstration of E-loan which offers flexible, short-term loans from as minimun as R500.00 up to R5000.00 for new Customers.</p><br><p>Details of you Loan applications administration go to <a href=\"custAuthenticate\">E-LOAN ADMINISTRATION</a> </p>	</div>																													<div class=\"col-md-6\">														<h3 class=\"padding-vert-10\">e-Loan Adminstration</h3>									<ul class=\"tick animate fadeInRight\">											<li>Manage customer master data</li>											<li>Manage e-Loan Applications</li>											<li>Approve, Reject, Cancel e-Loan Application</li>									<li>Manage e-Loan User Accounts</li>											</ul>														</div>","	<div class=\"col-md-6\">
\n																<h3>Welcome $Title $FirstName $LastName to Vogsphere e-Loan</h3>
\n																<p>E-loan offers flexible, short-term loans from as minimun as R500.00 up to R5000.00 for new Customers.Fixed Interest rate of <b>$personalloan%</b>. Loan duration from 1 to 6 months.
\n																<br>
\n																</p><h3 class=\"padding-vert-10\">Required Documents</h3>
\n																<ul class=\"tick animate fadeInRight\">
\n																 <li>ID Document</li>
\n																 <li>Latest 3 months Bank Statement</li>
\n																 <li>Proof of Employment/Income</li>
\n																 <li>Proof of Residence</li>
\n																</ul>
\n																<p></p>
\n																<br>
\n															</div>
\n															<!-- End Main Text -->
\n															<div class=\"col-md-6\">
\n																<h3 class=\"padding-vert-10\">Key Features</h3>
\n																<ul class=\"tick animate fadeInRight\">
\n																	<li>Maximum of 24 hours to approve you loan</li>
\n																	<li>Apply online 24/7</li>
\n																	<li>No branch visit</li>
\n																</ul>
\n															</div>
\n															
\n														    <div class=\"col-md-6\">
\n															<p>New Customers wishing to apply for a loan go to the 
\n																<a href=\"applicationForm\">Application Form</a>
\n																</p>
\n																<p>Existing Customers wishing to see details of their Loan go to
\n																<a href=\"customerLogin\">Customer Loan Information</a> </p>
\n																<p>(NOTE: you will need your Customer ID and password to access this page)
\n																</p></div>",
"0.00",
"","smtp.vogsphere.co.za","587","vogspesw","fCAnzmz9","tls","d","");

CREATE TABLE `loantype` 
(
  `loantypeid` int(11) NOT NULL AUTO_INCREMENT,
  `loantypedesc` varchar(255) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `currency` varchar(45) DEFAULT 'R',
  `lastchangedby` varchar(255) DEFAULT NULL,
  `lastchangedbydate` date DEFAULT NULL,
  `fromduration` int(11) DEFAULT '1',
  `toduration` int(11) DEFAULT '1',
  `fromamount` decimal(7,2) DEFAULT NULL,
  `toamount` decimal(7,2) DEFAULT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `qualifyfrom` decimal(7,2) DEFAULT '1.00',
  `qualifyto` decimal(7,2) DEFAULT '1.00',
  `funderInterest` DECIMAL(7,4) DEFAULT '1.00',
  PRIMARY KEY (`loantypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8

;INSERT INTO loantype VALUES("1","Personal Loan","20","R","9901019999999","2017-07-01","3","6","500.00","3000.00","2017-07-03","2017-08-31","3000.00","6000.00","1.00");
INSERT INTO loantype VALUES("3","Magadi Loan","60","R","9901019999999","2017-07-01","1","3","100.00","6003.15","2017-07-20","2017-11-24","100.00","6000.00","1.00");
INSERT INTO loantype VALUES("5","Special Loan","90","$","9901019999999","2017-07-02","2","6","200.00","1000.00","2017-07-20","2017-07-29","900.00","800.00","1.00");
INSERT INTO loantype VALUES("6","Batswana","60","P","9901019999999","2017-07-02","1","6","600.00","3000.00","2017-07-19","2017-07-31","300.00","2000.00","1.00");

CREATE TABLE `loanapp` (
  `ApplicationId` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerId` bigint(13) NOT NULL,
  `loantypeid` int(11) NOT NULL,
  `MonthlyIncome` decimal(7,2) DEFAULT NULL,
  `MonthlyExpenditure` decimal(7,2) DEFAULT NULL,
  `TotalAssets` decimal(7,2) DEFAULT NULL,
  `ReqLoadValue` decimal(7,2) DEFAULT NULL,
  `ExecApproval` enum('APPR','PEN','CAN','REJ','SET') DEFAULT 'PEN',
  `DateAccpt` date DEFAULT NULL,
  `StaffId` int(11) DEFAULT NULL,
  `InterestRate` decimal(7,4) NOT NULL,
  `LoanDuration` int(10) unsigned NOT NULL,
  `LoanValue` decimal(7,2) NOT NULL,
  `surety` varchar(45) NOT NULL,
  `Repayment` decimal(7,2) NOT NULL,
  `paymentmethod` varchar(45) NOT NULL,
  `FirstPaymentDate` date NOT NULL,
  `lastPaymentDate` date NOT NULL,
  `monthlypayment` decimal(7,2) NOT NULL,
  `paymentfrequency` varchar(45) DEFAULT NULL,
  `FILEIDDOC` varchar(45) DEFAULT NULL,
  `FILECONTRACT` varchar(45) DEFAULT NULL,
  `FILEBANKSTATEMENT` varchar(45) DEFAULT NULL,
  `FILEPROOFEMP` varchar(45) DEFAULT NULL,
  `FILEFICA` varchar(45) DEFAULT NULL,
  `accountholdername` varchar(45) DEFAULT NULL,
  `bankname` varchar(45) DEFAULT NULL,
  `accountnumber` varchar(45) DEFAULT NULL,
  `branchcode` varchar(45) DEFAULT NULL,
  `accounttype` varchar(45) DEFAULT NULL,
  `funderInterest` DECIMAL(7,4) DEFAULT '1.00',
  `rejectreason` varchar(255) DEFAULT NULL,
  `FILECREDITREP` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`ApplicationId`),
  KEY `loantypeid_idx` (`loantypeid`),
  KEY `customerid_idx` (`CustomerId`),
  CONSTRAINT `customerid` FOREIGN KEY (`CustomerId`) REFERENCES `customer` (`CustomerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `loantypeid` FOREIGN KEY (`loantypeid`) REFERENCES `loantype` (`loantypeid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO  `loanapp`  VALUES("1","8708035205087","1","6000.00","4000.00","0.00","6000.00","APPR","2017-04-15","1","20.0000","2","0.00","Test","7200.00","DTO","2017-04-07","2017-06-07","3600.00","ONC","","","","","","Tumelo Macdonald Modise","bankofathens","4065548100","410506","3","1.00","","");
INSERT INTO  `loanapp`  VALUES("2","8708035205087","1","2000.00","5000.00","0.00","8000.00","APPR","2017-04-15","1","20.0000","36","0.00","Test Golf Gti","9600.00","DTO","2017-05-31","2020-05-01","266.67","MON","","","","","","Tumelo Macdonald Modise","fnb","4065548160","250655","1","1.00","","");
INSERT INTO  `loanapp`  VALUES("3","8708035205087","1","15000.00","6000.00","0.00","2000.00","PEN","2017-04-16","1","20.0000","2","0.00","iPhone 6","2400.00","EFT","2017-04-01","2017-06-01","1200.00","MON","","","","","","Tumelo Macdonald Modise","Investec","4065548160","580105","2","1.00","","");
INSERT INTO  `loanapp`  VALUES("4","8708035205087","1","15.00","6000.00","0.00","2000.00","PEN","2017-04-16","1","20.0000","2","0.00","iPhone 6","2400.00","EFT","2017-04-26","2017-06-26","1200.00","MON","","","","","","Tumelo Macdonald Modise","Investec","4065548160","580105","2","1.00","","");
INSERT INTO  `loanapp`  VALUES("5","8708035205087","1","1000.00","200.00","0.00","1600.00","PEN","2017-04-16","1","20.0000","2","0.00","ssdsd","1920.00","DTO","2017-04-06","2017-06-06","960.00","MON","","","","","","Tumelo Macdonald Modise","capitec","4065548160","470010","2","1.00","","");
INSERT INTO  `loanapp`  VALUES("6","8708035205087","1","6000.00","200.00","0.00","1600.00","PEN","2017-04-16","1","20.0000","2","0.00","ssdsd","1920.00","DTO","2017-04-06","2017-06-06","960.00","MON","","","","","","Tumelo Macdonald Modise","capitec","4065548160","470010","2","1.00","","");
INSERT INTO  `loanapp`  VALUES("7","8708035545057","1","6000.00","7000.00","0.00","3000.00","PEN","2017-04-16","1","20.0000","6","0.00","Tests","3600.00","ONCE","2017-02-01","2017-08-01","600.00","MON","","","","","","Test","nedbank","45454545454","198765","1","1.00","","");
INSERT INTO  `loanapp`  VALUES("8","8708035545057","1","6000.00","7000.00","0.00","3000.00","PEN","2017-04-16","1","20.0000","6","0.00","Tests","3600.00","ONCE","2017-04-04","2017-10-04","600.00","MON","","","","","","Test","nedbank","45454545454","198765","1","1.00","","");

CREATE TABLE `loanappcharges` (
  `loanappchargesid` int(11) NOT NULL AUTO_INCREMENT,
  `chargeid` int(11) DEFAULT NULL,
  `applicationid` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  PRIMARY KEY (`loanappchargesid`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8;

INSERT INTO  `loanappcharges`  VALUES("1","1","7","-1");
INSERT INTO  `loanappcharges`  VALUES("2","2","7","1");
INSERT INTO  `loanappcharges`  VALUES("11","2","7","2");
INSERT INTO  `loanappcharges`  VALUES("12","2","7","3");
INSERT INTO  `loanappcharges`  VALUES("13","2","7","4");
INSERT INTO  `loanappcharges`  VALUES("14","2","7","5");
INSERT INTO  `loanappcharges`  VALUES("15","3","7","1");
INSERT INTO  `loanappcharges`  VALUES("16","3","7","2");
INSERT INTO  `loanappcharges`  VALUES("17","3","7","3");
INSERT INTO  `loanappcharges`  VALUES("18","3","7","4");
INSERT INTO  `loanappcharges`  VALUES("19","3","7","5");
INSERT INTO  `loanappcharges`  VALUES("20","4","7","1");
INSERT INTO  `loanappcharges`  VALUES("21","4","7","2");
INSERT INTO  `loanappcharges`  VALUES("22","4","7","3");
INSERT INTO  `loanappcharges`  VALUES("23","4","7","4");
INSERT INTO  `loanappcharges`  VALUES("24","4","7","5");
INSERT INTO  `loanappcharges`  VALUES("27","3","1","0");
INSERT INTO  `loanappcharges`  VALUES("28","0","1","0");
INSERT INTO  `loanappcharges`  VALUES("29","3","2","0");
INSERT INTO  `loanappcharges`  VALUES("30","1","2","0");
INSERT INTO  `loanappcharges`  VALUES("31","4","2","0");
INSERT INTO  `loanappcharges`  VALUES("32","2","2","0");
INSERT INTO  `loanappcharges`  VALUES("33","3","2","-1");
INSERT INTO  `loanappcharges`  VALUES("34","1","2","-1");
INSERT INTO  `loanappcharges`  VALUES("35","4","2","-1");
INSERT INTO  `loanappcharges`  VALUES("36","2","2","-1");
INSERT INTO  `loanappcharges`  VALUES("37","3","2","1");
INSERT INTO  `loanappcharges`  VALUES("38","1","2","1");
INSERT INTO  `loanappcharges`  VALUES("39","4","2","1");
INSERT INTO  `loanappcharges`  VALUES("40","2","2","1");
INSERT INTO  `loanappcharges`  VALUES("41","3","2","2");
INSERT INTO  `loanappcharges`  VALUES("42","1","2","2");
INSERT INTO  `loanappcharges`  VALUES("43","4","2","2");
INSERT INTO  `loanappcharges`  VALUES("44","2","2","2");
INSERT INTO  `loanappcharges`  VALUES("45","3","2","3");
INSERT INTO  `loanappcharges`  VALUES("46","1","2","3");
INSERT INTO  `loanappcharges`  VALUES("47","4","2","3");
INSERT INTO  `loanappcharges`  VALUES("48","2","2","3");
INSERT INTO  `loanappcharges`  VALUES("49","3","2","4");
INSERT INTO  `loanappcharges`  VALUES("50","1","2","4");
INSERT INTO  `loanappcharges`  VALUES("51","4","2","4");
INSERT INTO  `loanappcharges`  VALUES("52","2","2","4");
INSERT INTO  `loanappcharges`  VALUES("53","3","2","5");
INSERT INTO  `loanappcharges`  VALUES("54","1","2","5");
INSERT INTO  `loanappcharges`  VALUES("55","4","2","5");
INSERT INTO  `loanappcharges`  VALUES("56","2","2","5");
INSERT INTO  `loanappcharges`  VALUES("57","3","2","6");
INSERT INTO  `loanappcharges`  VALUES("58","1","2","6");
INSERT INTO  `loanappcharges`  VALUES("59","4","2","6");
INSERT INTO  `loanappcharges`  VALUES("60","2","2","6");
INSERT INTO  `loanappcharges`  VALUES("61","3","2","7");
INSERT INTO  `loanappcharges`  VALUES("62","1","2","7");
INSERT INTO  `loanappcharges`  VALUES("63","4","2","7");
INSERT INTO  `loanappcharges`  VALUES("64","2","2","7");
INSERT INTO  `loanappcharges`  VALUES("65","3","2","8");
INSERT INTO  `loanappcharges`  VALUES("66","1","2","8");
INSERT INTO  `loanappcharges`  VALUES("67","4","2","8");
INSERT INTO  `loanappcharges`  VALUES("68","2","2","8");
INSERT INTO  `loanappcharges`  VALUES("69","3","2","9");
INSERT INTO  `loanappcharges`  VALUES("70","1","2","9");
INSERT INTO  `loanappcharges`  VALUES("71","4","2","9");
INSERT INTO  `loanappcharges`  VALUES("72","2","2","9");
INSERT INTO  `loanappcharges`  VALUES("73","3","2","10");
INSERT INTO  `loanappcharges`  VALUES("74","1","2","10");
INSERT INTO  `loanappcharges`  VALUES("75","4","2","10");
INSERT INTO  `loanappcharges`  VALUES("76","2","2","10");
INSERT INTO  `loanappcharges`  VALUES("77","3","2","11");
INSERT INTO  `loanappcharges`  VALUES("78","1","2","11");
INSERT INTO  `loanappcharges`  VALUES("79","4","2","11");
INSERT INTO  `loanappcharges`  VALUES("80","2","2","11");
INSERT INTO  `loanappcharges`  VALUES("81","3","2","12");
INSERT INTO  `loanappcharges`  VALUES("82","1","2","12");
INSERT INTO  `loanappcharges`  VALUES("83","4","2","12");
INSERT INTO  `loanappcharges`  VALUES("84","2","2","12");
INSERT INTO  `loanappcharges`  VALUES("85","3","2","13");
INSERT INTO  `loanappcharges`  VALUES("86","1","2","13");
INSERT INTO  `loanappcharges`  VALUES("87","4","2","13");
INSERT INTO  `loanappcharges`  VALUES("88","2","2","13");
INSERT INTO  `loanappcharges`  VALUES("89","3","2","14");
INSERT INTO  `loanappcharges`  VALUES("90","1","2","14");
INSERT INTO  `loanappcharges`  VALUES("91","4","2","14");
INSERT INTO  `loanappcharges`  VALUES("92","2","2","14");
INSERT INTO  `loanappcharges`  VALUES("93","3","2","15");
INSERT INTO  `loanappcharges`  VALUES("94","1","2","15");
INSERT INTO  `loanappcharges`  VALUES("95","4","2","15");
INSERT INTO  `loanappcharges`  VALUES("96","2","2","15");
INSERT INTO  `loanappcharges`  VALUES("97","3","2","16");
INSERT INTO  `loanappcharges`  VALUES("98","1","2","16");
INSERT INTO  `loanappcharges`  VALUES("99","4","2","16");
INSERT INTO  `loanappcharges`  VALUES("100","2","2","16");
INSERT INTO  `loanappcharges`  VALUES("101","3","2","17");
INSERT INTO  `loanappcharges`  VALUES("102","1","2","17");
INSERT INTO  `loanappcharges`  VALUES("103","4","2","17");
INSERT INTO  `loanappcharges`  VALUES("104","2","2","17");
INSERT INTO  `loanappcharges`  VALUES("105","3","2","18");
INSERT INTO  `loanappcharges`  VALUES("106","1","2","18");
INSERT INTO  `loanappcharges`  VALUES("107","4","2","18");
INSERT INTO  `loanappcharges`  VALUES("108","2","2","18");
INSERT INTO  `loanappcharges`  VALUES("109","3","2","19");
INSERT INTO  `loanappcharges`  VALUES("110","1","2","19");
INSERT INTO  `loanappcharges`  VALUES("111","4","2","19");
INSERT INTO  `loanappcharges`  VALUES("112","2","2","19");
INSERT INTO  `loanappcharges`  VALUES("113","3","2","20");
INSERT INTO  `loanappcharges`  VALUES("114","1","2","20");
INSERT INTO  `loanappcharges`  VALUES("115","4","2","20");
INSERT INTO  `loanappcharges`  VALUES("116","2","2","20");
INSERT INTO  `loanappcharges`  VALUES("117","3","2","21");
INSERT INTO  `loanappcharges`  VALUES("118","1","2","21");
INSERT INTO  `loanappcharges`  VALUES("119","4","2","21");
INSERT INTO  `loanappcharges`  VALUES("120","2","2","21");
INSERT INTO  `loanappcharges`  VALUES("121","3","2","22");
INSERT INTO  `loanappcharges`  VALUES("122","1","2","22");
INSERT INTO  `loanappcharges`  VALUES("123","4","2","22");
INSERT INTO  `loanappcharges`  VALUES("124","2","2","22");
INSERT INTO  `loanappcharges`  VALUES("125","3","2","23");
INSERT INTO  `loanappcharges`  VALUES("126","1","2","23");
INSERT INTO  `loanappcharges`  VALUES("127","4","2","23");
INSERT INTO  `loanappcharges`  VALUES("128","2","2","23");
INSERT INTO  `loanappcharges`  VALUES("129","3","2","24");
INSERT INTO  `loanappcharges`  VALUES("130","1","2","24");
INSERT INTO  `loanappcharges`  VALUES("131","4","2","24");
INSERT INTO  `loanappcharges`  VALUES("132","2","2","24");
INSERT INTO  `loanappcharges`  VALUES("133","3","2","25");
INSERT INTO  `loanappcharges`  VALUES("134","1","2","25");
INSERT INTO  `loanappcharges`  VALUES("135","4","2","25");
INSERT INTO  `loanappcharges`  VALUES("136","2","2","25");
INSERT INTO  `loanappcharges`  VALUES("137","3","2","26");
INSERT INTO  `loanappcharges`  VALUES("138","1","2","26");
INSERT INTO  `loanappcharges`  VALUES("139","4","2","26");
INSERT INTO  `loanappcharges`  VALUES("140","2","2","26");
INSERT INTO  `loanappcharges`  VALUES("141","3","2","27");
INSERT INTO  `loanappcharges`  VALUES("142","1","2","27");
INSERT INTO  `loanappcharges`  VALUES("143","4","2","27");
INSERT INTO  `loanappcharges`  VALUES("144","2","2","27");
INSERT INTO  `loanappcharges`  VALUES("145","3","2","28");
INSERT INTO  `loanappcharges`  VALUES("146","1","2","28");
INSERT INTO  `loanappcharges`  VALUES("147","4","2","28");
INSERT INTO  `loanappcharges`  VALUES("148","2","2","28");
INSERT INTO  `loanappcharges`  VALUES("149","3","2","29");
INSERT INTO  `loanappcharges`  VALUES("150","1","2","29");
INSERT INTO  `loanappcharges`  VALUES("151","4","2","29");
INSERT INTO  `loanappcharges`  VALUES("152","2","2","29");
INSERT INTO  `loanappcharges`  VALUES("153","3","2","30");
INSERT INTO  `loanappcharges`  VALUES("154","1","2","30");
INSERT INTO  `loanappcharges`  VALUES("155","4","2","30");
INSERT INTO  `loanappcharges`  VALUES("156","2","2","30");
INSERT INTO  `loanappcharges`  VALUES("157","3","2","31");
INSERT INTO  `loanappcharges`  VALUES("158","1","2","31");
INSERT INTO  `loanappcharges`  VALUES("159","4","2","31");
INSERT INTO  `loanappcharges`  VALUES("160","2","2","31");
INSERT INTO  `loanappcharges`  VALUES("161","3","2","32");
INSERT INTO  `loanappcharges`  VALUES("162","1","2","32");
INSERT INTO  `loanappcharges`  VALUES("163","4","2","32");
INSERT INTO  `loanappcharges`  VALUES("164","2","2","32");
INSERT INTO  `loanappcharges`  VALUES("165","3","2","33");
INSERT INTO  `loanappcharges`  VALUES("166","1","2","33");
INSERT INTO  `loanappcharges`  VALUES("167","4","2","33");
INSERT INTO  `loanappcharges`  VALUES("168","2","2","33");
INSERT INTO  `loanappcharges`  VALUES("169","3","2","34");
INSERT INTO  `loanappcharges`  VALUES("170","1","2","34");
INSERT INTO  `loanappcharges`  VALUES("171","4","2","34");
INSERT INTO  `loanappcharges`  VALUES("172","2","2","34");
INSERT INTO  `loanappcharges`  VALUES("173","3","2","35");
INSERT INTO  `loanappcharges`  VALUES("174","1","2","35");
INSERT INTO  `loanappcharges`  VALUES("175","4","2","35");
INSERT INTO  `loanappcharges`  VALUES("176","2","2","35");
INSERT INTO  `loanappcharges`  VALUES("177","1","1","0");
INSERT INTO  `loanappcharges`  VALUES("178","4","1","0");
INSERT INTO  `loanappcharges`  VALUES("179","2","1","0");
INSERT INTO  `loanappcharges`  VALUES("180","3","1","-1");
INSERT INTO  `loanappcharges`  VALUES("183","3","1","1");
INSERT INTO  `loanappcharges`  VALUES("184","1","1","1");
INSERT INTO  `loanappcharges`  VALUES("185","4","1","1");
INSERT INTO  `loanappcharges`  VALUES("186","2","1","1");
INSERT INTO  `loanappcharges`  VALUES("187","1","7","0");
INSERT INTO  `loanappcharges`  VALUES("188","1","7","1");
INSERT INTO  `loanappcharges`  VALUES("189","1","7","2");
INSERT INTO  `loanappcharges`  VALUES("190","1","7","3");
INSERT INTO  `loanappcharges`  VALUES("191","1","7","4");
INSERT INTO  `loanappcharges`  VALUES("192","1","7","5");
INSERT INTO  `loanappcharges`  VALUES("193","1","8","-1");
INSERT INTO  `loanappcharges`  VALUES("194","3","8","0");
INSERT INTO  `loanappcharges`  VALUES("195","2","8","0");
INSERT INTO  `loanappcharges`  VALUES("196","2","8","-1");
INSERT INTO  `loanappcharges`  VALUES("197","2","8","1");
INSERT INTO  `loanappcharges`  VALUES("198","2","8","2");
INSERT INTO  `loanappcharges`  VALUES("199","2","8","3");
INSERT INTO  `loanappcharges`  VALUES("200","2","8","4");
INSERT INTO  `loanappcharges`  VALUES("201","2","8","5");
INSERT INTO  `loanappcharges`  VALUES("202","2","7","0");
INSERT INTO  `loanappcharges`  VALUES("203","4","7","0");
INSERT INTO  `loanappcharges`  VALUES("204","1","1","-1");


CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount` decimal(53,15) DEFAULT NULL,
  `notes` longtext,
  `FILE` longtext,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`),
  CONSTRAINT `payment_invoice_id_common_id` FOREIGN KEY (`invoice_id`) REFERENCES `common` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9901019989998 DEFAULT CHARSET=utf8;

INSERT INTO  `payment` VALUES("2","4","2016-09-17","600.000000000000000","7","");
INSERT INTO  `payment` VALUES("3","4","2016-09-17","600.000000000000000","7","17-09-2016-18-32-05Mr Thomas Mabitle Loan Agreement.pdf");
INSERT INTO  `payment` VALUES("4","4","2016-09-17","70.000000000000000","7","17-09-2016-18-32-57Obed supporting 2.pdf");
INSERT INTO  `payment` VALUES("5","4","2016-09-17","1300.000000000000000","7","17-09-2016-23-27-29Absa_Pay1.pdf");
INSERT INTO  `payment` VALUES("9901019989997","4","2016-09-17","1400.000000000000000","7","17-09-2016-17-52-36Kea_ID.pdf");



CREATE TABLE `paymentfrequency` (
  `paymentfrequencyid` varchar(45) NOT NULL,
  `paymentfrequencydesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`paymentfrequencyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `paymentfrequency`  VALUES("MON","Monthly");
INSERT INTO  `paymentfrequency`  VALUES("ONC","Once-off");




CREATE TABLE `paymentmethod` (
  `paymentmethodid` varchar(45) NOT NULL,
  `paymentmethoddesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`paymentmethodid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `paymentmethod`  VALUES("DTO","Debit Order");
INSERT INTO  `paymentmethod`  VALUES("EFT","EFT");
INSERT INTO  `paymentmethod`  VALUES("MON","Monthly");
INSERT INTO  `paymentmethod`  VALUES("ONCE","Once-off");

CREATE TABLE `paymentschedule` (
  `ApplicationId` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `scheduleddate` date DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `paidby` varchar(45) DEFAULT NULL,
  `disbursement` decimal(7,2) DEFAULT NULL,
  `amountdue` decimal(7,2) DEFAULT NULL,
  `balance` decimal(7,2) DEFAULT NULL,
  `interestdue` decimal(7,2) DEFAULT NULL,
  `fees` decimal(7,2) DEFAULT NULL,
  `penalty` decimal(7,2) DEFAULT NULL,
  `amountpaid` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`ApplicationId`,`item`),
  CONSTRAINT `fk_appid` FOREIGN KEY (`ApplicationId`) REFERENCES `loanapp` (`ApplicationId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `paymentschedule`  VALUES("1","-1","2017-04-15","0","","6000.00","0.00","0.00","1200.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("1","0","2017-05-07","0","","0.00","3600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("1","1","2017-06-07","0","","0.00","3600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","-1","2017-04-15","0","","8000.00","0.00","0.00","0.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","0","2017-06-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","1","2017-07-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","2","2017-08-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","3","2017-09-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","4","2017-10-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","5","2017-11-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","6","2017-12-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","7","2018-01-30","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","8","2018-02-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","9","2018-03-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","10","2018-04-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","11","2018-05-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","12","2018-06-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","13","2018-07-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","14","2018-08-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","15","2018-09-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","16","2018-10-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","17","2018-11-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","18","2018-12-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","19","2019-01-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","20","2019-02-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","21","2019-03-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","22","2019-04-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","23","2019-05-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","24","2019-06-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","25","2019-07-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","26","2019-08-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","27","2019-09-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","28","2019-10-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","29","2019-11-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","30","2019-12-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","31","2020-01-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","32","2020-02-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","33","2020-03-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","34","2020-04-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("2","35","2020-05-28","0","","0.00","266.67","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("3","-1","2017-04-16","0","","2000.00","0.00","0.00","0.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("3","0","2017-05-01","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("3","1","2017-06-01","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("4","-1","2017-04-16","0","","2000.00","0.00","0.00","400.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("4","0","2017-05-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("4","1","2017-06-26","0","","0.00","1200.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("5","-1","2017-04-16","0","","1600.00","0.00","0.00","0.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("5","0","2017-05-06","0","","0.00","960.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("5","1","2017-06-06","0","","0.00","960.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("6","-1","2017-04-16","0","","1600.00","0.00","0.00","320.00","42.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("6","0","2017-05-06","0","","0.00","960.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("6","1","2017-06-06","0","","0.00","960.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","-1","2017-04-16","0","","3000.00","0.00","0.00","0.00","43.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","0","2017-03-01","22","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","1","2017-04-01","22","","0.00","600.00","0.00","100.00","20.00","22.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","2","2017-05-01","21","","0.00","600.00","0.00","100.00","20.00","21.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","3","2017-06-01","0","","0.00","600.00","0.00","100.00","20.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","4","2017-07-01","0","","0.00","600.00","0.00","100.00","20.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("7","5","2017-08-01","0","","0.00","600.00","0.00","100.00","20.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","-1","2017-04-16","0","","3000.00","0.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","0","2017-05-04","18","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","1","2017-06-04","0","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","2","2017-07-04","0","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","3","2017-08-04","0","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","4","2017-09-04","0","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");
INSERT INTO  `paymentschedule`  VALUES("8","5","2017-10-04","0","","0.00","600.00","0.00","0.00","0.00","0.00","0.00");




CREATE TABLE `province` (
  `provinceid` varchar(45) NOT NULL DEFAULT '',
  `provincename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`provinceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO  `province`  VALUES("EC","Eastern Cape");
INSERT INTO  `province`  VALUES("FS","Free State");
INSERT INTO  `province`  VALUES("GP","Gauteng");
INSERT INTO  `province`  VALUES("LP","Limpopo");
INSERT INTO  `province`  VALUES("MP","Mpumalanga");
INSERT INTO  `province`  VALUES("NC","Northern Cape");
INSERT INTO  `province`  VALUES("NW","North West");
INSERT INTO  `province`  VALUES("WC","Western Cape");



CREATE TABLE `series` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `first_number` int(11) DEFAULT '1',
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



