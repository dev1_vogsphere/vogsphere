
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="uploadResult" type="{http://schemas.datacontract.org/2004/07/mebmsft}uploadResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uploadResult"
})
@XmlRootElement(name = "uploadResponse")
public class UploadResponse {

    @XmlElementRef(name = "uploadResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse> uploadResult;

    /**
     * Gets the value of the uploadResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link org.datacontract.schemas._2004._07.mebmsft.UploadResponse }{@code >}
     *     
     */
    public JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse> getUploadResult() {
        return uploadResult;
    }

    /**
     * Sets the value of the uploadResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link org.datacontract.schemas._2004._07.mebmsft.UploadResponse }{@code >}
     *     
     */
    public void setUploadResult(JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse> value) {
        this.uploadResult = ((JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse> ) value);
    }

}
