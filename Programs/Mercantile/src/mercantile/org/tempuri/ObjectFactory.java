
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DownloadResponseDownloadResult_QNAME = new QName("http://tempuri.org/", "downloadResult");
    private final static QName _UploadResponseUploadResult_QNAME = new QName("http://tempuri.org/", "uploadResult");
    private final static QName _UploadFootPrint_QNAME = new QName("http://tempuri.org/", "footPrint");
    private final static QName _UploadUserInst_QNAME = new QName("http://tempuri.org/", "userInst");
    private final static QName _UploadInMessage_QNAME = new QName("http://tempuri.org/", "inMessage");
    private final static QName _UploadPassWord_QNAME = new QName("http://tempuri.org/", "passWord");
    private final static QName _UploadUserName_QNAME = new QName("http://tempuri.org/", "userName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link org.tempuri.DownloadResponse }
     * 
     */
    public org.tempuri.DownloadResponse createDownloadResponse() {
        return new org.tempuri.DownloadResponse();
    }

    /**
     * Create an instance of {@link org.tempuri.UploadResponse }
     * 
     */
    public org.tempuri.UploadResponse createUploadResponse() {
        return new org.tempuri.UploadResponse();
    }

    /**
     * Create an instance of {@link Upload }
     * 
     */
    public Upload createUpload() {
        return new Upload();
    }

    /**
     * Create an instance of {@link Download }
     * 
     */
    public Download createDownload() {
        return new Download();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link org.datacontract.schemas._2004._07.mebmsft.DownloadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "downloadResult", scope = org.tempuri.DownloadResponse.class)
    public JAXBElement<org.datacontract.schemas._2004._07.mebmsft.DownloadResponse> createDownloadResponseDownloadResult(org.datacontract.schemas._2004._07.mebmsft.DownloadResponse value) {
        return new JAXBElement<org.datacontract.schemas._2004._07.mebmsft.DownloadResponse>(_DownloadResponseDownloadResult_QNAME, org.datacontract.schemas._2004._07.mebmsft.DownloadResponse.class, org.tempuri.DownloadResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link org.datacontract.schemas._2004._07.mebmsft.UploadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "uploadResult", scope = org.tempuri.UploadResponse.class)
    public JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse> createUploadResponseUploadResult(org.datacontract.schemas._2004._07.mebmsft.UploadResponse value) {
        return new JAXBElement<org.datacontract.schemas._2004._07.mebmsft.UploadResponse>(_UploadResponseUploadResult_QNAME, org.datacontract.schemas._2004._07.mebmsft.UploadResponse.class, org.tempuri.UploadResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "footPrint", scope = Upload.class)
    public JAXBElement<String> createUploadFootPrint(String value) {
        return new JAXBElement<String>(_UploadFootPrint_QNAME, String.class, Upload.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userInst", scope = Upload.class)
    public JAXBElement<String> createUploadUserInst(String value) {
        return new JAXBElement<String>(_UploadUserInst_QNAME, String.class, Upload.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "inMessage", scope = Upload.class)
    public JAXBElement<String> createUploadInMessage(String value) {
        return new JAXBElement<String>(_UploadInMessage_QNAME, String.class, Upload.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "passWord", scope = Upload.class)
    public JAXBElement<String> createUploadPassWord(String value) {
        return new JAXBElement<String>(_UploadPassWord_QNAME, String.class, Upload.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userName", scope = Upload.class)
    public JAXBElement<String> createUploadUserName(String value) {
        return new JAXBElement<String>(_UploadUserName_QNAME, String.class, Upload.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "footPrint", scope = Download.class)
    public JAXBElement<String> createDownloadFootPrint(String value) {
        return new JAXBElement<String>(_UploadFootPrint_QNAME, String.class, Download.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userInst", scope = Download.class)
    public JAXBElement<String> createDownloadUserInst(String value) {
        return new JAXBElement<String>(_UploadUserInst_QNAME, String.class, Download.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "passWord", scope = Download.class)
    public JAXBElement<String> createDownloadPassWord(String value) {
        return new JAXBElement<String>(_UploadPassWord_QNAME, String.class, Download.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "userName", scope = Download.class)
    public JAXBElement<String> createDownloadUserName(String value) {
        return new JAXBElement<String>(_UploadUserName_QNAME, String.class, Download.class, value);
    }

}
