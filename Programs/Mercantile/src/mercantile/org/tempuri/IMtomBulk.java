
package org.tempuri;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import org.datacontract.schemas._2004._07.mebmsft.DownloadResponse;
import org.datacontract.schemas._2004._07.mebmsft.UploadResponse;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2-hudson-752-
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IMtomBulk", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    org.tempuri.ObjectFactory.class,
    org.datacontract.schemas._2004._07.mebmsft.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class
})
public interface IMtomBulk {


    /**
     * 
     * @param userInst
     * @param footPrint
     * @param userName
     * @param passWord
     * @return
     *     returns org.datacontract.schemas._2004._07.mebmsft.DownloadResponse
     */
    @WebMethod(action = "http://tempuri.org/IMtomBulk/download")
    @WebResult(name = "downloadResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "download", targetNamespace = "http://tempuri.org/", className = "org.tempuri.Download")
    @ResponseWrapper(localName = "downloadResponse", targetNamespace = "http://tempuri.org/", className = "org.tempuri.DownloadResponse")
    public DownloadResponse download(
        @WebParam(name = "userName", targetNamespace = "http://tempuri.org/")
        String userName,
        @WebParam(name = "userInst", targetNamespace = "http://tempuri.org/")
        String userInst,
        @WebParam(name = "passWord", targetNamespace = "http://tempuri.org/")
        String passWord,
        @WebParam(name = "footPrint", targetNamespace = "http://tempuri.org/")
        String footPrint);

    /**
     * 
     * @param inMessage
     * @param userInst
     * @param footPrint
     * @param userName
     * @param passWord
     * @return
     *     returns org.datacontract.schemas._2004._07.mebmsft.UploadResponse
     */
    @WebMethod(action = "http://tempuri.org/IMtomBulk/upload")
    @WebResult(name = "uploadResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "upload", targetNamespace = "http://tempuri.org/", className = "org.tempuri.Upload")
    @ResponseWrapper(localName = "uploadResponse", targetNamespace = "http://tempuri.org/", className = "org.tempuri.UploadResponse")
    public UploadResponse upload(
        @WebParam(name = "inMessage", targetNamespace = "http://tempuri.org/")
        String inMessage,
        @WebParam(name = "userName", targetNamespace = "http://tempuri.org/")
        String userName,
        @WebParam(name = "userInst", targetNamespace = "http://tempuri.org/")
        String userInst,
        @WebParam(name = "passWord", targetNamespace = "http://tempuri.org/")
        String passWord,
        @WebParam(name = "footPrint", targetNamespace = "http://tempuri.org/")
        String footPrint);

}
