<?php
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

if(!function_exists('updated_beneficiary'))
{
	function updated_beneficiary($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		////////////////////////////////////////////////////////////////////////

		$message = '';
		try
		{
		$sql = "UPDATE benefiary
		SET
		Account_Holder = ?,
		Account_Type = ?,
		Account_Number = ?,
		Branch_Code = ?,
		Bank_Reference = ?,
		Client_Reference_1 = ?,
		Notify = ?,
		changedby = ?,
		changedon = ?,
		changedat = ?,
		benefiarycontactnumber = ?,
		benefiaryemail = ?	,
		Initials = ?		
		WHERE benefiaryid = ?;";
		$q = $pdo->prepare($sql);
		$q->execute(array(
		$dataFilter['Account_Holder'],		
		$dataFilter['Account_Type'],
		$dataFilter['Account_Number'],
		$dataFilter['Branch_Code'],
		$dataFilter['Bank_Reference'],
		$dataFilter['Client_Reference_1'],
		$dataFilter['Notify'],
		$dataFilter['changedby'],
		$dataFilter['changedon'],
		$dataFilter['changedat'],
		$dataFilter['benefiarycontactnumber'],
		$dataFilter['benefiaryemail'],
		$dataFilter['Initials'],
		$dataFilter['benefiaryid']));
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		$message = 'Caught exception: '.  $e->getMessage(). "\n";
	  }
	  return $message;
   }
}
////////////////////////////////////////////////////////////////////////
$list = getpost('list');
$list['changedon'] = date('Y-m-d');
$list['changedat'] = date('h:m:s');

////////////////////////////////////////////////////////////////////////
$message = updated_beneficiary($list);
if(empty($message))
{
	 $message = 'updated successfully';
}
echo $message;
?>