<?php
/*
Rules :
Get the debit order with below conditions:
Rule 1 - Auto SMS for actiondate that is due in 5 days.????
	   - Today date +5 days to get action_date to search collection table.
Rule 2 - Status_Description in ('pending') in a list current 'pending'
Rule 3 - susbscribed for Notification = 'Yes' 
Rule 4 - phone number not blank
Rule 5 - check on table api_response if url = collection~reference_1 and usercode = collection~usercode and amount = collection~amount
	   - has not been sent this month for e.g. 2019-12
*/
$title='script_auto_sms_billinstruction';
// -- Declarations
require 'database.php';
$message = '';
$arrayInstruction = null;
$collection_data = null;
// -- Dynamically API for data collection.
$tablename = 'collection';
$dbnameobj = 'ecashpdq_paycollections';
$queryFields = null;
$whereArray = null;								
$statusList = "('pending')"; // -- 'pending'
$today = date('Y-m-d');
// Due_ActionDate - Today's date plus 5 days
$due_actiondate = date('Y-m-d', strtotime(' + 5 days')); 
$notify = "('Yes')";
$cellphone = '';
$collection_amount = '';

// -- Payment Run Count
if(!function_exists('paymentRunCount'))	
{
	function paymentRunCount($accountnumber)
	{
			$paymentRunCount = 0;
			$dbnameobj 	 = 'ecashpdq_paycollections';
			$queryFields = null;
			$whereArray  = null;
			$whereArray[]  = "Client_Reference_1 = '{$accountnumber}'";  // -- Action Date.
			//$whereArray[]  = "Action_Date = '{$actiondate}'"; 			 // -- Client Reference.
			$queryFields[] = '*';
			$collectionObj = search_dynamic($dbnameobj,'collection',$whereArray,$queryFields);
			if($collectionObj->rowCount() > 0)
			{
				$paymentRunCount = $collectionObj->rowCount();
			}
			
			return $paymentRunCount;
	}
}
// -- check response queue - BOC for a month atleast 2 SMSs.
if(!function_exists('check_response_duplicates'))
{
	function check_response_duplicates($notification)
	{
		try 
		{
		$tab_name  = 'api_response';
		$tab_nameQ = 'api_responsequeue';
		$dbnameobj = 'ecashpdq_eloan';
		
		$usercode 	 = '';
		$reference_1 = '';
		$month 		 = date('Y-m');
		$cellphone   = '';
		$collection_amount = '';
		
		if(isset($notification['usercode'])){$usercode   = $notification['usercode'];}
		if(isset($notification['reference_1'])){$reference_1   = $notification['reference_1'];}
		if(isset($notification['phone'])){$cellphone   = $notification['phone'];}		
		if(isset($notification['amount'])){$collection_amount   = $notification['amount'];}		

		$queryFields = null;
		$whereArray = null;
		$queryFields[] = '*';
		$whereArray[] = "usercode = '{$usercode}'";
		$whereArray[] = "phone = '{$cellphone}'";
		$whereArray[] = "url = '{$reference_1}'";
		$whereArray[] = "amount = '{$collection_amount}'";
		$whereArray[] = "date LIKE '%{$month}%'";
		
		//print_r($whereArray);
// -- response Queue
$SMSs = 0;
		$smsObj = search_dynamic($dbnameobj,$tab_nameQ,$whereArray,$queryFields);
		$SMSs = $smsObj->rowCount();

// -- responses		
		$smsObj = search_dynamic($dbnameobj,$tab_name,$whereArray,$queryFields);
		$SMSs = $SMSs + $smsObj->rowCount();

		return $SMSs;
		//--					
		} 
		catch (PDOException $e)
		{
		echo $e->getMessage();
		}
	}
}
// -- check response queue - EOC

if(!function_exists('NextActionDate'))
{
	// -- Next Action Date.
function NextActionDate($Actiondate)
{
	$day = substr($Actiondate,-2);
	//$month = substr($Actiondate,5,2) + 1;
	//$year = substr($Actiondate,0,4);
	$NextActiondate = '';
	$daysInMonth = 0;
	
	// -- Get the current day, month, year.
	$cyear = date('Y');
	$cmonth = date('m');
	$cday = date('d');
	
	$month  = $cmonth;
	$year   = $cyear;
	
	// -- if the Next Month is greater than 12, we going to the next year. 
	if($month > 12)
	{
	  $year = $year + 1;
	  $month = 1;
	}
	
	// -- Get the days in a month.
	$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year );
	
	// -- Ensure the days dont get confused.
	if($day > $daysInMonth)
	{
	  $day = $daysInMonth;
	}
	
	// -- if the month is less than 10 add 0.
	if($month < 10)
	{
	   $month = "0".$month;
	}

	$NextActiondate = $year.'-'.$month.'-'.$day;
		
	return $NextActiondate;
}
}
?>
<div class="row">
  <div class="table-responsive">
	<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
	  <tr>
					  <td>Feedback</td>
					  <td>Client_Id</td>
					  <td>Client_Reference_1</td>
					  <td>Client_Reference_2</td>
					  <td>Client_ID_Type</td>
					  <td>Client_ID_No 	</td>
					  <td>Initials 		</td>
					  <td>Account_Holder</td>
					  <td>Account_Type 	</td>
					  <td>Account_Number</td>
					  <td>Branch_Code 	</td>
					  <td>No_Payments 	</td>
					  <td>Service_Type 	</td>					
					  <td>Service_Mode 	</td>
					  <td>Frequency 	</td>
					  <td>Action_Date 	</td>
					  <td>Bank_Reference</td>
					  <td>Amount 	    </td>
					  <td>Phone number  </td>
					  <td>Notification  </td>
		</tr>					  
				<?php 
				$arrayCollections = null;
				$whereArray = null;	
				$UserCode  = '';
				 
				$whereArray[] = "Action_Date = '{$due_actiondate }'"; // -- Due in 5 days from today.
				$whereArray[] = "Status_Description IN {$statusList}"; // -- pending status.
				$whereArray[] = "Notify IN {$notify}"; // -- Notify is Yes.
				
				$queryFields[] = '*';
				$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
				//print_r($collectionObj);
				if(empty($collectionObj))
				{
					$message = 'no record found to update';
					echo"<tr><td>".$message."</td><td></tr>";
				}
				else
				{
					foreach($collectionObj as $row)
					{	$valid = true;
						$message = '';
						if(isset($row['Client_Id']		   )){$arrayInstruction[1] =  $row['Client_Id']		;}else{$arrayInstruction[1] = '';}
						if(isset($row['Client_Reference_1'])){$arrayInstruction[2] =  $row['Client_Reference_1'];}else{$arrayInstruction[2] = '';}						
						if(isset($row['Client_Reference_2'])){$arrayInstruction[3] =  $row['Client_Reference_2'];}else{$arrayInstruction[3] = '';}
						if(isset($row['Client_ID_Type']    )){$arrayInstruction[4] =  $row['Client_ID_Type']    ;}else{$arrayInstruction[4] = '';}
						if(isset($row['Client_ID_No']      )){$arrayInstruction[5] =  $row['Client_ID_No']      ;}else{$arrayInstruction[5] = '';}
						if(isset($row['Initials']		  )) {$arrayInstruction[6] =  $row['Initials']		  ;  }else{$arrayInstruction[6] = '';}
						if(isset($row['Account_Holder']    )){$arrayInstruction[7] =  $row['Account_Holder']    ;}else{$arrayInstruction[7] = '';} 
						if(isset($row['Account_Type']      )){$arrayInstruction[8] =  $row['Account_Type']      ;}else{$arrayInstruction[8] = '';} 
						if(isset($row['Account_Number']    )){$arrayInstruction[9] =  $row['Account_Number']    ;}else{$arrayInstruction[9] = '';} 
						if(isset($row['Branch_Code']       )){$arrayInstruction[10] = $row['Branch_Code']       ;}else{$arrayInstruction[10] ='';} 
						if(isset($row['No_Payments']       )){$arrayInstruction[11] = $row['No_Payments'];/*'99';*/}else{$arrayInstruction[11] ='';} 
						if(isset($row['Service_Type']      )){$arrayInstruction[12] = $row['Service_Type']      ;}else{$arrayInstruction[12] ='';} 
						if(isset($row['Service_Mode']      )){$arrayInstruction[13] = $row['Service_Mode']      ;}else{$arrayInstruction[13] ='';} 
						if(isset($row['Frequency']         )){$arrayInstruction[14] = $row['Frequency']         ;}else{$arrayInstruction[14] ='';} 
						if(isset($row['Action_Date']       )){$arrayInstruction[15] = $row['Action_Date'];}else{$arrayInstruction[15] ='';} 
						if(isset($row['Bank_Reference']    )){$arrayInstruction[16] = $row['Bank_Reference']    ;}else{$arrayInstruction[16] ='';} 
						if(isset($row['Amount']            )){$arrayInstruction[17] = $row['Amount']            ;}else{$arrayInstruction[17] ='';} 
						$arrayInstruction[18] = '1';//$keydata['Status_Code']; 									
						$arrayInstruction[19] = 'pending';//$keydata['Status_Description']; 							
						$arrayInstruction[20] = '';//$keydata['mandateid']; 										
						$arrayInstruction[21] = '';//$keydata['mandateitem']; 
						if(isset($row['UserCode']          )){$arrayInstruction[22] = $row['UserCode']          ;}else{$arrayInstruction[22] ='';} 
						if(isset($row['IntUserCode']       )){$arrayInstruction[23] = $row['IntUserCode']       ;}else{$arrayInstruction[23] ='';} 

						if(isset($row['entry_class'])){$arrayInstruction[24] = $row['entry_class'];}else{$arrayInstruction[24] = 0;} 										
						if(isset($row['Contact_No'])){$arrayInstruction[25] = $row['Contact_No'];}else{$arrayInstruction[25] = '';} 										
						if(isset($row['Notify'])){$arrayInstruction[26] = $row['Notify'];}else{$arrayInstruction[26] = '';} 										

						$collection_amount = $arrayInstruction[17];
						$UserCode = $arrayInstruction[22];
						// -- Rule #1 if payment is zero or empty (it means its an infinite run until otherwise instructed.), else check total count of run against
						// -- number of payment runs
						/*if(empty($row['No_Payments']) or $row['No_Payments'] == '0')
						{
							// -- initialise to zero
							$arrayInstruction[11] = '00';
						}
						else
						{
							$payruncount = paymentRunCount($row['Client_Reference_2']);
							if( $payruncount >= $row['No_Payments'] )
							{
								$message = 'Reached limit of total number of payments :'.$row['No_Payments'].' by '.$payruncount;
								$valid = false;
							}
						}*/
						// -- Check if Action Date is populated.
						if(!empty($arrayInstruction[15]))
						{ 
							// -- do nothing
						}
						else{$message = 'no actiondate'; $valid = false;}
						
						// -- Check if notification is Yes
						if($arrayInstruction[26] == 'Yes')
						{	// -- no phone numbefr validation
							if(!empty($arrayInstruction[25]))
							{
								// -- sent sms - BOC
									$smstemplatename = 'debitorder_remainder'; // -- debit order remainder
									$applicationid = 0;		
									$Action_Date = $arrayInstruction[15];			
									$Reference_1 = $arrayInstruction[2];
									$userid = '9901019999999'; // -- batch auto scheduler user.
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{	
										$notification['phone'] 		 = $arrayInstruction[25];
										$notification['content'] 	 = $contents;
										$notification['reference_1'] = $arrayInstruction[2];	
										$notification['usercode'] 	 = $UserCode;
										$notification['createdby'] 	 = $userid;
										$notification['amount'] 	 = $collection_amount;

										// -- remainder
										$notificationsList[] = $notification;	// add into list
										
										// -- check for duplicates - atleast 1SMSs
										if(check_response_duplicates($notification) >= 1)
										{
											$message = 'Duplicate SMS for '.$notification['phone'].'; ref :'.$notification['reference_1'];
										}
										else
										{
										// -- notify clients who signed for notifications(queue)
										 if(!empty($notificationsList)){auto_sms_queue($notificationsList); $message = 'sms queued';}
										}
									}
									else
									{$message = 'Unknown SMS Template : '.$smstemplatename; }
								// -- sent sms - EOC
							}
							else{$message = 'no phone number.'; $valid = false;}
						}
						// -- exclude
						else
						{$valid = false;}
						
						// -- if its valid auto sent sms notification for billing instruction.
						if ($valid)
						{
							// -- $message = auto_create_billinstruction($arrayInstruction);
						   if(empty($message)){$message = 'success';}	
						   echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td><td>".$arrayInstruction[25]."</td><td>".$arrayInstruction[26]."</td></tr>";						
						}
					}			
				}
				//echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";
				?>	  
	<table>
   </div>	
</div>   
