/*------------------------------------------------------------------------------------------------------->
<!------------------------------ Debit Order Schedule 			  --------------------------------------->
<!-------------------------------------------------------------------------------------------------------*/
function cashpaymentschedule(href) 
{ 
    var debitorderschedule = document.getElementById("divdebitorderschedule"); 

			var i, title; 
		
			ReferenceNumber  = null;
			AccountHolder	 = null;
			BranchCode	     = null;
			AccountNumber	 = null;
			Servicetype	 	 = null;
			ServiceMode	 	 = null;
			AccountType	 	 = null;
			displayData      = null;
			
			indexofEqual = 0;
			MailArray = '';
			indexCount = 0;
			var Message = '';
		
			str  = href;
			Startpostion = href.indexOf("customerid");
			
			urlString = href.substr(Startpostion); 
			var res = urlString.split("&");
			
			customerid 				= res[0].substr(res[0].indexOf("=") + 1);
			ApplicationId	    	= res[1].substr(res[1].indexOf("=") + 1);
			ReferenceNumber			= res[2].substr(res[2].indexOf("=") + 1);
			AccountHolder 			= res[3].substr(res[3].indexOf("=") + 1);
			BranchCode 				= res[4].substr(res[4].indexOf("=") + 1);
			AccountNumber 			= res[5].substr(res[5].indexOf("=") + 1);
			Servicetype 			= res[6].substr(res[6].indexOf("=") + 1);
			ServiceMode 			= res[7].substr(res[7].indexOf("=") + 1);
			AccountType 			= res[8].substr(res[8].indexOf("=") + 1);
			frequency               = res[9].substr(res[9].indexOf("=") + 1);
			firstname				= res[10].substr(res[10].indexOf("=") + 1);
			lastname				= res[11].substr(res[11].indexOf("=") + 1);
			initials = firstname.substr(0,1);
			//alert(initials+" "+frequency+" "+firstname+" "+lastname);

    if (href) 
    { 
	//alert(href);
	jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"cashpaymentschedule.php",
					data:{customerid:customerid,ApplicationId:ApplicationId,
						  ReferenceNumber:ReferenceNumber,AccountHolder:AccountHolder,
						  BranchCode:BranchCode,AccountNumber:AccountNumber,
						  Servicetype:Servicetype,ServiceMode:ServiceMode,
						  AccountType:AccountType,initials:initials,frequency:frequency},
					success: function(data)
					{
					  Message = Message+"\n<p>Debit Orders Schedule Successful created!</p>";
					  //alert(data);
					 
					 jQuery("#divcashpaymentschedule").html(data);
					 
					 //--jQuery("#divSuccess").append(Message);
					 	//	move();
				    }
				});
			});
	}
}
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");   
		  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();*/
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}	