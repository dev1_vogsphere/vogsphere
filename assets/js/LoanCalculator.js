// JavaScript Document
var p = {

    0: "100",
    1: "150",
    2: "200",
    3: "250",
    4: "300",
    5: "350",
    6: "400",
    7: "450",
    8: "500",
    9: "550",
    10: "600",
    11: "650",
    12: "700",
    13: "750",
    14: "800",
    15: "850",
    16: "900",
    17: "950",
    18: "1,000",
    19: "1,100",
    20: "1,200",
    21: "1,300",
    22: "1,400",
    23: "1,500",
    24: "1,600",
    25: "1,700",
    26: "1,800",
    27: "19,00",
    28: "2,000",
    29: "2,100",
    30: "2,200",
    31: "2,300",
    32: "2,400",
    33: "2,500",
	34: "2,600",
    35: "2,700",
    36: "2,800",    
	37: "2,900",
    38: "3,000",

};

var t = {

    0: "100",
    1: "150",
    2: "200",
    3: "250",
    4: "300",
    5: "350",
    6: "400",
    7: "450",
    8: "500",
    9: "550",
    10: "600",
    11: "650",
    12: "700",
    13: "750",
    14: "800",
    15: "850",
    16: "900",
    17: "950",
    18: "1000",
    19: "1100",
    20: "1200",
    21: "1300",
    22: "1400",
    23: "1500",
    24: "1600",
    25: "1700",
    26: "1800",
    27: "1900",
    28: "2000",
	29: "2100",
    30: "2200",
    31: "2300",
    32: "2400",
    33: "2500",
    34: "2600",
    35: "2700",
    36: "2800",
    37: "2900",
    38: "3000",
}

var obj = {
    '1month' : {
        'monthly' : '1'
    },
    '2month' : {
        'monthly' : '2'
    },
    '3month' : {
        'monthly' : '3'
    }
};

$(document).ready(function() {

    $("#total").val("10000");

    
    var $myDiv = $('#slider_amirol');
if($myDiv.length)
{
    $("#slider_amirol").slider({
        range: "min",
        animate: true,

        min: 0,
        max: 38,
        step: 1,
        slide: 
            function(event, ui) 
            {
                update(1,ui.value); //changed
                calcualtePrice(ui.value);
            }
    });
}
    $('.month').on('click',function(event) {
        var id = $(this).attr('id');

        $('.month').removeClass('selected-month');
        $(this).addClass('selected-month');
        $(".month").removeClass("active-month");
        $(this).addClass("active-month");

        $('#month').val(id);

        calcualtePrice()
    });

    $('.term').on('click',function(event) {
        var id = $(this).attr('id');

        $('.term').removeClass('selected-term');
        $(this).addClass('selected-term');
        $(".term").removeClass("active-term");
        $(this).addClass("active-term");
        $('#term').val(id);

        calcualtePrice()
    });

if($myDiv.length)
{update();
calcualtePrice();}

});
        

        
function update(slider,val) {

    if(undefined === val) val = 0;
    var amount = p[val];

    $('#sliderVal').val(val);

    $('#slider_amirol a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
}

function calcualtePrice(val){
    
    if(undefined === val)
        val = $('#sliderVal').val();

    var month = $('#month').val();
    var term = obj[month][$('#term').val()];

    var totalPrice = t[val]*1;//*term
	var interest = totalPrice.toFixed(2) * 0.25;// -- 0.25 = 25%
	totalPrice = (parseFloat(totalPrice) + parseFloat(interest)).toFixed(2) / term;
	
//alert("month = "+month+"Term ="+term+"Value = "+t[val]+"Price = "+totalPrice+"Interest = "+interest);

    $("#total12").val(totalPrice.toFixed(2));
   // $("#total12").val(Math.round((totalPrice)/12).toFixed(2));
   // $("#total52").val(Math.round((totalPrice)/52).toFixed(2));
}