
/* Portfolio */
$(window).load(function() {
    var $cont = $('.portfolio-group');
    
    $cont.isotope({
        itemSelector: '.portfolio-group .portfolio-item',
        masonry: {columnWidth: $('.isotope-item:first').width(), gutterWidth: -20, isFitWidth: true},
        filter: '*',
    });

    $('.portfolio-filter-container a').click(function() {
        $cont.isotope({
            filter: this.getAttribute('data-filter')
        });

        return false;
    });

    var lastClickFilter = null;
    $('.portfolio-filter a').click(function() {

        //first clicked we don't know which element is selected last time
        if (lastClickFilter === null) {
            $('.portfolio-filter a').removeClass('portfolio-selected');
        }
        else {
            $(lastClickFilter).removeClass('portfolio-selected');
        }

        lastClickFilter = this;
        $(this).addClass('portfolio-selected');
    });

});

/* Image Hover  - Add hover class on hover */
$(document).ready(function(){
    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".image-hover figure").click(function(e){
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".image-hover figure").hasClass("hover")) {
                $(this).closest(".image-hover figure").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".image-hover figure").mouseenter(function(){
            $(this).addClass("hover");
        })
        // handle the mouseleave functionality
        .mouseleave(function(){
            $(this).removeClass("hover");
        });
    }
});

// thumbs animations
$(function () {
    
    $(".thumbs-gallery i").animate({
             opacity: 0
    
          }, {
             duration: 300,
             queue: false
          });

   $(".thumbs-gallery").parent().hover(
       function () {},
       function () {
          $(".thumbs-gallery i").animate({
             opacity: 0
          }, {
             duration: 300,
             queue: false
          });
   });
 
   $(".thumbs-gallery i").hover(
      function () {
          $(this).animate({
             opacity: 0
    
          }, {
             duration: 300,
             queue: false
          });

          $(".thumbs-gallery i").not( $(this) ).animate({
             opacity: 0.4         }, {
             duration: 300,
             queue: false
          });
      }, function () {
      }
   );

});

// Mobile Menu
    $(function(){
        $('#hornavmenu').slicknav();
        $( "div.slicknav_menu" ).addClass( "hidden-lg" );
    });

// Sticky Div
  $(window).load(function(){
    $("#hornav").sticky({ topSpacing: 0 });
  });
  
  
  function CalculateRepayment() 
  {
  // Required Amount
	 required_amnt = parseFloat(document.getElementById("ReqLoadValue").value);
  
  	var duration = parseInt(document.getElementById("LoanDuration").value); 

  // Monthly Payment Amount
	 monthlypayment = 0.00;
  
  // Get the Required Loan Amount Multiple by 0.2(20%)
     repayment_amnt = (required_amnt * 0.2) +  required_amnt;
	 document.getElementById("Repayment").value = repayment_amnt;
	 
	 if(duration > 0 && repayment_amnt > 0)
	 {
		 monthlypayment = repayment_amnt / duration;
		 document.getElementById("monthlypayment").value = monthlypayment;
	}
  }
  
  function LastPaymentDate() 
  {
    var lastdate = new Date();	

	var firstdate = toDate(""+document.getElementById("FirstPaymentDate").value+"");

	var duration = parseInt(document.getElementById("LoanDuration").value); 

	var month = firstdate.getMonth() + 1;

	var day = firstdate.getDate();
	
	var year;
	var  i = 0;

	while(i < duration)	
	{
	  firstdate.setMonth(month);
	  month = month + 1;
	  i = i + 1;
	}
	
	day = firstdate.getDate();
	month = firstdate.getMonth()+1;
	year = firstdate.getFullYear();
	
	if(month < 10)
	{
	  month = "0"+month;
	}	
	document.getElementById("lastPaymentDate").value = year+"-"+ month +"-"+day;
	
	CalculateRepayment();
  }
  
  function toDate(dateStr) 
  {
    //var parts = dateStr.split("-");
    return new Date(dateStr);
  }
  
  function formatDate(value)
  {
   return  value.getYear() + "-"+ (value.getMonth()+1) +"-"+ value.getDate();
  }

  function CreateUserID()
  {
	// Get customer id/password 
    var ID =  document.getElementById("idnumber").value;
	
	// Populate the UserID
    document.getElementById("Userid").value = ID;
  }



