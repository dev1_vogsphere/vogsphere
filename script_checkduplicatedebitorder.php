<?php
require 'database.php';
if(!function_exists('get_bill_single_instruction'))
{
	function get_bill_single_instruction($dataFilter)
	{
		// -- check with benefiaryid,Client_Reference_1, Account_Number,Action_Date,IntUserCode.
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'collection';			
		$sql = "select * from $tbl where 
		Action_Date  = ? and Client_Reference_1 = ? and IntUserCode = ? and 
		Account_Number = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['Action_Date'],
						  $dataFilter['Client_Reference_1'],$dataFilter['IntUserCode'],
						  $dataFilter['Account_Number']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
$message = '';
$list = getpost('list');

$databill_single_instructionObj = null;
// -- Check bill_single_instruction details
$databill_single_instructionObj = get_bill_single_instruction($list);

if(isset($databill_single_instructionObj['collectionid']))
{
	// -- Single Bill Instruction warning message.
	$message =  $message.'Warning: Debit Order already exist for on '.
	$databill_single_instructionObj['Action_Date'].' from bank account number '.
	$databill_single_instructionObj['Account_Number'].' with client reference '.
	$databill_single_instructionObj['Client_Reference_1'];			
}

 echo $message; 
?>