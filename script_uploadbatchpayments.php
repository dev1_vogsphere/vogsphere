<?php
header("Content-Type: text/html; charset=utf-8");
		error_reporting( E_ALL );
		ini_set('display_errors', 1);
////////////////////////////////////////////////////////////////////////
// -- https://www.php.net/manual/en/function.iconv.php
if(!function_exists('getCsvDelimiter'))	
{	
   function getCsvDelimiter($filePath)
   {
	  $checkLines = 1;
      $delimiters =[',', ';', '\t'];

      $default =',';

       $fileObject = new \SplFileObject($filePath);
       $results = [];
       $counter = 0;
       while ($fileObject->valid() && $counter <= $checkLines) {
           $line = $fileObject->fgets();
           foreach ($delimiters as $delimiter) {
               $fields = explode($delimiter, $line);
               $totalFields = count($fields);
               if ($totalFields > 1) {
                   if (!empty($results[$delimiter])) {
                       $results[$delimiter] += $totalFields;
                   } else {
                       $results[$delimiter] = $totalFields;
                   }
               }
           }
           $counter++;
       }
       if (!empty($results)) {
           $results = array_keys($results, max($results));

           return $results[0];
       }
return $default;
}
}

// -- Move Uploaded File -- //
if(!function_exists('moveuploadedfile'))
{
	function moveuploadedfile($name,$tmp_name )
	{
			$today = date('Y-m-d H-i-s');
			$userid = getpost('userid');
			$path = "Files/Collections/Payments/".$userid."_".$today."_"; // Upload directory 
			$count = 0;
		 // No error found! Move uploaded files 
		 // Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			$ext = pathinfo($name, PATHINFO_EXTENSION);
			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;
			
		 // -- Update File Name Directory	
	            if(move_uploaded_file($tmp_name , $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }
			$newfilename = $path.$name;	
			return 	$tmp_name ;
	}
}

//If you need a function which converts a string array into a utf8 encoded string array then this function might be useful for you:
if(!function_exists('utf8_string_array_encode'))	
{	

function utf8_string_array_encode($array){
    $func = function(&$value,&$key){
        if(is_string($value)){
            $value = utf8_encode($value);
        }
        if(is_string($key)){
            $key = utf8_encode($key);
        }
        if(is_array($value)){
            utf8_string_array_encode($value);
        }
    };
    array_walk($array,$func);
    return $array;
}
}
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

////////////////////////////////////////////////////////////////////////  
if(!function_exists('uploadcsvfile'))
{
	function uploadcsvfile()
	{
		$output = null;
		$column = null;
		$row_data = null;
		$message = '';
		$tmp_name    = '';
		$separator = '';
		try{
		$tmp_name = $_FILES['csv_file']['tmp_name'];
		$cachedData = getmasterdata();	
		if(!empty($tmp_name))
		{
		// -- Backuploaded file for history & tracking
		 $name = $_FILES['csv_file']['name'];	
		 
		 $separator = getCsvDelimiter($tmp_name); 
		 $file_data = fopen($tmp_name, 'r');
		 $column = fgetcsv($file_data,0,$separator);	 		 
		 array_unshift($column,'Feedback');	
 		 $colCount    = count($column) - 1; 
		 $colCountAft = count($column); 
		 array_unshift($column,'Count');	
		 $rowCount = 1;
		 while($row = fgetcsv($file_data,0,$separator))
		 {
			// -- Scan the File Convert all record to UTF-8
			for($x=0;$x<$colCount;$x++ )
			{
				 $row[$x] = scanfile_to_UTF8($row[$x]);
			}	
			$message = extractcsvfile($row,$cachedData);
			array_unshift($row,$message);	
			array_unshift($row,$rowCount);	
			
			/* -- Re-Scan the File Convert all record to UTF-8
			for($x=0;$x<$colCountAft;$x++ )
			{
				 $row[$x] = scanfile_to_UTF8($row[$x]);
			}*/	
			$rowCount = $rowCount + 1;
			$row_data[] = $row;
		 }
		 
		 $output = array('column'  => $column,'row_data'  => $row_data);
		 $filenameEncryp = moveuploadedfile($name,$tmp_name);		
		 $output = json_encode($output);
		}
		}
		catch (Exception $e) 
			{
				$message = $e->getMessage();
				$row_data[] = $message;
				$output = array('column'  => $column,'row_data'  => $row_data);
			}
		
		return $output;//json_encode(fgetcsv($file_data,0,";"));//$output ;
	}
}

if(!function_exists('extractcsvfile'))
{
	function extractcsvfile($keydata,$cachedData)
	{
			$keydataJson = array();
			$message 	 = '';
			$UserCode	 = getpost('UserCode'); // From FormData
			$IntUserCode = getpost('IntUserCode');
			$userid 	 = getpost('userid');
			$role 		 = getpost('role');
			$Action_Date = '';
			$Reference_1 = '';
			$notificationsList = null;
			$anySuccess = '';
			
			$cachedBank 	   = $cachedData[0];
			$cachedAccountType = $cachedData[1];
			$cachedClass 	   = $cachedData[2];
			
			try{
							//$keydata 	 = $csvdata[$x];
							$csvdata = $keydata;
							//$keydata = utf8_string_array_encode($keydata);
							//print($keydata['Reference 1']);
							//Debug Encoding - echo json_encode($keydata)."\n";
							// -- UTF-8
							$keydata = json_encode($keydata,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
							if(empty($keydata))
							{
								// -- ANSI
								$keydata = $csvdata;//$csvdata[$x];
							}
							else
							{
								$keydataJson = json_decode($keydata,true);
							}
							
							$arrayInstruction[1] = $UserCode;//$keydata['Client_Id']; 										
							$TotalColmn = 17;
							$index = 0;
							
							// -- Is to avoid unicharacters e.g. \ff
							if(!empty($keydataJson))
							{
							$keydata = array();
							foreach($keydataJson as $item => $v) 
							{ 
							//print_r($v);
							//print('<br/>');
							// -- Trim trailing & leading spaces.
								$v = trim($v);
								//$v = mysql_real_escape_string($v);
								if($index == 0){$keydata['Reference 1'] = $v;}
								if($index == 1){$keydata['Reference 2'] = $v;}
								if($index == 2){$keydata['ID Type'] = $v;}
								if($index == 3){$keydata['ID Number'] = $v;}

								if($index == 4){$keydata['Contact Number'] = $v;}
								if($index == 5){$keydata['Initials'] = $v;}
								if($index == 6){$keydata['Account Holder'] = $v;}
								if($index == 7){$keydata['Account Type'] = $v;}
								
								if($index == 8){$keydata['Account Number'] = $v;}
								if($index == 9){$keydata['Bank Name'] = $v;}
								if($index == 10){$keydata['Number of Payments'] = $v;}
								if($index == 11){$keydata['Service Mode'] = $v;}								
								if($index == 12){$keydata['Service Type'] = $v;}

								if($index == 13){$keydata['Frequency'] = $v;}
								if($index == 14){$keydata['Action Date'] = str_replace('/','-',$v); }
								if($index == 15){$keydata['Bank Reference'] = $v;}
								if($index == 16){$keydata['Amount'] = $v;}
								if($index == 17){$keydata['Notify'] = $v;}
								if($index == 18){$keydata['Entry Class'] = $v;}
								$index = $index  + 1;
							}
							}							
							// -- EFT 							
							// -- NAEDO 
							
							// -- UTF-8
							//print_r($keydata);

							//echo $keydata['Reference 1'];
							if(isset($keydata['Reference 1'])){$arrayInstruction[2] = $keydata['Reference 1'];}else{$arrayInstruction[2] = '';}
							// -- UTF-8 DOM
							//echo $arrayInstruction[2];
							if(empty($arrayInstruction[2])){if(isset($keydata['\ufeffReference 1'])){$arrayInstruction[2] = $keydata['\ufeffReference 1'];}else{$arrayInstruction[2] = '';}}
							if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';}							
							//if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';} 								
							if(isset($keydata['ID Type'])){$arrayInstruction[4] = $keydata['ID Type'];}else{$arrayInstruction[4] = '';} 									
							if(isset($keydata['ID Number'])){$arrayInstruction[5] = $keydata['ID Number'];}else{$arrayInstruction[5] = '';} 									
							if(isset($keydata['Initials'])){$arrayInstruction[6] = $keydata['Initials'];}else{$arrayInstruction[6] = '';} 										
							if(isset($keydata['Account Holder'])){$arrayInstruction[7] = $keydata['Account Holder'];}else{$arrayInstruction[7] = '';} 									
							if(isset($keydata['Account Type'])){$arrayInstruction[8] = GetaccounttypebynameCache($keydata['Account Type'],$cachedAccountType);}else{$arrayInstruction[8] = '';} 									
							if(isset($keydata['Account Number'])){$arrayInstruction[9] = $keydata['Account Number'];}else{$arrayInstruction[9] = '';} 	
// -- User bank name on the spreadsheet to get the branchcode.							
							if(isset($keydata['Bank Name'])){$arrayInstruction[10] = GetbrachcodefromnameCache($keydata['Bank Name'],$cachedBank);}else{$arrayInstruction[10] = '';}//$keydata['Bank Name']; 									
							if(isset($keydata['Number of Payments'])){$arrayInstruction[11] = $keydata['Number of Payments'];}else{$arrayInstruction[11] = '';} 									
							if(isset($keydata['Service Type'])){$arrayInstruction[12] = $keydata['Service Type'];}else{$arrayInstruction[12] = '';} 									
							if(isset($keydata['Service Mode'])){$arrayInstruction[13] = $keydata['Service Mode'];}else{$arrayInstruction[13] = '';} 									
							if(isset($keydata['Frequency'])){$arrayInstruction[14] = $keydata['Frequency'];}else{$arrayInstruction[14] = '';} 										
							if(isset($keydata['Action Date'])){$arrayInstruction[15] = $keydata['Action Date'];}else{$arrayInstruction[15] = '';} 									
							if(isset($keydata['Bank Reference'])){$arrayInstruction[16] = $keydata['Bank Reference'];}else{$arrayInstruction[16] = '';} 								
							if(isset($keydata['Amount'])){$arrayInstruction[17] = $keydata['Amount'];}else{$arrayInstruction[17] = '0.00';}
							$arrayInstruction[17] = str_replace(",",".",$arrayInstruction[17]);														
							$arrayInstruction[18] = '1';//$keydata['Status_Code']; 									
							$arrayInstruction[19] = 'pending';//$keydata['Status_Description']; 							
							$arrayInstruction[20] = '';//$keydata['mandateid']; 										
							$arrayInstruction[21] = '';//$keydata['mandateitem']; 
							$arrayInstruction[22] = $IntUserCode;									
							$arrayInstruction[23] = $UserCode; 	
							if(isset($keydata['Entry Class'])){$arrayInstruction[24] = GetentryclassidCache($keydata['Entry Class'],$cachedClass);}else{$arrayInstruction[24] = 0;} 										
							if(isset($keydata['Contact Number'])){$arrayInstruction[25] = $keydata['Contact Number'];}else{$arrayInstruction[25] = '';} 										
							if(isset($keydata['Notify'])){$arrayInstruction[26] = $keydata['Notify'];}else{$arrayInstruction[26] = '';} 										
							$arrayInstruction[27] = $userid; // -- created by 	

							$message = auto_create_billpayment($arrayInstruction); 
							//$name2 = 'Nedbank';
							//$name2 =  $keydata['Bank Name'];
							//$message = GetbrachcodefromnameCache($name2 ,$cachedBank);
							//$message = $arrayInstruction[10]."MODISE";
							
							if($message == 'success' )
							{ 
								$anySuccess  = $message ;
							    // -- Build Queue Message to notify client about their debit orders.
								if($arrayInstruction[26] == 'Yes')
								{
									$smstemplatename = 'payments_notification';
									$applicationid = 0;		
									$Action_Date = $arrayInstruction[15];			
									$Reference_1 = $arrayInstruction[2];
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{	
										$notification['phone'] 		 = $arrayInstruction[25];
										$notification['content'] 	 = $contents;
										$notification['reference_1'] = $arrayInstruction[2];	
										$notification['usercode'] 	 = $UserCode;
										$notification['createdby'] 	 = $userid;
										$notification['IntUserCode'] = $arrayInstruction[22];	
										$notification['amount'] 	 = $arrayInstruction[17];
										$notification['Account_Holder'] = $arrayInstruction[7];																				
										$notificationsList[] = $notification;	// add into list
									}
								}
							}
							// -- one sms/email with notification..
							if($anySuccess == 'success')
							{
								$smstemplatename = 'batch_debitorder_uploaded';
								$applicationid = 0;		
								$Action_Date = $arrayInstruction[15];			
								$Reference_1 = $arrayInstruction[2];					
								// -- Sent to the uploader.
								auto_email_collection($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								// -- Sent to info@ecashmeup.com
								auto_email_collection('8707135963082',$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								
								// -- notify clients who signed for notifications(queue)
								if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
							}
						// -- Add Message to begin of an array.	
						//array_unshift($keydata,$message);

			}
			catch (Exception $e) 
			{
				$message = $e->getMessage();
				if($message == "Could not execute: /var/qmail/bin/sendmail\n")
				{ 
					$message = 'ERROR:Could not sent an email.';
				}
			}
							return $message;
	}
}
////////////////////////////////////////////////////////////////////////
// ----				 Declarations & Executions					   ---//
////////////////////////////////////////////////////////////////////////
$valid_formats = array("csv");
$max_file_size = 1024*10000; //10MB
$today = date('Y-m-d H-i-s');
$userid = getpost('userid');
$path = "Files/Collections/Instructions/".$userid."_".$today."_"; // Upload directory 
$count = 0;
$output = null;
ob_start ();
$output = uploadcsvfile();
//$UserCode	 = $_FILES['csv_file']['name'];//getpost('UserCode');
ob_end_clean();
if($output == "SQLSTATE[HY000] [2002] Only one usage of each socket address (protocol/network address/port) is normally permitted.\r\n")
{
	$column[] = 'Feedback';
	$row_data[] = $output; 
	$output = array('column'  => $column,'row_data'  => $row_data);
	$output = json_encode($output);
}	
echo $output;

/*$name = 'NAEDO 5 Day Tracking';
$cached = getmasterdata();
$cachedDataBank = $cached[2];
echo GetentryclassidCache($name ,$cachedDataBank);
var_dump($cachedDataBank);*/
////////////////////////////////////////////////////////////////////////
?>
