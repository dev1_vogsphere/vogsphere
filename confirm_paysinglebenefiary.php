<style>
.font-bold {
  font-weight: bold;
}
</style>
<div class="controls">
<div class="container-fluid" style="border:1px solid #ccc ">
<div class="row">
<div class="control-group">
	<div class="col-md-3">
		<div class="form-group">
			<label class="font-bold">Beneficiary</label>
			<label name="beneficiaryLbl" id="beneficiaryLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Initials</label>
			<label name="InitialsLbl" id="InitialsLbl"></label>
		</div>
	</div>	
	</div>
</div>	
<div class="row">	
	<div class="col-md-3">	
        <div class="form-group">
			<label class="font-bold">Account Holder</label>
			<label name="Account_HolderLbl" id="Account_HolderLbl"></label>
		</div>
	</div>	
    <div class="col-md-3">
		<div class="form-group">
			<label class="font-bold">Account Type</label>
			<label name="Account_TypeLbl" id="Account_TypeLbl"></label>
		</div>
	</div>
</div>	
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Account Number</label>
			<label name="Account_NumberLbl" id="Account_NumberLbl"></label>
		</div>
	</div>			
	<div class="col-md-3">	
	<div class="form-group">
			<label class="font-bold">Bank Name</label>
			<label name="Branch_CodeLbl" id="Branch_CodeLbl"></label>
	</div>
	</div>
</div>	
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">My Reference</label>
			<label name="Reference_1Lbl" id="Reference_1Lbl"></label>
		</div>
	</div>	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Benefiary Reference</label>
			<label name="Bank_ReferenceLbl" id="Bank_ReferenceLbl"></label>
		</div>
	</div>		
</div>	
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Beneficiary Notice of payment</label>
			<label name="NotifyLbl" id="NotifyLbl"></label>
		</div>
	</div>
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Contact Number</label>
			<label name="contact_numberLbl" id="contact_numberLbl"></label>
		</div>
	</div>	
</div>
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">User Code</label>
			<label name="benefiaryusercodeLbl" id="benefiaryusercodeLbl"></label>
		</div>
	</div>
	<div class="col-md-3"></div>
</div>
<?php if(!empty($paysingle)){?>	
<div class="row">	
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Amount</label>
			<label name="AmountLbl" id="AmountLbl"></label>
		</div>
	</div>
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Payment Date</label>
			<label name="Action_DateLbl" id="Action_DateLbl"></label>
		</div>
	</div>	
</div>	
<!-- <div class="row">
<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Number of payments</label>
			<label name="No_PaymentsLbl" id="No_PaymentsLbl"></label>
		</div>
	</div>
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Frequency</label>
			<label name="FrequencyLbl" id="FrequencyLbl"></label>
		</div>
	</div>	
</div>	-->
<?php }?>	
</div>
</div>