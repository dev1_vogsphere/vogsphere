<?php
// -- BOC 2017.06.24
// -- To avoid : Fatal error: Cannot redeclare class.
// -- EOC 2017.06.24
if (!class_exists('Database'))
{
class Database
{
	private static $dbName = 'eloan' ;
	private static $dbHost = 'localhost' ;
	private static $dbUsername = 'root';
	private static $dbUserPassword = 'root';

	private static $cont  = null;
	public static $conn;

//Billing Module DataBase
private static $dbNameB = 'paycollections' ;
private static $dbHostB = 'localhost' ;
private static $dbUsernameB = 'root';
private static $dbUserPasswordB = 'root';

	public function __construct()
	{
		exit('Init function is not allowed');
	}

	/*	public static function connectDB()
		{
			self::$conn = mysql_connect("localhost", self::$dbUsername, self::$dbUserPassword) or die(mysql_error());
			return self::$conn;
		}
	*/
	public static function connect()
	{
	   // One connection through whole application
       if ( null == self::$cont )
       {
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
        }
        catch(PDOException $e)
        {
          die($e->getMessage());
        }
       }
       return self::$cont;
	}


	public static function connectDB()
	{
		 // One connection through whole application
			 if ( null == self::$cont )
			 {
				try
				{
					self::$cont =  new PDO( "mysql:host=".self::$dbHostB.";"."dbname=".self::$dbNameB, self::$dbUsernameB, self::$dbUserPasswordB);
				}
				catch(PDOException $e)
				{
					die($e->getMessage());
				}
			 }
			 return self::$cont;
	}

	public static function disconnect()
	{
		self::$cont = null;
	}
	//  -- BOC encrypt password - 16.09.2017.
	public static function encryptPassword($password)
	{
			/**
			 * In this case, we want to increase the default cost for BCRYPT to 12.
			 * Note that we also switched to BCRYPT, which will always be 60 characters.
			 */
			$options = ['cost' => 12,];
			return password_hash($password, PASSWORD_BCRYPT, $options);
	}
	// 	-- EOC encrypt password - 16.09.2017.
}

// -- Hash Every Page.
function redirect($url)
{
    ob_start();
	$hash="?guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}

// -- Hash to Page with parameters.
function redirectParam($url)
{
    ob_start();
	$hash="&guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}
// -- Contains Characters
// returns true if $needle is a substring of $haystack
function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}

// -- Redirect to Login Page with not logged in.

// -- Generate Captcha Image
function captchImage($value)
{
	 $generateImage = '';
	 if (!empty($_POST))
	 {
	 if (isset($_POST['login']))
	   {$generateImage = 'No';}

     if (isset($_POST['Apply']))
		{$generateImage = 'No';}
	}


	 if( $generateImage == '')
	 {
	$string = '';
	for ($i = 0; $i < 5; $i++) {
		$string .= chr(rand(97, 122));
	}

	$_SESSION['captcha'] = $string; //store the captcha
	$_SESSION['value'] = $value;
	$dir = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]).'/fonts/';//"REQUEST_URI-PHP_SELF-SERVER_NAME
	$image = imagecreatetruecolor(165, 50); //custom image size
	$font = "PlAGuEdEaTH.ttf"; // custom font style
	$color = imagecolorallocate($image, 113, 193, 217); // custom color
	$white = imagecolorallocate($image, 255, 255, 255); // custom background color
	imagefilledrectangle($image,0,0,399,99,$white);
	imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);
//echo $dir.$font;
	// Enable output buffering
	ob_start();
	imagepng($image);
	// Capture the output
	$imagedata = ob_get_contents();
	// Clear the output bufferx
	ob_end_clean();
	$_SESSION['image'] = $imagedata;
	}
	else
	{
	$imagedata = $_SESSION['image'];
	}
	$random = chr(rand(97, 122));

	//echo "<h2>Generate Image - $generateImage - $random</h2>";

	return $imagedata;
}

// -- Disbursement Fee,Insurance Fee.
function FeeContract($AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges";
		$tbl_charge ="charge";

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if (empty($dataDisbursementFee))
		{
		// -- Return the Fee.
		  		  return 0.00;
		}
		else
		{
			return $dataDisbursementFee['amount'];
		}
}
// -- Percentage Fee like VAT 15%
function FeePercentageBasedContract($AppID,$chargeid, $Item,$amountdue)
{
		$tbl_loanappcharges ="loanappcharges";
		$tbl_charge ="charge";
		$percentage = 0.00;

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if (empty($dataDisbursementFee))
		{
		// -- Return the Fee.
		  		  return 0.00;
		}
		else
		{
			$percentage = $dataDisbursementFee['percentage'];
			// -- convert to percentage amount to calculate with amount due
			if($amountdue > 0)
			{
				if($percentage > 0)
				{
					$percentage = $percentage / 100;
					$amountdue = ($amountdue * $percentage);
				}
				else
				{
					$amountdue = 0.00;
				}
			}

			return $amountdue;
		}
}

// BOC --- Get MutipleCharge ID - Fees; 01.05.2018.
// -- Ge the Fees IDs.
function GetContractFeesID($loanDate,$ChargeType,$chargeoptionid)
{
   // ----- Global - Payment Allocated Charges, description ----------------- //
	$indexInside = 0;
	$dataCharges = '';
	$ArrayCharges = null;

	// -- Database Connections.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// --------------------- Charges ------------------- //
	$sql = 'SELECT * FROM charge WHERE chargetypeid = ? AND fromdate <= ?  AND todate >= ? AND chargeoptionid = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($ChargeType,$loanDate ,$loanDate,$chargeoptionid));
	$dataCharges = $q->fetchAll(PDO::FETCH_ASSOC);

	//echo 'charge type:'.$ChargeType.';Loan Date:'.$loanDate;
	if(empty($dataCharges))
	{
		return $ArrayCharges;
	}
	else
	{

		foreach($dataCharges as $rowCharge)
		{
			  $ArrayCharges [$indexInside] = $rowCharge['chargeid'];
			  $indexInside = $indexInside + 1;
		}
				return $ArrayCharges;
	}
}

// -- Get Fee Fixed Amount.
function GetFeeFixedAmount($chargetypeid,$applicationid,$item,$scheduleddate,$chargeoptionid)
{
	$chargeidArray = GetContractFeesID($scheduleddate,$chargetypeid,$chargeoptionid);
	$IndexStart = 0;
	$fees = 0.00;
	if(empty($chargeidArray))
	{
		//echo 'empty';
	}
	else
	{
		// -- Go through all row per chargeID.
		$ArrayLength = count($chargeidArray);
		$chargeidArrayLength = count($chargeidArray);
		$chargeidArrayGlobal = $chargeidArray;
		// -- Get all charges & amount per chargeID.
		for($j=$IndexStart;$IndexStart<$ArrayLength;$IndexStart++)
		{
				$chargeid = $chargeidArray[$IndexStart];
				$fees = $fees + FeeContract($applicationid,$chargeid,$item);
		}
	}

	return $fees;
}
// EOC --- Get MutipleCharge ID - Fees; 01.05.2018.

if (!function_exists('GenerateAccountNumber'))
{
	// -- Account/Reference must be the 9 digits derived from the ID number.
	// -- Reading from the ID backward.
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';

	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);

		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9);
	   }
	   return $AccountNumber;
	}
}

if (!function_exists('GetPaymentSchedule'))
{
	function GetPaymentSchedule($ApplicationId)
	{
	  try
	  {
		Global $pdo;
		$tbl_name ="paymentschedule";
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_name where ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($ApplicationId));
		$data = $q->fetchAll();

		Database::disconnect();
		return $data;
	  }
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('formatSerialize'))
{

function formatSerialize(&$strItem, $strKey)
{
    $strItem = str_replace('&', '[amp;]',$strItem);
}
}
if (!function_exists('formatSerializeRev'))
{

function formatSerializeRev(&$strItem, $strKey)
{
    $strItem = str_replace('[amp;]', '&',$strItem);
}
}

if (!function_exists('GetRecentApplicationID'))
{

// -- Get Recent Created ApplicationID.
function GetRecentApplicationID($idnumber)
{
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
  $ApplicationID = '';
  $Today = date('Y-m-d');
  $count = 1;
  
  // -- Get the Application ID.
  $sql = 'SELECT * FROM loanapp WHERE CustomerId = ? AND DateAccpt = ? ORDER BY ApplicationId DESC';
  $q = $pdo->prepare($sql);
  $q->execute(array($idnumber,$Today));	
  $data = $q->fetchAll();

  // -- Get the first ApplicationID. 
  if(!empty($data))
  {
     foreach ($data as $row) 
	 {
	   if($count == 1)
	   {
	     $ApplicationID = $row['ApplicationId'];
	   }	   
	   $count = $count + 1;
	 }
  }
  return $ApplicationID;
}
}

if (!function_exists('GetAgentDetails'))
{
	// -- Data Models Functions
function GetAgentDetails($agentid)
{
	Global $pdo;
	$dataAgent = null;
	
	if(!empty($agentid))
	{
		$sql = 'SELECT * FROM customer WHERE CustomerId = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($agentid));	
		$dataAgent = $q->fetch(PDO::FETCH_ASSOC);
	}	
	return $dataAgent;
}

}

if (!function_exists('GetTemplateDetails'))
{
	// -- Data Models Functions
function GetTemplateDetails($templatename)
{
	Global $pdo;
	$dataTemplate = null;
	
	if(!empty($templatename))
	{
		$sql = 'SELECT * FROM smstemplate WHERE smstemplatename = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($templatename));	
		$dataTemplate = $q->fetch(PDO::FETCH_ASSOC);
	}	
	return $dataTemplate;
}

}

// -- auto email
if(!function_exists('auto_email'))
{
 function auto_email($customerid,$smstemplatename)
 {
		// -- Get Customer Data	 
		$customerObj  = GetAgentDetails($customerid);
	   
		// -- Get smstemplate Data  
		$smstemplateObj = GetTemplateDetails($smstemplatename);
		  
		// --- Send SMSs via CM Telcoms -- //
		$messageLog = '';
		
		// -- Subject
		$subject = '';
		
				// --
		$refnumber = '';
		$agentid = '';

		if(!empty($customerObj) && !empty($smstemplateObj))
		{	
			// 1. -- Get Placeholders
			// 2. -- Use Customer Data
			// 2. -- Use smstemplate Data
			$message 		   = $smstemplateObj['smstemplatecontent'];
			$subject 		   = $smstemplateObj['subject'];
			$CustomerId 	   = $customerObj['CustomerId'];
			$Title 			   = $customerObj['Title'];
			$FirstName  	   = $customerObj['FirstName'];
			$LastName 		   = $customerObj['LastName'];
			$Street 		   = $customerObj['Street'];
			$Suburb 		   = $customerObj['Suburb'];
			$City 			   = $customerObj['City'];
			$PostCode 		   = $customerObj['PostCode'];
			$Dob 			   = $customerObj['Dob'];
			$phone 			   = $customerObj['phone2'];
			$accountholdername = $customerObj['accountholdername'];
			$bankname  		   = $customerObj['bankname'];
			$accountnumber     = $customerObj['accountnumber'];
			$branchcode  	   = $customerObj['branchcode'];
			$accounttype  	   = $customerObj['accounttype'];
			$email  		   = $customerObj['email2'];
		    $agentid 		   = $customerObj['changedby'];

			// -- Placeholders replace with actual data.
			$message = str_replace('[CustomerId]',$CustomerId ,$message);
			$message = str_replace('[Title]',$Title ,$message);
			$message = str_replace('[FirstName]',$FirstName ,$message);
			$message = str_replace('[LastName]',$LastName ,$message);
			$message = str_replace('[Street]',$Street ,$message);
			$message = str_replace('[Suburb]',$Suburb ,$message);
			$message = str_replace('[City]',$City ,$message);
			$message = str_replace('[Dob]',$Dob ,$message);
			$message = str_replace('[phone2]',$phone ,$message);
			$message = str_replace('[refnumber]',$refnumber ,$message);
			$message = str_replace('[accountholdername]',$accountholdername ,$message);
			$message = str_replace('[bankname]',$bankname ,$message);
			$message = str_replace('[accountnumber]',$accountnumber ,$message);
			$message = str_replace('[branchcode]',$branchcode ,$message);
			$message = str_replace('[accounttype]',$accounttype ,$message);
			$message = str_replace('[email2]',$email ,$message);

			if(!empty($email))
			{
		// -- Send Email.		
				SendEmail($message, $email,$subject);
		// -- Log Email sent to recipients.		
				$status = 'success';
				AddCommunicatioHistoryGlobal($message,$CustomerId,$refnumber,$agentid,$status,$email,$phone,'email');
				$messageLog = '<p>Email Message - successfully sent to '.$phone.'</p><br/>';
			}
		}
		else
		{
			$messageLog = '<p>Email Message - unsuccessfully sent.</p><br/>';
	    }
    }	
}
// -- auto sms	
if(!function_exists('auto_sms'))
{
 function auto_sms($customerid,$smstemplatename)
 {
		// -- Get Customer Data	 
		$customerObj  = GetAgentDetails($customerid);
	   
		// -- Get smstemplate Data  
		$smstemplateObj = GetTemplateDetails($smstemplatename);
		  
		// --- Send SMSs via CM Telcoms -- //
		$messageLog = '';
		
		// -- Subject
		$subject = '';
		// --
		$refnumber = '';

		$agentid = '';
		
		if(!empty($customerObj) && !empty($smstemplateObj))
		{	
			// 1. -- Get Placeholders
			// 2. -- Use Customer Data
			// 2. -- Use smstemplate Data
			$message 		   = $smstemplateObj['smstemplatecontent'];
			$CustomerId 	   = $customerObj['CustomerId'];
			$Title 			   = $customerObj['Title'];
			$FirstName  	   = $customerObj['FirstName'];
			$LastName 		   = $customerObj['LastName'];
			$Street 		   = $customerObj['Street'];
			$Suburb 		   = $customerObj['Suburb'];
			$City 			   = $customerObj['City'];
			$PostCode 		   = $customerObj['PostCode'];
			$Dob 			   = $customerObj['Dob'];
			$phone 			   = $customerObj['phone2'];
			$accountholdername = $customerObj['accountholdername'];
			$bankname  		   = $customerObj['bankname'];
			$accountnumber     = $customerObj['accountnumber'];
			$branchcode  	   = $customerObj['branchcode'];
			$accounttype  	   = $customerObj['accounttype'];
			$email  		   = $customerObj['email2'];
		    $agentid 		   = $customerObj['changedby'];
			
			// -- Placeholders replace with actual data.
			$message = str_replace('[CustomerId]',$CustomerId ,$message);
			$message = str_replace('[Title]',$Title ,$message);
			$message = str_replace('[FirstName]',$FirstName ,$message);
			$message = str_replace('[LastName]',$LastName ,$message);
			$message = str_replace('[Street]',$Street ,$message);
			$message = str_replace('[Suburb]',$Suburb ,$message);
			$message = str_replace('[City]',$City ,$message);
			$message = str_replace('[Dob]',$Dob ,$message);
			$message = str_replace('[phone]',$phone ,$message);
			$message = str_replace('[refnumber]',$refnumber ,$message);
			$message = str_replace('[accountholdername]',$accountholdername ,$message);
			$message = str_replace('[bankname]',$bankname ,$message);
			$message = str_replace('[accountnumber]',$accountnumber ,$message);
			$message = str_replace('[branchcode]',$branchcode ,$message);
			$message = str_replace('[accounttype]',$accounttype ,$message);
			$message = str_replace('[email2]',$email ,$message);
			
			// -- Check if phone number is provided
			if(empty($phone))
			{
				// -- Log SMS sent to recipients.		
				//$status = 'success';
				//AddCommunicatioHistoryGlobal($message,$CustomerId,$refnumber,$agentid,$status,$email,$phone,'sms');
			}
			else
			{	
			// -- Add Country code : 0027 
			$phone = trim("0027".substr($phone,1));
			$smsRef = new CMSMS();
			
			// -- Send SMS.					
			$response = $smsRef->sendMessage($phone, $message);	
			
			// -- Log SMS sent to recipients.		
			$status = 'success';
			AddCommunicatioHistoryGlobal($message,$CustomerId,$refnumber,$agentid,$status,$email,$phone,'sms');
			$messageLog = '<p>SMS Message - successfully sent to '.$phone.'</p><br/>';
			}
		}
		else
		{
			$messageLog = '<p>SMS Message - unsuccessfully sent.</p><br/>';
	    }
    }	
  }
  
if (!function_exists('CheckCommunication'))
{
// -- Check Email & Phone.
function CheckCommunication($phone,$email)
{
	Global $phoneError;
	Global $emailError;
	Global $tbl_lead;
	Global $leadid;
	$leadidLv = $leadid; 
	$tbl_user = 'user';

	$lc_valid = true;	

		$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// ---------------- Check Phone number -------------- //
			if(!empty($phone))
			{
				$query = "SELECT * FROM $tbl_lead WHERE phone = ? AND leadid <> ?";
				$q = $pdo->prepare($query);
				$q->execute(array($phone,$leadidLv));
				if ($q->rowCount() >= 1)
				{$phoneError = 'Phone number already exists for lead.'; $lc_valid = false;}
			
			
				$query = "SELECT * FROM customer WHERE phone = ? AND leadid <> ?";
				$q = $pdo->prepare($query);
				$q->execute(array($phone,$leadidLv));
				if ($q->rowCount() >= 1)
				{$phoneError = 'Phone number already exists for customer.'; $lc_valid = false;}
			}
			
			// ---------------- Check E-mail in Users ------------------ //
			if(!empty($email))
			{
				$query = "SELECT * FROM $tbl_user WHERE email = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use for user.'; $lc_valid = false;}
			}
			
			// ---------------- Check E-mail in Customer ------------------ //
			if($lc_valid)
			{
				if(!empty($email))
				{
					$query = "SELECT * FROM $tbl_lead WHERE email = ? AND leadid <> ?";
					$q = $pdo->prepare($query);
					$q->execute(array($email,$leadidLv));
					if ($q->rowCount() >= 1)
					{$emailError = 'E-mail Address already in use for lead.'; $lc_valid = false;}
				
					$query = "SELECT * FROM customer WHERE email2 = ? AND leadid <> ?";
					$q = $pdo->prepare($query);
					$q->execute(array($email,$leadidLv));
					if ($q->rowCount() >= 1)
					{$emailError = 'E-mail Address already in use for customer.'; $lc_valid = false;}
				}
			}
	
		return $lc_valid;
}
	}
  
if (!class_exists('CMSMS'))
{
  class CMSMS
  {
	function __construct()
	{
		
	}
     public function buildMessageXml($recipient, $message) 
	{
			  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';

      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', $ProductToken);

      $msg = $xml->addChild('MSG');
      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);
      $msg->addChild('BODY', $message);


      return $xml->asXML();
    }

     public function sendMessage($recipient, $message) 
	{
      $xml = self::buildMessageXml($recipient, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);

	  	  echo '<h3>'.curl_error($ch).'</h3>';

	  	$info = curl_getinfo($ch);

      curl_close($ch);

      return $info;
    }
  }
}
}  
?>
