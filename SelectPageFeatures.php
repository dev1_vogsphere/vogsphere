<?php
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$page       = $_POST['page'];
$access_id  = '';
$featureid  = '';

if(isset($_POST['access_id']))
{
  $access_id  = $_POST['access_id'];
}
if(isset($_POST['featureid']))
{
	$featureid  = $_POST['featureid'];
}
$dataPageFeaturesArr = array();

$html = "";

// -- Select page features.
$sql = 'SELECT * FROM page WHERE page = ?';
$q = $pdo->prepare($sql);
$q->execute(array($page));	
$dataPageFeatures = $q->fetch(PDO::FETCH_ASSOC);
// -- 
	if (empty($dataPageFeatures['features']))
	{
		$html = "<td>No features</td>";
	}
	else
	{
	// -- split features into array..
		$checked = '';
		$dataPageFeaturesArr = explode(",", $dataPageFeatures['features']);
		$html=$html."<td>";
		foreach($dataPageFeaturesArr as $row)
		{
			// -- Select Access Page features
			if(!empty($access_id)){
			$sql = 'SELECT * FROM features WHERE page = ? AND  access_id = ? AND features = ?';
			$q = $pdo->prepare($sql);
			$q->execute(array($page,$access_id,$row));	
			$dataAccessPageFeatures = $q->fetch(PDO::FETCH_ASSOC);
			if(!empty($dataAccessPageFeatures))
			{
				if(!empty($dataAccessPageFeatures['active']))
				{
					$checked = 'checked';
				}
		    }}
			$html = $html."<p><b>{$row}</b><input style='height:30px' type='checkbox' id='{$row}' name='{$row}' {$checked} /></p>";
				//$html = $html."<p><b>{$row}</b></p>";
		}
			$html=$html."</td>";
	}	
	
 echo $html;		
?>