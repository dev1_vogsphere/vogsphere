CREATE TABLE `ecashpdq_eloan`.`api_webservice_moni` (
  `id` INT NOT NULL,
  `reference` VARCHAR(45) NULL,
  `date` VARCHAR(45) NULL,
  `time` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `usercode` VARCHAR(45) NULL,
  `operation` VARCHAR(45) NULL,
  `url` VARCHAR(45) NULL,
  `cost` VARCHAR(45) NULL,
  `requestcontent` VARCHAR(45) NULL,
  `responsecontent` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `ecashpdq_eloan`.`api_webservice_moni` 
CHANGE COLUMN `responsecontent` `responsecontent` JSON NULL DEFAULT NULL ;
