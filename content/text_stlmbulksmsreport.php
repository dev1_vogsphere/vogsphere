<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="assets/js/jquery.tableToExcel.js"></script>
<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="api_response"; // Table name 
 $bankid = '';
 $From = date("Y-m-d");
 $To = '';
 if($To == ""){$To = "9999-12-31";}		
 $customerid = '';
 $statusReq = '';
 $status = '';

// -- Call Communications.
 $tablename = 'api_response';
 $tablenameQ = 'api_responsequeue';
 
 $dbnameobj = 'ecashpdq_eloan';
 $queryFields = null;
 $queryFields[] = '*';

 $whereArray  = null;								

 $All 	  = '';
 $sent 	  = '';
 $failed  = '';
 $queued = '';

 $monrepo    = '';
 $chkmonrepo = '';
 $statusReq = '';
 
 if ( !empty($_GET['status'])) 
 {
	  $statusReq = $_GET['status'];
 }
	//echo $statusReq;

 include 'database.php';
// User Code
 $usercode = getsession('UserCode');
 $IntUserCode = getsession('IntUserCode');

if($_SESSION['role'] != 'customer')
{	 
	if(!empty($_POST))
	{
		$whereArr = null;
		$queryFields = null;
		$queryFields[] = '*';

		$From = getpost("From");
		$status = getpost("status");

		$monrepo    = getpost("monrepo");
		$chkmonrepo = getpost("chkmonrepo");
		
		$usercode = getsession('UserCode');
		$whereArr[] = "usercode = '{$usercode}'";

		$IntUserCode = getsession('IntUserCode');
		$whereArr[] = "IntUserCode = '{$IntUserCode}'";
		
		$lvmsgtype = $status;
		if(!empty($status) && $status != '*'){$status = "'".$status."'";$whereArr[] = "status = {$status}";}
		$status = $lvmsgtype;
// -- For a specifc Day Report.		
		if(empty($chkmonrepo))
		{
			$lvfrom = $From;
			if(!empty($From)){$From = "'".$From."%'"; $whereArr[] = "date LIKE {$From}"; }
			$From = $lvfrom;
		}
// -- Monthly Report		
		else
		{
			$lvfrom = $monrepo;
			if(!empty($monrepo)){$monrepo = "'".$monrepo."%'"; $whereArr[] = "date LIKE {$monrepo}"; }
			$monrepo = $lvfrom;			
		}
		
		
		$monrepo = getpost("monrepo");		
		$From = getpost("From");
		$status = getpost("status");
		//print_r($whereArr);
		$data = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);	
		$data1 = search_dynamic($dbnameobj,$tablenameQ,$whereArr,$queryFields);
		//print_r($data);

		if(!empty($data->rowCount()) && !empty($data1->rowCount()))
		{$dataArr = $data->fetchAll();$dataArr1 = $data1->fetchAll(); $data = array_merge($dataArr,$dataArr1);}
		elseif(!empty($data1->rowCount())){$data = $data1;}
		
	}
	else
	{
		$whereArr = null;
		$queryFields = null;
		$queryFields[] = '*';
		$whereArr[] = "usercode = '{$usercode}'";
		$whereArr[] = "IntUserCode = '{$IntUserCode}'";		
		$status = $statusReq;
//echo $usercode
		$lvmsgtype = $status;
		if(!empty($status) && $status != '*'){$status = "'".$status."'";$whereArr[] = "status = {$status}";}
		$status = $lvmsgtype;

		$lvfrom = $From;
		if(!empty($From)){$From = "'".$From."%'"; $whereArr[] = "date LIKE {$From}"; }
		$From = $lvfrom;
										
		$data  = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);
		$data1 = search_dynamic($dbnameobj,$tablenameQ,$whereArr,$queryFields);
		
		$SMSs = $data->rowCount();

		
		//print_r($data);
		if(!empty($data->rowCount()) && !empty($data1->rowCount())){$dataArr[] = $data;$dataArr[] = $data1; $data = $dataArr;/*$data = array_merge($data,$data1);*/}
		elseif(!empty($data1->rowCount())){$data = $data1;}
		
	}					
}

if($status == '*')
{
	$All = 'selected';
}
	
if($status == 'sent')
{
	$sent = 'selected';
}
		
if($status == 'failed')
{
	$failed = 'selected';
}
	
if($status == 'queued')
{
	$queued = 'selected';
}	
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role']!= 'customer')
	{
		$SMSs = 0;//$data->rowCount();
		$panelclass = "panel panel-yellow";
		$url = "stlmbulksmsreport"; 
		$Headtitle = "";
		$dashtitle = ''; 
		$dynamicDashSMS = dynamicDashboard($SMSs,$panelclass,$url,$Headtitle,$dashtitle);

		/*echo "
																<div class='margin-vert-30'>
																	<!-- Main Text -->

															<div id='page-wrapper'>
																<div>
																	<div class='col-lg-12'>
																		<h1 class='page-header'>Bulk SMS - Statistics</h1>
																	</div>
																</div>
															</div>".'

		<!------------------------------------- Dashboards ----------------------------------->
		<!-------- Success --------->'.
		'<!---------- Failed ---->'.$dynamicDashSMS."<div id='page-wrapper'><div><div class='col-lg-12'><h5 class='page-header'></h5></div></div>".'</div>';
		*/?>

	<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search for Bulk SMS</h2>
                                    </div>
                                    <!--<div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                    </div>	-->
									<div class="control-group">
										<div class='input-group margin-bottom-20'>
										 <span class='input-group-addon'>
											<i class='fa-caret-down'></i>
										 </span>
										<SELECT class="form-control" id="status" name="status" size="1" style='z-index:0'>
											<OPTION value='*' <?php echo $All;?>>All Status</OPTION>								
											<OPTION value='sent' <?php echo $sent;?>>Sent</OPTION>
											<OPTION value='failed' <?php echo $failed;?>>Failed</OPTION>
											<OPTION value='queued' <?php echo $queued;?>>Queued</OPTION>											
										</SELECT>
										</div>
									</div>
									
                                    <div class='input-group margin-bottom-20' style="z-index:0">
										<span class='input-group-addon'>
											<i class='fa fa-calendar'></i>
										</span>
                                        <input style='height:30px;z-index:0' id='From' name='From' placeholder='Date From' class='form-control' type='Date' onchange="getmonth()" value=<?php echo $From;?>>
                                    </div>

									<div class='col-sm-10'>
										<label class='control-label'>Month of Report:</label>
										<input style='height:30px' id='monrepo' name='monrepo' placeholder='Monthly Report' class='form-control' type='text' value=<?php echo $monrepo; ?>>
                                    </div>	
									
									<div class='col-sm-10'>
                                        <label class='checkbox'>
                                            <input type='checkbox' name="chkmonrepo" id="chkmonrepo" value="chkmonrepo" onchange="getmonth()" <?php if ($chkmonrepo == 'chkmonrepo') echo 'checked'; ?>>Show Monthly Report
                                        </label>
									</div>

                                    <!-- <div class='input-group margin-bottom-20'>
										<span class='input-group-addon'>
											<i class='fa fa-calendar'></i>
										</span>
                                    </div> -->
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
	<?php } ?> 							
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				
  				  <p><button class="btn btn-primary" onclick="$('table').tblToExcel();">Export to excel</button></p>

				  <div class="table-responsive">
					<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
		              <?php 
						echo "<tr>"; 
						echo "<td><b>Date Time</b></td>"; 
						echo "<td><b>Cellphone</b></td>"; 
						echo "<td><b>Status</b></td>"; 
						echo "<td><b>Message URL</b></td>"; 
						echo "<td><b>Cost</b></td>"; 												
						echo "<th>Message Content</th>"; 
						echo "</tr>"; 
						$count = 0;	
						$msgid = '';
						$dataArr = null;
						if(is_array($data)){$dataArr = $data;}else{$dataArr = $data->fetchAll();}
								//print_r($dataArr);	
						foreach($dataArr as $row)
						{ 
						//foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['date']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['status']) . "</td>"; 
						echo "<td valign='top'>" . nl2br( $row['url']) . "</td>";  		
						echo "<td valign='top'>" . nl2br( $row['cost']) . "</td>";  
						//echo "<td valign='top'>" . nl2br( $row['message']) . "</td>";  																		
						$mess = "'".$row['message']."'";//nl2br("(".$row['message'].")");	
						$mess = htmlspecialchars($mess);
						$msgid = "'".'mess'.$count."'";
						echo "<td valign='top'>";
						echo '<button type="button" class="btn btn-info" data-popup-open="popup-1" onclick="content('.$msgid.')">View Message</button>';
						echo "</td>";
						echo "<td valign='top'>";
						echo "<input style='height:30px' id=$msgid name=$msgid placeholder='Customer ID' class='form-control' type='hidden' value=".$mess.">";
						echo "</td>";
						echo "</tr>"; 	
						$count = $count + 1;
						} 
						echo "</table>";

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			<!-- BOC - Communications -->
					 <div class="popup" data-popup="popup-1" style="overflow-y:auto;">
						<div class="popup-inner">
										<div id="divContent" style="top-margin:100px;font-size: 10px;">										
										</div>							
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>	
					 </div>
					</div>			
			<!-- EOC - Communications -->
				</div>
				
    	</div>
</div>		
   		<!---------------------------------------------- End Data ----------------------------------------------->
<script>
function content(msgid)
{
	    var divContent = document.getElementById(msgid).value; 
		//alert(divContent);
		divContent = htmlEntities(divContent);
		document.getElementById('divContent').innerHTML = divContent;
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");   
		  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';*/

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) 
	{
      clearInterval(id);
    } else 
	{
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}	
function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}

function getmonth()
  {
	 var month = document.getElementById('From').value;
	 month = month.substring(0, 7);
	 document.getElementById('monrepo').value = month; 
  }

$( document ).ready(function() {
        getmonth();
    });  
</script>		