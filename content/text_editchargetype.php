<?php 
require 'database.php';
$tablename = 'chargetype';
$chargeoptionnameError = null;
$chargeoptiondescError = null;

$chargetypename = '';
$chargetypedesc = '';
$count = 0;
$chargetypeid = '';

if (isset($_GET['chargetypeid']) ) 
{ 
$chargetypeid = $_GET['chargetypeid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$chargetypename = $_POST['chargetypename'];
	$chargetypedesc = $_POST['chargetypedesc'];;
	$valid = true;
	
	if (empty($chargetypename)) { $chargetypenameError = 'Please enter Charge Type name.'; $valid = false;}
	if (empty($chargetypedesc)) { $chargetypedescError = 'Please enter Charge Type description.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE $tablename SET name = ?,chargetypedesc = ? WHERE chargetypeid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($chargetypename,$chargetypedesc,$chargetypeid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tablename where chargetypeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($chargetypeid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$chargetypename = $data['name'];
		$chargetypedesc = $data['chargetypedesc'];
}

?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
  
<div class="panel-heading">
<h2 class="text-center">
                         Charge Type
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>$tablename successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Charge Type</b><br />
		<input style="height:30px" type='text' name='chargetypename' value="<?php echo !empty($chargetypename)?$chargetypename:'';?>"/>
			<?php if (!empty($chargetypenameError)): ?>
			<span class="help-inline"><?php echo $chargetypenameError;?></span>
			<?php endif; ?>
			</p> 	
		
		<p><b>Charge Type Description</b><br />
		<input style="height:30px" type='text' name='chargetypedesc' value="<?php echo !empty($chargetypedesc)?$chargetypedesc:'';?>"/>
			<?php if (!empty($chargetypedescError)): ?>
			<span class="help-inline"><?php echo $chargetypedescError;?></span>
			<?php endif; ?>
			
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="chargetype">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>