<?php
// -- BOC Force All eCashMeUp pages not to cache on all browsers.14.10.2017
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// -- EOC Force All eCashMeUp pages not to cache all browsers.14.10.2017

require 'database.php';
$dbGlobal = 'ecashpdq_eloan';

// -- Captcha
$modise = '';

// -- BOC Session Timeouts 30.09.2017
$message="";

/*if(isset($_GET["session_expired"])) 
{
	$message = "Login Session is Expired. Please Login Again";
}*/
// -- EOC Session Timeouts 30.09.2017

if (!empty($_POST)) 
{
		
	 // Captcha declare
	 	$modise = $_SESSION['captcha'];
		$captcha = '';
		$captchaError = null;
		// --Captcha
		$captcha = $_POST['captcha'];
		
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$password1Error = null;
	
		// Login Details
		$password1 = $_POST['password1'];
		$idnumber  = $_POST['idnumber'];
		
		// -- echo "<h3>ID - $idnumber </h3>";
		
		// To protect MySQL injection for Security purpose (Encryption 30.09.2017)
		$password1 = $password1;// -- stripslashes(
		$idnumber  = $idnumber;// -- stripslashes(
		$password1 = $password1;// -- mysql_real_escape_string(
		$idnumber  = $idnumber;// -- mysql_real_escape_string(
		
		// -- echo "<h3>ID 2 - $idnumber </h3>";


		// validate input - 		// Customer Details
		$valid = true;
		if (empty($password1)) { $password1Error = 'Please enter password'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
	  // Captcha Error Validation.
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}
		//$captcha
			
			// Captcha
			// 	refresh Image
			if (!isset($_POST['login'])) // If refresh button is pressed just to generate Image. 
			{
				$valid = false;
			}
		if($valid)
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// ---------------- User ---------------
			//User - UserID & E-mail Duplicate Validation
			$query = "SELECT * FROM user WHERE userid = ?"; // --  AND password = ?
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));// -- Changed 16.09.2017 - MD5 Encryption. $passwordEncrypted
			
			//  -- BOC encrypt password - 16.09.2017.	
			$passwordEncrypted = $q->fetch(PDO::FETCH_ASSOC);
			//  -- EOC encrypt password - 16.09.2017.	

			if ($q->rowCount() < 1)
			{
				$password1Error = 'ID number and password you entered do not match.'; $valid = false;
			}
			else
			{
				//  -- BOC encrypt password - 16.09.2017.	
				if (!password_verify($password1, $passwordEncrypted['password'])) 
				{
					$password1Error = 'ID number and password you entered do not match.'; $valid = false;
					// -- 3 failed failed attempt
					if($passwordEncrypted['failedAttempt'] < 3)
					{
						// -- BOC Brut-force protection. count failedAttempt - 16.09.2017.
						$passwordEncrypted['failedAttempt'] = $passwordEncrypted['failedAttempt'] + 1;
						$sql = "UPDATE user SET failedAttempt = ? WHERE userid = ?"; 		
						$q = $pdo->prepare($sql);
						$q->execute(array($passwordEncrypted['failedAttempt'],$idnumber)); // -- Changed 16.09.2017 - MD5 Encryption.
						Database::disconnect();
					}
					else
					{	
						$password1Error = "We have temporarily locked your account after too many failed<br /> attempts to login.
						Please contact the administrator.";
						$valid = false;
					}
				}
				
				if($valid)
				{
					// -- Reset failedAttempt to zero.
					if($passwordEncrypted['failedAttempt'] < 3)
					{
						$passwordEncrypted['failedAttempt'] = 0;
						$sql = "UPDATE user SET failedAttempt = ? WHERE userid = ?"; 		
						$q = $pdo->prepare($sql);
						$q->execute(array($passwordEncrypted['failedAttempt'],$idnumber)); // -- Changed 16.09.2017 - MD5 Encryption.
						Database::disconnect();
					}
					else
					{
						$password1Error = "We have temporarily locked your account after too many failed<br /> attempts to login.
						Please contact administration.";
						$valid = false;
					}
					// -- BOC Brut-force protection. count failedAttempt - 16.09.2017.
				}
				//  -- BOC encrypt password - 16.09.2017.	
			}
		}	
	}
/*	
    session_start(); 
    echo "<h3> PHP List All Modise-2 Session Variables</h3>";
    foreach ($_SESSION as $key=>$val)
    echo $key." ".$val."<br/>";
*/

// -- Generate Captcha Image
function Image($value)
{

	 $generateImage = '';
	 if (!empty($_POST)) 
	 {
	 if (isset($_POST['login']))
	   {$generateImage = 'No';}
	 }
	
	 if( $generateImage == '')
	 {
	$string = '';
	for ($i = 0; $i < 5; $i++) 
	{
		$randomCharacter = rand(97, 122);
		$string .= chr($randomCharacter);
		//echo "<script type='text/javascript'>alert('".$string.$randomCharacter."')</script>";
	}

	$_SESSION['captcha'] = $string; //store the captcha
	$_SESSION['value'] = $value;
	$dir = 'fonts/';
	$image = imagecreatetruecolor(165, 50); //custom image size
	$font = "PlAGuEdEaTH.ttf"; // custom font style
	$color = imagecolorallocate($image, 113, 193, 217); // custom color
	$white = imagecolorallocate($image, 255, 255, 255); // custom background color
	imagefilledrectangle($image,0,0,399,99,$white);
	imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

	// Enable output buffering
	ob_start();
	imagepng($image);
	// Capture the output
	$imagedata = ob_get_contents();
	// Clear the output bufferx
	ob_end_clean();
	$_SESSION['image'] = $imagedata;
	}
	else
	{
	$imagedata = $_SESSION['image'];
	}
		
	return $imagedata;
}

 ?>
 <style>
 .message 
 {
	color: #333;
	border: #FF0000 1px solid;
	background: #FFE7E7;
	padding: 5px 20px;
 }
 </style>
<div class="container background-white bottom-border">
                    <div class="container">
                        <div class="row margin-vert-30">
                            <!-- Login Box -->
                            <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
								 <FORM class="login-page" METHOD="post" action=<?php echo "".$_SERVER['REQUEST_URI']."";?>>

									<?php if($message!="") { ?>
										<div class="message"><?php echo $message; ?></div>
										<br/>
									<?php } ?>
								 
                                    <div class="login-header margin-bottom-30">
                                        <h2 class="text-center">Login to your account</h2>
                                    </div>
									
									<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <h3>ID/Passport Number<span style='color:red'>*</span></h3> 
									<div class = "input-group">
									<span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
										<input style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>">
									</div>
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
								</div>
								
								<!-- Password  -->
								<div class="control-group <?php echo !empty($password1Error)?'error':'';?>">
									 <h3>Password<span style='color:red'>*</span></h3> 	
									<div class = "input-group">
									<span class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </span>
										<input style="height:30px"  class="form-control margin-bottom-20"  id="password1" name="password1" type="password"  placeholder="Password" value="<?php echo !empty($password1)?$password1:'';?>"  onchange="CreateUserID()">
									</div>
									<?php if (!empty($password1Error)): ?>
										<span class="help-inline"><?php echo $password1Error; ?></span>
									<?php endif; ?>	
								</div>	
								
								<!-- New Captcha -->
								<h3>Please enter the verification code shown below.</h3>
								<div id='captcha-wrap'>
									<!-- <img src="data:image/png;base64,".base64_encode(Image())" alt='' id='captcha' /> -->
									 <?php echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' />
											<img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>"; 
									  ?>								
								</div>

								<div class='control-group <?php echo !empty($captchaError)?'error':'';?>'>
											<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value="<?php echo !empty($captcha)?$captcha:'';?>">
											
											<?php if (!empty($captchaError)): ?>
										<span class="help-inline"><?php echo $captchaError; ?></span>
									<?php endif; ?>
											
								</div>
								<!-- End New Captcha -->
                     				
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- <label class="checkbox">
                                                <input type="checkbox">Stay signed in</label> -->
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-primary pull-right" value="login" name="login" type="submit">Login</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <h4>Forgot your Password ?</h4>
                                      <!-- <p>
                                      <a href="#">Click here</a>to reset your password.</p> -->
										<a id="password-trigger" href="forgotpassword">Forgot your password?</a>
                                </form>						
                            </div>
                            <!-- End Login Box -->
                        </div>
                    </div>
                </div>
<?php  
// -- Database Connections  

//require 'database.php'; 
if(isset($_POST['login']))  
{  
	$userid = $_POST['idnumber']; 
if($valid) //Validated
{
	 $pdo = Database::connectDB();
	 $tbl_name="user"; // Table name 
	 $tbl_customer ="customer"; // Table name 
	 $userid = $_POST['idnumber']; 
	 $password = $_POST['password1']; 
	 $check_user = "SELECT * FROM $tbl_name Inner Join $tbl_customer on $tbl_name.userid = $tbl_customer.customerid  WHERE userid='$userid'";//-- and password='$password'";
	 
	 mysql_select_db('ecashpdq_eloan',$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

// -- Read all the Loan Type Interest Rate Package.
// -- Loan Types (27.01.2017) - All Records

// -- Mysql_num_row is counting table row
	$count = mysql_num_rows($result);
// -- echo "<h1>Count - $count</h1>";

	if($count >= 1)
    {  
			$row = mysql_fetch_array($result);
  			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.  
			$_SESSION['email'] = $row['email'];
			$_SESSION['customerLoggedIn'] = true;

			// Title
			$_SESSION['Title'] = $row['Title'];	
			// First name
			$_SESSION['FirstName'] = $row['FirstName'];
			// Last Name
			$_SESSION['LastName'] = $row['LastName'];
			// Phone numbers
			$_SESSION['phone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];
			
			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];
			
			// Dob
			$_SESSION['Dob'] = $row['Dob'];
			
			// phone
			$_SESSION['phone'] = $row['phone'];
			
// -- =============================== Global Settings Values for Email Template ======================== -- //
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();
	
	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$_SESSION['GlobalCompany']  = $data['name'];
		$_SESSION['Globalphone']    = $data['phone'];
		$_SESSION['Globalfax']      = $data['fax'];
		$_SESSION['Globalemail']    = $data['email'];
		$_SESSION['Globalwebsite']  = $data['website']; 
		$_SESSION['Globallogo']     = $data['logo'];
		$_SESSION['GloballogoTemp'] = $logo;
		$_SESSION['Globalcurrency'] = $data['currency']; 
		$_SESSION['Globallegalterms'] = $data['legaltext'];

		$_SESSION['adminemail']  =  $data['email'];
		$_SESSION['companyname'] =  $data['name'];

		$_SESSION['Globalstreet'] = $data['street'];
		$_SESSION['Globalsuburb'] = $data['suburb'];
		$_SESSION['Globalcity'] = $data['city'];
		$_SESSION['Globalstate'] = $data['province'];
		$_SESSION['Globalpostcode'] = $data['postcode'];	
		$_SESSION['Globalregistrationnumber'] = $data['registrationnumber'];
		
		// ----- BOC -- 2017.06.04 ---------- //
		$_SESSION['terms'] = $data['terms'];	
		$_SESSION['ncr'] = $data['ncr'];
		// ----- EOC -- 2017.06.04 ---------- //
	}	
	
	// -- Creating Login Session Timeout - 30.09.2017.
		$_SESSION['loggedin_time'] = time();  
		$location = "index?hash=".md5(date("h:i:sa"));	
	// -- Creating Login Session Timeout - 30.09.2017.
// -- =============================== 24.04.2017 - EOC ================================================= -- //			
     echo "<script>window.open('".$location."','_self')</script>";  
	} 
} 
}
?>  