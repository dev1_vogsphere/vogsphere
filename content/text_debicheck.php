<div class='container background-white bottom-border'>
	<div class='row margin-vert-30'>
		 <div class="col-md-12 text-left">

			 <i><h2 class="fa fa-thumbs-o-up fa-3x color-primary padding-top-10 animate fadeIn"> DebiCheck</h2></i>
		 </br>
		 </br>
			 <p class="p1" class="animate fadeIn">DebiCheck debit order service require your customers to electronically confirm the DebiCheck debit order details before you debit their accounts.  Once the customer approves the mandate, the debit order may be collected from the customer’s account without being disputed. DebiCheck allows up to 10 days tracking to improve success rate.</p>
			 <p>
			<button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Subscribe today</font></button>
			</p>
		 </div>
	 </div>
</div>
