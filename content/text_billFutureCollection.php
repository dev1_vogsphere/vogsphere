<?php
// -- Constant Values
// DataTable : http://live.datatables.net/yeropoku/1/edit
$text_delete = "<span class='glyphicon glyphicon-trash'></span>";
$text_edit   = "<span class='glyphicon glyphicon-pencil'></span>";
$text_save   = "<span class='glyphicon glyphicon-floppy-disk'></span>";
$text_cancel = "<span class='glyphicon glyphicon-ban-circle'></span>";
$text_restore = "<span class='glyphicon glyphicon-ok'></span>";

// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
if(!function_exists('getaccountdescPhp'))
{
	function getaccountdescPhp($typeid,$Account_TypeArr )
	{
		   $typedesc = '';
		  foreach($Account_TypeArr as $row)
		  {
		    $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
		  
		    if($rowData[0] == $typeid)
		    {
				$typedesc = $rowData[1];
			}
		  }
		  return $typedesc;
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

/*Rules
1. Action Date must be greater than today's date.
*/
// Logged user details
$changedby 			   = getsession('username');
$changedon 			   = date('Y-m-d');
$changedat	           = date('h:m:s');
$Client_Id = '';
$collectionid = '';
$FromDate= '';
$ToDate='';
$sessionrole='';
$UserCode = '';
$userid = '';

$Allstatus = '';
$status    = '';
$Allbranch = ''; //22/02/2022
$AllServiceMode = '';
$ServiceMode    = '';
$srUserCode0     = '';
$display = '';

$AllServiceType = '';
$ServiceType    = '';

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}
$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$message = '';

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';


  if(isset($_SESSION['IntUserCode']))
    {
    	$Client_Id=$_SESSION['IntUserCode'];
    }
   
////////////////////////////////////////////////////////////////////////
// -- From Single Point Access 
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	
	if(empty($Client_Id)){$Client_Id = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
	if(empty($sessionrole)){$sessionrole = $params[3];}	
}	
if(empty($changedby)){ $changedby = $userid;}
	//echo $Client_Id.' - '.$UserCode.' - '.$userid.' - '.$sessionrole; 
////////////////////////////////////////////////////////////////////////	
//echo $param;

// -- Connect to 
// -- Database Declarations and config:
	$pdo = Database::connectDB();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result = null;
	$stat  = "('pending','Stopped Payment')";
	$today = date('Y-m-d');
	$sql   = '';
	//echo $sessionrole.$Client_Id;
    if($sessionrole=="admin")
    {	
		$stat  = "('pending','Stopped Payment')";
        $sql ="SELECT * FROM collection WHERE 
		Status_Description IN {$stat} and Action_Date >= ? 
		ORDER BY Action_Date AND Status_Description DESC";
		$q = $pdo->prepare($sql);
		$q->execute(array($today));		
		$result = $q->fetchAll();		
    }
    else
	{// '$today'  '$Client_Id' 'pending'
		// -- Check if its a main branch searching via UserCode...
        $sql = "SELECT * FROM collection WHERE UserCode = ? and 
		Status_Description IN {$stat} and Action_Date >= ?
		ORDER BY Action_Date AND Status_Description DESC ";
		$q = $pdo->prepare($sql);
		$q->execute(array($Client_Id,$today));		
		$result = $q->fetchAll();
		// -- 23/02/2022
		// -- if its not a main branch then check its subranch detail.
		$records = count($result);
		if($records <= 0 )
		{
		    $sql = "SELECT * FROM collection WHERE IntUserCode = ? and 
			Status_Description IN {$stat} and Action_Date >= ?
			ORDER BY Action_Date AND Status_Description DESC ";
			$q = $pdo->prepare($sql);
			$q->execute(array($Client_Id,$today));		
			$result = $q->fetchAll();	
		}
    }	
	
	function gettodate()
	{
		$effectiveDate = date('Y-m-d');
		//echo $effectiveDate;
$effectiveDate = date('Y-m-d', strtotime(' + 3 months'));
		
		return $effectiveDate;
	}	
	
// BOC -- Get sub branches 22/02/2022
$branchesArr = array();
$pdoObj = Database::connectDB();
$branchesArr = getsubbranches($pdoObj,$Client_Id);
// EOC -- Get sub branches 22/02/2022
?>

<!--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>-->

<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
-->
    <style>
#wrapper 
		{
			width: 50%;
			position: fixed;
			z-Index:1;
			top:43%;
		}
	
.divider{
    width:5px;
    height:auto;
    display:inline-block;
}
    </style>
<!-- cdn js  --> 		
<script type="text/javascript" src="assets/js/3.3.1/jquery-3.3.1.js"></script>
<script type="text/javascript" src="assets/js/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.bootstrap.min.js"></script>

<!-- cdnjs js -->
<script type="text/javascript" src="assets/js/datatables/jszip.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/vfs_fonts.js"></script>

<!-- cdn js  -->
<script type="text/javascript" src="assets/js/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.colVis.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/sum%28%29.js"></script>

<div class="container background-white bottom-border">
 <div class='row margin-vert-30'>
  <h2 align="center">Scheduled Debit Orders</h2>
  </br>
  </br>
<!-- Filter Buttons -->
<div class="portfolio-filter-container margin-top-20">
    <ul class="portfolio-filter">
        <li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            filter by:
        </li>
        <li>
		  <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='My Reference' class='form-control' type='text' value="<?php echo $customerid;?>" />
        </li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            From:
        </li>
        <li> <!-- min="<?php echo date('Y-m-d');?>" -->
		 <input style='height:30px;z-index:0'  id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="<?php echo date('Y-m-d');?>" />
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            To:
        </li>		
        <li>
         <input style='height:30px;z-index:0' min="<?php echo date('Y-m-d');?>" id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo gettodate();?>" />
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Status:
        </li>		
        <li>
			<SELECT style='width:150px;height:30px' class="form-control" id="status" name="status" size="1" style='z-index:0'>
				<OPTION value='pending' <?php echo $status;?>>Pending</OPTION>	
				<OPTION value='Stopped Payment' <?php echo $status;?>>Stopped Payment</OPTION>				
			</SELECT>
		</li><br/>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Branch:
        </li>
		<li style="<?php echo $display; ?>;height:30px;z-index:0;">
		   <SELECT style="width:200px;height:30px;z-index:0;" class="form-control" id="srUserCode0" name="srUserCode0" size="1" style='z-index:0'>
				<OPTION value='' <?php echo $Allbranch;?>>All Branches</OPTION>
				<?php
					$datasrUserCodeSelect = $srUserCode0;
					foreach($branchesArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $datasrUserCodeSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
						  $companyname = $rowData[1];
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
					}
				?>
			</SELECT>
		</li>
     </ul>
</div> 
  </div>
<div class="table-responsive">
<div class="messages" id="message" name="message">
  		 <?php
		 if (isset($message)) 	
		 {
			 if($message == 'updated successfully')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);	
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);	
			 }		
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}					
			}				
		 }
		 ?>
  </div>  
  
  <div id="wrapper" name="">
  </div> 			
  <input style="display:none" type ="text" id = "changedby" value="<?php echo $changedby; ?>" readonly />
  <input style="display:none" id = "changedon" value="<?php echo $changedon; ?>" readonly />
  <input style="display:none" id = "changedat" value="<?php echo $changedat; ?>" readonly />

<table id="futurecollections" class="display table table-striped table-bordered" style="font-size: 10px">

      <thead>
          <tr>
              <th style="display:none">Branch</th>  
              <th>Reference</th>
              <th>Account Holder</th>
              <th>Account Number</th>
			  <th>Account Type</th>			  
              <th>Branch Code</th> 
              <th>Amount</th>
              <th>Action Date</th>
			  <th>Status</th>
              <th style="display:none">Status Code</th> 
			  <th style="display:none">No of debit orders</th>		  
			  <th>Action</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th style="display:none"></th>	  
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>	
                      <th></th>		
					  <th></th>						  
                      <th style="display:none"></th>	
                      <th style="display:none"></th>						  
                      <th></th>					  
                  </tr>
      </tfoot>
      <?php
if(!empty($result)){
       // while($row = mysqli_fetch_array($result))
        foreach($result as $row )
		{
			
			$collectionid = $row["collectionid"];
			$id ="$collectionid";
			$idAccount_Holder = "Account_Holder$collectionid";
			$idAccount_Number = "Account_Number$collectionid";
			$idBranch_Code = "Branch_Code$collectionid";
			$idAmount = "Amount$collectionid";
			$idAction_Date = "Action_Date$collectionid";
			$idAccount_Type = "Account_Type$id";
			$idNo_Payments  = "No_Payments$id";
			$descAccType = getaccountdescPhp($row["Account_Type"],$Account_TypeArr );			
			$idStatus_Code  = "Status_Code$id";
			$idStatus_Description  = "Status_Description$id";

		  echo '<tr id = "r'.$id.'" name = "r'.$id.'" >';
          echo 
		  '
		    <td width="10%" style="display:none">'.$row["IntUserCode"].'</td>
            <td width="10%">'.$row["Client_Reference_1"].'</td>
            <td width="18%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Holder.'" value="'.$row["Account_Holder"].'" readonly />'.$row["Account_Holder"].'</td>
            <td width="18%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Number.'" value="'.$row["Account_Number"].'" readonly />'.$row["Account_Number"].'</td>
            <td width="15%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Type.'" value="'.$row["Account_Type"].'" readonly />'.$descAccType.'</td>            
			<td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idBranch_Code.'" value="'.$row["Branch_Code"].'" readonly />'.$row["Branch_Code"].'</td>
            <td width="5%"><input type ="text" style="font-size: 10px;width:100%" id = "'.$idAmount.'" value="'. $row["Amount"].'" readonly />'.$row["Amount"].'</td>
            <td width="10%"><input style="font-size: 10px;width:80%" type ="date" id = "'.$idAction_Date.'" value="'.$row["Action_Date"].'" readonly /><br/>'.$row["Action_Date"].'</td>
            <td width="10%" ><input style="font-size: 10px;width:80%" type ="text" id = "'.$idStatus_Description.'" value="'.$row["Status_Description"].'" readonly />'.$row["Status_Description"].'</td>
            <td style="display:none" width="10%"><input type ="text" id = "'.$idStatus_Code.'" value="'.$row["Status_Code"].'" readonly />'.$row["Status_Code"].'</td>			
			<td style="display:none" width="10%"><input type ="text" id = "'.$idNo_Payments.'" value="'.$row["No_Payments"].'" readonly />'.$row["No_Payments"].'</td>';
			//echo '&nbsp;';
			if( $row["Action_Date"] > date('Y-m-d'))
			{ 
					if ($row["Status_Description"] == "Stopped Payment")
					{
					echo '<td width="15%"><a class="btn btn-success" id = "'.$id.'" data-toggle="tooltip"  title="Edit" onclick="toggleUpdate(this);" style="display:inline">'.$text_edit.'</a>';
					echo '<div class="divider"></div>';				
					echo '<a class="btn btn-danger" id = "c'.$id.'" name = "c'.$id.'" data-toggle="tooltip" title="Cancel" onclick="cancelUpdate(this);" style="display:none" >'.$text_cancel.'</a>';
					echo '<div class="divider"></div>';
					echo '<a   class="btn btn-success" id = "Rtop'.$id.'" name = "Rtop'.$id.'" data-toggle="tooltip" title="Restore" onclick="ResumeUpdate(this);" style="display:inline" >'.$text_restore.'</a></td>';
						
					}
					else
					{

					echo '<td width="15%"><a class="btn btn-success" id = "'.$id.'" data-toggle="tooltip" title="Edit" onclick="toggleUpdate(this);" style="display:inline">'.$text_edit.'</a>';
					echo '<div class="divider"></div>';				
					echo '<a class="btn btn-danger" id = "c'.$id.'" name = "c'.$id.'" data-toggle="tooltip" title="Cancel"  onclick="cancelUpdate(this);" style="display:none" >'.$text_cancel.'</a>';
					echo '<div class="divider"></div>';
					echo '<a   class="btn btn-danger" id = "Stop'.$id.'" name = "Stop'.$id.'" data-toggle="tooltip"  title="Stop" onclick="StopUpdate(this);" style="display:inline" >'.$text_delete.'</a></td>';
					}
			}
			else
			{
				echo "<td>";
				echo "<p></p>";
				echo '&nbsp;';
				echo "<p></p>";
				echo '&nbsp;';
				echo "<p></p>";
				echo "</td>";
			}
			echo '</td></tr>';
        }
}	
      ?>


  </table>
    </div>
    </br>
</div>

<script>
$(document).ready(function(){
var table =$('#futurecollections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,
pageResize: true,
"lengthMenu": [ [10, 25, 50, 100,-1], [10, 25, 50,100, "All"] ],
buttons: 
[	
 {
	extend: 'excelHtml5', footer: true,header:true,
	title : 'Scheduled Debit Orders',
	exportOptions : {
    modifier : {
                    // DataTables core
                    order  : 'index', // 'current', 'applied',
                    //'index', 'original'
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            },
	customize: function( xlsx ) 
	{
		var sheet   = xlsx.xl.worksheets['sheet1.xml'];
		$('row c', sheet).attr( 's', '50' );
        $('row:first c', sheet).attr( 's', '2' );
	    $('row:last c', sheet).attr( 's', '42' );
		var strLast = $('row:last t', sheet).text();
		var res     = '';
		var n 	    = strLast.indexOf("Grand Total");
		var resStr  = strLast.substr(n);
		//if(strLast.includes("Page Total"))
		{
			strLast = strLast.replace("Page Total", "Total");
			res 	= strLast.replace(resStr, "");	
			res 	= res.trim();			
			$('row:last t', sheet).text(res);			
		}
		var amountColLength = res.length;
		if(amountColLength == 0){amountColLength = 0;}
		var col = $('col', sheet);
/* All column col.each(function () {$(this).attr('width', 30);}); */
		 $(col[0]).attr('width', 21);
		 $(col[2]).attr('width', 16);
		 $(col[3]).attr('width', 16);			 
		 $(col[4]).attr('width', 12);	
		 $(col[5]).attr('width', amountColLength);	
		 $(col[6]).attr('width', 11);	
		 $(col[7]).attr('width', 11);	
		 
		$('row', sheet).each(function(x) 
        {
		});
	}			
}
,
{
	 extend: 'pdfHtml5', footer: true,orientation: 'landscape',pageSize: 'LEGAL',
	 title : 'Scheduled Debit Orders',
	 exportOptions : {
     modifier : {
                    order  : 'index', // 'current', 'applied',
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            }
}
,'csv', 
 // -- 'print'
 { 
	 extend: 'print',footer: true,orientation: 'landscape',pageSize: 'LEGAL',
	 title : 'Scheduled Debit Orders',
	 exportOptions : {
     modifier : {
                    order  : 'index', // 'current', 'applied',
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            }	
 }
],
"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) 
		{
			
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
				var start = b.indexOf("<");
				var end = b.indexOf(">") + 1;
				b = b.substring(end);
				//alert(b);
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
				var start = b.indexOf("<");
				var end = b.indexOf(">") + 1;
				b = b.substring(end);
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 6 ).footer() ).html(
      'Page Total  R'+pageTotal.toFixed(2)+'        '+'    Grand Total R'+ total.toFixed(2)
        );

    }
	,
	initComplete:function () 
	{
		$.fn.dataTable.ext.search.push
		(
		function (settings, data, dataIndex){
			var min = Date.parse( $('#min').val(), 10 );
			var max = Date.parse( $('#max').val(), 10 );
			var Action_Date=Date.parse(data[7]) || 0;
			var srUserCode0  = data[0];

				//alert(srUserCode);
			if ( ( isNaN( min ) && isNaN( max ) ) ||
				 ( isNaN( min ) && Action_Date <= max ) ||
				 ( min <= Action_Date   && isNaN( max ) ) ||
				 ( min <= Action_Date   && Action_Date <= max ) )
			{
			   //alert(settings.nTable.id);
			  //location.reload();
			  $(document).ready();
				return true;

			}
			//alert("false");
			return false;
		}
	  );
	}
});

table.buttons().container().appendTo('#futurecollections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {table.draw();});

$('#customerid').on( 'keyup', function () 
{
      table.search( this.value ).draw();
});

// -- 22/02/2022
$('#srUserCode0').on('change', function()
{
   table.search( this.value ).draw();
});

$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});
</script>

<script>
// #myInput is a <input type="text"> element
function toggleUpdate(ele) 
{
	bid   = "Branch_Code"+ele.id;
	acid  = "Account_Type"+ele.id;
	achid = "Account_Holder"+ele.id;
	
	acnid 	 = "Account_Number"+ele.id;
	amtid 	 = "Amount"+ele.id;
	acdateid = "Action_Date"+ele.id;
	nopay 	 = "No_Payments"+ele.id;
	
	bvalue = '';
	var id = ele.id;
	
	// -- Constant Values
	text_delete = '<span class="glyphicon glyphicon-trash"></span>';
	text_edit   = '<span class="glyphicon glyphicon-pencil"></span>';
	text_save   = '<span class="glyphicon glyphicon-floppy-disk"></span>';
	text_cancel = '<span class="glyphicon glyphicon-ban-circle"></span>';
	
	if(ele.innerHTML == text_save || ele.innerHTML == text_cancel || ele.innerHTML == text_delete )
	{
		branchcode = document.getElementById(bid);
		bvalue = branchcode.value;
		// -- Branch Code outerhtml
		branchcode.readOnly = true;
		branchcode.outerHTML = "<input style='font-size: 10px;width:80%' type ='text' id = '"+bid+"' value="+bvalue+" readOnly />";

		// -- Account Type
		Account_Type = document.getElementById(acid);
		acvalue = Account_Type.value;
		Account_Type.readOnly = true;
		Account_Type.outerHTML = "<input style='font-size: 10px;width:80%' type ='text' id = '"+acid+"' value="+acvalue+" readOnly />";
		if(ele.innerHTML == text_save || ele.innerHTML == text_delete )
		{updatecollection(id);} 		
		// -- Account Holder
		Account_Holder = document.getElementById(achid);
		ahvalue = Account_Holder.value;
		Account_Holder.readOnly = true;	
		
		// Account_Number
		Account_Number = document.getElementById(acnid);
		Account_Number.readOnly = true;	
		
		// Amount
		Amount = document.getElementById(amtid);
		Amount.readOnly = true;	
		
		// Action_Date
		Action_Date = document.getElementById(acdateid);
		Action_Date.readOnly = true;	
		
		// no of payments
		No_Payments = document.getElementById(nopay);
		No_Payments.readOnly = true;
		
		// -- none display of cancel.
		id_ = "c"+ele.id;
		document.getElementById(id_).style.display = 'none';

		// -- display of Stop.
		id_ = "Stop"+ele.id;
		document.getElementById(id_).style.display = 'inline';
		// -- stop payment was actioned remove the edit button & stop buttons
		if(ele.innerHTML == text_delete)
		{
			ele.style.display = 'none';
			document.getElementById(id_).style.display = 'none';
		}
		
		ele.innerHTML = text_edit;
	}
	else 
	{
		ele.innerHTML = text_save;
		// -- Branch code outerhtml
		branchcode = document.getElementById(bid);
		bvalue = branchcode.value;
		branchcode.readOnly = false;
		id_ = bid;
		
		Arr = <?php echo json_encode($Branch_CodeArr); ?>;
		new_html_el = "";
		new_html_el = "<select style='font-size: 10px;width:80px' class='form-control' id='"+id_+"' name='"+id_+"'>";
		var value = ""+document.getElementById(id_).value;
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			 
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
	   
		new_html_el = new_html_el+"</select>";
		branchcode.outerHTML = new_html_el;
		
		// -- Account Type
		Account_Type = document.getElementById(acid);				
		Account_Type.readOnly = false;
		acvalue = Account_Type.value;
		value = acvalue;
		Arr = <?php echo json_encode($Account_TypeArr); ?>;
		new_html_el = "";
		new_html_el = "<select style='font-size: 10px;width:100px' class='form-control' id='"+acid+"' name='"+acid+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			 
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		Account_Type.outerHTML = new_html_el;
		
		// -- Account Holder
		Account_Holder = document.getElementById(achid);
		ahvalue = Account_Holder.value;
		Account_Holder.readOnly = false;	

		// Account_Number
		Account_Number = document.getElementById(acnid);
		Account_Number.readOnly = false;	
		
		// Amount
		Amount = document.getElementById(amtid);
		Amount.readOnly = false;	
		
		// Action_Date
		Action_Date = document.getElementById(acdateid);
		Action_Date.readOnly = false;	
		
		// no of payments
		No_Payments = document.getElementById(nopay);
		No_Payments.readOnly = false;	
		
		// -- display of cancel.
		id_ = "c"+ele.id;
		//style="font-size: 10px;" 
		//document.getElementById(id_).style.fontSize  = '10px';
		document.getElementById(id_).style.display = 'inline';

		// -- none display of Stop.
		id_ = "Stop"+ele.id;
		document.getElementById(id_).style.display = 'none';
	}
} 


function updatecollection(id) 
{
	//alert(id);
	var collectionid 	 = id;
	var Account_Holder   = document.getElementById("Account_Holder"+id).value;
	var Account_Type	 = document.getElementById("Account_Type"+id).value;
	var Account_Number   = document.getElementById("Account_Number"+id).value;
	var Branch_Code		 = document.getElementById("Branch_Code"+id).value;	
	var No_Payments		 = document.getElementById("No_Payments"+id).value;
	var Action_Date		 = document.getElementById("Action_Date"+id).value;
	var Amount			 = document.getElementById("Amount"+id).value;	

	var  changedby = document.getElementById("changedby").value;
	var  changedon = document.getElementById("changedon").value;
	var  changedat = document.getElementById("changedat").value;

	var Status_Code			 = document.getElementById("Status_Code"+id).value;	
	var Status_Description			 = document.getElementById("Status_Description"+id).value;	
	
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"updatecollection.php",
				       data:{collectionid:collectionid,Account_Holder:Account_Holder,
							Account_Type:Account_Type,Account_Number:Account_Number,Branch_Code:Branch_Code,
							No_Payments:No_Payments,Action_Date:Action_Date,Amount:Amount,Status_Code:Status_Code,
							Status_Description:Status_Description,changedby:changedby,changedon:changedon,
							changedat:changedat},
						success: function(data)
						 {	
							//jQuery("#message").html(data);
							var className  = 'alert alert-success fade in';								  
							var text = data;
							add_reporthere(className,text);
						 }
					});
				});				  
			  return false;
}

function cancelUpdate(ele)
{
	var id = ele.id;//substr(ele.id,1);
	id = id.substr(1);
	
		// -- Constant Values
	text_cancel = '<span class="glyphicon glyphicon-ban-circle"></span>';
	// -- Edit.
	ele2 = document.getElementById(id);
	document.getElementById(id).innerHTML = text_cancel;
	
	toggleUpdate(ele2);
}

function StopUpdate(ele)
{
	var id = ele.id;//substr(ele.id,1);
	//alert(id);
	id = id.substr(4);
	//alert("Test");
	var Status_Code			 = document.getElementById('Status_Code'+id);	
	var Status_Description   = document.getElementById('Status_Description'+id);	
	var nopay 	 = document.getElementById("No_Payments"+id);

	var rowChanged  = document.getElementById("r"+id);		
	var table = document.getElementById("futurecollections");
	//alert(id);
	// -- Constant Values
	text_delete = '<span class="glyphicon glyphicon-trash"></span>';
	feedback  = 'Are you sure you want to Stop this Debit Order?';
	if (confirm(feedback)) 
	{
	// -- stop debit order payments.
	Status_Code.value  = '04';
	Status_Description.value = 'Stopped Payment';
	nopay.value = 1;
	//alert(nopay.value);
	// -- Edit.
	ele2 = document.getElementById(id);
	document.getElementById(id).innerHTML = text_delete;
	//alert("Going In 2");
	toggleUpdate(ele2);
	table.deleteRow(rowChanged.rowIndex);
	}
}
function ResumeUpdate(ele)
{
	var id = ele.id;//substr(ele.id,1);
	id = id.substr(4);
	
	var Status_Code			 = document.getElementById("Status_Code"+id);	
	var Status_Description   = document.getElementById("Status_Description"+id);	
	var nopay 	 = document.getElementById("No_Payments"+id);

	var rowChanged  = document.getElementById("r"+id);		
	var table = document.getElementById("futurecollections");

	// -- Constant Values
	text_delete = '<span class="glyphicon glyphicon-trash"></span>';
	feedback  = 'Are you sure you want to restore this Debit Order?';
		if (confirm(feedback)) 
		{
	// -- Resume debit order payments.
	Status_Code.value  = '01';
	Status_Description.value = 'pending';
	nopay.value = 1;
	
	// -- Edit.
	ele2 = document.getElementById(id);
	document.getElementById(id).innerHTML = text_delete;
	
	toggleUpdate(ele2);
	table.deleteRow(rowChanged.rowIndex);							
		}
}
function add_reporthere(className,text)
{
	// -- CREATE DIV	
	html = "<div id='saved' name='saved' class='"+className+"'>"
		  +"<a href='#' class='close' data-dismiss='alert'>×</a>"
		  +text
		  +"</div>";
	document.getElementById("wrapper").innerHTML = html;
}

function getaccountdesc(Account_Type)
{
	Arr = <?php echo json_encode($Account_TypeArr);?>;	
	Account_TypeDesc = '';
	for (var key in Arr)
	{ 
	   row = new Array();
	   row = Arr[key].split('|');
	   if(Account_Type == row[0])
	   {
		   Account_TypeDesc = row[1];
	   }
	}
	
	return Account_TypeDesc;
}
</script>
