<?php 
	require 'database.php';
	
	$placeholderid = null;
	$table = 'placeholder';
	
	if ( !empty($_GET['placeholderid'])) 
	{
		$placeholderid = $_REQUEST['placeholderid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$placeholderid = $_POST['placeholderid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM $table WHERE placeholderid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($placeholderid));
			Database::disconnect();
			echo("<script>location.href='placeholders.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a Placeholder
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="placeholderid" value="<?php echo $placeholderid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="placeholders.php">No</a>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
