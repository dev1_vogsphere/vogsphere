<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> -->
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<?php
if(!function_exists('getprotocol'))
{
	function getprotocol()
    {		
		$url = '';
		if ($_SERVER['HTTPS'] != "on")
			{
				$url = "http://";
			}
			else
			{
				$url = "http://";
			}
		return $url;	
	}
}
if(!function_exists('GET_CONVERTED_ACC_BANK'))
{
function GET_CONVERTED_ACC_BANK($dataBanks,$dataAccountType,$BankName,$AccountType)
{
	// -- Retrun Array Data...
	$data = null;
	// -- Accounts	----- //
	$accounttySelect  = 0;
	$accounttypeid    = '';
	$accounttySelect  = $AccountType;
	$accounttypedesc  = '';
	$count 		 	  = 0;
	$conv_bankname    = '';
	$conv_accounttype = ''; 
	
foreach($dataAccountType as $rowA)
{   
	$accounttypeid = $rowA['accounttypeid'];
	$accounttypedesc = $rowA['accounttypedesc'];
	// Select the Selected Role 
	if($accounttypeid == $accounttySelect)
	{
	   $conv_accounttype = $accounttypedesc;
	}
	$count= $count + 1;
}
//$conv_accounttype = $dataAccountType->rowCount();//$accounttySelect;//$dataAccountType->rowCount();//$AccountType ;
// -- Get the Account Type.
if(empty($conv_accounttype))
 {
	$conv_accounttype = "No Account Type";
 } 

	// -- Banks	----- //
	$bankid 	= '';
	$bankSelect 	= $BankName;
	$bankdesc 	= '';


foreach($dataBanks as $rowB)
{
$bankid   = $rowB['bankid'];
$bankdesc = $rowB['bankname'];
												
		if($bankid == $bankSelect)
		{
		   $conv_bankname = $bankdesc;
		}
}
										
if(empty($conv_bankname))
{
    $conv_bankname = 'No Banks';
} 

// -- Return Data
$data['conv_bankname'] = $conv_bankname;
$data['conv_accounttype'] = $conv_accounttype; 

	return $data;
}} 

if(!function_exists('GET_ADMIN_CUSTOMER_LOANS'))
{
   function GET_ADMIN_CUSTOMER_LOANS($dataInput,$pdo,$sqlSelect)
   {
	$data 			= null;
	$ExecApproval	= $dataInput['ExecApproval'];
	$customerid		= $dataInput['customerid'];
	$From			= $dataInput['From'];	
	$To				= $dataInput['To'];
	$sql 	    	= '';
//print_r($dataInput);//-- Debug
					  if(!empty($ExecApproval))
					   {
					   if($ExecApproval !== "*") // !== Not Identical Is Not *
					   {
					   					//	echo '<h2> CustomerId = '.$ExecApproval.'</h2>';
					   if(!empty($customerid))
						 {
						  if(!empty($From))
						  {
						  $sql = $sqlSelect.' WHERE L.CustomerId = ? AND  ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY L.CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval,$From,$To));
						  $data = $q->fetchAll();
						  }
						  else
						  {
						  $sql = $sqlSelect.' WHERE L.Customerid = ? AND  ExecApproval = ? ORDER BY L.CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval));
						  $data = $q->fetchAll();
						  }
						 }
					     else if(!empty($From))
						 {
					     $sql = $sqlSelect.' WHERE ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY L.CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($ExecApproval,$From,$To));
						 $data = $q->fetchAll();
						 }
						 else
						 {
						  $sql = $sqlSelect.' WHERE ExecApproval = ?';
						  echo '<br/>'.$sqlSelect;
						  $q = $pdo->prepare($sql);
					      $q->execute(array($ExecApproval));
						  //$data = $q->fetchAll(); Debug
						 }
					   }
					   else // -- ExecApproval is *
					   {
						 if(!empty($customerid))
						 {
						 if(!empty($From))
						 {
						  $sql = $sqlSelect.' WHERE L.CustomerId = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY L.CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$From,$To));
						  $data = $q->fetchAll();
						 }
						 else
						 {
						  $sql = $sqlSelect.' WHERE L.CustomerId = ? ORDER BY L.CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid));
						  $data = $q->fetchAll();
						 }
						 }
						 else if(!empty($From))
						 {
							 $sql = $sqlSelect.' WHERE DATE(DateAccpt) BETWEEN ? AND ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($From,$To));
							 $data = $q->fetchAll();
						 }
						 else
						 {
							// -- All Records
							$sql = $sqlSelect.' ORDER BY L.CustomerId DESC';
							$data = $pdo->query($sql);
						 }
					   }
					  }

	return $data;

}}

if(!function_exists('GET_CUSTOMER_LOANS'))
{
   function GET_CUSTOMER_LOANS($dataInput,$pdo,$sqlSelect)
   {
	$data 		= null;
	$ExecApproval	= $dataInput['ExecApproval'];
	$customerid	= $dataInput['customerid'];
	$From		= $dataInput['From'];	
	$To		= $dataInput['To'];
	
	if(!empty($ExecApproval))
	{
	 if($ExecApproval != "*")
	 {
		   if(!empty($From))
	 {
	   $sql = $sqlSelect.' WHERE L.Customerid = ? AND ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY L.CustomerId DESC ';
	   $q = $pdo->prepare($sql);
	   $q->execute(array($username,$ExecApproval,$From,$To));
	 }
	 else
	 {
	 $sql = $sqlSelect.' WHERE C.Customerid = ? AND ExecApproval = ? ORDER BY C.CustomerId DESC ';
	   $q = $pdo->prepare($sql);
	   $q->execute(array($username,$ExecApproval));
	 }
	//$data = $q->fetchAll();
	 }
	 else
	 {
	 if(!empty($From))
	 {
		 $sql = $sqlSelect.' WHERE C.Customerid = ? AND DATE(DateAccpt) BETWEEN ? AND ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($username,$From,$To));
	 }
	 else
	 {
	  $sql = $sqlSelect.' WHERE C.Customerid = ? ORDER BY C.CustomerId DESC ';
	    $q = $pdo->prepare($sql);
	    $q->execute(array($username));
	 }
	$data = $q->fetchAll();
	 }
	}
	return $data;
}}
if(!function_exists('GET_IMS_INVOICE_PAYMENTS'))
{
   function GET_IMS_INVOICE_PAYMENTS($dataInput,$sql)
   {
	$paymentid  	 = 0;
	$iddocument 	 = '';
	$path	    	 = '';
	$tot_paid   	 = 0.00;
	$tot_outstanding = 0.00;
	$repaymentAmount = null;	
	$dataInvoice 	 = null;
	$dataCommon      = null;
	$amount   	     = 0.00;
	$customerid      = $dataInput['customerid'];
	$ApplicationId   = $dataInput['ApplicationId'];
	$amount			 = $dataInput['amount'];

//-----------------------------------------------------------//
// 					IMS Web Services [10 April 2021]       --//
//-----------------------------------------------------------//
// -- This is for file download Action, uncomment if we need
// -- a file to be downloaded.
//-----------------------------------------------------------//
		/*if(strtolower($_SERVER['SERVER_NAME']) == 'localhost')
		{
		   $WebServiceURL = getprotocol()."localhost/ims/webservices/ws_invoicepayments.php?customerid=".$customerid."&ApplicationId=".$ApplicationId."&amount=".$amount;			
		}
		else
		{
			$WebServiceURL = getprotocol()."www.vogsphere.co.za/ims/webservices/ws_invoicepayments.php?customerid=".$customerid."&ApplicationId=".$ApplicationId."&amount=".$amount;			;			
		}
		$sxml = simplexml_load_file($WebServiceURL);
		foreach($sxml->invoice as $invoice)
		{
				$id  = $invoice->paymentid;
				$str = "  ";
				$id  = trim($id);
				if (strlen($id) > 0) 
				{
					
					$paymentid 		= $id;
					$iddocument		= $invoice->iddocument;
					$path			= $invoice->path;
					$tot_paid		= $invoice->tot_paid;
					$tot_outstanding = $invoice->tot_outstanding;
				}		
		}*/
//-----------------------------------------------------------//		
		$dataInvoice['paymentid'] 		= $paymentid ;
		$dataInvoice['iddocument'] 		= $iddocument;
		$dataInvoice['path'] 			= $path;
		$dataInvoice['tot_paid'] 		= $tot_paid;
		$dataInvoice['tot_outstanding'] = $tot_outstanding;

	return $dataInvoice;
}}
if(!function_exists('GET_ESTATEMENT_TABLE_ROWS'))
{
   function GET_ESTATEMENT_TABLE_ROWS($row,$dataRows)
   {
	$profit   	= 0.00;   
	$tot_income = 0.00;
	$tot_loan   = 0.00;
	$tot_repayment = 0.00;
	$html_row   = '';
						   		$html_row = $html_row.'<tr>';
								// ======================= send an eStatement - 23.04.2017 ============================//
								if($dataRows['role'] == 'admin')
								{
								$html_row = $html_row. '<td width=150>';
								$html_row = $html_row. '<button type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="setupLinks('.$dataRows['href'].')">eMail eStatements</button></td>';
								// ======================= send an eStatement - 23.04.2017 ============================//
								$html_row = $html_row. '&nbsp;';	
								}
								$html_row = $html_row. '<td width=150>';
								$html_row = $html_row. '<a class="btn btn-info" href="eInvoice.php?repayment='.$dataRows['repaymentEncoded'].'&customerid='.$dataRows['CustomerIdEncoded']. '&ApplicationId='.$dataRows['ApplicationIdEncoded']. '">eStatement Details</a></td>';
								$html_row = $html_row. '&nbsp;';	
								if($dataRows['role'] == 'admin')
								{
								$html_row = $html_row. '<td width=150>';
							   	$html_row = $html_row. '<a class="btn btn-info" href="payment.php?repayment='.$dataRows['repaymentEncoded'].'&customerid='.$dataRows['CustomerIdEncoded']. '&ApplicationId='.$dataRows['ApplicationIdEncoded']. '">ePayment Details</a></td>';
								$html_row = $html_row. '&nbsp;';
								}
							   	if($row['ExecApproval'] == 'APPR')
								{
								$html_row = $html_row. '<td>Approved</td>';
								}
								if($row['ExecApproval'] == 'FUND')
								{
								$html_row = $html_row. '<td>Funded</td>';
								}
								if($row['ExecApproval'] == 'PEN')
								{
								$html_row = $html_row. '<td>Pending</td>';
								}
								if($row['ExecApproval'] == 'CAN')
								{
								$html_row = $html_row. '<td>Cancelled</td>';
								}
								if($row['ExecApproval'] == 'REJ')
								{
								$html_row = $html_row. '<td>Rejected</td>';
								}
								if($row['ExecApproval'] == 'SET')
								{
								$html_row = $html_row. '<td>Settled</td>';
								}
								// -- Defaulted - 20.06.2018
								if($row['ExecApproval'] == 'DEF')
								{
								$html_row = $html_row. '<td>Defaulted</td>';
								}
								$html_row = $html_row.'&nbsp;';
								$html_row = $html_row.'<td>'. $row['CustomerId'] . '</td>'; // 1.
							   	$html_row = $html_row.'<td>'. $dataRows['Title'] . '</td>';			// 2.
							    $html_row = $html_row.'<td>'. $dataRows['FirstName']. '</td>';// 3.
							   	$html_row = $html_row.'<td>'. $dataRows['LastName']. '</td>';// 4.
							   	$html_row = $html_row.'<td>'. substr($dataRows['FirstName'],0,1). '</td>';//$row['MonthlyIncome'] 5.
							   	$html_row = $html_row.'<td>'.$row['monthlypayment']. '</td>';//$row['ReqLoadValue']  6.
								$html_row = $html_row.'<td>English</td>';// $row['Repayment'] 7.
							   	$html_row = $html_row.'<td>'. $row['FirstPaymentDate'] . '</td>';// 8.
							   	$html_row = $html_row.'<td>'. $row['LoanDuration'] . 'Months</td>';// 9.
							   	$html_row = $html_row.'<td>'. number_format($row['InterestRate']) . '% </td>';// 10.
							   	$html_row = $html_row.'<td>'. $row['FirstPaymentDate'] .'</td>';// 11.
							   	$html_row = $html_row.'<td>'. $row['branchcode'] . '</td>';// 12.
							   	$html_row = $html_row.'<td>'. $row['accountnumber'] . '</td>';// 13
							   	$html_row = $html_row.'<td>Vogsphere (Pty) Ltd</td>';// 14.
							   	$html_row = $html_row.'<td>'. $row['ApplicationId'] . '</td>';// 8.
							   	$html_row = $html_row.'<td>By client</td>';// 8.
							   	$html_row = $html_row.'<td>R'.$dataRows['tot_paid'].'</td>';// 8.
							   	$html_row = $html_row.'<td>R'. $dataRows['tot_outstanding']. '</td>';// 8.
							   	$html_row = $html_row.'<td>eloan</td>';// 8.
							   	$html_row = $html_row.'<td>'.$dataRows['conv_accounttype'].'</td>';// 8.
							   	$html_row = $html_row.'<td>'.$dataRows['LoanRequested'].'</td>';// 8. 
							   	$html_row = $html_row.'<td>'.$dataRows['RepaidAmount'].'</td>';// 8.
							   	$html_row = $html_row.'<td></td>';// 8.
							   	$html_row = $html_row.'<td>'. $dataRows['phone']. '</td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td> </td>';// 8.
							   	$html_row = $html_row.'<td></td>';// 8.
							   	$html_row = $html_row.'<td></td>';// 8.
							   	$html_row = $html_row.'<td></td>';// 8.
							   	$html_row = $html_row.'<td></td>';// 8.
							   	$html_row = $html_row.'<td>'.$dataRows['conv_bankname'].'</td>';// 8. // -- 2017.04.16
							   	$html_row = $html_row.'<td>'.$dataRows['conv_accounttype'].'</td>';// $row['accounttype'] 8. // -- 2017.04.16
							   	$html_row = $html_row.'<td>'.$dataRows['typecode'].'</td>';// 8.
								
								// -- Totals
								$tot_income 	= $tot_income  + $row['MonthlyIncome'];
								$tot_loan 		= $tot_loan  + $row['ReqLoadValue'];
								$tot_repayment  = $tot_repayment + $row['Repayment'];
								
								// --- if Role = CUSTOMER & Approval status is not pending(Customer cannot cancel it.)
								if($dataRows['role'] == 'customer' AND $row['ExecApproval'] != 'PEN')
								{
										$html_row = $html_row.'<td width=250>';
										$html_row = $html_row.'<a class="btn btn-info" href="read.php?customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '">Details</a>';
										$html_row = $html_row.'&nbsp;';
										$html_row = $html_row.'</tr>';
								}
								else
								{
							   	$html_row = $html_row.'</tr>';
								}
						// Total Profit
					   $profit = $tot_repayment - $tot_loan;
					   return $html_row;
   }
}

if(!function_exists('CONNECT_IMS_DB'))
{
function CONNECT_IMS_DB()
{
	require_once 'database.php';
	$pdo2 = null;
	$pdo2 = Database2::connect();
	$pdo2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo2;
}}
if(!function_exists('CONNECT_DB'))
{
function CONNECT_DB()
{
	require_once 'database.php';
	$pdo = null;
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
}}

// sample array data
$data = array();
$FirstName = '';

 //require 'database.php';
 $webpage = "customerLogin.php";
 $lv_username ="root";
 $lv_password = "root";
 $tbl_name="user"; // Table name
 $ExecApproval = '';
 $To = '';
 $From = '';
 $Filecontent = '';
 $eMailInvoices = array(); // -- 2017.04.23
 // -- 14.12.2018
 $REH   = '';
 $LEGAL = '';
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- require_once 'database.php'; 27.01.2018 -- //
	$display = 'X';
	if(empty($post))
	{
		require_once 'database.php';
		require_once 'database_invoice.php';
		require_once 'Runpaymentschedule.php';
	}				   
// -- Database Declarations and config:  
  $pdo = CONNECT_DB();
// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $q = $pdo->prepare($sql);
  $q->execute();
  $dataBanks = $q->fetchAll();						   						 
  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 
//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $q = $pdo->prepare($sql);
 $q->execute();
 $dataAccountType = $q->fetchAll();//$pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

// -- Export to Exce.
if(isset($_POST["ExportType"]))
{
// -- Filecontent data.
if (isset($_SESSION['Filecontent']))
{
	$data = $_SESSION['Filecontent'];
}
	switch($_POST["ExportType"])
    {
        case "export-to-excel" :
            // Submission from
			$filename = $_POST["ExportType"] . ".xls";
            header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			ExportFile($data);
			//$_POST["ExportType"] = '';
            exit();
		case "export-to-csv" :
            // Submission from
			$filename = $_POST["ExportType"] . ".csv";
/*echo $filename; */
		//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Expires: 0");
			ExportCSVFile($data);
			//$_POST["ExportType"] = '';
            exit();
        default :
            die("Unknown action : ".$_POST["action"]);
            break;
    }
	// -- flush ob.
	ob_end_flush();
}
function ExportCSVFile($records) 
{
	// create a file pointer connected to the output stream
	$fh = fopen( 'php://output', 'w' );
	$heading = false;
		if(!empty($records))
		  foreach($records as $row) {
			if(!$heading) {
			  // output the column headings
			  fputcsv($fh, array_keys($row));
			  $heading = true;
			}
			// loop over the rows, outputting them
			 fputcsv($fh, array_values($row));
		  }
		  fclose($fh);
}
function ExportFile($records) {
	$heading = false;
	if(!empty($records))
	  foreach($records as $row) {
		if(!$heading) {
		  // display field/column names as a first row
		  echo implode("\t", array_keys($row)) . "\n";
		  $heading = true;
		}
		echo implode("\t", array_values($row)) . "\n";
	  }
	exit;
}
// -- End Export Excel.

			// Id number
			$idnumber = $_SESSION['username'];
			// Title
			$Title = $_SESSION['Title'];
			// First name
			$FirstName = $_SESSION['FirstName'];
			// Last Name
			$LastName = $_SESSION['LastName'];
			// Phone numbers
			$phone = $_SESSION['phone'];
			// Email
			$email = $_SESSION['email'];
			// Street
			$Street = $_SESSION['Street'];
			// Suburb
			$Suburb = $_SESSION['Suburb'];
			// City
			$City = $_SESSION['City'];
			// State
			$State = $_SESSION['State'];
			// PostCode
			$PostCode = $_SESSION['PostCode'];
			// Dob
			$Dob = $_SESSION['Dob'];
			// phone
			$phone = $_SESSION['phone'];
			
			$icamefrom = "";
			
			if(isset($_SERVER['HTTP_REFERER']))
			{
				$icamefrom = $_SERVER['HTTP_REFERER'];
			}
			
	
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
{
// keep track validation errors
		$From = null;
		$To = null;
		$ExecApproval = null;
	    $LastName = null;
		$FirstName = null;
		$customerid = null;
// Filter Records
if ( !empty($_POST))
{
// Customer role
	if($_SESSION['role'] == 'customer')
	{
		$From = $_POST['From'];
		$To = $_POST['To'];
		$ExecApproval = $_POST['ExecApproval'];
	}
  else 	if($_SESSION['role'])// == 'admin') -- 15.12.2018 Fixing Khetiwe's bug Issue #5
	{
	// keep track post values
		$LastName = $_POST['LastName'];
		$FirstName = $_POST['FirstName'];
		$customerid = $_POST['customerid'];
		$ExecApproval = $_POST['ExecApproval'];
		$From = $_POST['From'];
		$To = $_POST['To'];
	}
}
else
{
// Coming From read & Update.
$ExecApproval = "*";
}
}
//}
 echo "<div class='container background-white bottom-border'>";		
//<!-- Search Login Box -->
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
	{
	 $Approved = '';
	 $Funded = '';
	 $Pending = '';
	 $Cancelled = '';
	 $Rejected = '';
	 $All = '';
	 $Settled = '';
// -- 14.12.2018
	 $REH   = '';
	 $LEGAL = '';
	 
	 if($ExecApproval == 'REH')
		{
		  $REH = 'selected';
		}	 
	 if($ExecApproval == 'LEGAL')
		{
		  $LEGAL = 'selected';
		}
		
// -- Defaulted - 20.06.2018.
	 $Defaulted = '';  
	 if($ExecApproval == 'DEF')
		{
		  $Defaulted = 'selected';
		}	 
	 if($ExecApproval == 'SET')
		{
		  $Settled = 'selected';
		}
	//<?php if ($ExecApproval == 'APPR') echo 'selected';
	if($ExecApproval == 'APPR')
		{
		  $Approved = 'selected';
		}
	if($ExecApproval == 'FUND')
		{
		  $Funded = 'selected';
		}
	if($ExecApproval == 'PEN')
		{
		   $Pending = 'selected';
		}
	if($ExecApproval == 'CAN')
		{
			$Cancelled = 'selected';
		}
	if($ExecApproval == 'REJ')
		{
		   $Rejected = 'selected';
		}
	if($ExecApproval == 'All')
		{
		   $All = 'selected';
		}
	if($ExecApproval == '')
		{
		   $All = 'selected';
		}
	  if($To == "")
	   {
		$To = "9999-12-31";
	   }
/* eCashMeUp - Performace : Fixing 19.07.2018 
			   Defdault to status approved*/	   
	   if($ExecApproval == '*')
		{
		  // $All = 'selected';
		   		  $Approved = 'selected';
				  $ExecApproval = 'APPR';

		}
/* eCashMeUp - Performace : Fixing 19.07.2018 
			   Defdault to status approved*/	   
	   
	// Customer role
	if($_SESSION['role'] == 'customer')
	{
		echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Filter Loan Application</h2>
                                    </div>
									<div class='row'>
									 <div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='APPR' $Funded>Funded</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
												<OPTION value='DEF'  $Defaulted>Defaulted</OPTION>
												<OPTION value='REH'  $REH>Rehabilitation</OPTION>
												<OPTION value='LEGAL'  $LEGAL>Legal</OPTION>												
										 </SELECT>
									</div>
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value=$To>
                                    </div>
                                   </div>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>";
	}
  else 	if(!empty($_SESSION['role']))// == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search Loan Accounts</h2>
                                    </div>
									<div class='row'>
									<div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='FUND' $Funded>Funded</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
												<OPTION value='DEF'  $Defaulted>Defaulted</OPTION>		
												<OPTION value='REH'  $REH>Rehabilitation</OPTION>
												<OPTION value='LEGAL'  $LEGAL>Legal</OPTION>																								
										 </SELECT>
									</div>
                                    <div class='col-sm-6'>
                                        <input style='height:30px' name='LastName' placeholder='Last Name' id='LastName' class='form-control margin-bottom-20' type='hidden' value=$LastName>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input style='height:30px' name='FirstName' placeholder='First Name' id='age' class='form-control margin-bottom-20' type='hidden' value=$FirstName>
                                    </div>
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value=$To>
                                    </div>
                                   </div>
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0;' id='customerid' name='customerid' placeholder='ID' class='form-control' type='text' value=$customerid>
                                    </div>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
											<input style='height:30px' name='display' id='display' value='X' hidden/>
											<input style='height:30px' name='html' id='html' value='X' hidden/> 
                                        </div>
                                    </div>
                                </form>
                            </div>";
	}
}
?>
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<!------------        Start Data -------->
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title" style="line-height:35px;">eStatements : export to Excel or CSV file  <div class="btn-group pull-right">
						  <button type="button" class="btn btn-info">Action</button>
						  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						    <span class="caret"></span>
						    <span class="sr-only">Toggle Dropdown</span>
						  </button>
						  <ul class="dropdown-menu" role="menu" id="export-menu">
						    <li id="export-to-excel"><a href="#">Export to excel</a></li>
							<li id="export-to-csv"><a href="#">Export to csv</a></li>
						  </ul>
						</div>
					</h3>
				</div>
				<div class="panel-body">
					<form action="eStatements" method="post" id="export-form">
						<input type="hidden" value='' id='hidden-type' name='ExportType'/>
				  	</form>
				</div>	
			</div>
		</div>
	</div>
</div>

<!-- ===================================================== Send An Email 23.04.2017 =========================== -->
<div class="container">
<?php if(getsession('role') == 'admin'){?>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="panel panel-default">
			<div class="panel-heading">
<h3 class="panel-title" style="line-height:35px;">eMail all the eStatements of APPROVED statuses for <?php $dateForm = date('F')."  ".date('Y'); echo $dateForm; ?><div class="btn-group pull-right">
			<button type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="setupLinks()">eMail eStatements</button>
			</div>
			</div>
		</div>
	</div>
</div>
<?php }?>
<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <h2>eMail eStatements - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
		</div>
		<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
<!-- ===================================================== End Send An Email ======================= -->
<script  type="text/javascript">
	$(document).ready(function() {
		jQuery('#export-menu li').bind("click", function()
		{
			var target = $(this).attr('id');
			//alert("Bona fela"+target);
			switch(target) {
				case 'export-to-excel.' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
				case 'export-to-csv' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
			}
		});
    });
</script>
		<!------------- End ------------------------->
		<div class="row" style="overflow-x:auto;">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p>
				  <div class="table-responsive" width="100%">
				<table class="table table-striped table-bordered" cellspacing="0" width="200px">
<table id="example" class="display nowrap" cellspacing="0" width="100%">-->
	                 <table id="" class="table table-striped table-bordered" style="white-space: nowrap;">
		              <thead>
							<tr>
							<?php if(getsession('role') == 'admin'){?>
								<th><b>eMail Statement</b></th> <!-- 0. 23.04.2017 - added to e-mail a client -->
							<?php }?>	
								<th><b>eStatement</b></th> <!-- 0. -->
							<?php if(getsession('role') == 'admin'){?>	
								<th><b>ePayment</b></th> <!-- 0. -->
							<?php }?>	
								<th><b>eLoan status</b></th> <!-- 0. -->
								<th><b>Acc</b></th> <!-- 1. -->
								<th>Title</th><!-- 2. -->
								<th><b>Surname</b></th><!-- 3. -->
								<th><b>First Name</b></th><!-- 4. -->
								<th><b>Intials</b></th><!-- 5. -->
								<th><b>Instalment Amount</b></th><!-- 6. -->
								<th><b>Language</b></th><!-- 7. -->
								<th><b>Cause Date</b></th><!-- 8. -->
								<th><b>Loan Duration</b></th><!-- 9. -->
								<th><b>Interest Rate</b></th><!-- 10. -->
								<th>Interest start Date</th><!-- 11. -->
								<th><b>Bank Branch Code</b></th><!-- 12. -->
								<th><b>Bank Acc Number</b></th><!-- 13. -->
								<th><b>Client</b></th><!-- 14. -->
								<th><b>Contarct No</b></th><!-- 15. -->
								<th><b>Branch Contarct Signed</b></th><!-- 16. -->
								<th><b>Amount Paid</b></th><!-- 17. -->
								<th><b>[Outstanding]</b></th><!-- 18. -->
								<th><b>Description of service</b></th><!-- 19. -->
								<th><b>Acc type</b></th><!-- 20. -->
								<th><b>[Loan Requested]</b></th><!-- 21. -->
								<th><b>[Repaid]</b></th><!-- 22. -->
								<th><b>Empty</b></th><!-- 23. -->
								<th><b>Contact 1</b></th><!-- 24. -->
								<th><b>Relation</b></th><!-- 25. -->
								<th><b>Telno</b></th><!-- 26. -->
								<th><b>Contact 2</b></th><!-- 27. -->
								<th><b>Relation</b></th><!-- 28. -->
								<th><b>Telno</b></th><!-- 29. -->
								<th><b>Contact 3</b></th><!-- 30. -->
								<th><b>Relation</b></th><!-- 31. -->
								<th><b>Telno</b></th><!-- 32. -->
								<th><b>Contact 4</b></th><!-- 33. -->
								<th><b>Relation</b></th><!-- 34. -->
								<th><b>Telno</b></th><!-- 35. -->
								<th><b>Contact 5</b></th><!-- 36. -->
								<th><b>Relation</b></th><!-- 37. -->
								<th><b>Telno</b></th><!-- 38. -->
								<th><b>Add Field 1</b></th><!-- 39. -->
								<th><b>Add Field 2</b></th><!-- 40. -->
								<th><b>Add Field 3	(Office use only)</b></th><!-- 41. -->
								<th><b>Client Structure (Office use only)</b></th><!-- 42. -->
								<th><b>(Office Use Only)</b></th><!-- 43. -->
								<th><b>Bank Name, eg ABSA / FNB</b></th><!-- 44. -->
								<th><b>Acc type, eg. Cheque / Savings</b></th><!-- 45. -->
								<th><b>Acc type code, Cheque =1 / Savings = 2</b></th><!-- 46. -->
		                </tr>
		              </thead>
		              <tbody>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////					  
					 //  include 'database.php';
					   //$pdo = Database::connect();
					   //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					   $username = $_SESSION['username'];
/////////////////////////////////////////////////////////////////////////////////////////////
					// -- Total Income
					$tot_income = 0.00;
					$tot_loan = 0.00;
					$tot_repayment = 0.00;
					// -- Total Income
					$tot_outstanding = 0.00;
					$tot_paid = 0.00;
					$tot_repaymentInvoice = 0.00;
					$repaymentAmount = null;
					$phone = '';
					$indexCount = 0;
			   	    $indexInvoiceSent = 0;
					$paymentid = 0;
/////////////////////////////////////////////////////////////////////////////////////////////					
					if ( !empty($_POST))
					   {
					     $username = $_POST['customerid'];
					   }
					   $sql = "";
					   $q = "";
/////////////////////////////////// ---- Filter Approval ---- ////////////////////////////// 
// -- 14.03.2021 -- added to sort out the time out issue..					   
$sqlSelect = "SELECT * FROM customer as C inner join loanapp as L on C.CustomerId = L.CustomerId";					   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : Customer 										   //
/////////////////////////////////////////////////////////////////////////////////////////////
				// -- Data Input Parameters...
				$dataInput['ExecApproval'] = $ExecApproval;
				$dataInput['customerid']   = $customerid;
				$dataInput['From'] 		   = '1970-01-01';//$From;
				$dataInput['To'] 		   = $To;
				if($_SESSION['role'] == 'customer')
				{
					// -- Customer Loan Applications...
					$data = GET_CUSTOMER_LOANS($dataInput,$pdo,$sqlSelect);   
				}
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////
				elseif(!empty($_SESSION['role']))// == 'admin')
				{
					// -- Customer Loan Applications...
					$data = GET_ADMIN_CUSTOMER_LOANS($dataInput,$pdo,$sqlSelect);
				}
/////////////////////////////////////////////////////////////////////////////////////////////					
// -- IMS database object
//$pdo2 = CONNECT_IMS_DB();
//$pdo2 = null;
// -- SQL Invoice
    $sqlInvoice = "SELECT * FROM common as c inner join payment as p on c.id = p.invoice_id";
// -- Loan Application & Customer : Customer & Loan Application $data
	foreach ($data as $row)
	{
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //
		 $customerid = $row['CustomerId'];
		 $ApplicationId = $row['ApplicationId'];
		 
		 $dataInput 			     = null;
		 $dataInput['customerid']    = $customerid;
		 $dataInput['ApplicationId'] = $ApplicationId;
		 $dataInput['amount'] 		 = $row['Repayment'];
		// -- Get Payment Invoices from common Tab Data.
		$sql =  $sqlInvoice." where c.customer_identification = ? AND p.notes = ?";
		$dataInvoice = GET_IMS_INVOICE_PAYMENTS($dataInput,$sql);
		$paymentid   = $dataInvoice['paymentid'];
		$iddocument  = $dataInvoice['iddocument'];
		$path		 = $dataInvoice['path'];
		$tot_paid	 = $dataInvoice['tot_paid'];
		$tot_outstanding = $dataInvoice['tot_outstanding'];
		
		$FirstName = $row['FirstName'];
		$LastName = $row['LastName'];
		$Title = $row['Title'];
		$phone = $row['phone'];
// ---------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				  //
// ---------------------------------------------------------------------------------- //
$typecode = null;
if($row['accounttype'] == 'Cheque'){$typecode=1;}else{$typecode=2;}
// -----------------------------  16.04.2017 ---------------------------------------- //
// -- Bank Name & Account Type Name 												  //					
// ---------------------------------------------------------------------------------- //
$conv_bankname    = '';
$conv_accounttype = '';
// -- Get Converted Bank & Account.
$BankName 	 = $row['bankname'];
$AccountType = $row['accounttype'];

$convData 		  = GET_CONVERTED_ACC_BANK($dataBanks,$dataAccountType,$BankName,$AccountType);
$conv_bankname	  = $convData['conv_bankname'];
$conv_accounttype = $convData['conv_accounttype'];
											
// BOC -- PaymentOutstanding Determination. 27.01.2018 -- // 
	$LoanRequested = $row['Repayment'];
	$financeReport = PaymentSchedule($ApplicationId,0,0,0,0,0,0, 0,0,0,$display,'eStatement');
	$financials = explode("|", $financeReport);

	$tot_outstanding = $financials[0];
	$RepaidAmount    = $financials[1];
// EOC -- PaymentOutstanding Determination. 27.01.2018 -- //
// -----------------------------  16.04.2017 ------------------
// -- Bank Name & Account Type Name 
// ------------------------------------------------------------								
$Filecontent1 = array( ''.$indexCount.'' => array('Acc'=> $row['CustomerId'], 'Title' =>$Title, 'Surname'=>$LastName, 'First Name'=>$FirstName,'Intials'=>substr($FirstName,0,1),
'Instalment Amount'=>$row['monthlypayment'],'Language'=>'English','Cause Date'=>$row['FirstPaymentDate'],
'Loan Duration'=>$row['LoanDuration'],'Interest Rate'=>number_format($row['InterestRate']),'Interest start Date'=>$row['FirstPaymentDate'],
'Bank Branch Code'=>$row['branchcode'],'Bank Acc Number'=>$row['accountnumber'],'Client'=>'Vogsphere (Pty) Ltd','Contarct No'=>$row['ApplicationId'],'Branch Contarct Signed'=>'by the client','Amount Paid'=>$tot_paid,'Settlement'=>
$tot_outstanding,'Description of service'=>'Personal Loan','Acc type'=>$conv_accounttype,'LoanRequested'=>$LoanRequested,'Repaid'=>$RepaidAmount,'Empty'=>'001',
'Contact 1'=>$phone,'Relation'=>'001','Telno'=>'001','Contact 2'=>'001','Relation'=>'001','Telno'=>'001',
'Contact 3'=>'001','Relation'=>'001','Telno'=>'001','Contact 4'=>'001','Relation'=>'001','Telno'=>'001',
'Contact 5'=>'001','Relation'=>'001','Telno'=>'001','Add Field 1'=>'001','Add Field 2'=>'001','Add Field 3 	(Office use only)'=>'001',
'Client Structure (Office use only)'=>'001','Bank Name, eg ABSA / FNB'=>'001','(Office Use Only)'=>'001','Bank Name, eg ABSA / FNB'=>$conv_bankname,'Acc type, eg. Cheque / Savings'=>$conv_accounttype,'Acc type code, Cheque =1 / Savings = 2'=>$typecode));
//=========================== Store Links to Email Invoice to APR Accounts ================================ //
$eMailInvoice = '';
if($row['ExecApproval'] == 'APPR')
{
$eMailInvoices[$indexInvoiceSent] = 
'<a class="btn btn-info" href="eInvoice.php?repayment='.$row['Repayment'].'&customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '&email=yes">eMail eStatement</a>';
$indexInvoiceSent = $indexInvoiceSent + 1;
//$eMailInvoices = array_merge($eMailInvoice,$eMailInvoices);
}
// ========================================== 2017.04.23 =================================================== // 
if($indexCount == 0)
{
$Filecontent  = $Filecontent1;
}
else
{
  $Filecontent = array_merge($Filecontent1,$Filecontent);
}
$indexCount = $indexCount + 1;

// -- Encode Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = urlencode(base64_encode($row['CustomerId']));
$ApplicationIdEncoded = urlencode(base64_encode($row['ApplicationId']));
$repaymentEncoded = urlencode(base64_encode($row['Repayment']));
// -- Encode Encrypt 16.09.2017 - Parameter Data.
// -- Get eStatement - table rows
$href = "'".'href=eInvoice.php?repayment='.$repaymentEncoded.'&customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '&email=yes'."'";
$dataTableRows = null;
$dataTableRows['repaymentEncoded']   	= $repaymentEncoded;
$dataTableRows['CustomerIdEncoded']  	= $CustomerIdEncoded;
$dataTableRows['ApplicationIdEncoded']  = $ApplicationIdEncoded;
$dataTableRows['tot_income']  		    = $tot_income;

$dataTableRows['Title']   			 = $Title;
$dataTableRows['FirstName']   		 = $FirstName;
$dataTableRows['LastName']   		 = $LastName;
$dataTableRows['href'] 				 = $href;
$dataTableRows['role'] 				 = getsession('role');
$dataTableRows['tot_paid']			 = $tot_paid;
$dataTableRows['tot_outstanding']	 = $tot_outstanding;
$dataTableRows['conv_accounttype']	 = $conv_accounttype;
$dataTableRows['LoanRequested']		 = $LoanRequested;
$dataTableRows['RepaidAmount']		 = $RepaidAmount;
$dataTableRows['phone']				 = $phone;
$dataTableRows['conv_bankname']		 = $conv_bankname;
$dataTableRows['conv_accounttype']	 = $conv_accounttype;
$dataTableRows['typecode']			 = $typecode;
$html_row =  GET_ESTATEMENT_TABLE_ROWS($row,$dataTableRows);
echo $html_row;
	}
					   $_SESSION["Filecontent"] = $Filecontent;
					   Database::disconnect();
					  ?>
				      </tbody>
	            </table>
<!-- ======================== Build a Div of eMail URL to sent to Clients ============================ -->
		<div id="divWebsites" hidden>
			<?php 
			$count = count($eMailInvoices); 
			for($i=0;$i<$count;$i++)
				{
				 echo $eMailInvoices[$i];
				}
			?>	
			</div>
<!-- ======================== 2017.04.23 - Build a Div of eMail URL to sent to Clients =============== -->
				
				</div>
    	</div>
    	</div>
		
   		<!---------------------------------------------- End Data ----------------------------------------------->
		
<!--=================================== Javascript for Multiple e-mail send 23.04.2017 ========================-->
<script>
function setupLinks() 
{ 
    var webSites = document.getElementById("divWebsites"); 

    if (webSites) 
    { 
        //assign window.open event to divWebsites links 
        var links = webSites.getElementsByTagName("a"); 
        var i, href, title; 
		
		repayment = 0;
		customerid = 0;
		ApplicationId = 0;
		Email = 0;
		
		indexofEqual = 0;
		MailArray = '';
	    indexCount = 0;
		var Message = '';
		
        for (i=0; i<links.length; i++) 
        { 	
            href = links[i].getAttribute("href"); 
            title = links[i].getAttribute("title"); 
			str  = links[i];
			//alert(href);
			Startpostion = href.indexOf("repayment");
			//alert(Startpostion);
			
			urlString = href.substr(Startpostion); 
			var res = urlString.split("&");
			//alert(res[0]+res[1]+res[2]+res[3]);
			
			repayment = res[0].substr(res[0].indexOf("=") + 1);
			customerid = res[1].substr(res[1].indexOf("=") + 1);
			ApplicationId = res[2].substr(res[2].indexOf("=") + 1);
			Email = res[3].substr(res[3].indexOf("=") + 1);
			
			jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"eInvoice.php",
					data:{repayment:repayment,customerid:customerid,ApplicationId:ApplicationId,email:Email},
					success: function(data)
					{
					  Message = Message+"\n<p>eStatements were Succesfull sent to customers";// :"+customerid+" and For ApplicationId :"+ApplicationId+"</p>";
					 // alert(Message);
					 jQuery("#divSuccess").append(Message);
					 			move();

					 
				    }
				});
			});
        } 
		
    }     
}  

// -- e-mail an eStatement at a time.
function setupLinks(href) 
{ 
//alert(href);
    if (href) 
    { 
        //assign window.open event to divWebsites links 
			var i, href, title; 
		
			repayment = 0;
			customerid = 0;
			ApplicationId = 0;
			email = 0;
			
			indexofEqual = 0;
			MailArray = '';
			indexCount = 0;
			var Message = '';
		
			str  = href;
			Startpostion = href.indexOf("repayment");
			
			urlString = href.substr(Startpostion); 
			var res = urlString.split("&");
			
			repayment = res[0].substr(res[0].indexOf("=") + 1);
			customerid = res[1].substr(res[1].indexOf("=") + 1);
			ApplicationId = res[2].substr(res[2].indexOf("=") + 1);
			email = res[3].substr(res[3].indexOf("=") + 1);
						
			jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"eInvoice.php",
					data:{repayment:repayment,customerid:customerid,ApplicationId:ApplicationId,email:email},
					success: function(data)
					{ 
					   //var jsonData;//JSON.parse(data);
					   /* try {
							jsonData = jQuery.parseJSON(data);
						  } catch (e) {
							// error
							//printError(e,true);
							//return;
						  } */
					  // -- $jsonback = data;
					  var Message = "\n<p>eStatement was succesfull sent to customer";// :"+customerid+" and For ApplicationId :"+ApplicationId+" mail "+email+"</p>";
					  // -- alert(Message);
					 jQuery("#divSuccess").html(Message);
					 			move();

					 
				    }
				});
			});
        
		
    }     
}  
// -- end e-mail.

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>
<!-- ===========================================================================================================-->		