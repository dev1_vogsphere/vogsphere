<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}

$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021
require  $filerootpath.'/ffms/ffms_classess.php';

$ApplicationId='';
$CustomerId='';

if ( !empty($_GET['customerid']))
{
    $customerid = $_REQUEST['customerid'];
    // -- BOC Encrypt 16.09.2017 - Parameter Data.
    $CustomerIdEncoded = $customerid;
    $customerid = base64_decode(urldecode($customerid));
    // -- EOC Encrypt 16.09.2017 - Parameter Data.
    //echo print_r ($customerid );

}

if ( !empty($_GET['policyId']))
{
    $policyId = $_REQUEST['policyId'];
    // -- BOC Encrypt 16.09.2017 - Parameter Data.
    $policyIdEncoded = $policyId;
    $policyId = base64_decode(urldecode($policyId));
    // -- EOC Encrypt 16.09.2017 - Parameter Data.
    //echo print_r ($policyId);
}
if ( !empty($_GET['addressId']))
{
    $addressId = $_REQUEST['addressId'];
    // -- BOC Encrypt 16.09.2017 - Parameter Data.
    $addressIdIdEncoded = $addressId;
    $addressId = base64_decode(urldecode($addressId));
    // -- EOC Encrypt 16.09.2017 - Parameter Data.
    
    //echo print_r ($addressId);
}

$Member = new memberDAL();
$MemberList= $Member->member($customerid );
//echo print_r($MemberList);
foreach($MemberList  as $key => $rowData )
{

    $Title=$rowData[1];
    $FirstName=$rowData[2].' '.$rowData[3];
    $LastName=$rowData[5];
    $DOB=$rowData[7];
    $ID_Number=$rowData[0];
}
$Address = new AddressDAL();
$AddressList=$Address->Address($addressId );
foreach($AddressList  as $key => $rowData )
{
    
  
    $City=$rowData[3];
    $Contact_Number=$rowData[7];
    $Emailaddress=$rowData[5];

    $Street=$rowData[0];
    $SuburbId=$rowData[0];
    $PostalCode=$rowData[0];

    if ( !empty($SuburbId))
    {
      
       // foreach($data  as $key => $rowData )
        //{
            //echo print_r($rowData);
        //}
    }

}

?>

<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div class="row">
<!-- Login Box -->
<form class="login-page" action="read?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

									<table class=   "table table-user-information">
						<!-- Customer Details -->
<!-- Title,FirstName,LastName -->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Main Member Details
                    </a>
                </h2>
</td></div>

</tr>

<tr>
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Title</strong></label>
		<div class="controls">
		  	<?php echo $Title;?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>First Names</strong></label>
		<div class="controls">
			<?php echo $FirstName;?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Last Name</strong></label>
		<div class="controls">
			<?php echo $LastName;?>
		</div>
	</div>
	</td>

</tr>

<!-- Street,Suburb,City -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Street</strong></label>
		<div class="controls">
		  
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label"><strong>City</strong></label>
		<div class="controls">
		    <?php echo $City;?>
		</div>
	</div>
	</td>
</tr>

<!-- State,PostCode,Dob -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">

		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label"><strong>Postal Code</strong></label>
		<div class="controls">
		
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Date of birth</strong></label>
		<div class="controls">
		    <?php echo $DOB;?>
		</div>
	</div>
	</td>
</tr>
<tr>
<td>
	<div class="control-group">
		<label class="control-label"><strong>Contact Number</strong></label>
		<div class="controls">
		    <?php echo $Contact_Number;?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Email</strong></label>
		<div class="controls">
		    <?phpecho $Emailaddress;?>
		</div>
	</div>
        </td>
        <td>
    <div class="control-group">
		<label class="control-label"><strong>ID Number</strong></label>
		<div class="controls">
        <?php echo $ID_Number;?>
		</div>
	</div>
	</td>
</tr>
<!----------------------------------------------------------- Banking Details --------------------------------------------
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>
</td></div>

</tr>
<!-- Account Holder Name,Bank Name,Account number 
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Holder Name</strong></label>
		<div class="controls">
			<?php echo $data['accountholdername'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Bank Name</strong></label>
		<div class="controls">
			<?php echo $data['bankname'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
		    <?php echo $data['accountnumber'];?>
		</div>
	</div>
	</td>
</tr>
 branch code,Account type 
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Branch Code</strong></label>
		<div class="controls">
			<?php echo $data['branchcode'].' - '.$dataBankBranches['branchdesc'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Type</strong></label>
		<div class="controls">
			<?php echo $data['accounttype'];?>
		</div>
	</div>
	</td>
</tr>

--------------------------------------------------------- End Banking Details ----------------------------------------

--------------------------------------------------------- Loan Details --------------------------------------------
 MonthlyIncome,MonthlyExpenditure,TotalAssets -
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Loan Details
                    </a>
                </h2>
</td></div>

</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Income</strong></label>
		<div class="controls">
			<?php echo $data['MonthlyIncome'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Expenditure</strong></label>
		<div class="controls">
			<?php echo $data['MonthlyExpenditure'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Total Assets</strong></label>
		<div class="controls">
		    <?php echo $data['TotalAssets'];?>
		</div>
	</div>
	</td>
</tr>

<!-- ReqLoadValue,DateAccpt,LoanDuration 
<tr>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Requested Loan Value</strong></label>
		<div class="controls">
		    <?php echo $data['ReqLoadValue'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Date Accepted</strong></label>
		<div class="controls">
		   <?php echo $data['DateAccpt'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>Loan Duration</strong></label>
		<div class="controls">
		  <?php echo $data['LoanDuration'];?>
		</div>
	</div>
	</td>
</tr>

InterestRate,ExecApproval 
<tr>
	<td>
	<div class="control-group <?php echo !empty($InterestRateError)?'error':'';?>">
		<label class="control-label"><strong>Interest Rate(%)</strong></label>
		<div class="controls">
				<?php echo $data['InterestRate'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group 'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<div class="controls">
	
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Total Repayment Amount</strong></label>
		<div class="controls">
		   
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Payment Method</strong></label>
				<div class="controls">
					
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Surety</strong></label>
				<div class="controls">
					
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Monthly Payment Amount</strong></label>
				<div class="controls">
					
				</div>
		</div>
	</td>

</tr>
<tr>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>First Payment Date</strong></label>
				<div class="controls">
					
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Last Payment Date</strong></label>
				<div class="controls">
					
				</div>
		</div>
	</td>
</tr>
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
$rejectreason = $data['rejectreason'];
if ($data['ExecApproval'] == 'REJ')
{
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div>
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>-->
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						  <a class="btn btn-primary" href="finonboardingmain">Back</a>
<!-- 13.06.2018 - Debit Order Schedule -->
		<span></span>
						<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
						 <?php
						 	if(isset($_SESSION))
							{
								$role = $_SESSION['role'];
							}

						 // -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);
$AccountHolder	 = $data['accountholdername'];
$BranchCode	 = $data['branchcode'];
$AccountNumber	 =  $data['accountnumber'];
$Servicetype	 = 'NADREQ';
$ServiceMode	 = 'NAEDO';
$UserCode      = 'UFEC';
$IntUserCode   = 'UFEC';
$Entry_Class   = 'UFEC';
$Contact_No		 = 'UFEC';
$Notify				 = 'UFEC';
$createdby     = 'UFEC';
$createdon     = 'UFEC';
$AccountType	 = '';

// Show if the role is administrator
	if($role == 'admin')
	{
$href = "'"."href=debitorderschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency&fname=$fname&lname=$lname"."'";
						 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="debitorderschedule('.$href.')">Generate Payments</button>';
	}
						 ?>
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>

								</div>
								</br> -->
											<div id="divdebitorderschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Debit Order Schedules</h1>
											</div>
							<!-- <div id="divSuccess">

								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>
						</div>
					  </td>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
<!-- 13.06.2018 - Debit Order Schedule -->


						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php
	
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
//	echo "<a class='btn btn-primary' href=upload?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
						</table>
									</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>

   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php

    // $pdo = Database::connectDB();
	 $tbl_name="loanapp"; // Table name
	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	 $check_user = "SELECT * FROM $tbl_name WHERE customerid= ? and ApplicationId=?";

	 // -- ID Document
	 $iddocument = '';

	 // -- Contract Agreement
	 $contract = '';

	 // -- Bank statement
	 $bankstatement = '';

	 // -- Proof of income
	 $proofofincome = '';

	 // -- Fica
	 $fica = '';

	 $consentform = '';
	 $other = '';
   	 $other2 = '';
	 // --

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = '';
	 $other3    = "";
	 $other4    = "";
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);


		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$row = $q->fetch(PDO::FETCH_ASSOC);
		$count = $q->rowCount();

	if($count >= 1)
    {

	//$row = mysql_fetch_array($result);


	$iddocument  =  $row['FILEIDDOC'];
	$contract = $row['FILECONTRACT'];
	$bankstatement = $row['FILEBANKSTATEMENT'];
	$proofofincome = $row['FILEPROOFEMP'];
	$fica = $row['FILEFICA'];

   // -- BOC 01.10.2017 - Upload Credit Reports.
   	$creditreport = $row['FILECREDITREP'];

	$consentform = $row['FILECONSENTFORM'];
	$other = $row['FILEOTHER'];
   	$other2 = $row['FILEOTHER2'];
	$NoFIle = "";
   // -- EOC 01.10.2017 - Upload Credit Reports.

   	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = $row['FILEDEBITFORM'];
	 $other3    = $row['FILEOTHER3'];
	 $other4    = $row['FILEOTHER4'];
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


   // -- BOC 01.10.2017 - Alternative if no file upload.
   if(empty($iddocument)){$iddocument = $NoFIle;}
   if(empty($contract)){ $contract = $NoFIle;}
   if(empty($bankstatement)){$bankstatement = $NoFIle;}
   if(empty($proofofincome)){$proofofincome = $NoFIle;}
   if(empty($fica)){ $fica = $NoFIle;}
   if(empty($creditreport)){  $creditreport = $NoFIle;}
   if(empty($consentform)){  $consentform = $NoFIle;}
   if(empty($other)){  $other = $NoFIle;}
   if(empty($other2)){  $other2 = $NoFIle;}

   if(empty($debitform)){$debitform = $NoFIle;}
   if(empty($other3)){  $other3 = $NoFIle;}
   if(empty($other4)){  $other4 = $NoFIle;}
   // -- EOC 01.10.2017 - Alternative if no file upload.

    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";

	$iddocumentHTML = '';
	if($iddocument == '')
	{
		$iddocumentHTML = "<tr>
         <td><a href='uploads/$iddocument' target='_blank'>$iddocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	else
	{
		$iddocumentHTML = "<tr>
         <td><a $classCSS href='uploads/$iddocument' target='_blank'>$ViewDocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	$contractHTML = '';
	if($contract == '')
	{
		$contractHTML = "<tr>


         <td><a href='uploads/$contract' target='_blank'>$contract</a></td>
         <td>Contract Agreement</td>
      </tr>
      ";
	}
	else
	{
		$contractHTML = "

      <tr>
         <td><a $classCSS href='uploads/$contract' target='_blank'>$ViewDocument</a></td>
         <td>Contract Agreement</td>
      </tr>";
	}

	$bankstatementHTML = '';
	if($bankstatement == '')
	{
		$bankstatementHTML = "<tr>
         <td><a href='uploads/$bankstatement' target='_blank'>$bankstatement</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	else
	{
		$bankstatementHTML = "<tr>
         <td><a $classCSS href='uploads/$bankstatement' target='_blank'>$ViewDocument</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}

	$proofofincomeHTML = '';
	if($proofofincome == '')
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a href='uploads/$proofofincome' target='_blank'>$proofofincome</a></td>
         <td>Proof of income</td>
      </tr>
	  ";
	}
	else
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$proofofincome' target='_blank'>$ViewDocument</a></td>

         <td>Proof of income</td>
      </tr>
	  ";
	}
	$ficaHTML = '';
	if($fica == '')
	{
		$ficaHTML = "

	  <tr>
         <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	else
	{
		$ficaHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$fica' target='_blank'>$ViewDocument</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	$creditreportHTML = '';
	if($creditreport == '')
	{
		$creditreportHTML = "
	  <tr>
         <td><a href='uploads/$creditreport' target='_blank'>$creditreport</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	else
	{
		$creditreportHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$creditreport' target='_blank'>$ViewDocument</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	$consentformHTML = '';
	if($consentform == '')
	{
		$consentformHTML = "
	  <tr>
         <td><a href='uploads/$consentform' target='_blank'>$consentform</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	else
	{
		$consentformHTML = "
	  <tr>
         <td><a  $classCSS href='uploads/$consentform' target='_blank'>$ViewDocument</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	$otherHTML = '';
	if($other == '')
	{
		$otherHTML ="
	  <tr>
         <td><a href='uploads/$other' target='_blank'>$other</a></td>
         <td>Other</td>
      </tr>
	";
	}
	else
	{
		$otherHTML ="
	  <tr>
         <td><a $classCSS href='uploads/$other' target='_blank'>$ViewDocument</a></td>
         <td>Other</td>
      </tr>
	";
	}
	$other2HTML = '';
	if($other2 == '')
	{
		$other2HTML = "
  	  <tr>
         <td><a href='uploads/$other2' target='_blank'>$other2</a></td>
         <td>Other 2</td>
      </tr>
";
	}
	else
	{
		$other2HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other2' target='_blank'>$ViewDocument</a></td>
         <td>Other 2</td>
      </tr>";
	}


	$debitformHTML = '';
	if($debitform == '')
	{
		$debitformHTML = "
  	  <tr>
         <td><a href='uploads/$debitform' target='_blank'>$debitform</a></td>
         <td>Debit Form</td>
      </tr>
";
	}
	else
	{
		$debitformHTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$debitform' target='_blank'>$ViewDocument</a></td>
         <td>Debit Form</td>
      </tr>";
	}

	$other3HTML = '';
	if($other3 == '')
	{
		$other3HTML = "
  	  <tr>
         <td><a href='uploads/$other3' target='_blank'>$other3</a></td>
         <td>Other 3</td>
      </tr>
";
	}
	else
	{
		$other3HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other3' target='_blank'>$ViewDocument</a></td>
         <td>Other 3</td>
      </tr>";
	}

	$other4HTML = '';
	if($other4 == '')
	{
		$other4HTML = "
  	  <tr>
         <td><a href='uploads/$other4' target='_blank'>$other4</a></td>
         <td>Other 4</td>
      </tr>
";
	}
	else
	{
		$other4HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other4' target='_blank'>$ViewDocument</a></td>
         <td>Other 4</td>
      </tr>";
	}


   echo "<tbody>"
      .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$consentformHTML.$creditreportHTML.$otherHTML.$other2HTML.$debitformHTML.$other3HTML.$other4HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->
</table>
 <!------------------------------ End File List ------------------------------------>

<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------>
					</form>


                            <!-- End Login Box -->
                        </div>

                </div>
            <!-- === END CONTENT === -->
