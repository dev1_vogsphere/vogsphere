<?php
// * To change this template, choose Tools | Templates
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
	require 'database.php';
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$dbname = "ecashpdq_eloan";

    $IntUserCodeB = getsession('UserCode');

	$customerid = null;
	$ApplicationId = null;
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$FILEIDDOC 	= '';
	$FILEOTHER2Affidavit 	= '';
	$FILEOTHER3CV 	= '';
	$FILEOTHER4CIPC 	= '';
	$FILEOTHER5TAXCC 	= '';
	$FILEOTHER6LGCF 	= '';
	$FILEOTHER7UIF 	= '';
	$FILEOTHER8PAYE 	= '';
	$FILEOTHER9VAT 	= '';
	$FILEOTHER10PBST 	= '';
	$FILEOTHER11BBCL 	= '';
	$FILEOTHER12BBS 	= '';
	$FILEOTHER13PCR 	= '';
	$FILEOTHER14BCR 	= '';
	$FILEOTHER15BBBEEE 	= '';
	$FILEOTHER16TAL 	= '';
	$FILEOTHER16VSOA 	= '';
	$FILEOTHER17CFB 	= '';
	$FILEOTHER18BP 	= '';
	$FILEOTHER19ABP 	= '';
	$FILEOTHER20ACP 	= '';
	$FILEOTHER21ARG 	= '';
	
	// -- EOC 01.10.2017 - Upload Credit Reports.

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	$FILEDEBITFORM  = '';

	
	$debitform = 'debitform';

	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

	$fieldname = '';
/* 	// -- Documents to be uploaded.
	$ID = 'ID';
	$Contract = 'Contract';
	$BankStatement = 'BankStatement';
	$Employment = 'Employment';
	$residence = 'residence';
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$creditreport = "creditreport";
	$creditconsentfrom = "creditconsentfrom";
	$debitorderconsentfrom="debitorderconsentfrom";
	$powerofattorney="powerofattorney";
	$proofofpayment="proofofpayment";
	$creditreportxds="creditreportxds";
	$creditreporttpn="creditreporttpn";
	$accountsdetailedreport="accountsdetailedreport"; */
	
	
	
	/* $consentform = "consentform";
	$other = "other";
	$other1= "other1";
	$other2 = "other2";
	$other3 = "other3";
	$other4 = "other4";
	$other5 = "other5";
	$other6 = "other6";
	$other7 = "other7"; */
	// -- EOC 01.10.2017 - Upload Credit Reports.
	
	$dataRole = null;
	$role = ""; 
	// -- Role : Customer
	$ID 	= 'ID';
	$FILEOTHER2 	= 'Affidavit';
	$FILEOTHER3 	= 'Curriculum';
	$FILEOTHER4 	= 'CIPC';
	$FILEOTHER5 	= 'Tax';
	$FILEOTHER6 	= 'Letter';
	$FILEOTHER7 	= 'Unemployment';
	$FILEOTHER8 	= 'Valid';
	$FILEOTHER9 	= 'VAT';
	$FILEOTHER10 	= 'Personal';
	$FILEOTHER11 	= 'Official';
	$FILEOTHER12 	= 'Business';
	$FILEOTHER13 	= 'Credit';
	$FILEOTHER14 	= 'BCredit';
	$FILEOTHER15 	= 'BBBEEE';
	$FILEOTHER16 	= 'Tribal';
	$FILEOTHER17 	= 'signed';
	$FILEOTHER18 	= 'Expressed';
	$FILEOTHER19 	= 'Plan';
	$FILEOTHER20 	= 'biomass1';
	$FILEOTHER21 	= 'charcoal';
	$FILEOTHER22 	= 'revenue';

	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	// -- I'm coming from
	$comingfrom = $_SERVER['HTTP_REFERER'];
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	$location = 'index';
	if ( null==$customerid ) 
	    {
		header("Location: ".$location);
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: ".$location);
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";

		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		// Read the role 
		// if role = customer 	and loan status	!= approved 
		// To allow the following documents to be uploaded.
		// 1. Identity Document (ID),2. Loan Contract Agreement {Not allowed},3. One month Bank Statement
		// 4. Proof of employment, 5. Proof of residence.
		$sql = "select * from user where userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
		$dataRole = $q->fetch(PDO::FETCH_ASSOC);	
		Database::disconnect();
	}

$valid_formats = array("jpg", "png", "gif", "zip", "bmp","pdf","PDF");
$max_file_size = 1024*10000; //10MB
$path = "uploads/UFAV/"; // Upload directory
$count = 0;
if ( !empty($_POST)) 
		{
			// Stote Values For Later
			if($_POST['documents'] == $ID)
			{
			 $FILEIDDOC = $ID;
			 $fieldname = 'OTHER1';
			 error_log('ID Selected');
			 error_log( $fieldname);
			}
			if($_POST['documents'] == $FILEOTHER2)
			{
			 $FILEOTHER2Affidavit = $FILEOTHER2;
			 $fieldname = 'OTHER2';
			}
			if($_POST['documents'] == $FILEOTHER3)
			{
			 $FILEOTHER3CV = $FILEOTHER3;
			 $fieldname = 'OTHER3';
			}
			if($_POST['documents'] == $FILEOTHER4)
			{
			 $FILEOTHER4CIPC = $FILEOTHER4;
			 $fieldname = 'OTHER4';
			}
			if($_POST['documents'] == $FILEOTHER5)
			{
			 $FILEOTHER5TAXCC = $FILEOTHER5;
			 $fieldname = 'OTHER5';
			}
			if($_POST['documents'] == $FILEOTHER6)
			{
			 $FILEOTHER6LGCF = $FILEOTHER6;
			 $fieldname = 'OTHER6';
			}
			if($_POST['documents'] == $FILEOTHER7)
			{
			 $FILEOTHER7UIF = $FILEOTHER7;
			 $fieldname = 'OTHER7';
			}
			if($_POST['documents'] == $FILEOTHER8)
			{
			 $FILEOTHER8PAYE = $FILEOTHER8;
			 $fieldname = 'OTHER8';
			}
			if($_POST['documents'] == $FILEOTHER9)
			{
			 $FILEOTHER9VAT = $FILEOTHER9;
			 $fieldname = 'OTHER9';
			}
			if($_POST['documents'] == $FILEOTHER10)
			{
			 $FILEOTHER10PBST = $FILEOTHER10;
			 $fieldname = 'OTHER10';
			}
			if($_POST['documents'] == $FILEOTHER11)
			{
			 $FILEOTHER11BBCL = $FILEOTHER11;
			 $fieldname = 'OTHER11';
			}
			if($_POST['documents'] == $FILEOTHER12)
			{
			 $FILEOTHER12BBS = $FILEOTHER12;
			 $fieldname = 'OTHER12';
			}
			if($_POST['documents'] == $FILEOTHER13)
			{
			 $FILEOTHER13PCR = $FILEOTHER13;
			 $fieldname = 'OTHER13';
			}
			if($_POST['documents'] == $FILEOTHER14)
			{
			 $FILEOTHER14BCR = $FILEOTHER14;
			 $fieldname = 'OTHER14';
			}
			if($_POST['documents'] == $FILEOTHER15)
			{
			 $FILEOTHER15BBBEEE = $FILEOTHER15;
			 $fieldname = 'OTHER15';
			}
			if($_POST['documents'] == $FILEOTHER16)
			{
			 $FILEOTHER16TAL = $FILEOTHER16;
			 $fieldname = 'OTHER16';
			}
			if($_POST['documents'] == $FILEOTHER17)
			{
			 $FILEOTHER16VSOA = $FILEOTHER17;
			 $fieldname = 'OTHER17';
			}
			if($_POST['documents'] == $FILEOTHER18)
			{
			 $FILEOTHER17CFB = $FILEOTHER18;
			 $fieldname = 'OTHER18';
			}
			if($_POST['documents'] == $FILEOTHER19)
			{
			 $FILEOTHER18BP = $FILEOTHER19;
			 $fieldname = 'OTHER19';
			}
			if($_POST['documents'] == $FILEOTHER20)
			{
			 $FILEOTHER19ABP = $FILEOTHER20;
			 $fieldname = 'OTHER20';
			}
			if($_POST['documents'] == $FILEOTHER21)
			{
			 $FILEOTHER20ACP = $FILEOTHER21;
			 $fieldname = 'OTHER21';
			}
			if($_POST['documents'] == $FILEOTHER22)
			{
			 $FILEOTHER21ARG = $FILEOTHER22;
			 $fieldname = 'OTHER22';
			}
			    
		// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

}

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{	
$comingfrom = getpost('comingfrom');	
// Loop $_FILES to execute all files
	foreach ($_FILES['files']['name'] as $f => $name) 
	{     
	    if ($_FILES['files']['error'][$f] == 4) {
	        continue; // Skip file if any error found
	    }	       
	    if ($_FILES['files']['error'][$f] == 0) {	           
	        if ($_FILES['files']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
						// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;
			
			if($_POST['documents'] == $ID)
			{
			 $FILEIDDOC = $name;
			  error_log('Get document name');
			}
			
			if($_POST['documents'] == $FILEOTHER2)
			{
			 $FILEOTHER2Affidavit = $name;
			}
			
			if($_POST['documents'] == $FILEOTHER3)
			{
			 $FILEOTHER3CV = $name;
			}
			
			if($_POST['documents'] == $FILEOTHER4)
			{
			 $FILEOTHER4CIPC = $name;
			}
			if($_POST['documents'] == $FILEOTHER5)
			{
			 $FILEOTHER5TAXCC = $name;
			}
			if($_POST['documents'] == $FILEOTHER6)
			{
			 $FILEOTHER6LGCF = $name;
			}
			if($_POST['documents'] == $FILEOTHER7)
			{
			 $FILEOTHER7UIF = $name;
			}
			if($_POST['documents'] == $FILEOTHER8)
			{
			 $FILEOTHER8PAYE = $name;
			}
			if($_POST['documents'] == $FILEOTHER9)
			{
			 $FILEOTHER9VAT = $name;
			}
			if($_POST['documents'] == $FILEOTHER10)
			{
			 $FILEOTHER10PBST = $name;
			}
			if($_POST['documents'] == $FILEOTHER11)
			{
			 $FILEOTHER11BBCL = $name;
			}
			if($_POST['documents'] == $FILEOTHER12)
			{
			 $FILEOTHER12BBS = $name;
			}
			if($_POST['documents'] == $FILEOTHER13)
			{
			 $FILEOTHER13PCR = $name;
			}
			if($_POST['documents'] == $FILEOTHER14)
			{
			 $FILEOTHER14BCR = $name;
			}
			if($_POST['documents'] == $FILEOTHER15)
			{
			 $FILEOTHER15BBBEEE = $name;
			}
			if($_POST['documents'] == $FILEOTHER16)
			{
			 $FILEOTHER16TAL = $name;
			}
			if($_POST['documents'] == $FILEOTHER17)
			{
			 $FILEOTHER16VSOA = $name;
			}
			if($_POST['documents'] == $FILEOTHER18)
			{
			 $FILEOTHER17CFB = $name;
			}
			if($_POST['documents'] == $FILEOTHER19)
			{
			 $FILEOTHER18BP = $name;
			}
			if($_POST['documents'] == $FILEOTHER20)
			{
			 $FILEOTHER19ABP = $name;
			}
			if($_POST['documents'] == $FILEOTHER21)
			{
			 $FILEOTHER20ACP = $name;
			}
			if($_POST['documents'] == $FILEOTHER22)
			{
			 $FILEOTHER21ARG = $name;
			}
					
			// -- EOC 24.03.2019 - Upload Debit Order Form, other4.
			error_log($fieldname);
			$sql = "UPDATE $tbl_loanapp";
			$sql = $sql." SET  $fieldname = ?";
			$sql = $sql." WHERE Customerid = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			//error_log($q);
			error_log($path.$name);
			error_log($customerid);
			error_log($ApplicationId);
			$q->execute(array($path.$name,$customerid,$ApplicationId));
			
			
		    // -- Update File Name Directory	
	            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }
				
				// -- BOC Encrypt the uploaded PDF with ID as password.
					$file_parts = pathinfo($name);
					$filenameEncryp = $path.$name;
					switch($file_parts['extension'])
					{
						case "pdf":
						{
							

						break;
						}
						case "PDF":
						{
							/* -- TCPDF library (search for installation path).
							require_once('tcpdf_include.php');
							require_once "FPDI/fpdi.php";
							$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF
							// -- Page Config
								
							// -- Page Config	
							$pdf->setSourceFile($filenameEncryp);
							$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);*/
							
						break;
						}
					}
				// -- EOC Encrypt the uploaded PDF file with ID as password.
	        }
	    }
	}
}
?>

<!--<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Multiple File Upload with PHP - Demo</title> -->
<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Required Documents Checklist</h1>
		<?php
		# error messages
		if (isset($message)) {
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		
		// Refresh Database Data
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		
			printf("<p class='status'>%d files added successfully!</p>\n", $count);
		}
		?>
				<!-- Multiple file upload html form-->
		<form action="" method="post" enctype="multipart/form-data">
		<p>Max file size 10Mb, Valid formats jpg, png, gif, pdf</p>
		<br />
		<!-- <input readonly type="checkbox" name="vehicle" value="ID" class="tick animate fadeInRight"> Identity Documents(ID)<br>
		<input readonly type="checkbox" name="vehicle" value="FICA" checked> CREDIT REPORT TPN<br>
		<input readonly type="checkbox" name="vehicle" value="BANK" checked>1 month Bank Statement<br> 
		-->		
		<ul class="plus animate fadeInRight">
            <li align="left"><b>Indicates Missing document</b></li>
		</ul>
		
		<ul class="tick animate fadeInRight">
            <li align="left"><b>Indicates Uploaded document</b></li>
		</ul>
				
	<?php 
	$FILEIDDOCCHECKED = '';
	$FILEOTHER2CHECKED='';
	$FILEOTHER3CHECKED='';
	$FILEOTHER4CHECKED='';
	$FILEOTHER5CHECKED='';
	$FILEOTHER6CHECKED='';
	$FILEOTHER7CHECKED='';
	$FILEOTHER8CHECKED='';
	$FILEOTHER9CHECKED='';
	$FILEOTHER10CHECKED='';
	$FILEOTHER11CHECKED='';
	$FILEOTHER12CHECKED='';
	$FILEOTHER13CHECKED='';
	$FILEOTHER14CHECKED='';
	$FILEOTHER15CHECKED='';
	$FILEOTHER16CHECKED='';
	$FILEOTHER17CHECKED='';
	$FILEOTHER18CHECKED='';
	$FILEOTHER19CHECKED='';
	$FILEOTHER20CHECKED='';
	$FILEOTHER21CHECKED='';
	$FILEOTHER22CHECKED='';
	
	
	/* $FILECONTRACTCHECKED = '';
	$FILEBANKSTATEMENTCHECKED = '';
	$FILEPROOFEMPCHECKED = '';
	$FILEFICACHECKED = '';
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$FILECREDITREPCHECKED = '';
	$FILECONSENTFORMCHECKED = '';
	$FILECREDITCONSENTFORMCHECKED = '';
	$DEBITORDERCONSENTFORMCHECKED = '';
	$POWEROFATTORNEYCHECKED = '';
	$PROOFOFPAYMENTCHECKED = '';
	$CREDITREPORTXDSCHECKED = '';
	$CREDITREPORTTPNCHECKED = '';
	$ACCOUNTSDETAILEDREPORTCHECKED = '';
	$FILEOTHERCHECKED = '';
	$FILEOTHER1CHECKED = '';
	$FILEOTHER2CHECKED = '';
	$FILEOTHER3CHECKED = '';
	$FILEOTHER4CHECKED = '';
	$FILEOTHER5CHECKED = '';
	$FILEOTHER6CHECKED = '';
	$FILEOTHER7CHECKED = ''; */
	// -- EOC 01.10.2017 - Upload Credit Reports.
	// -- BOC 01.10.2017 - Upload Debit Form.
	// -- EOC 01.10.2017 - Upload Debit Form.

	if($FILEIDDOC 			!= ''){$FILEIDDOCCHECKED='checked';}
	if($FILEOTHER2Affidavit 			!= ''){$FILEOTHER2CHECKED='checked';}
	if($FILEOTHER3CV 			!= ''){$FILEOTHER3CHECKED='checked';}
	if($FILEOTHER4CIPC 			!= ''){$FILEOTHER4CHECKED='checked';}
	if($FILEOTHER5TAXCC 			!= ''){$FILEOTHER5CHECKED='checked';}
	if($FILEOTHER6LGCF 			!= ''){$FILEOTHER6CHECKED='checked';}
	if($FILEOTHER7UIF 			!= ''){$FILEOTHER7CHECKED='checked';}
	if($FILEOTHER8PAYE 			!= ''){$FILEOTHER8CHECKED='checked';}
	if($FILEOTHER9VAT 			!= ''){$FILEOTHER9CHECKED='checked';}
	if($FILEOTHER10PBST 		!= ''){$FILEOTHER10CHECKED='checked';}
	if($FILEOTHER11BBCL 			!= ''){$FILEOTHER11CHECKED='checked';}
	if($FILEOTHER12BBS 			!= ''){$FILEOTHER12CHECKED='checked';}
	if($FILEOTHER13PCR 			!= ''){$FILEOTHER13CHECKED='checked';}
	if($FILEOTHER14BCR 			!= ''){$FILEOTHER14CHECKED='checked';}
	if($FILEOTHER15BBBEEE 			!= ''){$FILEOTHER15CHECKED='checked';}
	if($FILEOTHER16TAL 			!= ''){$FILEOTHER16CHECKED='checked';}
	if($FILEOTHER16VSOA 			!= ''){$FILEOTHER17CHECKED='checked';}
	if($FILEOTHER17CFB 			!= ''){$FILEOTHER18CHECKED='checked';}
	if($FILEOTHER18BP 			!= ''){$FILEOTHER19CHECKED='checked';}
	if($FILEOTHER19ABP 			!= ''){$FILEOTHER20CHECKED='checked';}
	if($FILEOTHER20ACP 			!= ''){$FILEOTHER21CHECKED='checked';}
	if($FILEOTHER21ARG 			!= ''){$FILEOTHER22CHECKED='checked';}
	
	/* 
	if($FILECONTRACT 		!= ''){$FILECONTRACTCHECKED = 'checked';}
	if($FILEBANKSTATEMENT 	!= ''){$FILEBANKSTATEMENTCHECKED='checked';}
	if($FILEPROOFEMP 		!= ''){$FILEPROOFEMPCHECKED='checked';}
	if($FILEFICA 			!= ''){$FILEFICACHECKED='checked';}
	// -- BOC 01.10.2017 - Upload Credit Reports.
	if($FILECREDITREP 			!= ''){$FILECREDITREPCHECKED='checked';}
	if($FILECONSENTFORM 	!= ''){$FILECONSENTFORMCHECKED='checked';}
	if($FILECREDITCONSENTFORM 	!= ''){$FILECREDITCONSENTFORMCHECKED='checked';}
	if($DEBITORDERCONSENTFORM 	!= ''){$DEBITORDERCONSENTFORMCHECKED='checked';}
	if($POWEROFATTORNEY 	!= ''){$POWEROFATTORNEYCHECKED='checked';}
	if($PROOFOFPAYMENT 	!= ''){$PROOFOFPAYMENTCHECKED='checked';}
	if($CREDITREPORTXDS 	!= ''){$CREDITREPORTXDSCHECKED='checked';}
	if($CREDITREPORTTPN 	!= ''){$CREDITREPORTTPNCHECKED='checked';}
	if($ACCOUNTSDETAILEDREPORT 	!= ''){$ACCOUNTSDETAILEDREPORTCHECKED='checked';}
	if($FILEOTHER 			!= ''){$FILEOTHERCHECKED='checked';}
	if($FILEOTHER1 			!= ''){$FILEOTHER1CHECKED='checked';}
	if($FILEOTHER2 			!= ''){$FILEOTHER2CHECKED='checked';}
	if($FILEDEBITFORM   	!= ''){$FILEDEBITFORMCHECKED='checked';}
	if($FILEOTHER3 			!= ''){$FILEOTHER3CHECKED='checked';}
	if($FILEOTHER4 			!= ''){$FILEOTHER4CHECKED='checked';}
	if($FILEOTHER5 			!= ''){$FILEOTHER5CHECKED='checked';}
	if($FILEOTHER6 			!= ''){$FILEOTHER6CHECKED='checked';}
	if($FILEOTHER7 			!= ''){$FILEOTHER7CHECKED='checked';} */
	// -- BOC 01.10.2017 - Upload Credit Reports.
	
	if($FILEIDDOC == '' and 
	$FILEOTHER2Affidavit == '' and
	$FILEOTHER3CV == '' and
	$FILEOTHER4CIPC == '' and
	$FILEOTHER5TAXCC == '' and 
	$FILEOTHER6LGCF == '' and
	$FILEOTHER7UIF == '' and 
	$FILEOTHER8PAYE == '' and
	$FILEOTHER9VAT == '' and
	$FILEOTHER10PBST == '' and
	$FILEOTHER11BBCL == '' and
	$FILEOTHER12BBS == '' and
	$FILEOTHER13PCR == '' and
	$FILEOTHER14BCR == '' and
	$FILEOTHER15BBBEEE == '' and 
	$FILEOTHER16TAL == '' and 
	$FILEOTHER16VSOA == '' and 
	$FILEOTHER17CFB == '' and
	$FILEOTHER18BP == '' and
	$FILEOTHER19ABP == '' and
	$FILEOTHER20ACP == '' and
	$FILEOTHER21ARG == '' 
	){$FILEIDDOCCHECKED='checked';}
	
	// Role and Loan Status
	
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
	if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
	{ 
		if( empty($data['FILEIDDOC'])) // ID document
            {
			$FILEIDDOCCHECKED = '';
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED disabled>Certified ID copies</li></ul>";
			}
			else
			{
			$FILEIDDOCCHECKED = '';
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED disabled>Certified ID copies</li></ul>";
			}
			
			
	}
	elseif( empty($data['FILEIDDOC'])) // ID document
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED>Certified ID copies</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED>Certified ID copies</li></ul>";
			}
	//EOC (11.10.2016)
	
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['FILEOTHER2'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Affidavit' $FILEOTHER2CHECKED disabled>Affidavit from directors/ members that they are aware of the contents of the application form/li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Affidavit' $FILEOTHER2CHECKED disabled>Affidavit from directors/ members that they are aware of the contents of the application form</li></ul>";
			}
		  }
		  elseif( empty($data['FILEOTHER2'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Affidavit' $FILEOTHER2CHECKED >Affidavit from directors/ members that they are aware of the contents of the application form</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Affidavit' $FILEOTHER2CHECKED >Affidavit from directors/ members that they are aware of the contents of the application form</li></ul>";
			}
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['FILEOTHER3'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Curriculum' $FILEOTHER3CHECKED disabled>Curriculum Vitae(s) of all the Directors</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Curriculum' $FILEOTHER3CHECKED disabled>Curriculum Vitae(s) of all the Directors</li></ul>";
			}
		  }
		  elseif( empty($data['FILEOTHER3'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Curriculum' $FILEOTHER3CHECKED >Curriculum Vitae(s) of all the Directors</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='Curriculum' $FILEOTHER3CHECKED >Curriculum Vitae(s) of all the Directors</li></ul>";
			}
	//EOC (12.10.2016)
		// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['FILEOTHER4'])) // Contract Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='CIPC' $FILEOTHER4CHECKED disabled>CIPC Registration Certificate</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='CIPC' $FILEOTHER4CHECKED disabled>CIPC Registration Certificate</li></ul>";
			}
		  }
		  elseif( empty($data['FILEOTHER4'])) // Contract Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='CIPC' $FILEOTHER4CHECKED >CIPC Registration Certificate</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='CIPC' $FILEOTHER4CHECKED >CIPC Registration Certificate</li></ul>";
			}
	//EOC (12.10.2016)
			
		// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{		
				if( empty($data['FILEOTHER5'])) //  Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tax' $FILEOTHER5CHECKED disabled>Tax Clearance Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tax' $FILEOTHER5CHECKED disabled>Tax Clearance Certificate</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER5'])) //  Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tax' $FILEOTHER5CHECKED>Tax Clearance Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tax' $FILEOTHER5CHECKED>Tax Clearance Certificate</li></ul>";
				}
//EOC (12.10.2016)
// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER6'])) // FILEPROOFEMP Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Letter' $FILEOTHER6CHECKED disabled>Letter of Good Standing with Compensation Fund</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Letter' $FILEOTHER6CHECKED disabled>Letter of Good Standing with Compensation Fund</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER6'])) // FILEPROOFEMP Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Letter' $FILEOTHER6CHECKED>Letter of Good Standing with Compensation Fund</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Letter' $FILEOTHER6CHECKED>Letter of Good Standing with Compensation Fund</li></ul>";
				}
			//EOC (12.10.2016)	

// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{			
				if( empty($data['FILEOTHER7'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Unemployment' $FILEOTHER7CHECKED disabled>Unemployment Insurance Fund Compliance Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Unemployment' $FILEOTHER7CHECKED disabled>Unemployment Insurance Fund Compliance Certificate</li></ul>";
				}
			}	
			elseif( empty($data['FILEOTHER7'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Unemployment' $FILEOTHER7CHECKED>Unemployment Insurance Fund Compliance Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Unemployment' $FILEOTHER7CHECKED>Unemployment Insurance Fund Compliance Certificate</li></ul>";
				}
				
//EOC (12.10.2016)	
// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{			
				if( empty($data['FILEOTHER8'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Valid' $FILEOTHER8CHECKED disabled>Valid PAYE Registration Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Valid' $FILEOTHER8CHECKED disabled>Valid PAYE Registration Certificate</li></ul>";
				}
			}	
			elseif( empty($data['FILEOTHER8'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Valid' $FILEOTHER8CHECKED>Valid PAYE Registration Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Valid' $FILEOTHER8CHECKED>Valid PAYE Registration Certificate</li></ul>";
				}
		// -- Consent Form
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER9'])) // FILECONSENTFORM Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='VAT' $FILEOTHER9CHECKED disabled>Valid VAT Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='VAT' $FILEOTHER9CHECKED disabled>Valid VAT Certificate</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER9'])) // FILECONSENTFORM Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='VAT' $FILEOTHER9CHECKED>Valid VAT Certificate</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='VAT' $FILEOTHER9CHECKED>Valid VAT Certificate</li></ul>";
			}
		// -- Other Document
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER10'])) // FILEOTHER Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Personal' $FILEOTHER10CHECKED disabled>Personal Bank Statements for the past 12 months</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Personal' $FILEOTHER10CHECKED disabled>Personal Bank Statements for the past 12 months</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER10'])) // FILEOTHER Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Personal' $FILEOTHER10CHECKED>Personal Bank Statements for the past 12 months</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Personal' $FILEOTHER10CHECKED>Personal Bank Statements for the past 12 months</li></ul>";
			}
		
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER11'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Official' $FILEOTHER11CHECKED disabled>Official Business Bank Confirmation Letter</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Official' $FILEOTHER11CHECKED disabled>Official Business Bank Confirmation Letter</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER11'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Official' $FILEOTHER11CHECKED>Official Business Bank Confirmation Letter</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Official' $FILEOTHER11CHECKED>Official Business Bank Confirmation Letter</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER12'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Business' $FILEOTHER12CHECKED disabled>Business Bank Statements for the past 12 months</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Business' $FILEOTHER12CHECKED disabled>Business Bank Statements for the past 12 months</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER12'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Business' $FILEOTHER12CHECKED>Business Bank Statements for the past 12 months</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Business' $FILEOTHER12CHECKED>Business Bank Statements for the past 12 months</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Debit Form
// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER13'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Credit' $FILEOTHER13CHECKED disabled>Attached Personal Credit Report</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Credit' $FILEOTHER13CHECKED disabled>Attached Personal Credit Report</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER13'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Credit' $FILEOTHER13CHECKED>Attached Personal Credit Report</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Credit' $FILEOTHER13CHECKED>Attached Personal Credit Report</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER14'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BCredit' $FILEOTHER14CHECKED disabled>Attached Business Credit Report</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BCredit' $FILEOTHER14CHECKED disabled>Attached Business Credit Report</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER14'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BCredit' $FILEOTHER14CHECKED>Attached Business Credit Report</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BCredit' $FILEOTHER14CHECKED>Attached Business Credit Report</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER15'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BBBEEE' $FILEOTHER15CHECKED disabled>BBBEEE Affidavit or Certificate</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BBBEEE' $FILEOTHER15CHECKED disabled>BBBEEE Affidavit or Certificate</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER15'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BBBEEE' $FILEOTHER15CHECKED>BBBEEE Affidavit or Certificate</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='BBBEEE' $FILEOTHER15CHECKED>BBBEEE Affidavit or Certificate</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER16'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tribal' $FILEOTHER16CHECKED disabled>Tribal Authority letter - Permission to use land</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tribal' $FILEOTHER16CHECKED disabled>Tribal Authority letter - Permission to use land</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER16'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tribal' $FILEOTHER16CHECKED>Tribal Authority letter - Permission to use land</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Tribal' $FILEOTHER16CHECKED>Tribal Authority letter - Permission to use land</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER17'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='signed' $FILEOTHER17CHECKED disabled>A valid signed offtake agreement</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='signed' $FILEOTHER17CHECKED disabled>A valid signed offtake agreement</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER17'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='signed' $FILEOTHER17CHECKED>A valid signed offtake agreement</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='signed' $FILEOTHER17CHECKED>A valid signed offtake agreement</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER18'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Expressed' $FILEOTHER18CHECKED disabled>Expressed interest in establishing a charcoal focused business</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Expressed' $FILEOTHER18CHECKED disabled>	Expressed interest in establishing a charcoal focused business</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER18'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Expressed' $FILEOTHER18CHECKED>Expressed interest in establishing a charcoal focused business</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Expressed' $FILEOTHER18CHECKED>Expressed interest in establishing a charcoal focused business</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.
// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER19'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Plan' $FILEOTHER19CHECKED disabled>Approved Business Plan</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Plan' $FILEOTHER19CHECKED disabled>Approved Business Plan</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER19'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Plan' $FILEOTHER19CHECKED>Approved Business Plan</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Plan' $FILEOTHER19CHECKED>Approved Business Plan</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.		
// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER20'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='biomass1' $FILEOTHER20CHECKED disabled>Amount of biomass produced within the last twelve (12) months</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='biomass1' $FILEOTHER20CHECKED disabled>Amount of biomass produced within the last twelve (12) months</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER20'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='biomass1' $FILEOTHER20CHECKED>Amount of biomass produced within the last twelve (12) months</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='biomass1' $FILEOTHER20CHECKED>Amount of biomass produced within the last twelve (12) months</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.		
// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER21'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='charcoal' $FILEOTHER21CHECKED disabled>Amount of charcoal produced within the last twelve (12) months</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='charcoal' $FILEOTHER21CHECKED disabled>Amount of charcoal produced within the last twelve (12) months</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER21'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='charcoal' $FILEOTHER21CHECKED>Amount of charcoal produced within the last twelve (12) months</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='charcoal' $FILEOTHER21CHECKED>Amount of charcoal produced within the last twelve (12) months</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.
// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "avoSMME" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER22'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='revenue' $FILEOTHER22CHECKED disabled>Amount of revenue generated within the last twelve (12) months</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='revenue' $FILEOTHER22CHECKED disabled>Amount of revenue generated within the last twelve (12) months</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER22'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='revenue' $FILEOTHER22CHECKED>Amount of revenue generated within the last twelve (12) months</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='revenue' $FILEOTHER22CHECKED>Amount of revenue generated within the last twelve (12) months</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.			
		
	?>			
		<br />
		<br />
					<input  type="hidden" name="comingfrom" id="comingfrom" value=<?php echo $comingfrom; ?>/><br> 

			<input type="file" name="files[]" multiple="multiple" accept="*">
			<table class ="table table-user-information">
				  <tr>
					<td>
						<div>
							<!-- <input type="submit" value="Upload"> -->
							<input type="submit" class="btn btn-primary" value="Upload">
						</div>
					</td>
				<td>
				<div>
				<?php if(empty($comingfrom)){$comingfrom = 'readsme.php';} echo '<a class="btn btn-primary" href="'.$comingfrom.'?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded. '">Back</a>'; ?></div></td>
				</tr>
			</table>
		

		</form>
</div>
</div>

<!-- </body>
</html> -->