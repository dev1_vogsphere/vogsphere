<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_1="process_definition"; // Table name 
$tbl_2="process_event"; // Table name 

//1. ---------------------  Role ------------------- //
  $sql = 'SELECT * FROM '.$tbl_1.' ORDER BY PROC_ID';
  $dataPROC_ID = $pdo->query($sql);						   						 

$PROC_ID		=	'';
$EV_NAME		=	'';

// -- Errors
$PROC_IDError		=	'';
$EV_NAMEError		=	'';

$count = 0;
$valid = true;
if (!empty($_POST)) 
{   $count = 0;
	$labelError = '';
	$PROC_ID	=	$_POST['PROC_ID'];
	$EV_NAME	=	$_POST['EV_NAME'];
	$valid = true;
	$CREATEDBY = ''; 
	$CREATEDON = date('Y-m-d');
	$CREATEDAT = date('h:m:s');

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								EV_NAME  VALIDATION														  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if (empty($EV_NAME)) { $EV_NAMEError = 'Please enter EV_NAME'; $valid = false;}
	
	if ($valid) 
	{	

			$sql = "select * from $tbl_2 where EV_NAME = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($EV_NAME));
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(!empty($data))
			{
				$EV_NAMEError = 'Business Event aleady exist!.'; $valid = false;
			}
			
			if ($valid) 
			{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO process_event(EV_NAME, EV_PROCNAME,CREATEDBY, CREATEDON,CREATEDAT) VALUES (?,?,?,?,?)";
			
			$q = $pdo->prepare($sql);
			$q->execute(array($EV_NAME,$PROC_ID,$CREATEDBY,$CREATEDON,$CREATEDAT));
			Database::disconnect();
			$count = $count + 1;
			}
	}
}

?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Business Process Event
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Business Process Event successfully added.</p>");
		}
?>
				
</div>				
			<p><b>Business Name</b><br />
				<div class="controls">
					<SELECT class="form-control" id="PROC_ID" name="PROC_ID" size="1">
					<?php
						$PROC_IDLoc = '';
						$PROC_IDSelect = $PROC_ID;
						foreach($dataPROC_ID as $row)
						{
							$PROC_IDLoc = $row['PROC_NAME'];

							if($PROC_IDLoc == $PROC_IDSelect)
							{
								echo "<OPTION value=$PROC_IDLoc selected>$PROC_IDLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$PROC_IDLoc>$PROC_IDLoc</OPTION>";
							}
						}
												
						if(empty($dataPROC_ID))
						{
							echo "<OPTION value=0>No Business Processes</OPTION>";
						}
					?>
					</SELECT>
				</div>
			</p>			
			
			<p><b>Event Name</b><br />
			<input style="height:30px" type='text' name='EV_NAME' value="<?php echo !empty($EV_NAME )?$EV_NAME :'';?>"/>
			<?php if (!empty($EV_NAMEError)): ?>
			<span class="help-inline"><?php echo $EV_NAMEError;?></span>
			<?php endif; ?>
			</p>	
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'businessprocessworkevent';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
<script>
function parentmenu() 
{
	var ele = document.getElementById("parentmenu");
	var text = document.getElementById("parentmenu");
	if(ele.style.display == "block")
	{
    		ele.style.display = "none";
  	}
	else 
	{
		ele.style.display = "block";
	}
} 
</script>