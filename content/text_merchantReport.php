<?php
require 'database.php';
// -- redirect if user has been logged off.
$customerLogin = 'customerLogin';

// -- its okay, navigate to login back.
if(empty(getsession('role')))
{
	redirect($customerLogin);
}
$reference 						='';
if (isset($_GET['reference']) ) 
{ 
	$reference = $_GET['reference']; 
}

$changedby = getsession('username');

// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
		
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
		
	}
	// -- debicheckusercode
	if(getsession('debicheckusercode'))
	{
		$debicheckusercode = getsession('debicheckusercode');
		
	}
		// -- nominated_account
	if(getsession('nominated_account'))
	{
		$nominated_account = getsession('nominated_account');
		
	}
$FromDate= date('Y-m-01');
$ToDate=date('Y-m-t');
$sessionrole='';
$customerid='';

$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");

$sql = "SELECT * FROM merchant_services where Internal_user_code= '".$IntUserCode."'";
$datamerchant = mysqli_query($connect,$sql);


?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>


<div class="container background-white bottom-border">

  <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
  <h2 align="center">Merchant Summary</h2>
    </div>
      <div class='row'>
	   <!-- <div class='input-group margin-bottom-20'>
            <span class='input-group-addon'>
                <i class='fa fa-user'></i>
            </span>
            <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>-->

        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="<?php echo $FromDate;?>">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo $ToDate;?>">
        </div>
		
    <!--<div class='row'>
        <div class='col-md-7'>
            <button class='btn btn-primary pull-right' type='submit' onclick="mySearch">Search</button>
        </div>
    </div>-->

        </div>
      </form>
    </div>
  </div>
</br>
</br>
<div class="table-responsive">

<table id="mandates" class="table table-striped table-bordered table-hover" style="white-space: nowrap">     
	 <thead>
          <tr>
              <!--<th>Terminal ID</th>-->
              <th>Transaction Date</th>
              <th>Purchase Amount</th>
              <th>Refund Amount</th>
			  <th>Cash Back</th>
              <!--<th>Transactions</th>-->
              <!--<th>Discount</th>-->
              <th>Merchant Fee</th>
              <th>Merchant Vat Fee</th>
			  <th>Merchant Receivable</th> <!--Payout-->

          </tr>
      </thead>
	   <tbody>	
	   <?php
		if(!empty($datamerchant))
		{
				while($row = mysqli_fetch_array($datamerchant))
				{
					echo '<tr>';
					//	echo '<td>'.$row ["terminal_id"].'</td>';
						echo '<td>'.$row ["transaction_date"].'</td>';
						echo '<td>'."R ".$row ["purchase_amount"].'</td>';
						echo '<td>'."R ".$row ["refund_amount"].'</td>';
						echo '<td>'."R ".$row ["cash_back"].'</td>';
					//	echo '<td>'."R ".$row ["number_of_transactions"].'</td>';
						//echo '<td>'."R ".$row ["discount"].'</td>';
						echo '<td>'."R ".$row ["merchant_fee"].'</td>';
						echo '<td>'."R ".$row ["merchant_vat_fee"].'</td>';
						echo '<td>'."R ".$row ["merchant_receviable"].'</td>';
					echo '</tr>';
				}
		}	
		?>
	   
	   
	   </tbody>	
	  
	  
</table>



</div>
</br>
<script>

$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10);
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[0]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#mandates').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],
        "lengthMenu": [ [ 10, 50, 100, 500, 1000], [10, 50,100,500, 1000] ],
		"order": [[ 0, "desc" ]],
		"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 4)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 4 ).footer() ).html(
      //'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});


table.buttons().container().appendTo('#mandates_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#Frequency').on('change', function(){
   table.search( this.value ).draw();
});

});
</script>




