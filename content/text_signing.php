<?php 
// -- // Actual PDF Page Size : [w] => 612 [h] => 792
?>    
	<title>Draggable, Moveable and Resizable DIV using jQuery</title>	
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <style>
        #divContainer, #divResize 
		{ 
            border:dashed 1px #CCC; 
            width:120px; 
            height:120px; 
            padding:5px; 
            margin:5px; 
            font:13px Arial; 
            cursor:move; 
            float:left 
        } 
		#sidebar-nav
		{
			/*position: fixed!important*/
		}
		.divResize
		{
			z-Index:10;
			position: absolute;
			display : none
		}
    </style>
<?php 
//https://javascript.info/mouse-drag-and-drop#potential-drop-targets-droppables
$rootpath = $_SERVER['DOCUMENT_ROOT'];
$basepath = str_replace('signing.php','signature.php',$_SERVER['PHP_SELF']);
$basepath = $rootpath.$basepath;
require $basepath;
$pagecount =  0;
	//require 'database.php';
	$signaturefile 		= '';
	$file 				= '';
	if ( !empty($_GET['signfile'])) 
	{
		$file = $_GET['signfile'];
		$data['file'] = $file;
		$pagecount = page_count($data);
	}

	if ( !empty($_GET['userid'])) 
	{
		$dataObject 		= null;
		$data['usersignid'] = $_GET['userid'];
		$dataObject			= db_get_signature($data);
		$signaturefile 		= $dataObject['usersignfile'];
	}
	
	if(!empty($_POST))
	{
		echo 'Saved';
	}
?>
<div class='container background-white bottom-border'>
	<div class='margin-vert-30'>
        <!-- Begin Sidebar Menu -->
        <div class="col-md-3" >
            <ul class="list-group sidebar-nav" id="sidebar-nav">
                <!-- Typography -->
                <li class="list-group-item list-toggle">
                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-typography">Fields</a>
                    <ul id="collapse-typography" >
                        <li>
                            <a href="javascript:add_signature()" >
                             <i class="fa fa-sort-alpha-asc" ></i>Signature</a>
							<!-- <input id="btClickMe" type="button" style="float:left"  value="Signature" />-->
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-magic"></i>Initials</a>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-magic"></i>Date Signed</a>
                        </li>
						<li>
							<div id="divResize" class="divResize">
								<img style="z-Index:10px" src="<?php echo $signaturefile; ?>" id="ball">
							</div>
						</li>
                    </ul>
                </li>
			</ul>
			<div id="navs"></div>	
			<input type="hidden" id="count" name="count" value="0" />
			<input type="hidden" id="signature" name="signature" value="<?php echo $signaturefile; ?>" />
			<input type="hidden" id="filetobesigned" name="filetobesigned" value="<?php echo $file; ?>" />
			<input type="hidden" id="count" name="count" value="0" />

			</div>
		</div>	
        <div class="col-md-9" style="z-Index:0">
            <h2>Document for eSignature</h2>
			<div id='saved' name='saved' class='alert alert-success fade in'>report here</div>			
			<table class="table table-user-information">
				<tr>
					<td><a class="btn btn-primary" href="mysigneddocs">Back</a></td>
					<td><button id="btnSave1" name="btnSave1" class="btn btn-primary">Save</button></td>					
				</tr>			
			</table>
			<div id="pdffileDIV">	<!-- // -- Actual PDF Page Size : [w] => 612 [h] => 792 -->		
				<iframe style="z-Index:0" id="pdffile" name="pdffile" src="<?php echo $file;?>#zoom=FitH&toolbar=0&navpanes=0&scrollbar=0&statusbar=0&messages=0&scrollbar=0"  frameborder="0" width="100%" height="<?php $height = 967 * $pagecount; echo $height."px";?>" style="z-Index:0">
				</iframe>
			</div>
			<div>		
			<table class="table table-user-information" style="z-Index:0">
				<tr>
					<td><a class="btn btn-primary" href="mysigneddocs">Back</a></td>
					<td><button id="btnSave" name="btnSave" class="btn btn-primary">Save</button></td>					
				</tr>			
			</table>						
			</div>		
		</div>
		</div>
<script>
function validated_signature_to_document()
{
		var results = 0;
		var saved = document.getElementById("saved");
		saved.className  = 'alert alert-success fade in';								  
		saved.innerHTML = "signature successfully saved.";	
		var el = document.getElementById("Div0");
		if(el !== null)
		{
						var pic = document.getElementById("ball0");
	pic.style.backgroundColor = "rgba(0, 0, 0, 0.30)";

			//alert(getElementPosition(el));
			//alert('frame width : '+ document.getElementById('pdffile').offsetWidth);
			//alert('frame height : '+ document.getElementById('pdffile').offsetHeight);
			
		}
		else
		{
			saved.className = 'alert alert-error fade in';								  
			saved.innerHTML = "no signature placed.";	
			results = 1;
		}
		
		return results;
}

function save_signature_to_document()
{
		var saved = document.getElementById("saved");
		saved.className  = 'alert alert-success fade in';								  
		saved.innerHTML = "signature successfully saved.";
		esignatureFile = document.getElementById('signature').value;
		einitials = 'TM';
		edate = '03.06.2020';
		elocation = 'Pretoria';
		
// -- Pass array of signatures & their position.
var signatureCount = parseInt(document.getElementById("count").value);
esignature = new Array();

for(i=0;i<signatureCount;i++)
{
	var el = document.getElementById("Div"+i);
	var pic = document.getElementById("ball"+i);
	//pic.style.opacity = 0.5;
	//alert("pic width "+pic.width);
	//img File & Position
	esignature1 = {"esignature" : esignatureFile
				  ,"position" : getElementPosition(el)+"|"+pic.outerHTML
				  };//position
	esignature[i] = esignature1;
	//alert(pic.outerHTML);
	remove_signature(el);
}
	// -- reinitialise
document.getElementById("count").value = 0;
	
		file	  = document.getElementById('filetobesigned').value;
// -- Call jquery PHP script to add_signature_to_pdf_doc, signature.php.		
		jQuery.ajax({  type: "POST",
					   url:"signature.php",
				       data:{esignature:esignature,einitials:einitials,edate:edate,elocation:elocation
							,file:file},
					success: function(data)
						 {
							 //alert(data);
							pdffile_iframe = document.getElementById("pdffile").outerHTML;
							//alert(pdffile_iframe);
							var rand = Math.floor((Math.random()*1000000)+1);
							    $('#pdffile').remove();

  $('#pdffileDIV').append(pdffile_iframe);

							 $("pdffile").attr("src",file+"?uid="+rand);
							//jQuery("#pdffile").contentDocument.location.reload();
							//jQuery("#pdffile").contentDocument.location= file;
						 },
					
			error: function(xhr, textStatus, error){
					alert("error: "+xhr.statusText+textStatus+error);	  
		}});		

}
function remove_signature(el)
{
    el.remove();
}

function add_signature()
{
	var element_pos 	= 0;
			var signaturefile	= document.getElementById('signature').value;
			var iCnt = document.getElementById('count').value;
			var dynDiv = $(document.createElement('div'));
			
            var dynamic_div = dynDiv.css({
                 border: '1px dashed',position: 'absolute', left: element_pos, 
                top: $('#divResize').height() + 20,zIndex:'10',
                width: '120', height: '60', padding: '0', margin: '0'
            ,id:'Div0'
			});
			dynamic_div.attr('id',"Div"+iCnt);

			element_pos = element_pos + $('#divResize').width() + 20;	
            
            // APPEND THE NEWLY CREATED DIV TO "divContainer".
            $(dynamic_div).appendTo('#navs').draggable().resizable();
			
			var removeButton = $("<span class='removeButton'>X</span>").click(function(){
                   // var parentDiv = $(dynamic_div);
                   //parentDiv.remove();
					remove_signature(dynamic_div);
                    // $('#source div').appendTo($('#source'));
                });
			$(dynamic_div).append(removeButton);
			var innerHTML = '<img src="'+signaturefile+'" id="ball'+iCnt+'">';
            $(dynamic_div).append(innerHTML);
			
			//alert("count : "+iCnt);
            iCnt = parseInt(iCnt) + 1;
			document.getElementById('count').value = iCnt ;
}
 
// -- https://www.encodedna.com/javascript/practice-ground/default.htm?pg=draggable_resizeable_html_dom_elements_using_jquery
$(document).ready(function() {
        $(function() { $("#divResize").draggable().resizable(); });
		
		
		
		// CREATE MORE DIV, WITH 'ABSOLUTE' POSITIONING.
        $('#btClickMe').click(function() 
		{
			var element_pos 	= 0;
			var signaturefile	= document.getElementById('signature').value;
			var iCnt = document.getElementById('count').value;
			var dynDiv = $(document.createElement('div'));

            var dynamic_div = dynDiv.css({
                border: '1px dashed', position: 'absolute', left: element_pos, 
                top: $('#divResize').height() + 20,zIndex:'10',
                width: '120', height: '60', padding: '3', margin: '0'
            ,id:'Div0'});
dynamic_div.attr('id',"Div"+iCnt);

			element_pos = element_pos + $('#divResize').width() + 20;
			
			var innerHTML = '<img src="'+signaturefile+'" id="ball'+iCnt+'">';
            $(dynamic_div).append(innerHTML);//'You can drag me too');
		
            
            // APPEND THE NEWLY CREATED DIV TO "divContainer".
            $(dynamic_div).appendTo('#sidebar-nav').draggable().resizable();
			
			var removeButton = $("<span class='removeButton'>X</span>").click(function(){
                    var parentDiv = $(dynamic_div);
                    parentDiv.remove();
                    // $('#source div').appendTo($('#source'));
                });
			$(dynamic_div).append(removeButton);
			//alert("count : "+iCnt);
            iCnt = parseInt(iCnt) + 1;
			document.getElementById('count').value = iCnt ;

        });
});
 $("#btnSave").click(function(e)
 {
	if(validated_signature_to_document() == 0)
	  {
		 save_signature_to_document();
	  }
});
 $("#btnSave1").click(function(e)
 {
	 if(validated_signature_to_document() == 0)
	  {
		 save_signature_to_document();
	  }
});

function convert_px_to_pt(pixels)
{
	// -- convert Pixels to Pt for FPDF
	// -- Dpi is 96dpi
	points = (pixels / 96) * 72;
	points = points.toFixed(0);
	return points;
}
function getElementPosition(el)
{
		// -- Actual PDF Page Size : [w] => 612 [h] => 792
		// -- we cannot have X > 612, also Y > 792.
		// -- 230 is constant y position outside of PDF.	
		// -- 340 is constant x position outside of PDF.		
		xMax = 500;//558;
		yMax = 750;//792
		screenPageSize = 928;
		pdfPageSize    = 782;
		dividePage     = screenPageSize - pdfPageSize;

		xConstant  = 270;
		yConstant  = 161;//165;
		page = 1;
		//alert("Before Minus :"+el.offsetTop)
		x = xConstant - el.offsetLeft;
		y =  yConstant - el.offsetTop;
		
		if(x <0){x = x * -1;}
		if(y <0){y = y * -1;}
				//alert(x+","+y);

		// -- Determine page number
			page = y / screenPageSize;
			page = parseInt(Math.trunc(page)) + 1;
			//alert("Page :"+page);

		if(page > 1)
		{
			// -- prev page
			/*ppyAbsolute 	= (page - 1) * yMax;
			ppyAbsoluteEnd  = yMax + dividePage;
			dividePage = (page - 1) * dividePage;
			alert(y);//928 //1860 // 1714         - 1564 = 150
			// -- new page       // 782            - 782  = 0
			npyAbsolute 		= (y - dividePage) - ppyAbsolute;
			npyAbsolutePosition = npyAbsolute;
			y = npyAbsolutePosition;
			if(y <0){y = y * -1;}*/
			dividePage = (page - 1) * dividePage;
			
			// -- Screen Page
			// -- prev_page_end
			prev_page_end 	= (page - 1) * screenPageSize;			
			// -- new_page_start
			new_page_start 	= prev_page_end;			

			// -- new_page_end
			new_page_end = new_page_start + screenPageSize;
			
			// -- y position
			if(y > new_page_end){y = new_page_end;}
			
			// -- absoulte page
			y = y - dividePage;
			if(y <0){y = y * -1;}			
			
		}
		else
		{
			//alert(y);
			//if(y>yMax){y = (y - yMax);}
		}
		//alert("x = "+x+" y = "+y);
		//if(x>xMax){x = xMax;}		
		//iframe page h=919px,w=709px 
		//fpdf page  [h]=792pt,[w]=612pt 
		// -- Convert from Pixels to Points
		//x = convert_px_to_pt(x);
		//y = parseInt(convert_px_to_pt(y));
		var position = x  + "|" + y +"|"+page;
		//alert(position);		
		return position;
}
</script>   