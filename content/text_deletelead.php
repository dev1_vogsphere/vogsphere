<?php 
	require 'database.php';
	$tablename = 'lead';
	$leadid = null;
	
	if ( !empty($_GET['leadid'])) 
	{
		$leadid = $_REQUEST['leadid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$leadid = $_POST['leadid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM $tablename WHERE leadid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($leadid));
			Database::disconnect();
			echo("<script>location.href='leads.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
    			<div class="span10 offset1">
	    			<form class="login-page"  class="form-horizontal" action="" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a lead 
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" id="leadid" name="leadid" value="<?php echo $leadid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="leads.php">No</a>
						</div>
					</form>
				</div>
</div> <!-- /container -->
