<?php
	require 'database_ffms.php';
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	require 'database.php';
    $pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$tbl_member = "member";
	$tbl_member_policy = "member_policy";
	$tbl_address = 'address';
	$dbname = "eloan";

	$state=null;
	$msg=null;
	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
	$count = 0;

	$IntUserCode = getsession('UserCode');
	//echo($IntUserCode );
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}

	if ( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}

			if ( null==$customerid )
				{
				header($location);

				}
				else if( null==$ApplicationId )
				{
				header($location);

				}
			else {

			$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
			AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id
			AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id";
			
			
			//echo $sql;
			$q = $pdo_ffms->prepare($sql);
			$q->execute(array($customerid,$ApplicationId));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			//print_r($data);
			
			
			$sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
			$q = $pdo->prepare($sql);
			$q->execute(array($data['Bank'],$data['Branch_code']));
			$dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);

			//1. ---------------------- BOC - Account Types ------------------------------- //
			// 11.08.2023 - 
			 $sql = 'SELECT * FROM accounttype';
			 $dataAccountType = $pdo->query($sql);						   						 
			// ---------------------- EOC - Account Types ------------------------------- //  
		
			//1. ---------------------- BOC - Bank ------------------------------- //
			// 11.08.2023 - 
			 $sql = 'SELECT * FROM bank';
			 $q = $pdo->prepare($sql);
			 $q->execute(array());
			 $dataBank = $q->fetchAll(PDO::FETCH_ASSOC);					   						 
			// ---------------------- EOC - Bank ------------------------------- //  

			
			//3. ---------------------- BOC - Payment Frequency ------------------------------- //
			// 11.08.2023 - 
			 $sql = 'SELECT * FROM paymentfrequency';
			 $dataPaymentFrequency = $pdo->query($sql);						   						 
			// ---------------------- EOC - Payment Frequency ------------------------------- //  
			
			//4. ---------------------- BOC - Payment Method ------------------------------- //
			// 11.08.2023 -  
			 $sql = 'SELECT * FROM method_of_contribution where IntUserCode=?';
			 $q = $pdo_ffms->prepare($sql);
			 $q->execute(array($IntUserCode));
			 $dataPaymentMethod = $q->fetchAll(PDO::FETCH_ASSOC);
			// print_r($dataPaymentMethod);
			 //print_r($data['payment_method']);			 
			// ---------------------- EOC - Payment Method ------------------------------- //  
			//5. ---------------------- BOC - Source of Income ------------------------------- //
			// 11.08.2023 -  
			$sql = 'SELECT * FROM source_of_income';
			$dataSourceofIncome =  $pdo_ffms->query($sql);						   						 
			
			// ---------------------- EOC - Policy Name ------------------------------- //  
			 $sql = 'SELECT * FROM policy where IntUserCode=?';
			 $q =  $pdo_ffms->prepare($sql);						   						 
			 $q->execute(array($IntUserCode));
			 $dataPolicy = $q->fetchAll(PDO::FETCH_ASSOC);
		
			// ---------------------- EOC - Policy Name ------------------------------- //  
			
			// ---------------------- EOC - Notify ------------------------------- //  
			 $sql = 'SELECT * FROM notify where IntUserCode=?';
			 $q =  $pdo_ffms->prepare($sql);						   						 
			 $q->execute(array($IntUserCode));
			 $dataNotify = $q->fetchAll(PDO::FETCH_ASSOC);
		
			// ---------------------- EOC - Notify ------------------------------- // 
			
			
			// ---------------------- EOC - policy_status  ------------------------------- //  
			 $sql = 'SELECT * FROM policy_status where IntUserCode=?';
			 $q =  $pdo_ffms->prepare($sql);						   						 
			 $q->execute(array($IntUserCode));
			 $dataPolicyStatus = $q->fetchAll(PDO::FETCH_ASSOC);
		
			// ---------------------- EOC - policy_status ------------------------------- //  
			
			
			// ---------------------- EOC - Premium  ------------------------------- //  
			 $sql = 'SELECT * FROM premium where IntUserCode=?';
			 $q =  $pdo_ffms->prepare($sql);						   						 
			 $q->execute(array($IntUserCode));
			 $dataPremium = $q->fetchAll(PDO::FETCH_ASSOC);
			// ---------------------- EOC - Premium ------------------------------- //  
			
			
			
			
			$TitleError=null; 
			$FirstNameError = null; 
			$Middle_nameError= null;
			$SurnameNameError= null;
			$Member_emailError= null;
			$CellphoneError= null;
			$Line_6Error=null;
			$DateOfBirthError=null;
			$Personal_numberError=null;
			$Province_nameError=null;
			//$Postal_codeError =null;
			$Title = $data['Title'];

			

			Database::disconnect();
			$_SESSION['ApplicationId'] = $ApplicationId;
			$_SESSION['customerid'] = $customerid;
			}

			// -- BOC 13/12/2022 - Page Features Authorization...	
			// -- Authorization of features
			//$role = $dataUserEmail['role'];//
			$dataPageFeaturesArr = null;
			$currentpageArr = null;
			$currentpageArr = explode("?",basename($_SERVER['REQUEST_URI'],".php"));
			$currentpage = $currentpageArr[0];
			$authorized_featuresArr = null;
			$role = $_SESSION['role'];

			$displaygenerate_payments='display:inline-block';
			$displaydownloadcontract ='display:inline-block';
			$displayupload_document ='display:inline-block';

			$authorized_featuresArr   = authorize_features($pdo,$currentpage,$role);
			//print_r($authorized_featuresArr);
			$displaygenerate_payments = $authorized_featuresArr['displaygenerate_payments'];
			$displaydownloadcontract  = $authorized_featuresArr['displaydownloadcontract'];
			$displayupload_document   = $authorized_featuresArr['displayupload_document'];
			// -- EOC 13/12/2022 - Page Features Authorization...	

			// -- BOC CR1002 - 31/01/2023 - Only show generate payment button on APPROVE..	
			if ($data['Active'] != '0')
			{
				$displaygenerate_payments  = 'display:none';
			}
			if ($data['Active'] == '0')
			{
				$displayupload_document    = 'display:inline-block';
				$displaydownloadcontract   = 'display:inline-block';
			}
			// -- EOC 31/01/2023 - Only show generate payment button on APPROVE..	
			$displaygenerate_payments  = 'display:none';
			
			//print_r	($data);
			//update data 
			
			
			if(isset($_POST['Update']))
			{
				
				$Title= $_POST['Title'];
				$FirstName= $_POST['FirstName'];
				$Middle_name= $_POST['Middle_name'];
				$LastName= $_POST['LastName'];
				$Memberemail= $_POST['Member_email'];
				$Cellphone= $_POST['Cellphone'];
				$Street_address= $_POST['Street_address'];
				$Surburb_name= $_POST['Surburb_name'];
				$Town_name= $_POST['Town_name'];
				$Province_name= $_POST['Province_name'];
				//$Postal_code= $_POST['Postal_code'];
				//Banking
				$Debtorname= $_POST['Debtorname'];
				$Bank= $_POST['Bank'];
				$Account_number= $_POST['Account_number'];
				$Branch_code= $_POST['Branch_code'];
				$Account_type= $_POST['Account_type'];
				$Policy_name=$_POST['Policy_name'];
				$joined_date=$_POST['Creation_date'];
				$Source_of_income_name=$_POST['Source_of_income_name'];
				$Income_provider_name=$_POST['Income_provider_name'];
				$Commence_date=$_POST['Commence_date'];
				$Debit_date=$_POST['Debit_date'];
				$notify=$_POST['notify'];
				$policy_status=$_POST['policy_status'];
				
				// validate input - 		// Customer Details
				$valid = true;
				if (empty($Title)) { $TitleError = 'Please enter title'; $valid = false;}
				if (empty($FirstName)) { $FirstNameError = 'Please enter member name'; $valid = false;}
				//if (empty($Middle_name)) { $Middle_nameError = 'Please enter member middle name'; $valid = false;}
				if (empty($LastName)) { $SurnameNameError = 'Please enter member surname'; $valid = false;}
				//if (empty($Memberemail)) { $Member_emailError = 'Please enter member email address'; $valid = false;}
				if (empty($Cellphone)) { $CellphoneError = 'Please enter member cellphone number'; $valid = false;}
				//if (empty($Street_address)) { $Line_5Error= 'Please enter Line 1 address'; $valid = false;}
				//if (empty($Surburb_name)) { $Surburb_nameError= 'Please enter suburb name'; $valid = false;}
				//if (empty($Town_name)) { $Town_nameError= 'Please enter town'; $valid = false;}
				//if (empty($Province_name)) { $Provine_nameError= 'Please enter province'; $valid = false;}
				//if (empty($Postal_code)) { $Postal_codeError= 'Please enter potsal code'; $valid = false;}
				if (empty($Debtorname)) { $Account_holderError= 'Please enter debtor name'; $valid = false;}
				if (empty($Account_number)) { $Account_numberError= 'Please enter account number'; $valid = false;}
				if (empty($Branch_code)) { $Branch_codeError= 'Please enter branch code'; $valid = false;}
				//if (empty($Income_provider_name)) { $Income_provider_nameError= 'Please enter income_provider_name'; $valid = false;}
				//if (empty($Commence_date)) { $Commence_dateError= 'Please enter commence date'; $valid = false;}
				//if (empty($Debit_date)) { $Debit_dateError= 'Please enter debit order date'; $valid = false;}
				
				if ($valid)
				{
				$sql = "UPDATE $tbl_member,$tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium SET Title=?,First_name=?,Middle_name=?,Surname=?,Member_email=?,Cellphone=?,Line_5=?,Line_6=?,Line_7=?,Line_8=?,Account_holder=?,Bank=?,Account_number=?,Branch_code=?,Account_type=?,member_policy.Policy_id=?,Creation_date=?,member_income.Source_of_income_id=?,Income_provider_name=?,Commence_date=?,Debit_date=?,notify=?,member_policy.Active=? WHERE $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
				AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id
				AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id";
				$q = $pdo_ffms->prepare($sql);
				$q->execute(array($Title,$FirstName,$Middle_name,$LastName,$Memberemail,$Cellphone,$Street_address,$Surburb_name,$Town_name,$Province_name,$Debtorname,$Bank,$Account_number,$Branch_code,$Account_type,$Policy_name,$joined_date,$Source_of_income_name,$Income_provider_name,$Commence_date,$Debit_date,$notify,$policy_status,$customerid,$ApplicationId));
				//$results = $q->fetch(PDO::FETCH_ASSOC);
				//print_r ($results);
			
				if($q)
				{
					
					$state="alert alert-success";
					$msg="Client information updated successfully!";
				}
				else
				{	$state="alert alert-danger";
					$msg=$q;
					
				}
				}
			}
			

?>

<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div class="row">
<!-- Login Box -->
<form class="login-page" action="" method="post">
<div class="table-responsive">


<table class=   "table table-user-information">
<!-- Customer Details -->
<!-- Title,FirstName,LastName -->
<tr>
<td>
<p class="<?php echo ($state);?>" role="alert"><?php echo ($msg);?></p>
</td>
</tr>
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Main Member Details
                    </a>
                </h2>
</td></div>

</tr>

<tr>
<td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Title</strong></label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="Title" type="text"  placeholder="Title" value="<?php echo !empty($Title)?$Title:'';?>"> -->
			
			<SELECT class="form-control" id="Title" name="Title" size="1" >
						<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
						<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
						<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
						<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
						<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
			</SELECT>
			
			<?php if (!empty($TitleError)): ?>
			<span class="help-inline"><?php echo $TitleError;?></span>
			<?php endif; ?>
		</div>
		
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>Name</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text" placeholder="First Name" 
			value="<?php echo !empty($data['First_name'])?$data['First_name']:'';?>">
			<?php if (!empty($FirstNameError)): ?>
			<span class="help-inline"><?php echo $FirstNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Middle_nameError)?'error':'';?>">
		<label class="control-label"><strong>Middle Name</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Middle_name" type="text" placeholder="Middle name" 
			value="<?php echo !empty($data['Middle_name'])?$data['Middle_name']:'';?>">
			<?php if (!empty($Middle_nameError)): ?>
			<span class="help-inline"><?php echo $Middle_nameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($SurnameNameError)?'error':'';?>">
		<label class="control-label"><strong>Surname</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" 
			value="<?php echo !empty($data['Surname'])?$data['Surname']:'';?>">
			<?php if (!empty($SurnameNameError)): ?>
			<span class="help-inline"><?php echo $SurnameNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

</tr>
<!-- Street,Suburb,City -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($Line_5Error)?'error':'';?>">
		<label class="control-label"><strong>Address</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Street_address" type="text"  placeholder="Street/Postal address" 
			value="<?php echo !empty($data['Line_5'])?$data['Line_5']:'';?>">
			<?php if (!empty($Line_5Error)): ?>
			<span class="help-inline"><?php echo $Line_5Error;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($Surburb_nameError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Surburb_name" type="text"  placeholder="Suburb" 
			value="<?php echo !empty($data['Line_6'])?$data['Line_2']:'';?>">
			<?php if (!empty($Surburb_nameError)): ?>
			<span class="help-inline"><?php echo $Surburb_nameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Town_nameError)?'error':'';?>">
		<label class="control-label"><strong>Town</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Town_name" type="text"  placeholder="Town" 
			value="<?php echo !empty($data['Line_7'])?$data['Line_7']:'';?>">
			<?php if (!empty($Town_nameError)): ?>
			<span class="help-inline"><?php echo $Town_nameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Province_nameError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Province_name" type="text"  placeholder="Province" 
			value="<?php echo !empty($data['Line_8'])?$data['Line_8']:'';?>">
			<?php if (!empty($Province_nameError)): ?>
			<span class="help-inline"><?php echo $Province_nameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
</tr>

<!-- State,PostCode,Dob -->
<tr>

	<td>
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Date of birth</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="DateOfBirth" type="Date"  readonly placeholder="Date of birth" 
			value="<?php echo !empty($data['Date_of_birth'])?$data['Date_of_birth']:'';?>">
			<?php if (!empty($DobError)): ?>
			<span class="help-inline"><?php echo $DobError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($CellphoneError)?'error':''?>">
		<label class="control-label"><strong>Cell number</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Cellphone" type="text"  placeholder="Cell phone" 
			value="<?php echo !empty($data['Cellphone'])?$data['Cellphone']:'';?>">
			<?php if (!empty($CellphoneError)): ?>
			<span class="help-inline"><?php echo $CellphoneError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Member_emailError)?'error':'';?>" >
		<label class="control-label"><strong>Email address</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Member_email" type="text"  placeholder="Email address" 
			value="<?php echo !empty($data['Member_email'])?$data['Member_email']:'';?>">
			<?php if (!empty($Member_emailError)): ?>
			<span class="help-inline"><?php echo $Member_emailError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>
<tr>

	
</tr>
<!----------------------------------------------------------- Banking Details -------------------------------------------->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>
</td></div>

</tr>
<!-- Account Holder Name,Bank Name,Account number -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($Account_holderError)?'error':'';?>">
		<label class="control-label"><strong>Debtor name</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Debtorname" type="text" placeholder="Debtor name" 
			value="<?php echo !empty($data['Account_holder'])?$data['Account_holder']:'';?>">
			<?php if (!empty($Account_holderError)): ?>
			<span class="help-inline"><?php echo $Account_holderError;?></span>
			<?php endif; ?>
		</div>

	</div>
	</td>

	<td>
	<div class="control-group ">
		<label class="control-label"><strong>Bank name</strong></label>
		<SELECT class="form-control" id="Bank" name="Bank"  size="1">
			<!-------------------- BOC 2017.04.15 -  Account type --->	
			<?php
			$bankid = '';
			$bankSelect = $data['Bank'];
			$bankname = '';
			foreach($dataBank as $row)
			{
			$bankid = $row['bankid'];
			$bankname = $row['bankname'];
			// Select the Selected Role 
			if($bankid  == $bankSelect)
			{
			echo "<OPTION value=$bankid selected>$bankname</OPTION>";
			}
			else
			{
			echo "<OPTION value=$bankid>$bankname</OPTION>";
			}
			}

			if(empty($dataBank))
			{
			echo "<OPTION value=0>No Bank Selected</OPTION>";
			}
			?>
			<!-------------------- EOC 2017.04.15 - Account type --->
			</SELECT>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($Account_numberError)?'error':'';?>">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Account_number" type="text"   placeholder="Account number" 
			value="<?php echo !empty($data['Account_number'])?$data['Account_number']:'';?>">
			<?php if (!empty($Account_numberError)): ?>
			<span class="help-inline"><?php echo $Account_numberError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($Branch_codeError)?'error':'';?>">
		<label class="control-label"><strong>Branch code</strong></label>
			<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Branch_code" type="text"  placeholder="Branch code" 
			value="<?php echo !empty($data['Branch_code'])?$data['Branch_code']:'';?>">
			<?php if (!empty($Branch_codeError)): ?>
			<span class="help-inline"><?php echo $Branch_codeError;?></span>
			<?php endif; ?>
		</div>
		
	</div>
	</td>
	
	
</tr>
<!-- branch code,Account type -->
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account type</strong></label>
			<div class="controls">
			<SELECT class="form-control" id="Account_type" name="Account_type"  size="1">
			<!-------------------- BOC 2017.04.15 -  Account type --->	
			<?php
			$accounttypeid = '';
			$accounttySelect = $data['Account_type'];
			$accounttypedesc = '';
			foreach($dataAccountType as $row)
			{
			$accounttypeid = $row['accounttypeid'];
			$accounttypedesc = $row['accounttypedesc'];
			// Select the Selected Role 
			if($accounttypeid == $accounttySelect)
			{
			echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
			}
			else
			{
			echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
			}
			}

			if(empty($dataAccountType))
			{
			echo "<OPTION value=0>No Account Type</OPTION>";
			}
			?>
			</SELECT>
			</div>	
	</div>
	</td>
</tr>

<!----------------------------------------------------------- End Banking Details ---------------------------------------->

<!----------------------------------------------------------- Loan Details -------------------------------------------->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Membership Details
                    </a>
                </h2>
</td></div>

</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($Member_idError)?'error':'';?>">
		<label class="control-label"><strong>Member identifcation number</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Member_id" type="text" Readonly placeholder="Member ID" 
			value="<?php echo !empty($data['Member_id'])?$data['Member_id']:'';?>">
			<?php if (!empty($Member_idError)): ?>
			<span class="help-inline"><?php echo $Member_idError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Member_Policy_idError)?'error':'';?>">
		<label class="control-label"><strong>Policy number</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Member_Policy_id" type="text"  Readonly placeholder="Member Policy id" 
			value="<?php echo !empty($data['Member_Policy_id'])?$data['Member_Policy_id']:'';?>">
			<?php if (!empty($Member_Policy_idError)): ?>
			<span class="help-inline"><?php echo $Member_Policy_idError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
		<div class="control-group <?php echo !empty($Policy_nameError)?'error':'';?>">
			<label class="control-label"><strong>Policy name</strong></label>
			<SELECT class="form-control" id="Policy_name" name="Policy_name" size="1">
						<!-------------------- BOC 2017.04.15 -  Payment Method --->	
						<?php
						$Policy_id = '';
						$PolicySelect = $data['Policy_id'];
						$Policy_desc = '';
						foreach($dataPolicy as $row)
						{
						$Policy_id = $row['Policy_id'];
						$Policy_desc = $row['Policy_name'];
						// Select the Selected Role 
						if($Policy_id == $PolicySelect)
						{
						echo "<OPTION value=$Policy_id selected>$Policy_desc</OPTION>";
						}
						else
						{
						echo "<OPTION value=$Policy_id>$Policy_desc</OPTION>";
						}
						}

						if(empty($dataPolicy))
						{
						echo "<OPTION value=0>No Policy Selected</OPTION>";
						}
						?>
						<!-------------------- EOC 2.04.15 - Payment Method. --->
						</SELECT>
		</div>
		</td>

	<td>
	<div class="control-group <?php echo !empty($Policy_descError)?'error':'';?>">
			<label class="control-label"><strong>Monthly subscription</strong></label>
			<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Policy_desc" type="text"  Readonly placeholder="" 
			value="<?php echo !empty($data['Policy_desc'])?$data['Policy_desc']:'';?>">
			<?php if (!empty($Policy_descError)): ?>
			<span class="help-inline"><?php echo $Policy_descError;?></span>
			<?php endif; ?>
			</div>
		
	</div>
</td>
	
</tr>

<!-- ReqLoadValue,DateAccpt,LoanDuration -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($BenefitsError)?'error':'';?>">
		<label class="control-label"><strong>Benefit amount</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Benefits" type="text" Readonly placeholder="" 
			value="<?php echo !empty($data['Benefits'])?$data['Benefits']:'';?>">
			<?php if (!empty($BenefitsError)): ?>
			<span class="help-inline"><?php echo $BenefitsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($Creation_dateError)?'error':'';?>">
		<label class="control-label"><strong>Join date</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Creation_date" type="Date"  placeholder="Creation date" 
			value="<?php echo !empty($data['Creation_date'])?$data['Creation_date']:'';?>">
			<?php if (!empty($Creation_dateError)): ?>
			<span class="help-inline"><?php echo $Creation_dateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	
	
	<td>
	<div class="control-group <?php echo !empty($Spouse_idError)?'error':'';?>">
		<label class="control-label"><strong>Spouse identification number</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Spouse_id" type="text"  Readonly placeholder="Spouse ID" 
			value="<?php echo !empty($data['Spouse_id'])?$data['Spouse_id']:'';?>">
			<?php if (!empty($Spouse_idError)): ?>
			<span class="help-inline"><?php echo $Spouse_idError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
		<td>
	<div class="control-group <?php echo !empty($Source_of_income_nameError)?'error':'';?>">
		<label class="control-label"><strong>Source of income</strong></label>
		<SELECT class="form-control" id="Source_of_income_name" name="Source_of_income_name" size="1">
					<!-------------------- BOC 2017.04.15 -  Payment Method --->	
					<?php
					$Source_of_income_id = '';
					$Source_of_incomeSelect = $data['Source_of_income_id'];
					$Source_of_income_name = '';
					foreach($dataSourceofIncome as $row)
					{
					$Source_of_income_id = $row['Source_of_income_id'];
					$Source_of_income_name = $row['Source_of_income_name'];
					// Select the Selected Role 
					if($Source_of_income_id == $Source_of_incomeSelect)
					{
					echo "<OPTION value=$Source_of_income_id selected>$Source_of_income_name</OPTION>";
					}
					else
					{
					echo "<OPTION value=$Source_of_income_id>$Source_of_income_name</OPTION>";
					}
					}

					if(empty($dataSourceofIncome))
					{
					echo "<OPTION value=0>No Payment Method</OPTION>";
					}
					?>
					<!-------------------- EOC 2.04.15 - Payment Method. --->
					</SELECT>
	</div>
	</td>
	

</tr>
<!-- Spouse_id -->
<tr>
	<td>
		<div class="control-group <?php echo !empty($Income_provider_nameError)?'error':'';?>">
				<label class="control-label"><strong>Income provider name</strong></label>
				<div class="controls">
				<input style="height:30px" class="form-control margin-bottom-10" name="Income_provider_name" type="text"  placeholder="" 
				value="<?php echo !empty($data['Income_provider_name'])?$data['Income_provider_name']:'';?>">
				<?php if (!empty($Income_provider_nameError)): ?>
				<span class="help-inline"><?php echo $Income_provider_nameError;?></span>
				<?php endif; ?>
				</div>
		</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($PolicyStatusError)?'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<SELECT class="form-control" id="policy_status_id" name="policy_status" size="1">
					<!-------------------- BOC 2017.04.15 -  Payment Method --->	
					<?php
					$policy_status_id = '';
					$policystatusSelect = $data['Active'];
					$policy_status_desc = '';
					foreach($dataPolicyStatus as $row)
					{
					$policy_status_id = $row['policy_status_id'];
					$policy_status_desc = $row['policy_status_desc'];
					// Select the Selected Role 
					if($policy_status_id == $policystatusSelect)
					{
					echo "<OPTION value=$policy_status_id selected>$policy_status_desc</OPTION>";
					}
					else
					{
					echo "<OPTION value=$policy_status_id>$policy_status_desc</OPTION>";
					}
					}

					if(empty($dataPolicyStatus))
					{
					echo "<OPTION value=0>Draft Mode</OPTION>";
					}
					?>
		</SELECT>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Notify me</strong></label>
		<SELECT class="form-control" id="notify" name="notify" size="1">
					<!-------------------- BOC 2017.04.15 -  Payment Method --->	
					<?php
					$notify_id = '';
					$notifySelect = $data['notify'];
					$notify_desc = '';
					foreach($dataNotify as $row)
					{
					$notify_id = $row['notify_id'];
					$notify_desc = $row['notify_desc'];
					// Select the Selected Role 
					if($notify_id == $notifySelect)
					{
					echo "<OPTION value=$notify_desc selected>$notify_desc</OPTION>";
					}
					else
					{
					echo "<OPTION value=$notify_desc>$notify_desc</OPTION>";
					}
					}

					if(empty($dataNotify))
					{
					echo "<OPTION value=0>None</OPTION>";
					}
					?>
					<!-------------------- EOC 2.04.15 - Payment Method. --->
		</SELECT>
	</div>
	</td>
	
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Payment method</strong></label>
					<SELECT class="form-control" id="paymentmethod" name="paymentmethod" Readonly size="1">
				
					<?php
					$paymentmethodid = '';
					$paymentmethodSelect = '2';
					$paymentmethoddesc = '';
					foreach($dataPaymentMethod as $row)
					{
					$paymentmethodid = $row['Method_of_contribution_id'];
					$paymentmethoddesc = $row['Method_of_contribution_name'];
					// Select the Selected Role 
					if($paymentmethodid == $paymentmethodSelect)
					{
					echo "<OPTION value=$paymentmethodid selected>$paymentmethoddesc</OPTION>";
					}
					else
					{
					echo "<OPTION value=$paymentmethodid>$paymentmethoddesc</OPTION>";
					}
					}

					if(empty($dataPaymentMethod))
					{
					echo "<OPTION value=0>No Payment Method</OPTION>";
					}
					?>
				
					</SELECT>
		</div>
	</td>
	
	

</tr>
<tr>
	
	
	

</tr>
<tr>
<td>
		<div class="control-group <?php echo !empty($Commence_dateError)?'error':'';?>">
		<label class="control-label"><strong>First collection date</strong></label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Commence_date" type="Date"  placeholder="Commence_date" 
			value="<?php echo !empty($data['Commence_date'])?$data['Commence_date']:'';?>">
			<?php if (!empty($Commence_dateError)): ?>
			<span class="help-inline"><?php echo $Commence_dateError;?></span>
			<?php endif; ?>
		</div>
		</div>
	</td>
	
	<td>
		<div class="control-group <?php echo !empty($Debit_dateError)?'error':'';?>">
				<label class="control-label"><strong>Debit order date</strong></label>
			<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Debit_date" type="Date"  placeholder="Debit date" 
			value="<?php echo !empty($data['Debit_date'])?$data['Debit_date']:'';?>">
			<?php if (!empty($Debit_dateError)): ?>
			<span class="help-inline"><?php echo $Debit_dateError;?></span>
			<?php endif; ?>
			</div>
		</div>
		</div>
	</td>
</tr>
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
$rejectreason = $data['Remarks'];
if ($data['Active'] == '3')
{
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div>
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						  <a class="btn btn-primary" href="finfindmember">Back</a>
					<!-- <span></span>-->
					<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
					<?php
									
					if(isset($_SESSION))
					{
					$role = $_SESSION['role'];
					}
					// -- Generate Account Number
					$ReferenceNumber = GenerateAccountNumber($customerid);
					$AccountHolder	 = $data['Personal_number'];
					$BranchCode	 = $data['Branch_code'];
					$AccountNumber	 =  $data['Account_number'];
					$Servicetype	 = 'NADREQ';
					$ServiceMode	 = 'NAEDO';
					$UserCode      = 'UFEC';
					$IntUserCode   = 'UFEC';
					$Entry_Class   = 'UFEC';
					$Contact_No		 = 'UFEC';
					$Notify				 = 'UFEC';
					$createdby     = 'UFEC';
					$createdon     = 'UFEC';
					$AccountType	 = $data['Account_type'];
					$fname 			 = $data['First_name'];
					$lname 			 = $data['Surname'];
					$frequency 		 = '';//$data['paymentfrequency'];
					// Show if the role is administrator
					//	if($role == 'admin')
						{
					$href = "'"."href=debitorderschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency&fname=$fname&lname=$lname"."'";
											 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="debitorderschedule('.$href.')" style="'.$displaygenerate_payments.'">Generate Payments</button>';
						}
						 ?>
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>

								</div>
								</br> -->
											<div id="divdebitorderschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Debit Order Schedules</h1>
											</div>
							<!-- <div id="divSuccess">

								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>
						</div>
					  </td>
					<!--<button type="submit" name="Update" class="btn btn-success">Update</button>-->
					<!-- 13.06.2018 - Debit Order Schedule -->
	<?php
	echo "<td><div>
		<button type='submit' name='Update' class='btn btn-primary'>Update</button></div></td>";

	
	// -- if no role mean user never logged in go back to login page.
	if (empty($role))
	{
	//header("Location: customerLogin");
	echo '<script>alert("Data Updated")</script>';
	}

	// -- end of login check.

	// Only show contract if eloan application has been approved.
	if($data['Active'] == 0)// && $role == 'customer') 13/12/2022
	{
	  $hash="&guid=".md5(date("h:i:sa"));
	  // -- BOC Encode Change - 16.09.2017.

		
		

	  // -- EOC Encode Change - 16.09.2017.
	}
/* 13/12/2022 - 
	// Show if the role is administrator
	if($role == 'admin')
	{
		$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Download Contract</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.

	}
*/

	?>

						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php
	$ApplicationId = $data['Member_Policy_id'];
	$customerid = $data['Member_id'];
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
	echo "<a class='btn btn-primary' style='".$displayupload_document."' href=uploadffms?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
						</table>
									</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>

   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
//$filerootpath = 'ffms/upload/';
$filerootpath = '';
//str_replace('content','',filerootpath()).
    // $pdo = Database::connectDB();
	 $tbl_name="loanapp"; // Table name
	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	 $check_user = "SELECT * FROM member WHERE Member_id = ?";
	
	 $pdo = db_connect::db_connection();			
	 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 $sql =  $check_user;
	 $q = $pdo->prepare($sql);
	 $q->execute(array($customerid));
	 $row = $q->fetch(PDO::FETCH_ASSOC);
	 $count = $q->rowCount();

	 // -- ID Document
	 $iddocument = '';

	 // -- Contract Agreement
	 $contract = '';

	 // -- Bank statement
	 $bankstatement = '';

	 // -- Proof of income
	 $proofofincome = '';

	 // -- Fica
	 $fica = '';

	 $consentform = '';
	 $other = '';
   	 $other2 = '';
	 // --

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = '';
	 $other3    = "";
	 $other4    = "";
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);


	//if($count >= 1)
    {

	//$row = mysql_fetch_array($result);


	$iddocument  =  $row['FILEIDDOC'];
	$contract = $row['FILECONTRACT'];
	$bankstatement = $row['FILEBANKSTATEMENT'];
	$proofofincome = $row['FILEPROOFEMP'];
	$fica = $row['FILEFICA'];

   // -- BOC 01.10.2017 - Upload Credit Reports.
   	$creditreport = $row['FILECREDITREP'];

	$consentform = $row['FILECONSENTFORM'];
	$other = $row['FILEOTHER'];
   	$other2 = $data['FILEOTHER2'];
	$NoFIle = "";
   // -- EOC 01.10.2017 - Upload Credit Reports.

   	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = $row['FILEDEBITFORM'];
	 $other3    = $row['FILEOTHER3'];
	 $other4    = $row['FILEOTHER4'];
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


   // -- BOC 01.10.2017 - Alternative if no file upload.
   if(empty($iddocument)){$iddocument = $NoFIle;}
   if(empty($contract)){ $contract = $NoFIle;}
   if(empty($bankstatement)){$bankstatement = $NoFIle;}
   if(empty($proofofincome)){$proofofincome = $NoFIle;}
   if(empty($fica)){ $fica = $NoFIle;}
   if(empty($creditreport)){  $creditreport = $NoFIle;}
   if(empty($consentform)){  $consentform = $NoFIle;}
   if(empty($other)){  $other = $NoFIle;}
   if(empty($other2)){  $other2 = $NoFIle;}

   if(empty($debitform)){$debitform = $NoFIle;}
   if(empty($other3)){  $other3 = $NoFIle;}
   if(empty($other4)){  $other4 = $NoFIle;}
   // -- EOC 01.10.2017 - Alternative if no file upload.

    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";

	$iddocumentHTML = '';
	if($iddocument == '')
	{
		$iddocumentHTML = "<tr>
         <td><a href='$filerootpath$iddocument' target='_blank'>$iddocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	else
	{
		$iddocumentHTML = "<tr>
         <td><a $classCSS href='$filerootpath$iddocument' target='_blank'>$ViewDocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	$contractHTML = '';
	if($contract == '')
	{
		$contractHTML = "<tr>


         <td><a href='$filerootpath$contract' target='_blank'>$contract</a></td>
         <td>Contract Agreement</td>
      </tr>
      ";
	}
	else
	{
		$contractHTML = "

      <tr>
         <td><a $classCSS href='$filerootpath$contract' target='_blank'>$ViewDocument</a></td>
         <td>Contract Agreement</td>
      </tr>";
	}

	$bankstatementHTML = '';
	if($bankstatement == '')
	{
		$bankstatementHTML = "<tr>
         <td><a href='$filerootpath$bankstatement' target='_blank'>$bankstatement</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	else
	{
		$bankstatementHTML = "<tr>
         <td><a $classCSS href='$filerootpath$bankstatement' target='_blank'>$ViewDocument</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}

	$proofofincomeHTML = '';
	if($proofofincome == '')
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a href='$filerootpath$proofofincome' target='_blank'>$proofofincome</a></td>
         <td>Proof of income</td>
      </tr>
	  ";
	}
	else
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$proofofincome' target='_blank'>$ViewDocument</a></td>

         <td>Proof of income</td>
      </tr>
	  ";
	}
	$ficaHTML = '';
	if($fica == '')
	{
		$ficaHTML = "

	  <tr>
         <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	else
	{
		$ficaHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$fica' target='_blank'>$ViewDocument</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	$creditreportHTML = '';
	if($creditreport == '')
	{
		$creditreportHTML = "
	  <tr>
         <td><a href='$filerootpath$creditreport' target='_blank'>$creditreport</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	else
	{
		$creditreportHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$creditreport' target='_blank'>$ViewDocument</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	$consentformHTML = '';
	if($consentform == '')
	{
		$consentformHTML = "
	  <tr>
         <td><a href='$filerootpath$consentform' target='_blank'>$consentform</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	else
	{
		$consentformHTML = "
	  <tr>
         <td><a  $classCSS href='$filerootpath$consentform' target='_blank'>$ViewDocument</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	$otherHTML = '';
	if($other == '')
	{
		$otherHTML ="
	  <tr>
         <td><a href='$filerootpath$other' target='_blank'>$other</a></td>
         <td>Other</td>
      </tr>
	";
	}
	else
	{
		$otherHTML ="
	  <tr>
         <td><a $classCSS href='$filerootpath$other' target='_blank'>$ViewDocument</a></td>
         <td>Other</td>
      </tr>
	";
	}
	$other2HTML = '';
	if($other2 == '')
	{
		$other2HTML = "
  	  <tr>
         <td><a href='$filerootpath$other2' target='_blank'>$other2</a></td>
         <td>Other 2</td>
      </tr>
";
	}
	else
	{
		$other2HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other2' target='_blank'>$ViewDocument</a></td>
         <td>Other 2</td>
      </tr>";
	}


	$debitformHTML = '';
	if($debitform == '')
	{
		$debitformHTML = "
  	  <tr>
         <td><a href='$filerootpath$debitform' target='_blank'>$debitform</a></td>
         <td>Debit Form</td>
      </tr>
";
	}
	else
	{
		$debitformHTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$debitform' target='_blank'>$ViewDocument</a></td>
         <td>Debit Form</td>
      </tr>";
	}

	$other3HTML = '';
	if($other3 == '')
	{
		$other3HTML = "
  	  <tr>
         <td><a href='$filerootpath$other3' target='_blank'>$other3</a></td>
         <td>Other 3</td>
      </tr>
";
	}
	else
	{
		$other3HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other3' target='_blank'>$ViewDocument</a></td>
         <td>Other 3</td>
      </tr>";
	}

	$other4HTML = '';
	if($other4 == '')
	{
		$other4HTML = "
  	  <tr>
         <td><a href='$filerootpath$other4' target='_blank'>$other4</a></td>
         <td>Other 4</td>
      </tr>
";
	}
	else
	{
		$other4HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other4' target='_blank'>$ViewDocument</a></td>
         <td>Other 4</td>
      </tr>";
	}


   echo "<tbody>"
      .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$consentformHTML.$creditreportHTML.$otherHTML.$other2HTML.$debitformHTML.$other3HTML.$other4HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->
</table>
 <!------------------------------ End File List ------------------------------------>

<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------>
					</form>


                            <!-- End Login Box -->
                        </div>

                </div>
            <!-- === END CONTENT === -->
