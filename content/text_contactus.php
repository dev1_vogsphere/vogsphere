            <!-- === BEGIN CONTENT === -->
			<div class='container background-white bottom-border'>
                    <div class="row margin-vert-30">
                        <!-- Main Column -->
                        <div class="col-md-6">
                            <!-- Main Content -->
														<div class="panel panel-default">
															<div class="panel-heading">
                                <h2 class="panel-title">Get In Touch</h2>
															</div>
                            </div>
                            <!--<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas feugiat. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor
                                sit amet, consectetur adipiscing elit landitiis.</p>-->
                            <br>
                            <!-- Contact Form -->
							<div  id="divContent">
                <form>
								<label class="p1">Name<span class="color-red">*</span>
								</label>
								<div id="FirstNameError" class="form-group">
                                    <div class="row margin-bottom-20">
										<div class="col-sm-6">
											<input class="form-control margin-bottom-10" id="FirstName" name="FirstName" type="text"  placeholder="" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										</div>
										<div class="p1" class="col-sm-4">
											<small id="FirstNameErrorText" class="help-block" style=""></small>
										</div>
									</div>
								</div>
								<label class="p1">Contact Number
									<span class="color-red">*</span>
								</label>
								<div id="phoneError" class="form-group">
									 <div class="row margin-bottom-20">
									   <div class="col-sm-6">
											<input class="form-control margin-bottom-10" id="phone" name="phone" type="text"  placeholder="" value="<?php echo !empty($Lphone)?$Lphone:'';?>">
										</div>
										<div class="col-sm-4">
											<small id="phoneErrorText" class="help-block" style=""></small>
										</div>
									 </div>
								</div>

								<div id="emailError" class="form-group">
									<label class="p1">Email Address
										<span class="color-red">*</span>
									</label>
									 <div class="row margin-bottom-20">
									   <div class="col-sm-6">
											<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="" value="<?php echo !empty($Lemail)?$Lemail:'';?>">
										</div>
										<div class="col-sm-4">
											<small id="emailErrorText" name="emailErrorText" class="help-block" style=""></small>
									 </div></div>
								</div>
                <label class="p1">Message
									<span class="color-red">*</span>
								</label>
								 <div id="msgError" class="form-group">
									<div class="row margin-bottom-20">
										<div class="col-md-8 col-md-offset-0">
											<textarea id="msg" name="msg" rows="8" class="form-control"><?php echo !empty($msg)?$msg:'';?></textarea>
										</div>
										<div class="col-sm-4">
											<small id="msgErrorText" class="help-block" style=""></small>
										</div>
									</div>
								 </div>
                                <p class="p1">
                                    <!--<button type="submit" class="btn btn-primary">Send Message</button>-->
									<button type="button" class="btn btn-primary btn-label-left" onclick="validate_save()">
									Send Message</button>
                                </p>
                            </form>
							</div>
                            <!-- End Contact Form -->
                            <!-- End Main Content -->
                        </div>
                        <!-- End Main Column -->
                        <!-- Side Column -->
                        <div class="col-md-6">
                            <!-- Recent Posts -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title">Contact Info</h2>
                                </div>
                                <div class="panel-body">
                                    <!--<p>Lorem ipsum dolor sit amet, no cetero voluptatum est, audire sensibus maiestatis vis et. Vitae audire prodesset an his. Nulla ubique omnesque in sit.</p>-->
																		<p class="p1" i class="fa-phone color-primary">Tel no: </i><?php echo $phone; ?></p>
																		<p class="p1" i class="fa-phone color-primary">Email address: </i><?php echo $adminemail; ?></p>
																		
																	  </br>
																		<h4><strong>Business Address</strong></h4>
																   	</br>
																		<p class="p1">Head Office</p>
																		<p class="p1">Clearwater Office Park</p>
																	  <p class="p1">Strubens Valley</p>
																		<p class="p1">Johannesburg, 1735</p>
															   		</br>
																		<h4><strong>Operating Hours</strong></h4>
																		</br>
																		<p class="p1">Mon-Fri: 09:00am-17:00pm</p>
																		<p class="p1">Saturday: 09:00am-12:00pm</p>
																		<p class="p1">Sunday and Public Holidays: Closed</p>
                                </div>
                            </div>
                            <!-- End recent Posts -->
                            <!-- About -->
                            <!--<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">About</h3>
                                </div>
                                <div class="panel-body">
                                    Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.
                                </div>
                            </div>-->
                            <!-- End About -->
                        </div>
                        <!-- End Side Column -->
                    </div>
                </div>
            <!-- === END CONTENT === -->
			<script>

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");
		  var width = 10;
        elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';*/

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';
    }
  }
}

function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}


 // -- Enter to capture comments   ---
 function validate_save()
 {
	 	 msg  = document.getElementById('msg').value;
	     FirstName	  = document.getElementById('FirstName').value;
	     phone		  = document.getElementById('phone').value;
	     email		  = document.getElementById('email').value;
		 //service = document.getElementById('service').value;
		 valid = true;

		if(msg != '' && FirstName != '' && email != '' && phone != '')
		{

			if(phonenumber(phone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number';
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

			if(ValidateEmail(email) != true)
			{
				document.getElementById("emailError").setAttribute("class", "form-group has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email';
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group");
			 document.getElementById('emailErrorText').innerHTML = '';
			}

			// -- Message
			if(msg != '')
			{
				document.getElementById("msgError").setAttribute("class", "form-group");
				document.getElementById('msgErrorText').innerHTML = '';
		    }

		    // -- Company name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			if(valid)
			{		jQuery(document).ready(function(){
					jQuery.ajax({  type: "POST",
								   url:"jquery_contactus.php",
								   data:{msg:msg,FirstName:FirstName,phone:phone,email:email},
									success: function(data)
									 {
										jQuery("#divContent").html(data);
									 }
								});
							});
				//document.getElementById('divContent').innerHTML = 'Message successfully submited.';
			}

		}
		else
		{
			// -- Message
			if(msg == '')
			{
				document.getElementById("msgError").setAttribute("class", "form-group has-error");
				document.getElementById('msgErrorText').innerHTML = 'Please enter Message';
			}
			else
			{
				document.getElementById("msgError").setAttribute("class", "form-group");
				document.getElementById('msgErrorText').innerHTML = '';
		    }

		    // -- Company name
			if(FirstName == '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('FirstNameErrorText').innerHTML = 'Please enter name';
			}
			else
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			//-- phone
			if(phone == '')
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error");
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter phone';

			}
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

			// -- email
			if(email == '')
			{	document.getElementById("emailError").setAttribute("class", "form-group has-error");
			 	document.getElementById('emailErrorText').innerHTML = 'Please enter email';
			}
			else
			{
				document.getElementById("emailError").setAttribute("class", "form-group");
				document.getElementById('emailErrorText').innerHTML = '';
			}
		}
			  return false;
  }

function phonenumber(inputtxt)
{
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail)
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

</script>
