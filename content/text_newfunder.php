<?php 
require 'database.php';
$tablename = 'funder';
$fundername = '';
$fundernameError = null; 
$fundername = '';

$contactnameError = null;
$contactname = '';

$phoneError = null;
$phone = '';

$emailError = null;
$email = '';
 
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$contactname = $_POST['contactname'];
	$fundername = $_POST['fundername'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];

	$valid = true;
	
	if (empty($contactname)) { $contactnameError = 'Please enter Contact Name.'; $valid = false;}
	
	if (empty($fundername)) { $fundernameError = 'Please enter Funder Name.'; $valid = false;}

	if (empty($email)) { $emailError = 'Please enter email.'; $valid = false;}
	
	// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}

	if (empty($phone)) { $phoneError = 'Please enter phone.'; $valid = false;}	
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO $tablename (fundername,contactname,phone,email) VALUES(?,?,?,?) "; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($fundername,$contactname,$phone,$email));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">  
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Funder Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Funder successfully created.</p>");
		}
?>
				
</div>				
			<p><b>Funder Name</b><br />
			<input style="height:30px" type='text' name='fundername' value ="<?php echo !empty($fundername)?$fundername:'';?>"/>
			<?php if (!empty($fundernameError)): ?>
			<span class="help-inline"><?php echo $fundernameError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Contact Name</b><br />
			<input style="height:30px" type='text' name='contactname' value ="<?php echo !empty($contactname)?$contactname:'';?>"/>
			<?php if (!empty($contactnameError)): ?>
			<span class="help-inline"><?php echo $contactnameError;?></span>
			<?php endif; ?>
			</p> 
			
			<p><b>Contact Phone</b><br />
			<input style="height:30px" type='text' name='phone' value ="<?php echo !empty($phone)?$phone:'';?>"/>
			<?php if (!empty($phoneError)): ?>
			<span class="help-inline"><?php echo $phoneError;?></span>
			<?php endif; ?>
			</p> 
			
			<p><b>Contact Email</b><br />
			<input style="height:30px" type='text' name='email' value ="<?php echo !empty($email)?$email:'';?>"/>
			<?php if (!empty($emailError)): ?>
			<span class="help-inline"><?php echo $emailError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'funder';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	
			
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>