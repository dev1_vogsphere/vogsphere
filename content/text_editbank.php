<?php 
require 'database.php';
$banknameError = null;
$bankname = '';
$count = 0;
$bankid = '';

if (isset($_GET['bankid']) ) 
{ 
$bankid = $_GET['bankid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$bankname = $_POST['bankname'];
	$valid = true;
	
	if (empty($bankname)) { $banknameError = 'Please enter Bank Name.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE bank SET bankname = ? WHERE bankid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($bankname,$bankid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from bank where bankid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($bankid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$bankname = $data['bankname'];
}

?>
<div class="container background-white bottom-border">
	<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
			<div class="panel-heading">
			<h2 class="text-center">
									 Change Bank
			</h2>
			<?php # error messages
					if (isset($message)) 
					{
						foreach ($message as $msg) 
						{
							printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Bank successfully updated.</p>");
					}
			?>
							
			</div>				
			<p><b>Bank</b><br />
			<input style="height:30px" type='text' name='bankname' value="<?php echo !empty($bankname)?$bankname:'';?>"/>
			<?php if (!empty($banknameError)): ?>
			<span class="help-inline"><?php echo $banknameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="bank">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
