<?php 
require 'database.php';
$count = 0;
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 
  $currency = "";
  
  // -- BOC Loan Durations (30.06.2017)
  $fromduration = 1;
  $toduration = 1;
  $fromamount = 0.00;
  $toamount = 0.00;
  $fromdate = '';
  $todate = '';
  $qualifyfrom = 1.00;
  $qualifyto = 1.00;
  $Globalterms = "";
  $phone = "";
  
  // -- EOC Loan Durations (30.06.2017)

  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		if($row['loantypeid'] == 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
			$_SESSION['currency'] = $row['currency'];// -- 10.02.2017
			// -- BOC Loan Durations (30.06.2017)
			$fromduration = $row['fromduration'];
			$toduration = $row['toduration'];
			$fromamount = $row['fromamount'];
			$toamount = $row['toamount'];
			$fromdate = $row['fromdate'];
			$todate = $row['todate'];
			$qualifyfrom = $row['qualifyfrom'];
			$qualifyto = $row['qualifyto'];
			// -- EOC Loan Durations (30.06.2017)
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];
	$currency = $_SESSION['currency'];// -- 10.02.2017
// -- EOC 27.01.2017
// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

	Database::disconnect();	
// --------------------- EOC 2017.04.15 ------------------------- //

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$adminemail = "info@vogsphere.co.za";
$MessageErrorMail = "";

// -- Terms and Conditions. --- //
$agreeError = null;
$agree = null;		
// -- Terms and conditions. -- //

// -- Company Name:
$companyname = "";
if(isset($_SESSION['GlobalCompany']))
{
	$companyname = $_SESSION['GlobalCompany'];
}
else
{
	$companyname = "Vogsphere (PTY) LTD";
}
 
// ------------ Start Send an Email ----------------//
function SendEmail()
{
require_once('class/class.phpmailer.php');

try{
Global $MessageErrorMail;
	
// Admin E-mail
$adminemail = "";

// -- Company Name & Email Address
if(isset($_SESSION['adminemail']))
{
	$adminemail = $_SESSION['adminemail'];
}
else
{
	$adminemail = "info@vogsphere.co.za";
}
$ApplicationId = '';

if(isset($_SESSION['ApplicationId']))
{
	$ApplicationId = $_SESSION['ApplicationId'];
}

// -- Company Name:
$companyname = "";
if(isset($_SESSION['companyname']))
{
	$companyname = $_SESSION['companyname'];
}
else
{
	$companyname = "Vogsphere (PTY) LTD";
}

$AdminFirstName 		= 'E-loan';
$AdminLastName 		= 'Admin';

// -- Email Server Hosting -- //
	$Globalmailhost = $_SESSION['Globalmailhost'];
	$Globalport = $_SESSION['Globalport'];
	$Globalmailusername = $_SESSION['Globalmailusername'];
	$Globalmailpassword = $_SESSION['Globalmailpassword'];
	$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
	$Globaldev = $_SESSION['Globaldev'];
	$Globalprod = $_SESSION['Globalprod'];		
    // -- Email Server Hosting -- //
	
/*	Production - Live System */
	if($Globalprod == 'p')
	{
		$mail=new PHPMailer();
		$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
		$mail->SetFrom($adminemail,$companyname);
		$mail->AddReplyTo($adminemail,$companyname);
	}
/*	Development - Testing System */	
	else
	{
			$mail=new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug=1;
			$mail->SMTPAuth=true;
			$mail->SMTPSecure=$GlobalSMTPSecure;
			$mail->Host=$Globalmailhost;
			$mail->Port= $Globalport;
			$mail->Username=$Globalmailusername;
			$mail->Password=$Globalmailpassword;
	}	
			$mail->SetFrom($adminemail,$companyname);
			$mail->Subject=  $companyname." loan request ".$ApplicationId." received confirmation";

			ob_start();
			include 'text_emailWelcomeCustomer.php';//'text_emailWelcomeCustomer.php'; //execute the file as php
			$body = ob_get_clean();			
						
			$fullname = $FirstName.' '.$LastName;

			// print $body;
			$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
			$mail->AddAddress($email, $fullname);
			$mail->AddBCC($adminemail,$companyname); // BCC The Admin

			if($mail->Send()) 
			{
			  $MessageErrorMail = "<p class='status'>Your ".$companyname." Loan application is successfully registered. An e-mail is sent your Inbox!</p>";
			}
			else 
			{
			  // echo 'mail not sent';
			  $MessageErrorMail = "<p class='status'>Your ".$companyname." Loan application was successfully registered. but your e-mail address seems to be invalid!</p>";
			}			
	}
	catch(phpmailerException $e)
	{
		echo $e->errorMessage();
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}		
}
// ------------ End Send an Email ----------------//

// -- Generate Captcha Image
function Image($value)
{
 $generateImage = '';
 if (!empty($_POST)) 
 {
 if (isset($_POST['Apply']))
   {$generateImage = 'No';}
 }
 
 if( $generateImage == '')
 {
$string = '';
for ($i = 0; $i < 5; $i++) {
    $string .= chr(rand(97, 122));
}

$_SESSION['captcha'] = $string; //store the captcha
$_SESSION['value'] = $value;
$dir = 'fonts/';
$image = imagecreatetruecolor(165, 50); //custom image size
$font = "PlAGuEdEaTH.ttf"; // custom font style
$color = imagecolorallocate($image, 113, 193, 217); // custom color
$white = imagecolorallocate($image, 255, 255, 255); // custom background color
imagefilledrectangle($image,0,0,399,99,$white);
imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

// Enable output buffering
ob_start();
imagepng($image);
// Capture the output
$imagedata = ob_get_contents();
// Clear the output buffer
ob_end_clean();
$_SESSION['image'] = $imagedata;
}
else
{
$imagedata = $_SESSION['image'];
}

return $imagedata;
}
// -- End - Generate Captcha Image

$password1 = '';
$idnumber  = '';
$tbl_user="user"; // Table name 
$tbl_customer ="customer"; // Table name
$tbl_loanapp ="loanapp"; // Table name
$db_eloan = "ecashpdq_eloan";
$captcha = '';
$modise = '';
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';

if(isset($_SESSION))
{
		$Globalterms = $_SESSION['Globalterms'];
}
// EOC -- 15.04.2017 -- Updated.
  
//print '<p><img src="data:image/png;base64,'.base64_encode(Image('test')).'" alt="image 1" width="96" height="48"/></p>';
//echo "<prev>".print_r($_SESSION,true)."</prev>";

 if (!empty($_POST)) 
	{
	
	// Captcha declaration
	$modise = $_SESSION['captcha'];
	$captchaError = null;
	// --Captcha
	$captcha = $_POST['captcha'];
	
	 //echo "<h1>Post - $modise - $modise2 </h1>";
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$DobError = null;
		$SuretyError = null;
		$firstpaymentError = null;
		$password1Error = null;
		$password2Error = null;
	
				
		// keep track validation errors - 		// Loan Details
		$MonthlyExpenditureError = null; 
		$ReqLoadValueError = null; 
		$DateAccptError = null; 
		$LoanDurationError = null; 
		$StaffIdError = null; 
		$InterestRateError = null; 
		$ExecApprovalError = null; 
		$MonthlyIncomeError = null; 

		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //

		
		// keep track post values
		// Customer Details
		$Title = $_POST['Title'];
		$idnumber = $_POST['idnumber'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		$Dob		= $_POST['Dob'];
		$Surety		= $_POST['Surety'];
		$firstpayment = $_POST['FirstPaymentDate'];
		$loantypeid = $_POST['loantype'];// -- 2017.06.20 - Modise
		$StaffId = 1;
		
		// -- Terms and Conditions.
		$agree = "";
		if(!empty($_POST['agree'])) 
        {
          		$agree = $_POST['agree'];
        }
		if (empty($agree)) { $agreeError = 'You must agree with the terms and conditions'; $valid = false;}
		// -- Terms and conditions.
		
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
		//{
		 // $password1 = $_POST['password1'];
		//}
		$monthlypayment = $_POST['monthlypayment'];

		// Loan Details
		$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
		$ReqLoadValue = $_POST['ReqLoadValue'];
		$DateAccpt  = date('Y-m-d');
		$LoanDuration  = $_POST['LoanDuration'];
		$InterestRate = $_POST['InterestRate'];
		$ExecApproval  = "PEN";
		$MonthlyIncome  = $_POST['MonthlyIncome'];
		
		$Repayment = $_POST['Repayment'];
		$paymentmethod = $_POST['paymentmethod'];
		$FirstPaymentDate = $_POST['FirstPaymentDate'];
		$lastPaymentDate = $_POST['lastPaymentDate'];
		$loantype = $_POST['loantype'];
		$paymentfrequency = $_POST['paymentfrequency'];

		$TotalAssets = 0;
		$LoanValue = 0;
		
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);

		// ------------------------- $_POST - Banking Details ------------------------------------- //
		$AccountHolder = $_POST['AccountHolder'];
		$AccountType = $_POST['AccountType'];
		$AccountNumber = $_POST['AccountNumber'];
		$BranchCode = $_POST['BranchCode'];
		$BankName = $_POST['BankName'];
		// -------------------------------- Banking Details ------------------------------------------ //
		
		// validate input - 		// Customer Details
		$valid = true;
		// ----------------------------------- BOC - 01.07.2017 - From Date and End Date ------------- //
		/*$toduration = $row['toduration'];
		$fromamount = $row['fromamount']
		$toamount = $row['toamount']
		$fromdate = $row['fromdate'];*/
		$fromamount = $_POST['fromamount'];
		$toamount = $_POST['toamount'];
		$qualifyfrom = $_POST['qualifyfrom'];
		$qualifyto = $_POST['qualifyto'];
		// ----------------------------------- EOC - 01.07.2017 - From Date and End Date ------------- //
	
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($idnumber,2,2) > 12) 
		{ 
			$idnumberError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{
		// Day from 1 - 30
			if (in_array(substr($idnumber,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($idnumber,4,2) > 30)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($idnumber,2,2),$months31)) 

			{
				// Day 
			   if (substr($idnumber,4,2) > 31)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($idnumber,2,2),$monthsFeb)) 
			{
				// Leap Year
				$yearValidate = parseInt(substr(0,2));
				$leapyear = ($yearValidate % 4 == 0);
				
				if($leapyear == true)
				{
					// Day 
			   if (substr($idnumber,4,2) > 29)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($idnumber,4,2) > 28)
					   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		// Phone number must be characters
		if (!is_Numeric($phone)) { $phoneError = 'Please enter valid phone numbers - digits only'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}
		
		// is Number
		if (!is_Numeric($PostCode)) { $PostCodeError = 'Please enter valid Postal Code - numbers only'; $valid = false;}

		// Is 4 numbers
		if (strlen($PostCode) > 4) { $PostCodeError = 'Please enter valid Postal Code - 4 numbers'; $valid = false;}
		
		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter/select Date of birth'; $valid = false;}
		
		// -- BOC 11.09.2017. if User Age is greater than 18.
		$Age18 = Age18($Dob);
		if(empty($Age18)){$valid= true;}else{$DobError = Age18($Dob); $valid = false;}
		// -- EOC 11.09.2017.
		
		if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($firstpayment)) { $firstpaymentError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'.is_float($MonthlyExpenditure); $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter Required Loan Value'; $valid = false;}
		
		// ------------------------------ BOC 01.07.2017 ------------------------------ //
		// -- Determine the loan amount, one may request via the 
		// -- loan type: amount from to amount to.
		if($valid)
		{
			if (($ReqLoadValue >= $fromamount) && ($ReqLoadValue <= $toamount))
			{
				// -- "is between"
			}
			else
			{
				$ReqLoadValueError = "You have exceed the maximum borrowing amount of $currency $toamount"; 
				$valid = false; 
			}
		
			if(!empty($firstpayment))
			{// -- FirstPaymentDate cannot be in past.
				$date1=date_create(date('Y-m-d'));// Today's date. 
				$date2=date_create($firstpayment);// FirstPaymentDate

				$diff=date_diff($date1,$date2);
				$value = $diff->format("%R%a");
				
			// -- if its negative means first payment date is in the past.
				if($value < 0)
				{
					 $firstpaymentError = 'Please enter/select first payment date in future/present.'; 
					 $valid = false;
				}
				else
				{
		  // -- if its postive means first payment date is in the future/present.
				}
			}
		}
		// ------------------------------ EOC 01.07.2017 ------------------------------ //
		
		//if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		if (empty($DateAccpt)) { $DateAccptError = 'Please enter Date Accpt'; $valid = false;}
				
		// Loan duration is numeric.
		if (!is_Numeric($LoanDuration)) { $LoanDurationError = 'Please enter a number for Loan Duration'; $valid = false;}

		if (empty($LoanDuration)) { $LoanDurationError = 'Please enter Loan Duration'; $valid = false;}

		if (empty($InterestRate)) { $InterestRateError = 'Please enter Interest Rate'; $valid = false;}
		
		// Captcha Error Validation.
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}

		// -- Validate Password Only if not loggedin
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
			 // nothing
			}
			else
			{
			
			$password1 = $_POST['password1'];
			//$password2 = $_POST['password2'];
			
			if (empty($password1)) { $password1Error = 'Please enter Password'; $valid = false;}
			//if (empty($password2)) { $password2Error = 'Please confirm password'; $valid = false;}
			
			//if($password1 != $password2) {$password1Error = 'Password does not match'; $valid = false; }

			}
			
		// 	refresh Image
			if (!isset($_POST['Apply'])) // If refresh button is pressed just to generate Image. 
			{
				$valid = false;
			}
	
		// Insert e-Loan Application Data.
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// ---------------- User ---------------
			//User - UserID & E-mail Duplicate Validation
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));

			if ($q->rowCount() >= 1)
			{$idnumberError = 'ID number already exists.'; $valid = false;}
			
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
			
			if ($valid) 
			{
			 $count = 0;
				// -- Validate Password Only if not loggedin
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
				//  -- BOC encrypt password - 16.09.2017.	
				$password1 = Database::encryptPassword($password1);
				//  -- EOC encrypt password - 16.09.2017.	

				$sql = "INSERT INTO $tbl_user(userid,password,email,role) VALUES (?,?,?,'Customer')";
				$q = $pdo->prepare($sql);
				$q->execute(array($idnumber,$password1,$email));
			}

			// Customer - UserID & E-mail Duplicate Validation
			
			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,Street,Suburb,City,State,PostCode,Dob,phone)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone));

			// Insert Application Loan	
			$sql = "INSERT INTO $tbl_loanapp(CustomerId,loantypeid,MonthlyIncome,MonthlyExpenditure,TotalAssets,ReqLoadValue,ExecApproval,";
			$sql = $sql."DateAccpt,StaffId,InterestRate,LoanDuration,LoanValue,surety,Repayment,paymentmethod,FirstPaymentDate,lastPaymentDate,monthlypayment,paymentfrequency,accountholdername,bankname,accountnumber,branchcode,accounttype)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //24
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$loantypeid,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,$ReqLoadValue,$ExecApproval,$DateAccpt,$StaffId,$InterestRate,$LoanDuration,$LoanValue,$Surety,$Repayment,$paymentmethod,$FirstPaymentDate,$lastPaymentDate,$monthlypayment,$paymentfrequency,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType));//24
			/* 
			 Added Banking Details
			 $AccountHolder = $_POST['AccountHolder'];
			$AccountType = $_POST['AccountType'];
			$AccountNumber = $_POST['AccountNumber'];
			$BranchCode = $_POST['BranchCode'];
			$BankName = $_POST['BankName'];
				 
			*/
			$count = $count + 1;
			Database::disconnect();
		// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = $password1;
												
			// Id number
			$_SESSION['idnumber']  = $idnumber;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;		
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			$_SESSION['Street'] = $Street;
			// Suburb
			$_SESSION['Suburb'] = $Suburb;
			// City
			$_SESSION['City'] = $City;
			// State
			$_SESSION['State'] = $State;
			// PostCode
			$_SESSION['PostCode'] = $PostCode;
			// Dob
			$_SESSION['Dob'] = $Dob;
			// phone
			$_SESSION['phone'] = $phone;	
			//------------------------------------------------------------------- 
			//           				LOAN APP DATA							-
			//-------------------------------------------------------------------			
			$_SESSION['loandate'] = $DateAccpt;// "01-08-2016";
			$_SESSION['loanamount'] = $ReqLoadValue ;// "R2000";
			$_SESSION['loanpaymentamount'] = $Repayment  ; //"R2400";
			$_SESSION['loanpaymentInterest'] = $InterestRate ;// = "20%";
			$_SESSION['FirstPaymentDate'] = $FirstPaymentDate;// = ;//'01-08-2016';
			$_SESSION['LastPaymentDate'] = $lastPaymentDate;// = '01-08-2016';
			$_SESSION['applicationdate'] = $DateAccpt;// = '01-08-2016';
			$_SESSION['status'] = $ExecApproval;// = '01-08-2016';
			$_SESSION['monthlypayment'] = $monthlypayment;// Monthly Payment Amount 
			$_SESSION['LoanDuration'] = $LoanDuration;// Loan Duration
 
			// ------------------------- $_SESSION - Banking Details ------------------------------------- //
			$_SESSION['AccountHolder'] = $AccountHolder;
			$_SESSION['AccountType'] = $AccountType;
			$_SESSION['AccountNumber'] = $AccountNumber ;
			$_SESSION['BranchCode'] = $BranchCode;
			$_SESSION['BankName'] = $BankName;
		   // ------------------------------- $_SESSION Banking Details ------------------------------------------ //
			// -- BOC T.M Modise, 21.06.2017 - Adding Charges.	
			$ApplicationID = GetRecentApplicationID($idnumber);
			
			$_SESSION['ApplicationId'] = $ApplicationID;

		// --------------------------- End Session Declarations ---------------------//
		
		// --------------------------- SendEmail to the clients to inform them about the e-loan application -----//
			SendEmail();	
		  }
		  else
		  {
			  $count = -1;
		  } 
		}
		else
		{
			  $count = -1;
		}
} // END of isEmpty('Post')
else
{
// -- Default Bankname - ABSA
  $BankName = "ABSA";
}

// -- BOC Validate if Customer is 18+, 11.09.2017.
function Age18($imp_bday)
{
	$today = new DateTime(date("Y-m-d"));
	$bday = new DateTime($imp_bday);
	$interval = $today->diff($bday);
	if(intval($interval->y) > 18)
	{
		return "";
	}
	else
	{
		return "You must be 18 or older";
	}
}

// -- Get Recent Created ApplicationID.
function GetRecentApplicationID($idnumber)
{
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
  $ApplicationID = '';
  $Today = date('Y-m-d');
  $count = 1;
  
  // -- Get the Application ID.
  $sql = 'SELECT * FROM loanapp WHERE CustomerId = ? AND DateAccpt = ? ORDER BY ApplicationId DESC';
  $q = $pdo->prepare($sql);
  $q->execute(array($idnumber,$Today));	
  $data = $q->fetchAll();

  // -- Get the first ApplicationID. 
  if(!empty($data))
  {
     foreach ($data as $row) 
	 {
	   if($count == 1)
	   {
	     $ApplicationID = $row['ApplicationId'];
	   }	   
	   $count = $count + 1;
	 }
  }
  return $ApplicationID;
}

// -- EOC Validate if Customer is 18+, 11.09.2017.

?>
<!-- <div > class="container background-white"> -->
<div class="container background-white bottom-border"> <!-- class="container background-white"> -->
                    <div class="margin-vert-30">
                        <!-- Register Box id="captcha-form" name="captcha-form" -->
                        <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                              <form METHOD="post"  id="captcha-form" name="captcha-form" class="signup-page"  action=<?php echo "".$_SERVER['REQUEST_URI']."";?>> 
                                <div class="signup-header">
                                    <h2 class="text-center"><?php if(empty($companyname)){echo 'e-Loan';}else{echo $companyname;}?> Loan Application</h2>
									<p>Please complete all fields NOTE: <span style="color:red">*</span> = Required Field</p>
                                    <p>Already have an account? Click
                                        <a href="customerLogin"><span style="color:red">HERE</span></a> to login to your account.</p>
										<?php # error messages
									if (isset($message)) 	
									{
										foreach ($message as $msg) {
											printf("<p class='status'>%s</p></ br>\n", $msg);
										}
									}
									# success message
									if($count !=0)
									{
										# error message
										if($count == -1)
										{
											printf("<div class=alert alert-warning'>
												<strong>Warning!</strong>
												<p class='alert alert-warning'>Loan Application Error Below!</p></div>\n", $count);
										}else
									   # success message
										{printf("<p class='status'>Loan Application captured successfully!</p>\n".$MessageErrorMail, $count);}
									}									   
									?>
                                </div>
					<!-- BOC - Personal Information Details -->		
						<!-- Start - Accordion - Alternative -->
						
				<div class="tab-pane fade in" id="sample-3b">							
								<div class="panel <?php
if(!empty($idnumberError)){ echo 'panel-danger';}else if(!empty($FirstNameError)){ echo 'panel-danger';} 
else if(!empty($LastNameError)){ echo 'panel-danger';} else if(!empty($DobError)){ echo 'panel-danger';}
else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayRegister" href="javascript:toggleRegister();">
                                                Personal Information Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleRegister" 
							style="display<?php if(!empty($idnumberError)){ echo ':block';}else if(!empty($FirstNameError)){ echo ':block';} 
else if(!empty($LastNameError)){ echo ':block';} else if(!empty($DobError)){ echo ':block';}else{ echo ':none';}?>">						
					 
                                <label>Title</label>
								<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
								</SELECT>
								<br/>
                                <!-- <input class="form-control margin-bottom-20" type="text"> -->

								<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <label >ID/Passport Number
											<span style="color:red">*</span>									
									 </label> 
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								
								<!-- End ID number -->
								<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
									<label>First Name<span style="color:red">*</span></label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										<?php if (!empty($FirstNameError)): ?>
										<span class="help-inline"><?php echo $FirstNameError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Last Name -->
								
								<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
									 <label>Last Name
										 <span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
									<?php if (!empty($LastNameError)): ?>
										<span class="help-inline"><?php echo $LastNameError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
								<label>Date of birth<span style="color:red">*</span></label>
									<div class="controls">
									<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div> 
					</div>	
						<!-- EOC Personal Information Details -->			
						<!-- BOC Contact Details -->
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($phoneError)){ echo 'panel-danger';}else if(!empty($emailError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayContacts" href="javascript:toggleContacts();">
                                                Contact Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleContacts" style="display: <?php if(!empty($phoneError)){ echo 'block';}
																		  else if(!empty($emailError)){ echo 'block';}
																		  else{ echo 'none';}?>">						
								<!-- Contact number -->
								<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">E mail<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>	
						</div>	
					</div>	
						<!-- EOC Contact Details -->
						<!-- BOC Residential Address -->
					 <div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($StreetError)){ echo 'panel-danger';}else if(!empty($SuburbError)){ echo 'panel-danger';}
												else if(!empty($CityError)){ echo 'panel-danger';}else if(!empty($PostCodeError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayAddress" href="javascript:toggleAddress();">
                                                Residential Address Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleAddress" style="display:<?php if(!empty($StreetError)){ echo 'block';}else if(!empty($SuburbError)){ echo 'block';}
												else if(!empty($CityError)){ echo 'block';}else if(!empty($PostCodeError)){ echo 'block';}
												else{ echo 'none';}?>">						
								<!-- Street -->
								<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
									<label>Street<span style="color:red">*</span></label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<span class="help-inline"><?php echo $StreetError;?></span>
										<?php endif; ?>
									</div>
								</div>
														
								<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
										<label>Suburb<span style="color:red">*</span></label>
										<div class="controls">
							<input style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<span class="help-inline"><?php echo $SuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								
								<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
									<label>City/Town/Township<span style="color:red">*</span></label>   
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<span class="help-inline"><?php echo $CityError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<label>Province</label>
                                <SELECT class="form-control" id="State" name="State" size="1">
									<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->
								</SELECT>
								<br/>
								<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
									<label class="control-label">Postal Code<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
										<?php if (!empty($PostCodeError)): ?>
										<span class="help-inline"><?php echo $PostCodeError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>	
						<!-- EOC Residential Address -->									
							
<!------------------------------------------------------------ Banking Details --------------------------------------------------------->
						<!-- BOC Accordion Gompieno -->	
							<div class="tab-pane fade in" id="sample-3b">
							  <div class="panel <?php	if(!empty($AccountHolderError)){ echo 'panel-danger';}
														else if(!empty($BankNameError)){ echo 'panel-danger';}
														else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayBank" href="javascript:toggleBanking();">
                                                Banking Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleBank" style="display: <?php	if(!empty($AccountHolderError)){ echo 'block';}
														else if(!empty($BankNameError)){ echo 'block';}
														else{ echo 'none';}?>">
							<!-- Account Holder Name -->
							<label>Account Holder Name<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Bank Name -->
							<label>Bank Name</label>
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										  <SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
							<!-- Account Number  -->
							<label>Account Number<span style="color:red">*</span>
							</label>
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Account Type  -->
							<label>Account Type
							</label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1" >
								<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Account type --->
								</SELECT>

									</div>	
								</div>
								
							<!-- Branch Code  -->
							<label class="control-label">Branch Code
							</label>
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
									<div class="controls">
									 <SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
									</div>
								</div>
							</div>
							<!-- EOC Accordion Gompieno -->
							</div>
							<!-- EOC Accordion Gompieno -->
						</div>
					</div>
		
<!------------------------------------------------------------ End Banking Details --------------------------------------------------------->									
							<!-- Heading 2 -->
                              <div class="tab-pane fade in" id="sample-3b">						
								<div class="panel <?php if(!empty($MonthlyIncomeError)){ echo 'panel-danger';}
												else if(!empty($MonthlyExpenditureError)){ echo 'panel-danger';}else if(!empty($ReqLoadValueError)){ echo 'panel-danger';}
												else if(!empty($SuretyError)){ echo 'panel-danger';}else if(!empty($firstpaymentError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayLoan" href="javascript:toggleLoan();">
                                                Loan Application Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleLoan" style="display: <?php if(!empty($MonthlyIncomeError)){ echo 'block';}
												else if(!empty($MonthlyExpenditureError)){ echo 'block';}else if(!empty($ReqLoadValueError)){ echo 'block';}
												else if(!empty($SuretyError)){ echo 'block';}else if(!empty($firstpaymentError)){ echo 'block';}
												else{ echo 'none';}?>">
								<label>Loan Type</label>
                                <SELECT class="form-control" id="loantype" name="loantype" size="1" onChange="valueInterest(this);">
									<!-------------------- BOC 2017.04.15 - dataLoanType --->	
											<?php
											$loantypeid = '';
											$loantypeSelect = $loantype;
											$loantypedesc = '';
											$loantypevalue = 0;
											$currencyIn = '';
											foreach($dataLoanType as $row)
											{
											  // -- Loan Type of a selected Bank
												$loantypeid = $row['loantypeid'];
												$loantypedesc = $row['loantypedesc'];						
												$loantypevalue = $row['percentage'];
												// -- 30.06.2017
												// -- BOC Loan Durations (30.06.2017)
												  $fromduration = $row['fromduration'];
												  $toduration = $row['toduration'];
												  $fromdate = $row['fromdate'];
												  $todate = $row['todate'];
												  $fromamount = $row['fromamount'];
												  $toamount = $row['toamount'];
												  $qualifyfrom = $row['qualifyfrom'];
												  $qualifyto = $row['qualifyto'];
												  $currencyIn = $row['currency'];
												  
												  // -- Loan - Todate cannot be in past.
													$date1=date_create(date('Y-m-d'));// -- Today's date. 
													$date2=date_create($todate);// -- LoanToDate

													$diff=date_diff($date1,$date2);
													$value = $diff->format("%R%a");
													
												// -- if its negative means Loan To date is in the past.
													if($value < 0)
													{
														// -- Loan Type has expied.
														// -- DO NO SHOW IT ON THE Application.
													}
													else
													{
													  // -- if its postive means Loan Type To date is in the future/present.
													  // -- SHOW THE LOAN TYPE		
														// -- EOC Loan Durations (30.06.2017)
														// -- 30.06.2017
														// -- Select the Selected Bank 
													  if($loantypeid == $loantype)
													   {
															echo "<OPTION data-vogsInt=$loantypevalue 
															data-qualifyfrom=$qualifyfrom data-qualifyto=$qualifyto 
															data-fromamount=$fromamount data-toamount=$toamount 
															data-fromdate=$fromdate data-todate=$todate 
															data-from=$fromduration data-to=$toduration value=$loantypeid selected>$loantypedesc - Min.:$currencyIn$fromamount Max.:$currencyIn$toamount;Valid From:$fromdate To:$todate </OPTION>";
													   }
													  else
													   {
															echo "<OPTION data-vogsInt=$loantypevalue 
															data-qualifyfrom=$qualifyfrom data-qualifyto=$qualifyto 
															data-fromamount=$fromamount data-toamount=$toamount 
															data-fromdate=$fromdate data-todate=$todate
															data-from=$fromduration data-to=$toduration value=$loantypeid>$loantypedesc - 
															Min.:$currencyIn$fromamount Max.:$currencyIn$toamount;Valid From:$fromdate To:$todate </OPTION>";
													   }
													}
											}
											
								
											if(empty($dataLoanType))
											{
												echo "<OPTION value=0>No Loan Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - dataLoanType --->
								</SELECT>
								
								<!------------------------------ Start Date & End Date - 01.07.2017 ---->
                                <input style="height:30px;display:none" readonly value="<?php echo $fromamount;?>" id="fromamount" 
								name="fromamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $toamount;?>" id="toamount" 
								name="toamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $fromdate;?>" id="fromdate" 
								name="fromdate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $todate;?>" id="todate" 
								name="todate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $qualifyfrom;?>" id="qualifyfrom" 
								name="qualifyfrom" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $qualifyto;?>" id="qualifyto" 
								name="qualifyto" class="form-control margin-bottom-10" type="text" />
								
								<!------------------------------ EOC Start Date and End - 01.07.2017 ---->
								<br>
								<!-- update 27.01.2017 personal loan interest rate, Id="InterestRate" -->
								<div class = "input-group">
								<label>Interest rate (%)
                                </label>
								<!-- update 27.01.2017 personal loan interest rate, Id="InterestRate" -->
                                <input style="height:30px" readonly value="<?php echo $personalloan;?>" id="InterestRate" 
								name="InterestRate" class="form-control margin-bottom-10" type="text" />
								</div> 
								<br>
								<label>Monthly Income (e.g. 20000.00)<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
									<div class = "input-group">
										<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyIncome" name="MonthlyIncome" type="text"  placeholder="Monthly Income(eg 20000.00)" value="<?php echo !empty($MonthlyIncome)?$MonthlyIncome:0.00;?>" onchange="CalculateRepayment()">
									</div>
									<?php if (!empty($MonthlyIncomeError)): ?>
										<span class="help-inline"><?php echo $MonthlyIncomeError;?></span>
										<?php endif; ?>
								</div>

								<br>

								<label class="control-label">Monthly Expenditure(e.g. 20000.00)<span style="color:red">*</span>
									
								</label>

								<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
									<div class = "input-group">
										<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyExpenditure"  name="MonthlyExpenditure" type="text"  placeholder="Monthly Expenditure(e.g. 20000.00)" value="<?php echo !empty($MonthlyExpenditure)?$MonthlyExpenditure:0.00;?>" onchange="CalculateRepayment()"/>
										
									</div>
									<?php if (!empty($MonthlyExpenditureError)): ?>
									<span class="help-inline"><?php echo $MonthlyExpenditureError;?></span>
									<?php endif; ?>
								</div>
								
								<br>
								
								<label class="control-label">Required Loan Value(e.g. 20000.00)<span style="color:red">*</span>
								</label>
								<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
									<div class = "input-group">
									<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="ReqLoadValue" name="ReqLoadValue" type="text"  placeholder="Required Loan Value(e.g. 20000.00)" value="<?php echo !empty($ReqLoadValue)?$ReqLoadValue:0.00;?>" onchange="CalculateRepayment()">
										
									</div>
									<?php if (!empty($ReqLoadValueError)): ?>
										<span class="help-inline"><?php echo $ReqLoadValueError;?></span>
								<?php endif; ?>
								</div>
									
								<!--<div class = "input-group">
								  <span class = "input-group-addon">R</span>				
                                <input placeholder="Monthly Disposable Income (e.g. 20000.00)" name="MonthlyDispIncome" class="form-control margin-bottom-10" type="text">
								</div> -->

								<!-- <label>Liabilities(eg 20000.00)
                                    <span class="color-red">*</span>
                                </label>
                                <input name="Liabilities" class="form-control margin-bottom-10" type="text">
								-->
								<br>
								<div class="control-group <?php echo !empty($SuretyError)?'error':'';?>">
									<label>Surety Assets(e.g. Golf GTi , iPhone 5)<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="Surety" name="Surety" type="text"  placeholder="Surety Assets(e.g. Golf GTi , iPhone 5)" value="<?php echo !empty($Surety)?$Surety:'';?>">
										<?php if (!empty($SuretyError)): ?>
										<span class="help-inline"><?php echo $SuretyError;?></span>
										<?php endif; ?>
									</div>
								</div>
															
								<!--<label>Liquidity (eg 20000.00)
                                    <span class="color-red">*</span>
                                </label>
                                <input name="Liquidity" class="form-control margin-bottom-10" type="text">
								-->
								<!-- Start Loan Duration -->
								<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
									<label class="control-label">Loan Duration (Please select a number of months - e.g. 2)<span style="color:red">*</span>									</label>
									<!--<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="LoanDuration" id="LoanDuration"  type="text"  placeholder="Loan Duration" value="" onchange="LastPaymentDate()">
										<span class="help-inline"></span>
									</div> -->
									<div class="controls">
									 <SELECT class="form-control" id="LoanDuration" name="LoanDuration" size="1" onchange="CalculateRepayment()">
									   <?php 
											$LoanDurationSelect = $LoanDuration;
											// -- Define Loan Durations.
											for($i=$fromduration;$i<$toduration;$i++)
											{	 
											 if($LoanDurationSelect == $i)
												{
												  echo "<OPTION value=$i selected>$i</OPTION>";
												}
												else
												{
												  echo "<OPTION value=$i>$i</OPTION>";
												}
											}
									   ?>
									 </SELECT>
									</div>
								</div>
								
								<!-- End Loan Duration -->
								
								 <label>Total Repayment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  style="height:30px" readonly id="Repayment" name="Repayment" value="<?php echo !empty($Repayment)?$Repayment:0.00;?>" class="form-control margin-bottom-10" type="text" onchange="CalculateRepayment()">
							    </div>
								<br>
								
								<label>Monthly Payment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  style="height:30px" readonly id="monthlypayment" name="monthlypayment" value="<?php echo !empty($monthlypayment)?$monthlypayment:0.00;?>" class="form-control margin-bottom-10" type="text" onchange="CalculateRepayment()">
							    </div>
								<br>
								
								<label>Frequency of payment</label>
                                <SELECT class="form-control" id="paymentfrequency" name="paymentfrequency" size="1">
									<!-------------------- BOC 2017.04.15 -  Payment Frequency --->	
											<?php
											$paymentfrequencyid = '';
											$paymentfrequencySelect = $paymentfrequency;
											$paymentfrequencydesc = '';
											foreach($dataPaymentFrequency as $row)
											{
												$paymentfrequencyid = $row['paymentfrequencyid'];
												$paymentfrequencydesc = $row['paymentfrequencydesc'];
												// Select the Selected Role 
												if($paymentfrequencyid == $paymentfrequencySelect)
												{
												echo "<OPTION value=$paymentfrequencyid selected>$paymentfrequencydesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentfrequencyid>$paymentfrequencydesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentFrequency))
											{
												echo "<OPTION value=0>No Payment Frequency</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Frequency --->
								</SELECT>
									<br>
								<label>Payment method</label>
                                <SELECT class="form-control" id="paymentmethod" name="paymentmethod" size="1">
								<!-------------------- BOC 2017.04.15 -  Payment Method --->	
											<?php
											$paymentmethodid = '';
											$paymentmethodSelect = $paymentmethod;
											$paymentmethoddesc = '';
											foreach($dataPaymentMethod as $row)
											{
												$paymentmethodid = $row['paymentmethodid'];
												$paymentmethoddesc = $row['paymentmethoddesc'];
												// Select the Selected Role 
												if($paymentmethodid == $paymentmethodSelect)
												{
												echo "<OPTION value=$paymentmethodid selected>$paymentmethoddesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentmethodid>$paymentmethoddesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentMethod))
											{
												echo "<OPTION value=0>No Payment Method</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Method. --->
								</SELECT>
								<br>
								
							   <div class="control-group <?php echo !empty($firstpaymentError)?'error':'';?>">
								<label>First Payment Date<span style="color:red">*</span>
								</label>
									<div class="controls">
									<input  style="height:30px" id="FirstPaymentDate" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="FirstPaymentDate" placeholder="yyyy-mm-dd" type="Date"  value="<?php echo !empty($firstpayment)?$firstpayment:'';?>" onchange="LastPaymentDate()">
									<?php if (!empty($firstpaymentError)): ?>
									<span class="help-inline"><?php echo $firstpaymentError;?></span>
									<?php endif; ?>
									</div>
								</div>
							   
							   
							   <label>Last Payment Date 
							   </label>
                               <input  style="height:30px" readonly id="lastPaymentDate" value="<?php echo !empty($lastPaymentDate)?$lastPaymentDate:'';?>" name="lastPaymentDate" placeholder="yyyy-mm-dd" class="form-control margin-bottom-10" type="date" />
					</div></div> <!-- Acoordion Modise Today -->
								<!-- End - Accordion - Alternative -->
								
								<!-- Start - Captcha -->
								<?php 
								$classname = "control-group error";
								$html = "";

								if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
                                       // echo "<a href='applicationForm.php' class='fa-gears'>Apply for e-loan</a>";
									}
									else
									{									
								echo "<div class='panel-heading'>
											<h2 class='panel-title'>
                                            <a class='accordion-toggle' href='#collapse-Two' data-parent='#accordion' data-toggle='collapse'>
												Register a new account
                                            </a>
                                            </h2>
								      </div>

									  <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input  style='height:30px' readonly id='Userid' value='$idnumber' name='Username' placeholder='ID number' class='form-control' type='text'>
									 </div>
									 
									 <label>Password<span style='color:red'>*</span></label>";
									 if(!empty($password1Error))
									 { 
									 echo "<div class='control-group error'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password'>
									 <span class='help-inline'>$password1Error</span></div>";
									 }
									 else
									 {
									  echo "<div class='control-group'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password' value=$password1></div>";
									 }	
									  // -- New Captcha									 
									 echo "<div class='panel-heading'>
											<h2 class='panel-title'>
                                            <a class='accordion-toggle' href='#collapse-Two' data-parent='#accordion' data-toggle='collapse'>
												Please enter the verification code shown below.                                            </a>
                                            </h2>
								      </div>";
									  
									 echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' /><img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>";
									 if(!empty($captchaError))
									 {
									 echo "<div class='control-group error'>
											<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
											<span class='help-inline'>$captchaError</span></div>";
									 }
									 else
									 {
									 $code = '';
									 echo "<div class='control-group'>
									 <input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
									</div>";
									 }
									 // -- End Captcha

								}
								?>
								<!-- End - Captcha -->
								<!-- Register End here -->
                                <hr>
                                <div class='row'>
                                    <div class='col-lg-9'>
									   <div class="form-group <?php echo !empty($agreeError)?'error':'';?>">
                                        <label class='checkbox'>
                                            <input type='checkbox' name="agree" id="agree" value="agree" <?php if ($agree == 'agree') echo 'checked'; ?>>I read and accept the
                                            <a id="displayText" href="javascript:toggle();">Terms and Conditions</a>
                                        </label>
										<?php if(!empty($agreeError))
												{
													echo "<span class='text-danger'>$agreeError</span>";
												}
										?>
									   </div>	
                                    </div>
                                    <div class='col-lg-4 text-right'>
                                        <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button>
                                    </div>
                                </div>
								<!-- BOC - Testing -->
								<hr>
								<div id="toggleText" style="display: none;border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;"  >
									<h1><b>Terms and Conditions</b></h1>
									   <?php echo "<p>$Globalterms;</p>"; ?>
								</div>	
						</div>
	
								<!-- EOC - Testing -->
                            </form> 
							<?php
							if(!empty($_POST))
							  // if($_SERVER['REQUEST_METHOD']=='POST')
							   {
								  echo '<script type="text/javascript"> LastPaymentDate(); </script>';
							   } 
							?>
                         <!-- </div> Accordion Modise -->
                      </div>
			      </div>
			</div>		  
<?php  
// -- Database Connections  
// -- require 'database.php'; 
if(isset($_POST['Apply']))  
{  
	 $pdo = Database::connectDB(); 
	 $userid = $_POST['idnumber']; 
	 $password = $password1; 
	 $check_user = "SELECT * FROM $tbl_user Inner Join $tbl_customer on $tbl_user.userid = $tbl_customer.CustomerId  WHERE userid='$userid' and password='$password'";
	 
	 mysql_select_db('ecashpdq_eloan',$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	$count = mysql_num_rows($result);
	
	if($count >= 1)
    {  
			$row = mysql_fetch_array($result);
  			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.  
			
			// Title
			$_SESSION['Title'] = $row['Title'];	
			// First name
			$_SESSION['FirstName'] = $row['FirstName'];
			// Last Name
			$_SESSION['LastName'] = $row['LastName'];
			// Phone numbers
			$_SESSION['phone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];
			
			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];
			
			// Dob
			$_SESSION['Dob'] = $row['Dob'];
			
			// phone
			$_SESSION['phone'] = $row['phone'];
			
			$customerLoggedIn = true;
			$_SESSION['customerLoggedIn'] = $customerLoggedIn;		

     echo "<script>window.open('index.php','_self')</script>";  

    }  
    /*else  
    {  
         // echo "<script>alert('Email or password is incorrect!".$userid.$password."')</script>";  
    }*/ 
} 
?>  				 
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

// -- Different Loan Types.
 function valueInterest(sel) 
 {
		var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
		var fromdate = sel.options[sel.selectedIndex].getAttribute('data-fromdate');
		var todate = sel.options[sel.selectedIndex].getAttribute('data-todate');
		var fromduration = sel.options[sel.selectedIndex].getAttribute('data-from');
		var toduration = sel.options[sel.selectedIndex].getAttribute('data-to');
		var fromamount = sel.options[sel.selectedIndex].getAttribute('data-fromamount');
		var toamount = sel.options[sel.selectedIndex].getAttribute('data-toamount');
		var qualifyfrom = sel.options[sel.selectedIndex].getAttribute('data-qualifyfrom');
		var qualifyto = sel.options[sel.selectedIndex].getAttribute('data-qualifyto');
		
//alert(interest);			
		document.getElementById('InterestRate').value = interest;
		document.getElementById('fromdate').value = fromdate;
		document.getElementById('todate').value = todate;
		document.getElementById('fromamount').value = fromamount;
		document.getElementById('toamount').value = toamount;
		document.getElementById('qualifyfrom').value = qualifyfrom;
		document.getElementById('qualifyto').value = qualifyto;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;

 }		
// -- EOC Different Loan Types.

// -- 30.06.2017 -- LoanDuration
 // -- Enter to capture comments   ---
 function defineloandutations(sel) 
 {
		
		var fromduration = document.getElementById('fromduration').value;
		var toduration = document.getElementById('toduration').value;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 30.06.2017 -- LoanDuration

// BOC -- Jquery for terms & conditions
 function toggle() 
 {
	var ele = document.getElementById("toggleText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "Terms and Conditions";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "Terms and Conditions";
	}
}

function toggleBanking() 
 {
	var ele = document.getElementById("toggleBank");
	var text = document.getElementById("displayBank");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';

	}
} 

function toggleLoan() 
 {
	var ele = document.getElementById("toggleLoan");
	var text = document.getElementById("displayLoan");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayLoan" href="javascript:toggleLoan();">	Loan Application Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayLoan" href="javascript:toggleLoan();">	Loan Application Details</a>';

	}
} 

function toggleRegister() 
 {
	var ele = document.getElementById("toggleRegister");
	var text = document.getElementById("displayRegister");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayRegister" href="javascript:toggleRegister();">	Personal Information Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayRegister" href="javascript:toggleRegister();"> Personal Information Details</a>';

	}
} 

function toggleAddress() 
 {
	var ele = document.getElementById("toggleAddress");
	var text = document.getElementById("displayAddress");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';

	}
} 

function toggleContacts() 
 {
	var ele = document.getElementById("toggleContacts");
	var text = document.getElementById("displayContacts");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayContacts" href="javascript:toggleContacts();">	Contact Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayContacts" href="javascript:toggleContacts();"> Contact Details</a>';

	}
} 

// EOC -- Jquery. 
</script> 