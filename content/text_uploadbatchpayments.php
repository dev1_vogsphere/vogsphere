<?php
// -- https://api.jquery.com/jquery.noconflict/
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}

$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
////////////////////////////////////////////////////////////////////////
// -- From Sessions
	$userid = null;
	$role = '';
	$location = 'index';
	$UserCode = null; 
	$IntUserCode = null;

// -- Logged User Details.
if(getsession('username')){$userid = getsession('username');}
// -- User Code.
if(getsession('UserCode')){$UserCode = getsession('UserCode');}
// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');} 

// -- role
if(getsession('role')){$role = getsession('role');} 
////////////////////////////////////////////////////////////////////////
// -- From Single Point Access 
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	
	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
	if(empty($role)){$role = $params[3];}	
}	
	//echo $Client_Id.' - '.$UserCode.' - '.$userid.' - '.$sessionrole; 
////////////////////////////////////////////////////////////////////////	
echo $param;

?>
   <script src="assets/js/2.2.0/jquery.min.js"></script>
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
  <script src="assets/js/3.3.6/bootstrap.min.js"></script>
  <style>
  .box
  {
   max-width:600px;
   width:100%;
   margin: 0 auto;;
  }
  </style>
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>
<div class="container background-white bottom-border">
  <div> <!-- <div class="wrap"> -->
	 <h1>Upload Batch Payments via csv File</h1>
	 <div class="col-md-12">
			    <div class="messages">
				  <p id="message_infoBatchPayments" name="message_infoBatchPayments" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
    </div>
	<form id="uploadbatchpayments" method="post" enctype="multipart/form-data" style="display:block" role="form">
		<p>Download a Template EFT file <a href='Files/Collections/Instructions/Template-Batch Upload EFT.xlsx' target='_blank' style="color:red"><strong>here</strong></a> and Instructions <a href='Files/Collections/Instructions/Documentation_BatchInstructions.docx' target='_blank' style="color:red"><strong>here</strong></a></p>
		<p>Max file size 10Mb, Valid formats csv</p>
    
	<div class="col-md-3">
     <br />
     <label>Select CSV File</label>
    </div>  
		<div class="col-md-4">  
             <input type="file" name="csv_file" id="csv_file" accept=".csv" style="margin-top:15px;" />
         </div> 				
		 <table class ="table table-user-information">
		  <tr>
			<td>
				<div align="center">							
				  <input id="Upload" name="Upload" type="submit" class="btn btn-primary" value="Upload" />
				</div>
			</td>
		   </tr>
		 </table>	
		 
		<input id="IntUserCode" name="IntUserCode" type="text" style="display:none" value="<?php echo $IntUserCode;?>" />
		<input id="UserCode" name="UserCode" style="display:none"  type="text"  value="<?php echo $UserCode;?>" />
		<input id="userid" name="userid" style="display:none" type="text"  value="<?php echo $userid;?>" />
		<input id="role" name="role" style="display:none" type="text"  value="<?php echo $role;?>" />
   </form>
   <br />
   <div id="csv_file_data"></div>
   
</div></div>
<script charset="UTF-8">
jq162 = jQuery.noConflict( true );

jq162(document).ready(function(){
 jq162('#uploadbatchpayments').on('submit', function(event){
	 var fileInput     = document.getElementById('csv_file').value;
	 var message_info  = document.getElementById('message_infoBatchPayments');								

	 message_info.innerHTML = '';
     message_info.className  = '';	
     event.preventDefault();
	//alert(document.getElementById('csv_file').files[0].size);
	var filesize = 0;

  if(validFileExt(fileInput))
  {
	 filesize = document.getElementById('csv_file').files[0].size;
	 
	 if(validFileSize(filesize)){ 
  jq162.ajax({
   url:"script_uploadbatchpayments.php",
   method:"POST",
   data:new FormData(this),
   dataType:'json',
   contentType:false,
   cache:false,
   processData:false,
   success:function(data)
   {
	   obj = jQuery.parseJSON(JSON.stringify(data));
	   //alert(JSON.stringify(data));
	   	var message_info = document.getElementById('message_infoBatchPayments');								
	   if(data !== null)
	   {
		   message_info.innerHTML = 'file added successfully';
		   message_info.className  = 'status';	
		   report_uploadedbatchpayments(data);
	   }
	   else
	   {
		   message_info.innerHTML = 'no file was uploaded';
		   message_info.className  = 'alert alert-error';
	   }
   },
    beforeSend:function() {
    jq162('#Upload').button('loading');
  },
  complete:function() {
    jq162('#Upload').button('reset');
  }

  })
  }
  else
  {
	 message_info.innerHTML = 'file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';	  
  }
  }
  else
  {
	  	message_info.innerHTML = 'Invalid file format please upload .csv';
		message_info.className  = 'alert alert-error';
  }
 });

 /*$(document).on('click', '#import_data', function(){
  var student_name = [];
  var student_phone = [];
  $('.student_name').each(function(){
   student_name.push($(this).text());
  });
  $('.student_phone').each(function(){
   student_phone.push($(this).text());
  });
  $.ajax({
   url:"import.php",
   method:"post",
   data:{student_name:student_name, student_phone:student_phone},
   success:function(data)
   {
    $('#csv_file_data').html('<div class="alert alert-success">Data Imported Successfully</div>');
   }
  })
 });*/
 
});

</script>

<script>
function validFileExt(file) 
{
    var extension = file.substr( (file.lastIndexOf('.') +1) );
	switch(extension) {
        case 'csv':
		   return true;
		default:
           return false;
	}
}
function validFileSize(filesize) 
{
	var max_file_size = 1024*10000; //10MB
	//alert(filesize);
	if (filesize > max_file_size) 
	{
		return false;
	}else{return true;}
}
function report_uploadedbatchpayments(data)
{
	var html = '<div class="row"><div class="table-responsive"><table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
    var totalColumn = 0;
	if(data.column)
    {
		totalColumn  = data.column.length;
     html += '<tr>';
     for(var count = 0; count < data.column.length; count++)
     {
      html += '<th>'+data.column[count]+'</th>';
     }
     html += '</tr>';
    }

    if(data.row_data)
    {
     for(var count = 0; count < data.row_data.length; count++)
     {
	    
		     html += '<tr>';
			 for(var col = 0; col < data.column.length; col++)
			 {
			  html += '<td style="font-size: 10px;width:100%">'+data.row_data[count][col]+'</td>';
			 }
			 html += '</tr>';
		
     }
    }
    html += '</table></div></div>';
    //html += '<div align="center"><button type="button" id="import_data" class="btn btn-success">Import</button></div>';
    $('#csv_file_data').html(html);
    //$('#upload_csv')[0].reset();
}
/*jQuery(document).ready(function(){
 jQuery('#uploadbatchpayments').on('submit', function(event){
	 alert('Here');
  event.preventDefault();
  //call_script_uploadbatchpayments();
 });


});*/
</script>
<!-- </body>
</html> -->