<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// -- Default Database names.
$fieldnamesObj = null;
$databasename = ''; 
$tablename = 'information_schema.schemata';
$dbnameobj = 'ecashpdq_paycollections';
$tbl_name = 'collection';
$queryFields = null;
$queryFields[] = 'schema_name';
$whereArray = null;
$databaseObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;
//print_r($databaseObj);
$query = '';
// -- Default Database Tables.  
$selecteddatabasename = 'information_schema';

$tablename = 'information_schema.tables';
$dbnameobj = 'ecashpdq_paycollections';

$queryFields = null;
$queryFields[] = 'table_name';
$whereArray = null;
$whereArray[] = "table_schema = '{$selecteddatabasename}'";
$tablenames = '';
$tablenamesObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;
 
// -- Default Database Table FIELDS.
$selectedtablename = 'CHARACTER_SETS';
$tablename = 'information_schema.columns';
$queryFields = null;
$queryFields[] = 'column_name';
$whereArray = null;
$whereArray[] = "table_name = '{$selectedtablename}'";
$fields = '';
$fieldnamesObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;
$queryFields = null;
$countHeaders = 0;

if(!empty($_POST))
{
	// -- Runq Query from selection.
	$tablenames = getpost('tablenames');
	$tablename  = $tablenames;
	$databasename = getpost('databasename');
	$fields = getpost('fields');
	$dbnameobj = $databasename;
	$query = getpost('query');
	//echo 'MODISE- '.$dbnameobj.' -END';
	if(empty($query))
	{
		$fields = getpost('fields');
		$queryFields[] = $fields;
		$countHeaders = count($queryFields);
	}
	else
	{
		$queryFields = explode(",", $query);							
		$countHeaders = count($queryFields);
	}
	$whereArray = null;
	//$whereArray[] = "table_name = '{$selectedtablename}'";
}
 
 echo "<div class='container background-white bottom-border'>";		
?>   
<!-- Search Login Box -->
<?php if($_SESSION['role'] == 'admin')
	{?>
	<div class='row margin-vert-30'>
        <!-- Login Box -->
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
            <form class='login-page' method='post' id="form1" name="form1"  onsubmit=' return addRow()'>
                <div class='login-header margin-bottom-30'>
                    <h2 class='text-left'>New Question</h2>
                </div>
				
                    <span>
                        <!-- <i class='fa fa-user'></i> -->
						<label>data</label>
                    </span>
				<SELECT class="form-control" id="databasename" name="databasename" onChange="valueselect(this);" size="1">
					<?php
						$selectedDatabase = $databasename;
/////////////////////// ---- BOC
						foreach($databaseObj as $row)
						{
							$schema_name = $row['schema_name'];
							// -- Select the Selected Role 
							if($row['schema_name'] == $selectedDatabase)
							{
								echo "<OPTION value=$schema_name selected>$schema_name</OPTION>";
							}
							else
							{
							 echo "<OPTION value=$schema_name>$schema_name</OPTION>";
							}
						}

						if(empty($databaseObj))
						{
							echo "<OPTION value=0>No database</OPTION>";
						}
/////////////////////// ---- EOC
					?>
					</SELECT>
                	
						<!--- TABLE NAMES --->
							<label class="control-label">Table Names
							</label>
								<div class="control-group">
									<div class="controls">
									  <SELECT class="form-control" id="tablenames" name="tablenames" onChange="valueselect2(this);" size="1" >
										<?php
											// -- Select tablenames from a selected database name.
												$selectedtablename = $tablenames;
												foreach($tablenamesObj as $row)
												{
													// -- table_name
													$table_name = $row['table_name'];													
													// -- Select the Selected table name. 
														if($selectedtablename == $table_name)
														{
															echo "<OPTION value=$table_name selected>$table_name</OPTION>";
														}
														else
														{
															echo "<OPTION value=$table_name>$table_name</OPTION>";
														}
												}
																					
												/*if(empty($tablenamesObj->rowCount() <= 0))
												{
													echo "<OPTION value=0>No table names</OPTION>";
												}*/
										?>
									  </SELECT>
									</div>	
								</div>
								<!--- FIELDS --->
							<label class="control-label">Table Fields
							</label>
								<div class="control-group">
									<div class="controls">
									  <SELECT class="form-control" id="fields" name="fields" size="1" >
										<?php
										// -- Select tablenames from a selected database name.
										   $selectedfieldname = $fields;
											foreach($fieldnamesObj as $row)
											{
												// -- field_name
												$field_name = '';
												if(isset($row['column_name']))
												{
												 $field_name = $row['column_name'];																										
												}
												// -- Select the Selected table name. 
													if($selectedfieldname == $field_name)
													{
														echo "<OPTION value=$field_name selected>$field_name</OPTION>";
													}
													else
													{
														echo "<OPTION value=$field_name>$field_name</OPTION>";
													}
											}
																			
											/*if(empty($fieldnamesObj->rowCount() <= 0))
											{
												echo "<OPTION value=0>No table field names</OPTION>";
											}*/
										?>
									  </SELECT>
									</div>	
								</div>
							<label class="control-label">Query Builder
								<div class="control-group">
									<input  class='btn btn-primary' onclick='input()' type='button' value='Add' id='button'>
									<input  class='btn btn-primary' onclick='All()' type='button' value='Add ALL' id='button'>
									<input  class='btn btn-primary' onclick='removeALL()' type='button' value='Remove ALL' id='button'>
									<input  class='btn btn-primary' onclick='remove()' type='button' value='Remove' id='button'><br>														
									<input style="height:30px"  class="form-control margin-bottom-20"  id="query" name="query" type="text"  placeholder="columns" value="<?php echo !empty($query)?$query:'';?>" readonly>
								</div>								
							<label class="control-label">View as
							</label>
								<div class="control-group">
									<div class="controls">
									  <SELECT class="form-control" id="view" name="view" size="1" >
									  <OPTION value='RAW'>RAW DATA</OPTION>
									  <OPTION value='SUM'>SUM</OPTION>
									  <OPTION value='AVG'>AVERAGE</OPTION>									  
									  </SELECT>
									</div>
								</div>
							<label class="control-label">Virtulazation
							</label>
								<div class="control-group">
									<div class="controls">
									  <SELECT class="form-control" id="virtual" name="virtual" size="1" >
										  <OPTION value='TABLE'>TABLE</OPTION>
										  <OPTION value='PIA'>PIA CHART</OPTION>
										  <OPTION value='BAR'>BAR CHART</OPTION>	
										  <OPTION value='NUM'>NUMBER</OPTION>									  									  
									  </SELECT>
									</div>
								</div>
								
								<div class='col-md-6'>
                        <button class='btn btn-primary pull-right' type='submit'>Run Query</button>
                    </div>
					<br/>
				</div>							
                <div class='row'>
                    
                </div>                                    
            </form>							
             <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				  <div class="table-responsive">

				
		              <?php 
					  if(!empty($_POST))
					  {
						$fieldnamesObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//									DISPLAY QUERY															  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>";
						// -- Header.
//						echo 'countHeaders = '.$countHeaders;
						echo "<tr>";
						for($i=0;$i<$countHeaders;$i++)
						{
							echo "<td>".$queryFields[$i]."</td>"; 	
						}		
						echo "</tr>";
						// -- Items
						foreach($fieldnamesObj as $row)
						{ 
							foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
							echo "<tr>";  
							//if(isset($row["'".$fields."'"]))
							for($i=0;$i<$countHeaders;$i++)	
							{echo "<td valign='top'>" . nl2br($row[$i]) . "</td>";}  
							echo "</tr>"; 
						}
						echo "</table>"; 
						}
					  ?>
			
				</div>
    	</div>
        </div>
	</div>	
	<?php }?>  		
   		<!---------------------------------------------- End Data ----------------------------------------------->
		<script>
 // -- Database tables  ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selecteddatabasename = document.getElementById('databasename').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectTables.php",
				       data:{selecteddatabasename:selecteddatabasename},
						success: function(data)
						 {	
							//alert(data);
							jQuery("#tablenames").html(data);
											var selectedtablename = document.getElementById('tablenames').value;
					valueselect3(selectedtablename); 

						 }
					});
				});	
		
			  return false;
  }
  // -- Database table fields  --- //
 function valueselect2(sel) 
 {
		var selectedtablename = document.getElementById('tablenames').value;
		//alert(selectedtablename);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectFields.php",
				       data:{selectedtablename:selectedtablename},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#fields").html(data);
						 }
					});
				});				  
			  return false;
  } 
  
  function valueselect3(sel) 
 {
		var selectedtablename = sel;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectFields.php",
				       data:{selectedtablename:selectedtablename},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#fields").html(data);
						 }
					});
				});				  
			  return false;
  } 
  
  function All()
{
		var queries = document.getElementById('fields');
		var phone = '';
	for(var i=0; i < queries.length; i++)
     {
       cell = queries.options[i].value;
	   phone = document.forms.form1.query.value;
	   if(phone != '')
	   {
		   phone = phone+",";
	   }
	   document.forms.form1.query.value = phone + cell;
     }
}	
function input()
{
	var title = document.getElementById('fields').value;
	var area = document.forms.form1.query.value;
	 if(area != '')
	   {
		   //area = "027"+area.substring(1);
		   area = area+",";
	   }
    document.forms.form1.query.value = area + title;
}    
// -- Removing the last one added.. 16.12.2018
function remove()
{
	var title = document.getElementById('fields').value;
	var area = document.forms.form1.query.value;
	 if(area != '')
	   {
		   area = area+",";
	   }
	   else
	   {
		   
	   }
	str  = document.forms.form1.query.value;
	str = str.slice(0, -11);   
    document.forms.form1.query.value = str;
}

// -- Removing ALL the last one added.. 16.12.2018
function removeALL()
{
    document.forms.form1.query.value = '';
}
		</script>