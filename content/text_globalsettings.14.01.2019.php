<?php

require 'database.php';

// -- Database Connection.
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	
 $webpage = "customerLogin.php";
 $lv_username ="root";
 $lv_password = "root";
 $tbl_name="user"; // Table name 
 $ExecApproval = '';
 $To = '';
 $From = '';
 $count = 0;
 
// --------------------- BOC 2017.04.15 ------------------------- //
// --------------------- Bank and Branch code ------------------- //
 $sql = 'SELECT * FROM bank ORDER BY bankname';
 $dataBanks = $pdo->query($sql);						   						 

 $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
 $dataBankBranches = $pdo->query($sql);					

// ---------------------- BOC - Account Types ------------------------------- //
 $sql = 'SELECT * FROM accounttype';
 $dataaccounttype = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  
// ---------------------- BOC - Currency ------------------------------- //
 $sql = 'SELECT * FROM currency';
 $datacurrency = $pdo->query($sql);						   						 
// ---------------------- EOC - Currency ------------------------------- //  
 
// ------------------------- Declarations - Banking Details ------------------------------------- //
 $accountholdernameError = null;
 $accounttypeError = null;
 $accountnumberError = null;
 $branchcodeError = null;
 $BankNameError = null;
 
// ------------------------- $_POST - Banking Details ------------------------------------- //
 $accountholdername = '';//$_POST['accountholdername'];
 $accounttype = '';//$_POST['accounttype'];
 $accountnumber = '';//$_POST['accountnumber'];
 $branchcode = '';//$_POST['branchcode'];
 $bankname = '';//$_POST['bankname'];
// -------------------------------- Banking Details ------------------------------------------ //

// -------------------------------- Banking Details ------------------------------------------ //
 $homepage = '';
 $customerpage = '';
 $adminpage = '';
 
// --------------------- EOC 2017.04.15 ------------------------- //
 
// Id number
$idnumber = $_SESSION['username'];
			
$icamefrom = $_SERVER['HTTP_REFERER'];

// -- Address Errors
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$registrationnumberError = null;

// BOC --  03.08.2017 .
$vat = 0.00;
$vatError = null;
$vatnumber = '';
$vatnumberError = null;
// EOC --  03.08.2017 .

// -- Address 
$street = null;
$suburb = null;
$city = null;
$State = null;
$PostCode = null;

// -- Company details
$Company  = "";
$Address  = "";
$phone    = "";
$fax      = "";
$email    = "";
$website  = ""; 
$logo     = "";
$currency = "";
$legalterms    = "";
$target_dir  = "images/"; // Upload directory
$target_file = null;
$uploadOk = 1;
$imageFileType = null;

// -- Company Details Error Handle
$CompanyError  = "";
$AddressError  = "";
$phoneError    = "";
$faxError      = "";
$emailError    = "";
$websiteError  = "";
$logoError     = "";
$currencyError = "";
$legaltermsError  = "";
$rowCount = 0;
$globalsettingsid = 0;
$registrationnumber = "";
$logoTemp = "";


// ----- BOC -- 2017.06.04 ---------- //
$terms = "";
$ncr = "";
$disclaimer = "";

$mailhost = "";
$port = "";
$mailusername = "";
$mailpassword = "";
$SMTPSecure = "";
$dev = "";
$prod = "";
$radio = "p";

$mailhostError = "";
$portError = "";
$mailusernameError = "";
$mailpasswordError = "";
$SMTPSecureError = "";
$devError = "";
$prodError = "";

// -- Disable Captcha //
$captcha = '';
$captchaError = '';
// ----- EOC -- 2017.06.04 ---------- //
if (!empty($_POST)) 
{

// -- Company details from Form.
$logoTemp = $_POST['logoTemp'];
$Company  = $_POST['Company'];
$phone    = $_POST['phone'];
$fax      = $_POST['fax'];
$email    = $_POST['email'];
$website  = $_POST['website']; 

// BOC --  03.08.2017 .
$vat = $_POST['vat'];
// EOC --  03.08.2017 .

// ----- BOC -- 2017.06.04 ---------- //
$terms = trim_input($_POST['terms']);
$ncr = $_POST['ncr'];
$disclaimer = trim_input($_POST['disclaimer']);
// ------------------------- Declarations - Banking Details ------------------------------- //
// ------------------------- $_POST - Banking Details ------------------------------------- //
 $accountholdername = $_POST['accountholdername'];
 $accounttype = $_POST['accounttype'];
 $accountnumber = $_POST['accountnumber'];
 $branchcode = $_POST['BranchCode'];
 $bankname = $_POST['bankname'];
// -------------------------------- Banking Details ------------------------------------------ //
// ----- EOC -- 2017.06.04 ---------- //

// -- Logo.
if($_FILES["logo"]["name"] != "")
{
	$logo     = $_FILES["logo"]["name"];//$_POST['logo'];
}
else
{
	//$logo  = $_POST['logo'];
	$logo = $logoTemp; 
}
$currency = $_POST['currency']; 
$legalterms = trim_input($_POST['legalterms']);
$State    = $_POST['State'];
$street = $_POST['Street'];
$suburb = $_POST['Suburb'];
$city = $_POST['City'];
$PostCode = $_POST['PostCode'];
$registrationnumber = $_POST['registrationnumber'];
$target_file = $target_dir . basename($logo);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// ----- BOC -- 2017.06.04 ---------- //
$terms = trim_input($_POST['terms']);
$ncr = $_POST['ncr'];
$disclaimer = trim_input($_POST['disclaimer']);
// ------------------------- Declarations - Banking Details ------------------------------- //
// ------------------------- $_POST - Banking Details ------------------------------------- //
 $accountholdername = $_POST['accountholdername'];
 $accounttype = $_POST['accounttype'];
 $accountnumber = $_POST['accountnumber'];
 $branchcode = $_POST['BranchCode'];
 $bankname = $_POST['bankname'];

 $homepage = $_POST['txtEditor2'];
 $customerpage = $_POST['txtEditor3'];
 $adminpage = $_POST['txtEditor1'];
 
$mailhost = $_POST['mailhost'];
$port = $_POST['port'];
$mailusername = $_POST['mailusername'];
$mailpassword = $_POST['mailpassword'];
$SMTPSecure = $_POST['SMTPSecure'];

if(!empty($_POST['radio']))
{
	$radio = $_POST['radio'];
}

if(!empty($radio))
{
	if($radio == 'd')
	{
		$dev = 'd';
		$prod = '';
	}
	if($radio == 'p')
	{
		$prod = 'p';
		$dev = '';
	}
}

// -- Disable Captcha -- //
if(isset($_POST['captcha']))
{
	$captcha = $_POST['captcha'];
}
else
{
	$captcha = '';
}

// -- Company Details Validation.		
$valid = true;
// BOC --  03.08.2017 .
$vat = $_POST['vat'];
$vatnumber = $_POST['vatnumber'];

// -- if (empty($vatnumber)) { $vatError = 'Please enter Vat Number'; $valid = false;}
if (empty($vat)) { $vatError = 'Please enter Vat % e.g. 14.00'; $valid = false;}

if (!is_numeric($vat)) { $vatError = 'Please enter a number for Vat'.is_numeric($vat); $valid = false;}
// EOC --  03.08.2017 .

if (empty($street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
if (empty($suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
if (empty($city)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
if (empty($Company)) { $CompanyError = 'Please enter company name'; $valid = false;}

if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}

if (empty($phone)) { $phoneError = 'Please enter phone number'; $valid = false;}

if (empty($fax)) { $faxError = 'Please enter fax'; $valid = false;}

if (empty($email)) { $emailError = 'Please enter email'; $valid = false;}

if (empty($website)) { $websiteError = 'Please enter website'; $valid = false;}

if (empty($logo)) { $logoError = 'Please upload the logo'; $valid = false;}

if (empty($legalterms)) { $legaltermsError = 'Please enter legal terms'; $valid = false;}

// ---- BOC 2017.06.18 - Banking Details Error ------- //
if (empty($accountholdername)) { $AccountHolderError = 'Please enter account holder'; $valid = false;}
if (empty($bankname)) { $BankNameError = 'Please enter bank name'; $valid = false;}
if (empty($accounttype)) { $accounttypeError = 'Please enter account type'; $valid = false;}
if (empty($accountnumber)) { $accountnumberError = 'Please enter account number'; $valid = false;}
if (empty($branchcode)) { $branchcodeError = 'Please enter branch code'; $valid = false;}

if (empty($mailhost)) { $mailhostError = 'Please enter email host'; $valid = false;}
if (empty($port)) { $portError = 'Please enter port'; $valid = false;}
if (empty($mailusername)) { $mailusernameError = 'Please enter email host username'; $valid = false;}
if (empty($mailpassword)) { $mailpasswordError = 'Please enter email host password'; $valid = false;}
if (empty($SMTPSecure)) { $SMTPSecureError = 'Please enter email host password'; $valid = false;}
if (empty($dev) && empty($prod)) { $devError = 'Please select either production or development.'; $valid = false;}

// ---- EOC 2017.06.18 - Banking Details Error ------- //

// update data
if($valid) 
{	
	// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	
	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{
     	$globalsettingsid = $data['globalsettingsid'];
		$sql = "UPDATE globalsettings SET name = ?,phone = ?,fax = ?,email = ?,website = ?,logo = ?,legaltext = ?,currency = ?,
					   street = ?,suburb = ?,city = ?,province = ?,postcode = ?,registrationnumber = ?,terms = ?,ncr = ?,
					   disclaimer = ?,accountholdername = ?,bankname = ?,accountnumber = ?, branchcode = ?,accounttype = ?,homepage = ?,
					   adminpage = ?,customerpage = ?,vat = ?,vatnumber = ?,mailhost = ?,port = ?,username = ?,password = ?,SMTPSecure = ?,
					   dev = ?,prod = ?,captcha = ?
					   WHERE globalsettingsid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($Company,$phone,$fax,$email,$website,$logo,$legalterms,$currency,$street,$suburb,$city,$State,$PostCode,$registrationnumber,$terms,$ncr,$disclaimer,$accountholdername,$bankname,$accountnumber, $branchcode,$accounttype,$homepage,$adminpage,$customerpage,$vat,$vatnumber,
		$mailhost,$port,$mailusername,$mailpassword,$SMTPSecure,$dev,$prod,$captcha,$globalsettingsid));
					$count++;				  
	}
	// -- Insert Address & Company details.
	else
	{	

		$sql = "INSERT INTO globalsettings (name,phone ,fax ,email,website ,logo ,legaltext ,currency ,street ,suburb ,city ,province,postcode,registrationnumber,terms,ncr,disclaimer,accountholdername,bankname,accountnumber,branchcode,accounttype,homepage,
		adminpage,customerpage,vat,vatnumber,mailhost,port,username,password,SMTPSecure,dev,prod,captcha)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		$q = $pdo->prepare($sql);
		$q->execute(array($Company,$phone,$fax,$email,$website,$logo,
						  $legalterms,$currency,$street,$suburb,$city,$State,$PostCode,$registrationnumber,$terms,$ncr,
						  $disclaimer,$accountholdername,$bankname,$accountnumber,$branchcode,$accounttype,$homepage,$adminpage,$customerpage,$vat,$vatnumber,$mailhost,$port,$mailusername,$mailpassword,$SMTPSecure,$dev,$prod,$captcha));
					$count++;
				  
	}
	
// -- Move a logo to file directory.
		// -- Update File Name Directory	
		// Loop $_FILES to execute all files
	  if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) 
	  {
        //echo "The file ". basename( $_FILES["logo"]["name"]). " has been uploaded.";
	  } 
	  else 
	  {
        //echo "Sorry, there was an error uploading your file.";
      }
	Database::disconnect();
}	

}
else
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();
	
	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $data['name'];
		$phone    = $data['phone'];
		$fax      = $data['fax'];
		$email    = $data['email'];
		$website  = $data['website']; 
		$logo     = $data['logo'];
		$logoTemp = $logo;
		$currency = $data['currency']; 
		$legalterms = $data['legaltext'];

		$street = $data['street'];
		$suburb = $data['suburb'];
		$city = $data['city'];
		$State = $data['province'];
		$PostCode = $data['postcode'];	
		$registrationnumber = $data['registrationnumber'];
		
		$terms = trim_input($data['terms']);
		$ncr = $data['ncr'];
		
		// --- Banking Details ------ //
		$disclaimer = trim_input($data['disclaimer']);
		$bankname = $data['bankname'];	
		$accountholdername = $data['accountholdername'];
		$accountnumber = $data['accountnumber'];
		$branchcode  = $data['branchcode'];
		$accounttype = $data['accounttype'];
		$homepage = $data['homepage'];
		$customerpage = $data['customerpage'];
		$adminpage = $data['adminpage'];
		$vat = $data['vat'];
		$vatnumber = $data['vatnumber'];
		
		$mailhost = $data['mailhost'];
		$port = $data['port'];
		$mailusername = $data['username'];
		$mailpassword = $data['password'];
		$SMTPSecure = $data['SMTPSecure'];
		$dev = $data['dev'];
		$prod = $data['prod'];		
		// -- EOC Banking Details -- //
		
// -- Disable Captcha -- //
		$captcha = $data['captcha'];
		// -- Default Branch code.
		if(empty($bankname))
		{
			// -- Default bankname - ABSA
			$bankname = "ABSA";
		}
	}	
}
// -- To avoid : Redeclaring trim.
if (!function_exists('trim_input'))   
{
	function trim_input($data) 
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
}
?>
		<!---------------------------------------------- Start Data ----------------------------------------------->
		
<!-- <div class='container background-white'> -->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->		
		 <!--<div class='login-header margin-bottom-30'>
		 <br/>
              <h2>Loan Types</h2>
         </div> -->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  
		<!-------------------------------------- Company Details ------------------------------------------------>
		  <form  action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post" enctype="multipart/form-data">
			    <div class="table-responsive">
					<table class=   "table table-user-information">
					<tr>
						<td>
<button type="submit" class="btn btn-primary" onclick="UpdateHomePage()"><span><span>Save Company Details</span></span></button>		
						</td>
					</tr>
					<!-- BOC SMTP Server Setting -->
					<tr>
					<td colspan="3"> 
						<div class="panel-heading">
																	
										<h2 class="panel-title">
											<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
											 <h2>Email Server Setup - Enable sending of emails</h2>
											</a>
										</h2>
						</div>	
							<?php # error messages
							if (isset($message)) 
							{
								foreach ($message as $msg) {
									printf("<p class='status'>%s</p></ br>\n", $msg);
								}
							}
							# success message
							if($count !=0)
							{
								printf("<p class='status'>Email Server Setup Updated Successfully!</p>\n", $count);
								// Sent Rejected or Approved status to customer provided
							}
							?>
						</td>
					</tr>
					<tr>						
					  <td>
						<div class="control-group <?php echo !empty($mailhostError)?'error':'';?>">
							<label class="control-label">Email Host Server</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="mailhost" name="mailhost" type="text"  placeholder="mailhost" value="<?php echo !empty($mailhost)?$mailhost:'';?>">
								<?php if (!empty($mailhostError)): ?>
								<span class="help-inline"><?php echo $mailhostError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
						<div class="control-group <?php echo !empty($portError)?'error':'';?>">
							<label class="control-label">Email Port Number</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" name="port" type="text"  placeholder="port" value="<?php echo !empty($port)?$port:'';?>">
								<?php if (!empty($portError)): ?>
								<span class="help-inline"><?php echo $portError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
						<div class="control-group <?php echo !empty($SMTPSecureError)?'error':'';?>">
							<label class="control-label">Email SMTP Secure</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" name="SMTPSecure" type="text"  placeholder="SMTPSecure" value="<?php echo !empty($SMTPSecure)?$SMTPSecure:'';?>">
								<?php if (!empty($SMTPSecureError)): ?>
								<span class="help-inline"><?php echo $SMTPSecureError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					</tr>
					<!-- BOC - Second Row -->
					<tr>
<td>
						<div class="control-group <?php echo !empty($mailusernameError)?'error':'';?>">
							<label class="control-label">Email UserName</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="mailusername" name="mailusername" type="text"  placeholder="Email Username" value="<?php echo !empty($mailusername)?$mailusername:'';?>">
								<?php if (!empty($mailusernameError)): ?>
								<span class="help-inline"><?php echo $mailusernameError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>					
					  <td>
						<div class="control-group <?php echo !empty($mailpasswordError)?'error':'';?>">
							<label class="control-label">Email Password</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="mailpassword" name="mailpassword" type="password"  placeholder="password" value="<?php echo !empty($mailpassword)?$mailpassword:'';?>">
								<?php if (!empty($mailpasswordError)): ?>
								<span class="help-inline"><?php echo $mailpasswordError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td></td>
					</tr>
					<!-- 3rd Row -->
					<tr>
					  <td>
						<div class="control-group <?php echo !empty($prodError)?'error':'';?>">
							<label class="control-label">In Production</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" name="radio" type="radio" 
								value="p" <?php if(!empty($prod)){echo "checked";}?>>
								<?php if (!empty($prodError)): ?>
								<span class="help-inline"><?php echo $prodError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
					    <div class="control-group <?php echo !empty($devError)?'error':'';?>">
							<label class="control-label">In Development</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" name="radio" type="radio"  
								value="d" <?php if(!empty($dev)){echo "checked";}?>>
								<?php if (!empty($devError)): ?>
								<span class="help-inline"><?php echo $devError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
						 <div class="control-group <?php echo !empty($captchaError)?'error':'';?>">
							<label class="control-label">Disable captcha</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="captcha" name="captcha" type="checkbox"  
								value="d" <?php if(!empty($captcha)){echo "checked";}?>>
								<?php if (!empty($captchaError)): ?>
								<span class="help-inline"><?php echo $captchaError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					</tr>
					<!-- EOC SMPTP Server Settings -->
					<tr>
					<td colspan="3"> 
						<div class="panel-heading">
																	
										<h2 class="panel-title">
											<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
											 <h2>Company And Address Details</h2>
											</a>
										</h2>
						</div>	
							<?php # error messages
							if (isset($message)) 
							{
								foreach ($message as $msg) {
									printf("<p class='status'>%s</p></ br>\n", $msg);
								}
							}
							# success message
							if($count !=0)
							{
								printf("<p class='status'>Company Information updated successfully!</p>\n", $count);
								// Sent Rejected or Approved status to customer provided
							}
							?>
						</td>
					</tr>
					<tr>						
					  <td>
						<div class="control-group <?php echo !empty($CompanyError)?'error':'';?>">
							<label class="control-label">Name</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="Company" name="Company" type="text"  placeholder="name" value="<?php echo !empty($Company)?$Company:'';?>">
								<?php if (!empty($CompanyError)): ?>
								<span class="help-inline"><?php echo $CompanyError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
						<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
							<label class="control-label">Phone</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone" value="<?php echo !empty($phone)?$phone:'';?>">
								<?php if (!empty($phoneError)): ?>
								<span class="help-inline"><?php echo $phoneError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					  <td>
						<div class="control-group <?php echo !empty($faxError)?'error':'';?>">
							<label class="control-label">Fax</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="fax" name="fax" type="text"  placeholder="fax" value="<?php echo !empty($fax)?$fax:'';?>">
								<?php if (!empty($faxError)): ?>
								<span class="help-inline"><?php echo $faxError;?></span>
								<?php endif; ?>
							</div>
						</div>
					  </td>
					</tr>	
					<tr>
					<!-- Street,Suburb,City -->		
					<tr>
						<td> 
						<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
							<label class="control-label">Street</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($street)?$street:'';?>">
								<?php if (!empty($StreetError)): ?>
								<span class="help-inline"><?php echo $StreetError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>

						<td> 
						<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
							<label class="control-label">Suburb</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($suburb)?$suburb:'';?>">
								<?php if (!empty($SuburbError)): ?>
								<span class="help-inline"><?php echo $SuburbError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>

						<td> 
						<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
							<label class="control-label">City</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($city)?$city:'';?>">
								<?php if (!empty($CityError)): ?>
								<span class="help-inline"><?php echo $CityError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>
					</tr>	
					</tr>
					<!-- State,PostCode,Dob -->		
					<tr>
						<td> 
						<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
							<label class="control-label">Province</label>
							<div class="controls">
								<!-- <input class="form-control margin-bottom-10" name="State" type="text"  placeholder="Province" value="<?php echo !empty($State)?$State:'';?>"> -->
								 <SELECT class="form-control" id="State" name="State" size="1">
									<OPTION value="KZ" <?php if ($State == 'KZ') echo 'selected'; ?>>Kwazulu Natal</OPTION>
									<OPTION value="EC" <?php if ($State == 'EC') echo 'selected'; ?>>Eastern Cape</OPTION>
									<OPTION value="FS" <?php if ($State == 'FS') echo 'selected'; ?>>Free State</OPTION>
									<OPTION value="GP" <?php if ($State == 'GP') echo 'selected'; ?>>Gauteng</OPTION>
									<OPTION value="LP" <?php if ($State == 'LP') echo 'selected'; ?>>Limpopo</OPTION>
									<OPTION value="MP" <?php if ($State == 'MP') echo 'selected'; ?>>Mpumalanga</OPTION>
									<OPTION value="NW" <?php if ($State == 'NW') echo 'selected'; ?>>North West</OPTION>
									<OPTION value="NC" <?php if ($State == 'NC') echo 'selected'; ?>>Northern Cape</OPTION>
									<OPTION value="WC" <?php if ($State == 'WC') echo 'selected'; ?>>Western Cape</OPTION>
								</SELECT>
								<?php if (!empty($StateError)): ?>
								<span class="help-inline"><?php echo $StateError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>

						<td> 
						<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
							<label class="control-label">Postal Code</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="PostCode" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
								<?php if (!empty($PostCodeError)): ?>
								<span class="help-inline"><?php echo $PostCodeError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>
						<td> 
						<div class="control-group <?php echo !empty($currencyError)?'error':'';?>">
							<label class="control-label">Currency</label>
							<div class="controls">
								 <SELECT class="form-control" name="currency" id="currency" size="1">
								 <?php $currencyid = '';
											$currencySelect = $currency;
											$currencyname = '';
											foreach($datacurrency as $row)
											{
												$currencyid = $row['currencyid'];
												$currencyname = $row['currencyname'];
												if($currencySelect == $currencyid)
												{
												 echo "<OPTION value=$currencyid selected>$currencyname</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$currencyid>$currencyname</OPTION>";
												} 
											}
											if(empty($datacurrency))
											{
												echo "<OPTION value=0>No Currency</OPTION>";
											}
								 ?>
								 </SELECT>
							</div>
						</div>
						</td>

					</tr>
					<tr>
					<td> 
						<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
							<label class="control-label">Email</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
								<?php if (!empty($emailError)): ?>
								<span class="help-inline"><?php echo $emailError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>
						<td> 
						<div class="control-group <?php echo !empty($websiteError)?'error':'';?>">
							<label class="control-label">Website</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="website" name="website" type="text"  placeholder="website" value="<?php echo !empty($website)?$website:'';?>">
								<?php if (!empty($websiteError)): ?>
								<span class="help-inline"><?php echo $websiteError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>
						<td> 
						<div class="control-group <?php echo !empty($registrationnumberError)?'error':'';?>">
							<label class="control-label">Company Registration Number</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="registrationnumber" 
								name="registrationnumber" type="text"  placeholder="Company Registration Number" value="<?php echo !empty($registrationnumber)?$registrationnumber:'';?>">
								<?php if (!empty($registrationnumberError)): ?>
								<span class="help-inline"><?php echo $registrationnumberError;?></span>
								<?php endif; ?>
							</div>
						</div>
						</td>
						
					</tr>
					
					<tr>
					<td> 
						<div class="control-group <?php echo !empty($registrationnumberError)?'error':'';?>">
							<label class="control-label">National Credit Number(NCR)</label>
							<div class="controls">
								<input style="height:30px" class="form-control margin-bottom-10" id="ncr" 
								name="ncr" type="text"  placeholder="National Credit Number" value="<?php echo !empty($ncr)?$ncr:'';?>">
								<?php if (!empty($ncrError)): ?>
								<span class="help-inline"><?php echo $ncrError;?></span>
								<?php endif; ?>
							</div>
						</div>
					</td>
					<td> 
						<label>Account Holder Name</label>
								<div class="control-group <?php echo !empty($accountholdernameError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="accountholdername" name="accountholdername" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($accountholdername)?$accountholdername:'';?>" />
									
									<?php if (!empty($accountholdernameError)): ?>
										<span class="help-inline"><?php echo $accountholdernameError;?></span>
										<?php endif; ?>
									</div>	
								</div>
					</td>
					<td> 
						<!-- Bank Name -->
					 <label>Bank Name</label>
						<div class="control-group <?php echo !empty($banknameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										  <SELECT class="form-control" id="bankname" name="bankname" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $bankname;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
						</div>
					</td>
					</tr>
					<tr>
						<td>
						<!-- Account Type  -->
							<label>Account Type
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
										 <SELECT class="form-control" id="accounttype" name="accounttype" size="1" >
								<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $accounttype;
											$accounttypedesc = '';
											foreach($dataaccounttype as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataaccounttype))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Account type --->
								</SELECT>

									</div>	
								</div>
						</td>
						<td>
						    <!-- Branch Code  -->
							<label class="control-label">Branch Code
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($branchcodeError)?'error':'';?>">
									<div class="controls">
									 <SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $bankname;
											$BranchSelect = $branchcode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $branchcode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
									</div>
								</div>
						</td>
						<td>
						<!-- Account Number  -->
							<label>Account Number
							<span class="color-red">*</span></label>
							 <div class="control-group <?php echo !empty($accountnumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="accountnumber" name="accountnumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($accountnumber)?$accountnumber:'';?>" />
									
									<?php if (!empty($accountnumberError)): ?>
										<span class="help-inline"><?php echo $accountnumberError;?></span>
										<?php endif; ?>
									</div>	
							  </div>
								
						</td>					
					</tr>
					
					<tr>
					<td>
						<!-- Account Number  -->
							<label>VAT % e.g. 14.00
							</label>
							 <div class="control-group <?php echo !empty($vatError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="vat" name="vat" type="text"  placeholder="Vat % e.g 14.00" value="<?php echo !empty($vat)?$vat:'';?>" />
									
									<?php if (!empty($vatError)): ?>
										<span class="help-inline"><?php echo $vatError;?></span>
										<?php endif; ?>
									</div>	
							  </div>
								
						</td>
						<td>
						<!-- vat number  -->
							<label>VAT Number
							</label>
							 <div class="control-group <?php echo !empty($vatnumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="vatnumber" name="vatnumber" type="text"  placeholder="VAT Number" value="<?php echo !empty($vatnumber)?$vatnumber:'';?>" />
									
									<?php if (!empty($vatnumberError)): ?>
										<span class="help-inline"><?php echo $vatError;?></span>
										<?php endif; ?>
									</div>	
							  </div>
								
						</td>						
					</tr>
					
					</tr>
					<tr>
					<td colspan="3"> 
						<label for="config_company_logo">Logo Size(Width :1061px Height:255px)</label>
								  <div id="company_logo_container <?php echo !empty($logoError)?'error':'';?>">
										<div><img src="images/<?php echo $logo;?>" width="577"/>
										<br/><input type="file" name="logo" id="logo" value="<?php echo !empty($logo)?$logo:'';?>"  />
										<input type="text" name="logoTemp" id="logoTemp" value="<?php echo $logoTemp;?>" style="display:none"/>

										<?php if (!empty($logoError)): ?>
											<span class="help-inline"><?php echo $logoError;?></span>
										<?php endif; ?>
										</div>
								  </div>
						</td>
					</tr>
					<tr>
					<td colspan="3"> 
					  <div class="form-group">
					  	<label class="control-label">Legal Disclaimer</label>
						<textarea class="form-control" rows="3" name="legalterms" id="legalterms" placeholder="Legal Terms"><?php echo trim_input($legalterms);?>
						</textarea>
						<?php if (!empty($legaltermsError)): ?>
								<span class="help-inline"><?php echo $legaltermsError;?></span>
						<?php endif; ?>
					  </div>
					</td>
					</tr>
					<tr>
					<td colspan="3"> 
					  <div class="form-group">
					  	<label class="control-label">Company Compliance Disclaimer</label><textarea class="form-control" rows="3" name="disclaimer" id="disclaimer" placeholder="Terms And Conditions"><?php echo $disclaimer;?>
						</textarea><?php if (!empty($disclaimerError)): ?>
								<span class="help-inline"><?php echo $disclaimerError;?></span>
						<?php endif; ?>
					  </div>
					</td>
					</tr>
					<tr>
					<td colspan="3"> 
					  <div class="form-group">
					  	<label class="control-label">Terms and Conditions</label>
						<textarea class="form-control" rows="3" name="terms" id="terms" placeholder="Terms And Conditions"><?php echo $terms;?>
						</textarea>
						<?php if (!empty($termsError)): ?>
								<span class="help-inline"><?php echo $termsError;?></span>
						<?php endif; ?>
					  </div>
					</td>
					</tr>
					<!---- 			BOC - Editor  			---->
					<tr>
					<td colspan="3"> 
						<label class="control-label">Home Page - Content</label>
						   <div class="container-fluid">
								<div class="row">
									<div class="container">
										<div class="row">
											<div class="col-lg-12 nopadding">
												<textarea id="txtEditor" name="txtEditor"><?php echo $homepage;?></textarea> 
												<textarea id="txtEditor2" name="txtEditor2" style="display:none"><?php echo $homepage;?></textarea> 
											</div>
										</div>
									</div>
								</div>
							</div>  
						</td>
					</tr>
					<tr>
					<td colspan="3"> 
						<label class="control-label">Admin Page - Content</label>
							<div class="container-fluid">
								<div class="row">
									<div class="container">
										<div class="row">
											<div class="col-lg-12 nopadding">
												<textarea id="txtEditorAdmin" name="txtEditorAdmin"><?php echo $adminpage;?></textarea> 
												<textarea id="txtEditor1" name="txtEditor1" style="display:none"><?php echo $adminpage;?></textarea> 
											</div>
										</div>
									</div>
								</div>
							</div>  
						</td>
					</tr>
					<tr>
					<td colspan="3"> 
						<label class="control-label">Customer Page - Content</label>
							<div class="container-fluid">
								<div class="row">
									<div class="container">
										<div class="row">
											<div class="col-lg-12 nopadding">
												<textarea id="txtEditorCustomer" name="txtEditorCustomer"><?php echo $customerpage;?></textarea> 
												<textarea id="txtEditor3" name="txtEditor3" style="display:none"><?php echo $customerpage;?></textarea> 
											</div>
										</div>
									</div>
								</div>
							</div>  
						</td>
					</tr>
					<!---- 			EOC - Editor  			---->
					<tr>
						<td>
<button type="submit" class="btn btn-primary" onclick="UpdateHomePage()"><span><span>Save Company Details</span></span></button>								
						</td>
					</tr>
					</table>
				</div>
		  </form>
		<!-------------------------------------- End Company Details -------------------------------------------->
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
  
function UpdateHomePage()
{
// -- Home Page.
	var val1 = document.getElementById("txtEditor").value;  
	var val2 = document.getElementById("txtEditorAdmin").value;   
	var val3 = document.getElementById("txtEditorCustomer").value;   

	jQuery(document).ready(function() 
	{
				// -- Home Page
				val1 = jQuery("#txtEditor").Editor("getText");
			    document.getElementById("txtEditor2").value = val1;
				
				// -- Admin Page
				val2 = jQuery("#txtEditorAdmin").Editor("getText");
			    document.getElementById("txtEditor1").value = val2;
				
				// -- Customer Page.
				val3 = jQuery("#txtEditorCustomer").Editor("getText");
			    document.getElementById("txtEditor3").value = val3;
	});	   	
		
} 

jQuery(document).ready(function() 
{
				// -- Home Page.
			    val = document.getElementById("txtEditor2").value;
				 jQuery("#txtEditor").Editor("setText", val); 
				 
				 // -- Admin Page.
				val1 = document.getElementById("txtEditor1").value;
				 jQuery("#txtEditorAdmin").Editor("setText", val1); 

				 // -- Customer Page.
				 val2 = document.getElementById("txtEditor3").value;
				 jQuery("#txtEditorCustomer").Editor("setText", val2); 

});	   

// -- 15.07.2017 - Banking Details ---  30
</script>
		