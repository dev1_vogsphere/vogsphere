<!-- Debit Order Schedule 2018.06.02 -->
<!----------- debitorderschedule.js -----------------> 
<script src="assets/js/debitorderschedule.js"></script>
<!-- End Debit Order Schedule -->
<?php 
	require 'database.php';
	$tbl_customer = "customer";
	$tbl_loanapp = "rentalapp";
	$dbname = "eloan";

	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
	
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
	}
	
	if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
	if ( null==$customerid ) 
	    {
		header($location);
		}
		else if( null==$ApplicationId ) 
		{
		header($location);
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND rentalappid = ?";	
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		  
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header($location);
		}

		if( isset($data) && ($data!==null) )	
		{
		}
		else
		{
						header($location);

		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		$sql = 'SELECT * FROM user WHERE userid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
	    $dataUserEmail = $q->fetchAll();
		
		// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //

		// --------------------- BOC 2017.04.15 ------------------------- //
		//1. --------------------- Bank and Branch code ------------------- //
		  $sql = 'SELECT * FROM bank WHERE bankid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname']));
		  $dataBanks = $q->fetch(PDO::FETCH_ASSOC);						   						 
		 
		  
		  $sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['BankName'],$data['BranchCode']));
		  $dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);			
		  
		  // $data['branchcode'] = $dataBankBranches['branchdesc'];
		  //$data['BankName'] = $dataBanks['bankname'];
		  
		//2. ---------------------- BOC - Provinces ------------------------------- //
		// 15.04.2017 - 
		  $sql = 'SELECT * FROM province WHERE provinceid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['State']));
		  $dataProvince = $q->fetch(PDO::FETCH_ASSOC);						   						 
		  $data['State'] = $dataProvince['provincename'];
		  
		//3. ---------------------- BOC - Account Types ------------------------------- //
		// 15.04.2017 - 
		 $sql = 'SELECT * FROM accounttype WHERE accounttypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['accounttype']));
		 $dataAccountType = $q->fetch(PDO::FETCH_ASSOC);						   						
		 $data['accounttype'] = $dataAccountType['accounttypedesc']; 	
		  
		// ---------------------- EOC - Account Types ------------------------------- //  

		//4. ---------------------- BOC - Payment Method ------------------------------- //
		// 15.04.2017 - 
		 /*$sql = 'SELECT * FROM paymentmethod WHERE paymentmethodid = ?';
		 $q = $pdo->prepare($sql);	
		 $q->execute(array($data['paymentmethod']));		 
		 $dataPaymentMethod =$q->fetch(PDO::FETCH_ASSOC);							   						 
		 $data['paymentmethod'] = $dataPaymentMethod['paymentmethoddesc'];
		 */
		// ---------------------- EOC - Payment Method ------------------------------- //  

		//5. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 - 
		 /*$sql = 'SELECT * FROM paymentfrequency WHERE paymentfrequencyid = ?';
		 $q = $pdo->prepare($sql);	
		 $q->execute(array($data['paymentfrequency']));
		 $dataPaymentFrequency = $q->fetch(PDO::FETCH_ASSOC);						   						 
		 $data['paymentfrequency'] = $dataPaymentFrequency['paymentfrequencydesc'];
		 */
		// ---------------------- EOC - Payment Frequency ------------------------------- //  

		//6. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 - 
		/* $sql = 'SELECT * FROM loantype WHERE loantypeid = ?';
		 $q = $pdo->prepare($sql);		 
		 $q->execute(array($data['loantypeid']));
		 $dataLoanType = $q->fetch(PDO::FETCH_ASSOC);	
		 $data['loantypeid'] = $dataLoanType['loantypedesc'];		 
		 */
		// ---------------------- EOC - Payment Frequency ------------------------------- //  
		// BOC -- 15.04.2017 -- Updated.
		$BankName  = '';
		$BranchCode = '';
		// EOC -- 15.04.2017 -- Updated.	
		// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
		
		Database::disconnect();
		
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;
	}
?>
<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div class="row">
<!-- Login Box -->
<form class="login-page" action="read?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

									<table class=   "table table-user-information">
						<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->		
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>						
</td></div>							
		
</tr>

<tr>						
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Title</strong></label>
		<div class="controls">
		  	<?php echo $data['Title'];?>
		</div>
	</div>
	</td> 

	<td> 
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>First Name</strong></label>
		<div class="controls">
			<?php echo $data['FirstName'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Last Name</strong></label>
		<div class="controls">
			<?php echo $data['LastName'];?>
		</div>
	</div>
	</td>
 
</tr>	

<!-- Street,Suburb,City -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Street</strong></label>
		<div class="controls">
		   	<?php echo $data['Street'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<?php echo $data['Suburb'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label"><strong>City</strong></label>
		<div class="controls">
		    <?php echo $data['City'];?>
		</div>
	</div>
	</td>
</tr>	

<!-- State,PostCode,Dob -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<?php
		// --------------------- BOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
			echo $data['State'];
			?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label"><strong>Postal Code</strong></label>
		<div class="controls">
		  	<?php echo $data['PostCode'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Date of birth</strong></label>
		<div class="controls">
		    <?php echo $data['Dob'];?>
		</div>
	</div>
	</td>
</tr>	
<tr>
<td> 
	<div class="control-group">
		<label class="control-label"><strong>Contact Number</strong></label>
		<div class="controls">
		    <?php echo $data['phone'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Email</strong></label>
		<div class="controls">
		    <?php
			foreach(  $dataUserEmail as $rowEmail)
			{
			   echo $rowEmail['email'];
			}	
			?>
		</div>
	</div>
	</td>
</tr>
<tr>
<td> 
	<div class="control-group">
		<label class="control-label"><strong>Nationality</strong></label>
		<div class="controls">
		    <?php echo $data['nationality'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Sex</strong></label>
		<div class="controls">
		    <?php
			   echo $data['gender'];
			?>
		</div>
	</div>
	</td>
</tr>
<!----------------------------------------------------------- Banking Details -------------------------------------------->
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<!-- Account Holder Name,Bank Name,Account number -->	
<tr>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account Holder Name</strong></label>
		<div class="controls">
			<?php echo $data['AccountHolder'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Bank Name</strong></label>
		<div class="controls">
			<?php echo $data['BankName'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
		    <?php echo $data['AccountNumber'];?>
		</div>
	</div>
	</td>
</tr>	
<!-- branch code,Account type -->	
<tr>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Branch Code</strong></label>
		<div class="controls">
			<?php echo $data['BranchCode'].' - '.$dataBankBranches['branchdesc'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account Type</strong></label>
		<div class="controls">
			<?php echo $data['AccountType'];?>
		</div>
	</div>
	</td>
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Payment Method</strong></label>
				<div class="controls">
					<?php echo $data['optpayments'];?>
				</div>
		</div>
	</td> 
</tr>	

<!----------------------------------------------------------- End Banking Details ---------------------------------------->	

<!----------------------------------------------------------- Loan Details -------------------------------------------->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Questionares
                    </a>
                </h2>						
</td></div>							
		
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>How long will you live here?</strong></label>
		<div class="controls">
		    <?php echo $data['leasedurarion'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>How many evictions have been filled upon you?</strong></label>
		<div class="controls">
		   <?php echo $data['evictions'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>What pets do you have?</strong></label>
		<div class="controls">
		  <?php echo $data['pets'];?>
		</div>
	</div>
	</td>
</tr>	

<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>What would limit your ability to pay rent?</strong></label>
		<div class="controls">
		    <?php echo $data['limitstopay'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>How many vehicles do you have?(Parking space)</strong></label>
		<div class="controls">
		   <?php echo $data['vehicles'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>Have you ever broken a lease agreement?</strong></label>
		<div class="controls">
		  <?php echo $data['brokenlease'];?>
		</div>
	</div>
	</td>
</tr>	

<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>How did you find out about this vacancy?</strong></label>
		<div class="controls">
		    <?php echo $data['aboutvacancy'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Do you have a checking account?</strong></label>
		<div class="controls">
		   <?php echo $data['checkingaccount'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>How many felonies do you have?</strong></label>
		<div class="controls">
		  <?php echo $data['felonies'];?>
		</div>
	</div>
	</td>
</tr>	
<!-- NEW -->
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Emergency contact name & number</strong></label>
		<div class="controls">
		    <?php echo $data['emergencynamenumber'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Do you smoke?</strong></label>
		<div class="controls">
		   <?php echo $data['smoke'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>Why should we rent to you?</strong></label>
		<div class="controls">
		  <?php echo $data['whyrenttome'];?>
		</div>
	</div>
	</td>
</tr>	

<!-- NEW -->
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Property you are applying for?</strong></label>
		<div class="controls">
		    <?php echo $data['property'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Desire Move in Date?</strong></label>
		<div class="controls">
		   <?php echo $data['desiredate'];?>
		</div>
	</div>
	</td>
</tr>	

<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Payment Responsibility Details
                    </a>
                </h2>						
</td></div>								
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Name</strong></label>
		<div class="controls">
		    <?php echo $data['resppname'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Surname</strong></label>
		<div class="controls">
		   <?php echo $data['respsurname'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Workplace</strong></label>
		<div class="controls">
		   <?php echo $data['respworkplace'];?>
		</div>
	</div>
	</td>
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Occupation</strong></label>
		<div class="controls">
		   <?php echo $data['respoccupation'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Work Address</strong></label>
		<div class="controls">
		   <?php echo $data['respworkaddr'];?>
		</div>
	</div>
	</td>
	<td> 
		<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
			<label class="control-label"><strong>Work Tel No</strong></label>
			<div class="controls">
				<?php echo $data['respworktel'];?>
			</div>
		</div>
	</td>
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Work Email</strong></label>
		<div class="controls">
		    <?php echo $data['respworkemail'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Street</strong></label>
		<div class="controls">
		   <?php echo $data['respstreet'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
		   <?php echo $data['respsuburb'];?>
		</div>
	</div>
	</td>
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>City</strong></label>
		<div class="controls">
		    <?php echo $data['respcity'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
		   <?php echo $data['respprovince'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Postal Code</strong></label>
		<div class="controls">
		   <?php echo $data['resppostalcode'];?>
		</div>
	</div>
	</td>
</tr>
	
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                          Application Fee Payment Details
                    </a>
                </h2>						
</td></div>								
</tr>
<tr>
	<td colspan="2"> 
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Application Fee Acceptance</strong></label>
		<div class="controls">
			<?php echo $data['applicationfeeTextValue'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Accepted Application Fee</strong></label>
		<div class="controls">
			<?php echo $data['appfeesaccept'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Application Fee Debit Date</strong></label>
		<div class="controls">
		    <?php echo $data['appfeedebitorderdate'];?>
		</div>
	</div>
	</td>
</tr>

<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Current Apartment Details
                    </a>
                </h2>						
</td></div>								
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Current Apartment Name</strong></label>
		<div class="controls">
			<?php echo $data['currapartmentname'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Contact number</strong></label>
		<div class="controls">
		    <?php echo $data['currcontactnumber'];?>
		</div>
	</div>
	</td>
    <td> 
	 <div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Caretaker Name</strong></label>
		<div class="controls">
			<?php echo $data['currcaretaker'];?>
		</div>
	  </div>
    </td>	
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Rent(R)</strong></label>
		<div class="controls">
			<?php echo $data['currmonthlyrent'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Reasons for Leaving</strong></label>
		<div class="controls">
		    <?php echo $data['currreasnforleaving'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Previous Apartment Details
                    </a>
                </h2>						
</td></div>								
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Previous Apartment Name</strong></label>
		<div class="controls">
			<?php echo $data['prevapartmentname'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Contact number</strong></label>
		<div class="controls">
		    <?php echo $data['prevcontactnumber'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Caretaker Name</strong></label>
		<div class="controls">
			<?php echo $data['prevcaretaker'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Rent(R)</strong></label>
		<div class="controls">
		    <?php echo $data['appfeedebitorderdate'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Reasons for Leaving</strong></label>
		<div class="controls">
			<?php echo $data['prevmonthlyrent'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Referrals Details
                    </a>
                </h2>						
</td></div>								
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>How did you hear about us?</strong></label>
		<div class="controls">
			<?php echo $data['optreferrals'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>If other please specify:</strong></label>
		<div class="controls">
			<?php echo $data['OtherSpecify'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Friend's Full Name</strong></label>
		<div class="controls">
		    <?php echo $data['friendfullname'];?>
		</div>
	</div>
	</td>
		<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Friend's Contact Number</strong></label>
		<div class="controls">
		    <?php echo $data['friendcontactnumber'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Friend's Email Address</strong></label>
		<div class="controls">
		    <?php echo $data['friendemail'];?>
		</div>
	</div>
	</td>
</tr>	

<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Rental Application Details
                    </a>
                </h2>						
</td></div>							
</tr>	
<!-- InterestRate,ExecApproval -->		
<tr>
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Applied on</strong></label>
				<div class="controls">
					<?php echo $data['DateAccpt'];?>
				</div>
		</div>
	</td> 
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Agreed to Terms & Conditions</strong></label>
				<div class="controls">
					<?php echo $data['agree'];?>
				</div>
		</div>
	</td> 
	<td> 	
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<div class="controls">
		<?php 
		if($data['ExecApproval'] == 'APPR')
								{
								echo 'Approved';
								}
								if($data['ExecApproval'] == 'PEN')
								{
								echo 'Pending';
								}
								if($data['ExecApproval'] == 'CAN')
								{
								echo 'Cancelled';
								}
								if($data['ExecApproval'] == 'REJ')
								{
								echo 'Rejected';
								}
								if($data['ExecApproval'] == 'SET')
								{
								echo 'Settled';
								}
								// -- Defaulter - 20.06.2018.
								if($data['ExecApproval'] == 'DEF')
								{
								echo 'Defaulter';
								}
								
								// -- Legal - 07.11.2018.
								if($data['ExecApproval'] == 'LEGAL')
								{
								echo 'Legal';
								}
								
								// -- Rehabilitation - 07.11.2018.
								if($data['ExecApproval'] == 'REH')
								{
								echo 'Rehabilitation';
								}
		?>
		</div>
	</div>
	</td>
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Property Applied for</strong></label>
		<div class="controls">
		    <?php 
					//2. ---------------------- BOC - Provinces ------------------------------- //
		// 15.04.2017 - 
		  $sql = 'SELECT * FROM rentalproperty WHERE id = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['property']));
		  $dataProperty = $q->fetch(PDO::FETCH_ASSOC);						   						 
		  $propertyText = $dataProperty['rentalpropertyname'].' '.$dataProperty['rentalpropertytype'].' '.$dataProperty['rentalpropertyprice'].' '.$dataProperty['rentalpropertycity'];
			echo  $propertyText;
			?>
		</div>
	</div>
	</td>
</tr>	
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
$rejectreason = $data['rejectreason'];
if ($data['ExecApproval'] == 'REJ') 
{ 	
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div> 
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>						
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						  <a class="btn btn-primary" href="rentalapplicatonslist">Back</a>
<!-- 13.06.2018 - Debit Order Schedule -->						
		<span></span>						 
						<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
						 <?php 
						 	if(isset($_SESSION))
							{
								$role = $_SESSION['role'];
							}
	
						 // -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);
$AccountHolder	 = $data['accountholdername'];
$BranchCode	 = $data['branchcode'];
$AccountNumber	 =  $data['accountnumber'];	
$Servicetype	 = 'Debit Order';
$ServiceMode	 = 'TWO DAY';
$AccountType	 = $dataAccountType['accounttypeid'];//$data['accounttype'];
$fname 			 = $data['FirstName'];
$lname 			 = $data['LastName'];
//$frequency 		 = $data['paymentfrequency'];
// Show if the role is administrator
	if($role == 'admin')
	{
$href = "'"."href=debitorderschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency&fname=$fname&lname=$lname"."'";
						 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="debitorderschedule('.$href.')">Generate Payments</button>';
	}
						 ?>						
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>
									
								</div> 
								</br> -->
											<div id="divdebitorderschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Debit Order Schedules</h1>
											</div>
							<!-- <div id="divSuccess">
								
								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>	
						</div>
					  </td>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
<!-- 13.06.2018 - Debit Order Schedule -->						
	<?php 
	$ApplicationId = $data['rentalappid'];
	$customerid = $data['CustomerId'];
	
	// -- if no role mean user never logged in go back to login page.
	if (empty($role)) 
	{
			header("Location: customerLogin");
	}
	
	// -- end of login check.
	
	// Only show contract if eloan application has been approved.
	if($data['ExecApproval'] == 'APPR' && $role == 'renter')
	{
				$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=rentalcontract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded>Download Lease Agreement</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.
	} 
	echo $role;
	
	// Show if the role is administrator
	if($role == 'Administrator')
	{
		$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=rentalcontract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Download Lease Agreement</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.

	}
	
	?>
						
						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php 
	$ApplicationId = $data['rentalappid'];
	$customerid = $data['CustomerId'];
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
	echo "<a class='btn btn-primary' href=rentalapplicationuploads?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>					  
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
						</table>
									</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>
   
   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php 
 
    // $pdo = Database::connectDB();
	 $tbl_name="rentalapp"; // Table name 
	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	 $check_user = "SELECT * FROM $tbl_name WHERE customerid= ? and rentalappid=?";
	 
	 // -- ID Document
	 $iddocument = '';
	 
	 // -- Contract Agreement
	 $contract = '';
	 
	 // -- Bank statement
	 $bankstatement = '';
	 
	 // -- Proof of income
	 $proofofincome = '';
	 
	 // -- Fica
	 $fica = '';
	 
	 $consentform = '';
	 $other = '';
   	 $other2 = '';	
	 // -- 

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.	 
	 $debitform = '';
	 $other3    = "";
	 $other4    = "";
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

	 
	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);
	
	
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$row = $q->fetch(PDO::FETCH_ASSOC);
		$count = $q->rowCount();
	
	if($count >= 1)
    {  

	//$row = mysql_fetch_array($result);
	
	
	$iddocument  =  $row['copyidFile']; 
	$contract = $row['FILECONTRACT'];
	$bankstatement = $row['FILEBANKSTATEMENT'];
	$proofofincome = $row['payslipFile'];
	$fica = $row['FILEFICA'];
	
   // -- BOC 01.10.2017 - Upload Credit Reports.
   	$creditreport = $row['FILECREDITREP'];	
	
	$consentform = $row['FILECONSENTFORM'];
	$other = $row['FILEOTHER'];
   	$other2 = $row['FILEOTHER2'];	
	$NoFIle = "";
   // -- EOC 01.10.2017 - Upload Credit Reports.
   
   	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.	 
	 $debitform = $row['FILEDEBITFORM'];
	 $other3    = $row['FILEOTHER3'];
	 $other4    = $row['FILEOTHER4'];
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

   
   // -- BOC 01.10.2017 - Alternative if no file upload.
   if(empty($iddocument)){$iddocument = $NoFIle;}
   if(empty($contract)){ $contract = $NoFIle;}
   if(empty($bankstatement)){$bankstatement = $NoFIle;}
   if(empty($proofofincome)){$proofofincome = $NoFIle;}
   if(empty($fica)){ $fica = $NoFIle;}
   if(empty($creditreport)){  $creditreport = $NoFIle;}
   if(empty($consentform)){  $consentform = $NoFIle;}
   if(empty($other)){  $other = $NoFIle;}
   if(empty($other2)){  $other2 = $NoFIle;}

   if(empty($debitform)){$debitform = $NoFIle;}
   if(empty($other3)){  $other3 = $NoFIle;}
   if(empty($other4)){  $other4 = $NoFIle;}
   // -- EOC 01.10.2017 - Alternative if no file upload.
   
    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";
	
	$iddocumentHTML = '';
	if($iddocument == '')
	{
		$iddocumentHTML = "<tr>
         <td><a href='$iddocument' target='_blank'>$iddocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	else
	{
		$iddocumentHTML = "<tr>
         <td><a $classCSS href='$iddocument' target='_blank'>$ViewDocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	$contractHTML = '';
	if($contract == '')
	{
		$contractHTML = "<tr>


         <td><a href='uploads/$contract' target='_blank'>$contract</a></td>
         <td>Contract Agreement</td>
      </tr>
      ";
	}
	else
	{
		$contractHTML = "

      <tr>
         <td><a $classCSS href='uploads/$contract' target='_blank'>$ViewDocument</a></td>
         <td>Contract Agreement</td>
      </tr>";
	}
	
	$bankstatementHTML = '';
	if($bankstatement == '')
	{
		$bankstatementHTML = "<tr>
         <td><a href='uploads/$bankstatement' target='_blank'>$bankstatement</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	else
	{
		$bankstatementHTML = "<tr>
         <td><a $classCSS href='uploads/$bankstatement' target='_blank'>$ViewDocument</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	
	$proofofincomeHTML = '';
	if($proofofincome == '')
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a href='$proofofincome' target='_blank'>$proofofincome</a></td>
         <td>Proof of income</td>
      </tr>
	  ";
	}
	else
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a $classCSS href='$proofofincome' target='_blank'>$ViewDocument</a></td>

         <td>Proof of income</td>
      </tr>
	  ";
	}
	$ficaHTML = '';
	if($fica == '')
	{
		$ficaHTML = "

	  <tr>
         <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	else
	{
		$ficaHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$fica' target='_blank'>$ViewDocument</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	$creditreportHTML = '';
	if($creditreport == '')
	{
		$creditreportHTML = "
	  <tr>
         <td><a href='uploads/$creditreport' target='_blank'>$creditreport</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	else
	{
		$creditreportHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$creditreport' target='_blank'>$ViewDocument</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	$consentformHTML = '';
	if($consentform == '')
	{
		$consentformHTML = "
	  <tr>
         <td><a href='uploads/$consentform' target='_blank'>$consentform</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	else
	{
		$consentformHTML = "
	  <tr>
         <td><a  $classCSS href='uploads/$consentform' target='_blank'>$ViewDocument</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	$otherHTML = '';
	if($other == '')
	{
		$otherHTML ="
	  <tr>
         <td><a href='uploads/$other' target='_blank'>$other</a></td>
         <td>Other</td>
      </tr>
	";
	}
	else
	{
		$otherHTML ="
	  <tr>
         <td><a $classCSS href='uploads/$other' target='_blank'>$ViewDocument</a></td>
         <td>Other</td>
      </tr>
	";
	}
	$other2HTML = '';
	if($other2 == '')
	{
		$other2HTML = "
  	  <tr>
         <td><a href='uploads/$other2' target='_blank'>$other2</a></td>
         <td>Other 2</td>
      </tr>
";
	}
	else
	{
		$other2HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other2' target='_blank'>$ViewDocument</a></td>
         <td>Other 2</td>
      </tr>";
	}

	
	$debitformHTML = '';
	if($debitform == '')
	{
		$debitformHTML = "
  	  <tr>
         <td><a href='uploads/$debitform' target='_blank'>$debitform</a></td>
         <td>Debit Form</td>
      </tr>
";
	}
	else
	{
		$debitformHTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$debitform' target='_blank'>$ViewDocument</a></td>
         <td>Debit Form</td>
      </tr>";
	}

	$other3HTML = '';
	if($other3 == '')
	{
		$other3HTML = "
  	  <tr>
         <td><a href='uploads/$other3' target='_blank'>$other3</a></td>
         <td>Other 3</td>
      </tr>
";
	}
	else
	{
		$other3HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other3' target='_blank'>$ViewDocument</a></td>
         <td>Other 3</td>
      </tr>";
	}

	$other4HTML = '';
	if($other4 == '')
	{
		$other4HTML = "
  	  <tr>
         <td><a href='uploads/$other4' target='_blank'>$other4</a></td>
         <td>Other 4</td>
      </tr>
";
	}
	else
	{
		$other4HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other4' target='_blank'>$ViewDocument</a></td>
         <td>Other 4</td>
      </tr>";
	}

	
   echo "<tbody>"
      .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$consentformHTML.$creditreportHTML.$otherHTML.$other2HTML.$debitformHTML.$other3HTML.$other4HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->   
</table>								
 <!------------------------------ End File List ------------------------------------>
	
<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------> 	
					</form>			
						
				
                            <!-- End Login Box -->							
                        </div>
					
                </div>
            <!-- === END CONTENT === -->