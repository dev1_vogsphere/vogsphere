<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="assets/js/jquery.tableToExcel.js"></script>

<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="sentcomm_history"; // Table name
 $bankid = '';
 $From = date("Y-m-d");
 $To = '';
 if($To == ""){$To = "9999-12-31";}
 $customerid = '';
 $msgtype = '';

 // -- Call Communications...
 $tablename  = 'sentcomm_history';
 $tablename2 = 'api_response'; // 21/12/2021

 $dbnameobj = 'ecashpdq_eloan';
 $queryFields = null;
 $queryFields[] = '*';

 $whereArray  = null;

 $All = '';
 $email = '';
 $sms = '';

 // -- Count...
 $count2 = 0;
 $count  = 0;
 $data2  = null;

$IntUserCode = '';

// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');}
//echo $IntUserCode;

 include 'database.php';
/*
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					if($_SESSION['role'] != 'customer')
					{
						if ( !empty($_POST))
						   {
							 $customerid = $_POST['customerid'];
							 $From = getpost("From");
							 $To = getpost("To");
						   }
					}
					   $sql = "";
					   $q = "";

/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////
					// -- if($_SESSION['role'] == 'admin')
					if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'superuser')
					{
						if(!empty($_POST))
						   {
							 $customerid = $_POST['customerid'];
							 $From = getpost("From");
							 $To = getpost("To");
						   }
					 // -- both were.. 18.12.2018
					  if(!empty($customerid) && !empty($From))
					  {
						$sql = "SELECT * FROM $tbl_name WHERE customerid = ? AND sentcomm_historydate = ? ORDER BY sentcomm_historydate DESC,sentcomm_historytime ASC";
						$q = $pdo->prepare($sql);
					    $q->execute(array($customerid,$From));
						$data = $q->fetchAll();
					  }
					// -- Search by date only.
					  elseif(!empty($From))
					  {
						$sql = "SELECT * FROM $tbl_name WHERE sentcomm_historydate = ? ORDER BY sentcomm_historydate DESC,sentcomm_historytime ASC";
						$q = $pdo->prepare($sql);
					    $q->execute(array($From));
						$data = $q->fetchAll();
					  }
					// -- customerid only.
					  elseif(!empty($customerid))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE customerid = ? ORDER BY sentcomm_historydate DESC,sentcomm_historytime ASC";
						$q = $pdo->prepare($sql);
					    $q->execute(array($customerid));
						$data = $q->fetchAll();

					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name ORDER BY sentcomm_historydate DESC,sentcomm_historytime ASC";
						$q = $pdo->prepare($sql);
					    $q->execute();
						$data = $q->fetchAll();
						}
					}*/
		if($_SESSION['role'] != 'admin') // ---- admin..
		{
			$text_readonly = '';
			$customerid    = $_SESSION["username"];
			//$customerid    = '"'.$_SESSION["username"].'"';
		}
//$sql = "SELECT a.*,b.email,account as usercode FROM ecashpdq_eloan.api_response as a left join user as b on a.createdby = b.alias where account = {$customerid} ";
	//echo 	$sql;
if($_SESSION['role'] != 'customer')
{
	if(!empty($_POST))
	{
		$whereArr = null;
		$queryFields = null;
		//$queryFields[] = '*';
		
		$queryFields[] = 'sentcomm_historytype';
		$queryFields[] = 'sentcomm_historydate';
		$queryFields[] = 'email';
		$queryFields[] = 'status';
		$queryFields[] = 'userid';
		$queryFields[] = 'costcenter';
		$queryFields[] = 'customerid';
		//$queryFields[] = 'message';
		
		
		

if($_SESSION['role'] != 'admin') // -- dont show 'support@ecashmeup.com' if is not admin.. 09/01/2022
{
	$whereArr[] = "email NOT IN ('support@ecashmeup.com')";
}

		$From = getpost("From");
		$To = getpost("To");
		$customerid = getpost("customerid");
		$msgtype = getpost("msgtype");

		//if(!empty($customerid) && $customerid != ''){$whereArr[] = "customerid = ".'"'.$customerid.'"'."";}

		$lvmsgtype = $msgtype;
		if(!empty($customerid) && $customerid != ''){$whereArr[] = "customerid = ".'"'.$customerid.'"'."";}
		if(!empty($msgtype) && $msgtype != '*'){$msgtype = "'".$msgtype."'";$whereArr[] = "sentcomm_historytype = {$msgtype}";}
		$msgtype = $lvmsgtype;
	  //echo '$customerid = '.$customerid;

		$lvfrom = $From;
		if(!empty($From)){$From = "'".$From."'"; $whereArr[] = "sentcomm_historydate >= {$From}"; }
		$From = $lvfrom;

		$lvto = $To;
		if(!empty($To)){$To = "'".$To."'";$whereArr[] = "sentcomm_historydate <= {$To}";}
		$To = $lvto;

		$To = getpost("To");
		$From = getpost("From");
		$msgtype = getpost("msgtype");
		$data = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);;
    $count  = $data->rowCount();
		// -- Data from api response table. 21/12/2021
		if($msgtype != 'email')
		{
			//echo 'ke tsene';
		$whereArr = null;
		if(!empty($customerid) && $customerid != ''){$whereArr[] = "usercode = ".'"'.$customerid.'"';}
		$lvfrom = $From;
		if(!empty($From)){$From = "'".$From."'"; $whereArr[] = "date >= {$From}"; }
		$From = $lvfrom;

		$lvto = $To;
		if(!empty($To)){$To = "'".$To."'";$whereArr[] = "date <= {$To}";}
		$To = $lvto;
		
		// -- peformance... 03/03/2022
		$queryFields = null;
		$queryFields[] = '*';

		$data2 = search_dynamic($dbnameobj,$tablename2,$whereArr,$queryFields);

		// -- Merge two arrays..
		//print_r($data );
		$count  = $data->rowCount();
		$count2 = $data2->rowCount();
		//echo '$count2  = '.$count2.'<br/>';
if(empty($count2))
{
	$sql = "SELECT a.*,b.email,account as usercode FROM ecashpdq_eloan.api_response as a left join user as b on a.createdby = b.alias where account = '{$customerid}' AND date >= '{$From}' AND date <= '{$To}'";
	$q = $pdo->prepare($sql);
	$q = $pdo->query($sql);
	$data2 = $q ;
	$count2 = $data2->rowCount();
	//echo '$count2  = '.$count2.$customerid;

}
		//print("Retrieved $count2 rows.\n");

		/* if($count <= 0)
		 {
			$data =  $data2;
		 }
		 elseif($count2 > 0)
		 {
			 $data = array_merge($data, $data2);
		 }*/

		}
	}
	else
	{
		$whereArr = null;
		$queryFields = null;
		$queryFields[] = '*';
if($_SESSION['role'] != 'admin') // -- dont show 'support@ecashmeup.com' if is not admin.. 09/01/2022
{
	$whereArr[] = "email NOT IN ('support@ecashmeup.com')";
}
		$lvmsgtype = $msgtype;
		if(!empty($customerid) && $customerid != ''){$whereArr[] = "customerid = ".'"'.$customerid.'"';}
		if(!empty($msgtype) && $msgtype != '*'){$msgtype = "'".$msgtype."'";$whereArr[] = "sentcomm_historytype = {$msgtype}";}
		$msgtype = $lvmsgtype;

//echo '$customerid = '.$customerid;
		$lvfrom = $From;
		if(!empty($From)){$From = "'".$From."'"; $whereArr[] = "sentcomm_historydate >= {$From}"; }
		$From = $lvfrom;

		$lvto = $To;
		if(!empty($To)){$To = "'".$To."'";$whereArr[] = "sentcomm_historydate <= {$To}";}
		$To = $lvto;

		$data = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);

		// -- Data from api response table. 21/12/2021
		if($msgtype != 'email')
		{
		$whereArr = null;
		if(!empty($customerid) && $customerid != ''){$whereArr[] = "usercode = ".'"'.$customerid.'"';}
		$lvfrom = $From;
		if(!empty($From)){$From = "'".$From."'"; $whereArr[] = "date >= {$From}"; }
		$From = $lvfrom;

		$lvto = $To;
		if(!empty($To)){$To = "'".$To."'";$whereArr[] = "date <= {$To}";}
		$To = $lvto;

		$data2 = search_dynamic($dbnameobj,$tablename2,$whereArr,$queryFields);

		// -- Merge two arrays..
		$count  = $data->rowCount();
		$count2 = $data2->rowCount();
		//echo '$count2  = '.$count2;
		if(empty($count2))
		{
	$sql = "SELECT a.*,b.email,account as usercode FROM ecashpdq_eloan.api_response as a left join user as b on a.createdby = b.alias where account = '{$customerid}' AND date >= '{$From}' AND date <= '{$To}'";
	//echo $sql;
	$q = $pdo->prepare($sql);
	$q = $pdo->query($sql);
	$data2 = $q ;
	$count2 = $data2->rowCount();
	//echo '$count2  = '.$count2.$customerid;
	}

		}
	}
}
if($msgtype == '*')
{
  $All = 'selected';
}

if($msgtype == 'email')
{
  $email = 'selected';
}

if($msgtype == 'sms')
{
	$sms = 'selected';
}

// -- BOC Search by Usercode as admin. 09/01/2022
// -- Database Clients:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM client';
$dataclient = $pdo->query($sql);

$datasrUserCodeArr = array();
if($_SESSION['role'] == 'admin')
{
foreach($dataclient as $row)
{
	$datasrUserCodeArr[] = $row['username']."|".$row['Description'];
}
// -- Database Client

// -- Debicheck UserCode -- //
$sql = 'SELECT * FROM client WHERE debicheckusercode <> username';
$dataclientDebicheck = $pdo->query($sql);
foreach($dataclientDebicheck as $row)
{
	$datasrUserCodeArr[] = $row['debicheckusercode']."|".$row['Description'];
}
}
// -- EOC Search by Usercode as admin. 09/01/2022

//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "<div class='container background-white bottom-border'>";
$text_readonly = '';
//<!-- Search Login Box -->
  if($_SESSION['role']!= 'customer')
	{
		if($_SESSION['role'] != 'admin') // ---- admin..
		{
			$text_readonly = '';
			$customerid    = '"'.$_SESSION["username"].'"';
		}
		?>
	<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class="text-center">Communications</h2>
                                    </div>
                                   <div class='input-group margin-bottom-20' <?php if($_SESSION['role'] == 'admin')
                                  {
                                    echo "style='visibility:hidden';'display:none';";
                                  } ?>>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='customerid' name='customerid'
										placeholder='Customer ID' class='form-control' type='text' readonly='<?php ?>' value=<?php echo $customerid;?>>
									</div>
<!-- 09/01/2022  Search by customer code -->

<div class='input-group margin-bottom-30' <?php if($_SESSION['role'] != 'admin') {echo "style='visibility:hidden';'display:none';";} ?>>
<span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
		   <SELECT class="form-control" style="width:100%;height:30px;z-index:0;" class="form-control" id="customerid" name="customerid" size="1" style='z-index:0'>
				<!-- <OPTION value='' <?php echo $Allstatus;?>>All UserCodes</OPTION> -->
			    <?php
					$datasrUserCodeSelect = $customerid;
					foreach($datasrUserCodeArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $datasrUserCodeSelect)
					  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';}
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
					}
				?>
			</SELECT>
</div>
<!-- -09/01/2022 -->


									<div class="control-group">
										<div class='input-group margin-bottom-20'>
										 <span class='input-group-addon'>
											<i class='fa-caret-down'></i>
										 </span>
										<SELECT class="form-control" id="msgtype" name="msgtype" size="1" style='z-index:0'>
										<!-------------------- BOC 2017.04.15 -  Payment Method --->
											<OPTION value='*' <?php echo $All;?>>All Types</OPTION>
											<OPTION value='email' <?php echo $email;?>>Email</OPTION>
											<OPTION value='sms' <?php echo $sms;?>>SMS</OPTION>
										<!-------------------- EOC 2017.04.15 - Payment Method. --->
										</SELECT>
										</div>
									</div>

                                    <div class='input-group margin-bottom-20' style="z-index:0">
										<span class='input-group-addon'>
											<i class='fa fa-calendar'></i>
										</span>
                                        <input style='height:30px;z-index:0' id='From' name='From' placeholder='Date From' class='form-control' type='Date' value=<?php echo $From;?>>
                                    </div>

                                    <div class='input-group margin-bottom-20'>
										<span class='input-group-addon'>
											<i class='fa fa-calendar'></i>
										</span>
                                        <input style='height:30px;z-index:0' id='To' name='To' placeholder='Date To' class='form-control' type='Date' value=<?php echo $To;?>>
                                    </div>

                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
	<?php } ?>
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->

    <div class="row">

      <p><button class="btn btn-primary" onclick="$('table').tblToExcel();"><i class="fa fa-download"></i> Excel</button> <span class="badge"> Emails <?php echo $count;?></span> <span class="badge"> SMS <?php echo $count2;?></span></p>

    </div>

  	<div class="row">
				  <div class="table-responsive">
					<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
		              <?php
						//echo "<table class='table table-striped table-bordered'>";
						echo "<tr>";
						//echo "<td><b>Count</b></td>";
						//echo "<td><b>Type</b></td>";
						echo "<td><b>Date</b></td>";
						//echo "<td><b>Time</b></td>";
						//echo "<td><b>Customer ID</b></td>";
						//echo "<td><b>Reference</b></td>";
						echo "<td colspan='2'><b>Email</b></td>";
						echo "<td><b>Phone</b></td>";
						echo "<td><b>Status</b></td>";
						echo "<td><b>Sent by</b></td>";
						echo "<td><b>User Code</b></td>";
						echo "<th>Message Content</th>";
						echo "</tr>";
						$countA 	= 1;
						$countSMS 	= 1;
						$countEmail = 1;
						$msgid 		= '';
						foreach($data as $row)
						{
						// BOC -- 09/01/2022
						// -- exclude support@ecashmeup.com	 from list if not 'admin'
						if($_SESSION['role'] != 'admin' && $row['email'] == 'support@ecashmeup.com')
						{
							// -- Do nothing..
						}
						else{
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
						echo "<tr>";
						//echo "<td valign='top'>".$countA."</td>";
						//echo "<td valign='top'>" . nl2br( $row['sentcomm_historytype']) . "</td>";
						if($row['sentcomm_historytype'] == 'email')
						{$countEmail = $countEmail + 1;}else{$countSMS = $countSMS + 1;}

						echo "<td valign='top'>" . nl2br( $row['sentcomm_historydate']) . "</td>";
						//echo "<td valign='top'>" . nl2br( $row['sentcomm_historytime']) . "</td>";
						//echo "<td valign='top'>" . nl2br( $row['customerid']) . "</td>";
						//echo "<td valign='top'>" . nl2br( $row['refnumber']) . "</td>";
						echo "<td valign='top' colspan='2'>" . nl2br( $row['email']) . "</td>";
						//echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";
            echo "<td valign='top'>" . " ";
						echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";
						echo "<td valign='top'>" . nl2br( $row['userid']) . "</td>";
						if(!empty($row['costcenter']))
						{echo "<td valign='top'>" . nl2br( $row['costcenter']) . "</td>";}
						else
						{echo "<td valign='top'>" . nl2br( $row['customerid']) . "</td>";}

						//03/03/2022 - $mess = "'".$row['message']."'";//nl2br("(".$row['message'].")");
						$mess = "";
						$mess = str_replace('</body></html>',"",$mess);
						$mess = str_replace("<!-- Forcing initial-scale shouldn't be necessary -->","",$mess);
						$mess = htmlspecialchars($mess);
						$mess = str_replace(array("\n", "\r"), '', $mess);
						//print($mess);
						$msgid = "'".'mess'.$countA."'";
						echo "<td valign='top'>";// . nl2br( $row['message']) . "</td>";
								// BOC -- Get the Details Information.14.05.2017
						echo '<button type="button" class="btn btn-info float-right" data-popup-open="popup-1" onclick="content('.$msgid.')">View Message</button>';
								// EOC -- Get the Details Information.14.05.2017
            echo "<input style='height:30px' id=$msgid name=$msgid placeholder='Customer ID' class='form-control' type='hidden' value=$mess >";

            echo "</td>";

				//	echo "<td valign='top'>";
				//	echo "</td>";
        //  echo "</tr>";
						$countA = $countA + 1;
						  } // EOC -- 09/01/2022
						}
					// -- Search Login Box --
						if($count2 > 0)
						{
							//$countA = 0;
							foreach($data2 as $row)
							{
								foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
								echo "<tr>";
							//	echo "<td valign='top'>".$countA."</td>";
								//echo "<td valign='top'>" . nl2br('sms') . "</td>";
							  echo "<td valign='top'>" . nl2br( $row['date']) . "</td>";
								//echo "<td valign='top'>" . nl2br( $row['date']) . "</td>";
								//echo "<td valign='top'>" . nl2br( $row['usercode']) . "</td>";
								//echo "<td valign='top' colspan='2'>" . nl2br( $row['usercode']) . "</td>";
                echo "<td valign='top' colspan='2'>" . " ";
								echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";
								echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";
								echo "<td valign='top'>" . nl2br( $row['createdby']) . "</td>";
								echo "<td valign='top'>" . nl2br( $row['usercode']) . "</td>";
								
								//03/03/2022 - $mess = "'".$row['message']."'";//nl2br("(".$row['message'].")");
								$mess = "";
								$mess = str_replace('</body></html>',"",$mess);
								$mess = str_replace("<!-- Forcing initial-scale shouldn't be necessary -->","",$mess);
								$mess = htmlspecialchars($mess);
								$mess = str_replace(array("\n", "\r"), '', $mess);
								//print($mess);
								$msgid = "'".'mess'.$countA."'";
						echo "<td valign='top'>";// . nl2br( $row['message']) . "</td>";
								// BOC -- Get the Details Information.14.05.2017
						echo '<button type="button" class="btn btn-info float-right" data-popup-open="popup-1" onclick="content('.$msgid.')">View Message</button>';
								// EOC -- Get the Details Information.14.05.2017
            echo "<input style='height:30px' id=$msgid name=$msgid placeholder='Customer ID' class='form-control' type='hidden' value=$mess >";

					echo "</td>";

					//	echo "<td valign='top'>";
					//	echo "</td>";
						//echo "</tr>";
						$countA = $countA + 1;

							}
						}

						echo "</table>";

	 				  /*foreach ($data as $row)
					  {
					  }*/
					   Database::disconnect();
					  ?>
			<!-- BOC - Communications -->
					 <div class="popup" data-popup="popup-1" style="overflow-y:auto;">
						<div class="popup-inner">
										<div id="divContent" style="top-margin:100px;font-size: 10px;">
										</div>
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>
					 </div>
					</div>
			<!-- EOC - Communications -->
				</div>

    	</div>
</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->

<script>

function content(msgid)
{
	    var divContent = document.getElementById(msgid).value;
		divContent = htmlEntities(divContent);
		//alert(divContent);
		document.getElementById('divContent').innerHTML = divContent;
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");
		  var width = 10;
        elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';*/

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';
    }
  }
}

function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}
</script>
