<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="currency"; // Table name 
 $currencyid = '';
  
// -- echo "<div class='container background-white'>";
 echo "<div class='container background-white bottom-border'>";		
  
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search for Currency</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-dollar'></i>
                                        </span>
                                        <input style='height:30px' id='currencyid' name='currencyid' placeholder='Currency Symbol' class='form-control' type='text' value=$currencyid>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $currencyid = $_POST['currencyid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($currencyid))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE currencyid = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($currencyid));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<a class='btn btn-info' href=newcurrency.php>Add New Currency</a><p></p>"; 

						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Currency Symbol</b></td>"; 
						echo "<td><b>Currency Description</b></td>";
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
				
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['currencyid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['currencyname']) . "</td>";  
						echo "<td valign='top'><a class='btn btn-success' href=editcurrency.php?currencyid={$row['currencyid']}>Edit</a></td><td>
						<a class='btn btn-danger' href=deletecurrency.php?currencyid={$row['currencyid']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->