<script>
function UpdateEmailTemplate()
{
// -- Email Template.
	var val1 = document.getElementById("smstemplatecontent").value;  
// -- alert("Onload = "+val1);

	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = document.getElementById("smstemplatecontent").value;
		//alert(val1);
		jQuery("#txtEditor").Editor("setText", val1);
				
	});	
	
	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = jQuery("#txtEditor").Editor("getText");
		
		document.getElementById("smstemplatecontent").value = val1;		
	});
} 

</script>
<?php 

require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="smstemplate"; // Table name 

//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM placeholder';
$dataPlaceholders = $pdo->query($sql);						   						 
   
$count = 0;

$placeholder		=	'';
$smstemplatename	=	'';
$smstemplatecontent	=	'';

// -- Errors
$placeholderError		    =	'';
$smstemplatenameError		=	'';
$smstemplatecontentError	=	'';
$valid = true;
$smstemplateid  = '';
$message = '';

if (isset($_GET['smstemplateid']) ) 
  { 
	$smstemplateid = $_GET['smstemplateid']; 
  }
  
$subject 	  = '';
$subjectError = '';

if (!empty($_POST)) 
{   $count = 0;

$placeholder		=	$_POST['placeholder'];
$smstemplatename	=	$_POST['smstemplatename'];
$smstemplatecontent	=	$_POST['smstemplatecontent'];
$subject	=	$_POST['subject'];

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if (empty($smstemplatename)) { $smstemplatenameError = $smstemplatenameError.'Please enter sms template name'; $valid = false;}
	
	if (empty($subject)) { $subjectError = $subjectError.'Please enter email template subject'; $valid = false;}

	if (empty($smstemplatecontent)) { $smstemplatecontentError = $smstemplatecontentError.'Please enter sms template content'; $valid = false;}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE smstemplate SET smstemplatename = ?, smstemplatecontent = ?,subject = ? WHERE smstemplateid = ?";
			$q = $pdo->prepare($sql);
			//$smstemplatecontent =''.$smstemplatecontent.'' ;
			$q->execute(array($smstemplatename,$smstemplatecontent,$smstemplateid,$subject));
			Database::disconnect();
			$count = $count + 1;
	}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from smstemplate where smstemplateid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($smstemplateid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		$smstemplatename = $data['smstemplatename'];
		$smstemplatecontent = $data['smstemplatecontent'];
		$subject = $data['subject'];
		$_SESSION['Editor'] = $smstemplatecontent;
}

?>
<!--
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="emailtemplates.php">Email Templates</a></li>
		</ol>
	</div>
</div>-->
<div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
	  <div class="row">
		<form name='form1' action='' method='POST' class="signup-page">   
<?php # error messages
		/*if (isset($message)) 
		{
			{
				printf("<p class='status'>%s</p></ br>\n", $message);
			}
		}*/
		# success message
		if($count !=0)
		{
			printf("<p class='status'>emailtemplate  successfully updated.</p>");
		}
?>
			<p><b>emailtemplatename</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='smstemplatename' id="smstemplatename" value="<?php echo !empty($smstemplatename)?$smstemplatename :'';?>" readonly />
			<?php if (!empty($smstemplatenameError)): ?>
			<span class="help-inline"><?php echo $smstemplatenameError;?></span>
			<?php endif; ?>
			</p>	
			<!-- 2. Email Subject Template -->
		<div class="form-group <?php if (!empty($subjectError)){ echo "has-error";}?>">						
			<p><b>Subject</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='subject' id="subject" value="<?php echo !empty($subject)?$subject :'';?>"/>
			<?php if (!empty($subjectError)): ?>
			<small class="help-block"><?php echo $subjectError;?></small>
			<?php endif; ?>
			</p>
		</div>	
			<p><b>Placeholders</b><br />
				<SELECT class="form-control" id="placeholder" name="placeholder" size="1">
					<?php
						$placeholderLoc = '';
						$placeholderSelect = $placeholder;
						foreach($dataPlaceholders as $row)
						{
							$placeholderLoc = $row['placeholder'];

							if($placeholderLoc == $placeholderSelect)
							{
								echo "<OPTION value='".$placeholderLoc."' selected>$placeholderLoc</OPTION>";
							
							}
							else
							{
								echo "<OPTION value='".$placeholderLoc."'>$placeholderLoc</OPTION>";
							}
						}
												
						if(empty($dataPlaceholders))
						{
							echo "<OPTION value=0>No placeholders</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			<div class="form-group <?php if (!empty($smstemplatecontentError)){ echo "has-error";}?>">		
				<img src="w3html.jpg" onload="UpdateEmailTemplate()" width="100" height="132" style="display:none">
			<p>
					Click to add <input onclick='input()' type='button' value='Place holder' id='button'><br>
					<textarea class="form-control" rows="10" name='smstemplatecontent' id="smstemplatecontent" style="display:none"><?php echo !empty($smstemplatecontent )?$smstemplatecontent :'';?></textarea>
					<?php if (!empty($smstemplatecontentError)): ?>
							<small class="help-block"><?php echo $smstemplatecontentError;?></small>
					<?php endif; ?>
			</p>
			</div>	
			<div class="container-fluid">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 nopadding">
								<textarea id="txtEditor" name="txtEditor"><?php echo $smstemplatecontent;?></textarea> 
								<!-- <textarea id="smstemplatecontent" name="smstemplatecontent" $smstemplatecontent2.$smstemplatename style="display:none"></textarea> -->	
							</div>
						</div>
					</div>
				</div>
			</div>			
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'emailtemplates';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			//echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				//<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
		     </div>
			   <button class='btn btn-primary' type='submit'>Save</button><!-- Generate OTP -->
  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->

			 </td>
		</tr>
		</table>	
		</form> 
	</div>
</div>		
</div>
<script>
function input()
{
	var title = document.getElementById('placeholder').value;
	var area = document.forms.form1.smstemplatecontent.value;
    document.forms.form1.smstemplatecontent.value = area + title;
	UpdateEmailTemplate();
}    

function UpdateEmailTemplate()
{
// -- Email Template.
	var val1 = document.getElementById("smstemplatecontent").value;  
			alert("Onload = "+val1);

	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = document.getElementById("smstemplatecontent").value;
		//alert(val1);
		jQuery("#txtEditor").Editor("setText", val1);
				
	});	
	
	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = jQuery("#txtEditor").Editor("getText");
		
		document.getElementById("smstemplatecontent").value = val1;		
	});
} 

</script>