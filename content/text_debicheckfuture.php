<?php
/*Rules
The following only fields that can be updated:
1	Tracking Indicator						$tracking_indicator
2	Frequency								$Frequency
3	Mandate Initiation Date					$mandate_initiation_date
4	First Collection Date					$first_collection_date
5	Maximum Collection Amount				$maximum_collection_amount
6	Debtor Account Name						$debtor_account_name
7	Debtor Identification                   $debtor_identification
8	Document Type                           $document_type
9	Debtor Account Number                   $debtor_account_number
10  Debtor Account Type                     $debtor_account_type
11  Debtor Branch Number                    $debtor_branch_number
12  Debtor Telephone Contact Details        $debtor_contact_number
13  Debtor Email Contact Details			$debtor_email
14  Collection Day							$collection_date
15  Date Adjustment Rule Indicator			$date_adjustment_rule_indicator
16  Adjustment Category						$adjustment_category
17  Adjustment Rate							$adjustment_rate
18  Adjustment Amount						$adjustment_amount
19  First Collection Amount					$first_collection_amount
20  Debit Value Type						$debit_value_type
*/
require 'database.php';

$hide = 'style="display:none"';
$show = 'style="display:block"';

// -- redirect if user has been logged off.
$customerLogin = 'customerLogin';

// -- its okay, navigate to login back.
if(empty(getsession('role')))
{
	redirect($customerLogin);
}

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM debtor_cancellation_codes';
$debtor_cancellation_codes = $pdo->query($sql);

$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);
$dataentry_class = array();

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_debitvalue_types';
$datadebtor_debitvalue_types = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_entry_class_codes';
$datadebtor_entry_class_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_collection_codes';
$datacollection_day_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_adjustment_category';
$datadebtor_adjustment_category = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_auth_codes';
$datadebtor_auth_code = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_instalment_occurrence';
$datadebtor_instalment_occurrence = $pdo->query($sql);

$notification = array();
$notificationsList = array();
// -- Declare -- //
$Frequency1					    = '';
$reference 						='';
$contract_reference 			='';
$tracking_indicator 			='';
$debtor_auth_code 				='';
$auth_type 						='';
$instalment_occurence1 			='';
$mandate_initiation_date 		='';
$first_collection_date 			='';
$collection_amount_currency 	='ZAR';
$collection_amount 				='';
$maximum_collection_currency 	='ZAR';
$maximum_collection_amount 		='';
$entry_class 					='';
$debtor_account_name 			='';
$debtor_identification 			='';
$debtor_account_number 			='';
$debtor_account_type 			='';
$debtor_branch_number 			='';
$debtor_contact_number 			='';
$debtor_email 					='';
$collection_date 				='';
$date_adjustment_rule_indicator  ='';
$adjustment_category 			='';
$adjustment_rate 				='';
$adjustment_amount_currency 	='ZAR';
$adjustment_amount 				='';
$first_collection_amount_currency = 'ZAR';
$first_collection_amount1 		='';
$debit_value_type 				='';
$cancellation_reason 			='';
$amended 						='';
$status_code 					='';
$status_desc 					='';
$document_type 					='';
$amendment_reason_md16 			='';
$amendment_reason_text 			='';
$tracking_cancellation_indicator ='';
// -- EOC Declare -- //
// -- dropdown selection.

$debtor_cancellation_codes_Arr = array();
foreach($debtor_cancellation_codes as $row)
{
	$debtor_cancellation_codes_Arr[] = $row['reason_code'].'|'.$row['reason_desc'];
}

$collection_day_Arr = array();
foreach($datacollection_day_codes as $row)
{
	$collection_day_Arr[] = $row['collection_day_code'].'|'.$row['collection_desc'].'|'.$row['frequency_code'];
}
$_SESSION['collection_day_Arr'] = $collection_day_Arr;

$Client_ID_TypeArr = array();
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}
$_SESSION['Client_ID_TypeArr'] = $Client_ID_TypeArr;

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}
$_SESSION['Account_TypeArr'] = $Account_TypeArr;

$tracking_indicatorArr[] = 'Y|Yes';
$tracking_indicatorArr[] = 'N|No';
$_SESSION['tracking_indicatorArr'] = $tracking_indicatorArr;

$date_adjustment_rule_indicatorArr[] = 'Y|Yes';
$date_adjustment_rule_indicatorArr[] = 'N|No';
$_SESSION['date_adjustment_rule_indicatorArr'] = $date_adjustment_rule_indicatorArr;

$instalment_occurenceArr = array();
foreach($datadebtor_instalment_occurrence as $row)
{
	$instalment_occurenceArr[] = $row['instalment_occurrence'].'|'.$row['instalment_occurrence_desc'];
}
$_SESSION['instalment_occurenceArr'] = $instalment_occurenceArr;

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}
$_SESSION['Branch_CodeArr'] = $Branch_CodeArr;

$FrequencyArr = array();
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}
$_SESSION['FrequencyArr'] = $FrequencyArr;

$debtor_auth_codeArr = array();
foreach($datadebtor_auth_code as $row)
{
	$debtor_auth_codeArr[] = $row['debtor_auth_code'].'|'.$row['debtor_auth_code'].' - '.$row['debtor_auth_type'];
}
$_SESSION['debtor_auth_codeArr'] = $debtor_auth_codeArr;

$debit_value_typeArr = array();

foreach($datadebtor_debitvalue_types as $row)
{
	$debit_value_typeArr[] = $row['debtor_debitvalue_type'].'|'.$row['debitvalue_desc'];
}
$_SESSION['debit_value_typeArr'] = $debit_value_typeArr;

$entry_classArr = array();
foreach($datadebtor_entry_class_codes as $row)
{
	$entry_classArr[] = $row['debtor_entry_class'].'|'.$row['debtor_entry_class_desc'];
}
$_SESSION['entry_classArr'] = $entry_classArr;

$datadebtor_adjustment_categoryArr = array();
foreach($datadebtor_adjustment_category as $row)
{
	$datadebtor_adjustment_categoryArr[] = $row['category'].'|'.$row['category_desc'];
}
$_SESSION['datadebtor_adjustment_categoryArr'] = $datadebtor_adjustment_categoryArr;

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$changedby = getsession('username');
$IntUserCode = getsession('IntUserCode');
$customerid = '';

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
	  $statusList = "('pending','')";
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  if(isset($_SESSION['debicheckusercode']))
    {

    	$Client_Id=$_SESSION['debicheckusercode'];
     //echo $clientid;
    }
    else {
    	//echo "corpid does not exist for the user";
    }
	$Client_Id = $IntUserCode;
	//echo $sessionrole; 
    if($sessionrole=="admin")
    {
	  $today = date('Y-m-d');
      $sql ="SELECT * FROM mandates Where mandate_initiation_date >= '$today' and status_desc IN $statusList ORDER BY createdon DESC";
      //echo $sql ;     
	 $result = mysqli_query($connect,$sql);
    }
    elseif(!empty($Client_Id))
	{
		
	  $today = date('Y-m-d');
      $sql ="SELECT * FROM mandates WHERE IntUserCode ='$Client_Id' and mandate_initiation_date >= '$today' AND status_desc IN $statusList  ORDER BY createdon DESC ";
      $result = mysqli_query($connect,$sql);
    }
	
	function gettodate()
	{
		$effectiveDate = date('Y-m-d');
		//echo $effectiveDate;
$effectiveDate = date('Y-m-d', strtotime(' + 3 months'));
		
		return $effectiveDate;
	}
?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">

    <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
  <h2 align="center">Amend/Cancel Mandate</h2>
    </div>
      <div class='row'>
	    <div class='input-group margin-bottom-20'>
            <span class='input-group-addon'>
                <i class='fa fa-user'></i>
            </span>
            <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>

        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="<?php echo date('Y-m-d');?>">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo gettodate();?>">
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>


			<select class="form-control" id="Frequency1" name="Frequency1" size="1" style='z-index:0'>
  						  <?php
						  	$FrequencySelect = $Frequency1;

						  foreach($FrequencyArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}
						  }
						  ?>
            </select>

			</div>
		</div>
        </div>
      </form>
    </div>
  </div>
<div class="table-responsive">
<div class="messages" id="message" name="message">
  		 <?php
		 if (isset($message))
		 {
			 if($message == 'updated successfully')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);
			 }
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}
			}
		 }
		 ?>
  </div>  
<table id="collections" class="table table-striped table-bordered" style="font-size: 10px;">
      <thead>
          <tr>
			<th>Reference</th>
			<th>Collection Amount</th>						
			<th>Mandate Initiation Date</th>	
			<th>Account Holder</th>
			<th>Account Number</th>
			<th>Amendment Reason</th>
			<th>Status</th>
			<th>Amendment</th>
			<th>Stop</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="1" style="text-align:right">Total:</th>
                      <th colspan="8" ></th>
                  </tr>
      </tfoot>
      <?php
	  //--print_r($result);
		if(!empty($result))
		{

        while($row = mysqli_fetch_array($result))
        {
			$reference 							= $row["reference"];
			$idcollection_amount				= "collection_amount_$reference";
			$idtracking_indicator		  		= "tracking_indicator_$reference";
			$tracking_indicator					= $row['tracking_indicator'];
			$idFrequency                  		= "Frequency_$reference";$Frequency                          = $row['frequency'];
			$idmandate_initiation_date	  		= "mandate_initiation_date_$reference";$mandate_initiation_date	        = $row['mandate_initiation_date'];
			$idfirst_collection_date      		= "first_collection_date_$reference";$first_collection_date              = $row['first_collection_date'];
			$idmaximum_collection_amount  		= "maximum_collection_amount_$reference";$maximum_collection_amount	        = $row['maximum_collection_amount'];
			$iddebtor_account_name 	      		= "debtor_account_name_$reference";$debtor_account_name 	            = $row['debtor_account_name'];
			$iddebtor_identification 	  		= "debtor_identification_$reference";$debtor_identification 	            = $row['debtor_identification'];
			$iddocument_type	          		= "document_type_$reference";$document_type	                    = $row['document_type'];
			$iddebtor_account_number  	  		= "debtor_account_number_$reference";$debtor_account_number  	        = $row['debtor_account_number'];
			$iddebtor_account_type	      		= "debtor_account_type_$reference";$debtor_account_type	            = $row['debtor_account_type'];
			$iddebtor_branch_number  	  		= "debtor_branch_number_$reference";$debtor_branch_number  	            = $row['debtor_branch_number'];
			$iddebtor_contact_number	  		= "debtor_contact_number_$reference";$debtor_contact_number			    = $row['debtor_contact_number'];
			$iddebtor_email               		= "debtor_email_$reference";$debtor_email                       = $row['debtor_email'];
            $idcollection_date            		= "collection_date_$reference";$collection_date                    = $row['collection_date'];
            $iddate_adjustment_rule_indicator 	= "date_adjustment_rule_indicator_$reference";$date_adjustment_rule_indicator     = $row['date_adjustment_rule_indicator'];
            $idadjustment_category        		= "adjustment_category_$reference";$adjustment_category                = $row['adjustment_category'];
            $idadjustment_rate            		= "adjustment_rate_$reference";$adjustment_rate                    = $row['adjustment_rate'];
            $idadjustment_amount          		= "adjustment_amount_$reference";$adjustment_amount                  = $row['adjustment_amount'];
            $idfirst_collection_amount 	  		= "first_collection_amount_$reference";$first_collection_amount1 	        = $row['first_collection_amount'];
            $iddebit_value_type           		= "debit_value_type_$reference";$debit_value_type                   = $row['debit_value_type'];
            $idamendment_reason_text            = "amendment_reason_text_$reference";$amendment_reason_text                    = $row['amendment_reason_text'];
            $idamendment_reason_md16           	= "amendment_reason_md16_$reference";$amendment_reason_md16                   = $row['amendment_reason_md16'];
			$id_status = "status_desc_$reference";
			
echo '<tr id = "r'.$reference.'" name = "r'.$reference.'" >';
echo '<td width="5%">'.$row["reference"].'</td>';
echo '<td width="5%" id='.$idcollection_amount.' >'.$row["collection_amount"].'</td>';
echo '<td width="5%" id='.$idmandate_initiation_date.'>'.$row["mandate_initiation_date"].'</td>';
echo '<td width="5%" id='.$iddebtor_account_name.'>'.$row["debtor_account_name"].'</td>';
echo '<td width="5%" id='.$iddebtor_account_number.'>'.$row["debtor_account_number"].'</td>';
echo '<td width="5%" id='.$idamendment_reason_text.'>'.$row["amendment_reason_text"].'</td>';
echo '<td width="5%" id='.$id_status.' >'.$row["status_desc"].'</td>';
echo '<td width="5%"><a style="font-size: 10px;" class="btn btn-success" id = "'.$reference.'" name = "'.$reference.'" onclick="Update(this);" data-popup-open="popup-1"  >Edit</a></td>';

echo '<td width="5%"><a class="btn btn-danger" id = "c'.$reference.'" name = "c'.$reference.'" onclick="cancel(this);" style="font-size: 10px;" data-popup-open="popup-1" >Stop</a></td>';

			
			//echo '&nbsp;';
			echo '</tr>';
        }
	}
      ?>


  </table>
  <div class="popup" data-popup="popup-1" style="overflow-y:auto;z-index: 1;">
						<div class="popup-inner">
							<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
							<div id="myProgress">
								<div id="myBar">10%</div>
								
							</div> 
							</br> -->
										<div id="divPaymentSchedules" style="top-margin:100px;font-size: 10px;">
										
										</div>
						<!-- <div id="divSuccess">
							
							</div> -->
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>
					</div>			
				</div>
    </div>
    </br>
</div>

<script>


$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
	var col = 2;

	       //alert(data[col]);

    var Action_Date=Date.parse(data[col]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
var col = 1;
//alert('There are'+api.column(col).dataSrc() +' row(s) of data in this table');

        // Total over all pages
        total = api
            .column( col)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),'.', ''));
				//alert(b);
				
                return intVal(b).toFixed(2);//intVal(b);// + intVal(b);
				//return a + b;
            }, col );

        // Total over this page
        pageTotal = api
            .column( col, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
				//alert(b);
                return intVal(b).toFixed(2);//intVal(b);// + intVal(b);
				
				//return a + b;
            }, col );

            // Update footer
      $( api.column( col ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});

function cancelUpdate(ele)
{
	var id 								 = ele.id;//substr(ele.id,1);
	id = id.substr(1);
		//alert(id);
	var idtracking_indicator		  	 = "tracking_indicator_"+id;
	var idFrequency                  	 = "Frequency_"+id;
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;
	var idfirst_collection_date      	 = "first_collection_date_"+id;
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id;
	var iddebtor_identification 	  	 = "debtor_identification_"+id;
	var iddocument_type	          		 = "document_type_"+id;
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;
	var iddebtor_email               	 = "debtor_email_"+id;
	var idcollection_date            	 = "collection_date_"+id;
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;
	var idadjustment_rate            	 = "adjustment_rate_"+id;
	var idadjustment_amount          	 = "adjustment_amount_"+id;
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id;
	var iddebit_value_type           	 = "debit_value_type_"+id;

	var idamendment_reason_text           = "amendment_reason_text_"+id;
    var idamendment_reason_md16           = "amendment_reason_md16_"+id;

	// -- return to original position.
		document.getElementById(id).innerHTML = 'Edit';
		ele.style.display = "none";

		// -- tracking_indicator					--//
		id_= idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- mandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = true;

		// -- maximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- debtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = true;

		// -- debtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = true;

		// -- document_type	          				--//
		id_= iddocument_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;

		// -- iddebtor_account_type	      			--//
		id_= iddebtor_account_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_branch_number  	  			--//
		id_= iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = true;

		// -- debtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = true;

		// -- collection_day            			--//
		id_= idcollection_date;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value='"+value+"' readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- date_adjustment_rule_indicator		--//
		id_= iddate_adjustment_rule_indicator;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		id_= idadjustment_category;
		value = document.getElementById(id_);
		valueCat = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
			valueCat = value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- iddebit_value_type           			--//
		id_= iddebit_value_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- idamendment_reason_text 	  	--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = true;

		// -- idamendment_reason_text 	  	--//
		//id_= idamendment_reason_text;
		//document.getElementById(id_).readOnly = true;

}
// #myInput is a <input type="text"> element
function Update(ele)
{
			
		var id = ele.id;
		
			jQuery(document).ready(function()
			{
				file = "i_updatedebicheckmandate.php?reference="+id;
				jQuery("#divPaymentSchedules").load(file);

			});	
}

// -- Stop actual transactions 
function stop(id)
{
		
		//alert(id);
		id = id.substring(1);
		//alert(id);
				var str_data = '';

		var message = '';
		var message_info = document.getElementById('message_info');
		var valid = true;
		var changedby = document.getElementById('changedby').value;
		//alert(changedby);
		var rowChanged  = document.getElementById("r"+id);
		var table = document.getElementById("collections");
		
		// -- tracking_cancellation_indicator:
		var  tracking_cancellation_indicator = document.getElementById('tracking_cancellation_indicator');
		var  tracking_cancellation_indicator_val = tracking_cancellation_indicator.value;
		if(tracking_cancellation_indicator_val === 'N')
		{
			message = 'Please choose a cancellation indicator Yes.'; 
			valid  = false;
		}

		// -- cancellation_reason:
		var cancellation_reason = document.getElementById('cancellation_reason');
		var cancellation_reason_val = cancellation_reason.value;
		if(cancellation_reason_val === 'none')
		{
			message = 'Please choose a cancellation reason.'; 
			valid  = false;			
		}
				
		message_info.innerHTML = message;
		if(valid)
		{
			/// -----
			var list =
					{
					'tracking_cancellation_indicator' 			: tracking_cancellation_indicator_val
					,'cancellation_reason' 						: cancellation_reason_val
					,'changedby':changedby};
					
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"stopdebicheckmandate.php",
				       data:{id_:id,list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(str_data);
							jQuery("#message").html(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');							
							if(feedback.includes("was stopped successfully"))
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'status';
								
								// -- Updating updated fields
								//alert(list['collection_amount']);					
								document.getElementById("status_desc_"+id).innerHTML = 'Cancelled';	
								table.deleteRow(rowChanged.rowIndex);
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
							}

						 }
					});
				});
			/// -----
		}
		else
		{
			message_info.className  = 'alert alert-error';
		}	
			  return str_data;		
}

// -- cancel debicheck mandate
function cancel(ele)
{

		var id = ele.id;
		var cancellation_reason = <?php echo json_encode($debtor_cancellation_codes_Arr);?>;
		var changedby = <?php echo "'".$changedby."'";?>;
		
		
	jQuery(document).ready(function()
			{
				var html = '';
				var total = cancellation_reason.length;
	//html = '<div class="form-group">';
	//html = html+ '<div class="col-md-3">';
	 html = html+ '<label for="tracking_cancellation_indicator">Tracking Cancellation Indicator:<span style="color:red">*</span></label>';
	html = html+ '<select class="form-control" id="tracking_cancellation_indicator" name="tracking_cancellation_indicator">';
    html = html+'<option value="Y" selected>Yes</option>';
    html = html+'<option value="N">No</option>';	
    html = html+ '</select>';
	
	html = html+ '<label for="cancellation_reason">Cancellation Reason:<span style="color:red">*</span></label>';
	html = html+ '<select class="form-control" id="cancellation_reason" name="cancellation_reason">';
	html = html+'<option value="none" selected>none</option>';

	for(i = 0;i < total;i++)
	{
		l_split_array = cancellation_reason[i].split('|');
		html = html+'<option value="'+l_split_array[0]+'">'+l_split_array[1]+'</option>';
	}
    html = html+ '</select>';
	id = "'"+id+"'";
	html = html+ '<button style="margin: 8px 0;" type="submit" class="btn btn-primary" value="Submit to the bank"onclick="stop('+id+');" >Submit to the bank</button>';

	html = html+ '<p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>';
	
	html = html+ '<input style = "display:none" id="changedby" type="text"  name="changedby" class="form-control" placeholder="" readonly value ="'+changedby+'" />';

	//html = html+ '</div>';
	//html = html+ '</div>';
	//alert(html);
				//jQuery("divPaymentSchedules").html = html;
				document.getElementById('divPaymentSchedules').innerHTML = html;

			});	
}

function toggleUpdate(ele)
{
	var id 								 = ele.id;
	var idtracking_indicator		  	 = "tracking_indicator_"+id;
	var idFrequency                  	 = "Frequency_"+id;
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;
	var idfirst_collection_date      	 = "first_collection_date_"+id;
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id;
	var iddebtor_identification 	  	 = "debtor_identification_"+id;
	var iddocument_type	          		 = "document_type_"+id;
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;
	var iddebtor_email               	 = "debtor_email_"+id;
	var idcollection_date            	 = "collection_date_"+id;
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;
	var idadjustment_rate            	 = "adjustment_rate_"+id;
	var idadjustment_amount          	 = "adjustment_amount_"+id;
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id;
	var iddebit_value_type           	 = "debit_value_type_"+id;

	var idamendment_reason_text           = "amendment_reason_text_"+id;
    var idamendment_reason_md16           = "amendment_reason_md16_"+id;


    var value = '';
	// -- if Equal save it mean we are on edit mode
	// -- some elements needs to be change from text to dropdown.
	if(ele.innerHTML == 'Save')
	{
	// -- Validate Required if Empty.
	// -- Update debicheck mandate.
		var objects_array =
		{'tracking_indicator' 			: idtracking_indicator
		,'Frequency' 						: idFrequency
		,'mandate_initiation_date' 		: idmandate_initiation_date
		//,'first_collection_date' 			: idfirst_collection_date
		,'maximum_collection_amount' 		: idmaximum_collection_amount
		,'debtor_account_name' 			: iddebtor_account_name
		,'debtor_identification' 			: iddebtor_identification
		,'document_type' 					: iddocument_type
		,'debtor_account_number' 			: iddebtor_account_number
		,'debtor_account_type' 			: iddebtor_account_type
		,'debtor_branch_number' 			: iddebtor_branch_number
		//,'debtor_contact_number' 			: iddebtor_contact_number
		//,'debtor_email' 					: iddebtor_email
		,'collection_date' 				: idcollection_date
		,'date_adjustment_rule_indicator' 	: iddate_adjustment_rule_indicator
		,'adjustment_category' 			: idadjustment_category
		//,'adjustment_rate' 				: idadjustment_rate
		//,'adjustment_amount' 				: idadjustment_amount
		,'first_collection_amount' 		: idfirst_collection_amount
		,'debit_value_type' 			: iddebit_value_type
		,'amendment_reason_text' : idamendment_reason_text};

		if(IsEmpty(objects_array))
		{
		ele.innerHTML = 'Edit';

		// -- tracking_indicator					--//
		id_= idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- mandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = true;

		// -- maximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- debtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = true;

		// -- debtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = true;

		// -- document_type	          				--//
		id_= iddocument_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;

		// -- iddebtor_account_type	      			--//
		id_= iddebtor_account_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_branch_number  	  			--//
		id_= iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = true;

		// -- debtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = true;

		// -- collection_day            			--//
		id_= idcollection_date;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		Frequency = document.getElementById(idFrequency).value;
		//alert(Frequency);

		if(Frequency != 'WEEK' && Frequency != 'ADHO')
		{
			var d = new Date(value);
			value = d.getDate();
			if(value < 10){value = '0'+value;}
			//alert(value);
		}
		new_html_el = "<input type ='text' id = "+id_+" value='"+value+"' readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- date_adjustment_rule_indicator		--//
		id_= iddate_adjustment_rule_indicator;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		id_= idadjustment_category;
		value = document.getElementById(id_);
		valueCat = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
			valueCat = value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = true;

		// -- iddebit_value_type           			--//
		id_= iddebit_value_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- non display of cancel.
		id_ = "c"+ele.id;
		document.getElementById(id_).style.display = 'none';

		// -- Values to updated.
		var values_array =
			{'tracking_indicator' 			: document.getElementById(idtracking_indicator).value
		,'Frequency' 						: document.getElementById(idFrequency).value
		,'mandate_initiation_date' 			: document.getElementById(idmandate_initiation_date).value
		,'first_collection_date' 			: document.getElementById(idfirst_collection_date).value
		,'maximum_collection_amount' 		: document.getElementById(idmaximum_collection_amount).value
		,'debtor_account_name' 				: document.getElementById(iddebtor_account_name).value
		,'debtor_identification' 			: document.getElementById(iddebtor_identification).value
		,'document_type' 					: document.getElementById(iddocument_type).value
		,'debtor_account_number' 			: document.getElementById(iddebtor_account_number).value
		,'debtor_account_type' 				: document.getElementById(iddebtor_account_type).value
		,'debtor_branch_number' 			: document.getElementById(iddebtor_branch_number).value
		,'debtor_contact_number' 			: document.getElementById(iddebtor_contact_number).value
		,'debtor_email' 					: document.getElementById(iddebtor_email).value
		,'collection_date' 					: document.getElementById(idcollection_date).value
		,'date_adjustment_rule_indicator' 	: document.getElementById(iddate_adjustment_rule_indicator).value
		,'adjustment_category' 				: document.getElementById(idadjustment_category).value
		,'adjustment_rate' 					: document.getElementById(idadjustment_rate).value
		,'adjustment_amount' 				: document.getElementById(idadjustment_amount).value
		,'first_collection_amount' 			: document.getElementById(idfirst_collection_amount).value
		,'debit_value_type' 				: document.getElementById(iddebit_value_type).value
		,'amendment_reason_text' 			: document.getElementById(idamendment_reason_text).value
		,'amendment_reason_md16'			: document.getElementById(idamendment_reason_md16).value };

		// -- Pass reference & array of values to updated.
		updatedebicheckmandate(id,values_array);
	   }
	}
	else
	{
		id_ = "c"+ele.id;
		//alert(id_);
		document.getElementById(id_).style.display = 'block';
		ele.innerHTML = 'Save';
		// -- convert php array into jQuery arrays.
		// -- tracking_indicator					--//
		Arr = <?php echo json_encode($tracking_indicatorArr); ?>;
		id_ = idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		Arr = <?php echo json_encode($FrequencyArr); ?>;
		id_ = idFrequency;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' onChange='valueselect23(this,"+idcollection_date+");'>";
		for (var key in Arr)
		{
			if(Arr[key] !== " "){
			row = new Array();
			row = Arr[key].split('|');
			  if(value === row[0])	// -- selected
			  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
			  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}

			}
		}
		new_html_el = new_html_el+"</select>";
		//alert(new_html_el);
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idmandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = false;

		// -- idfirst_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = false;

		// -- idmaximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = false;

		// -- iddebtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = false;

		// -- iddebtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = false;

		// -- document_type	          				--//
		Arr = <?php echo json_encode($Client_ID_TypeArr); ?>;
		id_ = iddocument_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddebtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = false;

		// -- debtor_account_type	      			--//
		Arr = <?php echo json_encode($Account_TypeArr); ?>;
		id_ = iddebtor_account_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;
		// -- iddebtor_branch_number  	  			--//
		Arr = <?php echo json_encode($Branch_CodeArr); ?>;
		id_ = iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddebtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = false;
		// -- iddebtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = false;

		// -- idcollection_date            			--//
		id_= idcollection_date;
		value = document.getElementById(id_).value;
		value2 = document.getElementById(idFrequency).value;
		new_html_el = "";
		
		if(value2 != 'WEEK' && value2 != 'ADHO')
		{
		  value = document.getElementById(id_).value;
		  var d = GetFormattedDate(value);
		  //alert(d);
		  new_html_el = "<input type ='date' name = "+id_+" id = "+id_+" value = "+d+" />";
		  document.getElementById(id_).outerHTML = new_html_el;
		}
		else
		{
		Arr = <?php echo json_encode($collection_day_Arr); ?>;
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			//alert(Arr[key]);
			if(row[2] === document.getElementById(idFrequency).value) // for selected frequency.
			{
				if(value === row[0])	// -- selected
				{new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[0]+' - '+row[1]+"</option>";}
				else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[0]+' - '+row[1]+"</option>";}
			}
		}
		new_html_el = new_html_el+"</select>";
		}
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddate_adjustment_rule_indicator		--//
		Arr = <?php echo json_encode($date_adjustment_rule_indicatorArr); ?>;
		id_ = iddate_adjustment_rule_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
				row = new Array();
				row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		Arr = <?php echo json_encode($datadebtor_adjustment_categoryArr); ?>;
		id_ = idadjustment_category;
		value = document.getElementById(id_).value;
		valueCat = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' onChange='adjustment_category1(this,"+idadjustment_rate+","+idadjustment_amount+","+iddebit_value_type+");'>";
		for (var key in Arr)
		{
				row = new Array();
				row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = false;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = false;

		// -- idfirst_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = false;

		// -- iddebit_value_type           			--//
		Arr = <?php echo json_encode($debit_value_typeArr); ?>;
		id_ = iddebit_value_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' style='display:block'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- idamendment_reason_text 	  	--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = false;

	}
}


function updatedebicheckmandate(id,list)
{
	var str_data = '';
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"updatedebicheckmandate.php",
				       data:{id_:id,list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(str_data);
							jQuery("#message").html(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');							
							if(feedback.includes("was updated successfully"))
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'status';
								
								// -- Updating updated fields
								//alert(list['collection_amount']);
				document.getElementById("collection_amount_"+id).innerHTML = list['collection_amount'];	
				document.getElementById("mandate_initiation_date_"+id).innerHTML = list['mandate_initiation_date'];
				document.getElementById("debtor_account_name_"+id).innerHTML = list['debtor_account_name'];					
				document.getElementById("debtor_account_number_"+id).innerHTML = list['debtor_account_number'];	
				document.getElementById("amendment_reason_text_"+id).innerHTML = list['amendment_reason_text'];		document.getElementById("status_desc_"+id).innerHTML = 'pending';	
			
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
							}

						 }
					});
				});
			  return str_data;
}
function valueselect23(sel,ref)
 {
		idcollection_day =  ref.id;
		var frequency = sel.options[sel.selectedIndex].value;
		var selectedCollection_Day = ref.value;
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectCollectionDays.php",
				       data:{frequency:frequency,collection_day:selectedCollection_Day,id_:idcollection_day},
						success: function(data)
						 {
							//alert(data);
							jQuery("#"+idcollection_day).replaceWith(data);
						 }
					});
				});

			  return false;
  }
function adjustment_category1(sel,rate,amount,value_type)
 {
	var adjustment_category = sel.options[sel.selectedIndex].value;

	if(adjustment_category == 'N')
	{
	  	rate.style.display = "none";
	    amount.style.display = "none";
		value_type.style.display = "none";

		rate.value = '0,00';
		amount.value = '0,00';
		value_type.selectedIndex = 0; //Default to Fixed contract
	}
	else
	{
	  	rate.style.display = "block";
		amount.style.display = "block";
		value_type.style.display = "block";
	}
  			  return false;
 }

 isObject = function(Obj)
 {
                 // object is of type Element
            return Obj instanceof Element;
};

function IsEmpty(list)
{ // Going throuh a list to validate mandatory fields.
	for(var item in list)
	{
		id_ = list[item];
		//alert(id_);
		var value = "";
		value = document.getElementById(id_).value;
		value = value.trim();
		if(value.length == 0)
		{	document.getElementById(id_).focus();
			    //alert(id_+" is required.");
				feedback = id_+" is required.";
				var message_info = document.getElementById('message_info');							
				message_info.innerHTML = feedback;
				message_info.className  = 'alert alert-error';			
				return false;
		}
	}
  return true;
}

function CheckIsEmpty(value)
{ 
		value = value.trim();
		if(value.length == 0)
		{	
			return true;
		}
	return false;		
}

function IsNotEmpty(list)
{ // Going throuh a list to validate NOT mandatory fields.
	for(var item in list)
	{
		id_ = list[item];
		var value = "";
		value = document.getElementById(id_).value;
		value = value.trim();
		//alert(value.trim().length);
		if(value.length != 0)
		{		document.getElementById(id_).focus();
			    //alert(id_+" must be blank.");
				feedback = id_+" must be blank.";
				var message_info = document.getElementById('message_info');							
				message_info.innerHTML = feedback;
				message_info.className  = 'alert alert-error';				
				return false;
		}
	}
  return true;
}

function GetFormattedDate(in_day) 
{
    var todayTime = new Date();
    var month = todayTime .getMonth() + 1;
	if(month<10) 
	{
		month='0'+month;
	} 
	var day = '';
	if(in_day == '')
	 {day = todayTime .getDate();}
	 else
	 {
		day = in_day; 
	 }
    var year = todayTime .getFullYear();
    return year  + "-" + month  + "-" + day;
}
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");   
		  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();*/
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
        e.preventDefault();
		//location.reload();		
    });
});

</script>
