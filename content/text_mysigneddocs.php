<?php
$FromDate= '';
$ToDate='';
$sessionrole='';

$Allstatus = '';
$status    = '';

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM process_task_status';
$datafrequency = $pdo->query($sql);

$FrequencyArr[] = array();
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['statusId'];
}

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid 		= '';
$Client_Id 			= getsession('username');
$result 			= null;

    if($sessionrole=="admin")
    {
      $sql ="SELECT * FROM user_signed_doc ORDER BY createdon DESC, createdat DESC";
	  $q = $pdo->prepare($sql);
	  $q->execute();
	  $result = $q->fetchAll();
    }
    elseif(!empty($Client_Id))
	{
      $sql ="SELECT * FROM user_signed_doc WHERE userid = ? ORDER BY createdon DESC, createdat DESC";
	  $q = $pdo->prepare($sql);
	  $q->execute(array($Client_Id));
	  $result = $q->fetchAll();
    }
?>
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>
<div class="container background-white bottom-border">
  <div class='row margin-vert-30'>
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
  <h2 align="center">Documents for eSignature</h2>
    </div>
      <div class='row'>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="">
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>

			
			<select class="form-control" id="Frequency" name="Frequency" size="1" style='z-index:0'>
  						  <?php
						  	$FrequencySelect = $Frequency;

						  foreach($FrequencyArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}	}						  
						  }
						  ?>
            </select>
			
			</div>
		</div>
        </div>
      </form>
    </div>
  </div>
</br>
</br>
<!-- Modal -->
<div class="" >
<div class="modal fade modal-dialog" id="myModal">
        <!-- Modal content  -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Review Document for eSignature</h4>
            </div>
        <div class="modal-body">
		</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>  
</div> 		
</div>

<div class="table-responsive">
<table id="collections" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
              <th>Document</th>
              <th>Type</th>			  
              <th>Uploaded by</th>
              <th>Uploaded on</th>
			  <th>Uploaded At</th>
              <th>Changed by</th>
              <th>Status</th>
              <th>Action</th>			  
          </tr>
      </thead>
    <tfoot>
                  <!--<tr>
                      <th colspan="3" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>-->
      </tfoot>
      <?php
				foreach ($result as $row)
				{
				 $action = 
				 "<a class='btn btn-primary' href='signing?signfile=".$row['url']."&userid=".$Client_Id."'>e-Sign</a>";		
				 $html = "<a class='ls-modal btn btn-primary' href='signature?file=".$row['url']."'>REVIEW DOCUMENT</a>";
				 echo '<tr><td width="1%">'.$html.'</td>';
				 echo '<td width="2%">'.$row['signeddoctype'].'</td>';				 
				 echo '<td width="2%">'.$row['userid'].'</td>';
				 echo '<td width="1%">'.$row['createdon'].'</td>';
				 echo '<td width="1%">'.$row['createdat'].'</td>';				 
				 echo '<td width="2%">'.$row['userid'].'</td>';				 
				 echo '<td width="5%">'.$row['status'].'</td>';
				 echo '<td width="5%">'.$action.'</td>';
				 //echo '<td width="5%">'.$action.'</td>';				 
				 echo '</tr>';
				}
      ?>
  </table>
    </div>
    </br>
</div>

<script>

$('.ls-modal').on('click', function(e){
  e.preventDefault();
  $('#myModal').modal('show').find('.modal-body').load($(this).attr('href'));
  
});

$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[4]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function()
{
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 3)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 3 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#Frequency').on('change', function(){
   table.search( this.value ).draw();
});



});

// #myInput is a <input type="text"> element

</script>
