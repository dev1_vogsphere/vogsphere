  <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

<!--<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Multiple File Upload with PHP - Demo</title> -->
<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Upload Bulk SMS Contacts via csv File</h1>
		<!-- Multiple file upload html form-->
		<form id="loaddata" action="" method="post" enctype="multipart/form-data">
		<p>Download a Template Contacts file <a href='bulksmscontacts/Template-Contacts Upload.xlsx' target='_blank' style="color:red"><strong>here</strong></a> and Instructions <a href='bulksmscontacts/Documentation_UploadContacts.docx' target='_blank' style="color:red"><strong>here</strong></a></p>
		<p>Max file size 10Mb, Valid formats csv</p>
		<br />
			<input type="file" name="files[]" multiple="multiple" accept=".csv">
			<table class ="table table-user-information">
				  <tr>
					<td>
						<div>
							<!-- <input type="submit" value="Upload"> -->
							<input id="uploader" type="submit" class="btn btn-primary" data-popup-open="popup-1" value="Upload">
							<input type="text" name="bulksmsclientid" id="bulksmsclientid" style="display:none"
							value="<?php if(isset($_SESSION['IntUserCode'])){echo $_SESSION['IntUserCode'];}?>" />
							<br>
							<input type="text" name="UserCode" id="UserCode" style="display:none"
							value="<?php if(isset($_SESSION['UserCode'])){echo $_SESSION['UserCode'];}?>" />							
							<br>
						</div>
					</td>
				<td>
				<div>
				</tr>
			</table>
<div class='row'>
				  <div class="table-responsive">			
	<div  id="report"></div></div>
</div>
		</form>

</div>
</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <h2>Bulk SMS Contact Upload - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
		</div>
		<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
<!-- ===================================================== End Send An Email ======================= -->
<script  type="text/javascript">
$(document).ready(function(){
       jQuery("#uploader").change(function(){
		
          // file on change
alert("file changed");
      });

       
jQuery("#loaddata").submit(function(e){
	 // file on submit
	// alert("file submited");
	 			e.preventDefault();
	     value  = document.getElementById("bulksmsclientid").value;
	     value1 = document.getElementById("UserCode").value;
		 
		var formdata = new FormData(this);
			$.ajax({
				url: "script_AddNewCustomer.php",
				type: "POST",
				data:formdata,
				mimeTypes:"multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
				Message = "contacts uploaded successfully";
				jQuery("#divSuccess").append(Message);
				move();
				jQuery("#report").show().html(data);
				},error: function(){
					alert(data);
				}
		});
});
	});
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        //e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}	
</script>