<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
		
        <!-- Favicon -->
        <link href="http://vogsphere.co.za/eloan/assets/img/Icon_vogs.ico" rel="shortcut icon">
		
	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
	
	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->
	
	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->
	
	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>
<body bgcolor="#f0f0f0" width="100%" style="margin: 0;">
    <center style="width: 100%; background: ##f0f0f0;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <!-- <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            (Optional) This text will appear in the inbox preview, but not the email body.
        </div> -->
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
			<!-- <tr>
				<td style="padding: 20px 0; text-align: center">
					<img src="http://placehold.it/200x50" width="200" height="50" alt="alt_text" border="0">
				</td>
			</tr> -->
        </table>
        <!-- Email Header : END -->
        
        <!-- Email Body : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
				<td bgcolor="#f0f0f0">
					<img src="http://vogsphere.co.za/eloan/assets/img/logo-theone.png" width="600" height="" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px;">
				</td>
            </tr>
            <!-- Hero Image, Flush : END -->

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
<?php 
									
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$password 		= '';
									
// Id number
$idnumber 		= '';
$clientname 	= $Title.''.$FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= '';	
// Email
$email 			= '';	
// Street
$Street 		= '';	
// Suburb
$Suburb 		= '';	
// City
$City 			= '';	
// State
$State 			= '';	
// PostCode
$PostCode 		= '';	
// Dob
$Dob 			= '';	
// phone
$phone 			= '';	

$ApplicationId = '';
	
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
$loandate 		= '';	
$loanamount 	= '0.00';	
$loanpaymentamount = '0.00';	
$loanpaymentInterest = "";
$FirstPaymentDate = '';	
$LastPaymentDate  = '';	
$applicationdate  = '';	
$status 		  = '';	
$monthlypayment = '0.00';// Monthly Payment Amount 
$LoanDuration = '0';

if(isset($_SESSION))
{	
// -- BOC 27.01.2017
$personalloan = $_SESSION['personalloan'];
$loantype = $_SESSION['loantypedesc'];
$loanpaymentInterest = $personalloan."%";
// -- EOC 27.01.2017

$Title 			= $_SESSION['Title'];
$FirstName 		= $_SESSION['FirstName'];
$LastName 		= $_SESSION['LastName'];
$password 		= $_SESSION['password'];
									
// Id number
$idnumber 		= $_SESSION['idnumber'];
$clientname 	= $Title.''.$FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= $_SESSION['phone'];	
// Email
$email 			= $_SESSION['email'];	
// Street
$Street 		= $_SESSION['Street'];	
// Suburb
$Suburb 		= $_SESSION['Suburb'];	
// City
$City 			= $_SESSION['City'];	
// State
$State 			= $_SESSION['State'];	
// PostCode
$PostCode 		= $_SESSION['PostCode'];	
// Dob
$Dob 			= $_SESSION['Dob'];	
// phone
$phone 			= $_SESSION['phone'];		
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
$loandate 		= $_SESSION['loandate'];	
$loanamount 	= $_SESSION['loanamount'];	
$loanpaymentamount = $_SESSION['loanpaymentamount'];	
$loanpaymentInterest = "20%";
$FirstPaymentDate = $_SESSION['FirstPaymentDate'];	
$LastPaymentDate  = $_SESSION['LastPaymentDate'];	
$applicationdate  = $_SESSION['applicationdate'];	
$status 		  = $_SESSION['status'];	
$ApplicationId    =  $_SESSION['ApplicationId'];

if($status == 'REJ')
// Rejected
{
	$status  = 'Rejected';
}
// Approved
if($status == 'APPR')
{
	$status  = 'Approved';
}

$monthlypayment = $_SESSION['monthlypayment'];// Monthly Payment Amount 
$LoanDuration = $_SESSION['LoanDuration'];// Loan Duration

}

//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
$companyname 	= "Vogsphere Pty Ltd";
$companyStreet 	= "43 Mulder Street";
$companySuburb 	= "The reeds";
$companyPostCode = "0157";
$companyCity 	= "Centurion";
$companyphone 	= '0793401754/0731238839';				
				echo "<h3>Dear $Title $FirstName $LastName</h3>
																<h3><b>Welcome to Vogsphere e-Loan  - DEVELOPMENT TESTING ONLY!</b></h3>
																<p>Thank you for applying for an vogsphere (PTY) LTD e-Loan</p>
																<p>E-loan offers flexible, short-term loans from as minimun as R500.00 up to R5000.00 for new Customers.</p>
																<!--<p>For a new loan application go to the 
																<b><a href='../applicationForm.php'>Application Form</a></b>
																</p>
																<p>Details of you Loan applications go to
																<b><a href='../customerLogin.php'>MY E-LOAN ACCOUNT</a></b> </p>
															<!-- </div> -->
															<!-- End Main Text<p align='center'><b>LOAN - AGREEMENT STATEMENT</b></p>-->
															<h3><b>YOUR E-LOAN APPLICATION STATUS</b></h3>
															<p>
															
															We regret to inform you that your e-Loan application has been declined and we cannot provide you with any finance at this time.
															Even thou your requested eloan amount of <b>R$loanamount</b> falls within our lower band scheme, your personal information and 
															financial circumstances did not meet our internal criteria.
															
															<!-- Below is a summary of your plan details. Kindly check that all your details are correct; should there be any amendments, we ask that you update them and upload required documents via the website : <a href = 'http://www.vogsphere.co.za/eloan/login.php'>www.vogsphere.co.za/eloan/customerLogin.php</a>. -->
															
															</p>	


															
															<h3><b>YOUR E-LOAN APPLICATION DETAILS</b></h3>
															<p>Below is a summary of your plan details. Kindly check that all your details are correct; should there be any amendments, we ask that you update them and upload required documents via the website : <a href = 'http://www.vogsphere.co.za/eloan/login.php'>www.vogsphere.co.za/eloan/customerLogin.php</a>.</p>															
															<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent;
															  margin-bottom: 20px;'>
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;'><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2' >CLIENT PHYSICAL ADDRESS</th></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Company Client:</td><td><b>$Title  $FirstName  $LastName</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Physical address:</td><td><b>$Street</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$Suburb</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$City</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$PostCode</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Contact Number: </td><td><b>$phone</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Company Client ID: </td><td><b>$idnumber</b></td></tr>
															</table>
															<br>
										<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent; margin-bottom: 20px;'>
															   <tr><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>TOTAL COST AND INTEREST</th></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Requested Amount</td><td><b>R$loanamount</td></tr>
																
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Interest Percentage</td><td><b>$loanpaymentInterest</td></tr>
															   
															   	<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Monthly Payment Amount</td><td><b>R$monthlypayment</td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Loan Duration</td><td><b>$LoanDuration month(s)</td></tr>
															   
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Total amount repayment</td><td><b>R$loanpaymentamount</td></tr>
															   <tr><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>REPAYMENT ARRANGEMENTS</th></tr>
															   		
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Application date </td><td><b>$applicationdate</td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Application status</td><td><b>$status</td></tr>
																
															</table>
															  ";
															
															;
?>
<p>
You are free to pursue another application after 6 months. 
If you have any questions or queries about this rejection please send us an e-mail on <a href="mailto:eloan@vogsphere.co.za">eloan@vogsphere.co.za</a></p> 
<p><b>Kind Regards</b></p>
<p><b>VOGSPHERE (PTY) LTD Team</b></p>
                </td>
            </tr>
        </table>
          
        <!-- Email Footer : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            <tr>
                <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                    <webversion style="color:#cccccc; text-decoration:underline; font-weight: bold;">View as a Web Page</webversion>
                    <br><br>
                    Vogsphere (Pty) Ltd<br><span class="mobile-link--footer">43 Mulder street 31 SEOUL, Centurion,Gauteng, 0158</span><br><span class="mobile-link--footer">(079)-340-1754</span>
                    <br><br> 
                    <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->

    </center>
</body>
</html>

