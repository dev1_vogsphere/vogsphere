<?php
/*Rules
The following only fields that can be updated:
1	Tracking Indicator						$tracking_indicator
2	Frequency								$Frequency
3	Mandate Initiation Date					$mandate_initiation_date	
4	First Collection Date					$first_collection_date
5	Maximum Collection Amount				$maximum_collection_amount	
6	Debtor Account Name						$debtor_account_name 	
7	Debtor Identification                   $debtor_identification 	
8	Document Type                           $document_type	
9	Debtor Account Number                   $debtor_account_number  	
10  Debtor Account Type                     $debtor_account_type	
11  Debtor Branch Number                    $debtor_branch_number  	
12  Debtor Telephone Contact Details        $debtor_contact_number			
13  Debtor Email Contact Details			$debtor_email 
14  Collection Day							$collection_date 
15  Date Adjustment Rule Indicator			$date_adjustment_rule_indicator
16  Adjustment Category						$adjustment_category 
17  Adjustment Rate							$adjustment_rate
18  Adjustment Amount						$adjustment_amount 
19  First Collection Amount					$first_collection_amount 	
20  Debit Value Type						$debit_value_type 
*/

require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);
$dataentry_class = array();

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_debitvalue_types';
$datadebtor_debitvalue_types = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_entry_class_codes';
$datadebtor_entry_class_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_collection_codes';
$datacollection_day_codes = $pdo->query($sql);


$sql = 'SELECT * FROM debtor_adjustment_category';
$datadebtor_adjustment_category = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_auth_codes';
$datadebtor_auth_code = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_cancellation_codes';
$datadebtor_cancellation_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_instalment_occurrence';
$datadebtor_instalment_occurrence = $pdo->query($sql);

$notification = array();
$notificationsList = array();
// -- Declare -- //
$Frequency					    = '';
$reference 						='';
$contract_reference 			='';
$tracking_indicator 			='';
$debtor_auth_code 				='';
$auth_type 						='';
$instalment_occurence 			='';
$mandate_initiation_date 		='';
$first_collection_date 			='';
$collection_amount_currency 	='ZAR';
$collection_amount 				='';
$maximum_collection_currency 	='ZAR';
$maximum_collection_amount 		='';
$entry_class 					='';
$debtor_account_name 			='';
$debtor_identification 			='';
$debtor_account_number 			='';
$debtor_account_type 			='';
$debtor_branch_number 			='';
$debtor_contact_number 			='';
$debtor_email 					='';
$collection_date 				='';
$date_adjustment_rule_indicator  ='';
$adjustment_category 			='';
$adjustment_rate 				='';
$adjustment_amount_currency 	='ZAR';
$adjustment_amount 				='';
$first_collection_amount_currency = 'ZAR'; 
$first_collection_amount 		='';
$debit_value_type 				='';
$cancellation_reason 			='';
$amended 						='';
$status_code 					='';
$status_desc 					='';
$document_type 					='';
$amendment_reason_md16 			='';
$amendment_reason_text 			='';
$tracking_cancellation_indicator ='';

$IntUserCode = getsession('IntUserCode');
//echo $IntUserCode;

// -- EOC Declare -- //
// -- dropdown selection.
$debtor_cancellation_codes_Arr = array();
$debtor_cancellation_codes_Arr[] = "".'|'."";

foreach($datadebtor_cancellation_codes as $row)
{
	$debtor_cancellation_codes_Arr[] = $row['reason_code'].'|'.$row['reason_desc'];
}

$collection_day_Arr = array();
foreach($datacollection_day_codes as $row)
{
	$collection_day_Arr[] = $row['collection_day_code'].'|'.$row['collection_desc'].'|'.$row['frequency_code'];
}

$Client_ID_TypeArr = array();
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}

$tracking_indicatorArr[] = 'Y|Yes';
$tracking_indicatorArr[] = 'N|No';

$date_adjustment_rule_indicatorArr[] = 'Y|Yes';
$date_adjustment_rule_indicatorArr[] = 'N|No';

$instalment_occurenceArr = array();
foreach($datadebtor_instalment_occurrence as $row)
{
	$instalment_occurenceArr[] = $row['instalment_occurrence'].'|'.$row['instalment_occurrence_desc'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}

$debtor_auth_codeArr = array();
foreach($datadebtor_auth_code as $row)
{
	$debtor_auth_codeArr[] = $row['debtor_auth_code'].'|'.$row['debtor_auth_code'].' - '.$row['debtor_auth_type'];
}

$debit_value_typeArr = array();
foreach($datadebtor_debitvalue_types as $row)
{
	$debit_value_typeArr[] = $row['debtor_debitvalue_type'].'|'.$row['debitvalue_desc'];
}

$entry_classArr = array();
foreach($datadebtor_entry_class_codes as $row)
{
	$entry_classArr[] = $row['debtor_entry_class'].'|'.$row['debtor_entry_class_desc'];
}

$datadebtor_adjustment_categoryArr = array();
foreach($datadebtor_adjustment_category as $row)
{
	$datadebtor_adjustment_categoryArr[] = $row['category'].'|'.$row['category_desc'];
}

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  if(isset($_SESSION['UserCode']))
    {

    	$Client_Id=$_SESSION['UserCode'];
     //echo $clientid;
    }
    else {
    	echo "corpid does not exist for the user";
    }
	$Client_Id = $IntUserCode;
    if($sessionrole=="admin")
    {
      $sql ="SELECT * FROM mandates ORDER BY createdon DESC";
      $result = mysqli_query($connect,$sql);
    }
    else{
      $sql ="SELECT * FROM mandates WHERE usercode ='$Client_Id' ORDER BY createdon DESC ";
      $result = mysqli_query($connect,$sql);
    }
?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">

    <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
  <h2 align="center">DebitCheck Mandate Cancellations	</h2>
    </div>
      <div class='row'>
	    <div class='input-group margin-bottom-20'>
            <span class='input-group-addon'>
                <i class='fa fa-user'></i>
            </span>
            <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>

        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="">
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>

			
			<select class="form-control" id="Frequency" name="Frequency" size="1" style='z-index:0'>
  						  <?php
						  	$FrequencySelect = $Frequency;

						  foreach($FrequencyArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  
						  }
						  ?>
            </select>
			
			</div>
		</div>
        </div>
      </form>
    </div>
  </div>
</br>
</br>
<div class="table-responsive">
<div class="messages" id="message" name="message">
  		 <?php
		 if (isset($message)) 	
		 {
			 if($message == 'updated successfully')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);	
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);	
			 }		
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}					
			}				
		 }
		 ?>
  </div>  
<table id="collections" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
			<th>Reference</th>
			<th>Contract Reference</th>   
			<th>Frequency</th>
			<th>Tracking Cancellation Indicator</th>
			<th>Debtor Account Number</th>
		    <th>Collection Amount</th>				
			<th>mandate_initiation_date</th>			
			<th>Cancellation Reason</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="6" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>
      </tfoot>
      <?php

        while($row = mysqli_fetch_array($result))
        {
			$reference 							= $row["reference"];
			$idFrequency                  		= "Frequency_$reference";$Frequency                          			= $row['frequency'];			
			$iddebtor_account_number  	  		= "debtor_account_number_$reference";$debtor_account_number  	        = $row['debtor_account_number'];  				
			
			$idcontract_reference		  		= "contract_reference_$reference";$contract_reference					= $row['contract_reference'];
			$idtracking_cancellation_indicator	= "tracking_cancellation_indicator_$reference";$tracking_cancellation_indicator		= $row['tracking_cancellation_indicator'];	
			$idcancellation_reason		  		= "cancellation_reason_$reference";$cancellation_reason					= $row['cancellation_reason'];
			
			/*$idFrequency                  		= "Frequency_$reference";$Frequency                          = $row['frequency'];
			$idmandate_initiation_date	  		= "mandate_initiation_date_$reference";$mandate_initiation_date	        = $row['mandate_initiation_date'];
			$idfirst_collection_date      		= "first_collection_date_$reference";$first_collection_date              = $row['first_collection_date'];
			$idmaximum_collection_amount  		= "maximum_collection_amount_$reference";$maximum_collection_amount	        = $row['maximum_collection_amount'];	
			$iddebtor_account_name 	      		= "debtor_account_name_$reference";$debtor_account_name 	            = $row['debtor_account_name']; 	
			$iddebtor_identification 	  		= "debtor_identification_$reference";$debtor_identification 	            = $row['debtor_identification']; 	
			$iddocument_type	          		= "document_type_$reference";$document_type	                    = $row['document_type'];	
			*/
			/*$iddebtor_account_type	      		= "debtor_account_type_$reference";$debtor_account_type	            = $row['debtor_account_type'];	
			$iddebtor_branch_number  	  		= "debtor_branch_number_$reference";$debtor_branch_number  	            = $row['debtor_branch_number'];  	
			$iddebtor_contact_number	  		= "debtor_contact_number_$reference";$debtor_contact_number			    = $row['debtor_contact_number'];			
			$iddebtor_email               		= "debtor_email_$reference";$debtor_email                       = $row['debtor_email']; 
            $idcollection_date            		= "collection_date_$reference";$collection_date                    = $row['collection_date']; 
            $iddate_adjustment_rule_indicator 	= "date_adjustment_rule_indicator_$reference";$date_adjustment_rule_indicator     = $row['date_adjustment_rule_indicator'];
            $idadjustment_category        		= "adjustment_category_$reference";$adjustment_category                = $row['adjustment_category']; 
            $idadjustment_rate            		= "adjustment_rate_$reference";$adjustment_rate                    = $row['adjustment_rate'];
            $idadjustment_amount          		= "adjustment_amount_$reference";$adjustment_amount                  = $row['adjustment_amount']; 
            $idfirst_collection_amount 	  		= "first_collection_amount_$reference";$first_collection_amount 	        = $row['first_collection_amount']; 	
            $iddebit_value_type           		= "debit_value_type_$reference";$debit_value_type                   = $row['debit_value_type']; */
										
          echo 
		  '<tr>
            <td width="5%">'.$row["reference"].'</td>
			<td width="5%"><input type ="text" id = "'.$idcontract_reference  	  		.'" value="'.$contract_reference.'" required="required" readonly /></td>
			<td width="5%"><input type ="text" id = "'.$idFrequency  	  		.'" value="'.$Frequency.'" required="required" readonly /></td>
			<td width="5%"><input type ="text" id = "'.$idtracking_cancellation_indicator  	  		.'" value="'.$tracking_cancellation_indicator.'" required="required" readonly /></td>			
			<td width="5%"><input type ="text" id = "'.$iddebtor_account_number  	  		.'" value="'.$debtor_account_number.'" required="required" readonly /></td>						
		    <td width="5%">'.$row["collection_amount"].'</td>            
			<td width="5%">'.$row["mandate_initiation_date"].'</td>
			<td width="5%"><input type ="text" id = "'.$idcancellation_reason  .'" value="'.$cancellation_reason.'" required="required" readonly /></td>';
			/*if($adjustment_category == 'N')
			{
			echo '<td width="5%"><input type ="number" id = "'.$idadjustment_rate            		.'" value="'.$row["adjustment_rate"].'" style="display:none" readonly /></td>
				 <td width="5%"><input type ="number" id = "'.$idadjustment_amount          		.'" value="'.$row["adjustment_amount"].'" style="display:none" readonly /></td>';				
			}
			else
			{
			echo '<td width="5%"><input type ="number" id = "'.$idadjustment_rate            		.'" value="'.$row["adjustment_rate"].'" style="display:block" readonly /></td>
				 <td width="5%"><input type ="number" id = "'.$idadjustment_amount          		.'" value="'.$row["adjustment_amount"].'" style="display:block" readonly /></td>';
            }
			echo '<td width="5%"><input type ="number" id = "'.$idfirst_collection_amount 	  		.'" value="'.$row["first_collection_amount"].'" readonly required="required" data-error="debtor_account_name is required." /></td>';
			if($adjustment_category == 'N')
			{
				echo '<td width="5%"><input type ="text" id = "'.$iddebit_value_type           		.'" value="'.$row["debit_value_type"].'" style="display:none" readonly /></td>';
			}
			else
			{
				echo '<td width="5%"><input type ="text" id = "'.$iddebit_value_type           		.'" value="'.$row["debit_value_type"].'" style="display:block" readonly /></td>';				
			}
			echo '<td width="5%">';
			if($row["first_collection_date"] > date('Y-m-d'))
			{
				echo '<a class="btn btn-success" id = "'.$reference.'" name = "'.$reference.'" onclick="toggleUpdate(this);" >Edit</a></td>';
				echo '<td width="5%"><a class="btn btn-danger" id = "c'.$reference.'" name = "c'.$reference.'" onclick="cancelUpdate(this);" style="display:none" >Cancel</a></td>';
			}
			else
			{
				echo "<p></p>";
			}
			*/
			echo '&nbsp;';
			echo '</td>';
			
			echo '</tr>';
        }
      ?>


  </table>
    </div>
    </br>
</div>

<script>


$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[6]) || 0;
 //alert(Action_Date);
    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 5)
            .data()
            .reduce( function (a, b) {
                //\return (number_format((intVal(a) + intVal(b)),2'.', ''));
				//alert(a);
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 5 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );
		
    }


});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#reference').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#frequency').on('change', function(){
   table.search( this.value ).draw();
});

});

function cancelUpdate(ele)
{
	var id 								 = ele.id;//substr(ele.id,1);
	id = id.substr(1);
		//alert(id);
	var idtracking_indicator		  	 = "tracking_indicator_"+id;		  	
	var idFrequency                  	 = "Frequency_"+id;                  	
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;	  	
	var idfirst_collection_date      	 = "first_collection_date_"+id;      	
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;  	
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id; 	      	
	var iddebtor_identification 	  	 = "debtor_identification_"+id; 	  	
	var iddocument_type	          		 = "document_type_"+id;	          		
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;  	  	
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;	      	
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;  	  	
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;	  		
	var iddebtor_email               	 = "debtor_email_"+id;               	
	var idcollection_date            	 = "collection_date_"+id;            	
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;        	
	var idadjustment_rate            	 = "adjustment_rate_"+id;            	
	var idadjustment_amount          	 = "adjustment_amount_"+id;          	
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id; 	  	
	var iddebit_value_type           	 = "debit_value_type_"+id;       
	
	var idcontract_reference		  		= "contract_reference_"+id;
	var idtracking_cancellation_indicator	= "tracking_cancellation_indicator_"+id;	
	var idcancellation_reason		  		= "cancellation_reason_"+id;
	
	// -- return to original position.
		document.getElementById(id).innerHTML = 'Edit';
		ele.style.display = "none";
		/// -- idtracking_cancellation_indicator					--//
		id_= idtracking_cancellation_indicator;
		value = document.getElementById(id_).value;		
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el; 
		
		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		
		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;	

		// -- idcontract_reference  	  			--//
		id_= idcontract_reference;
		document.getElementById(id_).readOnly = true;	
		
		// -- idcancellation_reason  	  			--//
		id_= idcancellation_reason;
		value = document.getElementById(id_).value;	
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}		
		new_html_el = "<input type ='text' readonly id = "+id_+" value='"+value+"'  />";
		document.getElementById(id_).outerHTML = new_html_el; 
}
// #myInput is a <input type="text"> element
function toggleUpdate(ele) 
{	
	var id 								 = ele.id;	 				
	var idtracking_indicator		  	 = "tracking_indicator_"+id;		  	
	var idFrequency                  	 = "Frequency_"+id;                  	
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;	  	
	var idfirst_collection_date      	 = "first_collection_date_"+id;      	
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;  	
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id; 	      	
	var iddebtor_identification 	  	 = "debtor_identification_"+id; 	  	
	var iddocument_type	          		 = "document_type_"+id;	          		
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;  	  	
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;	      	
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;  	  	
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;	  		
	var iddebtor_email               	 = "debtor_email_"+id;               	
	var idcollection_date            	 = "collection_date_"+id;            	
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;        	
	var idadjustment_rate            	 = "adjustment_rate_"+id;            	
	var idadjustment_amount          	 = "adjustment_amount_"+id;          	
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id; 	  	
	var iddebit_value_type           	 = "debit_value_type_"+id;           	

	var idcontract_reference		  		= "contract_reference_"+id;
	var idtracking_cancellation_indicator	= "tracking_cancellation_indicator_"+id;	
	var idcancellation_reason		  		= "cancellation_reason_"+id;

    var value = '';
	// -- if Equal save it mean we are on edit mode
	// -- some elements needs to be change from text to dropdown. 
	if(ele.innerHTML == 'Save')
	{
		var objects_array = null;
		// -- Validate Required if Empty.
		id_= idtracking_cancellation_indicator;
		value = document.getElementById(id_).value;		
		// -- if tracking_cancellation_indicator is 'N', reason is not required, else 'Y' is required. 
		if(value === 'Y')
		{
			objects_array = 
			{'cancellation_reason': idcancellation_reason};			
		}
		else
		{
		 // -- Update debicheck mandate.
		 objects_array = 
			{'tracking_cancellation_indicator': idtracking_cancellation_indicator };			
		}
		
		if(IsEmpty(objects_array))
		{
		ele.innerHTML = 'Edit';
		
		/// -- idtracking_cancellation_indicator					--//
		id_= idtracking_cancellation_indicator;
		value = document.getElementById(id_).value;		
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el; 
		
		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		
		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;	

		// -- idcontract_reference  	  			--//
		id_= idcontract_reference;
		document.getElementById(id_).readOnly = true;	
		
		// -- idtracking_cancellation_indicator  	  			--//
		id_= idcancellation_reason;
		value = document.getElementById(id_).value;	
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}		
		new_html_el = "<input type ='text' readonly id = "+id_+" value='"+value+"'  />";
		document.getElementById(id_).outerHTML = new_html_el; 

		// -- Values to updated.
		var values_array = 
			{'tracking_cancellation_indicator' 			: document.getElementById(idtracking_cancellation_indicator).value		  			  	
			,'cancellation_reason' 						: document.getElementById(idcancellation_reason).value};		
		
		// -- Pass reference & array of values to updated.
		updatedebicheckmandate(id,values_array); 
		
		id_ = "c"+ele.id;
		document.getElementById(id_).style.display = 'none';
	   }
	}
	else 
	{
		id_ = "c"+ele.id;
		//alert(id_);
		document.getElementById(id_).style.display = 'block';
		ele.innerHTML = 'Save';
		// -- convert php array into jQuery arrays.
		// -- tracking_indicator					--//	
		Arr = <?php echo json_encode($tracking_indicatorArr); ?>;
		id_ = idtracking_cancellation_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' onChange='tracking_cancellation_indicator(this,"+idcancellation_reason+");'>";
		for (var key in Arr) 
		{
			row = new Array();
			row = Arr[key].split('|');		
		  if(value === row[0])	// -- selected		
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el; 

		// -- idcancellation_reason	  			--//
		id_= idcancellation_reason;
		Arr = <?php echo json_encode($debtor_cancellation_codes_Arr); ?>;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr) 
		{
			row = new Array();
			row = Arr[key].split('|');
			
		  if(value === row[0])	// -- selected		
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el; 
	}
} 


function updatedebicheckmandate(id,list) 
{
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"canceldebicheckmandate.php",
				       data:{id_:id,list:list},
						success: function(data)
						 {	
							alert(data);
							jQuery("#message").html(data);
						 }
					});
				});			  
			  return false;
}
function valueselect2(sel,ref) 
 { 	
		idcollection_day =  ref.id;
		var frequency = sel.options[sel.selectedIndex].value;    
		var selectedCollection_Day = ref.value;
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectCollectionDays.php",
				       data:{frequency:frequency,collection_day:selectedCollection_Day,id_:idcollection_day},
						success: function(data)
						 {	
							//alert(data);
							jQuery("#"+idcollection_day).replaceWith(data);
						 }
					});
				});	
					
			  return false;
  }
function adjustment_category1(sel,rate,amount,value_type) 
 {
	var adjustment_category = sel.options[sel.selectedIndex].value;   
		
	if(adjustment_category == 'N')
	{
	  	rate.style.display = "none";
	    amount.style.display = "none";
		value_type.style.display = "none";
		
		rate.value = '0,00';
		amount.value = '0,00';
		value_type.selectedIndex = 0; //Default to Fixed contract		
	}
	else
	{
	  	rate.style.display = "block";	  	
		amount.style.display = "block";
		value_type.style.display = "block";
	}	
  			  return false;
 }  
 
 function tracking_cancellation_indicator(sel,cancellation_reason)
 {
	 	var tci = sel.options[sel.selectedIndex].value;   
		if(tci == 'N')
		{
			cancellation_reason.selectedIndex =  0; // -- Default to Fixed blank reason code	
		}
		else
		{
			cancellation_reason.selectedIndex =  0; // -- Default to Fixed blank reason code	
		}
 }
 isObject = function(Obj) 
 {
                 // object is of type Element 
            return Obj instanceof Element; 
};

function IsEmpty(list) 
{ // Going throuh a list to validate mandatory fields.
	for(var item in list) 
	{
		id_ = list[item];
		value_ = document.getElementById(id_);
		//alert(value_.value);

		if(value_.value  === " " || value_.value  === "")
		{		value_.focus();
			    alert(id_+" is required.");
				    return false;
		}
	}
  return true;
}
</script>
