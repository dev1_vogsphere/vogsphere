<!-- Start Update the eLoan - Application Data -->
<?php

//require 'database.php';
$path = "workflow/engine/";
require $path.'xpdl.php';

$tbl_customer = "customer";
$tbl_loanapp = "loanapp";
$debitorderclient = '';
$debitorderclientstyle = 'block';
$valid = true;
$debicheckusercode = getsession('debicheckusercode');
$sqlStaged		   = '';
$json			   = '';
$datajsonStaged	   = array();
$lockedforchanges  = '';
$databasename 	   = 'ecashpdq_paycollections';

// -- Trigger Business Process Event
if(!function_exists('trigger_event'))
{
	function trigger_event($Data)
	{
		trigger_process_event('CHANGE_AGREGATE_LIMITS',$Data);
	}
}
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 -
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 -
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);
// ---------------------- EOC - Account Types ------------------------------- //

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 -
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);
// ---------------------- EOC - Payment Method ------------------------------- //

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 -
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);
// ---------------------- EOC - Payment Frequency ------------------------------- //

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 -
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);
// ---------------------- EOC - Payment Frequency ------------------------------- //
	Database::disconnect();
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
$paymentfrequency = '';
// EOC -- 15.04.2017 -- Updated.
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
$readonly = '';

$customerid = null;
$ApplicationId = null;
$usercode = null;

$count = 0;

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$rejectreason = "";
$rejectreasonError = "";

// -- BOC Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = "";
$ApplicationIdEncoded = "";
$usercodeEncoded = "";

// -- Declarations
$pdo1 = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- customer details
$Title 			= getpost('Title');
$FirstName 		= getpost('FirstName');
$LastName 		= getpost('LastName');
$Street 		= getpost('Street');
$Suburb 		= getpost('Suburb');
$City 			= getpost('City');
$State 			= getpost('State');
$PostCode 		= getpost('PostCode');
$Dob			= getpost('Dob');
$phone 			= getpost('phone');
$email 			= getpost('email');
$Original_email = getpost('Original_email');
$alias 			= getpost('alias');
// -- errors
$TitleError = null;
$FirstNameError = null;
$LastNameError = null;
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$DobError = null;
$phoneError = null;
$emailError = null;

// -- client
$ID 						 = '';
$Description 			     = '';
$Username 				     = '';
$Password 				     = '';
$aggregate_limits_debits     = '';
$aggregate_limits_naedo 	 = '';
$aggregate_limits_cards 	 = '';
$aggregate_limits_credits    = '';
$item_limits_debits 		 = '';
$item_limits_naedo 		     = '';
$item_limits_cards 		     = '';
$item_limits_credits 	     = '';
$changedby 					 = '';
$changedon 					 = '';
$changedat	                 = '';

// -- error.
$IDError 						 = '';
$DescriptionError 			     = '';
$UsernameError 				     = '';
$PasswordError 				     = '';
$aggregate_limits_debitsError    = '';
$aggregate_limits_naedoError 	 = '';
$aggregate_limits_cardsError 	 = '';
$aggregate_limits_creditsError   = '';
$item_limits_debitsError 		 = '';
$item_limits_naedoError 		 = '';
$item_limits_cardsError 		 = '';
$item_limits_creditsError 	     = '';
// -- user
$userid 						 = '';
$password						 = '';
$email 							 = '';
$role 							 = '';
$alias							 = '';

// -- error
$useridError 						 = '';
$passwordError						 = '';
$emailError 						 = '';
$roleError 							 = '';
$aliasError							 = '';

$location = "Location: customerLogin";
// -- EOC Encrypt 16.09.2017 - Parameter Data.
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}

	if ( !empty($_GET['usercode']))
	{
			$usercode = $_REQUEST['usercode'];
			$usercodeEncoded = $usercode;
			$usercode = base64_decode(urldecode($usercode));
	}

$IntUserCode = getsession('IntUserCode');

$Original_email = '';
$message = array();
// debug
//$table['name'] = 'client';
//$table['fields'] = 'Description';
//$actions = array();

//$disp = getheader($actions,$table);
//print_r($disp['fielname']);
// debug

if ( !empty($_POST))
{
	$message  = null;
	$debitorderclientstyle = getpost('debitorderclientstyle');
	$Original_email 	   = getpost('Original_email');
	$email 				   = getpost('email');
	$customerid 		   = getpost('customerid');
	$Title 		   = getpost('Title');

	$FirstName 		   = getpost('FirstName');
	$LastName 		   = getpost('LastName');
	$Street 		   = getpost('Street');
	$Suburb 		   = getpost('Suburb');

	$City 		   = getpost('City');
	$State 		   = getpost('State');
	$PostCode 		   = getpost('PostCode');
	$Dob 		   = getpost('Dob');
	$phone  = getpost('phone');
	$lockedforchanges = getpost('lockedforchanges');
	$alias 			= getpost('alias');

$ID = getpost('ID');
$Description = getpost('Description');
$Username = getpost('Username');
$password = getpost('password');

$changedby 			   = getpost('changedby');
$changedon 			   = getpost('changedon');
$changedat	           = getpost('changedat');
$readonly 			   = getpost('readonly');
$databasename 	       = getpost('databasename');

$debicheckusercode = getsession('debicheckusercode');

$aggregate_limits_debits = getpost('aggregate_limits_debits');
$aggregate_limits_naedo = getpost('aggregate_limits_naedo');
$aggregate_limits_cards = getpost('aggregate_limits_cards');
$aggregate_limits_credits = getpost('aggregate_limits_credits');

if(empty($aggregate_limits_debits)){$aggregate_limits_debits = '0.00';}
if(empty($aggregate_limits_naedo)){$aggregate_limits_naedo = '0.00';}
if(empty($aggregate_limits_cards)){$aggregate_limits_cards = '0.00';}
if(empty($aggregate_limits_credits)){$aggregate_limits_credits = '0.00';}

$item_limits_debits = getpost('item_limits_debits');
$item_limits_naedo = getpost('item_limits_naedo');
$item_limits_cards = getpost('item_limits_cards');
$item_limits_credits = getpost('item_limits_credits');

if(empty($item_limits_debits)){$item_limits_debits = '0.00';}
if(empty($item_limits_naedo)){$item_limits_naedo = '0.00';}
if(empty($item_limits_cards)){$item_limits_cards = '0.00';}
if(empty($item_limits_credits)){$item_limits_credits = '0.00';}

	$count = 0;
	//$TitleError = 'sas';
		$valid = true;
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}

		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}

		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}

		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}

		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}

		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}

		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}

		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}

		if (empty($Dob)) { $DobError = 'Please enter Date of birth'; $valid = false;}

	// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}

		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}


	// -- validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}

	if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
	else
	{
	// -- User - E-mail.
		if ($valid and ($Original_email != $email))
		{
			$query = "SELECT * FROM user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}

			if($valid)
			{
				$query = "SELECT * FROM customer WHERE email2 = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use.'; $valid = false;}
			}
		}
	}
// ------------------ Staged Data compared with New DATA  --------------
	if($debitorderclientstyle == 'block')
	{
// -- Staged data to restore on rejection.
	$sqlStaged = getpost('sqlStaged'); // -- stagedsqlstatment
	$json      = getpost('json');	   // --- stagedjsondata
	$posted_data = array();
    if (!empty($json))
	{
        $posted_data = json_decode(getpost('json'), true);
		//$json = htmlspecialchars(json_encode($posted_data,true));
    }
	// -- New Updated Data.
	// -- DATA json array
	$dataNew = null;
	$dataNew[] =  getpost('ID');
	$dataNew[] =  getpost('aggregate_limits_debits');
	$dataNew[] =  getpost('aggregate_limits_naedo');
	$dataNew[] =  getpost('aggregate_limits_cards');
	$dataNew[] =  getpost('aggregate_limits_credits');
	$dataNew[] =  getpost('item_limits_debits');
	$dataNew[] =  getpost('item_limits_naedo');
	$dataNew[] =  getpost('item_limits_cards');
	$dataNew[] =  getpost('item_limits_credits');
	$dataNew[] =  getpost('Username');
	$dataNew[] =  getpost('password');
	$dataNew[] =  getpost('changedby');
	$dataNew[] =  getpost('changedon');
	$dataNew[] =  getpost('changedat');
	$dataNew[] =  getpost('Description');
	// -- Compare staged data witn New Data
	$result = null;
	if(!empty($posted_data))
	  {
		$result=array_diff($posted_data,$dataNew);
	  }
	//print_r($result);

	if(empty($result))
	{
		// -- We ignore changed by, changed on, changed at.
		 if(!isset($result[13]))
		 {
			 $message[] = 'No New changes were made'; $valid = false;
		 }
		 else
		 {
			 $readonly = 'readonly';
		 }
	}
  }
// ---------------------------------------------------------------------
	if($valid)
	{
			$changedby 			   = $customerid;
			$changedon 			   = date('Y-m-d');
			$changedat	           = date('h:m:s');

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// Update User Email
			$sql = "UPDATE user SET email = ? WHERE userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($email,$customerid));
			Database::disconnect();
			$count++;

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE $tbl_customer";
			$sql = $sql." SET Title = ?,FirstName = ?,LastName = ?,Street = ?,Suburb = ?,City = ?,State = ?,PostCode = ?,Dob = ?,phone = ?,email2 = ?,changedby = ?,changedon = ?";
			$sql = $sql." WHERE customerid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone,$email,$changedby,$changedon,$customerid));
			Database::disconnect();
			$count++;

			// -- Update Client Table
			if(!empty($ID))
			{
				$lockedforchanges 			 = 'X';
				$pdo = Database::connectDB();
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "UPDATE client SET aggregate_limits_debits = ?,
				aggregate_limits_naedo = ?,aggregate_limits_cards = ?,aggregate_limits_credits = ?,item_limits_debits = ?,
				item_limits_naedo = ?,item_limits_cards = ?,item_limits_credits = ?,
				changedby = ?,changedon = ?,changedat = ?,lockedforchanges = ? WHERE ID = ?";
				$q = $pdo->prepare($sql);
				$q->execute(array($aggregate_limits_debits,$aggregate_limits_naedo,$aggregate_limits_cards,
				$aggregate_limits_credits,$item_limits_debits,$item_limits_naedo,$item_limits_cards,
				$item_limits_credits,$changedby,$changedon,$changedat,$lockedforchanges,$ID));
				Database::disconnect();
				$count++;
			}
///////////////////////////// ---------------------------------- SESSION DECLARATIONS --------------------------/////////
		// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = '';

			// Id number
			$_SESSION['idnumber']  = $customerid;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			$_SESSION['Street'] = $Street;
			// Suburb
			$_SESSION['Suburb'] = $Suburb;
			// City
			$_SESSION['City'] = $City;
			// State
			$_SESSION['State'] = $State;
			// PostCode
			$_SESSION['PostCode'] = $PostCode;
			// Dob
			$_SESSION['Dob'] = $Dob;
			// phone
			$_SESSION['phone'] = $phone;

// -- if is a Debit Order Client Trigger Event for Approval.
	if($debitorderclientstyle == 'block')
	{
		if(empty(getpost('lockedforchanges')))
		{
		// -- TRIGGER EVENT UPDATE_AGREGATE_LIMITS -- //
		// -- Task Data
			$Data[0]['DATA_ID']    = 'currentprocessor';
			$Data[0]['DATA_VALUE'] = $customerid ;

		// -- N.B : It is important to always pass them
			$Data[1]['DATA_ID']    = 'currenttask';
			$Data[1]['DATA_VALUE'] = '';

			$Data[2]['DATA_ID']    = 'currenttaskstatus';
			$Data[2]['DATA_VALUE'] = '';

		// -- Debicheckusercode Or NAEDO usercode
		if(!empty($debicheckusercode))
		{
			$Data[3]['DATA_ID']    = 'usercode';
			$Data[3]['DATA_VALUE'] = $debicheckusercode;
		}
		if(!empty($usercode))
		{
			$Data[3]['DATA_ID']    = 'usercode';
			$Data[3]['DATA_VALUE'] = $usercode;
		}
		// -- emailtemplate
		$emailtemplate = 'update_limits';
		if(!empty($emailtemplate))
		{
			$Data[4]['DATA_ID']    = 'emailtemplate';
			$Data[4]['DATA_VALUE'] = $emailtemplate;
		}
		// -- mailto - Mercantile Email for Limit Update.
		$mailto = '';
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "select * from globalsettings";
		$q = $pdo->prepare($sql);
		$q->execute();
		$dataglobalsetting = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();

		// -- Update Address & Company details.
		if($dataglobalsetting['globalsettingsid'] >= 1)
		{
			$mailto = $dataglobalsetting['email_mercantile_limit'];
		}
		else
		{
			$mailto  = 'info@ecashmeup.com';
		}

		if(!empty($usercode))
		{
			$Data[5]['DATA_ID']    = 'mailto';
			$Data[5]['DATA_VALUE'] = $mailto;
		}

		// -- table key value
		if(!empty($Description))
		{
			$Data[6]['DATA_ID']    = 'tablekeyvalue';
			$Data[6]['DATA_VALUE'] = $Description;
		}

		// -- table name
		if(!empty($Description))
		{
			$Data[7]['DATA_ID']    = 'tablename';
			$Data[7]['DATA_VALUE'] = 'client';
		}

		// -- table key
		if(!empty($Description))
		{
			$Data[8]['DATA_ID']    = 'tablekey';
			$Data[8]['DATA_VALUE'] = 'Description';
		}

		// -- workflow_initiator
		if(!empty($Description))
		{
			$Data[9]['DATA_ID']    = 'workflow_initiator';
			$Data[9]['DATA_VALUE'] = $customerid;
		}

		// -- sqlstagestatement
		if(!empty($Description))
		{
			$Data[10]['DATA_ID']    = 'sqlstagestatement';
			$Data[10]['DATA_VALUE'] = $sqlStaged;
		}

		// -- sqlstagedata
		if(!empty($Description))
		{
			$Data[11]['DATA_ID']    = 'sqlstagedata';
			$Data[11]['DATA_VALUE'] = $json;
		}
		// -- databasename
		if(!empty($databasename))
		{
			$Data[12]['DATA_ID']    = 'databasename';
			$Data[12]['DATA_VALUE'] = $databasename;
		}

		// -- table display fields, separated by comma ,.
$fields = "Description,aggregate_limits_debits,aggregate_limits_naedo,aggregate_limits_cards
,aggregate_limits_credits,item_limits_debits,item_limits_naedo,item_limits_cards,
item_limits_credits";
		if(!empty($databasename))
		{
			$Data[13]['DATA_ID']    = 'fields';
			$Data[13]['DATA_VALUE'] = $fields;
		}

		$readonly = 'readonly';
		// -- Trigger Business Process Event.
		$message[] = 'Information has been updated successfully & workflow has been initaited to the relavant approvers.';
		trigger_event($Data);
		// -- END TRIGGER EVENT --- //
	  }
	 }
	}
}
else
{
	// -- 1. Customer Details
$sql =  "select * from $tbl_customer where CustomerId = ?"; // AND usercode = ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid));//,$usercode));
$data = $q->fetch(PDO::FETCH_ASSOC);
	//echo 'customerid 0:'.$customerid;

// -- searched via alias
if(empty($data['CustomerId']))
{
	//echo 'customerid 1:'.$customerid.' - '.$data['CustomerId'];
	if( $data['CustomerId'] == 0)
	{
		$sql =  "select * from $tbl_customer where alias = ?"; // AND usercode = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));//,$usercode));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		if(!empty($data))
		{
			$customerid = $data['CustomerId'];
		}
	}
}
	// -- 2. ecashpdq_paycollections.client Details

$sql =  "select * from client where Username = ?";
$q1 = $pdo1->prepare($sql);
$q1->execute(array($usercode));
$data1 = $q1->fetch(PDO::FETCH_ASSOC);

if(empty($data1))
{ // not a debit order client
	$debitorderclientstyle = 'none';
}
else
{
					 // -- Build Staging Data for restoration if request is rejected.
// -- SQL statement
$sqlStaged = "
UPDATE client SET ID 	 = ?,
aggregate_limits_debits  = ?,
aggregate_limits_naedo   = ?,
aggregate_limits_cards   = ?,
aggregate_limits_credits = ?,
item_limits_debits 	   	 = ?,
item_limits_naedo 	   	 = ?,
item_limits_cards 	   	 = ?,
item_limits_credits 	 = ?,
username 				 = ?,
password  			     = ?,
changedby 			     = ?,
changedon 			     = ?,
changedat 			     = ?,
lockedforchanges		 = ?

WHERE Description 	     = ?";
// -- DATA json array
$datajsonStaged[] = $data1['ID'];
$datajsonStaged[] = $data1['aggregate_limits_debits'];
$datajsonStaged[] = $data1['aggregate_limits_naedo'];
$datajsonStaged[] = $data1['aggregate_limits_cards'];
$datajsonStaged[] = $data1['aggregate_limits_credits'];
$datajsonStaged[] = $data1['item_limits_debits'];
$datajsonStaged[] =  $data1['item_limits_naedo'];
$datajsonStaged[] =  $data1['item_limits_cards'];
$datajsonStaged[] =  $data1['item_limits_credits'];
$datajsonStaged[] =  $data1['username'];
$datajsonStaged[] =  $data1['password'];
$datajsonStaged[] =  $data1['changedby'];
$datajsonStaged[] =  $data1['changedon'];
$datajsonStaged[] =  $data1['changedat'];
$datajsonStaged[] =  $data1['lockedforchanges'];
$datajsonStaged[] =  $data1['Description'];

$json = htmlspecialchars(json_encode($datajsonStaged,true));

// -- disable limit changes, if usercode & Internal User Code are different.
	if($usercode != $IntUserCode)
	{
		$debitorderclientstyle = 'none';
	}
}


$ID 						 = '';//$data1['ID'];
$Description 			     = '';//$data1['Description'];
$Username 				     = '';//$data1['username'];
$password 				     = '';//$data1['password'];
if(isset($data1['username']))
{
	$Username 				     = $data1['username'];
}

if(isset($data1['password']))
{
	$password 				     = $data1['password'];

}

if(isset($data1['Description']))
{
	$Description 			     = $data1['Description'];

}
if(isset($data1['ID']))
{
	$ID 						 = $data1['ID'];

}
$lockedforchanges 			 = $data1['lockedforchanges'];
if(!empty($lockedforchanges))
{
	$readonly = 'readonly';
}
$aggregate_limits_debits     = $data1['aggregate_limits_debits'];
$aggregate_limits_naedo 	 = $data1['aggregate_limits_naedo'];
$aggregate_limits_cards 	 = $data1['aggregate_limits_cards'];
$aggregate_limits_credits    = $data1['aggregate_limits_credits'];

if(empty($aggregate_limits_debits)){$aggregate_limits_debits = '0.00';}
if(empty($aggregate_limits_naedo)){$aggregate_limits_naedo = '0.00';}
if(empty($aggregate_limits_cards)){$aggregate_limits_cards = '0.00';}
if(empty($aggregate_limits_credits)){$aggregate_limits_credits = '0.00';}

$item_limits_debits 		 = $data1['item_limits_debits'];
$item_limits_naedo 		     = $data1['item_limits_naedo'];
$item_limits_cards 		     = $data1['item_limits_cards'];
$item_limits_credits 	     = $data1['item_limits_credits'];

if(empty($item_limits_debits)){$item_limits_debits = '0.00';}
if(empty($item_limits_naedo)){$item_limits_naedo = '0.00';}
if(empty($item_limits_cards)){$item_limits_cards = '0.00';}
if(empty($item_limits_credits)){$item_limits_credits = '0.00';}

$changedby 					 = $data['changedby'];
$changedon 					 = $data['changedon'];
$changedat					 = $data1['changedat'];

// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
if(empty($data))
{
	//header("Location: userprofile");
}
// -- EOC Encrypt 16.09.2017 - Parameter Data.

// -- UserEmail Address
$sql =  "select * from user where userid= ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid));
$dataEmail = $q->fetch(PDO::FETCH_ASSOC);

// Customer Details
$Title = $data['Title'];
$FirstName = $data['FirstName'];
$LastName = $data['LastName'];
$Street = $data['Street'];
$Suburb = $data['Suburb'];
$City = $data['City'];
$State = $data['State'];
$PostCode = $data['PostCode'];
$Dob		= $data['Dob'];

$phone = $data['phone'];
$email =  $dataEmail['email'];
$Original_email = $dataEmail['email'];
$alias = $data['alias'];

	}
?>
<!-- End Update the eLoan - Application Data -->

<div class="container background-white bottom-border">
		<div class="row">

 <!-- <div class="span10 offset1"> -->
  <form  action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
    <div class="table-responsive">
<table class="table table-user-information">
<!-- Customer Details -->
<!-- Title,FirstName,LastName -->

<tr>
	<!--<td><div><a class="btn btn-primary" href="custAuthenticate">Back</a></div></td> -->
	<td><div><button type="submit" class="btn btn-primary">Update</button></div></td>
</tr>
<tr>
<td colspan="3">
<div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>
</div>

<?php # error messages
		if (isset($message))
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Information updated successfully!</p>\n", $count);
		}
		// -- if there any error display here.
		else
		{
				// keep track validation errors - 		// Customer Details
if(!empty($TitleError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($FirstNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($LastNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StreetError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($SuburbError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($CityError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($PostCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($DobError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($phoneError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($emailError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}
		}
		?>

</td>

</tr>

<tr>
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label">Title</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="Title" type="text"  placeholder="Title" value="<?php echo !empty($Title)?$Title:'';?>"> -->

			<SELECT class="form-control" id="Title" name="Title" size="1">
						<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
						<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
						<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
						<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
						<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
			</SELECT>

			<?php if (!empty($TitleError)): ?>
			<span class="help-inline"><?php echo $TitleError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label">First Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName"
			value="<?php echo !empty($FirstName)?$FirstName:'';?>">
			<?php if (!empty($FirstNameError)): ?>
			<span class="help-inline"><?php echo $FirstNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label">Last Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
			<?php if (!empty($LastNameError)): ?>
			<span class="help-inline"><?php echo $LastNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

</tr>

<!-- Street,Suburb,City -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label">Street</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
			<?php if (!empty($StreetError)): ?>
			<span class="help-inline"><?php echo $StreetError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label">Suburb</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
			<?php if (!empty($SuburbError)): ?>
			<span class="help-inline"><?php echo $SuburbError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label">City</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
			<?php if (!empty($CityError)): ?>
			<span class="help-inline"><?php echo $CityError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>

<!-- State,PostCode,Dob -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label">Province</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="State" type="text"  placeholder="Province" value="<?php echo !empty($State)?$State:'';?>"> -->
			    <SELECT class="form-control" name="State" size="1">
								<!-------------------- BOC 2017.04.15 - Provices  --->
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}

											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->
				</SELECT>
			<?php if (!empty($StateError)): ?>
			<span class="help-inline"><?php echo $StateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label">Postal Code</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
			<?php if (!empty($PostCodeError)): ?>
			<span class="help-inline"><?php echo $PostCodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label">Date of birth</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  value="<?php echo !empty($Dob)?$Dob:'';?>">
			<?php if (!empty($DobError)): ?>
			<span class="help-inline"><?php echo $DobError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>

<tr>
<td>
<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
</td>
<td>
<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">Email
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
									<p>
				<input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
									</p>
								</div>
</td>
<td>
<div class="control-group">
<label>Debi Check Usercode</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="debicheckusercode" name="debicheckusercode" type="text" value="<?php echo !empty($debicheckusercode)?$debicheckusercode:'';?>" />
	</div>
</div>
</td>
</tr>

<tr>
<td>
<div class="control-group">
<label>UserCode</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="UserCode1" name="UserCode1" type="text"  value="<?php echo !empty($usercode)?$usercode:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>Internal Usercode</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="IntUserCode" name="IntUserCode" type="text" value="<?php echo !empty($IntUserCode)?$IntUserCode:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>Alias</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="alias" name="alias" type="text" value="<?php echo !empty($alias)?$alias:'';?>" />
	</div>
</div>
</td>
</tr>
</table>
<!-----------------------------------------------------------  Agregates Details ---------------------------------------->
<div class="form-group" id='debitorderclient' name='debitorderclient' style="display:<?php echo $debitorderclientstyle;?>">
<table class="table table-user-information">
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Agregates Details
                    </a>
                </h2>
</td></div>

</tr>
<!-- fields -->
<tr>
<td>
<div class="control-group">
<label>ID</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="ID" name="ID" type="text" value="<?php echo !empty($ID)?$ID:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>Description</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="Description" name="Description" type="text"  value="<?php echo !empty($Description)?$Description:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>UserCode</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="Username" name="Username" type="text"  value="<?php echo !empty($Username)?$Username:'';?>" />
	</div>
</div>
</td>
</tr>

<tr>
<td>
<div class="control-group <?php echo !empty($aggregate_limits_debitsError)?'error':'';?>">
<label>aggregate_limits_debits</label>
	<div class="controls">
		<input <?php echo $readonly; ?> style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_debits" name="aggregate_limits_debits" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($aggregate_limits_debits)?$aggregate_limits_debits:'';?>" />
		<?php if (!empty($aggregate_limits_debitsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_debitsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>

<td>
<div class="control-group <?php echo !empty($aggregate_limits_naedoError)?'error':'';?>">
<label>aggregate_limits_naedo</label>
	<div class="controls">
		<input  <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_naedo" name="aggregate_limits_naedo" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($aggregate_limits_naedo)?$aggregate_limits_naedo:'';?>" />
		<?php if (!empty($aggregate_limits_naedoError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_naedoError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>

<td>
<div class="control-group <?php echo !empty($aggregate_limits_cardsError)?'error':'';?>">
<label>aggregate_limits_cards</label>
	<div class="controls">
		<input  <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_cards" name="aggregate_limits_cards" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($aggregate_limits_cards)?$aggregate_limits_cards:'';?>" />
		<?php if (!empty($aggregate_limits_cardsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_cardsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>
</tr>

<tr>
<td>
<div class="control-group <?php echo !empty($aggregate_limits_creditsError)?'error':'';?>">
<label>aggregate_limits_credits</label>
	<div class="controls">
		<input <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_credits" name="aggregate_limits_credits" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($aggregate_limits_credits)?$aggregate_limits_credits:'';?>" />
		<?php if (!empty($aggregate_limits_creditsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_creditsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>
</tr>
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Items Details
                    </a>
                </h2>
</td></div>

</tr>
<tr>
<td>
<div class="control-group <?php echo !empty($item_limits_debitsError)?'error':'';?>">
<label>item_limits_debits</label>
	<div class="controls">
		<input <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="item_limits_debits" name="item_limits_debits" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($item_limits_debits)?$item_limits_debits:'';?>" />
		<?php if (!empty($item_limits_debitsError)): ?>
			<span class="help-inline"><?php echo $item_limits_debitsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>

<td>
<div class="control-group <?php echo !empty($item_limits_naedoError)?'error':'';?>">
<label>item_limits_naedo</label>
	<div class="controls">
		<input <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="item_limits_naedo" name="item_limits_naedo" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($item_limits_naedo)?$item_limits_naedo:'';?>" />
		<?php if (!empty($item_limits_naedoError)): ?>
			<span class="help-inline"><?php echo $item_limits_naedoError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>

<td>
<div class="control-group <?php echo !empty($item_limits_cardsError)?'error':'';?>">
<label>item_limits_cards</label>
	<div class="controls">
		<input  <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="item_limits_cards" name="item_limits_cards" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($item_limits_cards)?$item_limits_cards:'';?>" />
		<?php if (!empty($item_limits_cardsError)): ?>
			<span class="help-inline"><?php echo $item_limits_cardsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>
</tr>
<tr>
<td>
<div class="control-group <?php echo !empty($item_limits_creditsError)?'error':'';?>">
<label>item_limits_credits</label>
	<div class="controls">
		<input <?php echo $readonly; ?>  style="height:30px" class="form-control margin-bottom-10" id="item_limits_credits" name="item_limits_credits" type="number" name="Amount" min="0" step="0.01"  value="<?php echo !empty($item_limits_credits)?$item_limits_credits:'';?>" />
		<?php if (!empty($item_limits_creditsError)): ?>
			<span class="help-inline"><?php echo $item_limits_creditsError;?></span>
		<?php endif; ?>
	</div>
</div>
</td>
</tr>
<tr>

<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change log
                    </a>
                </h2>
</td></div>
</tr>
<tr>
<td>
<div class="control-group">
<label>changedby</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="changedby" name="changedby" type="text" value="<?php echo !empty($changedby)?$changedby:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>changedon</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="changedon" name="changedon" type="text"  value="<?php echo !empty($changedon)?$changedon:'';?>" />
	</div>
</div>
</td>
<td>
<div class="control-group">
<label>changedat</label>
	<div class="controls">
		<input  style="height:30px" class="form-control margin-bottom-10" readonly id="changedat" name="changedat" type="text"  value="<?php echo !empty($changedat)?$changedat:'';?>" />
	</div>
</div>
</td>
</tr>
</table>
</div>
<table class="table table-user-information">
<!-- Buttons -->
						<tr>
							<!--<td><div>
								<a class="btn btn-primary" href="custAuthenticate">Back</a>
							</div> -->
							</td>
							<td>
							<div>
							  	<button type="submit" class="btn btn-primary">Update</button>
							</div>
							</td>
						</tr>
						</table>
						</div>
						    <input style="height:30px" type='hidden' name='customerid' value="<?php echo !empty($customerid)?$customerid:'';?>"   />
						    <input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
						    <input id="debitorderclientstyle" type="hidden" name="debitorderclientstyle" class="form-control" placeholder="" readonly value="<?php echo $debitorderclientstyle; ?>" style='display:none' />
							<input style="height:30px" type='hidden' name='password' value="<?php echo !empty($password)?$password:'';?>"   />
						    <input style="height:30px" type='hidden' name='sqlStaged' value="<?php echo !empty($sqlStaged)?$sqlStaged:'';?>"   />
						    <input style="height:30px" type='hidden' name='json' value="<?php echo !empty($json)?$json:'';?>"   />
						    <input style="height:30px" type='hidden' name='readonly' value="<?php echo !empty($readonly)?$readonly:'';?>"   />
						    <input style="height:30px" type='hidden' name='databasename' value="<?php echo !empty($databasename)?$databasename:'';?>"   />
					</form>
				</div>

    </div> <!-- /container -->
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel)
 {
		var bankid = sel.options[sel.selectedIndex].value;
		var selectedbranchcode = document.getElementById('BranchCode').value;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});
			  return false;
  }
// -- 15.07.2017 - Banking Details ---
</script>
