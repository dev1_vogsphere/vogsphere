<?php
// -- Database connection -- ///
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$IntUserCode = getsession('IntUserCode');
$StatementUserCode=$_SESSION['statementusercode'] ;

if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
if(!function_exists('gettodate'))
{
	function gettodate()
	{
		$effectiveDate = date('Y-m-d');
		//$effectiveDate = date('Y-m-d', strtotime(' + 3 months'));
		return $effectiveDate;
	}
}

if(!function_exists('getfromdate'))
{
	function getfromdate()
	{
		$effectiveDate = date('Y-m-01');
		$effectiveDate = date('Y-m-01', strtotime(' - 30 days'));
		return $effectiveDate;
	}
}

$To = '';
$From = '';
$sessionrole='';
$mina = '';
$maxa = '';


if(isset($_GET['From']))
{
	$From=$_GET['From'];
}

if(isset($_GET['To']))
{
	$To=$_GET['To'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';

if(isset($_SESSION['IntUserCode']))
{

$Client_Id=$_SESSION['IntUserCode'];
//echo $clientid;
}

////////////////////////////////////////////////////////////////////////
// -- From Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($Client_Id)){$Client_Id = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
	if(empty($sessionrole)){$sessionrole = $params[3];}
}
	//echo $param;
	//$today = getfromdate();
	$From= "'".getfromdate()."'";
	$To = "'".gettodate()."'";

////////////////////////////////////////////////////////////////////////
    if($sessionrole=="admin")
    {// -- AND Status_Description !='pending'WHERE Action_Date >= $today
			if(!empty($From))
			{
				  $sql ="SELECT * FROM bankstatements WHERE transaction_date between $From and $To and usercode ='$StatementUserCode' ORDER BY count desc LIMIT 1000000";

			}
			else
			{
					$sql ="SELECT * FROM bankstatements WHERE usercode ='$StatementUserCode' ORDER BY count desc LIMIT 1000000";

			}

      //$result = mysqli_query($connect,$sql);
	  $result = $pdo->query($sql);
    }
    else{

			if(!empty($From))
			{
					$sql ="SELECT * FROM bankstatements WHERE transaction_date between $From and $To and usercode ='$StatementUserCode' ORDER BY count desc LIMIT 1000000";

			}
			else
			{
					$sql ="SELECT * FROM bankstatements WHERE transaction_date between $From and $To  and usercode ='$StatementUserCode' ORDER BY count desc LIMIT 1000000";
			}
   		//$result = mysqli_query($connect,$sql);
		$result = $pdo->query($sql);

    }

	// BOC -- Get sub branches 22/02/2022
//$pdoObj = Database::connectDB();
//$branchesArr1 = getsubbranches($pdoObj,$Client_Id);
// EOC -- Get sub branches 22/02/2022
?>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">
 <div class='row margin-vert-60'>

<!-- Filter Buttons -->

<div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form class='login-page' method='post' onsubmit=' return addRow()'>
				<div class='login-header margin-bottom-30'>
						<h2 class='text-center'>Transaction History</h2>
				</div>
				<div >
				<label>Date From
				</label>
				<input style='height:30px' name='From' placeholder='Date From' class='form-control' readonly type='Date' value="<?php echo getfromdate();?>">
				</div>

				<div >
				<label>Date To
				</label>
				<input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-50' readonly type='Date' value="<?php echo gettodate();?>">
				</div>
				<!--<br>-->
				<div>
				<!--<button class='btn btn-primary pull-right' type='submit'>Search</button>-->
				<br>
				</div>
  </div>



</div>

<div class="table-responsive">
<table id="statement" class="display table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
              <th>Date</th>
              <th>Description</th>
              <th>Amount</th>
              <th>Balance</th>
          </tr>
      </thead>
      <?php
        foreach($result as $row)
        //while($row = mysqli_fetch_array($result))
        {

          echo '
          <tr>
				<td width="10%">'.$row["transaction_date"].'</td>
				<td width="30%">'.$row["user_reference"].'</td>
				<td width="10%">'.$row["amount"].'</td>
				<td width="10%">'.$row["account_balance"].'</td>
          </tr>
          ';
        }
      ?>
  </table>
    </div>
    </br>
</div>
<script>

$.fn.dataTable.ext.search.push(
function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[7]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;
}

);

$(document).ready(function(){
var table =$('#statement').DataTable({
lengthChange: false,
autoFill:true,
buttons: [ 'copy', 'excel','pdf','colvis'],
  "order": [[ 0, "descending" ]],
});
$('.dataTables_length').addClass('bs-select');

table.buttons().container().appendTo('#statement_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });
});
</script>
