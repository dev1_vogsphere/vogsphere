<script>
function UpdateEmailTemplate(val1)
{
// -- Email Template.
	//var val1 = document.getElementById("message").value;  
//alert("Onload = "+val1);

	jQuery(document).ready(function() 
	{
		// alert("Onload 2 = "+val1);

// -- Email Template.
		//val1 = document.getElementById("message").value;
		//alert(val1);
		jQuery("#txtEditor").Editor("setText", val1);		
	});	
	
	jQuery(document).ready(function() 
	{
// -- Email Template.
		val1 = jQuery("#txtEditor").Editor("getText");
		
		document.getElementById("message").value = val1;		
	});
} 

</script>
<?php
/// ------------------------------- BOC Send OTP sms to Clients --------------------------- //// 
/// ------------------------------- EOC Send OTP sms to Clients --------------------------- ////
?>

<?php
// -- Data File.
require 'database.php';

// -- Functions File.
require 'functions.php';

// -- Tenantid who logged in. 
$tenantid = '';
$templatetype = 'email';

 // -- Read SESSION tenantid.
 if(isset($_SESSION['userid']))
 {
	$tenantid = $_SESSION['userid'];
	
 }
 else
 {
	 if(isset($_SESSION['username']))
	 {
		 $tenantid = $_SESSION['username'];
	 }
 }
 
$companyname = '';
$adminemail  = '';
if(isset($_SESSION['adminemail']))
{
  $adminemail = $_SESSION['adminemail'];
}
if(isset($_SESSION['companyname']))
{
  $companyname = $_SESSION['companyname']; 	
}
	
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="smstemplate"; // Table name 

//1. ----------------- Tenant Customers (SQL Inner Join) --------------- //
$dataCustomers = GetTenantCustomers($tenantid);

// -- Customers 
$ArrayCustomers = null;
$indexInside = 0;
 
// -- Array Customer 
foreach($dataCustomers as $row)
{
	$ArrayCustomers[$indexInside][0] = $row['CustomerId'];
	$ArrayCustomers[$indexInside][1] = $row['Title'];
	$ArrayCustomers[$indexInside][2] = $row['FirstName'];
	$ArrayCustomers[$indexInside][3] = $row['LastName'];
	$ArrayCustomers[$indexInside][4] = $row['Street'];
	$ArrayCustomers[$indexInside][5] = $row['Suburb'];
	$ArrayCustomers[$indexInside][6] = $row['City'];
	$ArrayCustomers[$indexInside][7] = $row['PostCode'];
	$ArrayCustomers[$indexInside][8] = $row['Dob'];
	$ArrayCustomers[$indexInside][9] = $row['phone'];
	
	// -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($row['CustomerId']);
if(!empty($row['refnumber']))
{
	$ArrayCustomers[$indexInside][10] = $row['refnumber'];
}
else
{
	$ArrayCustomers[$indexInside][10] = $ReferenceNumber;
}

	
	$ArrayCustomers[$indexInside][11] = $row['accountholdername'];
	$ArrayCustomers[$indexInside][12] = $row['bankname'];
	$ArrayCustomers[$indexInside][13] = $row['accountnumber'];
	$ArrayCustomers[$indexInside][14] = $row['branchcode'];
	$ArrayCustomers[$indexInside][15] = $row['accounttype'];
	// -- Read User.
	$userData = readuser($row['CustomerId']);
	if(!empty($userData))
	{
			$ArrayCustomers[$indexInside][16] = $userData['email'];
	}
	else
	{
			$ArrayCustomers[$indexInside][16] = $row['email2'];
	}
	$ArrayCustomers[$indexInside][17] = $row['phone2'];
	$indexInside = $indexInside + 1;
}			
$indexInside = 0;  
//2. ---------------------  Placeholder ------------------- //
$datasmstemplates = GetTenantEmailTemplates($tenantid,'');
 
$smstemplate = "";
$customer = "";
$phonenumberLoc  = "";
 
// --- Send SMSs via CM Telcoms -- //
$phonenumber = "";
$companyname = "";
$otp 		 = "";
$generateResponse  = "";
$message 	= "";
$messageLoc = "";

$subject = '';

$messageError = '';
$messageLog = '';
$phonenumberError = '';
$valid = true;

// -- number_format for e.g.: '0027731238839'
if(!empty($_POST))
{
	$messageError = '';
	$phonenumber = $_POST['phonenumber'];
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	$messageLoc = $message;
	
	$smstemplate = $_POST['smstemplate'];
	$customer = $_POST['customer'];
	
	$phonenumbers = explode(',', $phonenumber);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if (empty($phonenumber)) { $phonenumberError = $phonenumberError.'Please enter or select email address'; $valid = false;}
	if (empty($message)) { $messageError = $messageError.'Please enter or select message template'; $valid = false;}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
	//var_dump($phonenumbers);

	  $ArraySize =  sizeof($phonenumbers);;
	//echo $ArraySize;
	  for ($i = 0; $i < $ArraySize; $i++) 
	  {
		$phonenumberLoc = $phonenumbers[$i];
		
		// -- Get Placeholders
		$indexInside = 0;
		$CustomerId = '';
		$Title 	 	 = '';
		$FirstName   = '';
		$LastName 	 = '';
		$Street 	 = '';
		$Suburb 	 = '';
		$City 		 = '';
		$PostCode 	 = '';
		$Dob 		 = '';
		$phone 	     = '';
		$refnumber   = '';	
		$accountholdername = '';
		$bankname  		   = '';
		$accountnumber     = '';
		$branchcode  	   = '';
		$accounttype  	   = '';
		$email  		   = '';
		$phone2 		   = '';
		//echo "Phone : ".$phonenumberLoc;

		if(!empty($phonenumberLoc))
		{

		// --  Customer Data
		/////////////////////// ---- BOC
		for ($j=0;$j<sizeof($ArrayCustomers);$j++)
		{
				$phone = $ArrayCustomers[$j][16];

				if($phonenumberLoc == $phone)
				{
					$message 		   = 	$messageLoc;
					$CustomerId 	   = $ArrayCustomers[$j][0];
					$Title 			   = $ArrayCustomers[$j][1];
					$FirstName  	   = $ArrayCustomers[$j][2];
					$LastName 		   = $ArrayCustomers[$j][3];
					$Street 		   = $ArrayCustomers[$j][4];
					$Suburb 		   = $ArrayCustomers[$j][5];
					$City 			   = $ArrayCustomers[$j][6];
					$PostCode 		   = $ArrayCustomers[$j][7];
					$Dob 			   = $ArrayCustomers[$j][8];
					$phone 			   = $ArrayCustomers[$j][9];
					$refnumber  	   = $ArrayCustomers[$j][10];	
					$accountholdername = $ArrayCustomers[$j][11];
					$bankname  		   = $ArrayCustomers[$j][12];
					$accountnumber     = $ArrayCustomers[$j][13];
					$branchcode  	   = $ArrayCustomers[$j][14];
					$accounttype  	   = $ArrayCustomers[$j][15];
					$email  		   = $ArrayCustomers[$j][16];
					$phone2 		   = $ArrayCustomers[$j][17];
				}
		}
// -- Placeholders replace with actual data.
		$message = str_replace('[CustomerId]',$CustomerId ,$message);
		$message = str_replace('[Title]',$Title ,$message);
		$message = str_replace('[FirstName]',$FirstName ,$message);
		$message = str_replace('[LastName]',$LastName ,$message);
		$message = str_replace('[Street]',$Street ,$message);
		$message = str_replace('[Suburb]',$Suburb ,$message);
		$message = str_replace('[City]',$City ,$message);
		$message = str_replace('[Dob]',$Dob ,$message);
		$message = str_replace('[phone]',$phone ,$message);
		$message = str_replace('[refnumber]',$refnumber ,$message);
		$message = str_replace('[accountholdername]',$accountholdername ,$message);
		$message = str_replace('[bankname]',$bankname ,$message);
		$message = str_replace('[accountnumber]',$accountnumber ,$message);
		$message = str_replace('[branchcode]',$branchcode ,$message);
		$message = str_replace('[accounttype]',$accounttype ,$message);
		$message = str_replace('[email]',$email ,$message);
		$message = str_replace('[phone2]',$phone2 ,$message);
		$phonenumberLoc = $phonenumberLoc;
		//echo "Number : ".$i;
// -- Send Email.		
		SendEmail($message, $email,$subject);
// -- Log Email sent to recipients.		
$status = 'success';
AddCommunicatioHistory($message,$CustomerId,$refnumber,$tenantid,$status,$email,$phone);
		//$messageLog = '<p>'.$message.' - successfully sent to '.$phonenumberLoc.'</p><br/>';
		$messageLog = '<p>Email - successfully sent to '.$phonenumberLoc.'</p><br/>';
		//echo $messageLog;
	  }
	  }
	}
}
else
{
	$smstemplate = 'default';
} 
// -- Show all information, defaults to INFO_ALL  
?>
<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="sendemail">SendEmail</a></li>
		</ol>
	</div>
</div> -->

<div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
	  <div class="row">
	  <?php if(!empty($_POST))
		{
			echo "<div class='alert alert-success fade in'>
            <a href='#' class='close' data-dismiss='alert'>×</a>".
				$messageLog
			."</div>";
		}
	  ?>
 <form METHOD="post"  id="form1" name="form1" class="signup-page"  action=""> 
 <p><b>Customers</b><br />
				<SELECT class="form-control" id="customer" name="customer" size="1">
					<?php
						$CustomerLoc = '';
						$CustomerSelect = $customer;
						$refnumber = '';
						$phone = '';						
/////////////////////// ---- BOC
								for ($i=0;$i<sizeof($ArrayCustomers);$i++)
								{
									$CustomerLoc = $ArrayCustomers[$i][0];
									$refnumber =  $ArrayCustomers[$i][10];
									$email = $ArrayCustomers[$i][16];
									if(empty($refnumber))
									{
										$refnumber = $CustomerLoc;
									}
									
									if($CustomerLoc == $CustomerSelect)
									{
										echo "<OPTION value='".$email."' selected>$refnumber - $email</OPTION>";
									}
									else
									{
										echo "<OPTION value='".$email."'>$refnumber - $email</OPTION>";
									}
								}
								if(empty($ArrayCustomers))
								{
									echo "<OPTION value=0>No customers</OPTION>";
								}
/////////////////////// ---- EOC
					?>
					
					</SELECT>
			</p>
<div class="form-group <?php if (!empty($phonenumberError)){ echo "has-error";}?>">						
  <!-- <img src="w3html.jpg" onload="UpdateEmailTemplate()" width="100" height="132" style="display:none"> -->
	<p>
		Click to add <input  class='btn btn-primary' onclick='input()' type='button' value='Email' id='button'>Click to add ALL<input  class='btn btn-primary' onclick='AllPhones()' type='button' value='ALL' id='button'><br>						
		<input style="height:30px"  class="form-control margin-bottom-20"  id="phonenumber" name="phonenumber" type="text"  placeholder="Email Address" value="<?php echo !empty($phonenumber)?$phonenumber:'';?>" readonly>
		<?php if (!empty($phonenumberError)): ?>
		<small class="help-block"><?php echo $phonenumberError;?></small>
		<?php endif; ?>
	</p>
</div>
  
<p><b>Email Template</b><br />
				<SELECT class="form-control" id="smstemplate" name="smstemplate" size="1" onChange="valueselect(this);" onload="valueselect(this);">
					<?php
						$smstemplateLoc = '';
						$smstemplateSelect = $smstemplate;
						foreach($datasmstemplates as $row)
						{
							$smstemplateLoc = $row['smstemplatename'];

							if($smstemplateLoc == $smstemplateSelect)
							{
								echo "<OPTION value='".$smstemplateLoc."' selected>$smstemplateLoc</OPTION>";
								$subject = $row['subject'];
							}
							else
							{
								echo "<OPTION value='".$smstemplateLoc."'>$smstemplateLoc</OPTION>";
								$subject = $row['subject'];
							}
						}
												
						if(empty($datasmstemplates))
						{
							echo "<OPTION value=0>No smstemplates</OPTION>";
						}		
					?>
					
					</SELECT>
			</p> 

			<!-- 2. Email Subject Template -->
		<div class="form-group <?php if (!empty($subjectError)){ echo "has-error";}?>">						
			<p><b>Subject</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='subject' id="subject" value="<?php echo !empty($subject)?$subject :'';?>"/>
			<?php if (!empty($subjectError)): ?>
			<small class="help-block"><?php echo $subjectError;?></small>
			<?php endif; ?>
			</p>
		</div>	
			
			<div class="form-group <?php if (!empty($messageError)){ echo "has-error";}?>">									
				  <p>
				  <textarea class="form-control" rows="10" name="message" id="message" placeholder="Message" style="display:none"><?php echo !empty($message)?$message:'';?></textarea>  
								<?php if (!empty($messageError)): ?>
							<small class="help-block"><?php echo $messageError;?></small>
							<?php endif; ?>
				</p>
			</div>
	<div class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 nopadding">
						<textarea id="txtEditor" name="txtEditor"><?php echo $message;?></textarea> 
								<!-- <textarea id="smstemplatecontent" name="smstemplatecontent" $smstemplatecontent2.$smstemplatename style="display:none"></textarea> -->	
					</div>
				</div>
			</div>
		</div>
	</div>		
 <?php if (!empty($otp)): ?>
	 <span class="help-inline"><?php echo $otp; ?></span>
  <?php endif; ?>
  <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Send Email</button><!-- Generate OTP -->
 <!-- Display message Log -->
  <?php if (!empty($messageLog)): ?>
	<p>
		<?php 	$messageLogs = explode('<br/>', $messageLog);
		 $ArraySize =  sizeof($messageLogs);
	//echo $ArraySize;
	  for ($i = 0; $i < $ArraySize; $i++) 
	  {
		//echo $messageLogs[$i];
	  }
				//echo $messageLog; ?>
	</p>	 
  <?php endif; ?>
 </form> 
 </div>
 </div></div>
<script>
// jQuery
 $(document).ready(function () 
 {
var smstemplate = document.getElementById('smstemplate');
var smstemplatename = smstemplate.options[smstemplate.selectedIndex].value; 
       valueselect(smstemplate);
  });

function AllPhones()
{
		var phones = document.getElementById('customer');
		var phone = '';
	for(var i=0; i < phones.length; i++)
     {
       cell = phones.options[i].value;
	   phone = document.forms.form1.phonenumber.value;
	   if(phone != '')
	   {
		   phone = phone+",";
	   }
	   document.forms.form1.phonenumber.value = phone + cell;
     }
}	
function input()
{
	//alert("Modise");
	var title = document.getElementById('customer').value;
	var area = document.forms.form1.phonenumber.value;
	 if(area != '')
	   {
		   area = area+",";
	   }
    document.forms.form1.phonenumber.value = area + title;
}    

 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var smstemplate = sel.options[sel.selectedIndex].value;      
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectSMSTemplates.php",
				       data:{smstemplate:smstemplate},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#message").html(data);
								UpdateEmailTemplate(data);

						 }
					});
				});	
				
			  return false;
  }  
</script> 
<!-- </body>
</html> -->