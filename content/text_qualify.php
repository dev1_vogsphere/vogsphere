<?php
$usercode=" ";
$usercode=getsession('IntUserCode');
$role=getsession('role');
//echo $role;
if ( !empty($usercode)) {
	$usercode=$usercode;
	//echo "Not empty".$usercode;
}else{
	$usercode="Null";
	//echo "Hello".$usercode;
}
//echo $usercode; 

include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype WHERE usercode = ? ORDER BY loantypeid DESC';
 // $data = $pdo->query($sql);	
  $data = $pdo->prepare($sql);
  $data->execute(array($usercode));
  $data = $data->fetchAll();
  //print_r( $data);
  $currency = "";
  $url = '';
  // -- BOC Loan Durations (30.06.2017)
  $MonthlyIncome = '0.00';
  $Incomebeforetax='0.00';
  $Incomeaftertax='0.00';
  $Commission='0.00';
  $Vehiclerepayments='0.00';
  $Homeloanrepayments='0.00';
  $Overdraftrepayments='0.00';
  $Othercreditinstalments='0.00';
  $Personalloanrepayments='0.00';
  $Rentalincome='0.00';
  $MonthlyDispIncome = '0.00';
  $MonthlyExpenditure = '0.00';
  
  $ReportMessage = "<td><span class='input-group-addon'><i class='fa fa-anchor'></i></span></td>
  <td>To do an ITC Credit Check - Enter Consumer Identity Number and click on Credit Scoring button.</p></td>";
  
  $fromduration = 1;
  $toduration = 1;
  $fromamount = 0.00;
  $toamount = 0.00;
  $fromdate = '';
  $todate = '';
  $qualifyfrom = 1.00;
  $qualifyto = 1.00;
  $loantype = 1;
  $ApplicationIdEncoded = '';
  $CustomerIdEncoded1 = '';
  // -- EOC Loan Durations (30.06.2017)

  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		if($row['loantypeid'] > 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
			$_SESSION['currency'] = $row['currency'];// -- 10.02.2017
			// -- BOC Loan Durations (30.06.2017)
			$fromduration = $row['fromduration'];
			$toduration = $row['toduration'];
			$fromamount = $row['fromamount'];
			$toamount = $row['toamount'];
			$fromdate = $row['fromdate'];
			$todate = $row['todate'];
			$qualifyfrom = $row['qualifyfrom'];
			$qualifyto = $row['qualifyto'];
			$loantype = $row['loantypeid'];
			// -- EOC Loan Durations (30.06.2017)
		}
	}

// -- For Drop Down List.	
 $sql = 'SELECT * FROM loantype WHERE usercode = ?';
 //$dataLoanType = $pdo->query($sql);	
 $dataLoanType = $pdo->prepare($sql);
 $dataLoanType->execute(array($usercode));
 $dataLoanType = $dataLoanType->fetchAll();

 $customerid = '';
 $CustomerIdEncoded = '';

if(isset($_GET['customerid'])) 
{
	$customerid = $_REQUEST['customerid'];
	$CustomerIdEncoded = $customerid;
	$customerid = base64_decode(urldecode($CustomerIdEncoded)); 
	$url = 'creditreport?customerid='.$CustomerIdEncoded. '&ApplicationId=0';	
}
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
 if (!empty($_POST)) 
{ 
	// -- Loan Type.
	$fromamount = $_POST['fromamount'];
	$toamount = $_POST['toamount'];
	$qualifyto = $_POST['qualifyto'];
	$qualifyfrom = $_POST['qualifyfrom'];
	$loantype = $_POST['loantype'];
	$customerid = $_POST['customerid'];;
	
	$CustomerIdEncoded = urlencode(base64_encode($customerid));
	
	// -- Entered Values
	//$MonthlyIncome = $_POST['MonthlyIncome'];
    $MonthlyDispIncome = $_POST['MonthlyDispIncome'];
  	//$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
	$currency = $_POST['currencyIn'];

	// -- 
			if (($MonthlyDispIncome >= $qualifyfrom))// -- && ($MonthlyDispIncome <= $qualifyto))
			{
			
			
			
				// -- "is between"
				$ReportMessage = "
				<td><span class='input-group-addon'><i class='fa fa-thumbs-up'></i></span></td>
				<td><p>The client qualify for the maximum borrowing amount of $currency $toamount.<br>
								  Disposable Income is greater than the min. qualify amount :$currency $qualifyfrom</p></td>"; 
			}
			else
			{
				$ReportMessage = "
				<td><span class='input-group-addon'><i class='fa fa-thumbs-down'></i></span></td>
				<td><p>The client does not qualify for the maximum borrowing amount of $currency $toamount.<br>
								  Disposable Income :$currency $MonthlyDispIncome is less than min. qualify amount :$currency $qualifyfrom</p></td>"; 
			}
			
			// -- Credit Report View
	
	/*if(empty($customerid)){ echo '""';}else{ $CustomerIdEncoded = urlencode(base64_encode($customerid));
	echo '"'.$CustomerIdEncoded.'"'; 	}*/
	if (array_key_exists('verify',$_POST)){
		if (empty($customerid))
		{
			echo "Enter Consumer Identification Number";
		}else{
			$url = 'creditreport?customerid='.$CustomerIdEncoded. '&ApplicationId=0';
			echo "yes";
		}
		
	}
	
}
?>   
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->		
<!-- Search Login Box -->
<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page'><!-- onsubmit=' return addRow()'> -->
                                    <div class='login-header margin-bottom-30'>
                                        <h2 style="display:none">ITC Credit Check - Client Qualify?</h2>
                                        <h2 style="text-align:center">Consumer Vetting</h2>	
										<p style="text-align:center"> Use this affordability calculator to determine how much disposable income the consumer has in order to make an informed decision</p>										
                                    </div>
									
									<div class="controls">
									<h3 >Product</h3>
                                	<SELECT  class="form-control" id="loantype" name="loantype" size="1" onChange="valueInterest(this);">
									<!-------------------- BOC 2017.04.15 - dataLoanType --->	
									<?php
											$loantypeid = '';
											$loantypeSelect = $loantype;
											$loantypedesc = '';
											$loantypevalue = 0;
											$currencyIn = '';
											foreach($dataLoanType as $row)
											{
											  // -- Loan Type of a selected Bank
												$loantypeid = $row['loantypeid'];
												$loantypedesc = $row['loantypedesc'];						
												$loantypevalue = $row['percentage'];
												// -- 30.06.2017
												// -- BOC Loan Durations (30.06.2017)
												  $todate = $row['todate'];
												  
												  // -- Loan - Todate cannot be in past.
													$date1=date_create(date('Y-m-d'));// -- Today's date. 
													$date2=date_create($todate);// -- LoanToDate

													$diff=date_diff($date1,$date2);
													$value = $diff->format("%R%a");
													
												// -- if its negative means Loan To date is in the past.
													if($value < 0)
													{
														// -- Loan Type has expied.
														// -- DO NO SHOW IT ON THE Application.
													}
													else
													{
													// -- Determine Range Values.
														  $fromduration = $row['fromduration'];
														  $toduration = $row['toduration'];
														  $fromdate = $row['fromdate'];
														  $fromamount = $row['fromamount'];
														  $toamountIn = $row['toamount'];
														  $qualifyfromIn = $row['qualifyfrom'];
														  $qualifytoIn = $row['qualifyto'];
														  $currencyInA = $row['currency'];
															
													  // -- if its postive means Loan Type To date is in the future/present.
													  // -- SHOW THE LOAN TYPE		
														// -- EOC Loan Durations (30.06.2017)
														// -- 30.06.2017
														// -- Select the Selected Bank 
													  if($loantypeid == $loantype)
													   {
													  // <!-- data-fromdate=$fromdate data-todate=$todate -->
													   // -- ;Valid From:$fromdate To:$todate 
															echo "<OPTION data-vogsInt=$loantypevalue
															data-currencyIn=$currencyInA													data-qualifyfrom=$qualifyfromIn data-qualifyto=$qualifytoIn 
															data-fromamount=$fromamount data-toamount=$toamountIn 
															data-fromdate=$fromdate data-todate=$todate
															data-from=$fromduration data-to=$toduration value=$loantypeid selected>$loantypedesc - Min.:$currencyInA$fromamount Max.:$currencyInA$toamountIn
															</OPTION>";
															// -- Initial Amount.
															$toamount = $toamountIn;
															$qualifyto = $qualifytoIn;
															$currencyIn = $currencyInA;
															$qualifyfrom = $qualifyfromIn;
													   }
													  else
													   {
													   //<!-- data-fromdate=$fromdate data-todate=$todate-->
													   // -- Valid From:$fromdate To:$todate
															echo "<OPTION data-vogsInt=$loantypevalue 
															data-currencyIn=$currencyInA 
															data-qualifyfrom=$qualifyfromIn data-qualifyto=$qualifytoIn 
															data-fromamount=$fromamount data-toamount=$toamountIn 
															data-fromdate=$fromdate data-todate=$todate
															data-from=$fromduration data-to=$toduration value=$loantypeid>
															$loantypedesc - 
															Min.:$currencyInA$fromamount Max.:$currencyInA$toamountIn</OPTION>";
													   }
													}
											}
											
								
											if(empty($dataLoanType))
											{
												echo "<OPTION value=0>No Loan Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - dataLoanType -->
								</SELECT>
								
								<!------------------------------ Start Date & End Date - 01.07.2017 ---->
                                <input style="height:30px;display:none" readonly value="<?php echo $fromamount;?>" id="fromamount" 
								name="fromamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $toamount;?>" id="toamount" 
								name="toamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $fromdate;?>" id="fromdate" 
								name="fromdate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $todate;?>" id="todate" 
								name="todate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $qualifyfrom;?>" id="qualifyfrom" 
								name="qualifyfrom" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $qualifyto;?>" id="qualifyto" 
								name="qualifyto" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none" readonly value="<?php echo $currencyIn;?>" id="currencyIn" 
								name="currencyIn" class="form-control margin-bottom-10" type="text" />
								
								<!------------------------------ EOC Start Date and End - 01.07.2017 ---->
								</div>
									<h3>Income</h3>	
									<br>
									<h4>Income before tax</h4>		
									<div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Incomebeforetax' name='Incomebeforetax' 
										placeholder='' class='form-control' 
										onchange="DisposableIncome()" type='text' 
										value=<?php echo $Incomebeforetax;?> >
                                    </div>	
									<h4>Income after tax</h4>	
									<div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Incomeaftertax' name='Incomeaftertax' 
										placeholder='' class='form-control' 
										onchange="DisposableIncome()" type='text' 
										value=<?php echo $Incomeaftertax;?> >
                                    </div>	
									<h4>Commission</h4>	
									<div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Commission' name='Commission' 
										placeholder='' class='form-control' 
										onchange="DisposableIncome()" type='text' 
										value=<?php echo $Commission;?> >
                                    </div>	
									<h4>Rental income</h4>	
									<div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Rentalincome' name='Rentalincome' 
										placeholder='Rentalincome' class='form-control' 
										onchange="DisposableIncome()" type='text' 
										value=<?php echo $Rentalincome;?> >
                                    </div>	
									<br>
									<h3>Expenses</h3>	
									<h4>Vehicle repayments</h4>									
                                    <div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0"  id='Vehiclerepayments' name='Vehiclerepayments' 
										placeholder='' class='form-control' 
										onchange="DisposableIncome()" type='text' 
										value=<?php echo $Vehiclerepayments;?> >
                                    </div>	
									<h4>Home loan repayments</h4>
									<div  class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Homeloanrepayments' name='Homeloanrepayments' 
										placeholder='' class='form-control' type='text' 
										onchange="DisposableIncome()" value=<?php echo $Homeloanrepayments; ?>  >
                                    </div>	
									<h4>Overdraft repayments</h4>
									<div  class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Overdraftrepayments' name='Overdraftrepayments' 
										placeholder='' class='form-control' type='text' 
										onchange="DisposableIncome()" value=<?php echo $Overdraftrepayments; ?>  >
                                    </div>	
									<h4>Personal loan repayments</h4>
									<div  class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Personalloanrepayments' name='Personalloanrepayments' 
										placeholder='' class='form-control' type='text' 
										onchange="DisposableIncome()" value=<?php echo $Personalloanrepayments; ?>  >
                                    </div>	
									<h4>Other credit instalments</h4>
									<div  class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0" id='Othercreditinstalments' name='Othercreditinstalments' 
										placeholder='' class='form-control' type='text' 
										onchange="DisposableIncome()" value=<?php echo $Othercreditinstalments; ?>  >
                                    </div>	
									<h3>Disposable Income</h3>
									<div class='input-group margin-bottom-20'>
									<span class = "input-group-addon">R</span>
                                        <input style="height:30px; z-index:0"  id='MonthlyDispIncome' name='MonthlyDispIncome' readonly
										placeholder='Total Monthly Disposable Amount' class='form-control' type='text' 
										onchange="DisposableIncome()" value=<?php echo $MonthlyDispIncome?>>
                                    </div>	
									<div 
									<br>
									<p style="text-align:center"><?php echo $ReportMessage?> </p>
                                    </div>	
                                    <div class='row'>
									<div class='col-md-4'>
											
											</div>
											<div class='col-md-4'>
												
											</div>
                                       <!-- <div class='col-md-4'>
                                            <button class='btn btn-primary pull-right' type='submit' onclick="DisposableIncome()">Affordability</button>
                                        </div>-->
                                    </div>
									
									<br/>
									<div class="input-group margin-bottom-20">
									<span style="height:5px" class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
									<input style="height:30px;z-index:0" id="customerid" name="customerid"  placeholder="Identity number" class="form-control" type="text" value="<?php echo $customerid;?>">
										</div>
									<div class='row'>
									<div class='col-md-4'>
											
										</div>
										<div class='col-md-4'>
											
										</div>
										<div class='col-md-4'>
											<!--<button class="btn btn-info btn-lg  pull-right" name="verify" type="submit" >Verify Info Ext</button--->
											
											<button class="btn btn-info btn-lg  pull-right" name="verify" type="button" onclick="creditreport()">
											 <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
											 Credit Scoring...
											</button>
											<!-- <a style="display:inline;align:center" class="btn btn-info">Credit Check</a> -->
										</div>
									</div>

																					
 </form>
                            </div>										
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				  <div class="table-responsive">
		              <?php 
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//echo "<table class='table table-striped table-bordered'>"; 
						//echo "<tr>"; 
						//echo "<td colspan='2'><b>Detailed Report</b></td>"; 
						//echo "</tr>"; 
						//if(!empty($ReportMessage))						
					//	{ 
						//foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
					//	echo "<tr>";  
					//	echo $ReportMessage;  
						//echo "</tr>"; 
						//} 
						//echo "</table>"; 
					   Database::disconnect();
					  ?>
			   <!-- <iframe  width="100%" id="resultFrame" name="resultFrame" src=""></iframe>-->

				</div>
    	</div></div>
   		<!---------------------------------------------- End Data ----------------------------------------------->
		<script>
		// -- Different Loan Types.
 function valueInterest(sel) 
 {
		// -- var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
		var fromdate = sel.options[sel.selectedIndex].getAttribute('data-fromdate');
		var todate = sel.options[sel.selectedIndex].getAttribute('data-todate');
		var fromduration = sel.options[sel.selectedIndex].getAttribute('data-from');
		var toduration = sel.options[sel.selectedIndex].getAttribute('data-to');
		var fromamount = sel.options[sel.selectedIndex].getAttribute('data-fromamount');
		var toamount = sel.options[sel.selectedIndex].getAttribute('data-toamount');
		var qualifyfrom = sel.options[sel.selectedIndex].getAttribute('data-qualifyfrom');
		var qualifyto = sel.options[sel.selectedIndex].getAttribute('data-qualifyto');
		var currencyIn = sel.options[sel.selectedIndex].getAttribute('data-currencyIn');

		
		var MonthlyDispIncome = document.getElementById('MonthlyDispIncome').value;
		var MonthlyExpenditure = document.getElementById('MonthlyExpenditure').value;
		var MonthlyIncome = document.getElementById('MonthlyIncome').value;

		// -- document.getElementById('InterestRate').value = interest;
		document.getElementById('fromdate').value = fromdate;
		document.getElementById('todate').value = todate;
		document.getElementById('fromamount').value = fromamount;
		document.getElementById('toamount').value = toamount;
		//alert(toamount);			

		document.getElementById('qualifyfrom').value = qualifyfrom;
		document.getElementById('qualifyto').value = qualifyto;
		document.getElementById('currencyIn').value = currencyIn;

		// -- Call Disposable Income.		
		DisposableIncome();
	 return false;
 }	
 function creditreport()
 {
	 // Simulate a mouse click:
	custid='';
	custid = document.getElementById('customerid').value;
	//alert(custid);

	var val = btoa(custid);
	//alert(val);
	url = 'creditreport?customerid='+val+ '&ApplicationId=0';
	//alert(url);

	window.location.href=url;
	iframe = document.getElementById("resultFrame");
	iframe.src = url;
 }
function DisposableIncome()
{
	var num = 0.0000;
    var n = num.toFixed(2);


	var Incomebeforetax = document.getElementById('Incomebeforetax').value;
	Incomebeforetax = parseFloat(Incomebeforetax);
	n = Incomebeforetax.toFixed(2);
	document.getElementById('Incomebeforetax').value = n;
	if(document.getElementById("Incomebeforetax").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Incomebeforetax").value = n;
	}		

	var Incomeaftertax = document.getElementById('Incomeaftertax').value;
	Incomeaftertax = parseFloat(Incomeaftertax);
	n = Incomeaftertax.toFixed(2);
	document.getElementById('Incomeaftertax').value = n;
	if(document.getElementById("Incomeaftertax").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Incomeaftertax").value = n;
	}		

	var Commission = document.getElementById('Commission').value;
	Commission = parseFloat(Commission);
	n = Commission.toFixed(2);
	document.getElementById('Commission').value = n;
	if(document.getElementById("Commission").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Commission").value = n;
	}		

	var Vehiclerepayments = document.getElementById('Vehiclerepayments').value;
	Vehiclerepayments = parseFloat(Vehiclerepayments);
	n = Vehiclerepayments.toFixed(2);
	document.getElementById('Vehiclerepayments').value = n;
	if(document.getElementById("Vehiclerepayments").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Vehiclerepayments").value = n;
	}		

	var Overdraftrepayments = document.getElementById('Overdraftrepayments').value;
	Overdraftrepayments = parseFloat(Overdraftrepayments);
	n = Overdraftrepayments.toFixed(2);
	document.getElementById('Overdraftrepayments').value = n;
	if(document.getElementById("Overdraftrepayments").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Overdraftrepayments").value = n;
	}		

	var Personalloanrepayments = document.getElementById('Personalloanrepayments').value;
	Personalloanrepayments = parseFloat(Personalloanrepayments);
	n = Personalloanrepayments.toFixed(2);
	document.getElementById('Personalloanrepayments').value = n;
	if(document.getElementById("Personalloanrepayments").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Personalloanrepayments").value = n;
	}	
	
	var Othercreditinstalments = document.getElementById('Othercreditinstalments').value;
	Othercreditinstalments = parseFloat(Othercreditinstalments);
	n = Othercreditinstalments.toFixed(2);
	document.getElementById('Othercreditinstalments').value = n;
	if(document.getElementById("Othercreditinstalments").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Othercreditinstalments").value = n;
	}		

	var Homeloanrepayments = document.getElementById('Homeloanrepayments').value;
	Homeloanrepayments = parseFloat(Homeloanrepayments);
	n = Homeloanrepayments.toFixed(2);
	document.getElementById('Homeloanrepayments').value = n;
	if(document.getElementById("Homeloanrepayments").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Homeloanrepayments").value = n;
	}	

	var Rentalincome = document.getElementById('Rentalincome').value;
	Rentalincome = parseFloat(Rentalincome);
	n = Rentalincome.toFixed(2);
	document.getElementById('Rentalincome').value = n;
	if(document.getElementById("Rentalincome").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("Rentalincome").value = n;
	}		

	var MonthlyDispIncome = document.getElementById('MonthlyDispIncome').value;
	MonthlyDispIncome = parseFloat(MonthlyDispIncome);
	n = MonthlyDispIncome.toFixed(2);
	document.getElementById('MonthlyDispIncome').value = n;
	if(document.getElementById("MonthlyDispIncome").value === 'NaN')
	{
		n = num.toFixed(2);
		document.getElementById("MonthlyDispIncome").value = n;
	}		

	var MonthlyExpenditure;
	var MonthlyIncome;
			//alert("Once-24");

	MonthlyIncome=Incomeaftertax+Commission+Rentalincome;
	MonthlyExpenditure=Vehiclerepayments+Homeloanrepayments+Overdraftrepayments+Othercreditinstalments+Personalloanrepayments+Rentalincome;


	// -- Determine the Disposable Income.
		MonthlyDispIncome = MonthlyIncome - MonthlyExpenditure;
	   //alert(MonthlyDispIncome);
	   n = MonthlyDispIncome.toFixed(2);
	   document.getElementById("MonthlyDispIncome").value = n;
	    
			if (($MonthlyDispIncome >= $qualifyfrom))// -- && ($MonthlyDispIncome <= $qualifyto))
			{
			
			
			
				// -- "is between"
				$ReportMessage = "Test";
			}
			else
			{
				$ReportMessage = "Yes"; 
			}
	  
}
// -- EOC Different Loan Types.
</script> 