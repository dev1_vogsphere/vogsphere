<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
if(!function_exists('gettodate'))
{
	function gettodate()
	{
		$effectiveDate = date('Y-m-d');
		//$effectiveDate = date('Y-m-d', strtotime(' + 3 months'));
		return $effectiveDate;
	}
}

if(!function_exists('getfromdate'))
{
	function getfromdate()
	{
		$effectiveDate = date('Y-m-01');
		$effectiveDate = date('Y-m-01', strtotime(' - 12 months'));
		return $effectiveDate;
	}
}

if(!function_exists('getmainstatus'))
{
	function getmainstatus($dataFilter,$databankresponsecodeArr)
	{
		$branchcode = '';
		$value0     = '';
		$value1		= '';

		foreach($databankresponsecodeArr as $row)
		{
			$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
			if(isset($rowData[0]))
			{
				$value0 = $rowData[0];
			}
			if($value0 == $dataFilter['Status_Description'])
			{
				$value1 = $rowData[1];
			}
		}
		return $value1;
	}
}

$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM bankresponsecodes';
$databankresponsecode = $pdo->query($sql);

$databankresponsecodeArr = array();
foreach($databankresponsecode as $row)
{
	$databankresponsecodeArr[] = $row['bankresponsecodedesc']."|".$row['mainstatus'];
}
//print_r($databankresponsecodeArr);
$dataMainStatusArr = array();
$dataMainStatusArr[] = 'Successful';
$dataMainStatusArr[] = 'UnPaid';

$FromDate= '';
$ToDate='';
$sessionrole='';
$mina = '';
$maxa = '';

$Allstatus = '';
$status    = '';
$mainstatus = '';
$Allbranch = ''; //22/02/2022
$srUserCode00 = '';
$display = '';

 $AllServiceMode = '';
 $ServiceMode    = '';
 $Client_ID_TypeSelect = '';

 $AllServiceType = '';
 $ServiceType    = '';

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';

$connect = mysqli_connect("ecashpdq.chk4kockgrje.eu-west-2.rds.amazonaws.com","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  if(isset($_SESSION['IntUserCode']))
    {

    	$Client_Id=$_SESSION['IntUserCode'];
     //echo $clientid;
    }

////////////////////////////////////////////////////////////////////////
// -- From Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($Client_Id)){$Client_Id = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
	if(empty($sessionrole)){$sessionrole = $params[3];}
}
	//echo $param;
	//$today = getfromdate();
	$mina = "'".getfromdate()."'";
	$maxa = "'".gettodate()."'";
////////////////////////////////////////////////////////////////////////
    if($sessionrole=="admin")
    {// -- AND Status_Description !='pending'WHERE Action_Date >= $today
      $sql ="SELECT * FROM collection WHERE Status_Description !='pending' AND Action_Date BETWEEN $mina AND $maxa ORDER BY Action_Date AND Status_Description DESC
	  ";
      $result = mysqli_query($connect,$sql);
    }
    else{
	// -- 23/02/2022
	//echo $Client_Id;
	// -- Check if its a main branch searching via UserCode...		
	// --  WHERE Status_Description!='pending' AND WHERE Action_Date >= $today
      $sql ="SELECT * FROM collection WHERE Status_Description !='pending' AND UserCode='$Client_Id' AND Action_Date BETWEEN $mina AND $maxa
	   ORDER BY Action_Date AND Status_Description DESC ";
	  $result = mysqli_query($connect,$sql);
	// -- 23/02/2022
		// -- if its not a main branch then check its subranch detail.
		$records = mysqli_num_rows($result);
		//echo $records; 
		if($records <= 0 )
		{
		  $sql ="SELECT * FROM collection WHERE Status_Description !='pending' AND IntUserCode='$Client_Id' AND Action_Date BETWEEN $mina AND $maxa
		   ORDER BY Action_Date AND Status_Description DESC ";
		  $result = mysqli_query($connect,$sql);			
		}  
	  
    }
	
	// BOC -- Get sub branches 22/02/2022
$branchesArr1 = array();
$pdoObj = Database::connectDB();
$branchesArr1 = getsubbranches($pdoObj,$Client_Id);
// EOC -- Get sub branches 22/02/2022
?>

<!--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>-->

<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">
 <div class='row margin-vert-30'>
  <h2 align="center">Debit Orders History</h2>
  </br>
  </br>
<!-- Filter Buttons -->
<div class="portfolio-filter-container margin-top-20">
    <ul class="portfolio-filter">
        <li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            filter by:
        </li>
        <li>
		  <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value="<?php echo $customerid;?>" />
        </li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            From:
        </li>
        <li> <!-- min="<?php echo getfromdate();?>" -->
		 <input style='height:30px;z-index:0'  id="mina" name='mina' placeholder='Date From' class='form-control' type='Date' value="<?php echo getfromdate();?>" />
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            To:
        </li>
        <li>
         <input style='height:30px;z-index:0' min="<?php echo getfromdate();?>" id="maxa" name='maxa' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo gettodate();?>" />
		</li>
     </ul>
	  <ul class="portfolio-filter">
        <li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            filter by:
        </li>
		<!-- mAIN status -->
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Main Status:
        </li>
        <li>

			<SELECT style="width:150px;height:30px;z-index:0;" class="form-control" id="mainstatus" name="mainstatus" size="1" style='z-index:0'>
				<OPTION value='' <?php echo $Allstatus;?>>All Status</OPTION>
			    <?php
					$dataMainStatusSelect = $mainstatus;
					foreach($dataMainStatusArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $dataMainStatusSelect)
					  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				?>
			</SELECT>

		</li>
		<!-- Main Status -->
		<!-- Sub Status -->
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Sub Status:
        </li>
        <li>
		<div id="statusDIV" name="statusDIV">
			<SELECT style="width:150px;height:30px;z-index:0;" class="form-control" id="status" name="status" size="1" style='z-index:0'>
				<OPTION value='' <?php echo $Allstatus;?>>All Status</OPTION>
			    <?php
					$databankresponsecodeSelect = $status;
					foreach($databankresponsecodeArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $Client_ID_TypeSelect)
					  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				?>
			</SELECT>
			</div>
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Branch:
        </li>
		<li style="<?php echo $display; ?>;height:30px;z-index:0;">
		   <SELECT style="width:200px;height:30px;z-index:0;" class="form-control" id="srUserCode00" name="srUserCode00" size="1" style='z-index:0'>
				<OPTION value='' <?php echo $Allbranch;?>>All Branches</OPTION>
				<?php
					$datasrUserCodeSelect = $srUserCode00;
					foreach($branchesArr1 as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $datasrUserCodeSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
						  $companyname = $rowData[1];
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
					}
				?>
			</SELECT>
		</li>
		<!-- Sub Status -->
	</ul>
</div>
  </div>
<div class="table-responsive">
<table id="collections" class="display table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
              <th style="display:none">Branch</th>		  
              <th>Reference</th>
              <th>Acc Holder</th>
              <th>Acc Number</th>
              <th>Branch Code</th>
              <th>Service Type</th>
              <th>Amount</th>
			  <th style="display:none">Mode </th>
              <th>Date</th>
              <th>Status</th>
              <th style="display:none">Main Status</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th style="display:none"></th>				  
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th style="display:none"></th>
                      <th></th>
                      <th></th>
                      <th style="display:none"></th>
                  </tr>
      </tfoot>
      <?php

        while($row = mysqli_fetch_array($result))
        {
			$dataFilter['Status_Description'] = $row["Status_Description"];
			$mainstatus = getmainstatus($dataFilter,$databankresponsecodeArr);
          echo '
          <tr>
            <td width="1%" style="display:none">'.$row["IntUserCode"].'</td>		  
            <td width="10%">'.$row["Client_Reference_1"].'</td>
            <td width="17%">'.$row["Account_Holder"].'</td>
            <td width="17%">'.$row["Account_Number"].'</td>
            <td width="17%">'.$row["Branch_Code"].'</td>
            <td width="17%">'.$row["Service_Type"].'</td>
            <td width="5%">' .$row["Amount"].'</td>
			<td width="10%" style="display:none">'.$row["Service_Mode"].'</td>
            <td width="18%">'.$row["Action_Date"].'</td>
            <td width="10%">'.$row["Status_Description"].'</td>
            <td width="1%" style="display:none">'.$mainstatus.'</td>
          </tr>
          ';
        }
      ?>
  </table>
    </div>
    </br>
</div>
<script>

$(document).ready(function(){
	$.fn.dataTable.ext.search.push(

	function (settings, data, dataIndex){
	var min = Date.parse( $('#mina').val(), 10 );
	var max = Date.parse( $('#maxa').val(), 10 );
	var Action_Date=Date.parse(data[8]) || 0;

	var mainstatus  = data[10];
	var srUserCode00  = data[0];

	if ( ( isNaN( min ) && isNaN( max ) ) ||
		 ( isNaN( min ) && Action_Date <= max ) ||
		 ( min <= Action_Date   && isNaN( max ) ) ||
		 ( min <= Action_Date   && Action_Date <= max ) )
	{
	  //  alert("True");
	  //location.reload();
	  $(document).ready();
		return true;

	}
	//alert("false");
	return false;

	}
	);
	
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print',{
	text: 'Refresh',
                action: function ( e, dt, node, config ) {
                   location.reload(true);
                }
	
}],
        "lengthMenu": [ [ 5, 10, 100, 500, 1000], [5, 10,100,500, 1000] ],
		"order": [[ 4, "desc" ]],
		"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 6 ).footer() ).html(
      'Page Total  R'+pageTotal.toFixed(2)+'        '+'    Grand Total R'+ total.toFixed(2));

    }

});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#mina,#maxa').change( function(){table.draw();});

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );

// -- 22/02/2022
$('#srUserCode00').on('change', function()
{
   table.search( this.value ).draw();
});

$('#status').on('change', function()
{
   table.search( this.value ).draw();
});

$('#mainstatus').on('change', function()
{
   table.search( this.value ).draw();
});
/* To do
$('#mainstatus').on('change', function()
{
   //sub_statuses();
	var mainstatus    = document.getElementById('mainstatus').value;
	var status		  = document.getElementById('statusDIV');
	var sub_statuses  = '<OPTION value="">All Status</OPTION>';
	var Arr  = <?php echo json_encode($databankresponsecodeArr);?>;
	//alert(mainstatus);
	if(mainstatus == '')
	{
		for (var key in Arr)
		{
		   row = new Array();
		   row = Arr[key].split('|');
		   sub_statuses = sub_statuses+'<option value="'+row[0]+'">'+row[0]+'</option>';
		}
	}
	else
	{
		for (var key in Arr)
		{
		   row = new Array();
		   row = Arr[key].split('|');
		   if(mainstatus == row[1])
		   {
			   sub_statuses = sub_statuses+'<option value="'+row[0]+'">'+row[0]+'</option>';
		   }
		}
	}
	$('#status').html(sub_statuses);
   table.search( this.value ).draw();
});*/

});

</script>
