<?php 
require 'database.php';
$chargeid = 0;
$applicationid = 0;
$item = 0;
$count = 0;

$applicationidError = null;
$itemError = null;

// -- Globa Setting.
$currency = $_SESSION['currency'];

// --------------------- BOC 2017.04.15 ------------------------- //
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Loan Application Charges ------------------- //
  $sql = 'SELECT * FROM charge ORDER BY chargename';
  $dataCharges = $pdo->query($sql);		

  $sql = 'SELECT * FROM charge ORDER BY chargename';
  $dataChargesArr = $pdo->query($sql);		
  
//2. --------------------- Loan Application ------------------- //
  $sql = 'SELECT * FROM loanapp';
  $dataLoanApplications = $pdo->query($sql);	
  
//3. -- Get Customer Data name.
  $sql = 'SELECT * FROM customer';
  $dataCustomers = $pdo->query($sql);	

  // -- Customers 
  $ArrayCustomers = null;
  $indexInside = 0;
  
   // -- Items 
  $ArrayItems = null;
  $indexItems = 0;
  
  // -- Charges
  $ArraCharge = null;
  $indexCharges = 0;												
  $message = '';
  
  //1. -- Customers - Execute Once.
	foreach($dataCustomers as $rowCustomer)
	{
	    $ArrayCustomers[$indexInside][0] = $rowCustomer['CustomerId'];
		$ArrayCustomers[$indexInside][1] = $rowCustomer['Title'];		
		$ArrayCustomers[$indexInside][2] = $rowCustomer['FirstName'];
		$ArrayCustomers[$indexInside][3] = $rowCustomer['LastName'];
		$indexInside = $indexInside + 1;
	}
	$indexInside = 0;
	
	//2. -- dataChargesArr - Execute Once.
	foreach($dataChargesArr as $rowChargeArr)
	{
		$ArraCharge[$indexInside][0] = $rowChargeArr['chargeid'];
		$ArraCharge[$indexInside][1] = $rowChargeArr['chargename'];
		$indexInside = $indexInside + 1;
	}
	$indexInside = 0;
  
  Database::disconnect();	

// -- Request Operation Get --- //
$loanappchargesid = '';

if (isset($_GET['loanappchargesid']) ) 
{ 
$loanappchargesid = (int) $_GET['loanappchargesid']; 
}

if (isset($_GET['applicationid']) ) 
{ 
$applicationid = (int) $_GET['applicationid']; 
}

if (isset($_GET['chargeid']) ) 
{ 
$chargeid = (int) $_GET['chargeid']; 
}

if (isset($_GET['item']) ) 
{ 
$item = (int) $_GET['item']; 
}
// -- EOC Request --//  
  
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

if (!empty($_POST)) 
{   $count = 0;
	$chargeid = $_POST['chargeid'];
	$applicationid = $_POST['applicationid'];
	$item = $_POST['item'];
	$loanappchargesid = $_POST['loanappchargesid'];
	
	$valid = true;

	// -- Check Payment Charges.
	$valid = CheckFunction($applicationid,$chargeid,$item);
	
	if($valid == false)
	{
	   $count = UpdateCharges($applicationid,$chargeid,$item);
    }
	else
	{
	   $message = "Loan Application Charges Item does not exist";}
}	


// -- Add LoanCharges
function UpdateCharges($applicationid,$chargeid,$item)
{		
	if($applicationid == ''){$applicationid = 0;}
	
		$count = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO loanappcharges  (chargeid,applicationid, item) VALUES (?,?,?)"; 	
			
		$q = $pdo->prepare($sql);
		$q->execute(array($chargeid,$applicationid,$item));
		Database::disconnect();
		$count = $count + 1;
			
	return $count;
}

// -- Check if Charges Exist.
function CheckFunction($applicationid,$chargeid,$item)
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
	$valid = true;
  
 // -- Check Payment Charges.
	$sql = 'SELECT * FROM loanappcharges WHERE applicationid = ? AND item = ? AND chargeid = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($applicationid,$item,$chargeid));
    $dataloanappcharges = $q->fetchAll();	
  
	Database::disconnect();
  
  // -- SizeOf an Array.
	if(sizeof($dataloanappcharges) > 0)
	{
		$valid = false;
	}
  return $valid;
}
?>
<div class="container background-white bottom-border">
<div class='margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method='POST' class="signup-page"> 
   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Update Loan Application Charges Details
                    </a>
                </h2>
<?php # error messages
		/*if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}*/
		
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Loan Application Charges successfully updated.</p>");
		}
		else
		{
		    printf("<p class='status'>$message</p>");
		}
?>
				
</div>				
			<p><b>Application ID</b><br /></p>
			<div class="controls">
					<div class="controls">
						<?php
							$applicationidTemp = '';
							$applicationidSelect = $applicationid;
							$customerid = '';
							$customername = '';
							$customernameDisp = '';

							$ArrayApplications = null;
							$indexInside = 0;
							$status = '';
							$dateAccepted = '';
							
							foreach($dataLoanApplications as $row)
							{
								$applicationidTemp = $row['ApplicationId'];
								$customerid = $row['CustomerId'];

								$ArrayApplications[$indexInside][0] = $row['ApplicationId'];
								$ArrayApplications[$indexInside][1] = $row['CustomerId'];
								
								$status = $row['ExecApproval'];
								$dateAccepted = $row['DateAccpt'];
							
								// -- Get the title,firstname,surname.
								for ($i=0;$i<sizeof($ArrayCustomers);$i++)
								{
									if($ArrayCustomers[$i][0] == $row['CustomerId'])
									{
										$customername = $ArrayCustomers[$i][1].'.'.$ArrayCustomers[$i][2].' '.$ArrayCustomers[$i][3];
									}
								}
								
								if($applicationidTemp == $applicationidSelect)
								{
								$customernameDisp = '"'.$applicationidTemp.' - '.$customerid. '('.$customername.')-'. $status.' - '. $dateAccepted.'"';
								// echo "<OPTION value=$applicationidTemp selected>$applicationidTemp - $customerid ($customername)- $status- $dateAccepted</OPTION>";
								}	
								else
								{
									//echo "<OPTION value=$applicationidTemp>$applicationidTemp - $customerid ($customername) - $status- $dateAccepted</OPTION>";
								}
								$indexInside = $indexInside + 1;
							}
										
							if(empty($dataCharges))
							{
								//echo "<OPTION value=0>No Applications</OPTION>";
							}
					?>
				
					
					<input style='height:30px;z-index:0;display:none' id='applicationid' name='applicationid' placeholder='applicationid' class='form-control' type='text' 
				value=<?php echo $applicationid; ?> readonly>
				
				<input style='height:30px;z-index:0' id='customernameDisp' name='item' placeholder='customernameDisp' class='form-control' type='text' 
				value=<?php echo $customernameDisp; ?> readonly>
				
					</div>
			</div>

			<p><b>Payment Schedule(Item)</b><br />			
			<div class="control-group">
				<div class="controls">
				<input style='height:30px;z-index:0' id='item' name='item' placeholder='item' class='form-control' type='text' 
				value=<?php echo $item; ?> readonly>
				</div>
				
				<input style='height:30px;z-index:0;display:none' id='loanappchargesid' name='loanappchargesid' placeholder='loanappchargesid' class='form-control' type='text' 
				value=<?php echo $loanappchargesid; ?> readonly>
			</div>
					</p> 
						<p><b>Charge</b><br />			
			<!------------------------------------------------->
				<div class="controls">
					<SELECT class="form-control" id="chargeid" name="chargeid" size="1">
						<?php
							$chargeidTemp = '';
							$chargeidSelect = $chargeid;
							$chargename2 = '';
							$ArraCharge = null;
							$indexInside = 0;
							$amount = '';
											
											foreach($dataCharges as $row)
											{
												$chargeidTemp = $row['chargeid'];
												$chargename2 = $row['chargename'];
												$amount = $row['amount'];
												
												if($chargeidTemp == $chargeidSelect)
												{
												echo "<OPTION value=$chargeidTemp selected>$chargeidTemp - $chargename2 - ($currency$amount)</OPTION>";
												}
												//elseif($chargeidSelect == '')
												//{
												  //echo "<OPTION value='' selected>All</OPTION>";
												//}
												else
												{
												 echo "<OPTION value=$chargeidTemp>$chargeidTemp - $chargename2 - ($currency$amount)</OPTION>";
												}
											}
										
											if(empty($dataCharges))
											{
												echo "<OPTION value=0>No Charges</OPTION>";
											}
											?>
					</SELECT>
					<!------------------------------------------------->
					</div></p>		
			
		

		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="loanappcharges">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>