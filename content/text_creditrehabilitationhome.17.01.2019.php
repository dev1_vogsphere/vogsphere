<?php
// -- Database Declarations and config:
  include 'database.php';
  $tbl_loanapp = "loanapp";
  $db_name = "ecashpdq_eloan";
  $cragents = null;
$agentid = '';
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);

  // -- Loan Types (27.01.2017)
	foreach ($data as $row)
	{
		if($row['loantypeid'] == 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];

// ----------- BOC Global Settings 16.04.2017 -------------------
// -- Currency & Company Global Details.
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	$q->execute();
	$dataGlobal = $q->fetch(PDO::FETCH_ASSOC);

	if($dataGlobal['globalsettingsid'] >= 1)
	{
		$_SESSION['currency'] = $dataGlobal['currency'];// -- 10.02.2017
	}
	else
	{
	  $_SESSION['currency'] = 'R';
	}
// ------ EOC Global Settings 16.04.2014 -------------------------

	Database::disconnect();
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
		{
				$loggedUser = $_SESSION['username'];
				$Title = $_SESSION['Title'];
				$FirstName = $_SESSION['FirstName'];
				$LastName = $_SESSION['LastName'];
$agentid = $loggedUser;
				// -- Add Dashboard to crmanager.
				// -- 14.01.2018
				if($_SESSION['role'] == 'crmanager' || $_SESSION['role'] == 'admin')
				{
				// -- Read all user with crmanager & crconsultant role add into an array.. use the array to search through out.
				$crrolesarray[1] = "'crmanager'";
				$crrolesarray[2] = "'crconsultant'";
				$cragents = GetCRAgents($crrolesarray);
				
				// -- All Leads - customer created by credit rehabilitation agent.
				$TotalLeads = GetTotalLeads($cragents);

				// -- Leads per Agent

				// -- All Sales - credit rehabilitation programm subscribed by customer.
				$TotalSales = GetTotalLeadsSales($cragents);

				// -- Sale per Agent
									 echo "<div class='container background-white bottom-border'>
														<div class='margin-vert-30'>
															<!-- Main Text -->

													<div id='page-wrapper'>
														<div>
															<div class='col-lg-12'>
																<h1 class='page-header'>Dashboard - Credit Rehabilitation</h1>
															</div>
														</div>
													</div>".'

<!------------------------------------- Dashboards ----------------------------------->
<!-------- Leads --------->
<div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Leads</h3>
                                </div>
                            </div>
                        </div>
                        <a href="leads">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!-------- Sales ------>
			<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalSales.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Sales</h3>
                                </div>
                            </div>
                        </div>
                        <a href="creditrehabilitationsales">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div>                    </div>

<hr/>
<!-------------------------------------- EOC Dashboard -------------------------------->

									</div></div>';

}
// -- Credit Rehabilitation crconsultant.
elseif($_SESSION['role'] == 'crconsultant')   // -- Dynamic Menu 13.11.2017
{				
				// -- Read all user with crmanager & crconsultant role add into an array.. use the array to search through out.
				$crrolesarray[1] = "'crmanager'";
				$crrolesarray[2] = "'crconsultant'";
				$cragents = GetCRAgents($crrolesarray);
				
				// -- All Leads - customer created by credit rehabilitation agent.
				$TotalLeads = GetTotalLeads($cragents);

				// -- Sale per Agent
				$TotalSales = GetTotalAgentLeadsSales($agentid);

									 echo "<div class='container background-white bottom-border'>
														<div class='margin-vert-30'>
															<!-- Main Text -->

													<div id='page-wrapper'>
														<div>
															<div class='col-lg-12'>
																<h1 class='page-header'>Dashboard</h1>
															</div>
														</div>
													</div>".'

<!------------------------------------- Dashboards ----------------------------------->
<!-------- Leads --------->
<div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Leads</h3>
                                </div>
                            </div>
                        </div>
                        <a href="lead?createdby=$agentid">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!-------- Sales ------>
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalSales.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Sales</h3>
                                </div>
                            </div>
                        </div>
                        <a href="creditrehabilitationsales?createdby=$agentid">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div> </div>
<hr/>
<!-------------------------------------- EOC Dashboard -------------------------------->

									</div></div>';
}
		}
		else
		{
									// -- BCO Loan Calculator --- //
									echo '<div class="container background-white bottom-border">
									<div class="margin-vert-30">'.

														$dataGlobal['homepage']
														.'</div>
														<!-- <h3 style=" margin: 0px 40% 0 40%;">Loan Calculator</h3> -->
														<hr>
									<div class="container">
      <div class="price-box">
        <div class="row">
          <div class="col-sm-6">
                <form class="form-horizontal form-pricing" role="form">

                  <div class="price-slider">
                    <h4 class="great">Amount</h4>
                    <span>Minimum R100 is required</span>
                    <div class="col-sm-12">
                      <div id="slider_amirol"></div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Duration</h4>
                    <span>Please choose one</span>
                    <div class="btn-group btn-group-justified">
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month active-month selected-month" id="1month">1 Month</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="2month">2 Months</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="3month">3 Months</button>
                      </div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Term</h4>
                    <span>Please choose one</span>
                      <input name="sliderVal" type="hidden" id="sliderVal" value="0" readonly="readonly" />
                <input name="month" type="hidden" id="month" value="1month" readonly="readonly" />

				<input name="term" type="hidden" id="term" value="monthly" readonly="readonly" />
                      <div class="btn-group btn-group-justified">
                        <!--<div class="btn-group btn-group-lg">
                    <button type="button" class="btn btn-primary btn-lg btn-block term" id="quarterly">Quarterly</button>
                  </div>-->
                        <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term active-term selected-term" id="monthly">Monthly</button>
                  </div>
                       <!-- <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term" id="weekly">Weekly</button>
                        </div> -->
                      </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="price-form">

                  <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Annually (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">-->
                            <!-- <p class="price lead" id="total"></p>
                            <input class="price lead" name="totalprice" type="text" id="total" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Monthly Instalment(R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             <!-- <p class="price lead" id="total12"></p>-->
                            <input class="price lead" name="totalprice12" type="text" id="total12" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>
                    <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Weekly (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             --><!-- <p class="price lead" id="total52"></p>
                            <input class="price lead" name="totalprice52" type="text" id="total52" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div style="margin-top:30px"></div>
                    <!-- <hr class="style"> -->

                  <div class="form-group">
                      <div class="col-sm-12">
                        <a href="applicationForm" class="btn btn-primary btn-lg btn-block">Apply Now<span class="glyphicon glyphicon-chevron-right"></span></a>
                      </div>
                  </div>

				   <div class="col-md-6">
						<h3 class="padding-vert-10">Required Documents</h3>
						<ul class="tick animate fadeInRight" style="width:280px">
						<li>ID Document</li>
						<li>Latest 3 months Bank Statement</li>
						<li>Proof of Employment/Income</li>
						<li>Proof of Residence</li>
						</ul>
						<p></p>
						<br>
					</div>

                    <!-- <div class="form-group">
                      <div class="col-sm-12">
                          <img src="https://github.com/AmirolAhmad/Bootstrap-Calculator/blob/master/images/payment.png?raw=true" class="img-responsive payment" />
                      </div>
                    </div> -->

                  </div>

                </form>
            </div>
            <!-- <p class="text-center" style="padding-top:10px;font-size:12px;color:#2c3e50;font-style:italic;">Created by <a href="https://twitter.com/AmirolAhmad" target="_blank">AmirolAhmad</a></p> -->
        </div>

          </div>

      </div>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>';
									// -- EOC Loan Calculator -- //
									echo "<hr>
														</div>";

}

// -- Partners. -- //
echo '
  <!-- Portfolio -->
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
			<!--	<h3 style=" margin: 0px 25% 0 30%;">Our Partners Bringing Value To Our Business</h3>-->
                    <div class="row padding-top-40">
                        <ul class="portfolio-group">
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="https://www.mercantile.co.za">
                                    <figure class="animate fadeInLeft">
                                        <img alt="image1" src="assets/img/mercantile.jpg">
                                        <figcaption>
                                            <h3>Mercantile Bank</h3>
											Mercantile Bank provides a wide range of international and local banking services to the business community with a segment focus on the Portuguese market.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li> -->
                            <!-- //Portfolio Item// -->
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="http://www.vogsphere.co.za">
                                    <figure class="animate fadeIn">
                                        <img alt="image2" src="assets/img/vogsphere.png">
                                        <figcaption>
                                            <h3>Vogsphere (Pty) Ltd</h3>
											Vogsphere (Pty) Ltd is a custom software development company based in Krugersdorp, South Africa.
											The company was established in 2010 and registered in 2012.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Portfolio -->

';
// -- End Partners	-- //

// -- Dashboards Function -- //
function GetTotalLoans($status)
{
	 Global $tbl_loanapp;
	 Global $db_name;
	 $Totals = 0;
	 $check_user = "SELECT * FROM $tbl_loanapp WHERE ExecApproval= ?";

/*	 mysql_select_db("$db_name",$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

	 // Mysql_num_row is counting table row
	 $Totals = mysql_num_rows($result);
	
	*/
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($status));
		$Totals = $q->rowCount();
		
	return $Totals;
}
// -- Dashboards Function -- //

// -- Dashboards Leads -- //
function GetTotalLeads($cragents)
{
	 Global $db_name;
	 $lead_table = 'lead';
	 $Totals = 0;
	 $agents = implode(",",$cragents);

	 $check_user = "SELECT * FROM $lead_table WHERE createdby IN ($agents)";

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = $check_user;
		$q = $pdo->prepare($sql);
		$q->execute();
		$Totals = $q->rowCount();
		
	return $Totals;
}
// -- Dashboards Leads -- //

// -- Dashboards per agent Leads -- //
function GetTotalAgentLeads($agentid)
{
	 Global $db_name;
	 $Totals = 0;
	 $lead_table = 'lead';

	 $check_user = "SELECT * FROM $lead_table WHERE createdby = ?";

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($agentid));
		$Totals = $q->rowCount();
		
	return $Totals;
}
// -- Dashboards per agent Leads -- //


// -- Dashboards Leads Sales -- //
function GetTotalLeadsSales($cragents)
{
	 Global $tbl_loanapp;
	 Global $db_name;
	 	 	 $agents = implode(",",$cragents);

	 $Totals = 0;
	 $check_user = "SELECT * FROM $tbl_loanapp WHERE createdby IN($agents)";

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = $check_user;
		$q = $pdo->prepare($sql);
		$q->execute();
		$Totals = $q->rowCount();
		
	return $Totals;
}
// -- Dashboards Leads Sales -- //

// -- Dashboards Leads per agent Sales -- //
function GetTotalAgentLeadsSales($agentid)
{
	 Global $tbl_loanapp;
	 Global $db_name;
	 
	 $Totals = 0;
	 $check_user = "SELECT * FROM $tbl_loanapp WHERE createdby = ?";

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($agentid));
		$Totals = $q->rowCount();
		
	return $Totals;
}
// -- Dashboards Leads per agent Sales -- //

// --  Get All Credit Rehabilitation Agents including Managers.
function GetCRAgents($crrolesarray)
{
	Global $db_name;
	$indexInside = 0;
	$dataAgents = '';
	$ArrayAgents = null;
$roles = implode(",",$crrolesarray);
		//print_r (implode(",",$crrolesarray));

	// -- Database Connections.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	// --------------------- Charges ------------------- //
	$sql = "SELECT * FROM user WHERE role IN ($roles)";
	$q = $pdo->prepare($sql);
	$q->execute();//array(explode("",$crrolesarray)));
	$dataAgents = $q->fetchAll(PDO::FETCH_ASSOC);

	if(empty($dataAgents))
	{
		return $ArrayAgents;
	}
	else
	{
	
		foreach($dataAgents as $row)
		{
			  $ArrayAgents [$indexInside] = "'".$row['userid']."'";
			  $indexInside = $indexInside + 1;
		}	
		//print_r (implode(",",$ArrayAgents));
	
				return $ArrayAgents;
	}		
}
// --  Get All Credit Rehabilitation Agent including Managers.

?>
