<?php 
require 'database.php';
$bankid = '';
$bankidError = null; 

$banknameError = null;
$bankname = '';
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$bankname = $_POST['bankname'];
	$bankid = $_POST['bankid'];

	$valid = true;
	
	if (empty($bankname)) { $banknameError = 'Please enter Bank Name.'; $valid = false;}
	
	if (empty($bankid)) { $bankidError = 'Please enter Bank ID.'; $valid = false;}

		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO bank (bankid,bankname) VALUES(?,?) "; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($bankid,$bankname));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">   
	<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
		<div class="panel-heading">
			<h2 class="panel-title">
								<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
									 New Bank Details
								</a>
							</h2>
			<?php # error messages
					if (isset($message)) 
					{
						foreach ($message as $msg) 
						{
							printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Bank successfully added.</p>");
					}
			?>
							
			</div>				

			<p><b>Bank</b><br />
			<input style="height:30px" type='text' name='bankid'/>
			<?php if (!empty($bankidError)): ?>
			<span class="help-inline"><?php echo $bankidError;?></span>
			<?php endif; ?>
			</p>
			
			<input style="height:30px" type='text' name='bankname'/>
			<?php if (!empty($banknameError)): ?>
			<span class="help-inline"><?php echo $banknameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'bank';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>
			</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>