<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_name="access"; // Table name 
$role = '';

// -- Get roles - 23/01/2022
if(!function_exists('getroles'))
{
  function getroles($pdo)
  {
	$sql = "select * from role";
	$dataclient = $pdo->query($sql);

	$rolesArr = array();
	foreach($dataclient as $row)
	{
		$rolesArr[] = $row['role'];
	}
	return $rolesArr;
}}
  
// -- Roles...
// BOC -- Get sub branches 17/01/2022
$rolesArr = array();
$pdo = Database::connect();
$rolesArr = getroles($pdo);

if ( !empty($_POST)) 
{
   $role = $_POST['role'];
}
  
 echo "<div class='container background-white bottom-border'>";		   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Access by Role</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                    <SELECT style='width:150px;height:30px;z-index:0;' class='form-control' id='role' name='role' size='1' style='z-index:0'>";
			    
					$roleSelect = $role;
					foreach($rolesArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $roleSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				
			echo "</SELECT>";
									echo "</div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $role = $_POST['role'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($role))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE role = ? ORDER BY role,parentpage,childpage,ordersequence";
						$q = $pdo->prepare($sql);
					    $q->execute(array($role));	
						$data = $q->fetchAll();  
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name ORDER BY role,parentpage,childpage,ordersequence";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newaccess.php>Add New Access</a><p></p>"; 
						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>"; 
						echo "<tr>"; 
						echo "<td><b>Role</b></td>"; 
						echo "<td><b>Parent</b></td>"; 
						echo "<td><b>Parent Menu Item</b></td>"; 
						echo "<td><b>child</b></td>"; 
						echo "<td><b>Page</b></td>"; 
						echo "<td><b>Menu Item</b></td>"; 
						echo "<td><b>Menu Sequence</b></td>"; 
						echo "<td><b>Parent Sequence</b></td>"; 
						// CRUD - Accessordersequence
						echo "<td><b>Default</b></td>"; 
						//echo "<td><b>Can Update?</b></td>"; 
						//echo "<td><b>Can Delete?</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						
						$parent = '';
						if(!empty($row['parent'])){$parent = "<input style='height:30px' type='checkbox' id='parent' checked disabled/>";}
						else{$parent ="<input style='height:30px' type='checkbox' id='parent' disabled/>";} 

						$child = '';
						if(!empty($row['child'])){$child = "<input style='height:30px' type='checkbox' id='child' checked disabled/>";}
						else{$child ="<input style='height:30px' type='checkbox' id='child' disabled/>";} 
						
						// CRUD - Access
						$default = '';
						if(!empty($row['defaultpage'])){$default = "<input style='height:30px' type='checkbox' id='default' checked disabled/>";}
						else{$default ="<input style='height:30px' type='checkbox' id='default' disabled/>";} 

						//$create = '';
						//if(!empty($row['cancreate'])){$create = "<input style='height:30px' type='checkbox' id='create' checked disabled/>";}
						//else{$create ="<input style='height:30px' type='checkbox' id='create' disabled/>";} 
						
						//$update = '';
						//if(!empty($row['canupdate'])){$update = "<input style='height:30px' type='checkbox' id='update' checked disabled/>";}
						//else{$update ="<input style='height:30px' type='checkbox' id='update' disabled/>";} 
						
						//$delete = '';
						//if(!empty($row['candelete'])){$delete = "<input style='height:30px' type='checkbox' id='delete' checked disabled/>";}
						//else{$delete ="<input style='height:30px' type='checkbox' id='delete' disabled/>";} 
						
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['role']) . "</td>";  
						echo "<td valign='top'>" . $parent . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['parentpage']) . "</td>";  
						echo "<td valign='top'>" . $child. "</td>";  
						echo "<td valign='top'>" . nl2br( $row['childpage']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['label']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['ordersequence']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['parentsequence']) . "</td>";  	
						// CRUD - Access
						echo "<td valign='top'>" . $default. "</td>";  
						//echo "<td valign='top'>" . $update. "</td>";  
						//echo "<td valign='top'>" . $delete. "</td>";  

						// -- Logout must never be deleted.
							//if(nl2br( $row['label']) == 'Logout')
							{
							}
							//else
							{
								echo "<td valign='top'><a class='btn btn-success' href=editaccess.php?accessid={$row['accessid']}>Edit</a></td>";

								echo "<td valign='top'>
								
								<a class='btn btn-danger' href=deleteaccess.php?accessid={$row['accessid']}>Delete</a></td> "; 
								echo "</tr>";
							}
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
		<?php 
			echo "</div>";
		?>
   		<!---------------------------------------------- End Data ----------------------------------------------->