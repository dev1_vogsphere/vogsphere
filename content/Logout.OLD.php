<?php  
session_start();//session is a way to store information (in variables) to be used across multiple pages. 
session_unset(); 
session_destroy();  
/*
unset($_SESSION["loggedin"]);
unset($_SESSION["username"]);

$url = "customerLogin";
if(isset($_GET["session_expired"])) {
	$url .= "?session_expired=" . $_GET["session_expired"];
}
*/
$url = "customerLogin";

header("Location:$url");

// -- echo "<script>window.open('".$url."','_self')</script>";  //use for the redirection to some page  
?>  