<?php 
require 'database.php';
$tablename = 'chargeoption';
$chargeoptionnameError = null;
$chargeoptiondescError = null;

$chargeoptionname = '';
$chargeoptiondesc = '';
$count = 0;
$chargeoptionid = '';

if (isset($_GET['chargeoptionid']) ) 
{ 
$chargeoptionid = $_GET['chargeoptionid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$chargeoptionname = $_POST['chargeoptionname'];
	$chargeoptiondesc = $_POST['chargeoptiondesc'];;
	$valid = true;
	
	if (empty($chargeoptionname)) { $chargeoptionnameError = 'Please enter charge option name.'; $valid = false;}
	if (empty($chargeoptiondesc)) { $chargeoptiondescError = 'Please enter charge option name.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE $tablename SET chargeoptionname = ?,chargeoptiondesc = ? WHERE chargeoptionid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($chargeoptionname,$chargeoptiondesc,$chargeoptionid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tablename where chargeoptionid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($chargeoptionid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$chargeoptionname = $data['chargeoptionname'];
		$chargeoptiondesc = $data['chargeoptiondesc'];
}

?>
<div class="container background-white bottom-border">  
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
 
<div class="panel-heading">
<h2 class="text-center">
                         Charge Option
</h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>$tablename successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Charge Option</b><br />
		<input style="height:30px" type='text' name='chargeoptionname' value="<?php echo !empty($chargeoptionname)?$chargeoptionname:'';?>"/>
			<?php if (!empty($chargeoptionnameError)): ?>
			<span class="help-inline"><?php echo $chargeoptionnameError;?></span>
			<?php endif; ?>
			</p> 	
		
		<p><b>Charge Option Description</b><br />
		<input style="height:30px" type='text' name='chargeoptiondesc' value="<?php echo !empty($chargeoptiondesc)?$chargeoptiondesc:'';?>"/>
			<?php if (!empty($chargeoptiondescError)): ?>
			<span class="help-inline"><?php echo $chargeoptiondescError;?></span>
			<?php endif; ?>
			
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="chargeoption">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>