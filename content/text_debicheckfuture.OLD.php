<?php
/*Rules
The following only fields that can be updated:
1	Tracking Indicator						$tracking_indicator
2	Frequency								$Frequency
3	Mandate Initiation Date					$mandate_initiation_date
4	First Collection Date					$first_collection_date
5	Maximum Collection Amount				$maximum_collection_amount
6	Debtor Account Name						$debtor_account_name
7	Debtor Identification                   $debtor_identification
8	Document Type                           $document_type
9	Debtor Account Number                   $debtor_account_number
10  Debtor Account Type                     $debtor_account_type
11  Debtor Branch Number                    $debtor_branch_number
12  Debtor Telephone Contact Details        $debtor_contact_number
13  Debtor Email Contact Details			$debtor_email
14  Collection Day							$collection_date
15  Date Adjustment Rule Indicator			$date_adjustment_rule_indicator
16  Adjustment Category						$adjustment_category
17  Adjustment Rate							$adjustment_rate
18  Adjustment Amount						$adjustment_amount
19  First Collection Amount					$first_collection_amount
20  Debit Value Type						$debit_value_type
*/
require 'database.php';

// -- redirect if user has been logged off.
$customerLogin = 'customerLogin';

// -- its okay, navigate to login back.
if(empty(getsession('role')))
{
	redirect($customerLogin);
}

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);
$dataentry_class = array();

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_debitvalue_types';
$datadebtor_debitvalue_types = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_entry_class_codes';
$datadebtor_entry_class_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_collection_codes';
$datacollection_day_codes = $pdo->query($sql);


$sql = 'SELECT * FROM debtor_adjustment_category';
$datadebtor_adjustment_category = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_auth_codes';
$datadebtor_auth_code = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_instalment_occurrence';
$datadebtor_instalment_occurrence = $pdo->query($sql);

$notification = array();
$notificationsList = array();
// -- Declare -- //
$Frequency					    = '';
$reference 						='';
$contract_reference 			='';
$tracking_indicator 			='';
$debtor_auth_code 				='';
$auth_type 						='';
$instalment_occurence 			='';
$mandate_initiation_date 		='';
$first_collection_date 			='';
$collection_amount_currency 	='ZAR';
$collection_amount 				='';
$maximum_collection_currency 	='ZAR';
$maximum_collection_amount 		='';
$entry_class 					='';
$debtor_account_name 			='';
$debtor_identification 			='';
$debtor_account_number 			='';
$debtor_account_type 			='';
$debtor_branch_number 			='';
$debtor_contact_number 			='';
$debtor_email 					='';
$collection_date 				='';
$date_adjustment_rule_indicator  ='';
$adjustment_category 			='';
$adjustment_rate 				='';
$adjustment_amount_currency 	='ZAR';
$adjustment_amount 				='';
$first_collection_amount_currency = 'ZAR';
$first_collection_amount 		='';
$debit_value_type 				='';
$cancellation_reason 			='';
$amended 						='';
$status_code 					='';
$status_desc 					='';
$document_type 					='';
$amendment_reason_md16 			='';
$amendment_reason_text 			='';
$tracking_cancellation_indicator ='';
// -- EOC Declare -- //
// -- dropdown selection.
$collection_day_Arr = array();
foreach($datacollection_day_codes as $row)
{
	$collection_day_Arr[] = $row['collection_day_code'].'|'.$row['collection_desc'].'|'.$row['frequency_code'];
}

$Client_ID_TypeArr = array();
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}

$tracking_indicatorArr[] = 'Y|Yes';
$tracking_indicatorArr[] = 'N|No';

$date_adjustment_rule_indicatorArr[] = 'Y|Yes';
$date_adjustment_rule_indicatorArr[] = 'N|No';

$instalment_occurenceArr = array();
foreach($datadebtor_instalment_occurrence as $row)
{
	$instalment_occurenceArr[] = $row['instalment_occurrence'].'|'.$row['instalment_occurrence_desc'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}

$debtor_auth_codeArr = array();
foreach($datadebtor_auth_code as $row)
{
	$debtor_auth_codeArr[] = $row['debtor_auth_code'].'|'.$row['debtor_auth_code'].' - '.$row['debtor_auth_type'];
}

$debit_value_typeArr = array();

foreach($datadebtor_debitvalue_types as $row)
{
	$debit_value_typeArr[] = $row['debtor_debitvalue_type'].'|'.$row['debitvalue_desc'];
}

$entry_classArr = array();
foreach($datadebtor_entry_class_codes as $row)
{
	$entry_classArr[] = $row['debtor_entry_class'].'|'.$row['debtor_entry_class_desc'];
}

$datadebtor_adjustment_categoryArr = array();
foreach($datadebtor_adjustment_category as $row)
{
	$datadebtor_adjustment_categoryArr[] = $row['category'].'|'.$row['category_desc'];
}

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  if(isset($_SESSION['UserCode']))
    {

    	$Client_Id=$_SESSION['UserCode'];
     //echo $clientid;
    }
    else {
    	echo "corpid does not exist for the user";
    }

    if($sessionrole=="admin")
    {
	  $today = date('Y-m-d');
      $sql ="SELECT * FROM mandates Where mandate_initiation_date >= '$today' ORDER BY createdon DESC";
      $result = mysqli_query($connect,$sql);
    }
    else
	{
	  $today = date('Y-m-d');
      $sql ="SELECT * FROM mandates WHERE usercode ='$Client_Id' and mandate_initiation_date >= '$today'ORDER BY createdon DESC ";
      $result = mysqli_query($connect,$sql);
    }
?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">

    <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
  <h2 align="center">Amend/Cancel Mandate</h2>
    </div>
      <div class='row'>
	    <div class='input-group margin-bottom-20'>
            <span class='input-group-addon'>
                <i class='fa fa-user'></i>
            </span>
            <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>

        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="">
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>


			<select class="form-control" id="Frequency" name="Frequency" size="1" style='z-index:0'>
  						  <?php
						  	$FrequencySelect = $Frequency;

						  foreach($FrequencyArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}
						  }
						  ?>
            </select>

			</div>
		</div>
        </div>
      </form>
    </div>
  </div>
</br>
</br>
<div class="table-responsive">
<div class="messages" id="message" name="message">
  		 <?php
		 if (isset($message))
		 {
			 if($message == 'updated successfully')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);
			 }
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}
			}
		 }
		 ?>
  </div>  <!-- style="font-size: 10px;table-layout:fixed;"> -->
<table id="collections" class="table table-striped table-bordered" style="font-size: 10px;">

      <thead>
          <tr>
			<th>Reference</th>
			<th style="display:none">Tracking Indicator</th>
			<th style="display:none">Frequency</th>
			<th>Mandate Initiation Date</th>
			<th style="display:none">First Collection Date</th>
			<th style="display:none">Maximum Collection Amount</th>
			<th>Account Holder</th>
			<th style="display:none">Debtor Identification</th>
			<th style="display:none">Document Type</th>
			<th>Account Number</th>
			<th style="display:none">Debtor Account Type</th>
			<th style="display:none">Debtor Branch Number</th>
			<th style="display:none">Debtor Telephone Contact Details</th>
			<th style="display:none">Debtor Email Contact Details</th>
			<th style="display:none">Collection Day</th>
			<th style="display:none">Date Adjustment Rule Indicator</th>
			<th style="display:none">Adjustment Category</th>
			<th style="display:none">Adjustment Rate</th>
			<th style="display:none">Adjustment Amount</th>
			<th>First Collection Amount</th>
			<th style="display:none">Debit Value Type</th>
			<th>Amendment Reason</th>
			<th style="display:none">Amendment Reason(MD16)</th>
			<th>Status</th>
			<th>Action</th>
			<th></th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="4" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>
      </tfoot>
      <?php

        while($row = mysqli_fetch_array($result))
        {
			$reference 							= $row["reference"];
			$idtracking_indicator		  		= "tracking_indicator_$reference";$tracking_indicator					= $row['tracking_indicator'];
			$idFrequency                  		= "Frequency_$reference";$Frequency                          = $row['frequency'];
			$idmandate_initiation_date	  		= "mandate_initiation_date_$reference";$mandate_initiation_date	        = $row['mandate_initiation_date'];
			$idfirst_collection_date      		= "first_collection_date_$reference";$first_collection_date              = $row['first_collection_date'];
			$idmaximum_collection_amount  		= "maximum_collection_amount_$reference";$maximum_collection_amount	        = $row['maximum_collection_amount'];
			$iddebtor_account_name 	      		= "debtor_account_name_$reference";$debtor_account_name 	            = $row['debtor_account_name'];
			$iddebtor_identification 	  		= "debtor_identification_$reference";$debtor_identification 	            = $row['debtor_identification'];
			$iddocument_type	          		= "document_type_$reference";$document_type	                    = $row['document_type'];
			$iddebtor_account_number  	  		= "debtor_account_number_$reference";$debtor_account_number  	        = $row['debtor_account_number'];
			$iddebtor_account_type	      		= "debtor_account_type_$reference";$debtor_account_type	            = $row['debtor_account_type'];
			$iddebtor_branch_number  	  		= "debtor_branch_number_$reference";$debtor_branch_number  	            = $row['debtor_branch_number'];
			$iddebtor_contact_number	  		= "debtor_contact_number_$reference";$debtor_contact_number			    = $row['debtor_contact_number'];
			$iddebtor_email               		= "debtor_email_$reference";$debtor_email                       = $row['debtor_email'];
            $idcollection_date            		= "collection_date_$reference";$collection_date                    = $row['collection_date'];
            $iddate_adjustment_rule_indicator 	= "date_adjustment_rule_indicator_$reference";$date_adjustment_rule_indicator     = $row['date_adjustment_rule_indicator'];
            $idadjustment_category        		= "adjustment_category_$reference";$adjustment_category                = $row['adjustment_category'];
            $idadjustment_rate            		= "adjustment_rate_$reference";$adjustment_rate                    = $row['adjustment_rate'];
            $idadjustment_amount          		= "adjustment_amount_$reference";$adjustment_amount                  = $row['adjustment_amount'];
            $idfirst_collection_amount 	  		= "first_collection_amount_$reference";$first_collection_amount 	        = $row['first_collection_amount'];
            $iddebit_value_type           		= "debit_value_type_$reference";$debit_value_type                   = $row['debit_value_type'];
            $idamendment_reason_text            = "amendment_reason_text_$reference";$amendment_reason_text                    = $row['amendment_reason_text'];
            $idamendment_reason_md16           	= "amendment_reason_md16_$reference";$amendment_reason_md16                   = $row['amendment_reason_md16'];

          echo
		  '<tr>
            <td width="5%">'.$row["reference"].'</td>

            <td style="display:none" width="5%"><input type ="text" id = "'.$idtracking_indicator		  		.'" value="'.$row["tracking_indicator"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$idFrequency                  		.'" value="'.$row["frequency"].'" readonly /></td>
            <td width="5%">'.$row["mandate_initiation_date"].'</td>
            <td style="display:none" width="5%"><input type ="date" id = "'.$idfirst_collection_date      		.'" value="'.$row["first_collection_date"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="number" id = "'.$idmaximum_collection_amount  		.'" value="'.$row["maximum_collection_amount"].'" required="required" readonly /></td>
			<td width="5%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$iddebtor_account_name 	      		.'" value="'.$row["debtor_account_name"].'" required="required" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddebtor_identification 	  		.'" value="'.$row["debtor_identification"].'" required="required" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddocument_type	          			.'" value="'.$row["document_type"].'" readonly /></td>
            <td width="5%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$iddebtor_account_number  	  		.'" value="'.$row["debtor_account_number"].'" required="required" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddebtor_account_type	      		.'" value="'.$row["debtor_account_type"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddebtor_branch_number  	  		.'" value="'.$row["debtor_branch_number"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddebtor_contact_number	  			.'" value="'.$row["debtor_contact_number"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddebtor_email               		.'" value="'.$row["debtor_email"].'" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$idcollection_date            		.'" value="'.$row["collection_date"].'" required="required" readonly /></td>
            <td style="display:none" width="5%"><input type ="text" id = "'.$iddate_adjustment_rule_indicator 	.'" value="'.$row["date_adjustment_rule_indicator"].'" readonly /></td>
			<td style="display:none" width="5%"><input type ="text" id = "'.$idadjustment_category        		.'" value="'.$row["adjustment_category"].'" readonly /></td>';
			if($adjustment_category == 'N')
			{
			echo '<td style="display:none" width="5%"><input type ="number" id = "'.$idadjustment_rate            		.'" value="'.$row["adjustment_rate"].'" style="display:none" readonly /></td>
				 <td style="display:none" width="5%"><input type ="number" id = "'.$idadjustment_amount          		.'" value="'.$row["adjustment_amount"].'" style="display:none" readonly /></td>';
			}
			else
			{
			echo '<td style="display:none" width="5%"><input type ="number" id = "'.$idadjustment_rate            		.'" value="'.$row["adjustment_rate"].'" style="display:block" readonly /></td>
				 <td style="display:none" width="5%"><input type ="number" id = "'.$idadjustment_amount          		.'" value="'.$row["adjustment_amount"].'" style="display:block" readonly /></td>';
            }
			echo '<td width="5%"><input style="font-size: 10px;width:100%" type ="number" id = "'.$idfirst_collection_amount 	  		.'" value="'.$row["first_collection_amount"].'" readonly required="required" data-error="debtor_account_name is required." />'.$row["first_collection_amount"].'</td>';

			if($adjustment_category == 'N')
			{
				echo '<td style="display:none" width="5%"><input type ="text" id = "'.$iddebit_value_type           		.'" value="'.$row["debit_value_type"].'" style="display:none" readonly /></td>';
			}
			else
			{
				echo '<td style="display:none" width="5%"><input type ="text" id = "'.$iddebit_value_type           		.'" value="'.$row["debit_value_type"].'" style="display:block" readonly /></td>';
			}

			echo '<td width="5%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idamendment_reason_text 	.'" value="'.$row["amendment_reason_text"].'" readonly /></td>
				  <td width="5%" style="display:none"><input type ="text" id = "'.$idamendment_reason_md16        		.'" value="'.$row["amendment_reason_md16"].'" readonly /></td>';
			echo '<td width="5%">'.$row["status_desc"].'</td>';
			echo '<td width="5%">';
			if($row["mandate_initiation_date"] > date('Y-m-d'))
			{
				echo '<a style="font-size: 10px;" class="btn btn-success" id = "'.$reference.'" name = "'.$reference.'" onclick="Update(this);" data-popup-open="popup-1"  >Edit</a></td>';
				echo '<td width="5%"><a class="btn btn-danger" id = "c'.$reference.'" name = "c'.$reference.'" onclick="cancelUpdate(this);" style="display:none;font-size: 10px;" >Cancel</a></td>';
			}
			else
			{
				echo "<p></p>";
			}
			echo '&nbsp;';
			echo '</td></tr>';
        }
      ?>


  </table>
  <div class="popup" data-popup="popup-1" style="overflow-y:auto;z-index: 1;">
						<div class="popup-inner">
							<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
							<div id="myProgress">
								<div id="myBar">10%</div>
								
							</div> 
							</br> -->
										<div id="divPaymentSchedules" style="top-margin:100px;font-size: 10px;">
										
										</div>
						<!-- <div id="divSuccess">
							
							</div> -->
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>
					</div>			
				</div>
    </div>
    </br>
</div>

<script>


$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
	       //alert(data[19]);

    var Action_Date=Date.parse(data[3]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 19)
            .data()
            .reduce( function (a, b) {
                //\return (number_format((intVal(a) + intVal(b)),2'.', ''));
				//alert(a);
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 19, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 19 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});

function cancelUpdate(ele)
{
	var id 								 = ele.id;//substr(ele.id,1);
	id = id.substr(1);
		//alert(id);
	var idtracking_indicator		  	 = "tracking_indicator_"+id;
	var idFrequency                  	 = "Frequency_"+id;
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;
	var idfirst_collection_date      	 = "first_collection_date_"+id;
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id;
	var iddebtor_identification 	  	 = "debtor_identification_"+id;
	var iddocument_type	          		 = "document_type_"+id;
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;
	var iddebtor_email               	 = "debtor_email_"+id;
	var idcollection_date            	 = "collection_date_"+id;
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;
	var idadjustment_rate            	 = "adjustment_rate_"+id;
	var idadjustment_amount          	 = "adjustment_amount_"+id;
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id;
	var iddebit_value_type           	 = "debit_value_type_"+id;

	var idamendment_reason_text           = "amendment_reason_text_"+id;
    var idamendment_reason_md16           = "amendment_reason_md16_"+id;

	// -- return to original position.
		document.getElementById(id).innerHTML = 'Edit';
		ele.style.display = "none";

		// -- tracking_indicator					--//
		id_= idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- mandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = true;

		// -- maximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- debtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = true;

		// -- debtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = true;

		// -- document_type	          				--//
		id_= iddocument_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;

		// -- iddebtor_account_type	      			--//
		id_= iddebtor_account_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_branch_number  	  			--//
		id_= iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = true;

		// -- debtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = true;

		// -- collection_day            			--//
		id_= idcollection_date;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value='"+value+"' readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- date_adjustment_rule_indicator		--//
		id_= iddate_adjustment_rule_indicator;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		id_= idadjustment_category;
		value = document.getElementById(id_);
		valueCat = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
			valueCat = value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- iddebit_value_type           			--//
		id_= iddebit_value_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- idamendment_reason_text 	  	--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = true;

		// -- idamendment_reason_text 	  	--//
		//id_= idamendment_reason_text;
		//document.getElementById(id_).readOnly = true;

}
// #myInput is a <input type="text"> element
function Update(ele)
{

		var id 								 = ele.id;
		//alert(id);
	jQuery(document).ready(function()
			{
html = '<div class="container background-white bottom-border"><br />';
html = html+'<form id="contact-form" method="POST" action="#"  role="form">';
html = html+'<h2 class="text-center">Register DebiCheck Mandate</h2>';
html = html+'<div class="messages"></div>';
html = html+'<div class="controls">';
html = html+'</form>';
html = html+'</div>';
    file = "i_updatedebicheckmandate.php?reference="+id;
	   /*  xhttp = new XMLHttpRequest();
      xhttp.open("GET", file, true);
	  xhttp.send();
	  html = xhttp.responseText;*/
//alert(html);
				jQuery("#divPaymentSchedules").load(file);

jQuery.ajax({
	url : file,
            dataType: "php",
            success : function (data) 
			{
                //$(".text").html(data);
				html = data;
				alert("Modise- "+html);
				jQuery("#divPaymentSchedules").load(file);
            }
		});

			});	
}
function toggleUpdate(ele)
{
	var id 								 = ele.id;
	var idtracking_indicator		  	 = "tracking_indicator_"+id;
	var idFrequency                  	 = "Frequency_"+id;
	var idmandate_initiation_date	  	 = "mandate_initiation_date_"+id;
	var idfirst_collection_date      	 = "first_collection_date_"+id;
	var idmaximum_collection_amount  	 = "maximum_collection_amount_"+id;
	var iddebtor_account_name 	      	 = "debtor_account_name_"+id;
	var iddebtor_identification 	  	 = "debtor_identification_"+id;
	var iddocument_type	          		 = "document_type_"+id;
	var iddebtor_account_number  	  	 = "debtor_account_number_"+id;
	var iddebtor_account_type	      	 = "debtor_account_type_"+id;
	var iddebtor_branch_number  	  	 = "debtor_branch_number_"+id;
	var iddebtor_contact_number	  		 = "debtor_contact_number_"+id;
	var iddebtor_email               	 = "debtor_email_"+id;
	var idcollection_date            	 = "collection_date_"+id;
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator_"+id;
	var idadjustment_category        	 = "adjustment_category_"+id;
	var idadjustment_rate            	 = "adjustment_rate_"+id;
	var idadjustment_amount          	 = "adjustment_amount_"+id;
	var idfirst_collection_amount 	  	 = "first_collection_amount_"+id;
	var iddebit_value_type           	 = "debit_value_type_"+id;

	var idamendment_reason_text           = "amendment_reason_text_"+id;
    var idamendment_reason_md16           = "amendment_reason_md16_"+id;


    var value = '';
	// -- if Equal save it mean we are on edit mode
	// -- some elements needs to be change from text to dropdown.
	if(ele.innerHTML == 'Save')
	{
		// -- Validate Required if Empty.
	// -- Update debicheck mandate.
		var objects_array =
		{'tracking_indicator' 			: idtracking_indicator
		,'Frequency' 						: idFrequency
		,'mandate_initiation_date' 		: idmandate_initiation_date
		//,'first_collection_date' 			: idfirst_collection_date
		,'maximum_collection_amount' 		: idmaximum_collection_amount
		,'debtor_account_name' 			: iddebtor_account_name
		,'debtor_identification' 			: iddebtor_identification
		,'document_type' 					: iddocument_type
		,'debtor_account_number' 			: iddebtor_account_number
		,'debtor_account_type' 			: iddebtor_account_type
		,'debtor_branch_number' 			: iddebtor_branch_number
		//,'debtor_contact_number' 			: iddebtor_contact_number
		//,'debtor_email' 					: iddebtor_email
		,'collection_date' 				: idcollection_date
		,'date_adjustment_rule_indicator' 	: iddate_adjustment_rule_indicator
		,'adjustment_category' 			: idadjustment_category
		//,'adjustment_rate' 				: idadjustment_rate
		//,'adjustment_amount' 				: idadjustment_amount
		,'first_collection_amount' 		: idfirst_collection_amount
		,'debit_value_type' 			: iddebit_value_type
		,'amendment_reason_text' : idamendment_reason_text};

		if(IsEmpty(objects_array))
		{
		ele.innerHTML = 'Edit';

		// -- tracking_indicator					--//
		id_= idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		id_= idFrequency;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- mandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = true;

		// -- maximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- debtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = true;

		// -- debtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = true;

		// -- document_type	          				--//
		id_= iddocument_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = true;

		// -- iddebtor_account_type	      			--//
		id_= iddebtor_account_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_branch_number  	  			--//
		id_= iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- debtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = true;

		// -- debtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = true;

		// -- collection_day            			--//
		id_= idcollection_date;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		Frequency = document.getElementById(idFrequency).value;
		//alert(Frequency);

		if(Frequency != 'WEEK' && Frequency != 'ADHO')
		{
			var d = new Date(value);
			value = d.getDate();
			if(value < 10){value = '0'+value;}
			//alert(value);
		}
		new_html_el = "<input type ='text' id = "+id_+" value='"+value+"' readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- date_adjustment_rule_indicator		--//
		id_= iddate_adjustment_rule_indicator;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		id_= idadjustment_category;
		value = document.getElementById(id_);
		valueCat = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
			valueCat = value;
		}
		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = true;

		// -- first_collection_amount 	  			--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = true;

		// -- iddebit_value_type           			--//
		id_= iddebit_value_type;
		value = document.getElementById(id_);
		// -- previusly lookup element.
		if(isObject(value))
		{
			value = value.value;
		}

		new_html_el = "<input type ='text' id = "+id_+" value="+value+" readonly />";
		document.getElementById(id_).outerHTML = new_html_el;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- non display of cancel.
		id_ = "c"+ele.id;
		document.getElementById(id_).style.display = 'none';

		// -- Values to updated.
		var values_array =
			{'tracking_indicator' 			: document.getElementById(idtracking_indicator).value
		,'Frequency' 						: document.getElementById(idFrequency).value
		,'mandate_initiation_date' 			: document.getElementById(idmandate_initiation_date).value
		,'first_collection_date' 			: document.getElementById(idfirst_collection_date).value
		,'maximum_collection_amount' 		: document.getElementById(idmaximum_collection_amount).value
		,'debtor_account_name' 				: document.getElementById(iddebtor_account_name).value
		,'debtor_identification' 			: document.getElementById(iddebtor_identification).value
		,'document_type' 					: document.getElementById(iddocument_type).value
		,'debtor_account_number' 			: document.getElementById(iddebtor_account_number).value
		,'debtor_account_type' 				: document.getElementById(iddebtor_account_type).value
		,'debtor_branch_number' 			: document.getElementById(iddebtor_branch_number).value
		,'debtor_contact_number' 			: document.getElementById(iddebtor_contact_number).value
		,'debtor_email' 					: document.getElementById(iddebtor_email).value
		,'collection_date' 					: document.getElementById(idcollection_date).value
		,'date_adjustment_rule_indicator' 	: document.getElementById(iddate_adjustment_rule_indicator).value
		,'adjustment_category' 				: document.getElementById(idadjustment_category).value
		,'adjustment_rate' 					: document.getElementById(idadjustment_rate).value
		,'adjustment_amount' 				: document.getElementById(idadjustment_amount).value
		,'first_collection_amount' 			: document.getElementById(idfirst_collection_amount).value
		,'debit_value_type' 				: document.getElementById(iddebit_value_type).value
		,'amendment_reason_text' 			: document.getElementById(idamendment_reason_text).value
		,'amendment_reason_md16'			: document.getElementById(idamendment_reason_md16).value };

		// -- Pass reference & array of values to updated.
		updatedebicheckmandate(id,values_array);
	   }
	}
	else
	{
		id_ = "c"+ele.id;
		//alert(id_);
		document.getElementById(id_).style.display = 'block';
		ele.innerHTML = 'Save';
		// -- convert php array into jQuery arrays.
		// -- tracking_indicator					--//
		Arr = <?php echo json_encode($tracking_indicatorArr); ?>;
		id_ = idtracking_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- Frequency                  			--//
		Arr = <?php echo json_encode($FrequencyArr); ?>;
		id_ = idFrequency;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' onChange='valueselect2(this,"+idcollection_date+");'>";
		for (var key in Arr)
		{
			if(Arr[key] !== " "){
			row = new Array();
			row = Arr[key].split('|');
			  if(value === row[0])	// -- selected
			  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
			  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}

			}
		}
		new_html_el = new_html_el+"</select>";
		//alert(new_html_el);
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idmandate_initiation_date	  			--//
		id_= idmandate_initiation_date;
		document.getElementById(id_).readOnly = false;

		// -- idfirst_collection_date      			--//
		id_= idfirst_collection_date;
		document.getElementById(id_).readOnly = false;

		// -- idmaximum_collection_amount  			--//
		id_= idmaximum_collection_amount;
		document.getElementById(id_).readOnly = false;

		// -- iddebtor_account_name 	      			--//
		id_= iddebtor_account_name;
		document.getElementById(id_).readOnly = false;

		// -- iddebtor_identification 	  			--//
		id_= iddebtor_identification;
		document.getElementById(id_).readOnly = false;

		// -- document_type	          				--//
		Arr = <?php echo json_encode($Client_ID_TypeArr); ?>;
		id_ = iddocument_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddebtor_account_number  	  			--//
		id_= iddebtor_account_number;
		document.getElementById(id_).readOnly = false;

		// -- debtor_account_type	      			--//
		Arr = <?php echo json_encode($Account_TypeArr); ?>;
		id_ = iddebtor_account_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;
		// -- iddebtor_branch_number  	  			--//
		Arr = <?php echo json_encode($Branch_CodeArr); ?>;
		id_ = iddebtor_branch_number;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddebtor_contact_number	  				--//
		id_= iddebtor_contact_number;
		document.getElementById(id_).readOnly = false;
		// -- iddebtor_email               			--//
		id_= iddebtor_email;
		document.getElementById(id_).readOnly = false;

		// -- idcollection_date            			--//
		id_= idcollection_date;
		value = document.getElementById(id_).value;
		value2 = document.getElementById(idFrequency).value;
		new_html_el = "";
		
		if(value2 != 'WEEK' && value2 != 'ADHO')
		{
		  value = document.getElementById(id_).value;
		  var d = GetFormattedDate(value);
		  //alert(d);
		  new_html_el = "<input type ='date' name = "+id_+" id = "+id_+" value = "+d+" />";
		  document.getElementById(id_).outerHTML = new_html_el;
		}
		else
		{
		Arr = <?php echo json_encode($collection_day_Arr); ?>;
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			//alert(Arr[key]);
			if(row[2] === document.getElementById(idFrequency).value) // for selected frequency.
			{
				if(value === row[0])	// -- selected
				{new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[0]+' - '+row[1]+"</option>";}
				else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[0]+' - '+row[1]+"</option>";}
			}
		}
		new_html_el = new_html_el+"</select>";
		}
		document.getElementById(id_).outerHTML = new_html_el;

		// -- iddate_adjustment_rule_indicator		--//
		Arr = <?php echo json_encode($date_adjustment_rule_indicatorArr); ?>;
		id_ = iddate_adjustment_rule_indicator;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"'>";
		for (var key in Arr)
		{
				row = new Array();
				row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- adjustment_category        			--//
		Arr = <?php echo json_encode($datadebtor_adjustment_categoryArr); ?>;
		id_ = idadjustment_category;
		value = document.getElementById(id_).value;
		valueCat = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' onChange='adjustment_category1(this,"+idadjustment_rate+","+idadjustment_amount+","+iddebit_value_type+");'>";
		for (var key in Arr)
		{
				row = new Array();
				row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		// -- idadjustment_rate            			--//
		id_= idadjustment_rate;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = false;

		// -- idadjustment_amount          			--//
		id_= idadjustment_amount;
		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}
		document.getElementById(id_).readOnly = false;

		// -- idfirst_collection_amount 	  			--//
		id_= idfirst_collection_amount;
		document.getElementById(id_).readOnly = false;

		// -- iddebit_value_type           			--//
		Arr = <?php echo json_encode($debit_value_typeArr); ?>;
		id_ = iddebit_value_type;
		value = document.getElementById(id_).value;
		new_html_el = "";
		new_html_el = "<select class='form-control' id='"+id_+"' name='"+id_+"' style='display:block'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		document.getElementById(id_).outerHTML = new_html_el;

		if(valueCat === 'N')
		{document.getElementById(id_).style.display = "none";}
		else
		{document.getElementById(id_).style.display = "block";}

		// -- idamendment_reason_text 	  	--//
		id_= idamendment_reason_text;
		document.getElementById(id_).readOnly = false;

	}
}


function updatedebicheckmandate(id,list)
{
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"updatedebicheckmandate.php",
				       data:{id_:id,list:list},
						success: function(data)
						 {
							alert(data);
							jQuery("#message").html(data);
						 }
					});
				});
			  return false;
}
function valueselect2(sel,ref)
 {
		idcollection_day =  ref.id;
		var frequency = sel.options[sel.selectedIndex].value;
		var selectedCollection_Day = ref.value;
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectCollectionDays.php",
				       data:{frequency:frequency,collection_day:selectedCollection_Day,id_:idcollection_day},
						success: function(data)
						 {
							//alert(data);
							jQuery("#"+idcollection_day).replaceWith(data);
						 }
					});
				});

			  return false;
  }
function adjustment_category1(sel,rate,amount,value_type)
 {
	var adjustment_category = sel.options[sel.selectedIndex].value;

	if(adjustment_category == 'N')
	{
	  	rate.style.display = "none";
	    amount.style.display = "none";
		value_type.style.display = "none";

		rate.value = '0,00';
		amount.value = '0,00';
		value_type.selectedIndex = 0; //Default to Fixed contract
	}
	else
	{
	  	rate.style.display = "block";
		amount.style.display = "block";
		value_type.style.display = "block";
	}
  			  return false;
 }

 isObject = function(Obj)
 {
                 // object is of type Element
            return Obj instanceof Element;
};

function IsEmpty(list)
{ // Going throuh a list to validate mandatory fields.
	for(var item in list)
	{
		id_ = list[item];
		if(document.getElementById(id_).value  === "" || document.getElementById(id_).value  === " ")
		{	document.getElementById(id_).focus();
			    alert(id_+" is required.");
				    return false;
		}
	}
  return true;
}


function GetFormattedDate(in_day) 
{
    var todayTime = new Date();
    var month = todayTime .getMonth() + 1;
	if(month<10) 
	{
		month='0'+month;
	} 
	var day = '';
	if(in_day == '')
	 {day = todayTime .getDate();}
	 else
	 {
		day = in_day; 
	 }
    var year = todayTime .getFullYear();
    return year  + "-" + month  + "-" + day;
}
$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");   
		  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();*/
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

</script>
