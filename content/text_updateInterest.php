<!-- Start Update the eLoan - Application Data -->
<?php 
	
require 'database.php';
$loantypeid = null;
$ApplicationId = null;
$count = 0;
// Id number
$lastchangedby = $_SESSION['username'];
$lastchangedbydate = date('Y-m-d');
			
$loantypedescError = null;
$percentageError = null; 
$lastchangedbyError = null;
$lastchangedbydateError = null;	
		
// -- Loan Type Id
if ( !empty($_GET['loantypeid'])) 
{
   $loantypeid = $_REQUEST['loantypeid'];
}

if ( !empty($_POST)) 
{

// keep track validation errors - 		// -- Loan Type Details
$loantypedescError = null;
$percentageError = null; 

// keep track post values
$loantypedesc = $_POST['loantypedesc'];
$percentage = $_POST['percentage'];

// validate input - 		// -- Loan Type Details
		$valid = true;
		if (empty($loantypedesc)) { $loantypedescError = 'Please enter LoanTypeDesc'; $valid = false;}
		
		if (empty($percentage)) { $percentageError = 'Please enter a percentage'; $valid = false;}
				
		// update data
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE loantype as L";
			$sql = $sql." SET loantypedesc = ?, percentage = ?,lastchangedby = ?,lastchangedbydate = ?";
			$sql = $sql." WHERE L.loantypeid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($loantypedesc,$percentage,$lastchangedby,$lastchangedbydate,$loantypeid));
			// -- Update the $_SESSION values. - 10.02.2017.
			$_SESSION['personalloan'] = $percentage;
			$_SESSION['loantypedesc'] = $loantypedesc;
			// -- EOC 10.02.2017
			Database::disconnect();
			$count++;
	} else 
	{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from loantype where loantypeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($loantypeid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		$loantypedesc = $data['loantypedesc'];
		$percentage = $data['percentage'];
	}
}
else
{
	$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from loantype where loantypeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($loantypeid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		$loantypedesc = $data['loantypedesc'];
		$percentage = $data['percentage'];
}

?>
<div class="container background-white bottom-border">   
		<div class="row">
 
<div class="span10 offset1">
  <form  action="updateInterest?loantypeid=<?php echo $loantypeid?>" method="post">
    <div class="table-responsive">

<table class=   "table table-user-information">
<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->		
<tr>
<td>
<div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Interest Rate Details
                    </a>
                </h2>
</div>	

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Interest Rate information updated successfully!</p>\n", $count);
		}
		?>				
</td>						
		
</tr>

<tr>						
	<td> 
	<div class="control-group <?php echo !empty($loantypeidError)?'error':'';?>">
		<label class="control-label">Loan Type Id</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="loantypeid" type="text"  placeholder="loantypeid" value="<?php echo !empty($loantypeid)?$loantypeid:'';?>" readonly>
			<?php if (!empty($loantypeidError)): ?>
			<span class="help-inline"><?php echo $loantypeidError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($loantypedescError)?'error':'';?>">
		<label class="control-label">Loan Type Desc</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="loantypedesc" type="text"  placeholder="loantypedesc" value="<?php echo !empty($loantypedesc)?$loantypedesc:'';?>">
			<?php if (!empty($loantypedescError)): ?>
			<span class="help-inline"><?php echo $loantypedescError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
 
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($lastchangedbyError)?'error':'';?>">
		<label class="control-label">last Changed by</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="lastchangedby" type="text"  placeholder="lastchangedby" value="<?php echo !empty($lastchangedby)?$lastchangedby:'';?>" readonly>
			<?php if (!empty($lastchangedbyError)): ?>
			<span class="help-inline"><?php echo $lastchangedbyError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($lastchangedbydateError)?'error':'';?>">
		<label class="control-label">last Changed By Date</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="lastchangedbydate" type="text"  placeholder="lastchangedbydate" value="<?php echo !empty($lastchangedbydate)?$lastchangedbydate:'';?>" readonly>
			<?php if (!empty($lastchangedbydateError)): ?>
			<span class="help-inline"><?php echo $lastchangedbydateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td> 
	<div class="control-group <?php echo !empty($percentageError)?'error':'';?>">
		<label class="control-label">Percentage(%)</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="percentage" type="text"  placeholder="percentage" value="<?php echo !empty($percentage)?$percentage:'';?>">
			<?php if (!empty($percentageError)): ?>
			<span class="help-inline"><?php echo $percentageError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	
<!-- Buttons -->
						<tr>
							<td><div class="form-actions">
							  <button type="submit" class="btn btn-success">Update</button>
						      <a class="btn btn-primary" href="globalsettings">Back</a>
							</div></td>
						</tr>
						</table>
						</div>
					</form>
				</div>
				
    </div> <!-- /container -->
	</div> 