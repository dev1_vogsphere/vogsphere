<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM entry_class';
$dataentry_class = $pdo->query($sql);


$notification = array();
$notificationsList = array();

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
$Account_Holder='';
$Account_Type  ='';
$Account_Number='';
$Branch_Code   ='';
$No_Payments   ='';
$Amount        ='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class   ='';
$Bank_Reference='';
$contact_number = '';
$Notify = '';

$entry_classSelect = '';
$message = '';
/* -- dropdown selection. */
$Client_ID_TypeArr = array();
$Client_ID_TypeArr[] = 'South African Identification Number';
$Client_ID_TypeArr[] = 'Passport';
$Client_ID_TypeArr[] = 'South African Business Registration Number';
$Client_ID_TypeArr[] = 'Foreign Passport';

/*
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}
*/

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$dataentry_classArr = Array();
foreach($dataentry_class as $row)
{
	$dataentry_classArr[] = $row['identry_class'].'|'.$row['Entry_Class_Des'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
$FrequencyArr[] = 'Last Working day of the Month';
/*
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}
*/

$NotifyArr[] = 'No';
$NotifyArr[] = 'Yes';

$arrayInstruction = null;

$UserCode = null;
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}

// BOC -- Get sub branches 22/02/2022
$branchesArr = array();
$pdoObj = Database::connectDB();
$branchesArr = getsubbranches($pdoObj,$IntUserCode);
// EOC -- Get sub branches 22/02/2022

   /*if (!empty($_POST))
   {
	   // -- Logged User Details.
		if(getsession('username'))
		{
			$userid = getsession('username');
		}
	   		$Service_Mode = $_POST['Service_Mode'];
			$Service_Type = $_POST['Service_Type'];
			$entry_class  = $_POST['entry_class'];

							//print_r($_POST);
							$arrayInstruction[1] = $UserCode;//$_POST['Client_Id'];
							// -- UTF-8
							if(isset($_POST['Reference_1'])){$arrayInstruction[2] = $_POST['Reference_1'];}else{$arrayInstruction[2] = '';}
							// -- UTF-8 DOM
							if(empty($arrayInstruction[2])){if(isset($_POST['\ufeffReference 1'])){$arrayInstruction[2] = $_POST['\ufeffReference 1'];}else{$arrayInstruction[2] = '';}}
							if(isset($_POST['Reference_2'])){$arrayInstruction[3] = $_POST['Reference_2'];}else{$arrayInstruction[3] = '';}
							//if(isset($_POST['Reference 2'])){$arrayInstruction[3] = $_POST['Reference 2'];}else{$arrayInstruction[3] = '';}
							if(isset($_POST['Client_ID_Type'])){$arrayInstruction[4] = $_POST['Client_ID_Type'];}else{$arrayInstruction[4] = '';}
							if(isset($_POST['Client_ID_No'])){$arrayInstruction[5] = $_POST['Client_ID_No'];}else{$arrayInstruction[5] = '';}
							if(isset($_POST['Initials'])){$arrayInstruction[6] = $_POST['Initials'];}else{$arrayInstruction[6] = '';}
							if(isset($_POST['Account_Holder'])){$arrayInstruction[7] = $_POST['Account_Holder'];}else{$arrayInstruction[7] = '';}
							if(isset($_POST['Account_Type'])){$arrayInstruction[8] = $_POST['Account_Type'];}else{$arrayInstruction[8] = '';}
							if(isset($_POST['Account_Number'])){$arrayInstruction[9] = $_POST['Account_Number'];}else{$arrayInstruction[9] = '';}
// -- User bank name on the spreadsheet to get the branchcode.
							if(isset($_POST['Branch_Code'])){$arrayInstruction[10] = $_POST['Branch_Code'];}else{$arrayInstruction[10] = '';}//$_POST['Bank Name'];
							if(isset($_POST['No_Payments'])){$arrayInstruction[11] = $_POST['No_Payments'];}else{$arrayInstruction[11] = '';}
							if(isset($_POST['Service_Type'])){$arrayInstruction[12] = $_POST['Service_Type'];}else{$arrayInstruction[12] = '';}
							if(isset($_POST['Service_Mode'])){$arrayInstruction[13] = $_POST['Service_Mode'];}else{$arrayInstruction[13] = '';}
							if(isset($_POST['Frequency'])){$arrayInstruction[14] = $_POST['Frequency'];}else{$arrayInstruction[14] = '';}
							if(isset($_POST['Action_Date'])){$arrayInstruction[15] = $_POST['Action_Date'];}else{$arrayInstruction[15] = '';}
							if(isset($_POST['Bank_Reference'])){$arrayInstruction[16] = $_POST['Bank_Reference'];}else{$arrayInstruction[16] = '';}
							if(isset($_POST['Amount'])){$arrayInstruction[17] = $_POST['Amount'];}else{$arrayInstruction[17] = '0.00';}
							$arrayInstruction[18] = '1';//$_POST['Status_Code'];
							$arrayInstruction[19] = 'pending';//$_POST['Status_Description'];
							$arrayInstruction[20] = '';//$_POST['mandateid'];
							$arrayInstruction[21] = '';//$_POST['mandateitem'];
							$arrayInstruction[22] = $IntUserCode;
							$arrayInstruction[23] = $UserCode;


							if(isset($_POST['entry_class'])){$arrayInstruction[24] = $_POST['entry_class'];}else{$arrayInstruction[24] = 0;}
							if(isset($_POST['contact_number'])){$arrayInstruction[25] = $_POST['contact_number'];}else{$arrayInstruction[25] = '';}
							if(isset($_POST['Notify'])){$arrayInstruction[26] = $_POST['Notify'];}else{$arrayInstruction[26] = '';}
							$arrayInstruction[27] = $userid;

$Reference_1   = $arrayInstruction[2];
$Reference_2   = $arrayInstruction[3];
$Client_ID_No  =$arrayInstruction[5];
$Initials      =$arrayInstruction[6];
$Account_Holder=$arrayInstruction[7];
$Account_Type  =$arrayInstruction[8];
$Account_Number=$arrayInstruction[9];
$Branch_Code   =$arrayInstruction[10];
$No_Payments   =$arrayInstruction[11];
$Amount        =$arrayInstruction[17];
$Frequency     =$arrayInstruction[14];
$Action_Date   =$arrayInstruction[15];
$Service_Mode  =$arrayInstruction[13];
$Service_Type  =$arrayInstruction[12];
$entry_class   =$arrayInstruction[24];

$Bank_Reference=$arrayInstruction[16];
$contact_number = $arrayInstruction[25];
$Client_ID_Type = $arrayInstruction[4];
$Notify = $arrayInstruction[26];

											$entry_classSelect = $entry_class;

	   // -- Default Mode
	  $sql = "SELECT * FROM entry_class where service_mode = '".$Service_Mode."'";
	  $dataentry_class = $pdo->query($sql);

							$message = auto_create_billinstruction($arrayInstruction);
							//echo $message.' '.$arrayInstruction[26].'<br/>';
							if($message == 'success')
							{
								$smstemplatename = 'single_debitorder_uploaded';
								$applicationid = 0;

								// -- Sent to info@ecashmeup.com
								auto_email_collection('8707135963082',$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								if($arrayInstruction[26] == 'Yes')
								{
									$smstemplatename = 'debitorder_notification';
									$applicationid = 0;
									$Action_Date = $arrayInstruction[15];
									$Reference_1 = $arrayInstruction[2];
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{
										$notification['phone'] 		 	= $arrayInstruction[25];
										$notification['content'] 	 	= $contents;
										$notification['reference_1'] 	= $arrayInstruction[2];
										$notification['usercode'] 	 	= $UserCode;
										$notification['createdby'] 	 	= $userid;
										$notification['IntUserCode'] 	= $arrayInstruction[22];
										$notification['amount'] 		= $arrayInstruction[17];
										$notification['Account_Holder'] = $arrayInstruction[7];
										// --
										$notificationsList[] = $notification;	// add into list
										// -- notify clients who signed for notifications(queue)
										if(!empty($notificationsList)){auto_sms_queue($notificationsList); }

									}
								}
							}


   }
   else
   {
	   // -- Default Mode
	  $Service_Mode = "EFT";
	  $sql = "SELECT * FROM entry_class where service_mode = '".$Service_Mode."'";
	  $dataentry_class = $pdo->query($sql);

   }*/

// -- Default Mode
$Service_Mode = "EFT";
$sql = "SELECT * FROM entry_class where service_mode = '".$Service_Mode."'";
$dataentry_class = $pdo->query($sql);
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>
<div class="container background-white bottom-border">
<br/>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a id="step1"  href="#step-1" type="button" class="btn btn-primary btn-circle inactiveLink">1</a>
            <p><strong>Account Information</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
            <p><strong>Banking Details</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
            <p><strong>Debit Order Details</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">4</a>
            <p><strong>Review</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-5" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">5</a>
            <p><strong>Complete</strong></p>
        </div>
    </div>
</div>
</br>
<form id="contact-form2" method="POST" action="#" style="display:block" role="form">
	<div class="row setup-content" id="step-1">
			<div class="controls">
				<div class="container-fluid" style="border:1px solid #ccc ">
					</br>
					<div class="panel-body">
					<div class="row">
					<div class="control-group">
						<div class="col-md-3" style="display:block">
							<div class="form-group">
								<label for="form_Initials"><strong>Initials:</strong><span style="color:red">*</span></label>
								<input required id="Initials" type="text" name="Initials" class="form-control" placeholder="Enter Benefiary Initials"  maxlength="3" data-error="Initials is required." value = "">
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-3" style="display:inline">
							<div class="form-group">
								<input id="User_Code" type="hidden" name="User_Code" class="form-control" value="<?php $_SESSION['UserCode']; ?>" placeholder="" required="required" data-error="Bank Reference is required.">
								<label for="form_Initials"><strong>Branch:</strong><span style="color:red">*</span></label>                         
								<ul class="portfolio-filter">
						
									<li style="<?php echo $display; ?>;height:30px;z-index:0;">
									<SELECT style="width:200px;height:30px;z-index:0;" class="form-control" id="srUserCode" name="srUserCode" size="1" style='z-index:0'>
											<?php
												$datasrUserCodeSelect = $srUserCode;
												foreach($branchesArr as $row)
												{
												$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												if($rowData[0] == $datasrUserCodeSelect)
												{
													echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
													$companyname = $rowData[1];
												}
												else
												{echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
												}
											?>
										</SELECT>
									</li>
								</ul>
							</div>
						</div>
					</div>
					</div>
					<div class="row">
					<div class="control-group">
						<div class="col-md-3">
							<div class="form-group">
								<label for="form_Reference_1"><strong>Customer Reference:</strong><span style="color:red">*</span></label>
								<input id="Reference_1" type="text" name="Reference_1" class="form-control" placeholder="Enter Customer Reference Number" required="optional" data-error="Reference 1 is required." maxlength="20" value ="<?php echo $Reference_1;?>" />
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="form_Reference_2"><strong>Contract Reference:</strong></label>
								<input id="Reference_2" type="text" name="Reference_2" class="form-control" placeholder="Enter Customer Contract Number" maxlength="14" value = "<?php echo $Reference_2;?>">
								<div class="help-block with-errors"></div>
							</div>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="form_Client_ID_Type"><strong>Customer Identification Type:</strong></label>
								<select class="form-control" id="Client_ID_Type" name="Client_ID_Type">
								<?php
								$Client_ID_TypeSelect = $Client_ID_Type;
								foreach($Client_ID_TypeArr as $row)
								{
									$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
									if($rowData[0] == $Client_ID_TypeSelect)
									{echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
									else
									{echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
								}
								?>
								</select>
								<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="form_Client_ID_No"><strong>Customer Identification Number:</strong></label>
								<input id="Client_ID_No" type="number" name="Client_ID_No" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" class="form-control" placeholder="Enter Customer Identification Number"  value = "<?php echo $Client_ID_No;?>">
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</br>
		<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
	</div>
	<br/>
	
<div class="row setup-content" id="step-2">
	<div class="col-xs-12">
		<div class="col-md-12">

			 <div class="container-fluid" style="border:1px solid #ccc ">
             </br>
			  </br>
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Holder"><strong>Account Holder Name:</strong><span style="color:red">*</span></label>
                         <input id="Account_Holder" type="text" name="Account_Holder" class="form-control" placeholder="Enter Account Holder Name" required="required" maxlength="30" data-error="Account Holder is required." value = "<?php echo $Account_Holder;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Type:"><strong>Bank Account Type:</strong><span style="color:red">*</span></label>
                         <select class="form-control" id="Account_Type" name="Account_Type">
						  <?php
						  	$Account_TypeSelect = $Account_Type;
							$rowData = null;
						  foreach($Account_TypeArr as $row)
						  {
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							  if($rowData[0] == $Account_TypeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
						  }?>
                        </select>
                        <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				</div>
				<div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Number"><strong>Bank Account Number:</strong><span style="color:red">*</span></label>
                   <input id="Account_Number" type="number" name="Account_Number" class="form-control" placeholder="Enter Bank Account Number" required="required" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" data-error="Account Number is required." value = "<?php echo $Account_Number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Branch_Code"><strong>Bank Name:</strong><span style="color:red">*</span></label>
                         <!--<input id="Branch_Code" type="text" name="Branch_Code" class="form-control" placeholder="" required="required" data-error="Branch Code is required.">-->
                         <select class="form-control" id="Branch_Code" name="Branch_Code">
						 <?php
						  	$Branch_CodeSelect = $Branch_Code;
							$rowData = null;
							foreach($Branch_CodeArr as $row)
							{
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $Branch_CodeSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
             </div>
           </div>
           </br>

                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>

<div class="row setup-content" id="step-3">
	<div class="col-xs-12">
		<div class="col-md-12">
<div class="container-fluid" style="border:1px solid #ccc ">
             </br>
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_No_Payments"><strong>Number of Instalments(0 = unspecified):</strong></label>
                         <input id="No_Payments" type="number" name="No_Payments" class="form-control" placeholder=""  data-error="Number of Payments is required." pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" value="0" value = "<?php echo $No_Payments;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>

                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Amount"><strong>Amount:</strong><span style="color:red">*</span></label>
                         <input id="Amount" type="number" name="Amount" min="0" step="0.01" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" required="required" data-error="Amount is required." value = "<?php echo $Amount;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
			</div>
			<div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                       <label for="form_Frequency"><strong>Frequency:</strong><span style="color:red">*</span></label>
                       <select class="form-control" id="Frequency" name="Frequency">
  						  <?php
						  	$FrequencySelect = $Frequency;

						  foreach($FrequencyArr as $row)
						  {
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
						  }
						  ?>

                      </select>
				</div></div>
				 <div class="col-md-3">
          <!-- -->
		  <div class="form-group">

                        <label for="form_Action_Date"><strong>Action Date:</strong><span style="color:red">*</span></label>
                        <input id="Action_Date" min="<?php echo date('Y-m-d'); ?>" type="date"  name="Action_Date" class="form-control" placeholder="aaa" required="required" data-error="Action_Date is required." value = "<?php echo $Action_Date;?>"
						onChange="applyrules(this)">
                        <div class="help-block with-errors">
						<div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
							<span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
						</div>
						</div>
          </div>
		  <!-- -->
                 </div>
			</div>
			<div class="row">

				 <!-- -->
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Service_Mode"><strong>Service Mode:</strong><span style="color:red">*</span></label>
								<SELECT class="form-control" id="Service_Mode" name="Service_Mode" size="1" onChange="valueselect(this);">
											<?php
											$idService_Mode = '';
											$Service_ModeSelect = $Service_Mode;
											$service_modedesc = '';
											foreach($dataservice_mode as $row)
											{
												$idService_Mode = $row['idservice_mode'];
												$service_modedesc = $row['service_modedesc'];
												// Select the Selected
												if($service_modedesc == $Service_ModeSelect)
												{
												echo "<OPTION value=$service_modedesc selected>$service_modedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$service_modedesc>$service_modedesc</OPTION>";
												}
											}

											if(empty($dataservice_mode))
											{
												echo "<OPTION value=0>No service mode</OPTION>";
											}
											?>
								</SELECT>
				   </div>
				</div>
				<!-- -->
				<div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Service_Type"><strong>Service Type:</strong><span style="color:red">*</span></label>
						 <SELECT class="form-control" id="Service_Type" name="Service_Type" size="1" onChange="servicetypeselect(this);" >
											<?php
											$Service_Type = '';
											$Service_ModeSelect = $Service_Mode;
											$Service_TypeSelect = $Service_Type;
											$service_type_desc = '';
											foreach($dataservice_type as $row)
											{
											  // -- Service_Type of a selected Service_Mode
												$Service_Mode 	   = $row['service_mode'];
												$service_type_desc = $row['service_type_desc'];
												$Service_Type 	   = $row['idservice_type'];

												// -- Select the Selected Service_Mode
											  if($Service_Mode == $Service_ModeSelect)
											   {
													if($Service_TypeSelect == $Service_Type)
													{
													echo '<OPTION value="'.$service_type_desc.'" selected>'.$service_type_desc.'</OPTION>';
													}
													else
													{
													 echo '<OPTION value="'.$service_type_desc.'">'.$service_type_desc.'</OPTION>';
													}
											   }
											}

											if(empty($dataservice_type))
											{
												echo "<OPTION value=0>No service types</OPTION>";
											}
											?>
							</SELECT>
				</div></div></div>
    <div class="row">
				<div class="col-md-3">
                     <div class="form-group">
						 <label for="entry_class"><strong>Tracking:</strong></label>
						 <SELECT class="form-control" id="entry_class" name="entry_class" size="1" >
											<?php
											$entry_class = '';
											$Service_ModeSelect = $Service_Mode;
											$Entry_Class_Des = '';
											$count = 0;
											//print_r($dataentry_class);
											foreach($dataentry_class as $row)
											{
											  // -- Service_Type of a selected Service_Mode
												$Service_Mode 	   = $row['service_mode'];
												$Entry_Class_Des = $row['Entry_Class_Des'];
												$entry_class 	   = $row['identry_class'];
													//echo $entry_class.' == '.$entry_classSelect;
												// -- Select the Selected Service_Mode
											  if($Service_Mode == $Service_ModeSelect)
											   {
													if($entry_classSelect == $entry_class)
													{
													echo "<OPTION value=$entry_class selected>$Entry_Class_Des</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$entry_class>$Entry_Class_Des</OPTION>";
													}
											   }
											   $count = $count + 1;
											}

											if(empty($count))
											{
												echo "<OPTION value=0>No tracking</OPTION>";
											}
											?>
							</SELECT>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Bank_Reference"><strong>Bank Reference:</strong><span style="color:red">*</span></label>
                         <input id="Bank_Reference" type="text" name="Bank_Reference" class="form-control" placeholder="" required="required" maxlength="10" data-error="Bank Reference is required." value = "<?php echo $Bank_Reference;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div></div>
    <div class="row">
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="Notify:"><strong>Notify:</strong></label>
                         <select class="form-control" id="Notify" name="Notify">
  						  <?php
						  	$NotifySelect = $Notify;

						  foreach($NotifyArr as $row)
						  {
							  if($row == $NotifySelect)
							  {echo  '<option value="'.$row.'" selected>'.$row.'</option>';}
							  else
							  {echo  '<option value="'.$row.'">'.$row.'</option>';}
						  }
						  ?>
                        </select>
                        <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				  <div class="col-md-3">
                     <div class="form-group">
                         <label for="contact_number"><strong>Contact Number</strong></label>
                         <input id="contact_number" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" name="contact_number" class="form-control" value = "<?php echo $contact_number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <!--<div class="col-md-6">
				 <div class="form-group">-->

                 <!--</div>
				 </div>	-->
                 <div class="col-md-3">
                     <div class="form-group">
                         <input id="User_Code" type="hidden" name="User_Code" class="form-control" value="<?php $_SESSION['UserCode']; ?>" placeholder="" required="required" data-error="Bank Reference is required.">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
             </div>
           </div>
		</br>
        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>

<div class="row setup-content" id="step-4">
<?php include $filerootpath.'/confirm_instruction.php'; ?>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Save</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
 <div class="row setup-content" id="step-5">
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Schedule New Debit Order</strong></button>
			  </div>
			</div>
			</div>
			<br/>
			</div></div>
			 <br/>
			 <a href="<?php if(empty($param)){ echo "billpayments";}else{ echo "javascript:clickTab('futuredebitorders');";}?>" class="btn btn-primary nextBtn btn-lg pull-right" id="PayNow" type="button" ><strong>Scheduled Debit Orders</strong></a>
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary btn-lg pull-right" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>
</div>
</form>
</div>
<script>
// -- 18 September 2021 -- //
function servicetypeselect(sel)
{
			var message_info   = document.getElementById('rulealert');
			message_info.className  = '';
			message_info.innerHTML  = '';

			var Service_type = sel.options[sel.selectedIndex].value;
			if(Service_type == 'TWO DAY')
			{
				// -- Call the rules
				applyrules(sel);
			}
			// -- 13.11.2021
			if(Service_type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}
}
function applyrules_sameday(sel)
{
		var selectedService_Type = document.getElementById('Service_Type').value;
		var Action_Date_EL = document.getElementById("Action_Date");
		var Action_Date_B4 = Action_Date_EL.value;
		var message_info   = document.getElementById('rulealert');

		var list =
		{'Action_Date'			: Action_Date_EL.value,
		 'Type' 	            : selectedService_Type
		}

		if(selectedService_Type == 'SAMEDAY')
		{
		// -- alert(selectedService_Type);
		// -- jQuery for PHP file
		jQuery(document).ready(function()
		{
			jQuery.ajax({  type: "POST",
					   url:"script_rule_two_day_service.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							if(feedback.includes("Proposed"))
							{
							  var res = feedback.split("|");
							  Action_Date_EL.value = res[1];
							  message_info.className  = 'alert alert-warning';
							  message_info.innerHTML  = 'Action Date :'+Action_Date_B4+' is a weekend or holiday. system auto generated action date:'+res[1];
							}
							else
							{
								message_info.className  = '';
							    message_info.innerHTML  = '';
								Action_Date_EL.value = feedback;
							}
						 }
			});
		});
		}
}
function applyrules(sel)
{
		var selectedService_Type = document.getElementById('Service_Type').value;
		var Action_Date_EL = document.getElementById("Action_Date");
		var Action_Date_B4 = Action_Date_EL.value;
		var message_info   = document.getElementById('rulealert');

		var list =
		{'Action_Date'			: Action_Date_EL.value,
		 'Type' : selectedService_Type}

		if(selectedService_Type == 'TWO DAY')
		{
			//alert(selectedService_Type);
		// -- jQuery for PHP file
		jQuery(document).ready(function()
		{
			jQuery.ajax({  type: "POST",
					   url:"script_rule_two_day_service.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							if(feedback.includes("Proposed"))
							{
							  var res = feedback.split("|");
							  Action_Date_EL.value = res[1];
							  message_info.className  = 'alert alert-warning';
							  message_info.innerHTML  = 'Action Date :'+Action_Date_B4+' is a weekend or holiday. system auto generated action date:'+res[1];
							}
							else
							{
								message_info.className  = '';
							    message_info.innerHTML  = '';
								Action_Date_EL.value = feedback;
							}
						 }
			});
		});
		}
		//script_rule_two_day_service.php
				
				// -- 13.11.2021
			if(selectedService_Type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}
}

function valueselect(sel)
 {
		var Service_Mode = sel.options[sel.selectedIndex].value;
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);

							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});
			  return false;
  }

  $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
		allpreviousBtn = $('.previousBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

	allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;
		//alert(objToString(allpreviousBtn));

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

 });


    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-4')
			{
				var message_info = document.getElementById('message_info');
				message_info.innerHTML = '';
				message_info.className  = '';
				ret2 = check_duplicate_debit_order();
				if(ret2 === 1)
				{
					isValid = false;
				}
			}

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }


	   // -- Validate contact number
		if(validate_contactnumber())
		{
			isValid = false;
		}

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus();
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');

		add_confirm_instruction();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

function validate_contactnumber()
{
	var req	= null;
	if(document.getElementById('Notify').value === 'Yes')
	{
		document.getElementById('contact_number').required = true;

		if(document.getElementById('contact_number').value === '')
		{req = true;}else{req = false;}
	}
	else
	{
		document.getElementById('contact_number').required = false;
		req = false;
	}
}

function maxLengthNumber(el,maxlength)
{
	alert(maxlength);
	if(el.value.length == maxlength)
	{
		return false;
	}
}
function toStep1()
{
document.getElementById("step1").click();
}
function add_confirm_instruction()
{
	// -- confirmation.
	document.getElementById('InitialsLbl').innerHTML = document.getElementById('Initials').value;
	document.getElementById('Account_HolderLbl').innerHTML = document.getElementById('Account_Holder').value;
	document.getElementById('Account_NumberLbl').innerHTML = document.getElementById('Account_Number').value;
	var Account_Type = document.getElementById('Account_Type').value;
	var Account_TypeDesc = '';
	var branchcode = document.getElementById('Branch_Code').value;
	var bankname = '';
	var tracking     = document.getElementById('entry_class').value;
	var trackingdesc = '';

	Branch_CodeArr  = <?php echo json_encode($Branch_CodeArr);?>;
	Arr = <?php echo json_encode($Account_TypeArr);?>;
	dataentry_classArr = <?php echo json_encode($dataentry_classArr);?>;

	for (var key in Arr)
	{
	   row = new Array();
	   row = Arr[key].split('|');
	   if(Account_Type == row[0])
	   {
		   Account_TypeDesc = row[1];
	   }
	}

	for (var key in Branch_CodeArr)
	{
	   row = new Array();
	   row = Branch_CodeArr[key].split('|');
	   if(branchcode == row[0])
	   {
		   bankname = row[1];
	   }
	}

	for (var key in dataentry_classArr)
	{
	   row = new Array();
	   row = dataentry_classArr[key].split('|');
	   if(tracking == row[0])
	   {
		   trackingdesc = row[1];
	   }
	}

	if(trackingdesc == ''){trackingdesc = 'No tracking'; }

	document.getElementById('Account_TypeLbl').innerHTML = Account_TypeDesc;
	document.getElementById('Branch_CodeLbl').innerHTML = bankname;
	document.getElementById('Reference_1Lbl').innerHTML = document.getElementById('Reference_1').value;
	document.getElementById('Reference_2Lbl').innerHTML = document.getElementById('Reference_2').value;
	document.getElementById('Bank_ReferenceLbl').innerHTML = document.getElementById('Bank_Reference').value;
	document.getElementById('NotifyLbl').innerHTML = document.getElementById('Notify').value;
	document.getElementById('contact_numberLbl').innerHTML = document.getElementById('contact_number').value;

document.getElementById('Client_ID_TypeLbl').innerHTML = document.getElementById('Client_ID_Type').value;
document.getElementById('Client_ID_NoLbl').innerHTML = document.getElementById('Client_ID_No').value;
document.getElementById('No_PaymentsLbl').innerHTML = document.getElementById('No_Payments').value;
document.getElementById('entry_classLbl').innerHTML = trackingdesc;
document.getElementById('Service_TypeLbl').innerHTML = document.getElementById('Service_Type').value;
document.getElementById('Service_ModeLbl').innerHTML = document.getElementById('Service_Mode').value;
document.getElementById('FrequencyLbl').innerHTML = document.getElementById('Frequency').value;
document.getElementById('Action_DateLbl').innerHTML = document.getElementById('Action_Date').value;

document.getElementById('companybranchLbl').innerHTML = document.getElementById('srUserCode').value;
document.getElementById('AmountLbl').innerHTML = document.getElementById('Amount').value;

}

function check_duplicate_debit_order()
{

	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;

var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;

	IntUserCode = document.getElementById('srUserCode').value; // -- srUserCode(22/02/2022)

			var list =
{
'Initials'			: document.getElementById('Initials').value,
'Account_Holder'	: document.getElementById('Account_Holder').value,
'Account_Type'		: document.getElementById('Account_Type').value,
'Account_Number'	: document.getElementById('Account_Number').value,
'Branch_Code'		: document.getElementById('Branch_Code').value,

'Client_Reference_1': document.getElementById('Reference_1').value,
'Bank_Reference'	: document.getElementById('Bank_Reference').value,
'Notify'			: document.getElementById('Notify').value,
'Contact_No'		: document.getElementById('contact_number').value,
'Amount'		    : document.getElementById('Amount').value,
'Action_Date'		: document.getElementById('Action_Date').value,

'No_Payments'		: document.getElementById('No_Payments').value,
'Service_Mode'		: document.getElementById('Service_Mode').value,
'Service_Type'		: document.getElementById('Service_Type').value,
'Frequency'			: document.getElementById('Frequency').value,
'entry_class'		: document.getElementById('entry_class').value,

'Client_Reference_2': document.getElementById('Reference_2').value,
'Client_ID_Type'	: document.getElementById('Client_ID_Type').value,
'Client_ID_No'		: document.getElementById('Client_ID_No').value,

'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode};

				var feedback = '';
				var ret = 0;

				//--check_duplicate_debit_order.php
				jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"script_checkduplicatedebitorder.php",
									   data:{list:list},
										success: function(data)
										{
										 feedback = data;
										 if(feedback === '')
										 {feedback  = 'Are you sure you want to schedule this debit order?';}
											if (confirm(feedback))
											{
											  // Save it!
												ret = 0;
												call_script_billinstruction();
											}
											else
											{
											  // Do nothing!
											    toStep1();
											    ret = 1;
											}

						}});
				});

				//return ret;
}

function call_script_billinstruction()
{
	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;
	
	IntUserCode = document.getElementById('srUserCode').value; // -- srUserCode(22/02/2022)
//alert(IntUserCode);
			var list =
{
'Initials'			: document.getElementById('Initials').value,
'Account_Holder'	: document.getElementById('Account_Holder').value,
'Account_Type'		: document.getElementById('Account_Type').value,
'Account_Number'	: document.getElementById('Account_Number').value,
'Branch_Code'		:document.getElementById('Branch_Code').value,

'Client_Reference_1': document.getElementById('Reference_1').value,
'Bank_Reference'	: document.getElementById('Bank_Reference').value,
'Notify'			: document.getElementById('Notify').value,
'Contact_No'		: document.getElementById('contact_number').value,
'Amount'		    : document.getElementById('Amount').value,
'Action_Date'		: document.getElementById('Action_Date').value,

'No_Payments'		: document.getElementById('No_Payments').value,
'Service_Mode'		: document.getElementById('Service_Mode').value,
'Service_Type'		: document.getElementById('Service_Type').value,
'Frequency'			: document.getElementById('Frequency').value,
'entry_class'		: document.getElementById('entry_class').value,

'Client_Reference_2': document.getElementById('Reference_2').value,
'Client_ID_Type'	: document.getElementById('Client_ID_Type').value,
'Client_ID_No'		: document.getElementById('Client_ID_No').value,
'Client_Id'		    : UserCode,//IntUserCode, 23/02/2022

'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode,
'debitorder_date'   : document.getElementById('Action_Date').value};// -- 19.09.2021

					//alert(document.getElementById('Initials').value);
var feedback = '';
var ret = 0;

// -- Check Duplicate Payment to benefiaciary.
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"script_billinstruction.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

PayNow = document.getElementById("PayNow");
Exit = document.getElementById("Exit");
backtoconfirm = document.getElementById("backtoconfirm");
addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								//alert(feedback);
								message_info.innerHTML = 'Debit Order scheduled successfully.';
								message_info.className  = 'status';
								PayNow.style.visibility = '';
								Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								addbeneficiary.style.visibility = '';
								var res = feedback.split("|");
								if(typeof res[2] === 'undefined')
								{}else{message_info.innerHTML = message_info.innerHTML + '<br/>'+res[2];}
								document.getElementById('bid').value = res[1];
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								PayNow.style.visibility = 'hidden';
								Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								addbeneficiary.style.visibility = 'hidden';
							}

						 }
					});
				});

}
</script>
