<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- checksinglefeature
if(!function_exists('checksinglefeature'))
{
function checksinglefeature($pdo,$childpage,$access_id,$feature)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$sql = 'SELECT * FROM features WHERE page = ? AND features = ? AND access_id = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($childpage,$feature,$access_id));	
	$dataPageFeatures    = $q->fetch(PDO::FETCH_ASSOC);
	$dataPageFeaturesArr = null;
	
	// -- array content..
	if (empty($dataPageFeatures))
	{
		//-- Do nothing..
	}
	else
	{
	   // -- split features into array..
		$dataPageFeaturesArr['featureid'] = $dataPageFeatures['featureid'];
	}
	return 	$dataPageFeaturesArr;
}}

// -- checkdefaultpage -- 30/01/2022
if(!function_exists('checkdefaultpagebyrole'))
{
function checkdefaultpagebyrole($pdo,$role)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$default  	  = 'X';
	
	$sql = 'SELECT * FROM access WHERE role = ? AND defaultpage = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($role,$default));	
	$datadefaultpage    = $q->fetch(PDO::FETCH_ASSOC);
	$datadefaultpageArr = null;
	
	//echo $sql.' - '.$role.' - '.$default;
	// -- array content..
	if (!isset($datadefaultpage['defaultpage']))
	{
		//-- Do nothing..
		//echo $sql.' - '.$role.' - modise '.$default;
	}
	else
	{
	   // -- split features into array..
		$datadefaultpageArr['defaultpage'] = $datadefaultpage['defaultpage'];
	}
	return 	$datadefaultpageArr;
}} 
// -- getfeatures
if(!function_exists('getfeatures'))
{
function getfeatures($pdo,$childpage)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$sql = 'SELECT * FROM page WHERE page = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($childpage));	
	$dataPageFeatures    = $q->fetch(PDO::FETCH_ASSOC);
	$dataPageFeaturesArr = null;
	
	// -- array content..
	if (empty($dataPageFeatures))
	{
		//-- Do nothing..
	}
	else
	{
	   // -- split features into array..
		$dataPageFeaturesArr = explode(",", $dataPageFeatures['features']);
		/*$featuresText=$featuresText."<td>";
		foreach($dataPageFeaturesArr as $row)
		{
			$featuresText = $featuresText."<p><b>{$row}</b><input style='height:30px' type='checkbox' id='{$row}' name='{$row}' /></p>";
				//$html = $html."<p><b>{$row}</b></p>";
		}
			$featuresText=$featuresText."</td>";*/
	}
	return 	$dataPageFeaturesArr;
}}	
// -- getfeaturesText
if(!function_exists('getfeaturesText'))
{
function getfeaturesText($pdo,$childpage,$accessid)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$sql = 'SELECT * FROM page WHERE page = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($childpage));	
	$dataPageFeatures = $q->fetch(PDO::FETCH_ASSOC);
	
	$checked = '';
	// -- 
	if (empty($dataPageFeatures['features']))
	{
		$featuresText = "<td>No features</td>";
	}
	else
	{
	// -- split features into array..
		$dataPageFeaturesArr = explode(",", $dataPageFeatures['features']);
		$featuresText=$featuresText."<td>";
		foreach($dataPageFeaturesArr as $row)
		{
			// -- Select Access Page features
			$sql = 'SELECT * FROM features WHERE page = ? AND  access_id = ? AND features = ?';
			$q = $pdo->prepare($sql);
			$q->execute(array($childpage,$accessid,$row));	
			$dataAccessPageFeatures = $q->fetch(PDO::FETCH_ASSOC);
			if(!empty($dataAccessPageFeatures))
			{
				if(!empty($dataAccessPageFeatures['active']))
				{
					$checked = 'checked';
				}
			}
			$featuresText = $featuresText."<p><b>{$row}</b><input style='height:30px' type='checkbox' id='{$row}' name='{$row}' onClick='valuechecked(this);' value = 'X' {$checked} /></p>";
				//$html = $html."<p><b>{$row}</b></p>";
				$checked = '';
		}
			$featuresText=$featuresText."</td>";
	}
	return 	$featuresText;
}}	
  
$tbl_user="access"; // Table name 

//1. ---------------------  Role ------------------- //
  $sql = 'SELECT * FROM role ORDER BY role';
  $dataRoles = $pdo->query($sql);						   						 

//2. ---------------------  Parent Pages ------------------- //
  $parentMark = 'X';
  $sql =  "select * from access where parent = 'X'";
  $dataParentPages = $pdo->query($sql);	
  
//3. ---------------------  Child Pages ------------------- //
  $sql = 'SELECT * FROM page ORDER BY page';
  $dataChildPage = $pdo->query($sql);	  

  if (isset($_GET['accessid']) ) 
  { 
	$accessid = $_GET['accessid']; 
  }
 
//4. ---------------------  Menu Order Sequence ------------------- //
  $sql = 'SELECT * FROM ordersequence ORDER BY ordersequence';
  $dataordersequence = $pdo->query($sql);	  

  if (isset($_GET['accessid']) ) 
  { 
	$accessid = $_GET['accessid']; 
  }
 
 
$count = 0;

$role		=	'';
$parent		=	'';
$parentpage	=	'';
$child    	=	'';
$childpage  =	'';
$label		=	'';

// -- CRUD ACCESS
$create  =	'';
$update  =	'';
$default  =	''; // -- 30/01/2022
$delete  =	'';
$ordersequence  = 0;
$parentsequence = 0;

// -- Errors
$roleError			=	'';
$parentError		=	'';
$parentpageError	=	'';
$childError    	 =	'';
$childpageError  =	'';
$labelError		 =	'';
$defaultError    = '';

// -- Page content
$featuresText = '';
$temppage     = '';
$dataPageFeaturesArr = null;

if (!empty($_POST)) 
{   $count = 0;
	$labelError = '';
	$role		=	$_POST['role'];
	$parentpage	=	$_POST['parentpage'];
	$childpage  =	$_POST['childpage'];
	$label		=	$_POST['label'];
	$valid = true;
	$ordersequence  =	$_POST['ordersequence'];
	$parentsequence = $_POST['parentsequence'];

	// -- featuresText 24/01/2022
	$featuresText = $_POST['featuresText'];
	
	//-- BOC Features 26/01/2022
$dataPageFeaturesArr = getfeatures($pdo,$childpage);
$length 			 = count($dataPageFeaturesArr);
$temppage = $childpage;


if(!empty($dataPageFeaturesArr)){
foreach($dataPageFeaturesArr as $row)
{
	Global ${$row};
	//echo "modise : ".$row;
	if(isset($_POST["{$row}"]))
		{
			${$row} = $_POST["{$row}"];
			${$row} = 'X';
			//echo "modise : ".${$row};
		}
}}
	//-- EOC Features 26/01/2022
	
	if(empty($parentsequence))
	{
		$parentsequence = 0;
	}
	
	if(isset($_POST['parent']))
	{
		$parent = 'X';
		$parentpage = '';
	}
	else
	{
		$parent = '';
	}
	
	if(isset($_POST['child']))
	{
		$child = 'X';
		$parentpage	=	$_POST['parentpage'];
	}
	
	
	// CRUD ACCESS
	if(isset($_POST['create']))
	{
		$create = 'X';
	}
	else
	{
		$create = '';
	}
	
	if(isset($_POST['update']))
	{
		$update = 'X';
	}
	else
	{
		$update = '';
	}
	
	// -- 30/01/2022
	if(isset($_POST['default']))
	{
		$default = 'X';
	}
	else
	{
		$default = '';
	}
	// -- 30/01/2022
	
	if(isset($_POST['delete']))
	{
		$delete = 'X';
	}
	else
	{
		$delete = '';
	}
	
	// -- Access Must be Indicate atleast parent or child.
	if(!isset($_POST['parent']) && !isset($_POST['child']))
	{
		$labelError = 'Please check either as parent or a child.<br/>'; $valid = false;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if (empty($label)) { $labelError = $labelError.'Please enter menu label'; $valid = false;}


	$objectArr = checkdefaultpagebyrole($pdo,$role);
	if(isset($objectArr['defaultpage'])) // -- New defaultpage...
	{
		if(!empty($default))
		{
		 $defaultError = 'Default page is already assign to a role.<br/>'; $valid = false;
		}else
		{
			$defaultError = '';
		}
	}else{$defaultError = '';}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								ACCESS  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE access SET role = ?,parent = ?,parentpage = ?, child = ?, childpage= ?,label= ?,cancreate = ?,canupdate = ?,candelete = ?,ordersequence = ?,parentsequence = ?,defaultpage = ? WHERE accessid = ?"; 		
			$q = $pdo->prepare($sql);
			//echo $sql;
			$q->execute(array($role,$parent,$parentpage,$child,$childpage,$label,$create,$update,$delete,$ordersequence,$parentsequence,$default,$accessid));
			Database::disconnect();
			$count = $count + 1;
			
			// --  BOC Insert and Update Features - 26/01/2022...
			if(!empty($dataPageFeaturesArr)){
			foreach($dataPageFeaturesArr as $row)
			{
				Global ${$row};
				$objectArr = checksinglefeature($pdo,$childpage,$accessid,$row);
				if(isset($objectArr['featureid'])) // -- New Features we update
				{
					$sql = "Update features SET active = ? WHERE featureid = ?";
				}			
				else // -- New Features we insert
				{
					$sql = "INSERT INTO features(page, features, active,access_id) VALUES (?,?,?,?)";
				}
	
				$q = $pdo->prepare($sql);
				
				if(isset($_POST["{$row}"]))// -- is checked
					{
										//echo "{$row}"."<br/>";

						${$row} = $_POST["{$row}"];
						${$row} = 'X';
						//echo $sql;
							if(isset($objectArr['featureid'])) // -- Old Features we update
							{
								$q->execute(array('X',$objectArr['featureid']));
							}// -- New Features we insert
							else{$q->execute(array($childpage,$row,'X',$accessid));}
						//echo "modise inserted: ".${$row};
					}
					else// -- Not checked
					{
						//echo "{$row}"."<br/>".$objectArr['featureid'];
						if(isset($objectArr['featureid'])) // -- Old Features we update
						{$q->execute(array('',$objectArr['featureid']));}
					   else{$q->execute(array($childpage,$row,'',$accessid));}						
					}
					Database::disconnect();
			}}
			// --  EOC Insert and Update Features - 26/01/2022...
			
			// -- 23/01/2022
		// -- Re-Read features after Insert and Update..
		$featuresText = getfeaturesText($pdo,$childpage,$accessid );
	}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from access where accessid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($accessid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		$role = $data['role'];
		$parent = $data['parent'];
		$parentpage = $data['parentpage'];
		$child = $data['child'];
		$childpage = $data['childpage'];
		$label = $data['label'];
		$create = $data['cancreate'];
		$update = $data['canupdate'];
		$delete = $data['candelete'];
		$ordersequence = $data['ordersequence'];
		$parentsequence = $data['parentsequence'];
		$accessid = $data['accessid'];
		
		// -- 23/01/2022
		// -- Read features
		$featuresText = getfeaturesText($pdo,$childpage,$accessid );
		// -- 30/01/2022
		// -- Default page
		$default = $data['defaultpage'];		
}

?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Update Access Control Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Access Control successfully Updated.</p>");
		}
?>
				
</div>				
			<p><b>Parent</b><br />
			<input style="height:30px" type='checkbox' name='parent' value="<?php echo !empty($parent)?$parent:'';?>" 
						<?php if(!empty($parent)){echo'checked';}?> onclick="parentmenu()"/>
			</p>
			
			<p><b>Order Sequence</b><br />
				<SELECT class="form-control" id="ordersequence" name="ordersequence" size="1">
					<?php
						$ordersequenceLoc = '';
						$ordersequenceSelect = $ordersequence;
						foreach($dataordersequence as $row)
						{
							$ordersequenceLoc = $row['ordersequence'];

							if($ordersequenceLoc == $ordersequenceSelect)
							{
								echo "<OPTION value='".$ordersequenceLoc."' selected>$ordersequenceLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$ordersequenceLoc."'>$ordersequenceLoc</OPTION>";
							}
						}
												
						if(empty($dataordersequence))
						{
							echo "<OPTION value=0>No Order Sequence</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			
				<div class="controls" id="parentmenu" name="parentmenu" <?php if(empty($parent)){echo "style='display:block'";}else{echo "style='display:none'";}?>>
							<p><b>Parent Menu Item</b><br />

					<SELECT class="form-control" id="parentpage" name="parentpage" size="1">
					<?php
						$parentpageLoc = '';
						$parentpageSelect = $parentpage;
						foreach($dataParentPages as $row)
						{
							$parentpageLoc = $row['label'];
							if($parentpageLoc == $parentpageSelect)
							{
								echo "<OPTION value='".$parentpageLoc."' selected>$parentpageLoc</OPTION>";
								$parentsequence = $row['ordersequence'];
							}
							else
							{
								echo "<OPTION value='".$parentpageLoc."'>$parentpageLoc</OPTION>";
							}
						}
												
						if(empty($dataChildPage))
						{
							echo "<OPTION value=0>No Menu</OPTION>";
						}
						

					?>
					
					</SELECT>

								</p>			 

				</div>
			
			<p><b>Parent Sequence</b><br />
			<input style="height:30px" type='text' name='parentsequence' id='parentsequence' 
			value="<?php echo !empty($parentsequence )?$parentsequence :'';?>" readonly />			
			</p>
			
			<p><b>Child</b><br />
			<input style="height:30px" type='checkbox' name='child' value="<?php echo !empty($child )?$child :'';?>"
			<?php if(!empty($child)){echo'checked';}?> />			
			</p>

			<p><b>Page</b><br />
				<div class="controls">
					<SELECT class="form-control" id="childpage" name="childpage" size="1" onChange="valueselect(this);">
					<!-------------------- BOC 2017.05.27 - Charge Details --->	
					<?php
						$childpageLoc = '';
						$childpageSelect = $childpage;
						foreach($dataChildPage as $row)
						{
							$childpageLoc = $row['page'];

							if($childpageLoc == $childpageSelect)
							{
								echo "<OPTION value=$childpageLoc selected>$childpageLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$childpageLoc>$childpageLoc</OPTION>";
							}
						}
												
						if(empty($dataChildPage))
						{
							echo "<OPTION value=0>No Pages</OPTION>";
						}
					?>
					<!-------------------- EOC 2017.05.27 - Charge Details  --->												
					</SELECT>
				</div>
			</p>			
			
			<p><b>Role</b><br />
				<div class="controls">
					<SELECT class="form-control" id="role" name="role" size="1">
					<?php
						$roleLoc = '';
						$roleSelect = $role;
						foreach($dataRoles as $row)
						{
							$roleLoc = $row['role'];

							if($roleLoc == $roleSelect)
							{
								echo "<OPTION value=$roleLoc selected>$roleLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$roleLoc>$roleLoc</OPTION>";
							}
						}
												
						if(empty($dataChildPage))
						{
							echo "<OPTION value=0>No Roles</OPTION>";
						}
					?>
					</SELECT>
				</div>
			</p>			
			
			<p><b>Menu Label</b><br />
			<input style="height:30px" type='text' name='label' value="<?php echo !empty($label )?$label :'';?>"/>
			<?php if (!empty($labelError)): ?>
			<span class="help-inline"><?php echo $labelError;?></span>
			<?php endif; ?>
			</p>

			<!-- <td><p><b>create</b>
			<input style="height:30px" type='checkbox' name='create' value="<?php echo !empty($create)?$create:'';?>" 
									<?php if(!empty($create)){echo'checked';}?> />
			</p>

			<p><b>update</b>
			<input style="height:30px" type='checkbox' name='update' value="<?php echo !empty($update)?$update:'';?>" 
						<?php if(!empty($update)){echo'checked';}?> />
			</p>

			<p><b>delete</b>
			<input style="height:30px" type='checkbox' name='delete' value="<?php echo !empty($delete)?$delete:'';?>" 
						<?php if(!empty($delete)){echo'checked';}?> />
			</p>
			</td>	-->		
			<td><p><b>Default Page</b>
						<input style="height:30px" type='checkbox' name='default' value="<?php echo !empty($default)?$default:'';?>" 
						<?php if(!empty($default)){echo'checked';}?> />
			<?php if (!empty($defaultError)): ?>
			<span class="help-inline" style="color:red"><?php echo $defaultError;?></span>
			<?php endif; ?>

			</p>

			</td>	
			<td>
			<h2 class="panel-title"><b>Features</b></h2>
				<div id="features" name="features"><?php echo $featuresText; ?></div>
				<input style="display:none" id="featuresText" name="featuresText" value="<?php echo $featuresText; ?>" />
			</td>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'access';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
<script>
function parentmenu() 
{
	var ele = document.getElementById("parentmenu");
	var text = document.getElementById("parentmenu");
	if(ele.style.display == "block")
	{
    		ele.style.display = "none";
  	}
	else 
	{
		ele.style.display = "block";
	}
} 
// -- 23.01.2022 - Access ---
 function valuechecked(sel) 
 {
	// alert(sel.id+' - '+sel.checked);
	 		//var check = sel.options[sel.selectedIndex].value;  
			if(sel.checked)
			{
				document.getElementById(sel.id).value = 'X';		
			}
			else
			{
				document.getElementById(sel.id).value = '';
				document.getElementById(sel.id).checked = false;
			}	
 }
 function valueselect(sel) 
 {
		var page = sel.options[sel.selectedIndex].value;      
		//alert(page);
		var access_id = <?php echo $accessid; ?>;
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectPageFeatures.php",
				       data:{page:page,access_id:access_id},
						success: function(data)
						 {	
							//alert(data);
							//jQuery("#features").html(data);
							document.getElementById("features").innerHTML = data;
							document.getElementById("featuresText").value = data;
							//alert(data);
						 }
					});
				});				  
			  return false;
  } 	
</script>
