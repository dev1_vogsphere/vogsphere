<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="loantype"; // Table name 
 $loantypedesc = '';
 $fixedamount = 0.00;
 
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search for Loan Type</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='loantypedesc' name='loantypedesc' placeholder='Loan Type' class='form-control' type='text' value=$loantypedesc>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $loantypedesc = $_POST['loantypedesc'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($loantypedesc))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE loantypedesc LIKE ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($loantypedesc));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<a class='btn btn-info' href=newloantype.php>Add New Loan Type</a><p></p>"; 

						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>"; 
						echo "<tr>"; 
						echo "<td><b>Loan Type ID</b></td>"; 
						echo "<td><b>Loan Type Description</b></td>"; 
						echo "<td><b>Currency</b></td>"; 
						echo "<td><b>Percentage</b></td>"; 
						echo "<td><b>Duration From</b></td>"; 						
						echo "<td><b>Duration To</b></td>"; 
						
						echo "<td><b>Loan :From Amount</b></td>"; 
						echo "<td><b>Loan :To Amount</b></td>";
						
						echo "<td><b>Qualify :Amount From</b></td>"; 
						echo "<td><b>Qualify :Amount To</b></td>";
						
						echo "<td><b>Loan Run : From Date</b></td>"; 
						echo "<td><b>Loan Run : Date</b></td>"; 
						echo "<td><b>Fixed Amount</b></td>"; 

						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
				
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['loantypeid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['loantypedesc']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['currency']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['percentage']) . "%</td>";  
						echo "<td valign='top'>" . nl2br( $row['fromduration']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['toduration']) . "</td>"; 

						echo "<td valign='top'>" . nl2br( $row['fromamount']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['toamount']) . "</td>"; 

						echo "<td valign='top'>" . nl2br( $row['qualifyfrom']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['qualifyto']) . "</td>"; 
						
						echo "<td valign='top'>" . nl2br( $row['fromdate']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['todate']) . "</td>"; 
						
						echo "<td valign='top'>" . nl2br( $row['fixedamount']) . "</td>"; 
						
						echo "<td valign='top'><a class='btn btn-success' href=editloantype.php?loantypeid={$row['loantypeid']}>Edit</a></td><td>
						<a class='btn btn-danger' href=deleteloantype.php?loantypeid={$row['loantypeid']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div></div>
   		<!---------------------------------------------- End Data ----------------------------------------------->