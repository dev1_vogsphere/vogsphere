<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="smstemplate"; // Table name 
$templatetype = 'sms';
//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM placeholder';
$dataPlaceholders 	= $pdo->query($sql);						   						 
$count 				= 0;
$placeholder		= '';
$smstemplatename	= '';
$smstemplatecontent	= '';
$userid 			= '';

// -- Read SESSION tenantid.
 if(isset($_SESSION['userid']))
 {
	$tenantid = $_SESSION['userid'];
	
 }
 else
 {
	 if(isset($_SESSION['username']))
	 {
		 $tenantid = $_SESSION['username'];
	 }
 }

// -- Errors
$placeholderError		 =	'';
$smstemplatenameError	 =	'';
$smstemplatecontentError =	'';
$valid = true;

if (!empty($_POST)) 
{   $count = 0;

$placeholder		=	$_POST['placeholder'];
$smstemplatename	=	$_POST['smstemplatename'];
$smstemplatecontent	=	$_POST['smstemplatecontent'];

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
if (empty($smstemplatename)) { $smstemplatenameError = $smstemplatenameError.'Please enter sms template name'; $valid = false;}

if (empty($smstemplatecontent)) { $smstemplatecontentError = $smstemplatecontentError.'Please enter sms template content'; $valid = false;}

$valid = CheckSmsTemplateExist($userid,$smstemplatename);	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
			AddSMSTemplate($smstemplatename,$smstemplatecontent,$userid);
			Database::disconnect();
			$count = $count + 1;
	}
}


// -- BOC - Check if Check Sms Template Exist.
function CheckSmsTemplateExist($tenantid,$smstemplatename)
{
	Global $smstemplatenameError;	
	$lc_valid 	  = true;	
	$tbl 	      = 'smstemplate';
	
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
	// ---------------- Check Userid --------------- //
	$query = "SELECT * FROM $tbl WHERE tenantid = ? AND smstemplatename = ?";
	$q = $pdo->prepare($query);
	$q->execute(array($tenantid,$smstemplatename));
	if ($q->rowCount() >= 1)
	{$smstemplatenameError = 'SMS Template Name already exists.'; $lc_valid = false;}
	return $lc_valid;	
}
// -- Add SMS Template.
function AddSMSTemplate($smstemplatename,$smstemplatecontent,$userid)
{
	Global $templatetype;
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO smstemplate(smstemplatename, smstemplatecontent,tenantid,templatetype) VALUES (?,?,?,?)";
	$q = $pdo->prepare($sql);
	$q->execute(array($smstemplatename,$smstemplatecontent,$userid,$templatetype));
}
// -- EOC - 
?>
<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="smstemplates">SMS Template</a></li>
		</ol>
	</div>
</div> -->
<div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
	  <div class="row">
		<form name='form1' action='' method='POST' class="signup-page">   
<!-- <div class="panel-heading"> -->
<!-- <h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New smstemplate  Details
                    </a>
                </h2> -->
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>smstemplate  successfully added.</p>");
		}
?>
				
<!-- </div>-->	
		<div class="form-group <?php if (!empty($smstemplatenameError)){ echo "has-error";}?>">						
			<p><b>smstemplatename</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='smstemplatename' id="smstemplatename" value="<?php echo !empty($smstemplatename)?$smstemplatename :'';?>"/>
			<?php if (!empty($smstemplatenameError)): ?>
			<small class="help-block"><?php echo $smstemplatenameError;?></small>
			<?php endif; ?>
			</p>
		</div>			
			<p><b>Placeholders</b><br />
				<SELECT class="form-control" id="placeholder" name="placeholder" size="1">
					<?php
						$placeholderLoc = '';
						$placeholderSelect = $placeholder;
						foreach($dataPlaceholders as $row)
						{
							$placeholderLoc = $row['placeholder'];

							if($placeholderSelect == $placeholderSelect)
							{
								echo "<OPTION value='".$placeholderLoc."' selected>$placeholderLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$placeholderLoc."'>$placeholderLoc</OPTION>";
							}
						}
												
						if(empty($dataPlaceholders))
						{
							echo "<OPTION value=0>No placeholders</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			<div class="form-group <?php if (!empty($smstemplatecontentError)){ echo "has-error";}?>">						
				<p>
					Click to add <input onclick='input()' type='button' value='Place holder' id='button'><br>
					<textarea class="form-control" rows="10" name='smstemplatecontent' id="smstemplatecontent"><?php if (!empty($smstemplatecontent)){ echo $smstemplatecontent; }?></textarea>
					<?php if (!empty($smstemplatecontentError)): ?>
							<small class="help-block"><?php echo $smstemplatecontentError;?></small>
					<?php endif; ?>
				</p>
			</div>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'smstemplates';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			//echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				//<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>
  <button class='btn btn-primary' type='submit'>Save</button><!-- Generate OTP -->
  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->
			
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>		
</div>		
<script>
function input()
{
	var title = document.getElementById('placeholder').value;
	var area = document.forms.form1.smstemplatecontent.value;
    document.forms.form1.smstemplatecontent.value = area + title;
}    
</script>