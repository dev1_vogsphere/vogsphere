   	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

   	<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<?php

// -- Personal Details
$Title 		= '';
$FirstName  = '';
$LastName 	= '';
$refnumber  = '';
$customerid = '';
$customeridError = '';

// -- Contact Details.
$phone 		= '';

$phoneError			='';
$FirstNameError 	= '';
$LastNameError 		= '';
$tbl_lead 		= 'lead';
$valid = true;
$email = '';
$message = '';
$idnumberError = '';
$emailError = '';
$phoneError = '';

$comments = '';

$count = 0;

/*
company NAME
reg nr
contct name 
phone
EMAIL
*/
?>
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<style>

html {
  font-size: 16px;
}

/*h3 {
  font-family: 'Lato', sans-serif;
  font-size: 2.125rem;
  font-weight: 700;
  color: #444;
  letter-spacing: 1px;
  text-transform: uppercase;
  margin: 55px 0 35px;
}
*/

.carousel-inner { margin: auto; width: 90%; }
.carousel-control 			 { width:  4%; }
.carousel-control.left,
.carousel-control.right {
  background-image:none;
}

.glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right {
  margin-top:-10px;
  margin-left: -10px;
  color: #444;
}

.carousel-inner {
  /*a {
    display:table-cell;
    height: 180px;
    width: 200px;
    vertical-align: middle;
  }*/
  img {
    max-height: 150px;
    margin: auto auto;
    max-width: 100%;
  }
}

@media (max-width: 767px) {
  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(100%, 0, 0);
      transform: translate3d(100%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-100%, 0, 0);
      transform: translate3d(-100%, 0, 0);
  }

}
@media (min-width: 767px) and (max-width: 992px ) {
  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(50%, 0, 0);
      transform: translate3d(50%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-50%, 0, 0);
      transform: translate3d(-50%, 0, 0);
  }
}
@media (min-width: 992px ) {

  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(16.7%, 0, 0);
      transform: translate3d(16.7%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-16.7%, 0, 0);
      transform: translate3d(-16.7%, 0, 0);
  }

}
</style>

<script type="text/javascript">
</script>
		  <div id="slideshow" >
                <div class="container no-padding background-white bottom-border">
                    <div class="row">
                        <!-- Carousel Slideshow -->
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <!-- Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example" data-slide-to="1"></li>
                                <li data-target="#carousel-example" data-slide-to="2"></li>
                            </ol>
                            <div class="clearfix"></div>
                            <!-- End Carousel Indicators -->
                            <!-- Carousel Images -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="assets/img/slideshow/slide7.jpg">
                                </div>
                                <div class="item">
                                    <img src="assets/img/slideshow/slide7.jpg">
                                </div>
                                <!--<div class="item">
                                    <img src="assets/img/slideshow/slide3.jpg">
                                </div>
                                 <div class="item">
                                    <img src="assets/img/slideshow/slide4.jpg">
                                </div> -->
                            </div>
                            <!-- End Carousel Images -->
                            <!-- Carousel Controls -->
                            <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                            <!-- End Carousel Controls -->
                        </div>
                        <!-- End Carousel Slideshow -->
                    </div>
                </div>
            </div>
                <div class="container background-grey bottom-border">
                    <div class="row padding-vert-60">
                        <!-- Icons -->
                        <div class="col-md-4 text-center">

                          <i class="fa-money fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">Loan</h2>
                            <p class="animate fadeIn">Get a loan up to R 5000.00.</p>
							                <p>
                                <a href="applicationForm" target="_blank "><font color="#FF8C00">Apply Now</font></a>
                             </p>

						</div>
                        <div class="col-md-4 text-center">
                            <i class="fa fa-thumbs-o-up fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">DebiCheck</h2>
                            <p class="animate fadeIn">DebiCheck Collections are electronically authenticated by customers on a once-off basis. When a customer decides to take out a new contract or buy goods on credit, the customer’s mandate will be registered with their bank. Once the customer has approved the mandate, the debit order may be collected from the customer’s account without being disputed.</p>
                            <p>
                           <button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Month trial</font></button>
                           </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <i class="fa fa-credit-card fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">NAEDO</h2>
                            <p class="animate fadeIn">Non-Authenticated Early Debit Order (NAEDO) aims to level the electronic debit order collection playing field by creating an equal opportunity for creditors to collect funds from their debtors</p>
                            <p>
                           <button id="NAEDO" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Month trial</font></button>
                           </p>

                        </div>
                        <!-- End Icons -->
                    </div>
					                            <hr class="tall">
					             <div class="row padding-vert-5">
                        <!-- Icons -->

                        <!--  <div>Icons made by <a href="https://www.flaticon.com/authors/pause08"
                          title="Pause08">Pause08</a> from <a href="https://www.flaticon.com/"
                          title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
                          title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
                        </div>-->

                        <div class="col-md-4 text-center">
                            <i class="fa-cogs fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">Debit Order</h2>
                            <p class="animate fadeIn">Simple and efficient ways to collect recurring payments from your customers.</p>
                            <p>
                           <button id="debitorder" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Month trial</font></button>
                           </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <i class="fa-cloud-download fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">Cash Pay</h2>
                            <p class="animate fadeIn">A reliable and hassle-free way to pay employees. Staff get paid on time,every time.</p>
                            <p>
                           <button id="CashPay" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Month trial</font></button>
                           </p>
                      </div>
                        <div class="col-md-4 text-center">
                            <i class="fa-bar-chart fa-4x color-primary animate fadeIn"></i>
                            <h2 class="padding-top-10 animate fadeIn">Account Verification</h2>
                            <p class="animate fadeIn">Simple and efficient way to verify your customers bank account</p>
                            <p>
                           <button id="accountverification" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Month trial</font></button>
						   </p>

                        </div>


                        <!-- End Icons -->
                    </div>
                  </br>
                  </br>
                  </br>
</div>
		<div class="container background-grey bottom-border">
				<div class="row padding-vert-60">
				<div class="col-md-12 text-center"><h10>Integration Ecosystem</h10></div>
      </br>
      </br>
      </br>
<div class="col-md-14 col-md-offset-1">
<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
  <div class="carousel-inner">
    <div class="item active">
      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="assets/img/slideshow/NCR.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/Mercantile.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/TransUnion.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-1 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/cm.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/Vogsphere.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/whatsapp.jpg" class="img-responsive"></a></div>
    </div>
  </div>

</div>
</div>

				</div>
		</div>
		
			<!-- BOC - PopUp for Trial -->
					 <div class="popup" data-popup="popup-1" style="overflow-y:auto;z-index: 2;">
						<div class="popup-inner">
										<div id="divContent" style="top-margin:100px;font-size: 10px;">	
<!-- BOC Form to enable capuring of Data -->
<div class='container background-white bottom-border'>		
<div class='row margin-vert-30'>
	<div class="col-xs-12 col-sm-12">
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
				<?php # error messages
					if (isset($message)) 
					{
						//foreach ($message as $msg) 
						{
							//printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Lead $message was successfully created.</p>");
					}
			   ?>
				<h4 class="page-header">Company Information Details</h4>
				<div id="companyError" class="form-group">
						<label class="col-sm-2 control-label">Registration ID</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="customerid" name="customerid" type="text"  placeholder="registration number" value="<?php echo !empty($customerid)?$customerid:'';?>">
							<small id="customeridErrorText" class="help-block" style=""></small>																			
						</div>
				</div>
				<br/>	
				<div id="FirstNameError" class="form-group">
						<label class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="FirstName" name="FirstName" type="text"  placeholder="Company name" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
							<small id="FirstNameErrorText" class="help-block" style=""></small>											
						</div>
				</div>
			 <h4 class="page-header">Contact Details</h4>
				<div id="phoneError" class="form-group">
						<label class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" id="phone" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
								<small id="phoneErrorText" class="help-block" style=""></small>											
							</div>
				</div>
				<br/>
				<div id="emailError" class="form-group">
					<label class="col-sm-2 control-label">Email</label>
					 <div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
							<small id="emailErrorText" class="help-block" style=""></small>											
					 </div>
				</div>		
				<!--<h4 class="page-header"></h4>					
	
					 <div class="clearfix"></div> -->
					 
					<div class="form-group">
						<div class="col-sm-2">
							<button type="button" class="btn btn-primary btn-label-left" onclick="validate_save()">
							<span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						</div>
					</div>					
				</form>
			<!-- </div> 298 -->
		</div>
	</div>
</div>
<!-- EOC Capuring data... -->										
										</div>							
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>	
					 </div>
					</div>			
			<!-- EOC - PopUp for Trial -->
		

                </div>
            </div>
		<input class="form-control margin-bottom-10" id="service" name="service" type="hidden"  placeholder="service">
<script>

/*function content(service)
{
		document.getElementById("service").value = service.id;
}
*/
function content(service)
{
	//alert(service.id);
	document.getElementById("service").value = service.id;
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");   
		  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';*/

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}	

function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}


 // -- Enter to capture comments   ---
 function validate_save() 
 {	 
	 	 customerid  = document.getElementById('customerid').value;
	     FirstName	  = document.getElementById('FirstName').value;
	     phone		  = document.getElementById('phone').value;
	     email		  = document.getElementById('email').value;	
		 service = document.getElementById('service').value;	
		 valid = true;
		 
		if(customerid != '' && FirstName != '' && phone != '' && email != '' )
		{
			
			if(phonenumber(phone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number'; 				
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = ''; 								
			}

    //alert("You have entered an invalid email address!")
			if(ValidateEmail(email) != true) 
			{
				document.getElementById("emailError").setAttribute("class", "form-group has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email'; 
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group");
			 document.getElementById('emailErrorText').innerHTML = ''; 
			}

			if(customerid != '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = ''; 									
			}

		    // -- Company name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = ''; 								
			}

if(valid)
{		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"addlead.php",
				       data:{customerid:customerid,FirstName:FirstName,phone:phone,email:email,service:service},
						success: function(data)
						 {	
							jQuery("#divContent").html(data);
						 }
					});
				});
}
				//data1 = "<p class='status'> Information was successfully submited for approval.</p>";
				//document.getElementById("divContent").innerHTML = data1;

		}	
		else
		{
			// -- CustomerId
			if(customerid == '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group has-error");
				document.getElementById('customeridErrorText').innerHTML = 'Please enter company registration'; 									
			}
			else
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = ''; 									
		    }
		
		    // -- Company name
			if(FirstName == '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('FirstNameErrorText').innerHTML = 'Please enter company name'; 								
			}
			else
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = ''; 				
			}
		
			// -- phone
			if(phone == '')
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error");
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter phone'; 
			}
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = ''; 
			}
			
			// -- email
			if(email == '')
			{	document.getElementById("emailError").setAttribute("class", "form-group has-error");
			 	document.getElementById('emailErrorText').innerHTML = 'Please enter email'; 
			}
			else
			{
				document.getElementById("emailError").setAttribute("class", "form-group");
				document.getElementById('emailErrorText').innerHTML = ''; 
			}
		}	
			  return false;
  }

function phonenumber(inputtxt)
{   
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;	   
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail) 
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

</script>		