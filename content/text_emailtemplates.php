<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="sendsms">Email Template </a></li>
		</ol>
	</div>
</div> 
-->
<?php
// -- Clear jQuery Editor.
if(isset($_SESSION['Editor']))
{
	$_SESSION['Editor'] = '';
}
// -- EOC Clear jQuery.
///////////////////////////////////////////////////////////////////////////////////////////
//									SEARCH BOX											 //
///////////////////////////////////////////////////////////////////////////////////////////
 $smstemplate   = '';
 $templatetype  = 'Email';
 $tenantid 		= '';

// -- Read SESSION tenantid.
 if(isset($_SESSION['userid']))
 {
	$tenantid = $_SESSION['userid'];
	
 }
 else
 {
	 if(isset($_SESSION['username']))
	 {
		 $tenantid = $_SESSION['username'];
	 }
 }

 if ( !empty($_POST))
 {
		$smstemplate = $_POST['smstemplate'];
 } 
 include 'database.php';
 $pdo = Database::connect();
 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);												   
 $sql = "";
 $q = "";								   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		smstemplate : admin     								   //
/////////////////////////////////////////////////////////////////////////////////////////////	
$data = GetTenantTemplates($tenantid,$smstemplate,$templatetype);					 
?>
<!-- //////////////////////////////////////////////////////////////////////////////////////////////
//					 			END SEARCH BOX 											    //
//////////////////////////////////////////////////////////////////////////////////////////////	-->				  
<div class='container background-white bottom-border'>		
   <!-- Search Login Box -->
<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search emailtemplate by name</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='smstemplate' name='smstemplate' placeholder='email template' class='form-control' type='text' value=<?php echo $smstemplate; ?>>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
							</div>
<?php
// -- Data Model Functions.
	function GetTenantTemplates($tenantid,$smstemplate,$templatetype)
	{
			Global $pdo;
			$tbl_name="smstemplate"; // Table name 
			
			if(!empty($smstemplate))
			{
				$sql = "SELECT * FROM $tbl_name WHERE smstemplatename = ? and tenantid = ? and templatetype = ?";
				$q = $pdo->prepare($sql);
				$q->execute(array($smstemplate,$tenantid,$templatetype));	
				$data = $q->fetchAll();  
			}
			else
			{
					// -- All Records
				$sql = "SELECT * FROM $tbl_name WHERE tenantid = ? and templatetype = ?";
				$q = $pdo->prepare($sql);
				$q->execute(array($tenantid,$templatetype));	
				$data = $q->fetchAll();  
			}
			
			return $data;
	}

?>  
            <!-- </div> Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				<div class="col-xs-12 col-sm-12">	
				  <div class="table-responsive">
		              <?php 
						echo "<a class='btn btn-info' href=newemailtemplate.php>Add New emailtemplate</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Email Template Name</b></td>"; 
						// -- CRUD - smstemplateordersequence
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
							foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
							
							$parent = '';
							if(!empty($row['parent'])){$parent = "<input style='height:30px' type='checkbox' id='parent' checked disabled/>";}
							else{$parent ="<input style='height:30px' type='checkbox' id='parent' disabled/>";} 

							$child = '';
							if(!empty($row['child'])){$child = "<input style='height:30px' type='checkbox' id='child' checked disabled/>";}
							else{$child ="<input style='height:30px' type='checkbox' id='child' disabled/>";} 
							
							// -- CRUD - smstemplate
							$create = '';
							if(!empty($row['cancreate'])){$create = "<input style='height:30px' type='checkbox' id='create' checked disabled/>";}
							else{$create ="<input style='height:30px' type='checkbox' id='create' disabled/>";} 
							
							$update = '';
							if(!empty($row['canupdate'])){$update = "<input style='height:30px' type='checkbox' id='update' checked disabled/>";}
							else{$update ="<input style='height:30px' type='checkbox' id='update' disabled/>";} 
							
							$delete = '';
							if(!empty($row['candelete'])){$delete = "<input style='height:30px' type='checkbox' id='delete' checked disabled/>";}
							else{$delete ="<input style='height:30px' type='checkbox' id='delete' disabled/>";} 
							
							echo "<tr>";  
							echo "<td valign='top'>" . nl2br( $row['smstemplatename']) . "</td>";  
							// -- Logout must never be deleted.
								//if(nl2br( $row['label']) == 'Logout')
								{
								}
								//else
								{
									echo "<td valign='top'><a class='btn btn-success' href=editemailtemplate.php?smstemplateid={$row['smstemplateid']}>Edit</a></td>";

									echo "<td valign='top'>
									
									<a class='btn btn-danger' href=deletesmstemplate.php?smstemplateid={$row['smstemplateid']}>Delete</a></td> "; 
									echo "</tr>";
								}
						} 
						echo "</table>"; 
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   </div>	
  </div>	
</div>	

   		<!---------------------------------------------- End Data ----------------------------------------------->