<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(!function_exists('create_rentalproperty'))
{
	function create_rentalproperty($data)
	{ 
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sqlData = sql_associative_array($data);
		//print_r($sqlData[2]);
			try
			{
				$sql = "INSERT INTO rentalproperty
				({$sqlData[0]})
				VALUES ({$sqlData[1]}?)";
				$q   = $pdo->prepare($sql);
				$q->execute($sqlData[2]);
				$id = $pdo->lastInsertId();	
				$pdo->commit();		
			}					
			catch (PDOException $e)
			{
				if($e->getMessage() != 'There is no active transaction')
				{
					echo  $e->getMessage();	
				}
			}
			return $id;
	}
}

if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}	  
  $bulksmsclientid = '';
  if(isset($_SESSION['bulksmsclientid'])){$bulksmsclientid = $_SESSION['bulksmsclientid'];}
  
$tbl_user="customer";  
$Title = '';
$FirstName = '';
$LastName = '';
$phone = '';
$occupation = '';
$status = '';
$position = '';
$Suburb = "";
$ward = "";
$userid = 0;
$count  = 0;
$IntUserCode = getsession('IntUserCode');

$rentalpropertyname 	= '';
$rentalpropertytype 	= '';
$rentalpropertysize 	= '';
$rentalpropertyprice 	= '';
$rentalpropertycity 	= '';
$website 				= '';
$leavy 					= '';
$status					= '';

$street = '';
$suburb = '';
$province = '';
$postalcode = '';


// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$UserIdDecoded = "";
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.

if (!empty($_POST)) 
{   $count = 0;

$dataProperty = array();	
$dataProperty['rentalpropertyname'] = $rentalpropertyname 	= getpost('rentalpropertyname');
$dataProperty['rentalpropertytype'] = $rentalpropertytype 	= getpost('rentalpropertytype');
$dataProperty['rentalpropertysize'] = $rentalpropertysize 	= getpost('rentalpropertysize');
$dataProperty['rentalpropertyprice'] = $rentalpropertyprice 	= getpost('rentalpropertyprice');
$dataProperty['rentalpropertycity'] = $rentalpropertycity 	= getpost('rentalpropertycity');
$dataProperty['website'] = $website 				= getpost('website');
$dataProperty['leavy'] = $leavy 				= getpost('leavy');
$dataProperty['status'] = $status				= getpost('status');
$dataProperty['IntUserCode'] = $IntUserCode = getsession('IntUserCode');

$dataProperty['street']     = $street = getpost('street');					
$dataProperty['suburb']     = $suburb = getpost('suburb'); 						
$dataProperty['province']   = $province = getpost('province');; 						
$dataProperty['postalcode'] = $postalcode = getpost('postalcode');; 						


create_rentalproperty($dataProperty);
$count = $count + 1;
$valid = true;
}
?>
<div class="container background-white bottom-border">
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Rental Property
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Rental Property was successfully created.</p>");
		}
?>		
</div>				
			<p><b>Name</b><br /><input style="height:30px" type='text' name='rentalpropertyname' value="<?php echo !empty($rentalpropertyname)?$rentalpropertyname:'';?>" /></p>
			<p><b>Type</b><br /><input style="height:30px" type='text' name='rentalpropertytype' value="<?php echo !empty($rentalpropertytype)?$rentalpropertytype:'';?>" /></p>
			<p><b>Size</b><br /><input style="height:30px" type='text' name='rentalpropertysize' value="<?php echo !empty($rentalpropertysize)?$rentalpropertysize:'';?>"  /></p>
			<p><b>Rental Price</b><br /><input style="height:30px" type='number' step='0.01' name='rentalpropertyprice' value="<?php echo !empty($rentalpropertyprice)?$rentalpropertyprice:'';?>"  /></p>
			<p><b>Leavy</b><br /><input style="height:30px" type='number' name='leavy' step="0.00" value="<?php echo !empty($leavy)?$leavy:'';?>"  /></p>
			<p><b>City</b><br /><input style="height:30px" type='text' name='rentalpropertycity' value="<?php echo !empty($rentalpropertycity)?$rentalpropertycity:'';?>"  /></p>			
			<p><b>WebSite URL</b><br /><input style="height:30px" type='text' name='website' value="<?php echo !empty($website)?$website:'';?>"  /></p>			

			<p><b>street</b><br /><input style="height:30px" type='text' name='street' value="<?php echo !empty($street)?$street:'';?>"  /></p>			
			<p><b>suburb</b><br /><input style="height:30px" type='text' name='suburb' value="<?php echo !empty($suburb)?$suburb:'';?>"  /></p>			
			<p><b>province</b><br /><input style="height:30px" type='text' name='province' value="<?php echo !empty($province)?$province:'';?>"  /></p>			
			<p><b>postalcode</b><br /><input style="height:30px" type='text' name='postalcode' value="<?php echo !empty($postalcode)?$postalcode:'';?>"  /></p>			
			
			<p><b>Status</b></p>
			<SELECT  id="status" class="form-control" name="status" size="1" style="width:200px">
									<OPTION value="Active">Active</OPTION>
									<OPTION value="Disabled">Disabled</OPTION>
			</SELECT>
		<table>
		<tr>
			<td>
			   <div>
				<button type="submit" class="btn btn-primary">Save New Rental Property</button>
				<a class="btn btn-primary" href="rentalproperties">Back</a>
			   </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>
