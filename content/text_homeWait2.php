<?php 
// -- Database Declarations and config:  
  include 'database.php';
  $tbl_loanapp = "loanapp";
  $db_name = "ecashpdq_eloan";
  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 

  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		if($row['loantypeid'] == 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];

// ----------- BOC Global Settings 16.04.2017 -------------------	
// -- Currency & Company Global Details.
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	$q->execute();
	$dataGlobal = $q->fetch(PDO::FETCH_ASSOC);
	
	if($dataGlobal['globalsettingsid'] >= 1)
	{
		$_SESSION['currency'] = $dataGlobal['currency'];// -- 10.02.2017
	}	
	else
	{
	  $_SESSION['currency'] = 'R';
	}
// ------ EOC Global Settings 16.04.2014 -------------------------	

	Database::disconnect();
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
		{
				$loggedUser = $_SESSION['username'];
				$Title = $_SESSION['Title'];
				$FirstName = $_SESSION['FirstName'];
				$LastName = $_SESSION['LastName'];
									
				if($_SESSION['role'] == 'customer')   
				{           										
						echo "<div class='container background-white bottom-border'>
								<div class='row margin-vert-30'>".
									$dataGlobal['customerpage']
								."</div>
								</div>";
														
			    }
				else if($_SESSION['role'] == 'admin')   
				{
										
										$TotalApprovedLoans = GetTotalLoans("APPR");
										$TotalRejectedLoans = GetTotalLoans("REJ");
										$TotalSettledLoans  = GetTotalLoans("SET");
										$TotalPendingLoans  = GetTotalLoans("PEN");
										$TotalCancelledLoans  = GetTotalLoans("CAN");

										$ApprovedLoans = urlencode(base64_encode("APPR"));
										$RejectedLoans = urlencode(base64_encode("REJ"));
										$SettledLoans = urlencode(base64_encode("SET"));
										$PendingLoans = urlencode(base64_encode("PEN"));
										$CancelledLoans = urlencode(base64_encode("CAN"));
										
									 echo "<div class='container background-white bottom-border'>
														<div class='margin-vert-30'>
															<!-- Main Text -->

													<div id='page-wrapper'>
														<div>
															<div class='col-lg-12'>
																<h1 class='page-header'>Dashboard</h1>
															</div>
														</div>		
													</div>".'		
										
<!------------------------------------- Dashboards ----------------------------------->
<!-------- Awaiting Approval --------->
<div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalPendingLoans.'</i>																		
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Pending Loans</h3>
                                </div>
                            </div>
                        </div>
                        <a href="custAuthenticate?ExecApproval='.$PendingLoans.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!-------- Approved Loans ------>
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalApprovedLoans.'</i>																		
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Approved Loans</h3>
                                </div>
                            </div>
                        </div>
                        <a href="custAuthenticate?ExecApproval='.$ApprovedLoans.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div>
<!----- Settled Loans -->
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalSettledLoans.'</i>																		
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Settled Loans</h3>
                                </div>
                            </div>
                        </div>
                        <a href="custAuthenticate?ExecApproval='.$SettledLoans.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div>
<!-------- Rejected Loans ------------->
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalRejectedLoans.'</i>																		
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Rejected Loans</h3>
                                </div>
                            </div>
                        </div>
                        <a href="custAuthenticate?ExecApproval='.$RejectedLoans.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>				
</div>				
<hr/>
<!-------- CancelledLoans Loans ------------->
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-orange">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalCancelledLoans.'</i>																		
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Cancelled Loans</h3>
                                </div>
                            </div>
                        </div>
                        <a href="custAuthenticate?ExecApproval='.$CancelledLoans.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>				
</div>				

<!-------------------------------------- EOC Dashboard -------------------------------->										
'.
									$dataGlobal['adminpage']									
									."</div></div>";
									
									}
									
		}
		else
		{
									// -- BCO Loan Calculator --- //
									echo '<div class="container background-white bottom-border">
									<div class="margin-vert-30">'.
												
														$dataGlobal['homepage']	
														.'</div>
														<!-- <h3 style=" margin: 0px 40% 0 40%;">Loan Calculator</h3> -->
														<hr>
									<div class="container">
      <div class="price-box">
        <div class="row">
          <div class="col-sm-6">
                <form class="form-horizontal form-pricing" role="form">

                  <div class="price-slider">
                    <h4 class="great">Amount</h4>
                    <span>Minimum R100 is required</span>
                    <div class="col-sm-12">
                      <div id="slider_amirol"></div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Duration</h4>
                    <span>Please choose one</span>
                    <div class="btn-group btn-group-justified">
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month active-month selected-month" id="1month">1 Month</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="2month">2 Months</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="3month">3 Months</button>
                      </div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Term</h4>
                    <span>Please choose one</span>
                      <input name="sliderVal" type="hidden" id="sliderVal" value="0" readonly="readonly" />
                <input name="month" type="hidden" id="month" value="1month" readonly="readonly" />
                
				<input name="term" type="hidden" id="term" value="monthly" readonly="readonly" />
                      <div class="btn-group btn-group-justified">
                        <!--<div class="btn-group btn-group-lg">
                    <button type="button" class="btn btn-primary btn-lg btn-block term" id="quarterly">Quarterly</button>
                  </div>-->
                        <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term active-term selected-term" id="monthly">Monthly</button>
                  </div>
                       <!-- <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term" id="weekly">Weekly</button>
                        </div> -->
                      </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="price-form">

                  <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Annually (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">-->
                            <!-- <p class="price lead" id="total"></p>
                            <input class="price lead" name="totalprice" type="text" id="total" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Monthly Instalment(R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             <!-- <p class="price lead" id="total12"></p>-->
                            <input class="price lead" name="totalprice12" type="text" id="total12" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>
                    <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Weekly (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             --><!-- <p class="price lead" id="total52"></p>
                            <input class="price lead" name="totalprice52" type="text" id="total52" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div style="margin-top:30px"></div>
                    <!-- <hr class="style"> -->

                  <div class="form-group">
                      <div class="col-sm-12">
                        <a href="applicationForm" class="btn btn-primary btn-lg btn-block">Apply Now<span class="glyphicon glyphicon-chevron-right"></span></a>
                      </div>
                  </div>
                    <!-- <div class="form-group">
                      <div class="col-sm-12">
                          <img src="https://github.com/AmirolAhmad/Bootstrap-Calculator/blob/master/images/payment.png?raw=true" class="img-responsive payment" />
                      </div>
                    </div> -->

                  </div>

                </form>
            </div>
            <!-- <p class="text-center" style="padding-top:10px;font-size:12px;color:#2c3e50;font-style:italic;">Created by <a href="https://twitter.com/AmirolAhmad" target="_blank">AmirolAhmad</a></p> -->
        </div>

          </div>

      </div>
      
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>';
									// -- EOC Loan Calculator -- //	
									echo "<hr>
														</div>";
														
}

// -- Dashboards Function -- //
function GetTotalLoans($status)
{ 	
	 Global $tbl_loanapp;
	 Global $db_name;
	 $Totals = 0;
	 $pdo = Database::connectDB(); 
	 $check_user = "SELECT * FROM $tbl_loanapp WHERE ExecApproval='$status'";
	 
	 mysql_select_db("$db_name",$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

	// Mysql_num_row is counting table row
	$Totals = mysql_num_rows($result);

	return $Totals;
}
// -- Dashboards Function -- //

?>