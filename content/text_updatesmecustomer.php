<!-- Start Update the eLoan - Application Data -->
<?php 
	
require 'database.php';

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_customer = "businessproxy";
$tbl_loanapp = "loanapp";
$tbl_businessInfo = "businessowneinfo";
$tbl_business = "business";
$dbname = "eloan";
$comingfrom = $_SERVER['HTTP_REFERER'];


// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
$paymentfrequency = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

$customerid = null;
$ApplicationId = null;
$count = 0;

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$rejectreason = "";
$rejectreasonError = "";
$ExecApproval = "";

// -- BOC Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = "";
$ApplicationIdEncoded = "";
$location = "Location: customerLogin";
// -- EOC Encrypt 16.09.2017 - Parameter Data.
		

	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
		if ( !empty($_GET['ApplicationId'])) 
		{
			$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
			$ApplicationIdEncoded = $ApplicationId;
			$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}
	if ( null==$customerid ) 
	{
		//header("Location: custAuthenticate.php");
	}
	
	if ( null==$ApplicationId ) {
		//header("Location: custAuthenticate.php");
	}

$Original_email = '';

if ( !empty($_POST)) 
{

// keep track validation errors - 		// Customer Details
$BusinessNameError = null;
$BusinessrIdError = null;
$BusinessTypeError = null;
$ProductsorServicesError = null;
$StageOfBusinessError = null;
$TurnOverAnnualError = null;
$TurnOverFirewoodError = null;
$AssetValueToDateError = null;
$BusinessLocationError = null;
$StreetError = null;
$SuburbError = null;
$StateError = null;
$KeyContactPersonError = null;
$KeyContactNumberError = null;
$KeyemailAddressError = null;
$DirectOrNameError = null;
$DirectorIdentificationNumberError = null;
$OwnerShipTypeError = null;
$SharesError = null;
$StartUpCostError = null;
$AssetAcquisitionError = null;
$WorkingCapitalError = null;
$FundingAmountError = null;
$DateAccptError = null;








$TitleError = null;
$FirstNameError = null; 
$LastNameError = null;
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$DobError = null;
$phoneError = null;
$emailError = null;
		
// keep track validation errors - 		// Loan Details
$MonthlyExpenditureError = null; 
$TotalAssetsError = null; 
$ReqLoadValueError = null; 
$DateAccptError = null; 
$LoanDurationError = null; 
$StaffIdError = null; 
$InterestRateError = null; 
$ExecApprovalError = null; 
$MonthlyIncomeError = null;
 
$RepaymentError = null; 
$paymentmethodError = null; 
$SuretyError = null; 
$monthlypaymentError = null; 
$FirstPaymentDateError = null; 
$lastPaymentDateError = null; 

// ------------------------- Declarations - Banking Details ------------------------------------- //
$AccountHolderError = null;
$AccountTypeError = null;
$AccountNumberError = null;
$BranchCodeError = null;
$BankNameError = null;
$rejectreasonError = null;
// -------------------------------- Banking Details ------------------------------------------ // 
 
// keep track post values
// Customer Details
$BusinessName = $_POST['BusinessName'];
$BusinessrId = $_POST['BusinessrId'];
$BusinessType = $_POST['BusinessType'];
$ProductsorServices = $_POST['ProductsorServices'];
$StageOfBusiness = $_POST['StageOfBusiness'];
$TurnOverAnnual = $_POST['TurnOverAnnual'];
$TurnOverFirewood = $_POST['TurnOverFirewood'];
$AssetValueToDate = $_POST['AssetValueToDate'];
$BusinessLocation = $_POST['BusinessLocation'];
$Street = $_POST['Street'];
$Suburb = $_POST['Suburb'];
$State = $_POST['State'];
$KeyContactPerson = $_POST['KeyContactPerson'];
$KeyContactNumber = $_POST['KeyContactNumber']; 
$KeyemailAddress = $_POST['KeyemailAddress'];
$DirectOrName = $_POST['DirectOrName'];
$DirectorIdentificationNumber = $_POST['DirectorIdentificationNumber'];
$OwnerShipType = $_POST['OwnerShipType'];
$Shares = $_POST['Shares'];
$StartUpCost = $_POST['StartUpCost'];
$AssetAcquisition = $_POST['AssetAcquisition'];
$WorkingCapital = $_POST['WorkingCapital'];
$FundingAmount = $_POST['FundingAmount'];
$DateAccpt = $_POST['DateAccpt'];
$ExecApproval  = $_POST['ExecApproval'];

// ------------------------- $_POST - Banking Details ------------------------------------- //
$AccountHolder = $_POST['AccountHolder'];
$AccountType = $_POST['AccountType'];
$AccountNumber = $_POST['AccountNumber'];
//$BranchCode = $_POST['BranchCode'];
$BankName = $_POST['BankName'];
// -------------------------------- Banking Details ------------------------------------------ //

// validate input - 		// Customer Details
		$valid = true;
	/* 	if (empty($BusinessName)) { $BusinessNameError = 'Please enter Business Name'; $valid = false;}
		if (empty($BusinessrId)) { $BusinessrIdError = 'Please enter Business Name'; $valid = false;}
		if (empty($BusinessType)) { $BusinessTypeError = 'Please enter Business Name'; $valid = false;}
		if (empty($ProductsorServices)) { $ProductsorServicesError = 'Please enter Business Name'; $valid = false;}
		if (empty($StageOfBusiness)) { $StageOfBusinessError = 'Please enter Business Name'; $valid = false;}
		if (empty($TurnOverAnnual)) { $TurnOverAnnualError = 'Please enter Business Name'; $valid = false;}
		if (empty($TurnOverFirewood)) { $TurnOverFirewoodError = 'Please enter Business Name'; $valid = false;}
		if (empty($AssetValueToDate)) { $AssetValueToDateError = 'Please enter Business Name'; $valid = false;}
		if (empty($BusinessLocation)) { $BusinessLocationError = 'Please enter Business Name'; $valid = false;}
		if (empty($Street)) { $StreetError = 'Please enter Business Name'; $valid = false;}
		if (empty($Suburb)) { $SuburbError = 'Please enter Business Name'; $valid = false;}
		if (empty($State)) { $StateError = 'Please enter Business Name'; $valid = false;}
		if (empty($KeyContactPerson)) { $KeyContactPersonError = 'Please enter Business Name'; $valid = false;}
		if (empty($KeyContactNumber)) { $KeyContactNumberError = 'Please enter Business Name'; $valid = false;}
		if (empty($KeyemailAddress)) { $KeyemailAddressError = 'Please enter Business Name'; $valid = false;}
		if (empty($DirectOrName)) { $DirectOrNameError = 'Please enter Business Name'; $valid = false;}
		if (empty($DirectorIdentificationNumber)) { $DirectorIdentificationNumberError = 'Please enter Business Name'; $valid = false;}
		if (empty($OwnerShipType)) { $OwnerShipTypeError = 'Please enter Business Name'; $valid = false;}
		if (empty($Shares)) { $SharesError = 'Please enter Business Name'; $valid = false;}
		if (empty($StartUpCost)) { $StartUpCostError = 'Please enter Business Name'; $valid = false;}
		if (empty($AssetAcquisition)) { $AssetAcquisitionError = 'Please enter Business Name'; $valid = false;}
		if (empty($WorkingCapital)) { $WorkingCapitalError = 'Please enter Business Name'; $valid = false;}
		if (empty($FundingAmount)) { $FundingAmountError = 'Please enter Business Name'; $valid = false;}
		if (empty($DateAccpt)) { $DateAccptError = 'Please enter Business Name'; $valid = false;}
	
						// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		if (empty($MonthlyExpenditure)) { $nameError = 'Please enter MonthlyExpenditure'; $valid = false;}
		if (empty($TotalAssets)) { $nameError = 'Please enter Total Assets'; $valid = false;}
		if (empty($ReqLoadValue)) { $nameError = 'Please enter Req Loan Value'; $valid = false;} */
		if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		
		if (!empty($ExecApproval))
		{ 
			if($ExecApproval == 'REJ')
			{
				if(isset($_POST['rejectreason']))
				{
					$rejectreason = $_POST['rejectreason'];
				}

				// -- Validate Reason -- //
				if (empty($rejectreason)) 
				{ 
					$rejectreasonError = 'Please provide the reason for rejection separated by a period. <br/>For e.g. Account in arrears.In debited.You are broke'; $valid = false;
				}
			}
			else
			{
				$rejectreasonError = null;
			}
		}
		else
		{
			$rejectreasonError = null;
		}

		if (empty($DateAccpt)) { $nameError = 'Please enter Date Accpt'; $valid = false;}
		
		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// -- validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		else
		{
		// -- User - E-mail.
			if ($valid and ($Original_email != $email))
			{
				$query = "SELECT * FROM user WHERE email = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use.'; $valid = false;}
			}
		}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		//if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		$valid = true;
		
		// update data
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE $tbl_loanapp ";
			$sql = $sql."SET ExecApproval = ?";
			$sql = $sql." WHERE CustomerId = ? and ApplicationId = ?";
			//echo $ExecApproval;
			$q = $pdo->prepare($sql);
			error_log("Before".$ExecApproval);
			$q->execute(array($ExecApproval,$customerid,$ApplicationId));
			error_log("after".$ExecApproval);
			Database::disconnect();
			$count++;

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
			// Update User Email
			$sql = "UPDATE user SET email = ? WHERE userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($email,$customerid));
			Database::disconnect();
			$count++; 
///////////////////////////// ---------------------------------- SESSION DECLARATIONS --------------------------/////////
// --------------------------- Session Declarations -------------------------//
	$_SESSION['Title'] = $Title;
	$_SESSION['FirstName'] = $FirstName;
	$_SESSION['LastName'] = $LastName;
	$_SESSION['password'] = '';
										
	// Id number
	$_SESSION['idnumber']  = $customerid;
	$clientname = $Title.' '.$FirstName.' '.$LastName ;
	$suretyname = $clientname;
	$name = $suretyname;		
	// Phone numbers
	$_SESSION['phone'] = $phone;
	// Email
	$_SESSION['email'] = $email ;
	// Street
	$_SESSION['Street'] = $Street;
	// Suburb
	$_SESSION['Suburb'] = $Suburb;
	// City
	$_SESSION['City'] = $City;
	// State
	$_SESSION['State'] = $State;
	// PostCode
	$_SESSION['PostCode'] = $PostCode;
	// Dob
	$_SESSION['Dob'] = $Dob;
	// phone
	$_SESSION['phone'] = $phone;	
	//------------------------------------------------------------------- 
	//           				LOAN APP DATA							-
	//-------------------------------------------------------------------			
	$_SESSION['loandate'] = $DateAccpt;// "01-08-2016";
	$_SESSION['loanamount'] = $ReqLoadValue ;// "R2000";
	$_SESSION['loanpaymentamount'] = $Repayment  ; //"R2400";
	$_SESSION['loanpaymentInterest'] = $InterestRate ;// = "20%";
	$_SESSION['FirstPaymentDate'] = $FirstPaymentDate;// = ;//'01-08-2016';
	$_SESSION['LastPaymentDate'] = $lastPaymentDate;// = '01-08-2016';
	$_SESSION['applicationdate'] = $DateAccpt;// = '01-08-2016';
	$_SESSION['status'] = $ExecApproval;// = '01-08-2016';
	$_SESSION['monthlypayment'] = $monthlypayment;// Monthly Payment Amount 
	$_SESSION['LoanDuration'] = $LoanDuration;// Loan Duration
	$_SESSION['ApplicationId'] = $ApplicationId;
	
	// ------------------------- $_SESSION - Banking Details ------------------------------------- //
	$_SESSION['AccountHolder'] = $AccountHolder;
	$_SESSION['AccountType'] = $AccountType;
	$_SESSION['AccountNumber'] = $AccountNumber ;
	$_SESSION['BranchCode'] = $BranchCode;
	$_SESSION['BankName'] = $BankName;
  // ------------------------------- $_SESSION Banking Details ------------------------------------------ //
  // -- start - 16.04.2017 -- Regroup maintainable tables. ---- // 
	$_SESSION['paymentfrequency'] = $paymentfrequency;
  // -- End - 16.04.2017 -- Regroup maintainable tables.   --- // 
   $_SESSION['rejectreason'] = $rejectreason;
  
///////////////////////////// ---------------------------------- END SESSION DECLARATIONS ----------------------/////////			

	// Sent Rejected or Approved status to customer provided
	if($_SESSION['ExecApproval'] != $ExecApproval)
	{
		// It means the status has been changed.
		
		// Rejected
		if($ExecApproval == 'REJ')
		{
		  SendEmailpage('content/text_emailLoanStatusRejected.php');	
		}
		// Approved
		if($ExecApproval == 'APPR')
		{
			SendEmailpage('content/text_emailLoanStatusAccepted.php');	
////////////////////////////////////////////////////////////////////////
// -- Date: 19/01/2023	
// -- Automatically create a loan payment schedule 
// -- and loan application charges.
// -- Call the functions – when the loan is approved.
// -- database.php->auto_paymentschedule and,
// -- database.php->application_charges
// ---------------------------------------------------------------------
// -- Automatically creation of payment schedules ----------------------
		$disbursementdate   = date('Y-m-d');
		$amountdue 			= $monthlypayment;
		$scheduleddate		= $FirstPaymentDate;
		$customername 	    = $FirstName.$LastName;
		$display			= '';
		$Indicator			= '';
		auto_paymentschedule(
		$ApplicationId,$disbursementdate,$ReqLoadValue,
		$Repayment,$amountdue,$scheduleddate,$LoanDuration, 
		$InterestRate,$customerid,
		$customername,$display,$Indicator);
// ---------------------------------------------------------------------	
// -- Automatically creation of application charges --------------------	
		application_charges($ApplicationId,'');
////////////////////////////////////////////////////////////////////////
		}
		// Funded
		if($ExecApproval == 'FUND')
		{
			SendEmailpage('content/text_emailLoanStatusFunded.php');	
		}
		// -- Cancelled
		if($ExecApproval == 'CAN')
		{
			SendEmailpage('content/text_emailLoanStatusCancelled.php');	
		}
		
		if($ExecApproval == 'REV')
		{
			//SendEmailpage('content/text_emailLoanStatusCancelled.php');	
		}
		
		// -- Settled.
		if($ExecApproval == 'SET')
		{
			SendEmailpage('content/text_emailLoanStatusSettled.php');	
		}
		// -- BOC 18.11.2018.				
		// -- Defaulter.
		if($ExecApproval == 'DEF')
		{
			SendEmailpage('content/text_emailLoanStatusDefaulted.php');	
		}

				// -- Legal.
				if($ExecApproval == 'LEGAL')
				{
					SendEmailpage('content/text_emailLoanStatusLegal.php');	
				}

				// -- Rehabilitation.
				if($ExecApproval == 'REH')
				{
					SendEmailpage('content/text_emailLoanStatusRehabilitation.php');	
				}
				// -- EOC 18.11.2018.				
			}
		}
	} 
else 
{
$sql =  "select * from $tbl_loanapp where CustomerId = ? AND ApplicationId = ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid,$ApplicationId));
$data = $q->fetch(PDO::FETCH_ASSOC);

$businessreg=$data[businessrId];

error_log('Get business ID');
error_log($businessreg);


//Get Business Information 
$sql =  "select * from $tbl_business  where regNumber = ?";
$q = $pdo->prepare($sql);
$q->execute(array($businessreg));
$data2 = $q->fetch(PDO::FETCH_ASSOC);


//Business Owners Details

$sql =  "select * from  $tbl_businessInfo where idbusiness = ? limit 1";
$q = $pdo->prepare($sql);
$q->execute(array($businessreg));
$BussOwnerdata3 = $q->fetch(PDO::FETCH_ASSOC);

//Business Proxy Details

$sql =  "select * from $tbl_customer where idbusiness = ?";
$q = $pdo->prepare($sql);
$q->execute(array($businessreg));
$proxydata4 = $q->fetch(PDO::FETCH_ASSOC);


// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
if(empty($data))
{
	header("Location: custAuthenticate");
}
// -- EOC Encrypt 16.09.2017 - Parameter Data.

// -- UserEmail Address
$sql =  "select * from user where userid= ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid));
$dataEmail = $q->fetch(PDO::FETCH_ASSOC);

// Customer Details
$BusinessName = $data['businessname'];
$BusinessrId = $data['businessrId'];
$BusinessType = $data2['businessType'];
$ProductsorServices = $data2['productsorServices'];
$StageOfBusiness = $data['stageOfBusiness'];
$TurnOverAnnual = $data['turnoverfirewood'];
$TurnOverFirewood = $data['turnoverfirewood'];
$AssetValueToDate = $data['assetvaluetodate'];
$BusinessLocation = $data2['businesslocation'];
$Street = $data2['street'];
$Suburb = $data2['suburb'];
$State = $data2['state'];
$KeyContactPerson = $proxydata4['keytitle'].' '.$data['keyfirstname'].' '.$data['keysurname'];
$KeyContactNumber = $proxydata4['keycontactNumber']; 
$KeyemailAddress = $proxydata4['keyemailAddress'];
$DirectOrName = $BussOwnerdata3['title'].' '.$BussOwnerdata3['firstname'].' '.$BussOwnerdata3['surname'];
$DirectorIdentificationNumber = $BussOwnerdata3['identificationNumber'];
$OwnerShipType = $BussOwnerdata3['ownershiptype'];
$Shares = $BussOwnerdata3['shares'];
$StartUpCost = $data['startupcost'];
$AssetAcquisition = $data['assetacquisition'];
$WorkingCapital = $data['workingcapital'];
$FundingAmount = $data['fundingamount'];
$DateAccpt = $data['DateAccpt'];
$ExecApproval  = $data['ExecApproval'];

	
$_SESSION['ExecApproval'] = $ExecApproval; // Store Approval Status for later user via e-mail to customer sending.		

// ------------------------- $data - Banking Details ------------------------------------- //
$AccountHolder = $data['accountholdername'];
$paymentmethod = $data['paymentmethod']; 
$AccountType = $data['accounttype'];
$AccountNumber = $data['accountnumber'];
$BranchCode = $data['branchcode'];
$BankName = $data['bankname'];
$rejectreason = $data['rejectreason'];

// -------------------------------- Banking Details ------------------------------------------ //

// -------------------------------- Start 16.04.2017 --- Regroup all the maintainable table ---//
$paymentfrequency = $data['paymentfrequency'];
// -------------------------------- End 16.04.2017 ---- Regroup all the maintainable table ---//
		Database::disconnect();
	}
?>
<!-- End Update the eLoan - Application Data -->

<div class="container background-white bottom-border">   
		<div class="row">
 
 <!-- <div class="span10 offset1"> -->
  <form  action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
    <div class="table-responsive">
<table class=   "table table-user-information">
<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->	

<!--<tr>
	<td><div><a class="btn btn-primary" href="smeAuthenticate">Back</a></div></td>
	<td><div><button type="submit" class="btn btn-primary">Update</button></div></td>
</tr>	-->
<tr>
<td colspan="3"> 
<div class="panel-heading">
<h4>Business Info</h4>
</div>	

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Client information updated successfully!</p>\n", $count);
			// Sent Rejected or Approved status to customer provided
			if($_SESSION['ExecApproval'] != $ExecApproval)
			{	
				// Rejected
				if($ExecApproval == 'REJ')
				{
					printf("<p class='status'>The e-Loan application status is rejected. A rejected e-mail is sent to customer Inbox!</p>\n");

				}
				// Approved
				if($ExecApproval == 'APPR')
				{
					printf("<p class='status'>The e-Loan application status is successfully approved. An approved e-mail is sent to customer Inbox!</p>\n");
				}
				// Funded
				if($ExecApproval == 'FUND')
				{
					printf("<p class='status'>The e-Loan application status is successfully funded. An approved e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Settled
				if($ExecApproval == 'SET')
				{
					printf("<p class='status'>The e-Loan application status is successfully settled. An comfirmation e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Cancelled
				if($ExecApproval == 'CAN')
				{
					printf("<p class='status'>The e-Loan application status has been cancelled. An cancelation e-mail is sent to customer Inbox!</p>\n");
				}
				if($ExecApproval == 'REV')
				{
					printf("<p class='status'>The e-Loan application is in review.</p>\n");
				}
			}
		}
		// -- if there any error display here.
		else
		{
				// keep track validation errors - 		// Customer Details
		if(!empty($BusinessNameError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		if(!empty($BusinessrIdError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($BusinessTypeError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($ProductsorServicesError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($StageOfBusinessError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($TurnOverAnnualError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($TurnOverFirewoodError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($AssetValueToDateError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($BusinessLocationError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($StreetError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($SuburbError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($StateError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($KeyContactPersonError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($KeyContactNumberError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($KeyemailAddressError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($DirectOrNameError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($DirectorIdentificationNumberError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($OwnerShipTypeError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($SharesError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($StartUpCostError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($AssetAcquisitionError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($WorkingCapitalError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($FundingAmountError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		if(!empty($DateAccptError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		// ------------------------- Declarations - Banking Details ------------------------------------- //
		if(!empty($AccountHolderError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		if(!empty($AccountTypeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		if(!empty($AccountNumberError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		//if(!empty($BranchCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		if(!empty($BankNameError )){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}
		}
		?>
				
</td>						
		
</tr>

<tr>
	 <td>
		<div class="control-group <?php echo !empty($BusinessNameError)?'error':'';?>">
			<label class="control-label"><strong>Business name</strong></label>
			<div class="controls">
				<input style="height:30px" readonly class="form-control margin-bottom-10" name="BusinessName" type="text"  placeholder="BusinessName" value="<?php echo !empty($BusinessName)?$BusinessName:'';?>">
				<?php if (!empty($BusinessNameError)): ?>
				<span class="help-inline"><?php echo $BusinessNameError;?></span>
				<?php endif; ?>
			</div>
		</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($BusinessrIdError)?'error':'';?>">
		<label class="control-label"><strong>CIPC reg number</strong></label>
		<div class="controls">
				<input style="height:30px" readonly class="form-control margin-bottom-10" name="BusinessrId" type="text"  placeholder="BusinessrId" value="<?php echo !empty($BusinessrId)?$BusinessrId:'';?>">
				<?php if (!empty($BusinessrIdError)): ?>
				<span class="help-inline"><?php echo $BusinessrIdError;?></span>
				<?php endif; ?>
			
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($BusinessTypeError)?'error':'';?>">
		<label class="control-label"><strong>Type of business</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="BusinessType" type="text"  placeholder="Business Type" value="<?php echo !empty($BusinessType)?$BusinessType:'';?>">
			<?php if (!empty($BusinessTypeError)): ?>
			<span class="help-inline"><?php echo $BusinessTypeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ProductsorServicesError)?'error':'';?>">
		<label class="control-label"><strong>Type of products offered</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="ProductsorServices" type="text"  placeholder="Products or Services" value="<?php echo !empty($ProductsorServices)?$ProductsorServices:'';?>">
			<?php if (!empty($ProductsorServicesError)): ?>
			<span class="help-inline"><?php echo $ProductsorServicesError;?></span>
			<?php endif; ?>

		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($StageOfBusinessError)?'error':'';?>">
		<label class="control-label"><strong>Stage of Business</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="StageOfBusiness" type="text"  placeholder="Stage Of Business" value="<?php echo !empty($StageOfBusiness)?$StageOfBusiness:'';?>">
			<?php if (!empty($StageOfBusinessError)): ?>
			<span class="help-inline"><?php echo $StageOfBusinessError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($TurnOverAnnualError)?'error':'';?>">
		<label class="control-label"><strong>Annual Sales</strong></label>
		<div class="controls">
			<input style="height:30px"readonly  class="form-control margin-bottom-10" name="TurnOverAnnual" type="text"  placeholder="Turn Over Annual" value="<?php echo !empty($TurnOverAnnual)?$TurnOverAnnual:'';?>">
			<?php if (!empty($TurnOverAnnualError)): ?>
			<span class="help-inline"><?php echo $TurnOverAnnualError;?></span>
			<?php endif; ?>
		</div>
		
	</div>
	</td>
	<td>
		<div class="control-group <?php echo !empty($TurnOverFirewoodError)?'error':'';?>">
		<label class="control-label"><strong>Annual firewood Sales</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="TurnOverFirewood" type="text"  placeholder="Turn Over Firewood" value="<?php echo !empty($TurnOverFirewood)?$TurnOverFirewood:'';?>">
			<?php if (!empty($TurnOverFirewoodError)): ?>
			<span class="help-inline"><?php echo $TurnOverFirewoodError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
		<div class="control-group <?php echo !empty($AssetValueToDateError)?'error':'';?>">
		<label class="control-label"><strong>Asset value</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="AssetValueToDate" type="text"  placeholder="Asset Value To Date" value="<?php echo !empty($AssetValueToDate)?$AssetValueToDate:'';?>">
			<?php if (!empty($AssetValueToDateError)): ?>
			<span class="help-inline"><?php echo $AssetValueToDateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	

</tr>

<!-- Street,Suburb,City -->
<tr>

	<td>
	<div class="control-group <?php echo !empty($BusinessLocationError)?'error':'';?>">
		<label class="control-label"><strong>Business location</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="BusinessLocation" type="text"  placeholder="Business Location" value="<?php echo !empty($BusinessLocation)?$BusinessLocation:'';?>">
			<?php if (!empty($BusinessLocationError)): ?>
			<span class="help-inline"><?php echo $BusinessLocationError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Physical address</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
			<?php if (!empty($StreetError)): ?>
			<span class="help-inline"><?php echo $StreetError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
			<?php if (!empty($SuburbError)): ?>
			<span class="help-inline"><?php echo $SuburbError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="State" type="text"  placeholder="State" value="<?php echo !empty($State)?$State:'';?>">
			<?php if (!empty($StateError)): ?>
			<span class="help-inline"><?php echo $StateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>


</tr>	

<!-- State,PostCode,Dob -->		
<!-- State,PostCode,Dob -->
<tr>


	<td>
	<div class="control-group <?php echo !empty($KeyContactPersonError)?'error':'';?>">
		<label class="control-label"><strong>Key contact person</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="KeyContactPerson" type="text"  placeholder="Key Contact Person" value="<?php echo !empty($KeyContactPerson)?$KeyContactPerson:'';?>">
			<?php if (!empty($KeyContactPersonError)): ?>
			<span class="help-inline"><?php echo $KeyContactPersonError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group" <?php echo !empty($KeyContactNumber)?'error':'';?>">
		<label class="control-label"><strong>Contact number</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="KeyContactNumber" type="text"  placeholder="Key Contact Number" value="<?php echo !empty($KeyContactNumber)?$KeyContactNumber:'';?>">
			<?php if (!empty($KeyContactNumberError)): ?>
			<span class="help-inline"><?php echo $KeyContactNumberError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group" <?php echo !empty($KeyemailAddressError)?'error':'';?>">
		<label class="control-label"><strong>Email address</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="KeyemailAddress" type="text"  placeholder="Key Email Address" value="<?php echo !empty($KeyemailAddress)?$KeyemailAddress:'';?>">
			<?php if (!empty($KeyemailAddressError)): ?>
			<span class="help-inline"><?php echo $KeyemailAddressError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	
<tr>
<td><div class="panel-heading">
<h4>Directors Info</h4>
</td></div>

</tr>
<tr>
  <td>
	<div class="control-group <?php echo !empty($DirectOrNameError)?'error':'';?>">
		<label class="control-label"><strong>Name</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="DirectOrName" type="text"  placeholder="Direct Or Name" value="<?php echo !empty($DirectOrName)?$DirectOrName:'';?>">
			<?php if (!empty($DirectOrNameError)): ?>
			<span class="help-inline"><?php echo $DirectOrNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DirectorIdentificationNumberError)?'error':'';?>">
		<label class="control-label"><strong>Identification number</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="DirectorIdentificationNumber" type="text"  placeholder="Director Identification Number" value="<?php echo !empty($DirectorIdentificationNumber)?$DirectorIdentificationNumber:'';?>">
			<?php if (!empty($DirectorIdentificationNumberError)): ?>
			<span class="help-inline"><?php echo $DirectorIdentificationNumberError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($OwnerShipTypeError)?'error':'';?>">
		<label class="control-label"><strong>Ownership type</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="OwnerShipType" type="text"  placeholder="Owner Ship Type" value="<?php echo !empty($OwnerShipType)?$OwnerShipType:'';?>">
			<?php if (!empty($OwnerShipTypeError)): ?>
			<span class="help-inline"><?php echo $OwnerShipTypeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
		<td>
	<div class="control-group <?php echo !empty($SharesError)?'error':'';?>">
		<label class="control-label"><strong>Shares</strong></label>
		<div class="controls">
			<input style="height:30px" readonly class="form-control margin-bottom-10" name="Shares" type="text"  placeholder="Shares" value="<?php echo !empty($Shares)?$Shares:'';?>">
			<?php if (!empty($SharesError)): ?>
			<span class="help-inline"><?php echo $SharesError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	

</tr>
<tr>
<td><div class="panel-heading">
<h4>Banking Info</h4>
</td></div>
</tr>
<!-- Account Holder Name,Bank Name,Account number -->	
<tr>
<td> 
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
								<label>Account Holder Name</label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td> 	
<td>
<!-- Bank Name -->
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
								<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT readonly class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
</td>							
<td>
<!-- Account Number  -->
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
								 <label>Account Number</label>
									<div class="controls">
										<input  readonly style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td>

<td>

<!-- Account Type  -->
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
								<label>Account Type</label>
									<div class="controls">
										 <SELECT readonly class="form-control" id="AccountType" name="AccountType" size="1">
									<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
									<!-------------------- EOC 2017.04.15 - Account type --->
									</SELECT>
									</div>	
								</div>
</td>


</tr>
<!----------------------------------------------------------- End Banking Details ---------------------------------------->	

<!-- Loan Details -->
<tr>
	<td>
		<div class="panel-heading">
			<h4>Purpose of Financing</h4>
		</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($StartUpCostError)?'error':'';?>">
		<label class="control-label"><strong>Start up costs</strong></label>
		<div class="controls">
			<input readonly style="height:30px" class="form-control margin-bottom-10" name="StartUpCost" type="text"  placeholder="StartUpCost" value="<?php echo !empty($StartUpCost)?$StartUpCost:'';?>">
			<?php if (!empty($StartUpCostError)): ?>
			<span class="help-inline"><?php echo $StartUpCostError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($AssetAcquisitionError)?'error':'';?>">
		<label class="control-label"><strong>Asset acquisition</strong></label>
		<div class="controls">
			<input readonly style="height:30px" class="form-control margin-bottom-10" name="AssetAcquisition" type="text"  placeholder="Asset Acquisition" value="<?php echo !empty($AssetAcquisition)?$AssetAcquisition:'';?>">
			<?php if (!empty($AssetAcquisitionError)): ?>
			<span class="help-inline"><?php echo $AssetAcquisitionError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($WorkingCapitalError)?'error':'';?>">
		<label class="control-label"><strong>Working capital</strong></label>
		<div class="controls">
			<input readonly style="height:30px" class="form-control margin-bottom-10" name="WorkingCapital" type="text"  placeholder="Working Capital" value="<?php echo !empty($WorkingCapital)?$WorkingCapital:'';?>">
			<?php if (!empty($WorkingCapitalError)): ?>
			<span class="help-inline"><?php echo $WorkingCapitalError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($FundingAmountError)?'error':'';?>">
		<label class="control-label"><strong>Funding required</strong></label>
		<div class="controls">
			<input readonly style="height:30px" class="form-control margin-bottom-10" name="FundingAmount" type="text"  placeholder="Funding Amount" value="<?php echo !empty($FundingAmount)?$FundingAmount:'';?>">
			<?php if (!empty($FundingAmountError)): ?>
			<span class="help-inline"><?php echo $FundingAmountError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>



</tr>


<!-- InterestRate,ExecApproval -->		
<tr>
		<td> 
		<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
			<label class="control-label"><strong>Application Date</strong></label>
			<div class="controls">
			   <input readonly style="height:30px" class="form-control margin-bottom-10" name="DateAccpt" type="text"  placeholder="Date Accpt" value="<?php echo !empty($DateAccpt)?$DateAccpt:'';?>">
			<?php if (!empty($DateAccptError)): ?>
			<span class="help-inline"><?php echo $DateAccptError;?></span>
			<?php endif; ?>
			</div>
		</div>
		</td> 
		
		<td> 
		<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
			<label class="control-label"><strong>Status</strong></label>
			<?php 
			$status = '';
			$statusAPPR = '';
			$statusFUND = ''; //add Mulu 20220727
			$statusREJ = '';
			$statusPEN = '';
			$statusREV = '';
			$statusCAN = '';
			$statusSET = '';
			// -- Defaulter - 20.06.2018.
			$statusDEF = '';

			// -- Legal - 07.11.2018.
			$statusLEGAL = '';
			
			// -- Rehabilitation - 07.11.2018.
			$statusREH = '';
			
			if ($ExecApproval == 'APPR'){ $statusAPPR = 'selected';}
			if ($ExecApproval == 'FUND'){ $statusFUND = 'selected';}//add Mulu 20220727
			if ($ExecApproval == 'REJ') { $statusREJ = 'selected';}
			if ($ExecApproval == 'PEN') { $statusPEN = 'selected';}
			if ($ExecApproval == 'REV') { $statusREV = 'selected';}
			if ($ExecApproval == 'CAN') { $statusCAN = 'selected';}
			if ($ExecApproval == 'SET') { $statusSET = 'selected';}

			// -- Legal - 07.11.2018.
			if ($ExecApproval == 'LEGAL') { $statusLEGAL = 'selected';}
			
			// -- Rehabilitation - 07.11.2018.
			if ($ExecApproval == 'REH') { $statusREH = 'selected';}
			
			// -- Defaulter - 20.06.2018.
			if ($ExecApproval == 'DEF') { $statusDEF = 'selected';}

			if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'avoAdmin' )
			{
				if ($ExecApproval == 'PEN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
							<OPTION value='REV' $statusREV>Review</OPTION>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REV')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'CAN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'APPR')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='APPR' $statusAPPR>Approved</OPTION>					
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'SET')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='SET' $statusSET>Settled</OPTION>				
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'FUND')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='FUND' $statusFUND>Funded</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REJ')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='REJ' $statusREJ>Rejected</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'DEF')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'LEGAL')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='LEGAL' $statusDEF>Legal</OPTION>		
					 </SELECT>
					</div>";
				}
			}
			else if  ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'avoSuper' )
			{
				if ($ExecApproval == 'PEN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
							<OPTION value='REV' $statusREV>Review</OPTION>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REV')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>
							<OPTION value='APPR' $statusAPPR>Approved</OPTION>							
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'CAN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'APPR')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
						<OPTION value='APPR' $statusAPPR>Approved</OPTION>					
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'SET')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='SET' $statusSET>Settled</OPTION>				
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'FUND')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='FUND' $statusFUND>Funded</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REJ')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='REJ' $statusREJ>Rejected</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'DEF')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'LEGAL')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='LEGAL' $statusDEF>Legal</OPTION>		
					 </SELECT>
					</div>";
				}
			
			}
			else if  ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'avoFinance' )
			{
				if ($ExecApproval == 'PEN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='REV' $statusREV>Review</OPTION>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REV')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'CAN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'APPR')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1'>
							<OPTION value='FUND' $statusFUND>Funded</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'SET')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='SET' $statusSET>Settled</OPTION>				
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'FUND')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='FUND' $statusFUND>Funded</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REJ')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='REJ' $statusREJ>Rejected</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'DEF')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'LEGAL')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='LEGAL' $statusDEF>Legal</OPTION>		
					 </SELECT>
					</div>";
				}
			}
			else if  ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'avoSuper' )
			{
				if ($ExecApproval == 'PEN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='REV' $statusREV>Review</OPTION>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REV')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>
							<OPTION value='APPR' $statusAPPR>Approved</OPTION>							
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'CAN')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
							<OPTION value='CAN' $statusCAN>Cancel</OPTION>						
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'APPR')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' >
						<OPTION value='SET' $statusSET>Settled</OPTION>					
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'SET')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='SET' $statusSET>Settled</OPTION>				
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'FUND')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='FUND' $statusFUND>Funded</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'REJ')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='REJ' $statusREJ>Rejected</OPTION>			
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'DEF')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
					 </SELECT>
					</div>";
				}
				else if ($ExecApproval == 'LEGAL')
				{
					echo "<div class='control-group'>
					 <SELECT class='form-control' name='ExecApproval' size='1' disabled>
						<OPTION value='LEGAL' $statusDEF>Legal</OPTION>		
					 </SELECT>
					</div>";
				}
			}
			else
			{
			if ($ExecApproval == 'APPR'){ $status = 'Approved';}
			if ($ExecApproval == 'FUND'){ $status = 'Funded';}
			if ($ExecApproval == 'REJ') { $status = 'Rejected';}
			if ($ExecApproval == 'PEN') { $status = 'Pending';}
			if ($ExecApproval == 'CAN') { $status = 'Cancel';}
			if ($ExecApproval == 'SET') { $status = 'Settled';}
			// -- Defaulter - 20.06.2018.
			if ($ExecApproval == 'DEF') { $status = 'Defaulter';}
			// -- Legal - 07.11.2018.
			if ($ExecApproval == 'LEGAL') { $status = 'Legal';}
			// -- Rehabilitation - 07.11.2018.
			if ($ExecApproval == 'REH') { $status = 'Rehabilitation';}

			echo "<input readonly style='height:30px' class=form-control margin-bottom-10' name='ExecApprovalDisplay' type='text'  placeholder='ExecApproval' value='$status'>";
			echo "<input type='hidden' readonly style='height:30px' class='form-control margin-bottom-10' name='ExecApproval' type='text'  placeholder='ExecApproval' value='$ExecApproval'>";
			}
			?>
			
		</div>
		</td>
</tr>	
<?php
$errorClass = "";
$formClass = "form-control margin-bottom-10";
$displayError = "";

if (!empty($rejectreasonError))
{			
	$displayError =  "<span class='help-inline'>$rejectreasonError</span>";
}

if(!empty($rejectreasonError))
{
	$errorClass = 'control-group error';
}
else
{
	$errorClass = 'control-group';
}

if ($ExecApproval == 'REJ') 
{ 	
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label>Rejection Reason</label>
		<input  style='height:30px' id='rejectreason' name='rejectreason'  class=$formClass type='text'  value='".$rejectreason ."'/>
		$displayError
	</div> 
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>
<!-- Buttons -->
						 <tr>
							<td>
							<div>
								<!--<a class="btn btn-primary" href="smeAuthenticate">Back</a>-->
								<?php if(empty($comingfrom)){$comingfrom = 'updatesme.php';} echo '<a class="btn btn-primary" href="'.$comingfrom.'?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded. '">Back</a>'; ?></div></td>
							</div>
							</td>
							<td>
							<div>
							  	<button type="submit" class="btn btn-primary">Update</button>
							</div>
							</td>
						</tr> 
						</table>
						</div>
					</form>
				</div>
				
    </div> <!-- /container -->
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  
</script> 				  	