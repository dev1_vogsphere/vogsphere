<?php 
require 'database.php';
$paymentmethoddescError = null;
$paymentmethoddesc = '';
$count = 0;
$paymentmethodid = 0;

if (isset($_GET['paymentmethodid']) ) 
{ 
$paymentmethodid = $_GET['paymentmethodid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$paymentmethoddesc = $_POST['paymentmethoddesc'];
	$valid = true;
	
	if (empty($paymentmethoddesc)) { $paymentmethoddescError = 'Please enter Payment Method Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE paymentmethod SET paymentmethoddesc = ? WHERE paymentmethodid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($paymentmethoddesc,$paymentmethodid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from paymentmethod where paymentmethodid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($paymentmethodid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$paymentmethoddesc = $data['paymentmethoddesc'];
}

?>
<div class="container background-white bottom-border">
<div class='margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change Payment Method
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Payment Method successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Payment Method</b><br />
			<input style="height:30px" type='text' name='paymentmethoddesc' value="<?php echo !empty($paymentmethoddesc)?$paymentmethoddesc:'';?>"/>
			<?php if (!empty($paymentmethoddescError)): ?>
			<span class="help-inline"><?php echo $paymentmethoddescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="paymentmethod">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>