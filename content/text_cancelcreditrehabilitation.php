<?php 
	require 'database.php';
	
	$customerid = null;
	$ApplicationId = null;
	$location = 'creditrehabilitationsales';
	// -- BOC Encrypt 16.09.2017.
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$dbname = "eloan";
	// -- EOC Encrypt 16.09.2017.

	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}
	
		if ( !empty($_GET['ApplicationId'])) {
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.		
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
	if ( !empty($_GET['customerid'])) {
		$id = $_REQUEST['customerid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$customerid = $_POST['customerid'];
		$ApplicationId = $_POST['ApplicationId'];

		$ExecApproval = 'CAN';
		$DateAccpt = date("Y-m-d");
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE loanapp";
			$sql = $sql." SET ExecApproval = ?, DateAccpt = ?";
			$sql = $sql." WHERE customerid = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ExecApproval,$DateAccpt,$customerid,$ApplicationId));
			Database::disconnect();
		// -- Update Debit Order Status -- //
			$statuscode = '2';
			$statusdesc = 'cancelled';
			auto_update_collectionstatus($ApplicationId,$statuscode,$statusdesc,$customerid);

			// -- Welcome SMS Template.
			$smstemplatename = 'crcancelsms';
			
			// -- Welcome Email Template.
			$emailtemplate = 'crcancelemail';
			
			// -- Send auto Welcome SMS. 
			auto_sms($customerid,$smstemplatename,$ApplicationId);
			
			// -- Send auto Welcome Email.
			auto_email($customerid,$emailtemplate,$ApplicationId);

			echo("<script>location.href = '".$location."';</script>");
		}
	   // -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	  else 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid,$ApplicationId));		
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(empty($data))
			{
				//header("Location: $location");
				echo $customerid.' - '.$ApplicationId;
			}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}	


		
?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Cancel a Credit Rehabilitation Sales Application
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="customerid" value="<?php echo $customerid;?>"/>
					  <input type="hidden" name="ApplicationId" value="<?php echo $ApplicationId;?>"/>
					  <p class="alert alert-error">Are you sure to cancel ?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href=<?php echo $location; ?>>No</a>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
