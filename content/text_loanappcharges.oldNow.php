<!-- Payment Schedule 2017.05.24 -->
<!----------- Runpaymentschedule.js -----------------> 
<script src="assets/js/Runpaymentschedule.js"></script>
<!-- End Payment Schedule -->
<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Loan Application Charges ------------------- //
  $sql = 'SELECT * FROM charge ORDER BY chargename';
  $dataCharges = $pdo->query($sql);						

// --------------------- Charge Option ------------------- //
  $sql = 'SELECT * FROM chargeoption ORDER BY chargeoptionname';
  $dataChargeOption = $pdo->query($sql);						   						 

//2. --------------------- Loan Application ------------------- //
  $sql = 'SELECT * FROM loanapp';
  $dataLoanApplications = $pdo->query($sql);	

  // -- Customers 
$ArrayCustomers = null;
  
//3. -- Get Customer Data name.
  $sql = 'SELECT * FROM customer';
  $dataCustomers = $pdo->query($sql);	

// -- Items 
$ArrayItems = null;
$indexItems = 0;
  
// -- Charge Option
$indexInside = 0;
$ChargeOption = '';

foreach($dataChargeOption as $rowChargeOption)
{
$ArraChargeOption[$indexInside][0] = $rowChargeOption['chargeoptionid'];
$ArraChargeOption[$indexInside][1] = $rowChargeOption['chargeoptionname'];
$indexInside = $indexInside + 1;
}			
$indexInside = 0;

  
//1. -- Customers - Execute Once.
	foreach($dataCustomers as $rowCustomer)
	{
	    $ArrayCustomers[$indexInside][0] = $rowCustomer['CustomerId'];
		$ArrayCustomers[$indexInside][1] = $rowCustomer['Title'];		
		$ArrayCustomers[$indexInside][2] = $rowCustomer['FirstName'];
		$ArrayCustomers[$indexInside][3] = $rowCustomer['LastName'];
		$indexInside = $indexInside + 1;
	}
	$indexInside = 0;
	
  Database::disconnect();	
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

 $tbl_name="loanappcharges"; // Table name 
 $applicationid = '';
 $chargename = '';
 $amount = '';
 $item = 0;
 
 $chargeid = '';
 $display = 'X'; // -- Display Payment Schedule Only.

 	if ( !empty($_POST)) 
	{
	   $chargeid = $_POST['chargeid'];
	   $applicationid = $_POST['applicationid'];
	   $item =  $_POST['item'];
	}
						   
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search for Loan Charges</h2>
                                    </div>
									
                                    <!-- <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                       <input style='height:30px;z-index:0' id='applicationid' name='applicationid' placeholder='Loan Application ID' class='form-control' type='text' value=$applicationid>
                                    </div>	 -->
                                    ";
	}  							
?>  
<p><b>Application ID</b><br /></p>
			<div class="controls">
					<div class="controls">
						<SELECT class="form-control" id="applicationid" name="applicationid" size="1" onChange="itemselect(this);" 
						onload="itemselect(this);">
						<?php
							$applicationidTemp = '';
							$applicationidSelect = $applicationid;
							$customerid = '';
							$customername = '';
							$ArrayApplications = null;
							$indexInside = 0;
							$status = '';
							$dateAccepted = '';
							
							echo "<OPTION value='' selected>All</OPTION>";
							
							foreach($dataLoanApplications as $row)
							{
								$applicationidTemp = $row['ApplicationId'];
								$customerid = $row['CustomerId'];

								$ArrayApplications[$indexInside][0] = $row['ApplicationId'];
								$ArrayApplications[$indexInside][1] = $row['CustomerId'];
								
								$status = $row['ExecApproval'];
								$dateAccepted = $row['DateAccpt'];
							
								// -- Get the title,firstname,surname.
								for ($i=0;$i<sizeof($ArrayCustomers);$i++)
								{
									if($ArrayCustomers[$i][0] == $row['CustomerId'])
									{
										$customername = $ArrayCustomers[$i][1].'.'.$ArrayCustomers[$i][2].' '.$ArrayCustomers[$i][3];
									}
								}
								
								if($applicationidTemp == $applicationidSelect)
								{
								 echo "<OPTION value=$applicationidTemp selected>$applicationidTemp - $customerid ($customername)- $status- $dateAccepted</OPTION>";
								}	
								else
								{
									echo "<OPTION value=$applicationidTemp>$applicationidTemp - $customerid ($customername) - $status- $dateAccepted</OPTION>";
								}
								$indexInside = $indexInside + 1;
							}
										
							if(empty($dataCharges))
							{
								echo "<OPTION value=0>No Applications</OPTION>";
							}
					?>
					</SELECT>
					</div>
</div>

<p><b>Payment Schedule(Item)</b><br />			
			<div class="control-group">
				<div class="controls">
					<SELECT class="form-control" id="item" name="item" size="1" onChange="selected(this)">	
						<OPTION value='' selected>All</OPTION>
					</SELECT>
					
					<SELECT class="form-control" id="itemArray" multiple name="itemArray[]" size="5" hidden>	
					
					</SELECT>
				</div>
			</div>
			
			</p> 
<label>Charges</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="chargeid" name="chargeid" size="1">
											<?php
											$chargeidTemp = '';
											$chargeidSelect = $chargeid;
											$chargename2 = '';
											$ArraCharge = null;
							$indexInside = 0;

							//1. -- Charges $dataCharges - Execute Once.
											
											// -- Add the All Option.
											echo "<OPTION value='' selected>All</OPTION>";
											
											foreach($dataCharges as $row)
											{
												$chargeidTemp = $row['chargeid'];
												$chargename2 = $row['chargename'];

												if($chargeidTemp == $chargeidSelect)
												{
												echo "<OPTION value=$chargeidTemp selected>$chargeidTemp - $chargename2</OPTION>";
												}
												//elseif($chargeidSelect == '')
												//{
												  //echo "<OPTION value='' selected>All</OPTION>";
												//}
												else
												{
												 echo "<OPTION value=$chargeidTemp>$chargeidTemp - $chargename2</OPTION>";
												}
												
														$ArraCharge[$indexInside][0] = $row['chargeid'];
														$ArraCharge[$indexInside][1] = $row['chargename'];
														$ArraCharge[$indexInside][2] = $row['chargeoptionid'];
														$ArraCharge[$indexInside][3] = $row['amount'];
														$indexInside = $indexInside + 1;
											}
										
											if(empty($dataCharges))
											{
												echo "<OPTION value=0>No Charges</OPTION>";
											}
											?>
										  </SELECT>
										</div></div>
<div>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>										
 </form>
                            </div>										
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				  <div class="table-responsive">

				
		              <?php 
		
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $chargeid = $_POST['chargeid'];
							 $applicationid = $_POST['applicationid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
				
					  if(!empty($applicationid))
					    {
						if($chargeid != '')
						{
						    if(!empty($item))
							{
								$sql = "SELECT * FROM $tbl_name WHERE chargeid = ? AND applicationid = ? AND item = ? ORDER BY applicationid ASC, item ASC";
								
								$q = $pdo->prepare($sql);
								$q->execute(array($chargeid,$applicationid,$item));	
								$data = $q->fetchAll();
							}
						  else
						    {
							    $sql = "SELECT * FROM $tbl_name WHERE chargeid = ? AND applicationid = ? ORDER BY applicationid ASC, item ASC";
								$q = $pdo->prepare($sql);
								$q->execute(array($chargeid,$applicationid));	
								$data = $q->fetchAll();
							}							
						}
						else
						{
							if(!empty($item))
							{
								$sql = "SELECT * FROM $tbl_name WHERE applicationid = ? AND item = ? ORDER BY applicationid ASC, item ASC";
								$q = $pdo->prepare($sql);
								$q->execute(array($applicationid,$item));	
								$data = $q->fetchAll(); 
							}
							else
							{
							    $sql = "SELECT * FROM $tbl_name WHERE applicationid = ? ORDER BY applicationid ASC, item ASC";
								$q = $pdo->prepare($sql);
								$q->execute(array($applicationid));	
								$data = $q->fetchAll(); 
							}
						}
						
					    }
						else
						{
						
							if($chargeid != '')
							{
								if(!empty($item))
							    {
									$sql = "SELECT * FROM $tbl_name WHERE chargeid = ? AND item = ? ORDER BY applicationid ASC, item ASC";
									$q = $pdo->prepare($sql);
									$q->execute(array($chargeid,$item));	
									$data = $q->fetchAll();
								}
								else
								{
									$sql = "SELECT * FROM $tbl_name WHERE chargeid = ? ORDER BY applicationid ASC, item ASC";
									$q = $pdo->prepare($sql);
									$q->execute(array($chargeid));	
									$data = $q->fetchAll();								
								}
							}
							else
							{	
							
								if(!empty($item))
							    {
								  // -- All Records
									$sql = "SELECT * FROM $tbl_name WHERE item = ? ORDER BY applicationid ASC, item ASC";
									$q = $pdo->prepare($sql);
									$q->execute(array($item));	
									$data = $q->fetchAll(); 
								}
								else
								{
									// -- All Records
									$sql = "SELECT * FROM $tbl_name ORDER BY applicationid ASC, item ASC";
									$q = $pdo->prepare($sql);
									$q->execute();	
									$data = $q->fetchAll(); 
								}
							}
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						if(!empty($data)){echo "<a class='btn btn-info' href=newloanappcharges.php>Add New Loan Charge</a> <br/> <p></p>"; }
						
						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>"; 
						echo "<tr>"; 
						echo "<td><b>Loan Application ID</b></td>"; 
						echo "<td><b>Payment Schedule Item</b></td>";
						echo "<td><b>Charge</b></td>"; 
						echo "<td><b>Charge Option</b></td>"; 
						echo "<td><b>Amount</b></td>";
						echo "<td><b>Charge Date</b></td>";
						echo "<th colspan='3'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						//foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						//1. -- Charge Types Names
							for ($i=0;$i<sizeof($ArraCharge);$i++)
							{
								if($ArraCharge[$i][0] == $row['chargeid'])
								{
									$chargename = $ArraCharge[$i][1];
									$amount = $ArraCharge[$i][3];
									// -- BOC - Get the charge option.
									// -- Charge Option Name
									for ($j=0;$j<sizeof($ArraChargeOption);$j++)
									{
										if($ArraChargeOption[$j][0] == $ArraCharge[$i][2])
										{
											$ChargeOption = $ArraChargeOption[$j][1];
										}
									}
									// -- EOC - Get the charge option.
								}
							}
							//"'". ."'"
$href = "'".'href=eInvoice.php?repayment=0&Duration=0&ApplicationId='.$row['applicationid'].'&FirstPaymentDate=0&ReqLoadValue=0&monthlypayment=0&DateAccpt=0&customername=0&customerid=0&interest=0&display='.$display."'";
								
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['applicationid']) . "</td>"; 
						echo "<td valign='top'>" . nl2br( $row['item']) . "</td>";  						
						echo "<td valign='top'>" . $chargename . "</td>";  
						echo "<td valign='top'>" . $ChargeOption . "</td>";  	
						echo "<td valign='top'>" . $amount. "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['chargedate']). "</td>";  	
						echo "<td valign='top'><a class='btn btn-success' href=editloanappcharges.php?applicationid={$row['applicationid']}&loanappchargesid={$row['loanappchargesid']}&chargeid={$row['chargeid']}&item={$row['item']}>Edit</a></td>
						
						<td valign='top'>";
						echo '<button type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="PaymentSchedule('.$href.')">Payment Schedule</button></td><td valign="top">';
						
						echo "<a class='btn btn-danger' href=deleteloanappcharges.php?loanappchargesid={$row['loanappchargesid']}>Delete</a></td> 
						</tr>"; 
						} 
						echo "</table>"; 
						echo "<a class='btn btn-info' href=newloanappcharges.php>Add New Loan Charge</a>"; 

					   Database::disconnect();
					  ?>
			
				</div>
<!------------------------------------------------------------------------------------------------------->
<!--------------------------------------------- BOC:Pop-up Modal Page --------------------------------------->
<!------------------------------------------------------------------------------------------------------->
			<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
				<div class="popup-inner"> <!--- Displays the Payment Schedule -->
				<div id="divPaymentSchedules" style="top-margin:100px;font-size: 10px;">
				</div>
				<p><a data-popup-close="popup-1" href="#">Close</a></p>
					<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
				</div>
			</div>			
<!------------------------------------------------------------------------------------------------------->
<!--------------------------------------------- EOC : Pop-up Modal Page --------------------------------->
<!------------------------------------------------------------------------------------------------------->				
    	</div>
<!---------------------------------------------- End Data ----------------------------------------------->

<!------------------------------------------------------------------------------------------------------->
<!------------------------------ BOC , PaymentSchedule Pop-up ------------------------------------------->
<!------------------------------------------------------------------------------------------------------->
<script  type="text/javascript">
// jQuery
 $(document).ready(function () {
 						var item = document.getElementById('item');
var itemID = item.options[item.selectedIndex].value; 

 //alert("Modise"+item.selectedIndex);
       itemselect(document.getElementById("applicationid"));
    });
 // -- Enter to capture comments   ---
 function itemselect(sel) 
 {
		var applicationid = sel.options[sel.selectedIndex].value;      
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectPaymentScheduleItems.php",
				       data:{applicationid:applicationid},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#item").html(data);
							jQuery("#itemArray").html(data);
							
						var earrings = document.getElementById('itemArray');
						earrings.style.display = 'none';//visibility
						
						// -- Only assist if all is selected.
						jQuery('#itemArray option').prop('selected', true);
						
						 }
					});
				});				  
			  return false;
  }
  
  function selected(sel) 
  {
	 var applicationid = sel.options[sel.selectedIndex].value;  
	 sel.selectedIndex  = sel.selectedIndex;
	 
	 //alert(sel.selectedIndex);
	 			  return false;

  }
  selected(this)
</script>

<!------------------------------------------------------------------------------------------------------->
<!------------------------------ EOC , PaymentSchedule Pop-up ------------------------------------------->
<!------------------------------------------------------------------------------------------------------->				