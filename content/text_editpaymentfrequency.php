<?php 
require 'database.php';
$paymentfrequencydescError = null;
$paymentfrequencydesc = '';
$count = 0;
$paymentfrequencyid = 0;

if (isset($_GET['paymentfrequencyid']) ) 
{ 
$paymentfrequencyid = $_GET['paymentfrequencyid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$paymentfrequencydesc = $_POST['paymentfrequencydesc'];
	$valid = true;
	
	if (empty($paymentfrequencydesc)) { $paymentfrequencydescError = 'Please enter Payment Frequency Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE paymentfrequency SET paymentfrequencydesc = ? WHERE paymentfrequencyid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($paymentfrequencydesc,$paymentfrequencyid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from paymentfrequency where paymentfrequencyid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($paymentfrequencyid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$paymentfrequencydesc = $data['paymentfrequencydesc'];
}

?>
<div class="container background-white bottom-border"> 
<div class='margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change Payment Frequency
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Payment Frequency successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Payment Frequency</b><br />
			<input style="height:30px" type='text' name='paymentfrequencydesc' value="<?php echo !empty($paymentfrequencydesc)?$paymentfrequencydesc:'';?>"/>
			<?php if (!empty($paymentfrequencydescError)): ?>
			<span class="help-inline"><?php echo $paymentfrequencydescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="paymentfrequency">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>