<?php 
require 'database.php';
// -- Functions File.
require 'functions.php';
$display = 'X';
$emailFlag = 'X';

// -- Runpaymentschedule.php
require 'Runpaymentschedule.php';

// -- paycollections functions
//$path = "fintegrate/PayCollection/functions.php";
//require $path;

$backLinkName = 'creditrehabilitationsales';
$backLinkLead = 'leads';

$LoanDuration  = 1;
$ReqLoadValue = 0;
$Repayment = 0;

$employerphoneError ='';
$employernameError  ='';
$salaryError        ='';
$salarydateError    ='';
$occupationError    ='';
$customerid = '';
$nextofkinameError  ='';
$nextofkinphoneError='';

// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- CR -- 18.01.2019 -- //
$creditcheck = '';
$debitorder  = '';
$marketinformartion = '';
$leadid = '';

// -- Read Customer Details. -- //
	if ( !empty($_GET['leadid'])) 
	{
		$leadid = $_REQUEST['leadid'];
	}

$employername = '';
$employerphone = '';
$salary = '';
$otherincome = '';
$salarydate = '';

$spousename = '';
$spousephone = '';
$nextofkiname = '';
$nextofkinphone = '';
$occupation		= '';	
// -- CR -- 18.01.2019 -- //

$password1 = '';
$count=0;
$idnumber  = '';
$tbl_user="user"; // Table name 
$tbl_customer ="customer"; // Table name
$tbl_loanapp ="loanapp"; // Table name
$db_eloan = "ecashpdq_eloan";
$sendemail = '';

$CustomerIdReadonly = '';
// -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 

  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		//if($row['loantypeid'] == 1) // -- 27.01.2017
		if($row['loantypedesc'] == 'Rehabilitation') // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
			$_SESSION['loantypeid'] = $row['loantypeid'];// -- Loan Type ID
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];
	// -- Loan Type ID    
	$loantypeid = $_SESSION['loantypeid'];

	Database::disconnect();
// -- EOC 27.01.2017


// -- BOC 27.01.2017
$personalloan = $_SESSION['personalloan'];
$loantype = $_SESSION['loantypedesc'];
$currency = $_SESSION['currency'];// -- 10.02.2017
// -- EOC 27.01.2017

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 
  $currency = "";
  
  // -- BOC Loan Durations (30.06.2017)
  $fromduration = 1;
  $toduration = 1;
  $fromamount = 0.00;
  $toamount = 0.00;
  $fromdate = '';
  $todate = '';
  $qualifyfrom = 1.00;
  $qualifyto = 1.00;
  // -- EOC Loan Durations (30.06.2017)
  $changedby = '';
  $status = '';

   // -- Read SESSION createdby.
 if(isset($_SESSION['username']))
 {
	$changedby = $_SESSION['username'];
 }
  
  $createdon = date('Y-m-d');
  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		//if($row['loantypeid'] == 1) // -- 27.01.2017
		if($row['loantypedesc'] == 'Rehabilitation') // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
			$_SESSION['currency'] = $row['currency'];// -- 10.02.2017
			$_SESSION['loantypeid'] = $row['loantypeid'];
			
			// -- BOC Loan Durations (30.06.2017)
			$fromduration = $row['fromduration'];
			$toduration = $row['toduration'];
			$fromamount = $row['fromamount'];
			$toamount = $row['toamount'];
			$fromdate = $row['fromdate'];
			$todate = $row['todate'];
			$qualifyfrom = $row['qualifyfrom'];
			$qualifyto = $row['qualifyto'];
			$ReqLoadValue = $row['fixedamount'];
			$repayment = $ReqLoadValue; 
			// -- EOC Loan Durations (30.06.2017)
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];
	$currency = $_SESSION['currency'];// -- 10.02.2017

// -- EOC 27.01.2017
// -- There must be only one rehabiltation applied for by a client. 1 to 1.
		$whereCustomer	 = array();
		if(!empty($leadid))
		{
			$whereCustomer[] = "leadid = {$leadid}";
			$dataRehabilitationDone = GetCustomerDetails($whereCustomer);
			if(!empty($dataRehabilitationDone)){$idnumber = $dataRehabilitationDone['CustomerId'];}
		}
		// --  Search to check if Credit Rehabilitation Application exist in the system for the client.
		$whereDupCheck	 = array();
		$loantypeid = '';
		$loantypeid = getsession('loantypeid');
		if(!empty($idnumber) && !empty($loantypeid))
		{	
			$whereDupCheck[] = "CustomerId = {$idnumber}";
			$whereDupCheck[] = "loantypeid = {$loantypeid}";
			$dataRehabilitation = search_dynamic_loanapp($whereDupCheck);
			
			$location = "Location: leads";
			if(!empty($dataRehabilitation))
			{
				header($location);
			}
		}
// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

// ----------------------  Declarations --------------
// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$DobError = null;
		$SuretyError = null;
		$firstpaymentError = null;
		$password1Error = null;
		$password2Error = null;


				
		// keep track validation errors - 		// Loan Details
		$MonthlyExpenditureError = null; 
		$ReqLoadValueError = null; 
		$DateAccptError = null; 
		$LoanDurationError = null; 
		$StaffIdError = null; 
		$InterestRateError = null; 
		$ExecApprovalError = null; 
		$MonthlyIncomeError = null; 

		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //
		
		// keep track post values
		// Customer Details
		$Title = '';
		$idnumber = '';
		$FirstName = '';
		$LastName = '';
		$phone = '';
		$email = '';
		$Street = '';
		$Suburb = '';
		$City = '';
		$State = '';
		$PostCode = '';
		$Dob		= '';
		$Surety		= '';
		$firstpayment = '';
		$loantypeid = 1;
		$StaffId = 1;
		// -------------------------  Banking Details - Initial Values ------------------------------------- //
		$AccountHolder = '';
		$AccountType = '';
		$AccountNumber = '';
		$BranchCode = '';
		$BankName = '';
		// -------------------------  Banking Details - Initial Values ------------------------------------- //
// -- EOC T.M Modise 20.06.2017
// -- Add Immediate Transfer Charge
// -- Add Insurance Charges
$immediatetransferfee = ''; // -- CheckBox.
$charge_fee_id = 1;			// -- Default in the system.
$dataImmediateTransfer = '';


$loanprotecionplan = '';   // -- CheclBox.
$charge_protecion_id = 4;  // -- Default in the system.
$dataProtectionplan = '';

// -- Read the config charges on the table.
// -- Immediate Transfer Fees.
		$sql =  "select * from charge where chargeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($charge_fee_id));
		$dataImmediateTransfer = $q->fetch(PDO::FETCH_ASSOC);
// -- Loan Protection Plan.
		$sql =  "select * from charge where chargeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($charge_protecion_id));
		$dataProtectionplan = $q->fetch(PDO::FETCH_ASSOC);
// -- EOC T.M Modise 20.06.2017

// ---------------------- End Declarations --------------
	$changedby = $_SESSION['username'];
	$changedon = date('Y-m-d');

	$createdby = $_SESSION['username'];
	$createdon = date('Y-m-d');

	$sql =  "select * from lead WHERE leadid = ?";	
	$q = $pdo->prepare($sql);
	$q->execute(array($leadid));
	$dataLeadDetails = $q->fetch(PDO::FETCH_ASSOC);
// -- Read Lead Details. -- //
	if(!empty($dataLeadDetails))
	{		
			// Title
			$Title = $dataLeadDetails['title'];	
			// First name
			$FirstName = $dataLeadDetails['name'];
			// Last Name
			$LastName = $dataLeadDetails['surname'];
			// Phone numbers
			$phone = $dataLeadDetails['phone'];
			// Email
			$email = $dataLeadDetails['email'];
			
			// -- status
			$status = $dataLeadDetails['status'];
			
			// -- comments
			$comments  = $dataLeadDetails['comments'];
			$changedby = '';
			$changedon = date('Y-m-d');
			$customerid = $dataLeadDetails['customerid'];
			if(!empty($customerid ))
			{
				$idnumber = $customerid; 
				$CustomerIdReadonly = 'readonly';
			}
	}		
// -- Read Customer Details. -- //
	$sql =  "select * from customer WHERE leadid = ?";	
	$q = $pdo->prepare($sql);
	$q->execute(array($leadid));
	$dataDetails = $q->fetch(PDO::FETCH_ASSOC);
	if(!empty($dataDetails))
	{		
			// Street
			$Street = $dataDetails['Street'];

			// Suburb
			$Suburb = $dataDetails['Suburb'];

			// City
			$City = $dataDetails['City'];
			// State
			$State = $dataDetails['State'];
			
			// PostCode
			$PostCode = $dataDetails['PostCode'];
			
			// Dob
			$Dob = $dataDetails['Dob'];
			
			// -- Bankname
			$BankName = $dataDetails['bankname'];
			
			// -- Account Holdername
			
			// -- Account number 
			
			// -- Account Type
			// ------------------------- $_POST - Banking Details ------------------------------------- //
			$AccountHolder = $dataDetails['accountholdername'];
			$AccountType = $dataDetails['accounttype'];
			$AccountNumber = $dataDetails['accountnumber'];
			$BranchCode = $dataDetails['branchcode'];
			$BankName = $dataDetails['bankname'];
			$idnumber = $dataDetails['CustomerId'];
			
			$nextofkiname		= $dataDetails['nextofkiname'];
			$nextofkinphone    =  $dataDetails['nextofkinphone'];
			$spousename        =  $dataDetails['spousename'];
			$spousephone       =  $dataDetails['spousephone'];
			$employername      =  $dataDetails['employername'];
			$employerphone     =  $dataDetails['employerphone'];
			$occupation        =  $dataDetails['occupation'];
			$creditcheck       =  $dataDetails['creditcheck'];
			$debitorder        =  $dataDetails['debitorder'];
			$marketinformartion=  $dataDetails['marketinformartion'];
			$salary            =  $dataDetails['salary'];
			$salarydate        =  $dataDetails['salarydate'];
			$otherincome       =  $dataDetails['otherincome'];
			
			$CustomerIdReadonly = 'readonly';

	}			
// -- Company Name:
$companyname = "";
if(isset($_SESSION['companyname']))
{
	$companyname = $_SESSION['companyname'];
}
else
{
	$companyname = "Vogsphere (PTY) LTD";
}

			
 if (!empty($_POST)) 
	{
		$count = 0;
// -- CR 18.01.2019 -- //
// -- Read Customer Details. -- //
	if ( !empty($_GET['leadid'])) 
	{
		$leadid = $_REQUEST['leadid'];
	}

  $status = 'finalised';
  
		// creditcheck
if(isset($_POST['creditcheck']))
{
	$creditcheck = $_POST['creditcheck'];
}
else
{
	$creditcheck = '';
}
//debitorder
if(isset($_POST['debitorder']))
{
	$debitorder = $_POST['debitorder'];
}
else
{
	$debitorder = '';
}
//marketinformartion
if(isset($_POST['marketinformartion']))
{
	$marketinformartion = $_POST['marketinformartion'];
}
else
{
	$marketinformartion = '';
}

$employername 		 = $_POST['employername'];
$employerphone 		 = $_POST['employerphone'];
$salary 			 = $_POST['salary'];
$otherincome 		 = $_POST['otherincome'];
$salarydate 		 = $_POST['salarydate'];

$spousename 		 = $_POST['spousename'];
$spousephone 		 = $_POST['spousephone'];
$nextofkiname 		 = $_POST['nextofkiname'];
$nextofkinphone 	 = $_POST['nextofkinphone'];
$occupation 	 = $_POST['occupation'];
$comments  	= $_POST['comments'];
	$changedby = $_SESSION['username'];
	$changedon = date('Y-m-d');
	
	$createdby = $_SESSION['username'];
	$createdon = date('Y-m-d');
	
		// -- CR 18.01.2019 -- //
		
		
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$DobError = null;
		$SuretyError = null;
		$firstpaymentError = null;
		$password1Error = null;
		$password2Error = null;


				
		// keep track validation errors - 		// Loan Details
		$MonthlyExpenditureError = null; 
		$ReqLoadValueError = null; 
		$DateAccptError = null; 
		$LoanDurationError = null; 
		$StaffIdError = null; 
		$InterestRateError = null; 
		$ExecApprovalError = null; 
		$MonthlyIncomeError = null; 

		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //
		$employerphoneError	 =null;
		$employernameError    =null;
		$salaryError          =null;
		$salarydateError      =null;
		$occupationError      =null;
		$nextofkinameError    =null;
		$nextofkinphoneError  =null;

		// keep track post values
		// Customer Details
		$Title = $_POST['Title'];
		$idnumber = $_POST['idnumber'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		$Dob		= $_POST['Dob'];
		$Surety		= '';//$_POST['Surety'];
		$firstpayment = $_POST['FirstPaymentDate'];
		$StaffId = 1;
		
		// -- EOC T.M Modise 20.06.2017
		// -- Add Immediate Transfer Charge
		// -- Add Insurance Charges
		// -- Loan Type Must be the choosen one.
		$loantypeid = $_POST['loantype'];
		
		// --  Immediatetransferfee - CheckBox
		if(isset($_POST['immediatetransferfee']))
		{$immediatetransferfee = $_POST['immediatetransferfee'];}
		else 
		{$immediatetransferfee = '';} 
		
		// -- Loanprotecionplan - CheckBox
		if(isset($_POST['loanprotecionplan']))
		{$loanprotecionplan = $_POST['loanprotecionplan'];}
		else
		{$loanprotecionplan = '';}
		
		// -- EOC T.M Modise 20.06.2017
		
		if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
		{
		  $password1 = $_POST['password1'];
		}
		$monthlypayment = $_POST['monthlypayment'];

		// Loan Details
		$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
		$ReqLoadValue = $_POST['ReqLoadValue'];
		$DateAccpt  = date('Y-m-d');
		$LoanDuration  = $_POST['LoanDuration'];
		$InterestRate = $_POST['InterestRate'];
		$ExecApproval  = "REH";
		
		$MonthlyIncome  = $_POST['MonthlyIncome'];
		$Repayment = $_POST['Repayment'];
		$paymentmethod = $_POST['paymentmethod'];
		$FirstPaymentDate = $_POST['FirstPaymentDate'];
		$lastPaymentDate = $_POST['lastPaymentDate'];
		$loantype = $_POST['loantype'];
		$paymentfrequency = $_POST['paymentfrequency'];

		$TotalAssets = 0;
		$LoanValue = 0;
		
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);

		// ------------------------- $_POST - Banking Details ------------------------------------- //
		$AccountHolder = $_POST['AccountHolder'];
		$AccountType = $_POST['AccountType'];
		$AccountNumber = $_POST['AccountNumber'];
		$BranchCode = getpost('BranchCode');
		$BankName = $_POST['BankName'];
		// -------------------------------- Banking Details ------------------------------------------ //
// validate input - 		// Customer Details
		$valid = true;
		// ----------------------------------- BOC - 01.07.2017 - From Date and End Date ------------- //
		/*$toduration = $row['toduration'];
		$fromamount = $row['fromamount']
		$toamount = $row['toamount']
		$fromdate = $row['fromdate'];*/
		$fromamount = $_POST['fromamount'];
		$toamount = $_POST['toamount'];
		$qualifyfrom = $_POST['qualifyfrom'];
		$qualifyto = $_POST['qualifyto'];
		// ----------------------------------- EOC - 01.07.2017 - From Date and End Date ------------- //
		// --  Search to check if Credit Rehabilitation Application exist in the system for the client.
		$whereDupCheck	 = array();
		$loantypeid = '';
		$loantypeid = getsession('loantypeid');
		if(!empty($idnumber) && !empty($loantypeid))
		{   $whereDupCheck = null;
			$whereDupCheck[] = "CustomerId = {$idnumber}";
			$whereDupCheck[] = "loantypeid = {$loantypeid}";
			
			//print_r($whereDupCheck);
			$dataRehabilitationDone = search_dynamic_loanapp($whereDupCheck);
			//print_r($dataRehabilitationDone->rowCount());
			
			if($dataRehabilitationDone->rowCount()> 0)
			{
				$valid=false;
				$count = -2;
			}
		}
		
		// -- EOC Search Duplicate.
		if(empty($employerphone)){$employerphoneError ='Please enter employer name';$valid=false;}
		if(empty($employername)){$employernameError  ='Please enter employer phone';$valid=false;}
		if(empty($salary)){$salaryError        ='Please enter salary';$valid=false;}
		if(empty($salarydate)){$salarydateError    ='Please enter salary date';$valid=false;}
		if(empty($occupation)){$occupationError    ='Please enter occupation';$valid=false;}
//if(empty($nextofkiname)){$nextofkinameError  ='Please enter next of kin';$valid=false;}
//if(empty($nextofkinphone)){$nextofkinphoneError='Please enter next of kin phone';$valid=false;}
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($idnumber,2,2) > 12) 
		{ 
			$idnumberError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{
		// Day from 1 - 30
			if (in_array(substr($idnumber,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($idnumber,4,2) > 30)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($idnumber,2,2),$months31)) 

			{
				// Day 
			   if (substr($idnumber,4,2) > 31)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($idnumber,2,2),$monthsFeb)) 
			{
				// Leap Year
				//$yearValidate = parseInt(substr($idnumber,0,2));
				
				$val = substr($idnumber,0,2);
				
				// Leap Year
				$yearValidate = $val;//parseInt(substr($idnumber,0,2));
				
				$leapyear = ($yearValidate % 4 == 0);
				
				if($leapyear == true)
				{
					// Day 
			   if (substr($idnumber,4,2) > 29)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($idnumber,4,2) > 28)
					   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		// Phone number must be characters
		if (!is_Numeric($phone)) { $phoneError = 'Please enter valid phone numbers - digits only'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}
		
		// is Number
		if (!is_Numeric($PostCode)) { $PostCodeError = 'Please enter valid Postal Code - numbers only'; $valid = false;}

		// Is 4 numbers
		if (strlen($PostCode) > 4) { $PostCodeError = 'Please enter valid Postal Code - 4 numbers'; $valid = false;}
		
		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter/select Date of birth'; $valid = false;}
		
		//if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($firstpayment)) { $firstpaymentError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'; $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter application charge amount'; $valid = false;}
		// ------------------------------ BOC 01.07.2017 ------------------------------ //
		// -- Determine the loan amount, one may request via the 
		// -- loan type: amount from to amount to.
		if($valid)
		{
			if (($ReqLoadValue >= $fromamount) && ($ReqLoadValue <= $toamount))
			{
				// -- "is between"
			}
			else
			{
				//$ReqLoadValueError = "You have exceed the maximum charging amount of $currency $toamount from $fromamount"; 
				$valid = false; 
			}
		
			if(!empty($firstpayment))
			{// -- FirstPaymentDate cannot be in past.
				$date1=date_create(date('Y-m-d'));// Today's date. 
				$date2=date_create($firstpayment);// FirstPaymentDate

				$diff=date_diff($date1,$date2);
				$value = $diff->format("%R%a");
				
			// -- if its negative means first payment date is in the past.
				if($value < 0)
				{
					 $firstpaymentError = 'Please enter/select first payment date in future/present.'; 
					 $valid = false;
				}
				else
				{
		  // -- if its postive means first payment date is in the future/present.
				}
			}
		}
		// ------------------------------ EOC 01.07.2017 ------------------------------ //

		//if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		if (empty($DateAccpt)) { $DateAccptError = 'Please enter Date Accpt'; $valid = false;}
				
		// Loan duration is numeric.
		if (!is_Numeric($LoanDuration)) { $LoanDurationError = 'Please enter a number for Loan Duration'; $valid = false;}

		if (empty($LoanDuration)) { $LoanDurationError = 'Please enter Loan Duration'; $valid = false;}
$InterestRate = 0;
		//if (empty($InterestRate)) { $InterestRateError = 'Please enter Interest Rate'; $valid = false;}
		
		// -- Validate Password Only if not loggedin
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
			 // -- nothing
			}
			else
			{
			
			//if (empty($password2)) { $password2Error = 'Please confirm password'; $valid = false;}
			
			//if($password1 != $password2) {$password1Error = 'Password does not match'; $valid = false; }

			}
	
		// Insert e-Loan Application Data.
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
			if ($valid) 
			{
				$count = 0;
			   
			   // -- Db connect.
				$pdoDB = Database::connect();

			   // -- Check he Funder exist as a customer, if not create as customer.
				$check_user = "SELECT * FROM $tbl_customer WHERE CustomerId=?";
	 			
			  // -- sql..	
				$q = $pdo->prepare($check_user);
				$q->execute(array($idnumber));
				$result = $q->fetch(PDO::FETCH_ASSOC);
				$count = $q->rowCount();
				
			  // -- 01.05.2018
				if($count < 1)
				{
					// -- CR -- 18.01.2019 -- //
								if(empty($Dob))
			{
				$Dob = '9999-01-01';
			}
			
			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,Street,Suburb,City,State,PostCode,Dob,phone2,
accountholdername,
bankname,
accountnumber,
branchcode,
accounttype,email2,createdby,createdon,changedby,changedon,status
,comments		  
,nextofkiname		
,nextofkinphone		
,spousename			
,spousephone			
,employername		
,employerphone		
,occupation			
,creditcheck			
,debitorder			
,marketinformartion
,salary				
,salarydate			
,otherincome,leadid       
)";
$today = date('Y-m-d');
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$email,$createdby,$createdon,$changedby,$changedon,$status
			,$comments		  
			,$nextofkiname		
			,$nextofkinphone		
			,$spousename			
			,$spousephone		
			,$employername		
			,$employerphone		
	        ,$occupation			
            ,$creditcheck		
            ,$debitorder			
            ,$marketinformartion
	        ,$salary				
			,$salarydate			
			,$otherincome,$leadid ));      
			$count = $count + 1;
			$message = $idnumber;
				    // -- CR -- 18.01.2019 -- //			
				}
			else // -- Update customer details -- //
			{
				
									if(empty($Dob))
			{
				$Dob = '9999-01-01';
			}
			
			$sql = "UPDATE $tbl_customer SET Title = ?,FirstName = ?,LastName = ?,Street = ?,Suburb = ?,City = ?,State = ?,PostCode = ?,Dob = ?,phone2 = ?,
accountholdername = ?,
bankname = ?,
accountnumber = ?,
branchcode = ?,
accounttype = ?,email2 = ?,createdby = ?,createdon = ?,changedby = ?,changedon = ?,status = ?,comments = ?,nextofkiname = ?,nextofkinphone = ?,spousename = ?,spousephone = ?,employername		
 = ?,employerphone = ?,occupation = ?,creditcheck = ?,debitorder = ?,marketinformartion = ?,salary = ?,salarydate = ?,otherincome = ?      
WHERE CustomerId = ?";
$today = date('Y-m-d');
			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$email,$createdby,$createdon,$changedby,$changedon,$status
			,$comments		  
			,$nextofkiname		
			,$nextofkinphone		
			,$spousename			
			,$spousephone		
			,$employername		
			,$employerphone		
	        ,$occupation			
            ,$creditcheck		
            ,$debitorder			
            ,$marketinformartion
	        ,$salary				
			,$salarydate			
			,$otherincome,$idnumber));      
			// -- Update customer details  -- //
			}
			// -- EOC Funder applying for a loan. 13.08.2017
			// Insert Application Loan	
			$sql = "INSERT INTO $tbl_loanapp(CustomerId,loantypeid,MonthlyIncome,MonthlyExpenditure,TotalAssets,ReqLoadValue,ExecApproval,";
			$sql = $sql."DateAccpt,StaffId,InterestRate,LoanDuration,LoanValue,surety,Repayment,paymentmethod,FirstPaymentDate,lastPaymentDate,monthlypayment,paymentfrequency,accountholdername,bankname,accountnumber,branchcode,accounttype,createdby,createdon)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //24
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$loantypeid,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,$ReqLoadValue,$ExecApproval,$DateAccpt,$StaffId,$InterestRate,$LoanDuration,$LoanValue,$Surety,$Repayment,$paymentmethod,$FirstPaymentDate,$lastPaymentDate,$monthlypayment,$paymentfrequency,
			$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$createdby,$createdon));//24
			
			// -- Update Lead Status to close.
			$status = 'closed';
			$sql = 
			"UPDATE lead SET 
			 status		= ?,
			 changedby = ?,
			 changedon = ?
			 where leadid = ?"; 
			$q = $pdo->prepare($sql);
			$q->execute(array($status,$createdby,$createdon,$leadid));      
			// -- Update Status.
			
			/*
				getpaymentschedule	- exist
				create payment schedule - exist
				getpaymentschedule	- exist
				do_debitorderschedule - new 
			*/
			// -- 0. get Recent ApplicationID created for customerid.
			$ApplicationID = GetRecentApplicationID($idnumber);
			//echo 'ApplicationID :'.$ApplicationID; 
			// -- 1. getpaymentschedule
			$dataPaymentSchedules = null;
			$dataPaymentSchedule = null;
			$dataPaymentSchedules = GetPaymentSchedule($ApplicationID);
			// -- 2. if does not exist create PaymentSchedule($ApplicationId,$disbursementdate,$disbursement,$repayment,$amountdue,$scheduleddate,$Duration, $interest,$customerid,$customername,$display,$indicator) -- //
			if(empty($dataPaymentSchedules) && $ApplicationID != 0)
			{
				$display= ''; $indicator = '';
				$disbursementdate=$createdon;$disbursement=0.00;$amountdue=$monthlypayment;$repayment=$ReqLoadValue;$scheduleddate=$FirstPaymentDate;$Duration=$LoanDuration;
				$interest=$InterestRate;$customername=$FirstName.' '.$LastName;
				PaymentSchedule($ApplicationID,$disbursementdate,$disbursement,$repayment,$amountdue,$scheduleddate,$Duration, $interest,$idnumber,$customername,$display,$indicator);		
			// -- 3. Get the paymentschedules to use to create debitorder schedule --//
				$dataPaymentSchedules = GetPaymentSchedule($ApplicationID);
			 }
			// -- 4. Check debitorder schedule - function readPaymentschedule($customerid,$applicationid,$item)
			//print_r($dataPaymentSchedules);
			if(!empty($dataPaymentSchedules) && $ApplicationID != 0)
			{ $index = 1;
				foreach($dataPaymentSchedules as $row)
				{
					//print('ApplicationId : '.$row['ApplicationId'].'id: '.$idnumber.' itm:'.$row['item']);
				  if((int)$row['item'] >= 0)
				  {
					$dataPaymentSchedule = readPaymentschedule($idnumber,$row['ApplicationId'],$row['item']);
					
					if(empty($dataPaymentSchedule['Client_ID']))
					{
						///print('Client_ID We In:'.$dataPaymentSchedule['Client_ID']);

						$debitorderscheduleStatus = '1';
						$debitorderscheduleDesc = 'pending';
						$companyname  = 'ECASHMEUP'; // -- Bank Reference.
						$IntCode  = getsession('IntUserCode');
						$UserCode = getsession('UserCode');			
						if(empty($IntCode)){$IntCode='CRDB';}
						if(empty($UserCode)){$UserCode='UFEC';}		
				
			// -- 5. if does not exist do debitorder schedule - function createcollections($arrayCollections)
						$oneDimensional = null;
						$oneDimensional[1] = $IntCode;  							 // -- CustomerID
						$oneDimensional[2] = GenerateCRAccountNumber($idnumber);     // -- Client_Reference_1
						$oneDimensional[3] = '';									 // -- Client_Reference_2
						$oneDimensional[4] = 'South African';			     		 // -- Client_ID_Type
						$oneDimensional[5] = $idnumber;       				     	 // -- Client_ID_No
						$oneDimensional[6] = substr($FirstName,0, 1);	     		 // -- Initials
						$oneDimensional[7] = $AccountHolder;  						 // -- Account_Holder
						$oneDimensional[8] = $AccountType;		 					 // -- Account_Type
						$oneDimensional[9]  = $AccountNumber;  				         // -- Account_Number
						$oneDimensional[10] = $BranchCode;  					 // -- Branch_Code.
						$oneDimensional[11] = $index;		 			 		 // -- No_Payments.
						$oneDimensional[12] = 'Debit Order';  	 					 // -- Service_Type.
						$oneDimensional[13] = 'NADEO';  	 						 // -- Service_Mode.
						$oneDimensional[14] = 'Monthly';		 			 		 // -- Frequency.
						$oneDimensional[15] = $row['scheduleddate'];  	 			 // -- Action_Date.
						$oneDimensional[16] = $companyname;      					 // -- Bank_Reference.
						$oneDimensional[17] = $row['amountdue'];  	 				 // -- Amount.
						$oneDimensional[18] = $debitorderscheduleStatus;  		 	 // -- Status_Code
						$oneDimensional[19] = $debitorderscheduleDesc; 		 		 // -- Status_Description
						$oneDimensional[20] = $row['ApplicationId']; 	 			 // -- ApplicationId
						$oneDimensional[21] = $row['item']; 	 					 // -- Item														
						$oneDimensional[22] = $IntCode; 	 					 	 // -- IntCode	
						$oneDimensional[23] = $UserCode; 	 					 	 // -- UserCode	
						createcollections($oneDimensional);						
						$index = $index + 1;
					}
				}
			 }	
			}		
			$message = $Title.' '.$FirstName.' '.$LastName;
			$count = 1; 
			Database::disconnect();			 
			$count++;
			}

			// -- Welcome SMS Template.
			$smstemplatename = 'Credit Rehabilitation';
			
			// -- Welcome Email Template.
			$emailtemplate = 'crwelcome';
			
			// -- Send auto Welcome SMS. 
			auto_sms($idnumber,$smstemplatename,$ApplicationID);
			
			// -- Send auto Welcome Email.
			auto_email($idnumber,$emailtemplate,$ApplicationID);

			// -- Debi Order Notifications -- //
			// -- Debit Order SMS Template.
			$smstemplatename = 'crdebitordernotification';
			
			// -- Debit Order Email Template.
			$emailtemplate = 'crdebitordernotificationemail';
			
			// -- Send auto Debit Order SMS. 
			auto_sms($idnumber,$smstemplatename,$ApplicationID);
			
			// -- Send auto Debit Order Email.
			auto_email($idnumber,$emailtemplate,$ApplicationID);
			
			// -- Indicate email has been sent.
			$sendemail = 'yes';
			
			Database::disconnect();
			
			$ApplicationID = GetRecentApplicationID($idnumber);
			
			echo '<script>alert($ApplicationID)</script>';				
		}
		else
		{
			if($count != -2)
			{$count = -1;}
		} 
}
else
{
// -- Default Bankname - ABSA
// --------------------- BOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
  if(empty($BankName))
  {
	  $BankName = "ABSA";
  }
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //  
}

?>
<div class="container background-white bottom-border"> 
                    <div class="margin-vert-30">
                        <!-- Register Box -->
                        <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                              <form METHOD="post" class="signup-page" action=<?php echo "".$_SERVER['REQUEST_URI']."";?>> 
                                <div class="signup-header">
                                    <h2 class="text-center"><?php if(empty($companyname)){echo 'e-';}else{echo $companyname;}?> Application</h2>
									<p>Please complete all fields NOTE: <span style="color:red">*</span> = Required Field</p>
                                    <!-- <p>Already a member? Click
                                        <a href="customerLogin.php">HERE</a>to login to your account.</p> -->
									<?php # error messages
									/*if (isset($message)) 
									{
										foreach ($message as $msg) {
											printf("<p class='status'>%s</p></ br>\n", $msg);
										}
									}*/
									
									# success message
									if($count !=0)
									{
										# error message
										if($count == -1)
										{
											printf("<div class=alert alert-warning'>
												<strong>Warning!</strong>
												<p class='alert alert-warning'>Application Error Below!</p></div>\n", $count);
										# error message
										}
										else if($count == -2)
										{
												printf("<div class=alert alert-warning'>
												<strong>Warning!</strong>
												<p class='alert alert-warning'>Credit Rehabilitation Application already exist!</p></div>\n", $count);
										}else
									   # success message
										{  if(empty($sendemail)){
										printf("<p class='status'> Application captured successfully!</p>\n", $count);}
											$CustomerIdReadonly = 'readonly';
										}
									}
									else
									{
										

									}
								   if ($count !=0) 
								   {
									   if($count != -1)
										{
											if($sendemail == 'yes')
											{
											   printf("<p class='status'>Your application is successfully registered.<br/> An e-mail & sms has been sent!</p>\n");
											}
											elseif($sendemail == 'no')
											{
											   printf("<p class='status'>Your application was successfully registered. but your e-mail address seems to be invalid!</p>\n");
											}
										}
								    }
									?>
                                </div>
					<!-- BOC - Personal Information Details -->				
								<!-- Start - Accordion - Alternative -->
						<div class="tab-pane fade in" id="sample-3b">									
							<div class="panel <?php	if(!empty($idnumberError)){ echo 'panel-danger';}
														else if(!empty($FirstNameError)){ echo 'panel-danger';}
														else if(!empty($LastNameError)){ echo 'panel-danger';}
														else if(!empty($DobError)){ echo 'panel-danger';}
														else{ echo 'panel-default';}?>">	
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayRegister" href="javascript:toggleRegister();">
                                                Personal Information Details
                                         </a>
                                    </h2>		
								</div>
								<div id="toggleRegister" style="display: <?php	if(!empty($idnumberError)){ echo 'block';}
														else if(!empty($FirstNameError)){ echo 'block';}
														else if(!empty($LastNameError)){ echo 'block';}
														else if(!empty($DobError)){ echo 'block';}
														else{ echo 'block';}?>">		
                                <label>Title</label>			
								<SELECT  class="form-control" id="Title" name="Title" size="1">
									<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
									<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
									<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
									<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
									<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
								</SELECT>
								
								<br/>
                                <!-- <input class="form-control margin-bottom-20" type="text"> -->

								<!-- ID number  -->
								<div  class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <label >ID/Passport Number<span style="color:red">*</span>
									</label> 
									<div class="controls">
										<input <?php echo $CustomerIdReadonly; ?>  style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								
								<!-- End ID number -->
								<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
									<label>First Name<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										<?php if (!empty($FirstNameError)): ?>
										<span class="help-inline"><?php echo $FirstNameError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Last Name -->
								
								<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
									 <label>Last Name<span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
									<?php if (!empty($LastNameError)): ?>
										<span class="help-inline"><?php echo $LastNameError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
								<label>Date of birth
								</label>
									<div class="controls">
									<input  style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date" placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
									</div>
								</div>

							</div>
						</div> 	
					</div>		
						<!-- EOC - Personal Information Details -->	
						<!-- BOC Contact Details -->
					<div class="tab-pane fade in" id="sample-3b">								
						<div class="panel <?php	if(!empty($phoneError)){ echo 'panel-danger';}
														else if(!empty($phoneError)){ echo 'panel-danger';}
														else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayContacts" href="javascript:toggleContacts();">
                                                Contact Details
                                         </a>
                                    </h2>		
								</div>
								<div id="toggleContacts" style="display: <?php	if(!empty($phoneError)){ echo 'block';}
														else if(!empty($emailError)){ echo 'block';}
														else{ echo 'block';}?>">		

								<!-- Contact number -->
								<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input   style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">E mail<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input  style="height:30px"  class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>	
						</div>	
					</div>	
						<!-- EOC Contact Details -->	
						<!-- BOC Residential Address -->
					<div class="tab-pane fade in" id="sample-3b">							
							 <div class="panel <?php	if(!empty($StreetError)){ echo 'panel-danger';}
								else if(!empty($SuburbError)){ echo 'panel-danger';}
								else if(!empty($CityError)){ echo 'panel-danger';}
								else if(!empty($PostCodeError)){ echo 'panel-danger';}

								else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayAddress" href="javascript:toggleAddress();">
                                                Residential Address Details
                                         </a>
                                    </h2>		
								</div>
								<div id="toggleAddress" style="display: <?php	
											if(!empty($StreetError)){ echo 'block';}
											else if(!empty($SuburbError)){ echo 'block';}
											else if(!empty($CityError)){ echo 'block';}
											else if(!empty($PostCodeError)){ echo 'block';}
											else{ echo 'block';}?>">		

								<!-- Street -->
								<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
									<label class="control-label">Street</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<span class="help-inline"><?php echo $StreetError;?></span>
										<?php endif; ?>
									</div>
								</div>
														
								<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
										<label class="control-label">Suburb</label>
										<div class="controls">
							<input  style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<span class="help-inline"><?php echo $SuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								
								<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
									<label class="control-label">City/Town/Township</label>   
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<span class="help-inline"><?php echo $CityError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<label>Province</label>
								<SELECT   class="form-control" name="State" size="1">
								<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->	
								</SELECT>
								<br/>
								<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
									<label class="control-label">Postal Code
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
										<?php if (!empty($PostCodeError)): ?>
										<span class="help-inline"><?php echo $PostCodeError;?></span>
										<?php endif; ?>
									</div>
								</div>
						</div>
					</div>
				</div>	
						<!-- EOC Residential Address -->
				<!------------------------------------------------------------ Banking Details --------------------------------------------------------->
							<!-- BOC Accordion Gompieno -->	
							<div class="tab-pane fade in" id="sample-3b">
							  <div class="panel <?php	if(!empty($AccountHolderError)){ echo 'panel-danger';}
														else if(!empty($BankNameError)){ echo 'panel-danger';}
														else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayBank" href="javascript:toggleBanking();">
                                                Banking Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleBank" style="display: <?php	if(!empty($AccountHolderError)){ echo 'block';}
														else if(!empty($BankNameError)){ echo 'block';}
														else{ echo 'block';}?>">		
							<!-- Account Holder Name -->
							<label>Account Holder Name<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Bank Name -->
							<label>Bank Name</label>
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>
									</div>										
								</div>
							<!-- Account Number  -->
							<label>Account Number
							<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Account Type  -->
							<label>Account Type</label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
									<SELECT class="form-control" id="AccountType" name="AccountType" size="1">
									<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
									<!-------------------- EOC 2017.04.15 - Account type --->
									</SELECT>
								</div>	
							</div>					
							<!-- Branch Code  -->
							<label class="control-label">Branch Code
							</label>
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
									<div class="controls">
									<SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->										
										  </SELECT>
									</div>	
								</div>
							</div>	
					<!-- EOC Accordion Gompieno -->
							</div>
							<!-- EOC Accordion Gompieno -->
						</div>
<!------------------------------------------------------------ End Banking Details --------------------------------------------------------->								
									<!-- Heading 2 -->
                        <div class="tab-pane fade in" id="sample-3b">						
							<div class="panel <?php if(!empty($MonthlyIncomeError)){ echo 'panel-danger';}
												else if(!empty($MonthlyExpenditureError)){ echo 'panel-danger';}else if(!empty($ReqLoadValueError)){ echo 'panel-danger';}
												else if(!empty($firstpaymentError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
												
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayLoan" href="javascript:toggleLoan();">
                                                 Application Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleLoan" style="display: <?php if(!empty($MonthlyIncomeError)){ echo 'block';}
												 else if(!empty($MonthlyExpenditureError)){ echo 'block';}else if(!empty($ReqLoadValueError)){ echo 'block';}
												 else if(!empty($SuretyError)){ echo 'block';}else if(!empty($firstpaymentError)){ echo 'block';}
												 else{ echo 'block';}?>">
								
							   <label>Product/Package Type</label>
                                <SELECT class="form-control" id="loantype" name="loantype" size="1" onChange="valueInterest(this);">
									<!-------------------- BOC 2017.04.15 - dataLoanType --->	
											<?php
											$loantypeid = '';
											$loantypeSelect = $loantype;
											$loantypedesc = '';
											$loantypevalue = 0;
											$currencyIn = '';
											foreach($dataLoanType as $row)
											{
												if($row['loantypedesc'] == 'Rehabilitation') // -- 27.01.2017
												{

											  // -- Loan Type of a selected Bank
												$loantypeid = $row['loantypeid'];
												$loantypedesc = $row['loantypedesc'];						
												$loantypevalue = $row['percentage'];
												// -- 30.06.2017
												// -- BOC Loan Durations (30.06.2017)
												  $fromduration = $row['fromduration'];
												  $toduration = $row['toduration'];
												  $fromdate = $row['fromdate'];
												  $todate = $row['todate'];
												  $fromamount = $row['fromamount'];
												  $toamount = $row['toamount'];
												  $qualifyfrom = $row['qualifyfrom'];
												  $qualifyto = $row['qualifyto'];
												  $currencyIn = $row['currency'];
												  //$fixedamount = $row['fixedamount'];
												  $ReqLoadValue = $row['fixedamount'];
												  $Repayment = $ReqLoadValue;
												  $fixedamount = $ReqLoadValue;
												  $monthlypayment = $ReqLoadValue;
												  // -- Loan - Todate cannot be in past.
													$date1=date_create(date('Y-m-d'));// -- Today's date. 
													$date2=date_create($todate);// -- LoanToDate

													$diff=date_diff($date1,$date2);
													$value = $diff->format("%R%a");
													
												// -- if its negative means Loan To date is in the past.
													//if($value < 0)
													if($loantypedesc != 'Rehabilitation')
													{
														// -- Loan Type has expied.
														// -- DO NO SHOW IT ON THE Application.
													}
													else
													{
													  // -- if its postive means Loan Type To date is in the future/present.
													  // -- SHOW THE LOAN TYPE		
														// -- EOC Loan Durations (30.06.2017)
														// -- 30.06.2017
														// -- Select the Selected Bank 
													  if($loantypeid == $loantype)
													   {
															echo "<OPTION data-vogsInt=$loantypevalue 
															data-qualifyfrom=$qualifyfrom data-qualifyto=$qualifyto 
															data-fromamount=$fromamount data-toamount=$toamount 
															data-fromdate=$fromdate data-todate=$todate 
															data-from=$fromduration data-to=$toduration value=$loantypeid selected>$loantypedesc - R$fixedamount</OPTION>";
													   }
													  else
													   {
															echo "<OPTION data-vogsInt=$loantypevalue 
															data-qualifyfrom=$qualifyfrom data-qualifyto=$qualifyto 
															data-fromamount=$fromamount data-toamount=$toamount 
															data-fromdate=$fromdate data-todate=$todate
															data-from=$fromduration data-to=$toduration value=$loantypeid>$loantypedesc - R$fixedamount</OPTION>";
													   }
													}
											}
											}	
								
											if(empty($dataLoanType))
											{
												echo "<OPTION value=0>No Product Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - dataLoanType --->
								</SELECT>
								
								<!------------------------------ Start Date & End Date - 01.07.2017 ---->
                                <input style="height:30px;display:none"  value="<?php echo $fromamount;?>" id="fromamount" 
								name="fromamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none"  value="<?php echo $toamount;?>" id="toamount" 
								name="toamount" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none"  value="<?php echo $fromdate;?>" id="fromdate" 
								name="fromdate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none"  value="<?php echo $todate;?>" id="todate" 
								name="todate" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none"  value="<?php echo $qualifyfrom;?>" id="qualifyfrom" 
								name="qualifyfrom" class="form-control margin-bottom-10" type="text" />
								
								<input style="height:30px;display:none"  value="<?php echo $qualifyto;?>" id="qualifyto" 
								name="qualifyto" class="form-control margin-bottom-10" type="text" />
								
								<!------------------------------ EOC Start Date and End - 01.07.2017 ---->						
								<div class = "input-group" style="display:none">
								<label>Interest rate (%)
                                </label> 
								<!-- 27.01.2017 Id="InterestRate" -->
                                <input  style="height:30px" value="<?php echo $personalloan;?>" id="InterestRate" name="InterestRate" class="form-control margin-bottom-10" type="text">
								</div> 
								
								<div class = "input-group" style="display:none">
								<label>Monthly Income (e.g. 20000.00)<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
									<div class = "input-group">
										<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyIncome" name="MonthlyIncome" type="text"  placeholder="Monthly Income(eg 20000.00)" value="<?php echo !empty($MonthlyIncome)?$MonthlyIncome:0.00;?>" onchange="CalculateRepayment()"><!-- Monthly Format Income -->
									</div>
									<?php if (!empty($MonthlyIncomeError)): ?>
										<span class="help-inline"><?php echo $MonthlyIncomeError;?></span>
										<?php endif; ?>
								</div>
								</div>
								<div class = "input-group" style="display:none">
									<label class="control-label">Monthly Expenditure(e.g. 20000.00)<span style="color:red">*</span></label>
									<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
										<div class = "input-group">
											<span class = "input-group-addon">R</span>
											<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyExpenditure" name="MonthlyExpenditure" type="text"  placeholder="Monthly Expenditure(e.g. 20000.00)" value="<?php echo !empty($MonthlyExpenditure)?$MonthlyExpenditure:0.00;?>" onchange="CalculateRepayment()"/>
											
										</div>
										<?php if (!empty($MonthlyExpenditureError)): ?>
										<span class="help-inline"><?php echo $MonthlyExpenditureError;?></span>
										<?php endif; ?>
									</div>
								</div>

								<div class = "input-group" style="display:none">
								<label class="control-label">Package Charge amount(e.g. 20000.00)<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
									<div class = "input-group">
									<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="ReqLoadValue" name="ReqLoadValue" type="text"  placeholder="Required Loan Value(e.g. 20000.00)" value="<?php echo !empty($ReqLoadValue)?$ReqLoadValue:0.00;?>" onchange="CalculateRepayment()" >
										
									</div>
									<?php if (!empty($ReqLoadValueError)): ?>
										<span class="help-inline"><?php echo $ReqLoadValueError;?></span>
								<?php endif; ?>
								</div>
								</div>
									
															
								<!--<label>Liquidity (eg 20000.00)
                                    <span class="color-red">*</span>
                                </label>
                                <input name="Liquidity" class="form-control margin-bottom-10" type="text">
								-->
								<!-- Start Loan Duration -->
								<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
									<label class="control-label">Payment Duration (Please select a number of months - e.g. 2)
									</label>
									<div class="controls">
									 <SELECT class="form-control" id="LoanDuration" name="LoanDuration" size="1" onchange="CalculateRepayment()">
									   <?php 
											$LoanDurationSelect = $LoanDuration;
											// -- Define Loan Durations.
											for($i=$fromduration;$i<$toduration;$i++)
											{	 
											 if($LoanDurationSelect == $i)
												{
													if($i == 1)
													{echo "<OPTION value=$i selected>$i-Single Payment</OPTION>";}
													else
													{echo "<OPTION value=$i selected>$i-Split Payment</OPTION>";}
												}
												else
												{
													if($i == 1)													
													{echo "<OPTION value=$i>$i-Single Payment</OPTION>";}
													else
													{echo "<OPTION value=$i>$i-Split Payment</OPTION>";}	
												}
											}
									   ?>
									 </SELECT>
									</div>
								</div>
								<!-- End Loan Duration -->
								 <label>Monthly Payment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  style="height:30px"  id="monthlypayment" name="monthlypayment" value="<?php echo !empty($monthlypayment)?$monthlypayment:0.00;?>" class="form-control margin-bottom-10" type="text" onchange="CalculateRepayment()">
							    </div>
								<br>
								
								 <label>Total Repayment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  readonly style="height:30px"  id="Repayment" name="Repayment" value="<?php echo !empty($Repayment)?$Repayment:0.00;?>" class="form-control margin-bottom-10" type="text" onchange="CalculateRepayment()">
							    </div>

								<br>
								
								
							<div class="control-group">
								<label>Frequency of payment</label>
								<div class="controls">
                                  <SELECT class="form-control" id="paymentfrequency" name="paymentfrequency" size="1">
									<!-------------------- BOC 2017.04.15 -  Payment Frequency --->	
											<?php
											$paymentfrequencyid = '';
											$paymentfrequencySelect = $paymentfrequency;
											$paymentfrequencydesc = '';
											foreach($dataPaymentFrequency as $row)
											{
												$paymentfrequencyid = $row['paymentfrequencyid'];
												$paymentfrequencydesc = $row['paymentfrequencydesc'];
												// Select the Selected Role 
												if($paymentfrequencyid == $paymentfrequencySelect)
												{
												echo "<OPTION value=$paymentfrequencyid selected>$paymentfrequencydesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentfrequencyid>$paymentfrequencydesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentFrequency))
											{
												echo "<OPTION value=0>No Payment Frequency</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Frequency --->
								</SELECT>
								</div></div>
							<div class="control-group">
								<label>Payment method</label>
								<div class="controls">
								<SELECT class="form-control" id="paymentmethod" name="paymentmethod" size="1">
								<!-------------------- BOC 2017.04.15 -  Payment Method --->	
											<?php
											$paymentmethodid = '';
											$paymentmethodSelect = $paymentmethod;
											$paymentmethoddesc = '';
											foreach($dataPaymentMethod as $row)
											{
												$paymentmethodid = $row['paymentmethodid'];
												$paymentmethoddesc = $row['paymentmethoddesc'];
												// Select the Selected Role 
												if($paymentmethodid == $paymentmethodSelect)
												{
												echo "<OPTION value=$paymentmethodid selected>$paymentmethoddesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentmethodid>$paymentmethoddesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentMethod))
											{
												echo "<OPTION value=0>No Payment Method</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Method. --->
								</SELECT>
								</div></div>
								
							   <div class="control-group <?php echo !empty($firstpaymentError)?'error':'';?>">
								<label class="control-label">First Payment Date
									<span style="color:red">*</span>
								</label>
									<div class="controls">
									<input  style="height:30px" id="FirstPaymentDate" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="FirstPaymentDate" placeholder="yyyy-mm-dd" type="Date"  value="<?php echo !empty($firstpayment)?$firstpayment:'';?>" onchange="LastPaymentDate()">
									<?php if (!empty($firstpaymentError)): ?>
									<span class="help-inline"><?php echo $firstpaymentError;?></span>
									<?php endif; ?>
									</div>
								</div>
							   
							   
							   <label>Last Payment Date 
							   </label>
                               <input readonly  style="height:30px"  id="lastPaymentDate" placeholder="yyyy-mm-dd" name="lastPaymentDate" value="<?php echo !empty($lastPaymentDate)?$lastPaymentDate:'';?>" class="form-control margin-bottom-10" type="date" />
							   <label>Agent Comments 
							   </label>
								<textarea class="form-control" rows="3" name="comments" id="comments" placeholder="comments"><?php echo $comments;?></textarea>	
						</div></div> <!-- Acoordion Modise Today -->
							
						<!-- CR -- 18.01.2019 -->
						<div class="tab-pane fade in" id="sample-3b1">						
							<div class="panel <?php if(!empty($nextofkinameError)){ echo 'panel-danger';}
												else if(!empty($nextofkinphoneError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displaynextofkin" href="javascript:togglenextofkin();">
                                                 Spouse & Next of Kin Details
                                         </a>
                                    </h2>		
								</div>
								<div id="togglenextofkin" style="display: <?php if(!empty($nextofkinameError)){ echo 'block';}
									else if(!empty($nextofkinphoneError)){ echo 'block';}
									else{ echo 'block';}?>">
									<div class="form-group">	
									<label class="col-sm-6 control-label">Spouse Name</label>
										<div class="controls">
											<input style="height:30px" class="form-control margin-bottom-10" name="spousename"  
											type="text"  value="<?php echo !empty($spousename)?$spousename:'';?>">
										</div>
										<label class="col-sm-6 control-label">Spouse Phone</label>
										<div class="controls">
											<input style="height:30px" class="form-control margin-bottom-10" name="spousephone"  
											type="text"  value="<?php echo !empty($spousephone)?$spousephone:'';?>">
										</div>
									
									<label class="col-sm-6 control-label">Next of Kin Name</label>
										<div class="control-group <?php echo !empty($nextofkinameError)?'error':'';?>">
											<input style="height:30px" class="form-control margin-bottom-10" name="nextofkiname"  
											type="text"  value="<?php echo !empty($nextofkiname)?$nextofkiname:'';?>">
											<?php if (!empty($nextofkinameError)): ?>
												<span class="help-inline"><?php echo $nextofkinameError;?></span>
											<?php endif; ?>
										</div>
										<label class="col-sm-6 control-label">Next of Kin Phone</label>
										<div class="control-group <?php echo !empty($nextofkinphoneError)?'error':'';?>">
											<input style="height:30px" class="form-control margin-bottom-10" name="nextofkinphone"  
											type="text"  value="<?php echo !empty($nextofkinphone)?$nextofkinphone:'';?>">
											<?php if (!empty($nextofkinphoneError)): ?>
												<span class="help-inline"><?php echo $nextofkinphoneError;?></span>
											<?php endif; ?>
										</div>				
									</div>
								</div>
							</div>
					     </div>
						 <div class="tab-pane fade in" id="sample-3b2">						
							<div class="panel <?php if(!empty($employernameError)){ echo 'panel-danger';}
												else if(!empty($employerphoneError)){ echo 'panel-danger';}else if(!empty($salaryError)){ echo 'panel-danger';}
												else if(!empty($salarydateError)){ echo 'panel-danger';}else if(!empty($occupationError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
												
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayemployer" href="javascript:toggleemployer();">
                                                 Employer & Income Declarations
                                         </a>
                                    </h2>		
								</div>
								<div id="toggleemployer" style="display: <?php if(!empty($employernameError)){ echo 'block';}
									else if(!empty($employerphoneError)){ echo 'block';}else if(!empty($salaryError)){ echo 'block';}
									else if(!empty($salarydateError)){ echo 'block';}else if(!empty($occupationError)){ echo 'block';}
									else{ echo 'block';}?>">
								<div class="control-group">
													<label class="col-sm-4 control-label">Employer<span style="color:red">*</span></label>
													<div class="control-group <?php echo !empty($employernameError)?'error':'';?>">
														<input style="height:30px" class="form-control margin-bottom-10" name="employername"  
														type="text"  value="<?php echo !empty($employername)?$employername:'';?>">
														<?php if (!empty($employernameError)): ?>
														<span class="help-inline"><?php echo $employernameError;?></span>
														<?php endif; ?>
													</div>
													<label class="col-sm-6 control-label">Employer Phone<span style="color:red">*</span></label>
													<div class="control-group <?php echo !empty($employerphoneError)?'error':'';?>">
														<input style="height:30px" class="form-control margin-bottom-10" name="employerphone"  
														type="text"  value="<?php echo !empty($employerphone)?$employerphone:'';?>">
														<?php if (!empty($employerphoneError)): ?>
														<span class="help-inline"><?php echo $employerphoneError;?></span>
														<?php endif; ?>														
													</div>
									<div class="form-group">	
													<label class="col-sm-10 control-label">Salary(e.g. 20000.00)<span style="color:red">*</span></label>
													<div class="control-group <?php echo !empty($salaryError)?'error':'';?>">
														<input style="height:30px" class="form-control margin-bottom-10" name="salary"  
														type="text"  value="<?php echo !empty($salary)?$salary:'0.00';?>">
														<?php if (!empty($salaryError)): ?>
														<span class="help-inline"><?php echo $salaryError;?></span>
														<?php endif; ?>																												
													</div>
													<label class="col-sm-4 control-label">Salary Date<span style="color:red">*</span></label>
													<div class="control-group <?php echo !empty($salarydateError)?'error':'';?>"> <!-- yyyy-mm-->
														<input style="height:30px" class="form-control margin-bottom-10" name="salarydate" placeholder="dd" 
														type="date"  value="<?php echo !empty($salarydate)?$salarydate:'';?>">
														<?php if (!empty($salarydateError)): ?>
														<span class="help-inline"><?php echo $salarydateError;?></span>
														<?php endif; ?>																																										
													</div>
									</div>
									<div class="form-group">	
													<label class="col-sm-10 control-label">Other Income (e.g. 20000.00)</label>
													<div class="controls">
														<input style="height:30px" class="form-control margin-bottom-10" name="otherincome"  
														type="text"  value="<?php echo !empty($otherincome)?$otherincome:'0.00';?>">
													</div>
													<label class="col-sm-2 control-label">occupation<span style="color:red">*</span></label>
													<div class="control-group <?php echo !empty($occupationError)?'error':'';?>">
														<input style="height:30px" class="form-control margin-bottom-10" name="occupation"  
														type="text"  value="<?php echo !empty($occupation)?$occupation:'';?>">
														<?php if (!empty($occupationError)): ?>
														<span class="help-inline"><?php echo $occupationError;?></span>
														<?php endif; ?>																																																								
													</div>
									</div>
								</div>	
								</div>
							</div>
					     </div>
						 <div class="tab-pane fade in" id="sample-3b3">						
							<div class="panel <?php echo 'panel-default';?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa-angle-up" id="displayauthorization" href="javascript:toggleauthorize();">
                                                 Authorization Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleauthorize" style="display: <?php echo 'block';?>">
								<div class="control-group">
								<table>
								  <tr><td><label class="col-sm-2 control-label">creditcheck</label></td>
									<td><div class="col-sm-1">
											<input style="height:30px" class="form-control margin-bottom-10" name="creditcheck" placeholder="creditcheck" 
											type="checkbox"  value="d" <?php if(!empty($creditcheck)){echo "checked";}?>>
									</div></td>
								  </tr>	
								  <tr>
										<td><label class="col-sm-2 control-label">debitorder</label></td>
										<td><div class="col-sm-1">
											<input style="height:30px" class="form-control margin-bottom-10" name="debitorder" placeholder="debitorder" 
											type="checkbox"  value="d" <?php if(!empty($debitorder)){echo "checked";}?>>
										</div></td>
								   </tr>
									
									<tr>
										<td><label class="col-sm-2 control-label">marketinformartion</label></td>
										<td><div class="col-sm-1">
											<input style="height:30px" class="form-control margin-bottom-10" name="marketinformartion" placeholder="marketinformartion" 
											type="checkbox"  value="d" <?php if(!empty($marketinformartion)){echo "checked";}?>>
										</div></td>
									</tr> 		
								</table>	 
								</div>
							</div>

							</div>
					     </div>						 
						<!-- CR -- 18.01.2019 -->							
					<div class='row'>	
											<div class="form-group">
						    <div class="col-sm-2">
							  <button type="submit" class="btn btn-primary btn-label-left">
							  <span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						    </div>
														<div class="col-sm-offset-2 col-sm-3">
								  <a class='btn btn-primary'  href=<?php echo $backLinkLead; ?> >Back to Leads</a><!-- Generate OTP -->
							</div>

					    </div>

						<div class="form-group">
							<div class="col-sm-offset-1 col-sm-2">
								  <a class='btn btn-primary'  href=<?php echo $backLinkName; ?> >Back to sales</a><!-- Generate OTP -->
							</div>
						</div>
					</div>
						
					</div>
                    </form> 
							<?php
							if(!empty($_POST))
							  // if($_SERVER['REQUEST_METHOD']=='POST')
							   {
								  echo '<script type="text/javascript"> LastPaymentDate(); </script>';
							   } 
							?>
				            </div>
                         </div>
                      </div>
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

// -- Different Loan Types.
 function valueInterest(sel) 
 {
		var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
		var fromdate = sel.options[sel.selectedIndex].getAttribute('data-fromdate');
		var todate = sel.options[sel.selectedIndex].getAttribute('data-todate');
		var fromduration = sel.options[sel.selectedIndex].getAttribute('data-from');
		var toduration = sel.options[sel.selectedIndex].getAttribute('data-to');
		var fromamount = sel.options[sel.selectedIndex].getAttribute('data-fromamount');
		var toamount = sel.options[sel.selectedIndex].getAttribute('data-toamount');
		var qualifyfrom = sel.options[sel.selectedIndex].getAttribute('data-qualifyfrom');
		var qualifyto = sel.options[sel.selectedIndex].getAttribute('data-qualifyto');
		
//alert(interest);			
		document.getElementById('InterestRate').value = interest;
		document.getElementById('fromdate').value = fromdate;
		document.getElementById('todate').value = todate;
		document.getElementById('fromamount').value = fromamount;
		document.getElementById('toamount').value = toamount;
		document.getElementById('qualifyfrom').value = qualifyfrom;
		document.getElementById('qualifyto').value = qualifyto;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;

 }		
// -- EOC Different Loan Types.

// -- 30.06.2017 -- LoanDuration
 // -- Enter to capture comments   ---
 function defineloandutations(sel) 
 {
		
		var fromduration = document.getElementById('fromduration').value;
		var toduration = document.getElementById('toduration').value;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 30.06.2017 -- LoanDuration
function toggleBanking() 
 {
	var ele = document.getElementById("toggleBank");
	var text = document.getElementById("displayBank");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';

	}
} 

function toggleLoan() 
 {
	var ele = document.getElementById("toggleLoan");
	var text = document.getElementById("displayLoan");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayLoan" href="javascript:toggleLoan();">	 Application Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayLoan" href="javascript:toggleLoan();">	 Application Details</a>';

	}
} 

function toggleauthorize() 
 {
	var ele = document.getElementById("toggleauthorize");
	var text = document.getElementById("displayauthorize");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayauthorize" href="javascript:displayauthorize();">Authorize Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayauthorize" href="javascript:displayauthorize();">Authorize Details</a>';

	}
} 

function togglenextofkin() 
 {
	var ele = document.getElementById("togglenextofkin");
	var text = document.getElementById("displaynextofkin");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displaynextofkin" href="javascript:togglenextofkin();">Spouse & Next of Kin</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displaynextofkin" href="javascript:togglenextofkin();">Spouse & Next of Kin</a>';

	}
} 

function toggleemployer() 
 {
	var ele = document.getElementById("toggleemployer");
	var text = document.getElementById("displayemployer");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayemployer" href="javascript:toggleemployer();">Employer & Declarations</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayemployer" href="javascript:toggleemployer();">Employer & Declarations</a>';

	}
} 


function toggleRegister() 
 {
	var ele = document.getElementById("toggleRegister");
	var text = document.getElementById("displayRegister");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayRegister" href="javascript:toggleRegister();">	Personal Information Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayRegister" href="javascript:toggleRegister();"> Personal Information Details</a>';

	}
} 

function toggleAddress() 
 {
	var ele = document.getElementById("toggleAddress");
	var text = document.getElementById("displayAddress");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';

	}
} 

function toggleContacts() 
 {
	var ele = document.getElementById("toggleContacts");
	var text = document.getElementById("displayContacts");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle fa-angle-up" id="displayContacts" href="javascript:toggleContacts();">	Contact Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayContacts" href="javascript:toggleContacts();">	Contact Details</a>';

	}
} 
</script> 