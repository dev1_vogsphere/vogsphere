<?php
				
	require 'database_ffms.php';
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	require 'database.php';
    $pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$tbl_member = "member";
	$tbl_member_policy = "member_policy";
	$tbl_address = 'address';
	$dbname = "eloan";

	$state=null;
	$msg=null;
	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
	$count = 0;

	$IntUserCode = getsession('UserCode');
	//echo($IntUserCode );
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}

	if ( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}

			if ( null==$customerid )
				{
				header($location);

				}
				else if( null==$ApplicationId )
				{
				header($location);

				}
			else {

			$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
			AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id
			AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id";
			
			
			//echo $sql;
			$q = $pdo_ffms->prepare($sql);
			$q->execute(array($customerid,$ApplicationId));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			//print_r($data);
			
				// ---------------------- EOC - Notify ------------------------------- // 
			
			
			// ---------------------- EOC - policy_status  ------------------------------- //  
			 $sql = 'SELECT * FROM policy_status where IntUserCode=?';
			 $q =  $pdo_ffms->prepare($sql);						   						 
			 $q->execute(array($IntUserCode));
			 $dataPolicyStatus = $q->fetchAll(PDO::FETCH_ASSOC);
		
			// ---------------------- EOC - policy_status ------------------------------- //  
			
			
			
			$TitleError=null; 
			$FirstNameError = null; 
			$Middle_nameError= null;
			$SurnameNameError= null;
			$Member_emailError= null;
			$CellphoneError= null;
			$Line_2Error=null;
			$DateOfBirthError=null;
			$Personal_numberError=null;
			$Province_nameError=null;
			$Postal_codeError =null;
			$Title = $data['Title'];

			

			Database::disconnect();
			$_SESSION['ApplicationId'] = $ApplicationId;
			$_SESSION['customerid'] = $customerid;
			}

			// -- BOC 13/12/2022 - Page Features Authorization...	
			// -- Authorization of features
			//$role = $dataUserEmail['role'];//
			$dataPageFeaturesArr = null;
			$currentpageArr = null;
			$currentpageArr = explode("?",basename($_SERVER['REQUEST_URI'],".php"));
			$currentpage = $currentpageArr[0];
			$authorized_featuresArr = null;
			$role = $_SESSION['role'];

			$displaygenerate_payments='display:inline-block';
			$displaydownloadcontract ='display:inline-block';
			$displayupload_document ='display:inline-block';

			$authorized_featuresArr   = authorize_features($pdo,$currentpage,$role);
			//print_r($authorized_featuresArr);
			$displaygenerate_payments = $authorized_featuresArr['displaygenerate_payments'];
			$displaydownloadcontract  = $authorized_featuresArr['displaydownloadcontract'];
			$displayupload_document   = $authorized_featuresArr['displayupload_document'];
			// -- EOC 13/12/2022 - Page Features Authorization...	

			// -- BOC CR1002 - 31/01/2023 - Only show generate payment button on APPROVE..	
			if ($data['Active'] != '0')
			{
				$displaygenerate_payments  = 'display:none';
			}
			if ($data['Active'] == '0')
			{
				$displayupload_document    = 'display:inline-block';
				$displaydownloadcontract   = 'display:inline-block';
			}
			// -- EOC 31/01/2023 - Only show generate payment button on APPROVE..	
			$displaygenerate_payments  = 'display:none';
			
			//print_r	($data);
			//update data 
			
			if(isset($_POST['Continue']))
			{
				$customerid=$_POST['Member_id'];
				$ApplicationId=$_POST['Member_Policy_id'];

				$policy_status="1";
				$sql = "UPDATE $tbl_member_policy SET $tbl_member_policy.Active=? WHERE Member_id = ? AND Member_Policy_id = ?";
				$q = $pdo_ffms->prepare($sql);
				$q->execute(array($policy_status,$customerid,$ApplicationId));
				//$results = $q->fetch(PDO::FETCH_ASSOC);
				header("Location: finfindmember.php");
				
			}
			

?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?>  method="post">
					<tr>
						<td>
						<div class="control-group <?php echo !empty($Member_idError)?'error':'';?>">
						<label class="control-label"><strong>Member identifcation number</strong></label>
						<div class="controls">
						<input style="height:30px" class="form-control margin-bottom-10" name="Member_id" type="text" Readonly placeholder="Member ID" 
						value="<?php echo !empty($data['Member_id'])?$data['Member_id']:'';?>">
						<?php if (!empty($Member_idError)): ?>
						<span class="help-inline"><?php echo $Member_idError;?></span>
						<?php endif; ?>
						</div>
						</div>
						</td>
						
						<td>
						<div class="control-group <?php echo !empty($Member_Policy_idError)?'error':'';?>">
						<label class="control-label"><strong>Policy number</strong></label>
						<div class="controls">
						<input style="height:30px" class="form-control margin-bottom-10" name="Member_Policy_id" type="text"  Readonly placeholder="Member Policy id" 
						value="<?php echo !empty($data['Member_Policy_id'])?$data['Member_Policy_id']:'';?>">
						<?php if (!empty($Member_Policy_idError)): ?>
						<span class="help-inline"><?php echo $Member_Policy_idError;?></span>
						<?php endif; ?>
						</div>
						</div>
						</td>
				   </tr>
				   
	    			  <input type="hidden" name="customerid" value="<?php echo $customerid;?>"/>
					  <input type="hidden" name="ApplicationId" value="<?php echo $ApplicationId;?>"/>
					  <p class="alert alert-error" align="centre">Are you sure you want to deactivate <?php echo $customerid;?>?</p>
					  <div class="form-actions">
						  <button type="submit" name="Continue" class="btn btn-danger">Yes</button>
						  <a class="btn" href="finfindmember.php">No</a>
						</div>

					</form>
				</div>
		</div>		
</div> <!-- /container -->

