<?php 
require 'database.php';
$backLinkName = 'customers';

$userid 			= '';
$status = 'open';
// -- Read SESSION userid.
// -- Logged User Details.
if(isset($_SESSION['username']))
{
	$userid = $_SESSION['username'];
}
//echo $userid;

 $status = '';
	 $open = '';
	 $closed = '';
	 $finalised = '';

	$crrolesarray[1] = "'crmanager'";
	$crrolesarray[2] = "'crconsultant'";
	$cragents = GetCRAgents($crrolesarray);
		$whereArr = array();

	$agents = implode(",",$cragents);
	$whereArr[] = "CustomerId IN ({$agents})";
	$dataAgents = search_dynamic_customers($whereArr);


$count = 0;
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 
  
//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);
 
//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM customer';
$dataCustomers = $pdo->query($sql);		
 
// BOC -- 15.04.2017 -- Updated.

// -- Personal Details
$CustomerId = '';
$Title 		= '';
$FirstName  = '';
$LastName 	= '';
$Dob 		= '';
$refnumber  = '';

// -- Contact Details.
$phone 		= '';
$refnumber  = '';

// -- Residential Address.
$Street 	= '';
$Suburb 	= '';
$City 		= '';
$PostCode 	= '';

// -- Banking Details.
$AccountHolder = '';
$AccountType   = '';
$AccountNumber = '';
$BranchCode    = '';
$BankName      = '';

// -- Required fields
$AccountHolderError ='';
$AccountNumberError	='';
$BranchCodeError	='';
$bankError			='';
$phoneError			='';
$FirstNameError 	= '';
$LastNameError 		= '';
$tbl_customer 		= 'customer';
$valid = true;
$email = '';
$message = '';
$idnumberError = '';
$emailError = '';
$phoneError = '';

$tbl				= "tenantcustomers"; // Table name 
$customerError		= '';

$changedby = '';
$comments  = '';

$creditcheck = '';
$debitorder  = '';
$marketinformartion = '';

$employername = '';
$employerphone = '';
$salary = '';
$otherincome = '';
$salarydate = '';

$spousename = '';
$spousephone = '';
$nextofkiname = '';
$nextofkinphone = '';
$occupation		= '';	

if (!empty($_POST)) 
{
	
// creditcheck
if(isset($_POST['creditcheck']))
{
	$creditcheck = $_POST['creditcheck'];
}
else
{
	$creditcheck = '';
}
//debitorder
if(isset($_POST['debitorder']))
{
	$debitorder = $_POST['debitorder'];
}
else
{
	$debitorder = '';
}
//marketinformartion
if(isset($_POST['marketinformartion']))
{
	$marketinformartion = $_POST['marketinformartion'];
}
else
{
	$marketinformartion = '';
}

$employername 		 = $_POST['employername'];
$employerphone 		 = $_POST['employerphone'];
$salary 			 = $_POST['salary'];
$otherincome 		 = $_POST['otherincome'];
$salarydate 		 = $_POST['salarydate'];

$spousename 		 = $_POST['spousename'];
$spousephone 		 = $_POST['spousephone'];
$nextofkiname 		 = $_POST['nextofkiname'];
$nextofkinphone 	 = $_POST['nextofkinphone'];
$occupation 	 = $_POST['occupation'];
$comments  	= $_POST['comments'];
$changedby  	= $_POST['changedby'];
	
	$count = 0;
// -- Personal Details
		$Title = $_POST['Title'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$Dob		= $_POST['Dob'];
		$idnumber = $_POST['idnumber'];
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
// -- Contact Details
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		// validate e-mail
		if (!empty($email)){if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}}
		if (empty($phone)) { $phoneError = 'Please enter cell phone number'; $valid = false;}

// -- Residential Address
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		
// -- Banking Details.
if(isset($_POST['BranchCode']))
{$BranchCode = $_POST['BranchCode'];}
		
		$BankName = $_POST['BankName'];
		$AccountHolder = $_POST['AccountHolder'];
		$AccountNumber = $_POST['AccountNumber'];
		$AccountType = $_POST['AccountType'];
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please selec Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}

// -- Id Error.	
if (!empty($idnumber)) { $valid = CheckUserExist($idnumber);}

// -- Communication Error.
if (!empty($phone)) {CheckCommunication($phone,$email);}		

if (!empty($email)) {CheckCommunication($phone,$email);}	
		
		if ($valid) 
		{
			$number = $dataCustomers->rowCount() + 1;
			$size = 13;

			if(empty($idnumber))
			{
				// -- no id number we shall generate the customerid
				$refnumber = number_pad($number,$size);
				$idnumber = $refnumber;
			}
			
			if(empty($Dob))
			{
				$Dob = '9999-01-01';
			}
			// --- Customer Details
			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,Street,Suburb,City,State,PostCode,Dob,phone,
accountholdername,
bankname,
accountnumber,
branchcode,
accounttype,email2,createdby,createdon,changedby,status
,comments		  
,nextofkiname		
,nextofkinphone		
,spousename			
,spousephone			
,employername		
,employerphone		
,occupation			
,creditcheck			
,debitorder			
,marketinformartion
,salary				
,salarydate			
,otherincome       
)";
$today = date('Y-m-d');
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$email,$userid,$today,$changedby,$status
			,$comments		  
			,$nextofkiname		
			,$nextofkinphone		
			,$spousename			
			,$spousephone		
			,$employername		
			,$employerphone		
	        ,$occupation			
            ,$creditcheck		
            ,$debitorder			
            ,$marketinformartion
	        ,$salary				
			,$salarydate			
			,$otherincome ));      
			// -- Check if assignment exit, true mean does not exists.
			if(CheckTenantExist($userid,$idnumber))
			{
				$count = AssignTenantCustomers($userid,$idnumber);
			}
			$count = $count + 1;
			$message = $idnumber;
			Database::disconnect();
		}
}
else
{
	// -- Default Bankname - ABSA
  $BankName = "ABSA";
}

// -- Assign Tenant Customers.
function AssignTenantCustomers($user,$customer)
{
	Global 	$tbl;
	$count = 0;
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO $tbl(tenantid, customerid) VALUES (?,?)";
	$q = $pdo->prepare($sql);
	$q->execute(array($user,$customer));
	Database::disconnect();
	$count = $count + 1;
	return $count;
}

// -- Checked Assigned Customer to Tenant.
function CheckTenantExist($tenantid,$customerid)
{
	Global $customerError;
	Global 	$tbl;

	$lc_valid = true;	
	
	if($tenantid == $customerid)
	{$customerError = "<p class='status'>Tenant Customer Assignment cannot be the same.</p>"; $lc_valid = false;}

	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	// ---------------- Check TenantID --------------- //
	$query = "SELECT * FROM $tbl WHERE tenantid = ? AND customerid = ?";
	$q = $pdo->prepare($query);
	$q->execute(array($tenantid,$customerid));
	
	if ($q->rowCount() >= 1)
	{$customerError = "<div class='form-group has-error'><p class='status'>Tenant Customer Assignment already exists.</p></div>"; $lc_valid = false;}
	return $lc_valid;
}

// -- Check Email & Phone.
function CheckCommunication($phone,$email)
{
	Global $phoneError;
	Global $emailError;
	Global $tbl_customer;
	$tbl_user = 'user';

	$lc_valid = true;	

		$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// ---------------- Check Phone number -------------- //
			if(!empty($phone))
			{
				$query = "SELECT * FROM $tbl_customer WHERE phone = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($phone));
				if ($q->rowCount() >= 1)
				{$phoneError = 'Phone number already exists.'; $lc_valid = false;}
			}
			
			// ---------------- Check E-mail in Users ------------------ //
			if(!empty($email))
			{
				$query = "SELECT * FROM $tbl_user WHERE email = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use.'; $lc_valid = false;}
			}
			
			// ---------------- Check E-mail in Customer ------------------ //
			if($lc_valid)
			{
				if(!empty($email))
				{
					$query = "SELECT * FROM $tbl_customer WHERE email2 = ?";
					$q = $pdo->prepare($query);
					$q->execute(array($email));
					if ($q->rowCount() >= 1)
					{$emailError = 'E-mail Address already in use.'; $lc_valid = false;}
				}
			}
	
		return $lc_valid;
}

// -- Check if customer exist.
function CheckUserExist($userid)
{
	Global $idnumberError;	
			$lc_valid 	  = true;	
			$tbl_user 	  = 'user';
			$tbl_customer = 'customer';
	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// ---------------- Check Userid --------------- //
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($userid));
			if ($q->rowCount() >= 1)
			{$idnumberError = 'Unique number already exists.'; $lc_valid = false;}


			// ---------------- Check CustomerId -------------- //
			if($lc_valid)
			{
				$query = "SELECT * FROM $tbl_customer WHERE CustomerId = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($userid));
				if ($q->rowCount() >= 1)
				{$idnumberError = 'Unique number already exists.'; $lc_valid = false;}
			}

		return $lc_valid;	
}
// -- Generate Reference Number.
function number_pad($number,$n) 
{
	return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
}

// --  Get All Credit Rehabilitation Agents including Managers.
function GetCRAgents($crrolesarray)
{
	Global $db_name;
	$indexInside = 0;
	$dataAgents = '';
	$ArrayAgents = null;
$roles = implode(",",$crrolesarray);
		//print_r (implode(",",$crrolesarray));

	// -- Database Connections.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	// --------------------- Charges ------------------- //
	$sql = "SELECT * FROM user WHERE role IN ($roles)";
	$q = $pdo->prepare($sql);
	$q->execute();//array(explode("",$crrolesarray)));
	$dataAgents = $q->fetchAll(PDO::FETCH_ASSOC);

	if(empty($dataAgents))
	{
		return $ArrayAgents;
	}
	else
	{
	
		foreach($dataAgents as $row)
		{
			  $ArrayAgents [$indexInside] = $row['userid'];
			  $indexInside = $indexInside + 1;
		}	
		//print_r (implode(",",$ArrayAgents));
	
				return $ArrayAgents;
	}		
}
// --  Get All Credit Rehabilitation Agent including Managers.

// -- Dynamic Query.
	 function search_dynamic_customers($queryArr)
    {
		Global $pdo;
		
		$queryStr = implode(" AND ", $queryArr);
		if(empty($queryArr)){
			$sql = "SELECT * FROM customer ORDER BY createdon DESC";
		}else{
        $sql = "SELECT * FROM customer WHERE {$queryStr} ORDER BY createdon DESC";}
		$q = $pdo->prepare($sql);
		$q = $pdo->query($sql);
		//echo $sql;
		
        return $q;
    }

?>
<div class='container background-white bottom-border'>		
<div class='row margin-vert-30'>
	<div class="col-xs-12 col-sm-12">
		<!-- <div class="box"> -->
<!--			<div class="box-content">-->
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
					<!-- <div class="box-header">
						<div class="box-name">
							<span>Registration form</span>
						</div>
					</div> -->
				<?php # error messages
					if (isset($message)) 
					{
						//foreach ($message as $msg) 
						{
							//printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Lead successfully created.Reference Number :$message</p>");
					}
			   ?>
				<h4 class="page-header">Pesonal Information Details</h4>
				<div class="form-group <?php if (!empty($idnumberError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Title</label>
						<div class="col-sm-4">
							<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
							</SELECT>
						</div>

						<label class="col-sm-2 control-label">ID/Passport</label>
						<div class="col-sm-4">
										<input class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
								    <small class="help-block" style=""><?php echo $idnumberError; ?></small>
									<?php endif; ?>	
						</div>
					</div>
					
				<div class="form-group <?php if(!empty($FirstNameError)){ echo "has-error";} if(!empty($LastNameError) && empty($FirstNameError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">First name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
								<?php if (!empty($FirstNameError)): ?>
								<small class="help-block" style=""><?php echo $FirstNameError; ?></small>
								<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Last name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
							<?php if (!empty($LastNameError)): ?>
										<small class="help-block" style=""><?php echo $LastNameError; ?></small>	
							<?php endif; ?>	
						</div>
				</div>
				<div class="form-group <?php if (!empty($DobError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Date of birth</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
						</div>
					</div>
					<h4 class="page-header">Contact Details</h4>
					<div class="form-group <?php if (!empty($phoneError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<small class="help-block" style=""><?php echo $phoneError; ?></small>	
										<?php endif; ?>
							</div>
						<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<small class="help-block" style=""><?php echo $emailError; ?></small>											
										<?php endif; ?>
							</div>
					</div>

					<h4 class="page-header">Residential Address</h4>

					<div class="form-group <?php if (!empty($StreetError)){ echo "has-error";} if (!empty($SuburbError) && empty($StreetError)){ echo "has-error";} ?>">
						<label class="col-sm-2 control-label">Street</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<small class="help-block" style=""><?php echo $StreetError; ?></small>	
										<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Suburb</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<small class="help-block" style=""><?php echo $SuburbError; ?></small>	
											<?php endif; ?>
						</div>
					</div>
					<div class="form-group <?php if (!empty($CityError)){ echo "has-error";}?>">
					<label class="col-sm-2 control-label">City/Town/Township</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<small class="help-block" style=""><?php echo $CityError; ?></small>											
										<?php endif; ?>
						</div>
						
						<label class="col-sm-2 control-label">Province</label>
						<div class="col-sm-4">
						<SELECT class="form-control" id="State" name="State" size="1">
									<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->
								</SELECT>

						</div>
						
					</div>
					<div class="form-group <?php if (!empty($PostCodeError)){ echo "has-error";}?>">
					<label class="col-sm-2 control-label">Postal Code</label>
						<div class="col-sm-2">
							<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
							<?php if (!empty($PostCodeError)): ?>
								<small class="help-block" style=""><?php echo $PostCodeError; ?></small>																			
							<?php endif; ?>
						</div>
					</div>	
				<h4 class="page-header">Banking Details</h4>
					<div class="form-group <?php if (!empty($AccountHolderError)){ echo "has-error";}?>">					
						<label class="col-sm-2 control-label">Account Holder Name</label>
						<div class="col-sm-4">
						<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
							<?php if (!empty($AccountHolderError)): ?>
										<small class="help-block" style=""><?php echo $AccountHolderError; ?></small>																													
							<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Bank Name</label>						
						<div class="col-sm-4">
							<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
						</div>
					</div>
					<div class="form-group <?php if (!empty($AccountNumberError)){ echo "has-error";}?>">										
						<label class="col-sm-2 control-label">Account Number</label>
						<div class="col-sm-4">
						 <input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />		
							<?php if (!empty($AccountNumberError)): ?>
								<small class="help-block" style=""><?php echo $AccountNumberError; ?></small>																																					
							<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Account Type</label>						
						<div class="col-sm-4">
							<SELECT class="form-control" id="AccountType" name="AccountType" size="1" >
								<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Account type --->
							</SELECT>
						</div>
					</div>
					<div class="form-group"> <!-- has-error has-feedback"> -->
						<label class="col-sm-2 control-label">Branch Code</label>
						<div class="col-sm-4">
							<SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
																<?php if (!empty($BranchCodeError)): ?>
									<span class="help-inline"><?php echo $BranchCodeError;?></span>
									<?php endif; ?>
						</div>
					</div>
	<h4 class="page-header">Agent Details</h4>	
				<div class="form-group <?php if (!empty($AccountHolderError)){ echo "has-error";}?>">					
				<?php echo " 
						<label class='col-sm-2 control-label'>Lead Status</label>
						<div class='col-sm-4'>
										 <SELECT class='form-control' id='status' name='status' size='1'>
												<OPTION value='open' $open>Open</OPTION>
												<OPTION value='closed'  $closed>Closed</OPTION>
												<OPTION value='finalised'  $finalised>Finalised</OPTION>
										 </SELECT>
									</div>";	?>
				<?php   if($_SESSION['role'] != 'crconsultant'){?>
				<label class="col-sm-2 control-label">Assign Agent</label>
						<div class="col-sm-4">
							<SELECT class="form-control" id="changedby" name="changedby" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$agentid = '';
											$agentSelect = $changedby;
											$agentname = '';
											foreach($dataAgents as $row)
											{
											  // -- Selected Agent.
												$agentname = $row['FirstName']." ".$row['LastName'];
												$agentid = $row['CustomerId'];
													
												// -- Select the Selected Bank 
											  if($agentid == $changedby)
											   {
													echo "<OPTION value=$agentid selected>$agentid - $agentname</OPTION>";
											   }
											   else
											   {
													 echo "<OPTION value=$agentid>$agentid - $agentname</OPTION>";
											   }
										
											if(empty($dataAgents))
											{
												echo "<OPTION value=0>No Agents</OPTION>";
											}
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
						</div>
				<?php }?>	
				</div>
				<br/>
				<div class="form-group">
					<label class="col-sm-2 control-label">comments</label>
						<!--<div class="col-sm-2">
						</div>-->
					<div class="col-sm-10">	
						<textarea class="form-control" rows="3" name="comments" id="comments" placeholder="comments"><?php echo $comments;?></textarea>	
					</div>
				</div>		
				
				<h4 class="page-header">Spouse & Next of Kin Details</h4>	
				<div class="form-group">	
				<label class="col-sm-2 control-label">Spouse Name</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="spousename" placeholder="spousename" 
						type="text"  value="<?php echo !empty($spousename)?$spousename:'';?>">
					</div>
				    <label class="col-sm-2 control-label">Spouse Phone</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="spousephone" placeholder="spousephone" 
						type="text"  value="<?php echo !empty($spousephone)?$spousephone:'';?>">
					</div>
				
				</div>
				
				<div class="form-group">	
				<label class="col-sm-2 control-label">Next of Kin Name</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="nextofkiname" placeholder="nextofkiname" 
						type="text"  value="<?php echo !empty($nextofkiname)?$nextofkiname:'';?>">
					</div>
				    <label class="col-sm-2 control-label">Next of Kin Phone</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="nextofkinphone" placeholder="nextofkinphone" 
						type="text"  value="<?php echo !empty($nextofkinphone)?$nextofkinphone:'';?>">
					</div>				
				</div>
				
				<h4 class="page-header">Employer & Income Declarations</h4>	
				<div class="form-group">	
				    <label class="col-sm-2 control-label">Employer</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="employername" placeholder="employername" 
						type="text"  value="<?php echo !empty($employername)?$employername:'';?>">
					</div>
				    <label class="col-sm-2 control-label">Employer Phone</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="employerphone" placeholder="employerphone" 
						type="text"  value="<?php echo !empty($employerphone)?$employerphone:'';?>">
					</div>

				</div>
				<div class="form-group">	
				    <label class="col-sm-2 control-label">Salary</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="salary" placeholder="salary" 
						type="text"  value="<?php echo !empty($salary)?$salary:'';?>">
					</div>
				    <label class="col-sm-2 control-label">Salary Date</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="salarydate" placeholder="yyyy-mm-dd" 
						type="date"  value="<?php echo !empty($salarydate)?$salarydate:'';?>">
					</div>
				</div>
				<div class="form-group">	
				    <label class="col-sm-2 control-label">Other income</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="otherincome" placeholder="otherincome" 
						type="text"  value="<?php echo !empty($otherincome)?$otherincome:'';?>">
					</div>
				    <label class="col-sm-2 control-label">occupation</label>
					<div class="col-sm-4">
						<input style="height:30px" class="form-control margin-bottom-10" name="occupation" placeholder="occupation" 
						type="text"  value="<?php echo !empty($occupation)?$occupation:'';?>">
					</div>
				</div>
				<h4 class="page-header">Authorization Details</h4>	
				<div class="form-group">	
					<label class="col-sm-2 control-label">creditcheck</label>
					<div class="col-sm-1">
						<input style="height:30px" class="form-control margin-bottom-10" name="creditcheck" placeholder="creditcheck" 
						type="checkbox"  value="d" <?php if(!empty($creditcheck)){echo "checked";}?>>
					</div>
					
					<label class="col-sm-2 control-label">debitorder</label>
					<div class="col-sm-1">
						<input style="height:30px" class="form-control margin-bottom-10" name="debitorder" placeholder="debitorder" 
						type="checkbox"  value="d" <?php if(!empty($debitorder)){echo "checked";}?>>
					</div>

					<label class="col-sm-2 control-label">marketinformartion</label>
					<div class="col-sm-1">
						<input style="height:30px" class="form-control margin-bottom-10" name="marketinformartion" placeholder="marketinformartion" 
						type="checkbox"  value="d" <?php if(!empty($marketinformartion)){echo "checked";}?>>
					</div>

				</div>
					
				<h4 class="page-header"></h4>					
	
					<div class="clearfix"></div>
					<div class="form-group">
												<div class="col-sm-offset-2 col-sm-2">
							<!--<a type="cancel" class="btn btn-default btn-label-left">
							<span><i class="fa fa-clock-o txt-danger"></i></span>
								Cancel
							</button> -->
							  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->

						</div>

						<div class="col-sm-2">
							<button type="submit" class="btn btn-primary btn-label-left">
							
							  <!-- <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button> -->

							
							<span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						</div>
					</div>
				</form>
			<!-- </div> 298 -->
		</div>
	</div>
</div>
<script type="text/javascript">
/*
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
*/

// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

</script>
