<div class='container background-white bottom-border'>
	<div class='row margin-vert-30'>
		 <div class="col-md-12 text-left">
			 <i><h2 class="fa fa-thumbs-o-up fa-3x color-primary padding-top-10 animate fadeIn"> Debit Order Collection</h2></i>
		 </br>
		 </br>
			 <p class="p1" class="animate fadeIn">The Debit Order Collection service is a simple and efficient way to collect recurring payments from customers with selection payment periods that suit your needs. This includes same day or next day collections.  You can also process bulk credit card collections through our preferred credit card acquiring service provider.</p>
			 <p>
			<button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Subscribe today</font></button>
			</p>
		 </div>
	 </div>
</div>
