<?php
require 'database.php';
$B_IntUserCode = getsession('IntUserCode');

// -- Database Declarations and config:
$pdoeloan = Database::connect();
$pdoeloan->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$log_file = "/var/log/httpd/debug_log";

if(!function_exists('get_tenant_logo'))
{
	function get_tenant_logo($IntUserCode)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'Seq_Generator_fin';			
		$sql = "select * from $tbl where Client_Id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
if(!function_exists('get_ffms_premium_amnt'))
{
	function get_ffms_premium_amnt($Policy_id)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$PackageOption_id = get_ffms_package_option($Policy_id);
		//print_r( $PackageOption_id);
		$data = null;
		$tbl  = 'premium';			
		$sql = "select * from $tbl where PackageOption_id = ? and Years = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($PackageOption_id,1));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);
		return number_format($PackageOption_id);		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_package_option'))
{
	function get_ffms_package_option($Policy_id)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'policy';			
		$sql = "select * from $tbl where Policy_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($Policy_id));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data['PackageOption_id'];		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_benefits'))
{
	function get_ffms_benefits($Policy_id)
	{
////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$PackageOption_id = get_ffms_package_option($Policy_id);
		$data = null;
		$tbl  = 'packageoption';			
		$sql = "select * from $tbl where PackageOption_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($PackageOption_id));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return number_format($data['Benefits'],2);		
	}
}
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021
require  $filerootpath.'/ffms/ffms_classess.php';
$im_value = 0;
$source_of_income = new Source_of_incomeDAL();
$source_of_incomeList = $source_of_income->Source_of_income($im_value);

$tenant_array = get_tenant_logo($B_IntUserCode);

// BOC -- Premimum & Benef Amnt -- //
$premiumAmnt	= 0.00;
$futureBenefits = 0.00;
// EOC -- Premimum & Benef Amnt -- //

$Occupation = new OccupationDAL();
$im_occupationList  = $Occupation->Occupation($im_value);
//print_r($im_occupationList );
$im_occupation_id_1 = '';

// -- MaritalDAL
$Marital = new MaritalStatusDAL();
$MaritalList = $Marital->MaritalStatus($im_value);

// -- MarriageDAL
$Marriage = new MarriageDAL();
$MarriageList = $Marriage->marriage($im_value);

// -- BrokersDAL
$Broker = new BrokerDAL();
$brokerList = $Broker->Broker_IntUserCode($B_IntUserCode);
//print_r($brokerList);
// -- Payment Method DAL
$PaymentMethod = new PaymentMethodDAL();
$PaymentMethodList = $PaymentMethod->PaymentMethod_IntUserCode($B_IntUserCode);
//print_r($PaymentMethodList);
// -- Comms Method DAL
$CommsMethod = new CommsMethodDAL();
$CommsMethodList = $CommsMethod->CommsMethod_IntUserCode($B_IntUserCode);
// -- BranchesDAL
$Branch = new BranchDAL();
$BranchList = $Branch->Branch_IntUserCode($B_IntUserCode);
//print_r($BranchList);


// -- PolicyDAL options
$Policy = new policyDAL();
$PolicyList = $Policy->policy_IntUserCode($B_IntUserCode);

// -- MemberDAL and BLL 
$MemberDAL = new MemberDAL();
$MemberBO  = new MemberBLL();

// -- AddressDAL and BLL
$AddressDAL = new AddressDAL();
$AddressBO = new AddressBLL();

/* -- Database Declarations and config:*/
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM bank';
$databank= $pdo->query($sql);

$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM entry_class';
$dataentry_class = $pdo->query($sql);

require 'database_ffms.php';
$pdo_ffms = db_connect::db_connection();
$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM province';
$q = $pdo_ffms->prepare($sql);
$q->execute(array());
$provinceArr = $q->fetchAll(PDO::FETCH_ASSOC);

$notification = array();
$notificationsList = array();

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
$No_Payments   ='';
$Amount        ='';
$Account_holder ='';
$Account_type ='';
$Account_Number='';
$Branch_Code   ='';
$Bank='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class    ='';
$Bank_Reference ='';
$contact_number = '';
$Notify 		= '';
$Brush_Cutter  		= '';
$Shovels  		= '';
$Scale  		= '';
$Lasher_Bows  		= '';
$Extinguishers  		= '';
$Axes  		= '';
$Wheelbarrows  		= '';
$Chainsaws 		= '';
$Spades  		= '';
$Kilns  		= '';


//$paymentmethod  = '';
$backHTML = "display:none";
$im_member_id='';
$im_key_member_id='';
$im_cellphone='';
$im_email='';
$im_member_id='';
$Commence_date='';
$Debit_date='';



$entry_classSelect = '';
$message = '';
/* -- dropdown selection. */
$Client_ID_TypeArr = array();
//$Client_ID_TypeArr[] = '';
// -- Member not responsible for account payemnts-  added 2023/07/09
$debtor_identification 			 = '';
$debtor_id_type 			 = '';
$debtor_contact_number 			 = '';
$debtor_email 			 = '';
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_desc'];
}

/*Title Read tile from Db */ 
$im_titleArr[] = 'Mr'.'|'.'Mr';
$im_titleArr[] = 'Mrs'.'|'.'Mrs';
$im_titleArr[] = 'Ms'.'|'.'Ms';
$im_titleArr[] = 'Dr'.'|'.'Dr';
$im_titleArr[] = 'Prof'.'|'.'Prof';


/*Title Read tile from Db */ 
$im_companyArr[] = 'Non-Profit Organisation'.'|'.'NPO';
$im_companyArr[] = 'Sole Proprieto'.'|'.'Sole Proprietor';
$im_companyArr[] = 'Private Company'.'|'.'(Pty) Ltd';
$im_companyArr[] = 'Personal Liability Company'.'|'.'Inc';
$im_companyArr[] = 'Public Companies'.'|'.'(Ltd.)';
$im_companyArr[] = 'State Owned Companies'.'|'.'(SOC.)';

foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

/*Country*/
$countryArr[] = 'South Africa'.'|'.'South Africa';

/*City*/
$cityArr[] 	  = 'Johannesburg'.'|'.'Johannesburg';
$cityArr[] 	  = 'Pretoria'.'|'.'Pretoria';
$cityArr[] 	  = 'Polokwane'.'|'.'Polokwane';
$cityArr[] 	  = 'Bloemfontein'.'|'.'Bloemfontein';
$cityArr[] 	  = 'Botshabelo'.'|'.'Botshabelo';
$cityArr[] 	  = 'Brandfort'.'|'.'Brandfort';
$cityArr[] 	  = 'Thaba-Nchu'.'|'.'Thaba-Nchu';


$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$bankArr = array();
foreach($databank as $row)
{
	$bankArr[] = $row['bankid'].'|'.$row['bankname'];
}

$dataentry_classArr = Array();
foreach($dataentry_class as $row)
{
	$dataentry_classArr[] = $row['identry_class'].'|'.$row['Entry_Class_Des'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
$FrequencyArr[] = 'Last Working day of the Month';

//$paymentmethodArr[] = 'Debit Order';
//$paymentmethodArr[] = 'Debit Check';
//$paymentmethodArr[] = 'Persal';


$NotifyArr[] = 'Yes';
$NotifyArr[] = 'No';

$arrayInstruction = null;

$UserCode = null;
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
		$CustomerId = getsession('CustomerId');
	
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}
//-------------------------------------------------------------------
//           				DECLARATIONS-update 	    			-
//-------------------------------------------------------------------	
	$emailFlag = '';
	$tenantid = '';
	$customerid = '';
	$ApplicationId = '';
// ================================ BOC - tenantid =========================
//$tenantid = 'UFRF';
if ( !empty($_GET['tenantid']))
	{
		$tenantid = $_REQUEST['tenantid'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['tenantid']))
	 {
	    $tenantid = $_POST['tenantid'];
	 }
	}
   if( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['ApplicationId']))
	 {
	    $ApplicationId = $_POST['ApplicationId'];
	 }
	}	
	
	//echo $tenantid;
// ================================ EOC - tenantid =========================
//$customerid = '84080355450866'; //'620103554586';//
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		/*if(isurlencode($customerid))
		{
			$customerid = base64_decode(urldecode($customerid));
		}*/
	}
	else
	{
		if(isset($_POST['customerid']))
		  {
		  	$customerid = $_POST['customerid'];
			/*if(isurlencode($customerid))
			{
				$customerid = base64_decode(urldecode($customerid));
			}*/
		  }
	}

// Declarations - Owner
$im_owner_id = '';
$im_title = '';
$im_company = '';
$im_first_name = '';
$im_surname = '';
$im_ownership_type =''; 
$im_Shareholding =''; 
$tableChildren 				 = '';

// key contact person in your business
$im_key_title ='';
$im_key_first_name ='';
$im_key_surname ='';
$im_key_cellphone ='';
$im_key_email = '';

//Business Information
$im_business_name='';
$im_business_cipc_no='';
$im_business_type='';
$im_products_offered='';
$im_business_location='';
//Business Address
$im_line_1='';
$im_line_2='';
$im_line_3='';
$im_line_4='';
$im_line_5='';
//Stage of business
$im_stage_less_than_year='';
$im_stage_between_one_and_three_years='';
$im_stage_greater_than_three_years='';

//Business Banking Details
$Account_holder ='';
$Account_type ='';
$Account_Number='';
$Branch_Code   ='';
$Bank='';
$Personal_number='';
$Account_number 			 = '';	

//Business Annual Firewood/Charcoal Turnover
$im_turnover_100000='';
$im_turnover_200000='';
$im_turnover_300000='';
$im_turnover_500000='';
$im_turnover_500001='';
$im_annual_sales='';
$im_assets_value='';
//Material
$im_Kilns=''; 
$im_Spades=''; 
$im_Chainsaws=''; 
$im_Wheelbarrows=''; 
$im_Axes=''; 
$im_Extinguishers=''; 
$im_Lasher_Bows=''; 
$im_Scale=''; 
$im_Shovels=''; 
$im_Brush_Cutter='';
$OtherEquipments=''; 
//Registrations
$im_compensation_fund_no=''; 
$im_compensation_fund_yes=''; 
$im_uif_no=''; 
$im_uif_yes=''; 
$im_vat_no=''; 
$im_vat_yes=''; 
$im_paye_no=''; 
$im_paye_yes=''; 
$im_income_tax_no=''; 
$im_income_tax_yes=''; 
$im_income_tax_number=''; 
$im_accounting_officer_no=''; 
$im_accounting_officer_yes=''; 
$im_businessplan_no=''; 
$im_im_businessplan_yes=''; 
$im_cash_flow_no=''; 
$im_cash_flow_yes=''; 

//Purpose of Financing
$im_startup_cost_yes=''; 
$im_asset_acquisition_yes=''; 
$im_working_yes=''; 
$im_funding_yes=''; 

//Owners' contribution
$im_invested_yes='';
$im_invested_no='';
$im_invested_free_text='';

//Consent
$im_consent='';
//-------------------------------------------------------------------	
// --- Check if the application does not exist...

?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>
<div class="container background-white bottom-border">
<br/>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
          <a id="step1"  href="#step-1" type="button" class="btn btn-primary btn-circle inactiveLink">1</a>
					<p><strong>Welcome</strong></p>

        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
            <p><strong>Owner Profile</strong></p>
        </div>
        <div class="stepwizard-step">
            <a id="step3" href="#step-3" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
            <p><strong>Business profile</strong></p>
        </div>
       <!-- add for demo-->
	   <div class="stepwizard-step">
            <a id="step-3-demo" href="#step-3-demo" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">4</a>
            <p><strong>Declaration</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-6" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">5</a>
            <p><strong>Review</strong></p>
        </div>
		<div class="stepwizard-step">
			<a href="#step-7" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
			<p><strong>Complete</strong></p>
		</div>
		<!--<div class="stepwizard-step">
				<a href="#step-6" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
				<p><strong>Activation</strong></p>
		</div>
		<div class="stepwizard-step">
			<a href="#step-7" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
			<p><strong>Complete</strong></p>
		</div>-->
    </div>
</div>
</br>
<form name="sme_Applicationform" id="sme_Applicationform" method="POST" action="#" style="display:block" role="form">
<?php include $filerootpath.'/step1_sme_application.php'; ?>
<?php include $filerootpath.'/step2_sme_application.php'; ?>
<?php include $filerootpath.'/step3_sme_application.php'; ?>
<?php include $filerootpath.'/step3_sme_demo_application.php'; ?>
<?php include $filerootpath.'/step4_sme_application.php'; ?>
<?php include $filerootpath.'/step5_sme_application.php'; ?>
<div class="row setup-content" id="step-6">
 <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_confirmation" name="message_confirmation" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div></div></div>
<?php include $filerootpath.'/confirm_sme_application.php'; ?>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Apply</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
 <div class="row setup-content" id="step-7">
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Create new application</strong></button>
			  </div>			  
			  <!-- <div class="col-md-6">
			    <a name="ffms_link" id="ffms_link" class="btn btn-info"  target="_blank" href="#">Policy Schedule</a>
			  </div> -->
			</div>
			</div>
			<br/>
			</div></div>
			 <br/>
			 <!--<a href="finApplicationform" class="btn btn-primary nextBtn btn-lg pull-right" name="PayNow" id="PayNow" type="button" ><strong>Create new application</strong></a>-->
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary btn-lg pull-right" name="Exit" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>
</div>

</form>
</div>
<script>
//Dependants
// -- 18 September 2021 -- //
function servicetypeselect(sel)
{
		/* 	var message_info   = document.getElementById('rulealert');
			message_info.className  = '';
			message_info.innerHTML  = '';

			var Service_type = 'TWO DAY';//sel.options[sel.selectedIndex].value;
			if(Service_type == 'TWO DAY')
			{
				// -- Call the rules
				applyrules(sel);
			}
			// -- 13.11.2021
			if(Service_type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			} */
}
function applyrules_sameday(sel)
{
		
}
function applyrules(sel)
{

}

function valueselect(sel)
 {
		/* var Service_Mode = sel.options[sel.selectedIndex].value;
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);
							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});
			  return false; */
  }

  $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
		allpreviousBtn = $('.previousBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

	allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;
		//alert(objToString(allpreviousBtn));

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

 });


    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-6')
			{
				var message_info 	   = document.getElementById('message_info');
				message_info.innerHTML = '';
				message_info.className = '';
				
				/*var message_confirmation 	   = document.getElementById('message_confirmation');
				message_confirmation.innerHTML = '';
				message_confirmation.className = '';*/
				
				//create application
				ret2 = 0;
				ret2 = check_duplicate();
				//alert(ret2);
				if(ret2 === 1)
				{
					isValid = false;
				}
				
			//add_confirm_instruction_sme();
			}
			// -- files uploads
			if(curStepBtn == 'step-5')
			{
				//if(!validateFiles())
			//	{
				//	isValid = false;
			//	}
			add_confirm_instruction_sme();
			}
			// -- Dependant/Extend Family/Benefiaciary...
			if(curStepBtn == 'step-3')
			{
				//if(check_dependent())
				//{
				//	isValid = false;
				//}

				//check_dependent();
			}
			if(curStepBtn == 'step-3-demo')
			{
				//if(check_dependent())
				//{
				//	isValid = false;
				//}

				//check_dependent();
			}
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus();
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');

		//add_confirm_instruction();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});


function maxLengthNumber(el,maxlength)
{
	//alert(maxlength);
	if(el.value.length == maxlength)
	{
		return false;
	}
}
function toStep1()
{
location.reload();
document.getElementById("step1").click();
}



function toStep3()
{
	//alert("go to three");
document.getElementById("step3").click();
}
//hide and show fields
function debtor(z)
{
/* //alert(z);
	if(z==0)
	{
	document.getElementById('Debtor').style.display='block';
	}
	else
	{
	document.getElementById('Debtor').style.display='none';
	}
return; */
}
//hide and show fields- Member Migration--2024/04/14
function policystatus(z)
{
/* //alert(z);
	if(z==0)
	{
	document.getElementById('MemberStatus').style.display='block';
	}
	else
	{
	document.getElementById('MemberStatus').style.display='none';
	}
return; */
}


function clear_tables()
{
/* // 1-- main_member
   $("#main_member").find("tr:gt(0)").remove();

  var tablemain_member = document.getElementById("main_member");
  var main_member_row = tablemain_member.insertRow(); 
  var fullname = main_member_row.insertCell(0);
  var idno     = main_member_row.insertCell(1);
  var premium  = main_member_row.insertCell(2);
// -- Data..
	fullname.innerHTML  = "Surname and Name";
    idno.innerHTML      = "ID No";
    premium.innerHTML   = "Premium";
// 2-- spouse
   $("#spouse").find("tr:gt(0)").remove();

  var tableSpouse 	 = document.getElementById("spouse");
  var Spouse_row = tableSpouse.insertRow();
  var fullname = Spouse_row.insertCell(0);
  var idno     = Spouse_row.insertCell(1);
  var Date_Joined = Spouse_row.insertCell(2);
// -- Data..
	fullname.innerHTML  	= "Surname and Name";
    idno.innerHTML      	= "ID No";
    Date_Joined.innerHTML   = "Date Joined";
// 3-- Children
     $("#Children").find("tr:gt(0)").remove();

  var tableChildren	 = document.getElementById("Children");
  var Spouse_row = tableChildren.insertRow();
  var fullname = Spouse_row.insertCell(0);
  var idno     = Spouse_row.insertCell(1);
  var Date_Joined = Spouse_row.insertCell(2);
// -- Data..
	fullname.innerHTML  	= "Surname and Name";
    idno.innerHTML      	= "ID No";
    Date_Joined.innerHTML   = "Date Joined";
    // 4-- Extended
     $("#Extended").find("tr:gt(0)").remove();
		
// -- Extended  confirm adding...
var tableExtended = document.getElementById("Extended");
var Extended_row = tableExtended.insertRow();
// -- Extended_row -- //
	var fullname = Extended_row.insertCell(0);
    var idno     = Extended_row.insertCell(1);
    var Waiting_Period = Extended_row.insertCell(2);
    var Benefits = Extended_row.insertCell(3);
    var Date_Joined = Extended_row.insertCell(4);
// -- Data..
	fullname.innerHTML  	  = "Surname and Name";
    idno.innerHTML      	  = "ID No";
    Waiting_Period.innerHTML  = "Waiting_Period";
    Benefits.innerHTML  	  = "Benefits";
    Date_Joined.innerHTML     = "Date Joined";
// 5-- benefiary
   $("#benefiary").find("tr:gt(0)").remove();

  var tablebenefiary = document.getElementById("benefiary");
  var main_member_row = tablebenefiary.insertRow(); 
  var fullname = main_member_row.insertCell(0);
  var idno     = main_member_row.insertCell(1);
// -- Data..
	fullname.innerHTML  = "Surname and Name";
    idno.innerHTML      = "ID No"; */
}

//var doc = new jsPDF();
// -- Dependant
function childrenRow() {
var dependents_pk = [];
var table = document.getElementById("directors");
var tbodyRowCount = $("#directors > tbody > tr").length;
var tableDependent = table.tBodies[0];
const $tableID = $('#directors1'); 
var i=0;
$('.table-add').on('click', 'i', () => {
const $clone = $tableID.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
if ($tableID.find('tbody tr ').length ===0) {
	$('tbody').append(newTr);
}
	$tableID.find('#directors').append($clone);
});

for (i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}
//Dependants


const $BTN = $('#export-btn'); 
const $EXPORT = $('#export');
const newTr =  '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';
const newTr3 = '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';



$tableID.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});

$tableID.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    }); 
  
// A few jQuery helpers for exporting only jQuery.fn.pop= [].pop;
jQuery.fn.shift = [].shift;
$BTN.on('click', () => {
    const $rows =$tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = []; // Get the headers(add special header logic here)
	 $($rows.shift()).find('th:not(:empty)').each(function() {
        headers.push($(this).text().toLowerCase());
    }); // Turn all existing rows into a loopable array
	 $rows.each(function() {
        const $td = $(this).find('td');
        const h = {}; // Use the headers from earlier to name our hash keys
		 headers.forEach((header, i) => {
            h[header] = $td.eq(i).text();
        });
        data.push(h);
    }); // Output the result
    $EXPORT.text(JSON.stringify(data));
});




}





function add_confirm_instruction_sme()
{
var today  = <?php echo "'".date('Y-m-d')."'"; ?>;
var userid = <?php echo "'".$CustomerId."'"; ?>;
//alert(userid);

document.getElementById('member_applicationdate_Lbl').innerHTML = today;
document.getElementById('submittedby_name_Lbl').innerHTML = userid;
document.getElementById('key_person_Lbl').innerHTML   = document.getElementById('im_key_first_name').value;
document.getElementById('contact_Lbl').innerHTML   = document.getElementById('im_key_cellphone').value;

}
function checkIfDuplicateExists(arr) {
    return new Set(arr).size !== arr.length
}
function check_dependent()
{
var ret = false;
var dependents_pk = [];
var message_info = document.getElementById('message_extended');
var row = 0;
message_info.innerHTML = '<br/>';
message_info.className = '';							 

////////////////////////////////////////////////////////////////////////
// -- Dependant
var table = document.getElementById("dependent");
var tbodyRowCount = $("#dependent > tbody > tr").length;
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{
	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'3',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	row = row + 1;

	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							 message_info.innerHTML = '<br/>'+feedback;
							 message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates DEPENDANT DETIALS Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}	
/////////////////////////////////////////////////////////////////////////////
// -- Extetended family
////////////////////////////////////////////////////////////////////////
if(!ret){
var table = document.getElementById("extendend");
var tbodyRowCount = $("#extendend > tbody > tr").length;
var tableDependent = table.tBodies[0];
var dependents_pk = [];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{

	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'4',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	
	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates EXTENDED FAMILY DETAILS Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}}	
/////////////////////////////////////////////////////////////////////////////
// -- benefiaciary 
////////////////////////////////////////////////////////////////////////
if(!ret){
var table = document.getElementById("benefiaciary1");
var tbodyRowCount = $("#benefiaciary > tbody > tr").length;
var tableDependent = table.tBodies[0];
var dependents_pk = [];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{
	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'4',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates NOMINATED BENEFIACIARY Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}}	
////////////////////////////////////////////////////////////////////////
//alert(message_info.innerHTML );
			return ret; 
}
function check_duplicate()
{
// 1 -- MemberID	
var member =
{ 
'Member_id':document.getElementById('im_member_id').value};

var feedback = '';
var ret = 0;
var update1 = <?php echo "'".$backHTML."'"; ?>;
//alert(update1);
var update = {'update':update1}

				//--script_checkduplicate_ffms.php
				jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"script_checkduplicate_sme.php",
									   data:{member:member,update:update},
										success: function(data)
										{
										 feedback = data;
										 
										 if(feedback === '')
										 {
											 feedback  = 'Are you sure you want to proceed with submitting the application ?';
											 ret = 0;
										 }
										 else
										 {
											ret = 1;											 
											var message_info       = document.getElementById('message_info');
											message_info.innerHTML = feedback;
											message_info.className = 'alert alert-error';
										 }
										if(ret == 0)
										{
										if (confirm(feedback))
										{
										//alert("calling call_script_sme_application");
										call_script_sme_application();
										}
										else
										{
										  //-- Do nothing!
										    toStep1();
										}}
						}});
				});
				return ret;
}
function validateFiles()
{
	var ret = true;
	var fileInput1 = document.getElementById('files1').value;
	var fileInput2 = document.getElementById('files2').value;
	var fileInput3 = document.getElementById('files3').value;
	var fileInput4 = document.getElementById('files4').value;
	var fileInput5 = document.getElementById('files5').value;
	var fileInput6 = document.getElementById('files6').value;
	var fileInput7 = document.getElementById('files7').value;
	var fileInput8 = document.getElementById('files8').value;
	var fileInput9 = document.getElementById('files9').value;
	var fileInput10 = document.getElementById('files10').value;
	var fileInput11 = document.getElementById('files11').value;
	var fileInput12 = document.getElementById('files12').value;
	var fileInput13 = document.getElementById('files13').value;
	var fileInput14 = document.getElementById('files14').value;
	var fileInput15 = document.getElementById('files15').value;
	var fileInput16 = document.getElementById('files16').value;
	var fileInput17 = document.getElementById('files17').value;
	var fileInput18 = document.getElementById('files18').value;
	var fileInput19 = document.getElementById('files19').value;
	var fileInput20 = document.getElementById('files20').value;
	var fileInput21 = document.getElementById('files21').value;
	var fileInput22 = document.getElementById('files22').value;
    var message_info = document.getElementById('message_infoUploadfile');

	
	var file1 = 'Certified ID copies';
	var file2 = 'Affidavit from directors/members that they are aware of the contents of the application form';
	var file3 = 'Curriculum Vitae(s) of all the Directors';
	var file4 = 'CIPC Registration Certificate';
	var file5 = 'Tax Clearance Certificate';
	var file6 = 'Letter of Good Standing with Compensation Fund';
	var file7 = 'Unemployment Insurance Fund Compliance Certificate';
	var file8 = 'Valid PAYE Registration Certificate';
	var file9 = 'Valid VAT Certificate';
	var file10 = 'Personal Bank Statements for the past 12 months';
	var file11 = 'Official Business Bank Confirmation Letter';
	var file12 = 'Business Bank Statements for the past 12 months';
	var file13 = 'Attached Personal Credit Report';
	var file14 = 'Attached Business Credit Report';
	var file15 = 'BBBEEE Affidavit or Certificate';
	var file16 = 'Tribal Authority letter - Permission to use land';
	var file17 = 'A valid signed offtake agreement';
	var file18 = 'Expressed interest in establishing a charcoal focused business';
	var file19 = 'Approved Business Plan';
	var file20 = 'Amount of biomass produced within the last twelve (12) months';
	var file21 = 'Amount of charcoal produced within the last twelve (12) months';
	var file22 = 'Amount of revenue generated within the last twelve (12) months';

	 	message_info.innerHTML = '';
		message_info.className  = '';
	
  if(!validFileExt(fileInput1))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file1 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;	  
  }
  else if(!validFileExt(fileInput2))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file2 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput3))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file3 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput4))
  {
		message_info.innerHTML = 'Invalid file format for '+ file4 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput5))
  {
		message_info.innerHTML = 'Invalid file format for '+ file5 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput6))
  {
		message_info.innerHTML = 'Invalid file format for '+ file6 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput7))
  {
		message_info.innerHTML = 'Invalid file format for '+ file7 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput8))
  {
		message_info.innerHTML = 'Invalid file format for '+ file8 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput9))
  {
		message_info.innerHTML = 'Invalid file format for '+ file9 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput10))
  {
		message_info.innerHTML = 'Invalid file format for '+ file10 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput11))
  {
		message_info.innerHTML = 'Invalid file format for '+ file11 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput12))
  {
		message_info.innerHTML = 'Invalid file format for '+ file12 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput13))
  {
		message_info.innerHTML = 'Invalid file format for '+ file13 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput14))
  {
		message_info.innerHTML = 'Invalid file format for '+ file14 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput15))
  {
		message_info.innerHTML = 'Invalid file format for '+ file15 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput16))
  {
		message_info.innerHTML = 'Invalid file format for '+ file16 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput17))
  {
		message_info.innerHTML = 'Invalid file format for '+ file17 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput18))
  {
		message_info.innerHTML = 'Invalid file format for '+ file18 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput19))
  {
		message_info.innerHTML = 'Invalid file format for '+ file19 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput20))
  {
		message_info.innerHTML = 'Invalid file format for '+ file20 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput21))
  {
		message_info.innerHTML = 'Invalid file format for '+ file21 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput22))
  {
		message_info.innerHTML = 'Invalid file format for '+ file22 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  
if(ret){
	filesize1 = document.getElementById('files1').files[0].size;
	filesize2 = document.getElementById('files2').files[0].size;
	filesize3 = document.getElementById('files3').files[0].size;
	filesize4 = document.getElementById('files4').files[0].size;
	filesize5 = document.getElementById('files5').files[0].size;
	filesize6 = document.getElementById('files6').files[0].size;
	filesize7 = document.getElementById('files7').files[0].size;
	filesize8 = document.getElementById('files8').files[0].size;
	filesize9 = document.getElementById('files9').files[0].size;
	filesize10 = document.getElementById('files10').files[0].size;
	filesize11 = document.getElementById('files11').files[0].size;
	filesize12 = document.getElementById('files12').files[0].size;
	filesize13 = document.getElementById('files13').files[0].size;
	filesize14 = document.getElementById('files14').files[0].size;
	filesize15 = document.getElementById('files15').files[0].size;
	filesize16 = document.getElementById('files16').files[0].size;
	filesize17 = document.getElementById('files17').files[0].size;
	filesize18 = document.getElementById('files18').files[0].size;
	filesize19 = document.getElementById('files19').files[0].size;
	filesize20 = document.getElementById('files20').files[0].size;
	filesize21 = document.getElementById('files21').files[0].size;
	filesize22 = document.getElementById('files22').files[0].size;
		
	if(!validFileSize(filesize1))
	{
	 message_info.innerHTML = file1 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize2))
	{
	 message_info.innerHTML = file2 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize3))
	{
	 message_info.innerHTML = file3 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize4))
	{
	 message_info.innerHTML = file4 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize5))
	{
	 message_info.innerHTML = file5 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize6))
	{
	 message_info.innerHTML = file6 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize7))
	{
	 message_info.innerHTML = file7 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize8))
	{
	 message_info.innerHTML = file8 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize9))
	{
	 message_info.innerHTML = file9 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize10))
	{
	 message_info.innerHTML = file10 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize11))
	{
	 message_info.innerHTML = file11 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize12))
	{
	 message_info.innerHTML = file12 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize13))
	{
	 message_info.innerHTML = file13 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize14))
	{
	 message_info.innerHTML = file14 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize15))
	{
	 message_info.innerHTML = file15 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize16))
	{
	 message_info.innerHTML = file16 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize17))
	{
	 message_info.innerHTML = file17 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize18))
	{
	 message_info.innerHTML = file18 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize19))
	{
	 message_info.innerHTML = file19 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize20))
	{
	 message_info.innerHTML = file20 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize21))
	{
	 message_info.innerHTML = file21 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
		else if(!validFileSize(filesize22))
	{
	 message_info.innerHTML = file22 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	
}
	return ret; 
}
function validFileSize(filesize)
{
	var max_file_size = 1024*10000; //10MB
	if (filesize > max_file_size)
	{
		return false;
	}else{return true;}
}
function validFileExt(file)
{
    var extension = file.substr( (file.lastIndexOf('.') +1) );
	//alert(extension);
	switch(extension) {
        case 'pdf':
		   return true;
		default:
           return false;
	}
}
function change_marital(value)
{
	var marital_status = document.getElementById('im_maritalStatus').value;
	//alert(marital_status);
}	

function id_to_dob()
{
/* 	var member_id = document.getElementById('im_member_id').value;
    var current_year = new Date().getFullYear().toString().substr(-2);
	var current_year_prefix = new Date().getFullYear().toString().substr(0,2);
	var get_typed_year  = member_id.substring(0,2);
	var get_typed_month   = member_id.substring(2,4);
	var get_typed_day   = member_id.substring(4,6);
	
	var yob = "";
	var dob = "";
	if(current_year >= get_typed_year)
	{
		yob = current_year_prefix + get_typed_year;
	}
	else
	{
		 yob = "19" + get_typed_year;
	}
	//dob = yob+"-"+get_typed_month+"-"+get_typed_day;
	//alert(yob+"-"+get_typed_month+"-"+get_typed_day);
	document.getElementById("im_Date_of_birth").value = yob+"-"+get_typed_month+"-"+get_typed_day; */
}

function get_memberid()
{
  return document.getElementById('im_member_id').value;
}


function call_script_sme_application()
{
var confirm_ffms_contents = document.getElementById('confirm_sme').innerHTML;
//alert(confirm_ffms_contents);						
var userid = <?php echo "'".$CustomerId."'"; ?>;
var UserCode = <?php echo "'".$UserCode."'"; ?>;
var IntUserCode = <?php echo "'".$tenant_array['Client_Id']."'"; ?>;
//alert(member_gender_value);
var feedback = '';
var ret = 0;
var file1 = document.getElementById('files1').files[0];
//alert(document.getElementById('files1').value);	
var file2 = document.getElementById('files2').files[0];
var file3 = document.getElementById('files3').files[0];
var file4 = document.getElementById('files4').files[0];
var file5 = document.getElementById('files5').files[0];
var file6 = document.getElementById('files6').files[0];
var file7 = document.getElementById('files7').files[0];
var file8 = document.getElementById('files8').files[0];
var file9 = document.getElementById('files9').files[0];
var file10 = document.getElementById('files10').files[0];
var file11 = document.getElementById('files11').files[0];
var file12 = document.getElementById('files12').files[0];
var file13 = document.getElementById('files13').files[0];
var file14 = document.getElementById('files14').files[0];
var file15 = document.getElementById('files15').files[0];
var file16 = document.getElementById('files16').files[0];
var file17 = document.getElementById('files17').files[0];
var file18 = document.getElementById('files18').files[0];
var file19 = document.getElementById('files19').files[0];
var file20 = document.getElementById('files20').files[0];
var file21 = document.getElementById('files21').files[0];
var file22 = document.getElementById('files22').files[0];

var form = $('sme_Applicationform')[0];
var formData = new FormData(form);

// 1 -- Owner	
formData.append('Member_id' 		    ,document.getElementById('im_member_id').value);		
formData.append('title' 			 	,document.getElementById('im_title').value);	
formData.append('firstname' 			,document.getElementById('im_first_name').value);	
formData.append('surname' 				,document.getElementById('im_surname').value);	
formData.append('ownershiptype' 				,document.getElementById('im_ownership_type').value);	
formData.append('shares' 				,document.getElementById('im_Shareholding').value);
formData.append('UserCode' 				,UserCode);		
formData.append('userid' 				,userid);

// 2 -- Key Contact person
formData.append('keytitle' 		    	,document.getElementById('im_key_title').value);		
formData.append('keyfirstname' 			 	,document.getElementById('im_key_first_name').value);	
formData.append('keysurname' 			,document.getElementById('im_key_surname').value);
formData.append('keymember_id' 			,document.getElementById('im_key_member_id').value);	
formData.append('keycontactNumber' 				,document.getElementById('im_key_cellphone').value);	
formData.append('keyemailAddress' 				,document.getElementById('im_key_email').value);

//Business Information
formData.append('businessName' 		    	,document.getElementById('im_business_name').value);		
formData.append('regNumber' 			 	,document.getElementById('im_business_cipc_no').value);	
formData.append('businessType' 			,document.getElementById('im_business_type').value);	
formData.append('productsorServices' 				,document.getElementById('im_products_offered').value);	
formData.append('businesslocation' 				,document.getElementById('im_business_location').value);
//$im_business_name='';
//$im_business_cipc_no='';
//$im_business_type='';
//$im_products_offered='';
//$im_business_location='';

//Business Address
formData.append('street' 				,document.getElementById('im_line_1').value);
formData.append('suburb' 				,document.getElementById('im_line_2').value);
formData.append('city' 				,document.getElementById('im_line_3').value);
formData.append('state' 				,document.getElementById('im_line_4').value);
formData.append('postalCode' 				,document.getElementById('im_line_5').value);
//$im_line_1='';
//$im_line_2='';
//$im_line_3='';
//$im_line_4='';
//$im_line_5='';
//Application Information
formData.append('accountholdername' 				,document.getElementById('Account_holder').value);
formData.append('accounttype' 				,document.getElementById('Account_type').value);
formData.append('accountnumber' 				,document.getElementById('Account_number').value);
formData.append('bankname' 				,document.getElementById('Bank').value);
formData.append('turnover' 				,document.getElementById('im_turnover').value);
formData.append('kilns' 				,document.getElementById('im_Kilns').value);
formData.append('spades' 				,document.getElementById('im_Spades').value);
formData.append('chainsaws' 				,document.getElementById('im_Chainsaws').value);
formData.append('wheelbarrows' 				,document.getElementById('im_Wheelbarrows').value);
formData.append('scale' 				,document.getElementById('im_Scale').value);
formData.append('shovels' 				,document.getElementById('im_Shovels').value);
formData.append('brushcutter' 				,document.getElementById('im_Brush_Cutter').value);
formData.append('axes' 				,document.getElementById('im_Axes').value);
formData.append('fireExtinguishers' 				,document.getElementById('im_Extinguishers').value);
formData.append('lasherbows' 				,document.getElementById('im_Lasher_Bows').value);
formData.append('other' 				,document.getElementById('OtherEquipments').value);
formData.append('startupcost' 				,document.getElementById('im_startup_cost_amount').value);
formData.append('assetacquisition' 				,document.getElementById('im_asset_acquisition_amount').value);
formData.append('workingcapital' 				,document.getElementById('im_startup_cost_amount').value);
formData.append('consentIndicator' 				,document.getElementById('im_business_location').value);
formData.append('workingcapitalamount' 				,document.getElementById('im_working_capital_amount').value);
formData.append('fundingindicator' 				,document.getElementById('im_funding').value);
formData.append('fundingamount' 				,document.getElementById('im_funding_amount').value);
formData.append('ownerscontributionindicator' 				,document.getElementById('im_invested').value);
formData.append('ownerscontributionnotes' 				,document.getElementById('im_invested_free_text').value);
formData.append('assetacquisitionindicator' 				,document.getElementById('im_asset_acquisition').value);
formData.append('startupcostindicator' 				,document.getElementById('im_startup_cost').value);
formData.append('turnoverfirewood' 				,document.getElementById('im_annual_sales').value);
formData.append('assetvaluetodate' 				,document.getElementById('im_assets_value').value);
formData.append('compensationfundindicator' 				,document.getElementById('im_compensation_fund').value);
formData.append('uifindicator' 				,document.getElementById('im_uif').value);
formData.append('vatfindicator' 				,document.getElementById('im_vat_no').value);
formData.append('payefindicator' 				,document.getElementById('im_paye').value);
formData.append('incometaxindicator' 				,document.getElementById('im_income_tax').value);
formData.append('incometaxnumber' 				,document.getElementById('im_income_tax_number').value);
formData.append('accountingofficerindicator' 				,document.getElementById('im_accounting_officer').value);
formData.append('businessplanrindicator' 				,document.getElementById('im_businessplan').value);
formData.append('managementaccountsindicator' 				,document.getElementById('im_management_accounts').value);
formData.append('cashflowindicator' 				,document.getElementById('im_cash_flow').value);
formData.append('funding_prev' 				,document.getElementById('im_funding_prev').value);
formData.append('funding_desc' 				,document.getElementById('im_funding_details').value);


//alert(document.getElementById('im_stage').value);

//Stage of business
formData.append('stageOfBusiness' 				,document.getElementById('im_stage').value);
//$im_stage_less_than_year='';
//$im_stage_between_one_and_three_years='';
//$im_stage_greater_than_three_years='';


//Files
// Files
formData.append("File1", file1);
formData.append("File2", file2);
formData.append("File3", file3);
formData.append("File4", file4);
formData.append("File5", file5);
formData.append("File6", file6);
formData.append("File7", file7);
formData.append("File8", file8);
formData.append("File9", file9);
formData.append("File10", file10);
formData.append("File11", file11);
formData.append("File12", file12);
formData.append("File13", file13);
formData.append("File14", file14);
formData.append("File15", file15);
formData.append("File16", file16);
formData.append("File17", file17);
formData.append("File18", file18);
formData.append("File19", file19);
formData.append("File20", file20);
formData.append("File21", file21);
formData.append("File22", file22);


var table = document.getElementById("directors");
var tbodyRowCount = $("#directors > tbody > tr").length;
if(tbodyRowCount > 0)
{
	var tableDependent = table.tBodies[0];
	var row = 0;
	for (let i = 0; i < tbodyRowCount; i++) 
	{
		//alert(tableDependent.rows[i].cells[0].innerHTML);
		//alert(tableDependent.rows[i].cells[1].innerHTML);
		//alert(tableDependent.rows[i].cells[2].innerHTML);
		//alert(tableDependent.rows[i].cells[3].innerHTML);
		//alert(tableDependent.rows[i].cells[4].innerHTML);
		formData.append('Title_director'+row,tableDependent.rows[i].cells[0].innerHTML);		
		formData.append('firstname_director'+row,tableDependent.rows[i].cells[1].innerHTML);
		formData.append('surname_director'+row,tableDependent.rows[i].cells[2].innerHTML);
		formData.append('id_director'+row,tableDependent.rows[i].cells[3].innerHTML);
		formData.append('Shareholding'+row,tableDependent.rows[i].cells[4].innerHTML);
		row = row + 1;
	}
}
formData.append('rows',row);




//Business Banking Details
/* formData.append('accountHolderName' 				,document.getElementById('Account_holder').value);
formData.append('accountType' 				,document.getElementById('Account_type').value);
formData.append('accountNumber' 				,document.getElementById('Account_Number').value);
formData.append('branchCode' 				,document.getElementById('Branch_Code').value);
formData.append('bankName' 				,document.getElementById('Bank').value); */
//Account_holder ='';
//Account_type ='';
//$Account_Number='';
//$Branch_Code   ='';
//$Bank='';
//$Personal_number='';
//formData.append('turnover' 				,document.getElementById('im_turnover').value);
//Business Annual Firewood/Charcoal Turnover
//$im_turnover_100000='';
//$im_turnover_200000='';
//$im_turnover_300000='';
//$im_turnover_500000='';
//$im_turnover_500001='';
/* formData.append('annualsales' 				,document.getElementById('im_annual_sales').value);
formData.append('assetsvalue' 				,document.getElementById('im_assets_value').value); */
//$im_annual_sales='';
//$im_assets_value='';
//Material
/* formData.append('kilns' 				,document.getElementById('$im_Kilns').value);
formData.append('spades' 				,document.getElementById('$im_Spades').value);
formData.append('chainsaws' 				,document.getElementById('$im_Chainsaws').value);
formData.append('wheelbarrows' 				,document.getElementById('$im_Wheelbarrows').value);
formData.append('axes' 				,document.getElementById('$im_Axes').value);
formData.append('fireExtinguishers' 				,document.getElementById('$im_Extinguishers').value);
formData.append('lasherbows' 				,document.getElementById('$im_Lasher_Bows').value);
formData.append('scale' 				,document.getElementById('$im_Scale').value);
formData.append('shovels' 				,document.getElementById('$im_Shovels').value);
formData.append('brushcutter' 				,document.getElementById('$im_Brush_Cutter').value);
formData.append('other' 				,document.getElementById('$OtherEquipments').value); */
//$im_Kilns=''; 
//$im_Spades=''; 
//$im_Chainsaws=''; 
//$im_Wheelbarrows=''; 
//$im_Axes=''; 
//$im_Extinguishers=''; 
//$im_Lasher_Bows=''; 
//$im_Scale=''; 
//$im_Shovels=''; 
//$im_Brush_Cutter='';
//$OtherEquipments=''; 
//formData.append('compensationfund' 				,document.getElementById('$im_compensation_fund').value);
//formData.append('uifIndicator' 				,document.getElementById('$im_uif').value);
//formData.append('vatIndicator' 				,document.getElementById('$im_vat_no').value);
//formData.append('payeIndicator' 				,document.getElementById('$im_paye').value);
//formData.append('incomeTaxIndicator' 				,document.getElementById('$im_income_tax').value);
//formData.append('incomeNumber' 				,document.getElementById('$im_income_tax_number').value);
//formData.append('accountingOfficerIndicator' 				,document.getElementById('$im_accounting_officer').value);
//formData.append('businessPlanIndicator' 				,document.getElementById('$im_businessplan').value);
//formData.append('managementAccountIndicator' 				,document.getElementById('$im_management_accounts').value);
//formData.append('cashflowProjectionsIndicator' 				,document.getElementById('$im_cash_flow').value);
//Registrations
//$im_compensation_fund_no=''; 
//$im_compensation_fund_yes=''; 
//$im_uif_no=''; 
//$im_uif_yes=''; 
//$im_vat_no=''; 
//$im_vat_yes=''; 
//$im_paye_no=''; 
//$im_paye_yes=''; 
//$im_income_tax_no=''; 
//$im_income_tax_yes=''; 
//$im_income_tax_number=''; 
//$im_accounting_officer_no=''; 
//$im_accounting_officer_yes=''; 
//$im_businessplan_no=''; 
//$im_im_businessplan_yes=''; 
//$im_cash_flow_no=''; 
//$im_cash_flow_yes=''; 
//formData.append('startupcostindicator' 				,document.getElementById('$im_startup_cost').value);
//formData.append('workingcapital' 				,document.getElementById('$im_startup_cost_amount').value);
//formData.append('assetacquisitionindicator' 				,document.getElementById('$im_asset_acquisition').value);
//formData.append('assetacquisition' 				,document.getElementById('$im_asset_acquisition_amount').value);
//formData.append('workingcapital' 				,document.getElementById('$im_working_capital').value);
//formData.append('workingcapitalamount' 				,document.getElementById('$im_working_capital_amount').value);
//formData.append('fundingindicator' 				,document.getElementById('$im_funding').value);
//formData.append('fundingamount' 				,document.getElementById('$im_funding_amount').value);

//Purpose of Financing
//$im_startup_cost_yes=''; 
//$im_asset_acquisition_yes=''; 
//$im_working_yes=''; 
//$im_funding_yes=''; 
//formData.append('ownerscontributionindicator' 				,document.getElementById('$im_invested').value);
//formData.append('ownerscontributionnotes' 				,document.getElementById('$im_invested_free_text').value);
//Owners' contribution
//$im_invested_yes='';
//$im_invested_no='';
//$im_invested_free_text='';
//formData.append('consentIndicator' 				,document.getElementById('$im_consent').value);
//Consent
//$im_consent='' */;
/*
/* formData.append('spouse_id' 			,document.getElementById('im_spouse_id').value);			 
formData.append('spousetitle'			,document.getElementById('im_spousetitle').value);
formData.append('spousefirst_name' 		,document.getElementById('im_spousefirst_name').value);
formData.append('spousemiddle_name' 	,document.getElementById('im_spouseMiddle_name').value);	
formData.append('spouseinitials' 		,document.getElementById('im_spouseinitials2').value);
formData.append('spousesurname' 		,document.getElementById('im_spousesurname').value);
formData.append('spousegender' 			,'');//document.getElementById('im_spousegender').value,
formData.append('spousedate_of_birth' 	,'');//document.getElementById('im_spouseDate_of_birth').value,
formData.append('spouseaddress' 		,'');//document.getElementById('im_spouseaddress').value,
formData.append('spouselife_State' 		,'1');//document.getElementById('im_spouseLife_State').value,
formData.append('spouselanguage_id' 	,'2');//document.getElementById('im_spouselanguage_id').value};	 */


var list =
{
'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode};


//var formdata = new FormData(this);
jfinApplication = jQuery.noConflict( true );
// -- Check Duplicate Payment to benefiaciary.
		jfinApplication(document).ready(function()
		{
			
		jfinApplication.ajax({  type: "POST",
					   url:"script_create_sme_application.php",
				       data:/*{member:member,spouse:spouse,
							 address:address,source_of_income:source_of_income,
							 banking_details:banking_details,list:list,member_policy:member_policy
							},*/
							formData,
							 //  dataType:'json', // -- 1.Comment out for debugging
							contentType:false,
							cache:false,
							 processData:false,
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

							//alert(feedback);
							backtoconfirm = document.getElementById("backtoconfirm");
							//addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								var res = feedback.split("|");
								//alert(feedback);
								var update = <?php echo "'".$backHTML."'"; ?>;
								if(update == 'display:none'){message_info.innerHTML = 'Application submitted successfully for '+document.getElementById('im_business_name').value+'.';}
								else{message_info.innerHTML = 'Application updated successfully for '+document.getElementById('im_business_name').value+'.';}	
								message_info.className  = 'status';
								//PayNow.style.visibility = '';
								//Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								//addbeneficiary.style.visibility = '';
								if(typeof res[2] === 'undefined')
								{}else{message_info.innerHTML = message_info.innerHTML + '<br/> Member policy number is:'+(String(res[2]).padStart(4, '0'));}
								document.getElementById('bid').value = (String(res[1]).padStart(4, '0'));
							}
							else
							{
								//alert("Request failed");
								//alert(feedback);
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								//alert(feedback); --debugging
								//PayNow.style.visibility = 'hidden';
								//Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								//addbeneficiary.style.visibility = 'hidden';
							}
 
 
						 }
					});
				});

}

//Dependants
const $tableID = $('#dependent1'); 
const $tableID2 = $('#extendend1'); 
const $tableID3 = $('#benefiaciary1'); 


const $BTN = $('#export-btn'); 
const $EXPORT = $('#export');
const newTr =  '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';
const newTr3 = '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';

  $('.table-add').on('click', 'i', () => {
	const $clone = $tableID.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
		$tableID.find('#dependent').append($clone);
});
$('.table2-add').on('click', 'i', () => {

	const $clone = $tableID2.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID2.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
	$tableID2.find('#extendend').append($clone);
});
$('.table3-add').on('click', 'i', () => {
	const $clone1 = $tableID3.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID3.find('tbody tr ').length ===0) {
	$('tbody').append(newTr);
	}
	$tableID3.find('#benefiaciary').append($clone1);
});

$tableID.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID2.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID3.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID2.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID3.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    }); 
$tableID2.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });
$tableID3.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });  
// A few jQuery helpers for exporting only jQuery.fn.pop= [].pop;
jQuery.fn.shift = [].shift;
$BTN.on('click', () => {
    const $rows =$tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = []; // Get the headers(add special header logic here)
	 $($rows.shift()).find('th:not(:empty)').each(function() {
        headers.push($(this).text().toLowerCase());
    }); // Turn all existing rows into a loopable array
	 $rows.each(function() {
        const $td = $(this).find('td');
        const h = {}; // Use the headers from earlier to name our hash keys
		 headers.forEach((header, i) => {
            h[header] = $td.eq(i).text();
        });
        data.push(h);
    }); // Output the result
    $EXPORT.text(JSON.stringify(data));
});

</script>
</div>