<?php 
require 'database.php';
$count = 0;
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 
  $currency = "";
  if (isset($_SESSION['currency'])) 
  {
      $currency = $_SESSION['currency'];
  }
// -- EOC 27.01.2017
// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- BOC - Source of Income ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM sourceofincome';
 $dataSourceOfIncome = $pdo->query($sql);
 // ---------------------- EOC - Payment Frequency ------------------------------- //  

Database::disconnect();	

// --------------------- EOC 2017.04.15 ------------------------- //
// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$adminemail = "info@vogsphere.co.za";
$Status = "";
$ApplicationDate = date('Y-m-d');
$sourceofincome = "";		
// ------------ Start Send an Email ----------------//
function SendEmail()
{
require_once('class/class.phpmailer.php');

try{
// Admin E-mail
$adminemail = "info@vogsphere.co.za";
$AdminFirstName 		= 'eCashMeUp';
$AdminLastName 		= 'Admin';

			$mail=new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug=1;
			$mail->SMTPAuth=true;
			$mail->SMTPSecure="tls";
			$mail->Host="smtp.vogsphere.co.za";
			$mail->Port= 587;//465;
			$mail->Username="vogspesw";
			$mail->Password="fCAnzmz9";
			$mail->SetFrom("info@vogsphere.co.za",$AdminFirstName." (PTY) LTD");

			//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
			$mail->Subject=  "Your ".$AdminFirstName." Documentation";
			//$mail->Body= $email_text;//"This is the HTML message body <b>in bold!</b>";
			// Enable output buffering
			
ob_start();
include 'text_emailWelcomeCustomer.php';//'text_emailWelcomeCustomer.php'; //execute the file as php
$body = ob_get_clean();			
			
$fullname = $FirstName.' '.$LastName;

			// print $body;
			$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
			$mail->AddAddress($email, $fullname);

			if($mail->Send()) 
			{
			printf("<p class='status'>Your $AdminFirstName application is successfully registered. An e-mail is sent your Inbox!</p>\n");
			}
			else 
			{
			  // echo 'mail not sent';
			printf("<p class='status'>Your $AdminFirstName application was successfully registered. but your e-mail address seems to be invalid!</p>\n");
			}
			
		// -- send to Eloan Administrator
		// clear addresses of all types
			$mail->ClearAddresses();  // each AddAddress add to list
			$mail->ClearCCs();
			$mail->ClearBCCs();
			
			$mail->Subject=  "New $AdminFirstName Funder Application for approval";
			ob_start();
			include 'text_emailNotifyAdmin.php';// -- Admin 
			$body = ob_get_clean();						
			$fullname = $AdminFirstName.' '.$AdminLastName;
			
			// print $body;
			$mail->MsgHTML($body );
			$mail->AddAddress($adminemail, $fullname);
			if($mail->Send()) 
			{
			  // nothing
			}
			
		}
	catch(phpmailerException $e)
	{
		echo $e->errorMessage();
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}	
}
// ------------ End Send an Email ----------------//

// -- Generate Captcha Image
function Image($value)
{
 $generateImage = '';
 if (!empty($_POST)) 
 {
 if (isset($_POST['Apply']))
   {$generateImage = 'No';}
 }
 
 if( $generateImage == '')
 {
$string = '';
for ($i = 0; $i < 5; $i++) {
    $string .= chr(rand(97, 122));
}

$_SESSION['captcha'] = $string; //store the captcha
$_SESSION['value'] = $value;
$dir = 'fonts/';
$image = imagecreatetruecolor(165, 50); //custom image size
$font = "PlAGuEdEaTH.ttf"; // custom font style
$color = imagecolorallocate($image, 113, 193, 217); // custom color
$white = imagecolorallocate($image, 255, 255, 255); // custom background color
imagefilledrectangle($image,0,0,399,99,$white);
imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

// Enable output buffering
ob_start();
imagepng($image);
// Capture the output
$imagedata = ob_get_contents();
// Clear the output buffer
ob_end_clean();
$_SESSION['image'] = $imagedata;
}
else
{
$imagedata = $_SESSION['image'];
}

return $imagedata;
}
// -- End - Generate Captcha Image

$password1 = '';
$idnumber  = '';
$tbl_user="user"; // Table name 
$tbl_funder ="funder"; // Table name

$db_eloan = "eloan";
$captcha = '';
$modise = '';
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.
  
 if (!empty($_POST)) 
	{
	
	// Captcha declaration
	$modise = $_SESSION['captcha'];
	$captchaError = null;
	// --Captcha
	$captcha = $_POST['captcha'];
	
	 //echo "<h1>Post - $modise - $modise2 </h1>";
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$DobError = null;
		
		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //
		$Title = $_POST['Title'];
		$idnumber = $_POST['idnumber'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		$Dob		= $_POST['Dob'];
		
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);
		
		// ------------------------- $_POST - Banking Details ------------------------------------- //
		$AccountHolder = $_POST['AccountHolder'];
		$AccountType = $_POST['AccountType'];
		$AccountNumber = $_POST['AccountNumber'];
		$BranchCode = $_POST['BranchCode'];
		$BankName = $_POST['BankName'];
		// -------------------------------- Banking Details ------------------------------------------ //
		$Status = "Pending";
		$ApplicationDate = date('Y-m-d');
		$sourceofincome = $_POST['sourceofincome'];
		
		$valid = true;
			
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($idnumber,2,2) > 12) 
		{ 
			$idnumberError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{
		// Day from 1 - 30
			if (in_array(substr($idnumber,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($idnumber,4,2) > 30)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($idnumber,2,2),$months31)) 

			{
				// Day 
			   if (substr($idnumber,4,2) > 31)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($idnumber,2,2),$monthsFeb)) 
			{
				// Leap Year
				$yearValidate = parseInt(substr(0,2));
				$leapyear = ($yearValidate % 4 == 0);
				
				if($leapyear == true)
				{
					// Day 
			   if (substr($idnumber,4,2) > 29)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($idnumber,4,2) > 28)
					   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		// Phone number must be characters
		if (!is_Numeric($phone)) { $phoneError = 'Please enter valid phone numbers - digits only'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}
		
		// is Number
		if (!is_Numeric($PostCode)) { $PostCodeError = 'Please enter valid Postal Code - numbers only'; $valid = false;}

		// Is 4 numbers
		if (strlen($PostCode) > 4) { $PostCodeError = 'Please enter valid Postal Code - 4 numbers'; $valid = false;}
		
		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter/select Date of birth'; $valid = false;}
		
		// ------------------------------ BOC 01.07.2017 ------------------------------ //
		// -- Determine the loan amount, one may request via the 
		// -- loan type: amount from to amount to.
		if($valid)
		{
		
		}
		// ------------------------------ EOC 01.07.2017 ------------------------------ //
		// Captcha Error Validation.
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}

		// -- Validate Password Only if not loggedin
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
			 // nothing
			}
			else
			{
			
			$password1 = $_POST['password1'];
			
			if (empty($password1)) { $password1Error = 'Please enter Password'; $valid = false;}
			
			}
			
		// 	refresh Image
			if (!isset($_POST['Apply'])) // If refresh button is pressed just to generate Image. 
			{
				$valid = false;
			}
		$validUser = true;
	
		// Insert eCashMeUp Application Data.
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// ---------------- User ---------------
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));
			$dataFunder = $q->fetch(PDO::FETCH_ASSOC);	

			if ($q->rowCount() >= 1)
			{$validUser = false;}
			
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{ if($validUser)
			   {
			     
			   }			  
			  else
			   {
			       $email = $dataFunder['email'];				   
			   }
			}
			
			// -- BOC Check if already exist as Funder.
			$query = "SELECT * FROM $tbl_funder WHERE funderid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));
			if ($q->rowCount() >= 1)
			{$idnumberError = 'Your already exist as a Funder, Please login with your credentials.'; $valid = false;}
			// -- EOC - Funder Check.
			
			if ($valid) 
			{
			 $count = 0;
				// -- Validate Password Only if not loggedin
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
				if($validUser)
				{
					// Insert User			
					$sql = "INSERT INTO $tbl_user(userid,password,email,role) VALUES (?,?,?,'Customer')";
					$q = $pdo->prepare($sql);
					$q->execute(array($idnumber,$password1,$email));
				}
			
			// Customer - UserID & E-mail Duplicate Validation
			
			$sql = "INSERT INTO $tbl_funder (funderid,Title,fundername,contactname,phone,email,Street,Suburb,City,State,PostCode,Dob,accountholdername,bankname,accountnumber,accounttype,branchcode,FILEIDDOC,FILEPROOFADR,FILECONTRACT,status,applicationdate,sourceofincome)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$phone,$email,$Street,$Suburb,$City,$State,$PostCode,$Dob,$AccountHolder,$BankName,$AccountNumber,$AccountType,$BranchCode,"","","",$Status,$ApplicationDate,$sourceofincome));

			// -- Create a transaction history.
			$transHisDesc = "Account Balance Brought Forward";
			$transHisAmt = "0.00";
			$transHisDate = date('Y-m-d');
			
			$sql = "INSERT INTO transactionhistory(description, userid, date,amount) VALUES (?, ?, ?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($transHisDesc,$idnumber,$transHisDate,$transHisAmt));

			$count = $count + 1;
			Database::disconnect();
		// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = $password1;
												
			// Id number
			$_SESSION['idnumber']  = $idnumber;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;		
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			$_SESSION['Street'] = $Street;
			// Suburb
			$_SESSION['Suburb'] = $Suburb;
			// City
			$_SESSION['City'] = $City;
			// State
			$_SESSION['State'] = $State;
			// PostCode
			$_SESSION['PostCode'] = $PostCode;
			// Dob
			$_SESSION['Dob'] = $Dob;
			// phone
			$_SESSION['phone'] = $phone;	
			//------------------------------------------------------------------- 
			//           				LOAN APP DATA							-
			//-------------------------------------------------------------------			
			$_SESSION['applicationdate'] = $ApplicationDate;
			$_SESSION['status'] = $Status;
			$_SESSION['sourceofincome'] = $sourceofincome;

			// ------------------------- $_SESSION - Banking Details ------------------------------------- //
			$_SESSION['AccountHolder'] = $AccountHolder;
			$_SESSION['AccountType'] = $AccountType;
			$_SESSION['AccountNumber'] = $AccountNumber ;
			$_SESSION['BranchCode'] = $BranchCode;
			$_SESSION['BankName'] = $BankName;
			
			// -- Funder Logged In
			$funderLoggedIn = true;
			
				// -- funderLoggedIn
			$_SESSION['funderLoggedIn'] = $funderLoggedIn;
			
			// -- BOC Check if the user is customer as well.
			 // -- Db connect.
				$pdoDB = Database::connectDB();

				$tbl_customer = "customer";
				$db_eloan = "eloan";
				$check_user = "SELECT * FROM $tbl_customer WHERE CustomerId='$idnumber'";
				 
				 mysql_select_db($db_eloan,$pdoDB)  or die(mysql_error());
				 $result=mysql_query($check_user,$pdoDB) or die(mysql_error());
				
				// -- Mysql_num_row is counting table row
				 $count = mysql_num_rows($result);
				 
				 $customerLoggedIn = false;
				 
				// -- if the user is a customer, indicate.
				if($count >= 1)
				{
				 	$customerLoggedIn = true;
				}
				// -- customerLoggedIn
				$_SESSION['customerLoggedIn'] = $customerLoggedIn;		

			// -- EOC Check if the user is a customer as well.
		   // ------------------------------- $_SESSION Banking Details ------------------------------------------ //
			
		// --------------------------- End Session Declarations ---------------------//
		
		// --------------------------- SendEmail to the clients to inform them about the e-loan application -----//
			//SendEmail();	
		  }
		  else
		  {
			  $count = -1;
		  } 
		}
} // END of isEmpty('Post')
else
{
// -- Default Bankname - ABSA
  $BankName = "ABSA";
}
?>
<div class="container background-white bottom-border"> <!-- class="container background-white"> -->
                    <div class="margin-vert-30">
                        <!-- Register Box id="captcha-form" name="captcha-form" -->
                        <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                              <form METHOD="post"  id="captcha-form" name="captcha-form" class="signup-page"  action="applicationFunder"> 
                                <div class="signup-header">
                                    <h2 class="text-center">e-CashMeUp Application to be Funder </h2>
									<p>Applying is really quick & then we use your personal info to check your Identity and carry out a 	credit score.
									You’re applying for a small unsecured short term loan, intended to address short term financial needs.
									</p>
									<p>NOTE: * = Required Field</p>
                                    <p>Already a member? Click
                                        <a href="customerLogin">HERE</a>to login to your account.</p>
										<?php # error messages
									if (isset($message)) 
									{
										foreach ($message as $msg) {
											printf("<p class='status'>%s</p></ br>\n", $msg);
										}
									}
									# success message
									if($count !=0)
									{
										# error message
										if($count == -1)
										{
											printf("<div class=alert alert-warning'>
												<strong>Warning!</strong>
												<p class='alert alert-warning'>Funder Application Error Below!</p></div>\n", $count);
										}else
									   # success message
										{printf("<p class='status'>Funder Application captured successfully!</p>\n", $count);}
									}									   
									?>
                                </div>
								
								<!-- Start - Accordion - Alternative -->
								  <div class="panel-body">
                                    <div> 
									
								 
                                <label>Title</label>
								<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
									<OPTION value="Company">Company</OPTION>
								</SELECT>
								<br/>
                                <!-- <input class="form-control margin-bottom-20" type="text"> -->

								<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <label >ID/Passport Number
										<span class="color-red">*</span>
									</label> 
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								
								<!-- End ID number -->
								<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
									<label class="control-label">First Name</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										<?php if (!empty($FirstNameError)): ?>
										<span class="help-inline"><?php echo $FirstNameError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Last Name -->
								
								<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
									 <label>Last Name
										<span class="color-red">*</span>
										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
									<?php if (!empty($LastNameError)): ?>
										<span class="help-inline"><?php echo $LastNameError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								<!-- Contact number -->
								<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									    <span class="color-red">*</span>
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">E mail
									    <span class="color-red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Street -->
								<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
									<label class="control-label">Street</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<span class="help-inline"><?php echo $StreetError;?></span>
										<?php endif; ?>
									</div>
								</div>
														
								<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
										<label class="control-label">Suburb</label>
										<div class="controls">
							<input style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<span class="help-inline"><?php echo $SuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								
								<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
									<label class="control-label">City/Town/Township</label>   
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<span class="help-inline"><?php echo $CityError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<label>Province</label>
                                <SELECT class="form-control" id="State" name="State" size="1">
									<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->
								</SELECT>
								<br/>
								<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
									<label class="control-label">Postal Code
										<span class="color-red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
										<?php if (!empty($PostCodeError)): ?>
										<span class="help-inline"><?php echo $PostCodeError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
								<label class="control-label">Date of birth
									<span class="color-red">*</span>
								</label>
									<div class="controls">
									<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
									</div>
								</div>
								
								<label>Source of Income</label>
                                <SELECT class="form-control" id="State" name="sourceofincome" size="1">
											<?php
											$sourceofincomeid = '';
											$sourceofincomeSelect = $State;
											$sourceofincomename = '';
											foreach($dataSourceOfIncome as $row)
											{
												$sourceofincomeid = $row['sourceofincomeid'];
												$sourceofincomename = $row['sourceofincomename'];
												// Select the Selected Role 
												if($sourceofincomeid == $sourceofincomeSelect)
												{
												echo "<OPTION value=$sourceofincomeid selected>$sourceofincomename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$sourceofincomeid>$sourceofincomename</OPTION>";
												}
											}
										
											if(empty($dataSourceOfIncome))
											{
												echo "<OPTION value=0>No Source Of Income</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - sourceofincome  --->
								</SELECT>
								<br/>
								
								      </div>
								   </div>

<!------------------------------------------------------------ Banking Details --------------------------------------------------------->
							<div class="tab-pane fade in" id="sample-3b">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                                                Banking  Details
                                         </a>
                                    </h2>		
								</div>
								
							<!-- Account Holder Name -->
							<label>Account Holder Name</label>
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Bank Name -->
							<label>Bank Name</label>
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										  <SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
							<!-- Account Number  -->
							<label>Account Number
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Account Type  -->
							<label>Account Type
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1" >
								<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Account type --->
								</SELECT>

									</div>	
								</div>
								
							<!-- Branch Code  -->
							<label class="control-label">Branch Code
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
									<div class="controls">
									 <SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
									</div>
								</div>
							</div>
<!------------------------------------------------------------ End Banking Details --------------------------------------------------------->									
														<!-- End - Accordion - Alternative -->
								
								<!-- Start - Captcha -->
								<?php 
								$classname = "control-group error";
								$html = "";

								if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
                                       // echo "<a href='applicationForm.php' class='fa-gears'>Apply for e-loan</a>";
									}
									else
									{									
								echo "<div class='panel-heading'>
											<h2 class='panel-title'>
                                            <a class='accordion-toggle' href='#collapse-Two' data-parent='#accordion' data-toggle='collapse'>
												Register a new account
                                            </a>
                                            </h2>
								      </div>

									  <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input  style='height:30px' readonly id='Userid' value='$idnumber' name='Username' placeholder='ID number' class='form-control' type='text'>
									 </div>
									 
									 <label>Password
                                            <span class='color-red'>*</span>
                                     </label>";
									 if(!empty($password1Error))
									 { 
									 echo "<div class='control-group error'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password'>
									 <span class='help-inline'>$password1Error</span></div>";
									 }
									 else
									 {
									  echo "<div class='control-group'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password' value=$password1></div>";
									 }	
									  // -- New Captcha
									 echo "<label class='' for='captcha'>Please enter the verification code shown below.</label>";
									 echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' /><img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>";
									 if(!empty($captchaError))
									 {
									 echo "<div class='control-group error'>
											<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
											<span class='help-inline'>$captchaError</span></div>";
									 }
									 else
									 {
									 $code = '';
									 echo "<div class='control-group'>
									 <input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
									</div>";
									 }
									 // -- End Captcha

								}
								?>
								<!-- End - Captcha -->
										
								
								<!-- Register End here -->
                                <hr>
                                <div class='row'>
                                    <div class='col-lg-8'>
                                        <label class='checkbox'>
                                            <input type='checkbox'>I read the
                                            <a href='#'>Terms and Conditions</a>
                                        </label>
                                    </div>
                                    <div class='col-lg-4 text-right'>
                                        <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button>
                                    </div>
                                </div>
                            </form> 
							<?php
							if(!empty($_POST))
							  // if($_SERVER['REQUEST_METHOD']=='POST')
							   {
								  echo '<script type="text/javascript"> LastPaymentDate(); </script>';
							   } 
							?>
				            </div>
                         </div>
                      </div>
			      </div>
<?php  
// -- Database Connections  
//require 'database.php'; 
if(isset($_POST['Apply']))  
{  
	 $pdo = Database::connectDB(); 
	 $userid = $_POST['idnumber']; 
	 $password = $_POST['password1']; 
	 $check_user = "SELECT * FROM $tbl_user Inner Join $tbl_funder on $tbl_user.userid = $tbl_funder.funderid  WHERE userid='$userid' and password='$password'";
	 
	 mysql_select_db("$db_eloan",$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	$count = mysql_num_rows($result);
	
	if($count >= 1)
    {  
			$row = mysql_fetch_array($result);
  			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.  
			
			// Title
			$_SESSION['Title'] = $row['Title'];	
			// First name
			$_SESSION['fundername'] = $row['fundername'];
			// Last Name
			$_SESSION['contactname'] = $row['contactname'];
			// Phone numbers
			$_SESSION['phone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];
			
			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];
			
			// Dob
			$_SESSION['Dob'] = $row['Dob'];
			
			// phone
			$_SESSION['phone'] = $row['phone'];
			
			
			echo "<script>window.open('index','_self')</script>";  

    }  
    /* else  
    {  
          echo "<script>alert('Email or password is incorrect!')</script>";  
    }   */
} 
?>  				 
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

// -- Different Loan Types.
 function valueInterest(sel) 
 {
		var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
		var fromdate = sel.options[sel.selectedIndex].getAttribute('data-fromdate');
		var todate = sel.options[sel.selectedIndex].getAttribute('data-todate');
		var fromduration = sel.options[sel.selectedIndex].getAttribute('data-from');
		var toduration = sel.options[sel.selectedIndex].getAttribute('data-to');
		var fromamount = sel.options[sel.selectedIndex].getAttribute('data-fromamount');
		var toamount = sel.options[sel.selectedIndex].getAttribute('data-toamount');
		var qualifyfrom = sel.options[sel.selectedIndex].getAttribute('data-qualifyfrom');
		var qualifyto = sel.options[sel.selectedIndex].getAttribute('data-qualifyto');
		
//alert(interest);			
		document.getElementById('InterestRate').value = interest;
		document.getElementById('fromdate').value = fromdate;
		document.getElementById('todate').value = todate;
		document.getElementById('fromamount').value = fromamount;
		document.getElementById('toamount').value = toamount;
		document.getElementById('qualifyfrom').value = qualifyfrom;
		document.getElementById('qualifyto').value = qualifyto;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;

 }		
// -- EOC Different Loan Types.

// -- 30.06.2017 -- LoanDuration
 // -- Enter to capture comments   ---
 function defineloandutations(sel) 
 {
		
		var fromduration = document.getElementById('fromduration').value;
		var toduration = document.getElementById('toduration').value;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 30.06.2017 -- LoanDuration
</script> 