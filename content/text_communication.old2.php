<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="sentcomm_history"; // Table name 
 $bankid = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'superuser')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search for Customer Communications</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='customerid' name='customerid' placeholder='Customer ID' class='form-control' type='text' value=$customerid>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $bankid = $_POST['customerid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					// -- if($_SESSION['role'] == 'admin')
					if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'superuser')	
					{
					  if(!empty($bankid))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE customerid = ? ORDER BY sentcomm_historydate DESC";
						$q = $pdo->prepare($sql);
					    $q->execute(array($customerid));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name ORDER BY sentcomm_historydate DESC";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Type</b></td>"; 
						echo "<td><b>Date</b></td>"; 
						echo "<td><b>Time</b></td>"; 
						echo "<td><b>Customer ID</b></td>"; 
						echo "<td><b>Reference</b></td>"; 
						echo "<td><b>Email</b></td>"; 
						echo "<td><b>Phone</b></td>"; 
						echo "<td><b>Status</b></td>"; 
						echo "<td><b>Sent by</b></td>"; 
						echo "<th colspan='2'>Message Content</th>"; 
						echo "</tr>"; 						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['sentcomm_historytype']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['sentcomm_historydate']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['sentcomm_historytime']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['customerid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['refnumber']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['email']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['status']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['userid']) . "</td>";  
						echo "<td valign='top' colspan='2'>" . nl2br( $row['message']) . "</td>";  
						echo "</tr>"; 	
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->