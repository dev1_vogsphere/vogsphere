<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="tenantcustomers"; // Table name 
 $userid = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
	if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'superuser')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search for Customers by Tenant ID</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='userid' name='userid' placeholder='Tenant ID' class='form-control' type='text' value=$userid>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $userid = $_POST['userid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'superuser')
					{
					  if(!empty($userid))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE tenantid = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($userid));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<a class='btn btn-info' href=assigntenantcustomers.php>Assign Tenant Customers</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>tenantid</b></td>"; 
						echo "<td><b>customerid</b></td>";
						echo "<th colspan='3'>Action</th>"; 
						echo "</tr>"; 
				
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['tenantid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['customerid']) . "</td>";  
						
						// -- Encrypt 16.09.2017 - Parameter Data.
						$tenantidEncoded = urlencode(base64_encode($row['tenantid']));
						$customeridEncoded = urlencode(base64_encode($row['customerid']));
						// -- Encrypt 16.09.2017 - Parameter Data.
						echo "<td><a class='btn btn-danger' href=deletetenantcustomer.php?customerid={$customeridEncoded}&tenantid={$tenantidEncoded}>Unassign</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->