<!-- Debit Order Schedule 2018.06.02 -->
<!----------- debitorderschedule.js ----------------->
<script src="assets/js/debitorderschedule.js"></script>
<!-- End Debit Order Schedule -->
<?php
	require 'database.php';
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$tbl_customer = "businessproxy";
	$tbl_loanapp = "loanapp";
	$tbl_businessInfo = "businessowneinfo";
	$tbl_business = "business";
	$comingfrom = $_SERVER['HTTP_REFERER'];
	$dbname = "eloan";

	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
    $State='';
	$FirstName='';
	$LastName='';
	$other5='';
	$other6='';
	$other7='';
	$other8='';
	$other9='';
	$other10='';
	$other11='';
	$other12='';
	$other13='';
	$other14='';
	$businessrId='';
	
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}

	if ( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}

	if ( null==$customerid )
	    {
		header($location);
		error_log('customer ID id null');
		}
		else if( null==$ApplicationId )
		{
		header($location);
		error_log('Application ID is null');
		}
	 else {

		//$sql = "SELECT * FROM customers where id = ?";
		//$sql =  "select * from $tbl_customer, $tbl_loanapp,	$tbl_businessInfo , $tbl_business where $tbl_customer.keyidentificationNumber = ? AND ApplicationId = ?";
		$sql =  "select * from $tbl_loanapp where CustomerId = ? AND ApplicationId = ?";
		error_log($sql);
		error_log($customerid);
		error_log($ApplicationId);
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$businessreg=$data[businessrId];
		
		error_log('Get business ID');
		error_log($businessreg);
		
		
		//Get Business Information 
		$sql =  "select * from $tbl_business  where regNumber = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($businessreg));
		$data2 = $q->fetch(PDO::FETCH_ASSOC);


		//Business Owners Details
		
		$sql =  "select * from  $tbl_businessInfo where idbusiness = ? limit 1";
		$q = $pdo->prepare($sql);
		$q->execute(array($businessreg));
		$BussOwnerdata3 = $q->fetch(PDO::FETCH_ASSOC);
		
		//Business Proxy Details
		
		$sql =  "select * from $tbl_customer where idbusiness = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($businessreg));
		$proxydata4 = $q->fetch(PDO::FETCH_ASSOC);


		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header($location);
		}

		if( isset($data) && ($data!==null) )
		{
		}
		else
		{
						header($location);

		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

		$sql = 'SELECT * FROM user WHERE userid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
	    $dataUserEmail = $q->fetchAll();

		// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //

		// --------------------- BOC 2017.04.15 ------------------------- //
		//1. --------------------- Bank and Branch code ------------------- //
		  $sql = 'SELECT * FROM bank WHERE bankid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname']));
		  $dataBanks = $q->fetch(PDO::FETCH_ASSOC);


		  $sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname'],$data['branchcode']));
		  $dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);

		  // $data['branchcode'] = $dataBankBranches['branchdesc'];
		  $data['bankname'] = $dataBanks['bankname'];

		//2. ---------------------- BOC - Provinces ------------------------------- //
		// 15.04.2017 -
		  $sql = 'SELECT * FROM province WHERE provinceid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['provinceid']));
		  $dataProvince = $q->fetch(PDO::FETCH_ASSOC);
		  $data['State'] = $dataProvince['provincename'];

		//3. ---------------------- BOC - Account Types ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM accounttype WHERE accounttypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['accounttype']));
		 $dataAccountType = $q->fetch(PDO::FETCH_ASSOC);
		 $data['accounttype'] = $dataAccountType['accounttypedesc'];

		// ---------------------- EOC - Account Types ------------------------------- //

		//4. ---------------------- BOC - Payment Method ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM paymentmethod WHERE paymentmethodid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['paymentmethod']));
		 $dataPaymentMethod =$q->fetch(PDO::FETCH_ASSOC);
		 $data['paymentmethod'] = $dataPaymentMethod['paymentmethoddesc'];

		// ---------------------- EOC - Payment Method ------------------------------- //

		//5. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM paymentfrequency WHERE paymentfrequencyid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['paymentfrequency']));
		 $dataPaymentFrequency = $q->fetch(PDO::FETCH_ASSOC);
		 $data['paymentfrequency'] = $dataPaymentFrequency['paymentfrequencydesc'];

		// ---------------------- EOC - Payment Frequency ------------------------------- //

		//6. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM loantype WHERE loantypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['loantypeid']));
		 $dataLoanType = $q->fetch(PDO::FETCH_ASSOC);
		 $data['loantypeid'] = $dataLoanType['loantypedesc'];

		// ---------------------- EOC - Payment Frequency ------------------------------- //
		// BOC -- 15.04.2017 -- Updated.
		$BankName  = '';
		$BranchCode = '';
		// EOC -- 15.04.2017 -- Updated.
		// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //

		Database::disconnect();

		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;
	}
	
// -- BOC 13/12/2022 - Page Features Authorization...	
// -- Authorization of features
//$role = $dataUserEmail['role'];//
$dataPageFeaturesArr = null;
$currentpageArr = null;
$currentpageArr = explode("?",basename($_SERVER['REQUEST_URI'],".php"));
$currentpage = $currentpageArr[0];
$authorized_featuresArr = null;
$role = $_SESSION['role'];

$displaygenerate_payments='display:inline-block';
$displaydownloadcontract ='display:inline-block';
$displayupload_document ='display:inline-block';

$authorized_featuresArr   = authorize_features($pdo,$currentpage,$role);
//print_r($authorized_featuresArr);
$displaygenerate_payments = $authorized_featuresArr['displaygenerate_payments'];
$displaydownloadcontract  = $authorized_featuresArr['displaydownloadcontract'];
$displayupload_document   = $authorized_featuresArr['displayupload_document'];
// -- EOC 13/12/2022 - Page Features Authorization...	

// -- BOC CR1002 - 31/01/2023 - Only show generate payment button on APPROVE..	
if ($data['ExecApproval'] != 'APPR')
{
	$displaygenerate_payments  = 'display:none';
}
if ($data['ExecApproval'] == 'APPR')
{
	$displayupload_document    = 'display:inline-block';
	$displaydownloadcontract   = 'display:inline-block';
}
// -- EOC 31/01/2023 - Only show generate payment button on APPROVE..	

?>
<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">

<!-- Login Box -->
<form class="login-page" action="read?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
<div class="table-responsive">
<table class=   "table table-user-information">
<div class="row">
<tr>
<td><div class="panel-heading">
<h3>Business Info</h3>
</td></div>

</tr>

<tr>
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Business name</strong></label>
		<div class="controls">
		  	<?php echo $data['businessname'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>CIPC reg number</strong></label>
		<div class="controls">
			<?php echo $data['businessrId'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Type of business</strong></label>
		<div class="controls">
			<?php echo $data2['businessType'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Type of products offered</strong></label>
		<div class="controls">
			<?php echo $data2['productsorServices'];?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Stage of Business</strong></label>
		<div class="controls">
			<?php echo $data['stageOfBusiness'];?>
		</div>
	</div>
	</td>
	<!--<td>
	<div class="control-group <?php// echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Annual Sales</strong></label>
		<div class="controls">
			<?php //echo 'R '. number_format(($data['turnoverfirewood']+ $data['assetvaluetodate']),2);?>
		</div>
	</div>
	</td>-->
	<td>
		<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Annual firewood Sales</strong></label>
		<div class="controls">
			<?php echo 'R '. number_format($data['turnoverfirewood'],2);?>
		</div>
	</div>
	</td>
	<td>
		<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Asset value</strong></label>
		<div class="controls">
			<?php echo 'R '. number_format($data['assetvaluetodate'],2);?>
		</div>
	</div>
	</td>
	

</tr>

<!-- Street,Suburb,City -->
<tr>

	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Business location</strong></label>
		<div class="controls">
			<?php echo $data2['businesslocation'];?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Physical address</strong></label>
		<div class="controls">
		   	<?php echo $data2['street'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<?php echo $data2['suburb'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<?php
		// --------------------- BOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
			echo $data2['state'];
			?>
		</div>
	</div>
	</td>


</tr>

<!-- State,PostCode,Dob -->
<tr>


	<td>
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Key contact person</strong></label>
		<div class="controls">
		    <?php echo $proxydata4['keytitle'].' '.$proxydata4['keyfirstname'].' '.$proxydata4['keysurname'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Contact number</strong></label>
		<div class="controls">
		    <?php echo $proxydata4['keycontactNumber'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Email address</strong></label>
		<div class="controls">
				<?php echo $proxydata4['keyemailAddress'];?>
		</div>
	</div>
	</td>
</tr>
</div>
<div class="row">
<!----------------------------------------------------------- Directors Info-------------------------------------------->
<tr>
<td><div class="panel-heading">
<h3>Directors Info</h3>
</td></div>

</tr>
<tr>
		
			<tbody>
					<?php
					// Get directors Information
					 if (!empty ($data['businessrId']))
					 {
						$sql = 'SELECT * FROM businessowneinfo WHERE idbusiness = ?';
						$q = $pdo->prepare($sql);
						error_log( print_r( $q , true ) );
						error_log( print_r( $data['businessrId'] , true ) );
						$q->execute(array($data['businessrId']));
						error_log( print_r( $q , true ) );
						$DirectorData = $q->fetchAll();
						
							 foreach ($DirectorData as $row)
								{
									echo '<tr>';
									echo '<td>'.$row['title'].' '.$row['firstname'].' '.$row['surname'].'</td>';
									echo '<td>'.$row['identificationNumber'].'</td>';
									echo '<td>'. $row['ownershiptype']. '</td>';
									echo '<td>'. $row['shares'].' %'.'</td>';
									echo '</tr>';
								}
								
					 }
					
					?>
			</tbody>
		
</tr>




</div>
<div class="row">

<!----------------------------------------------------------- Banking Details -------------------------------------------->
<tr>
<td><div class="panel-heading">
<h3>Banking Info</h3>
</td></div>

</tr>
<!-- Account Holder Name,Bank Name,Account number -->
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Holder Name</strong></label>
		<div class="controls">
			<?php echo $data['accountholdername'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Bank Name</strong></label>
		<div class="controls">
			<?php echo $data['bankname'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
		    <?php echo $data['accountnumber'];?>
		</div>
	</div>
	</td>

<!-- branch code,Account type -->
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Type</strong></label>
		<div class="controls">
			<?php echo $data['accounttype'];?>
		</div>
	</div>
	</td>
</tr>
</div>
<div class="row">

<!----------------------------------------------------------- End Banking Details ---------------------------------------->

<!----------------------------------------------------------- Loan Details -------------------------------------------->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->
<tr>
<td><div class="panel-heading">
<h3>Declaration of Assets</h3>
</td></div>

</tr>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Kilns</strong></label>
		<div class="controls">
			<?php echo $data['kilns'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Spades</strong></label>
		<div class="controls">
			<?php echo $data['spades'];?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Chainsaws</strong></label>
		<div class="controls">
		    <?php echo $data['chainsaws'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Wheelbarrows</strong></label>
		<div class="controls">
		    <?php echo $data['wheelbarrows'];?>
		</div>
	</div>
	</td>
	</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Axes</strong></label>
		<div class="controls">
		    <?php echo $data['axes'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Fire Extinguishers</strong></label>
		<div class="controls">
		    <?php echo $data['fireExtinguishers'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Lasher Bows</strong></label>
		<div class="controls">
		    <?php echo $data['lasherbows'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Scale</strong></label>
		<div class="controls">
		    <?php echo $data['scale'];?>
		</div>
	</div>
	</td>
	</tr>
	<tr>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Shovels</strong></label>
		<div class="controls">
		    <?php echo $data['shovels'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Brush Cutter</strong></label>
		<div class="controls">
		    <?php echo $data['brushcutter'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Other:</strong></label>
		<div class="controls">
		    <?php echo $data['other'];?>
		</div>
	</div>
	</td>
</tr>
</br>
</div>
<div class="row">

<tr>
<td><div class="panel-heading">
<h3>Purpose of Financing</h3>
</td></div>

</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Start up costs</strong></label>
		<div class="controls">
			<?php echo 'R '. number_format($data['startupcost'],2);?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Asset acquisition</strong></label>
		<div class="controls">
			<?php echo 'R '. number_format($data['assetacquisition'],2);?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Working capital</strong></label>
		<div class="controls">
		    <?php echo 'R '. number_format($data['workingcapital'],2);?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Funding required</strong></label>
		<div class="controls">
		    <?php echo 'R '. number_format($data['fundingamount'],2);?>
		</div>
	</div>
	</td>



</tr>

<!-- InterestRate,ExecApproval -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Application Date</strong></label>
		<div class="controls">
		   <?php echo $data['DateAccpt'];?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<div class="controls">
		<?php
		if($data['ExecApproval'] == 'APPR')
								{
								echo 'Approved';
								}
								if($data['ExecApproval'] == 'PEN')
								{
								echo 'Pending';
								}
								if($data['ExecApproval'] == 'REV')
								{
								echo 'Review';
								}
								if($data['ExecApproval'] == 'CAN')
								{
								echo 'Cancelled';
								}
								if($data['ExecApproval'] == 'REJ')
								{
								echo 'Rejected';
								}
								if($data['ExecApproval'] == 'SET')
								{
								echo 'Settled';
								}
								// -- Defaulter - 20.06.2018.
								if($data['ExecApproval'] == 'DEF')
								{
								echo 'Defaulter';
								}

								// -- Legal - 07.11.2018.
								if($data['ExecApproval'] == 'LEGAL')
								{
								echo 'Legal';
								}

								// -- Rehabilitation - 07.11.2018.
								if($data['ExecApproval'] == 'REH')
								{
								echo 'Rehabilitation';
								}
		?>
		</div>
	</div>
	</td>
	<td>
</tr>
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
$rejectreason = $data['rejectreason'];
if ($data['ExecApproval'] == 'REJ')
{
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div>
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						 <!-- <a class="btn btn-primary" href="dashboardsme">Back</a>-->
						  <?php if(empty($comingfrom)){$comingfrom = 'readsme.php';} echo '<a class="btn btn-primary" href="'.$comingfrom.'?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded. '">Back</a>'; ?></div></td>
<!-- 13.06.2018 - Debit Order Schedule -->
		<span></span>
						<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
						 <?php
						 	if(isset($_SESSION))
							{
								$role = $_SESSION['role'];
							}

						 // -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);
$AccountHolder	 = $data['accountholdername'];
$BranchCode	 = $data['branchcode'];
$AccountNumber	 =  $data['accountnumber'];
$Servicetype	 = 'NADREQ';
$ServiceMode	 = 'NAEDO';
$UserCode      = 'UFEC';
$IntUserCode   = 'UFEC';
$Entry_Class   = 'UFEC';
$Contact_No		 = 'UFEC';
$Notify				 = 'UFEC';
$createdby     = 'UFEC';
$createdon     = 'UFEC';
$AccountType	 = $dataAccountType['accounttypeid'];//$data['accounttype'];
//$fname 			 = $data['FirstName'];
//$lname 			 = $data['LastName'];
$frequency 		 = $data['paymentfrequency'];
// Show if the role is administrator
//	if($role == 'admin')
	{
$href = "'"."href=debitorderschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency"."'";
						 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="debitorderschedule('.$href.')" style="'.$displaygenerate_payments.'">Generate Payments</button>';
	}
						 ?>
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>

								</div>
								</br> -->
											<div id="divdebitorderschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Debit Order Schedules</h1>
											</div>
							<!-- <div id="divSuccess">

								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>
						</div>
					  </td>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
<!-- 13.06.2018 - Debit Order Schedule -->
	<?php
	$ApplicationId = $data['ApplicationId'];
	$customerid = $data['CustomerId'];

	// -- if no role mean user never logged in go back to login page.
	if (empty($role))
	{
			header("Location: customerLogin");
	}

	// -- end of login check.

	// Only show contract if eloan application has been approved.
	if($data['ExecApproval'] == 'APPR')// && $role == 'customer') 13/12/2022
	{
	  $hash="&guid=".md5(date("h:i:sa"));
	  // -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' style='".$displaydownloadcontract."' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded>Download Contract</a></div></td>";
	  // -- EOC Encode Change - 16.09.2017.
	}
/* 13/12/2022 - 
	// Show if the role is administrator
	if($role == 'admin')
	{
		$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Download Contract</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.

	}
*/

	?>

						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php
	$ApplicationId = $data['ApplicationId'];
	$customerid = $data['CustomerId'];
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
	echo "<a class='btn btn-primary' style='".$displayupload_document."' href=uploadsme?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
</table>
</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>

   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php

    // $pdo = Database::connectDB();
	 $tbl_name="loanapp"; // Table name
	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	 $check_user = "SELECT * FROM $tbl_name WHERE customerid= ? and ApplicationId=?";

	$FILEOTHER1 	= '';
	$FILEOTHER2 	= '';
	$FILEOTHER3 	= '';
	$FILEOTHER4 	= '';
	$FILEOTHER5 	= '';
	$FILEOTHER6 	= '';
	$FILEOTHER7 	= '';
	$FILEOTHER8 	= '';
	$FILEOTHER9 	= '';
	$FILEOTHER10 	= '';
	$FILEOTHER11 	= '';
	$FILEOTHER12 	= '';
	$FILEOTHER13 	= '';
	$FILEOTHER14 	= '';
	$FILEOTHER15 	= '';
	$FILEOTHER16 	= '';
	$FILEOTHER17 	= '';
	$FILEOTHER18 	= '';
	$FILEOTHER19 	= '';
	$FILEOTHER20 	= '';
	$FILEOTHER21 	= '';
	$FILEOTHER22 	= '';


	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);


		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$row = $q->fetch(PDO::FETCH_ASSOC);
		$count = $q->rowCount();

	if($count >= 1)
    {

	//$row = mysql_fetch_array($result);

	$FILEOTHER1 	= $row['OTHER1'];
	$FILEOTHER2 	= $row['OTHER2'];
	$FILEOTHER3 	= $row['OTHER3'];
	$FILEOTHER4 	= $row['OTHER4'];
	$FILEOTHER5 	= $row['OTHER5'];
	$FILEOTHER6 	= $row['OTHER6'];
	$FILEOTHER7 	= $row['OTHER7'];
	$FILEOTHER8 	= $row['OTHER8'];
	$FILEOTHER9 	= $row['OTHER9'];
	$FILEOTHER10 	= $row['OTHER10'];
	$FILEOTHER11 	= $row['OTHER11'];
	$FILEOTHER12 	= $row['OTHER12'];
	$FILEOTHER13 	= $row['OTHER13'];
	$FILEOTHER14 	= $row['OTHER14'];
	$FILEOTHER15 	= $row['OTHER15'];
	$FILEOTHER16 	= $row['OTHER16'];
	$FILEOTHER17 	= $row['OTHER17'];
	$FILEOTHER18 	= $row['OTHER18'];
	$FILEOTHER19 	= $row['OTHER19'];
	$FILEOTHER20 	= $row['OTHER20'];
	$FILEOTHER21 	= $row['OTHER21'];
	$FILEOTHER22 	= $row['OTHER22'];

   // -- BOC 01.10.2017 - Alternative if no file upload.
	if(empty($FILEOTHER1)){$FILEOTHER1 = $NoFIle;}
	if(empty($FILEOTHER2)){$FILEOTHER2 = $NoFIle;}
	if(empty($FILEOTHER3)){$FILEOTHER3 = $NoFIle;}
	if(empty($FILEOTHER4)){$FILEOTHER4 = $NoFIle;}
	if(empty($FILEOTHER5)){$FILEOTHER5 = $NoFIle;}
	if(empty($FILEOTHER6)){$FILEOTHER6 = $NoFIle;}
	if(empty($FILEOTHER7)){$FILEOTHER7 = $NoFIle;}
	if(empty($FILEOTHER8)){$FILEOTHER8 = $NoFIle;}
	if(empty($FILEOTHER9)){$FILEOTHER9 = $NoFIle;}
	if(empty($FILEOTHER10)){$FILEOTHER10 = $NoFIle;}
	if(empty($FILEOTHER11)){$FILEOTHER11 = $NoFIle;}
	if(empty($FILEOTHER12)){$FILEOTHER12 = $NoFIle;}
	if(empty($FILEOTHER13)){$FILEOTHER13 = $NoFIle;}
	if(empty($FILEOTHER14)){$FILEOTHER14 = $NoFIle;}
	if(empty($FILEOTHER15)){$FILEOTHER15 = $NoFIle;}
	if(empty($FILEOTHER16)){$FILEOTHER16 = $NoFIle;}
	if(empty($FILEOTHER17)){$FILEOTHER17 = $NoFIle;}
	if(empty($FILEOTHER18)){$FILEOTHER18 = $NoFIle;}
	if(empty($FILEOTHER19)){$FILEOTHER19 = $NoFIle;}
	if(empty($FILEOTHER20)){$FILEOTHER20 = $NoFIle;}
	if(empty($FILEOTHER21)){$FILEOTHER21 = $NoFIle;}
	if(empty($FILEOTHER22)){$FILEOTHER22 = $NoFIle;}
   // -- EOC 01.10.2017 - Alternative if no file upload.

    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";

	$FILEOTHER1HTML = '';
	if($FILEOTHER1 == '')
	{
		$FILEOTHER1HTML = "<tr>
         <td><a href='$FILEOTHER1' target='_blank'>$FILEOTHER1</a></td>
         <td>Certified ID copies</td>
		</tr>";
	}
	else
	{
		$FILEOTHER1HTML = "<tr>
         <td><a $classCSS href='$FILEOTHER1' target='_blank'>$ViewDocument</a></td>
         <td>Certified ID copies</td>
		</tr>";
	}
	$FILEOTHER2HTML = '';
	if($FILEOTHER2 == '')
	{
		$FILEOTHER2HTML = "<tr>


         <td><a href='$FILEOTHER2' target='_blank'>$FILEOTHER2</a></td>
         <td>Affidavit from directors/ members that they are aware of the contents of the application form</td>
      </tr>
      ";
	}
	else
	{
		$FILEOTHER2HTML = "

      <tr>
         <td><a $classCSS href='$FILEOTHER2' target='_blank'>$ViewDocument</a></td>
         <td>Affidavit from directors/ members that they are aware of the contents of the application form</td>
      </tr>";
	}

	$FILEOTHER3HTML = '';
	if($FILEOTHER3 == '')
	{
		$FILEOTHER3HTML = "<tr>
         <td><a href='$FILEOTHER3' target='_blank'>$FILEOTHER3</a></td>
         <td>Curriculum Vitae(s) of all the Directors</td>
      </tr>";
	}
	else
	{
		$FILEOTHER3HTML = "<tr>
         <td><a $classCSS href='$FILEOTHER3' target='_blank'>$ViewDocument</a></td>
         <td>Curriculum Vitae(s) of all the Directors</td>
      </tr>";
	}

	$FILEOTHER4HTML = '';
	if($FILEOTHER4 == '')
	{
		$FILEOTHER4HTML = "
	  <tr>
         <td><a href='$FILEOTHER4' target='_blank'>$FILEOTHER4</a></td>
         <td>CIPC Registration Certificate</td>
      </tr>
	  ";
	}
	else
	{
		$FILEOTHER4HTML = "
	  <tr>
         <td><a $classCSS href='$FILEOTHER4' target='_blank'>$ViewDocument</a></td>

         <td>CIPC Registration Certificate</td>
      </tr>
	  ";
	}
	$FILEOTHER5HTML = '';
	if($FILEOTHER5 == '')
	{
		$FILEOTHER5HTML = "

	  <tr>
         <td><a href='$FILEOTHER5' target='_blank'>$FILEOTHER5</a></td>
         <td>Tax Clearance Certificate</td>
      </tr>
	  ";
	}
	else
	{
		$FILEOTHER5HTML = "
	  <tr>
         <td><a $classCSS href='$FILEOTHER5' target='_blank'>$ViewDocument</a></td>
         <td>Tax Clearance Certificate</td>
      </tr>
	  ";
	}
	$FILEOTHER6HTML = '';
	if($FILEOTHER6 == '')
	{
		$FILEOTHER6HTML = "
	  <tr>
         <td><a href='$FILEOTHER6' target='_blank'>$FILEOTHER6</a></td>
         <td>Unemployment Insurance Fund Compliance Certificate</td>
      </tr>
	  ";
	}
	else
	{
		$FILEOTHER6HTML = "
	  <tr>
         <td><a $classCSS href='$FILEOTHER6' target='_blank'>$ViewDocument</a></td>
         <td>Unemployment Insurance Fund Compliance Certificate</td>
      </tr>
	  ";
	}
	$FILEOTHER7HTML = '';
	if($FILEOTHER7 == '')
	{
		$FILEOTHER7HTML = "
	  <tr>
         <td><a href='$FILEOTHER7' target='_blank'>$FILEOTHER7</a></td>
         <td>Letter of Good Standing with Compensation Fund</td>
      </tr>";
	}
	else
	{
		$FILEOTHER7HTML = "
	  <tr>
         <td><a  $classCSS href='$FILEOTHER7' target='_blank'>$ViewDocument</a></td>
         <td>Letter of Good Standing with Compensation Fund</td>
      </tr>";
	}
	$FILEOTHER8HTML = '';
	if($FILEOTHER8 == '')
	{
		$FILEOTHER8HTML ="
	  <tr>
         <td><a href='$FILEOTHER8' target='_blank'>$FILEOTHER8</a></td>
         <td>Valid PAYE Registration Certificate</td>
      </tr>
	";
	}
	else
	{
		$FILEOTHER8HTML ="
	  <tr>
         <td><a $classCSS href='$FILEOTHER8' target='_blank'>$ViewDocument</a></td>
         <td>Valid PAYE Registration Certificate</td>
      </tr>
	";
	}
	$FILEOTHER9HTML = '';
	if($FILEOTHER9 == '')
	{
		$FILEOTHER9HTML = "
  	  <tr>
         <td><a href='$FILEOTHER9' target='_blank'>$FILEOTHER9</a></td>
         <td>Valid VAT Certificate</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER9HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER9' target='_blank'>$ViewDocument</a></td>
         <td>Valid VAT Certificate</td>
      </tr>";
	}


	$FILEOTHER10HTML = '';
	if($FILEOTHER10 == '')
	{
		$FILEOTHER10HTML = "
  	  <tr>
         <td><a href='$FILEOTHER10' target='_blank'>$FILEOTHER10</a></td>
         <td>Personal Bank Statements for the past 12 months</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER10HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER10' target='_blank'>$ViewDocument</a></td>
         <td>Personal Bank Statements for the past 12 months</td>
      </tr>";
	}

	$FILEOTHER11HTML = '';
	if($FILEOTHER11 == '')
	{
		$FILEOTHER11HTML = "
  	  <tr>
         <td><a href='$FILEOTHER11' target='_blank'>$FILEOTHER11</a></td>
         <td>Official Business Bank Confirmation Letter</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER11HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER11' target='_blank'>$ViewDocument</a></td>
         <td>Official Business Bank Confirmation Letter</td>
      </tr>";
	}

	$FILEOTHER12HTML = '';
	if($FILEOTHER12 == '')
	{
		$FILEOTHER12HTML = "
  	  <tr>
         <td><a href='$FILEOTHER12' target='_blank'>$FILEOTHER12</a></td>
         <td>Business Bank Statements for the past 12 months</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER12HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER12' target='_blank'>$ViewDocument</a></td>
         <td>Business Bank Statements for the past 12 months</td>
      </tr>";
	}
	
	$FILEOTHER13HTML = '';
	if($FILEOTHER13 == '')
	{
		$FILEOTHER13HTML = "
  	  <tr>
         <td><a href='$FILEOTHER13' target='_blank'>$FILEOTHER13</a></td>
         <td>Attached Personal Credit Report</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER13HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER13' target='_blank'>$ViewDocument</a></td>
         <td>Attached Personal Credit Report</td>
      </tr>";
	}
	$FILEOTHER14HTML = '';
	if($FILEOTHER14 == '')
	{
		$FILEOTHER14HTML = "
  	  <tr>
         <td><a href='$FILEOTHER14' target='_blank'>$FILEOTHER14</a></td>
         <td>Attached Business Credit Report</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER14HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER14' target='_blank'>$ViewDocument</a></td>
         <td>Attached Business Credit Report</td>
      </tr>";
	}
	$FILEOTHER15HTML = '';
	if($FILEOTHER15 == '')
	{
		$FILEOTHER15HTML = "
  	  <tr>
         <td><a href='$FILEOTHER15' target='_blank'>$FILEOTHER15</a></td>
         <td>BBBEEE Affidavit or Certificate</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER15HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER15' target='_blank'>$ViewDocument</a></td>
         <td>BBBEEE Affidavit or Certificate</td>
      </tr>";
	}
	$FILEOTHER16HTML = '';
	if($FILEOTHER16 == '')
	{
		$FILEOTHER16HTML = "
  	  <tr>
         <td><a href='$FILEOTHER16' target='_blank'>$FILEOTHER16</a></td>
         <td>Tribal Authority letter - Permission to use land</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER16HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER16' target='_blank'>$ViewDocument</a></td>
         <td>Tribal Authority letter - Permission to use land</td>
      </tr>";
	}
	$FILEOTHER17HTML = '';
	if($FILEOTHER17 == '')
	{
		$FILEOTHER17HTML = "
  	  <tr>
         <td><a href='$FILEOTHER17' target='_blank'>$FILEOTHER17</a></td>
         <td>A valid signed offtake agreement</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER17HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER17' target='_blank'>$ViewDocument</a></td>
         <td>A valid signed offtake agreement</td>
      </tr>";
	}
	$FILEOTHER18HTML = '';
	if($FILEOTHER18 == '')
	{
		$FILEOTHER18HTML = "
  	  <tr>
         <td><a href='$FILEOTHER18' target='_blank'>$FILEOTHER18</a></td>
         <td>Expressed interest in establishing a charcoal focused business</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER18HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER18' target='_blank'>$ViewDocument</a></td>
         <td>Expressed interest in establishing a charcoal focused business</td>
      </tr>";
	}
	$FILEOTHER19HTML = '';
	if($FILEOTHER19 == '')
	{
		$FILEOTHER19HTML = "
  	  <tr>
         <td><a href='$FILEOTHER19' target='_blank'>$FILEOTHER19</a></td>
         <td>Approved Business Plan</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER19HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER19' target='_blank'>$ViewDocument</a></td>
         <td>Approved Business Plan</td>
      </tr>";
	}
	$FILEOTHER20HTML = '';
	if($FILEOTHER20 == '')
	{
		$FILEOTHER20HTML = "
  	  <tr>
         <td><a href='$FILEOTHER20' target='_blank'>$FILEOTHER20</a></td>
         <td>Amount of biomass produced within the last twelve (12) months</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER20HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER20' target='_blank'>$ViewDocument</a></td>
         <td>Amount of biomass produced within the last twelve (12) months</td>
      </tr>";
	}
	$FILEOTHER21HTML = '';
	if($FILEOTHER21 == '')
	{
		$FILEOTHER21HTML = "
  	  <tr>
         <td><a href='$FILEOTHER21' target='_blank'>$FILEOTHER21</a></td>
         <td>Amount of charcoal produced within the last twelve (12) months</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER21HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER21' target='_blank'>$ViewDocument</a></td>
         <td>Amount of charcoal produced within the last twelve (12) months</td>
      </tr>";
	}
	$FILEOTHER22HTML = '';
	if($FILEOTHER22 == '')
	{
		$FILEOTHER22HTML = "
  	  <tr>
         <td><a href='$FILEOTHER22' target='_blank'>$FILEOTHER22</a></td>
         <td>Amount of revenue generated within the last twelve (12) months</td>
      </tr>
";
	}
	else
	{
		$FILEOTHER22HTML = "
  	  <tr>
         <td><a $classCSS href='$FILEOTHER22' target='_blank'>$ViewDocument</a></td>
         <td>Amount of revenue generated within the last twelve (12) months</td>
      </tr>";
	}

   echo "<tbody>"
      .$FILEOTHER1HTML.$FILEOTHER2HTML.$FILEOTHER3HTML.$FILEOTHER4HTML.$FILEOTHER5HTML.$FILEOTHER6HTML.$FILEOTHER7HTML.$FILEOTHER8HTML.$FILEOTHER9HTML.$FILEOTHER10HTML.$FILEOTHER11HTML.$FILEOTHER12HTML.$FILEOTHER13HTML.$FILEOTHER14HTML.$FILEOTHER15HTML.$FILEOTHER16HTML.$FILEOTHER17HTML.$FILEOTHER18HTML.$FILEOTHER19HTML.$FILEOTHER20HTML.$FILEOTHER21HTML.$FILEOTHER22HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->
</table>
 <!------------------------------ End File List ------------------------------------>

<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------>
</div>
</form>

</div>


            <!-- === END CONTENT === -->
