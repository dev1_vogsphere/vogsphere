<?php 
require 'database.php';
$loantypedescError = null;
$percentageError = null;


$loantypedesc = '';
$count = 0;
$loantypeid = 0;
$percentage = '';
$lastchangedby = '';
$lastchangedbydate = '';
// -- BOC 30.06.2017
$todurationError = null;
$fromdurationError = null;

$fromamountError = null;
$toamountError = null;

$fromdateError = null;
$todateError = null;

$qualifyfromError = null;
$qualifytoError = null;

$fromduration = 1;
$toduration = 1;
$fromamount = 1;
$toamount = 1;
$fromdate = "";
$todate = "";
$qualifyfrom = 1;
$qualifyto = 1;
// ---------------------- BOC - Currency ------------------------------- //
 // -- Database Connection.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	$currency = '';
	$sql = 'SELECT * FROM currency';
	$datacurrency = $pdo->query($sql);						   						 
// ---------------------- EOC - Currency ------------------------------- //  

// -- EOC 30.06.2017

if (isset($_GET['loantypeid']) ) 
{ 
$loantypeid = (int) $_GET['loantypeid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$loantypedesc = $_POST['loantypedesc'];
	$percentage = $_POST['percentage'];
	
	$lastchangedby = $_SESSION['username'];
	$lastchangedbydate = date('Y-m-d');
	
	// -- BOC 30.06.2017
	$fromduration = $_POST['fromduration'];
	$toduration = $_POST['toduration'];
	
	$fromamount = $_POST['fromamount'];
	$toamount = $_POST['toamount'];
	
	$fromdate = $_POST['fromdate'];
	$todate = $_POST['todate'];
	
	$qualifyfrom = $_POST['qualifyfrom'];
	$qualifyto = $_POST['qualifyto'];
	$currency = $_POST['currency'];
	
	// -- EOC 30.06.2017

	$valid = true;
	if (empty($loantypedesc)) { $loantypedescError = 'Please enter Loan Type Description.'; $valid = false;}
	if (empty($percentage)) { $percentageError = 'Please enter percentage.'; $valid = false;}
	
	// -- BOC 30.06.2017
	// -- Duration Range.
	if (!is_Numeric($fromduration)) { $fromdurationError = 'Please enter a number for Loan : From Duration'; $valid = false;}
	if (!is_Numeric($toduration)) { $todurationError = 'Please enter a number for Loan : To Duration'; $valid = false;}
		
	if (empty($fromduration)) { $fromdurationError = 'Please enter from duration(e.g. 1).'; $valid = false;}
	if (empty($toduration)) { $todurationError = 'Please enter to duration(e.g. 1).'; $valid = false;}
	
	// -- From & To Amount.
	if (!is_Numeric($fromamount)) { $fromamountError = 'Please enter a number for Loan : From Amount'; $valid = false;}
	if (!is_Numeric($toamount)) { $toamountError = 'Please enter a number for Loan : To Amount'; $valid = false;}
		
	if (empty($fromamount)) { $fromamountError = 'Please enter from amount(e.g. 1.00).'; $valid = false;}
	if (empty($toduration)) { $todurationError = 'Please enter to amount(e.g. 1.00).'; $valid = false;}
	
	// -- From & To Date 
	if (empty($fromdate)) { $fromdateError = 'Please enter from date(e.g. 01-01-2000).'; $valid = false;}
	if (empty($todate)) { $todateError = 'Please enter to date(e.g. 01-01-2000).'; $valid = false;}
	
	// -- From Qualify From & to Qualify Amount.
	if (!is_Numeric($qualifyfrom)) { $qualifyfromError = 'Please enter a number for Loan Qualify: From Amount'; $valid = false;}
	if (!is_Numeric($qualifyto)) { $qualifytoError = 'Please enter a number for Loan Qualify: To Amount'; $valid = false;}
		
	if (empty($fromamount)) { $fromamountError = 'Please enter qualify from amount(e.g. 1.00).'; $valid = false;}
	if (empty($qualifyto)) { $qualifytoError = 'Please enter qualify to amount(e.g. 1.00).'; $valid = false;}

	// -- EOC 30.06.2017

	if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE loantype SET loantypedesc = ?, percentage = ?,currency = ?,lastchangedby = ?,lastchangedbydate = ?,fromduration = ?,
			toduration = ?,fromamount = ?,toamount = ?,toduration = ?,fromdate = ?,todate = ?,qualifyfrom = ?,qualifyto = ? WHERE loantypeid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($loantypedesc,$percentage,$currency,$lastchangedby,$lastchangedbydate,$fromduration,$toduration,
			$fromamount,$toamount,$toduration,$fromdate,$todate,$qualifyfrom,$qualifyto,$loantypeid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from loantype where loantypeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($loantypeid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$loantypedesc = $data['loantypedesc'];
		$percentage = $data['percentage'];
		
		// -- BOC 30.06.2017
		$fromduration = $data['fromduration'];
		$toduration = $data['toduration'];
		$fromamount = $data['fromamount'];
		$toamount = $data['toamount'];
		$fromdate = $data['fromdate'];
		$todate = $data['todate'];
		$qualifyfrom = $data['qualifyfrom'];
		$qualifyto = $data['qualifyto'];
		$curreny = $data['currency'];
		// -- EOC 30.06.2017
}

?>
<div class="container background-white bottom-border"> 
<div class='margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change Loan Type
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Loan Type successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Currency</b><br /></p>
					<div class="controls">
								 <SELECT class="form-control" name="currency" id="currency" size="1">
								 <?php $currencyid = '';
											$currencySelect = $currency;
											$currencyname = '';
											foreach($datacurrency as $row)
											{
												$currencyid = $row['currencyid'];
												$currencyname = $row['currencyname'];
												if($loantypeid == $loantype)
												{
												 echo "<OPTION value=$currencyid selected>$currencyid - $currencyname</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$currencyid>$currencyid - $currencyname</OPTION>";
												}
											}
											
											if(empty($datacurrency))
											{
												echo "<OPTION value=0>No Currency</OPTION>";
											}
								 ?>
								 </SELECT> 
			<p><b>Loan Type</b><br />
			<input style="height:30px" type='text' name='loantypedesc' value="<?php echo !empty($loantypedesc)?$loantypedesc:'';?>"/>
			<?php if (!empty($loantypedescError)): ?>
			<span class="help-inline"><?php echo $loantypedescError;?></span>
			<?php endif; ?>
			</p> 
			
			<p><b>Percentage(%)</b><br />
			<input style="height:30px" type='text' name='percentage' value="<?php echo !empty($percentage)?$percentage:'';?>"/>
			<?php if (!empty($percentageError)): ?>
			<span class="help-inline"><?php echo $percentageError;?></span>
			<?php endif; ?>
			</p>
			<!--------------------- BOC 30.06.2017 --------------------------->
			<p><b>From Duration(e.g. 1)</b><br />
			<input style="height:30px" type='text' name='fromduration' value="<?php echo !empty($fromduration)?$fromduration:'';?>"/>
			<?php if (!empty($fromdurationError)): ?>
			<span class="help-inline"><?php echo $fromdurationError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>To Duration(e.g. 1)</b><br />
			<input style="height:30px" type='text' name='toduration' value="<?php echo !empty($toduration)?$toduration:'';?>"/>
			<?php if (!empty($todurationError)): ?>
			<span class="help-inline"><?php echo $todurationError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>From Amount(e.g. 1.00)</b><br />
			<input style="height:30px" type='text' name='fromamount' value="<?php echo !empty($fromamount)?$fromamount:'';?>"/>
			<?php if (!empty($fromamountError)): ?>
			<span class="help-inline"><?php echo $fromamountError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>To Amount(e.g. 1.00)</b><br />
			<input style="height:30px" type='text' name='toamount' value="<?php echo !empty($toamount)?$toamount:'';?>"/>
			<?php if (!empty($toamountError)): ?>
			<span class="help-inline"><?php echo $toamountError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>From Date(e.g. 01-01-2000)</b><br />
			<input style="height:30px" placeholder="yyyy-mm-dd" type="Date" name='fromdate' value="<?php echo !empty($fromdate)?$fromdate:'';?>"/>
			<?php if (!empty($fromdateError)): ?>
			<span class="help-inline"><?php echo $fromdateError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>To Date(e.g. 01-01-2000)</b><br />
			<input style="height:30px" placeholder="yyyy-mm-dd" type="Date" name='todate' value="<?php echo !empty($todate)?$todate:'';?>"/>
			<?php if (!empty($todateError)): ?>
			<span class="help-inline"><?php echo $todateError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>From Qualify(e.g. 1.00)</b><br />
			<input style="height:30px" type='text' name='qualifyfrom' value="<?php echo !empty($qualifyfrom)?$qualifyfrom:'';?>"/>
			<?php if (!empty($qualifyfromError)): ?>
			<span class="help-inline"><?php echo $qualifyfromError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>To Qualify(e.g. 1.00)</b><br />
			<input style="height:30px" type='text' name='qualifyto' value="<?php echo !empty($qualifyto)?$qualifyto:'';?>"/>
			<?php if (!empty($qualifytoError)): ?>
			<span class="help-inline"><?php echo $qualifytoError;?></span>
			<?php endif; ?>
			</p>
			<!--------------------- EOC 30.06.2017 --------------------------->
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="loantype">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
							</div>

	</div></div>
