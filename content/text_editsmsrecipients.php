<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  
$tbl_user="customer";  
$bulksmsclientid = '';
$Title = '';
$FirstName = '';
$LastName = '';
$phone = '';
$occupation = '';
$status = '';
$position = '';
$Suburb = "";
$ward = "";

$userid = 0;
$count  = 0;


  if(isset($_SESSION['bulksmsclientid'])){$bulksmsclientid = $_SESSION['bulksmsclientid'];}

$UserIdDecoded = "";
$userid = "";
if (isset($_GET['customerid']) ) 
{ 
$userid = $_GET['customerid']; 
// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$userid = base64_decode(urldecode($userid)); 
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
}

// -- LookUps
//2. ---------------------- BOC - Positions ------------------------------- //
  $sql = 'SELECT * FROM positions';
  $dataPositions = $pdo->query($sql);	

  $sql = 'SELECT * FROM suburbs';
  $dataSuburbs = $pdo->query($sql);	

  $sql = 'SELECT * FROM ward';
  $dataWards = $pdo->query($sql);	

  $sql = 'SELECT * FROM ward';
  $dataWards = $pdo->query($sql);	

if (!empty($_POST)) 
{   $count = 0;
	
$Title 		= $_POST['Title'];
$FirstName 	= $_POST['FirstName'];
$LastName 	= $_POST['LastName'];
$phone 	= $_POST['phone'];
$occupation = $_POST['occupation'];
$status 	= $_POST['status'];
$position 	= $_POST['position'];
$Suburb 	= $_POST['Suburb'];
$ward 		= $_POST['ward'];
$userid = 	 $_POST['userid'];

//echo $position;
	$valid = true;
	

		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "UPDATE $tbl_user SET 	 Title 		 = ?,  	
                                         FirstName 	 = ?,
                                         LastName 	 = ?,
                                         phone 	 = ?,
                                         occupation  = ?,
                                         status 	 = ?,
                                         subgroupext 	 = ?,
                                         subgroupext2 	 = ?,
                                         ward 		 = ?
			WHERE Customerid = ?";
	
			$q = $pdo->prepare($sql);
			$q->execute(array($Title 		,
			                  $FirstName 	,
			                  $LastName 	,
							  $phone		,
			                  $occupation   ,
			                  $status 	    ,
			                  $position 	,
			                  $Suburb 	    ,
			                  $ward 		,$userid));
			Database::disconnect();
			$count = $count + 1;					
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from customer where customerid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header("Location: custAuthenticate");
		}
$Title 		= $data['Title'];
$FirstName 	= $data['FirstName'];
$LastName 	= $data['LastName'];
$phone 		= $data['phone'];
$occupation = $data['occupation'];
$status 	= $data['status'];
$position 	= $data['subgroupext'];
$Suburb 	= $data['subgroupext2'];
$ward 		= $data['ward'];

}
?>
<div class="container background-white bottom-border">
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Update SMS Recipients
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>SMS Recipient successfully Updated.</p>");
		}
?>		
</div>				
			<p style="display:none"><b>User ID</b><br /><input style="height:30px" type='text' name='userid' value="<?php echo !empty($userid)?$userid:'';?>" readonly /></p>
			<p><b>Title</b><br /><input style="height:30px" type='text' name='Title' value="<?php echo !empty($Title)?$Title:'';?>" /></p>
			<p><b>Firstname</b><br /><input style="height:30px" type='text' name='FirstName' value="<?php echo !empty($FirstName)?$FirstName:'';?>"  /></p>
			<p><b>Lastname</b><br /><input style="height:30px" type='text' name='LastName' value="<?php echo !empty($LastName)?$LastName:'';?>"  /></p>
			<p><b>Cellphone Nr	</b><br /><input style="height:30px" type='text' name='phone' value="<?php echo !empty($phone)?$phone:'';?>"  /></p>
			<p><b>Designation</b><br /><input style="height:30px" type='text' name='occupation' value="<?php echo !empty($occupation)?$occupation:'';?>"  /></p>
			<p style="display:none"><b>Generated ID</b><br /><input style="height:30px" type='text' name='generatedid' value="<?php echo !empty($generatedid)?$generatedid:'';?>" readonly /></p>
			
						<p style="display:none"><b>bulksmsclient</b><br /><input style="height:30px" type='text' name='bulksmsclientid' value="<?php echo !empty($bulksmsclientid)?$bulksmsclientid:'';?>" readonly /></p>

			<p><b>Status</b></p>
			<SELECT  id="status" class="form-control" name="status" size="1" style="width:200px">
									<OPTION value="Active">Active</OPTION>
									<OPTION value="Disabled">Disabled</OPTION>
			</SELECT>
			<p><b>position</b></p>
			<SELECT class="form-control" id="position" name="position" size="1">
											<?php
											$positionid 	= '';
											$positionSelect = $position;
											$positionname 	= '';
											foreach($dataPositions as $row)
											{
												$positionid   = $row['positionsid'];
												$positionname = $row['positionname'];

												if($positionid == $positionSelect)
												{
												echo "<OPTION value=$positionid selected>$positionname</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$positionid>$positionname</OPTION>";
												}
											}
										
											if(empty($dataPositions))
											{
												echo "<OPTION value=0>No positions</OPTION>";
											}
											?>
			</SELECT>
			<p><b>suburb</b></p>
						<SELECT class="form-control" id="Suburb" name="Suburb" size="1">
															<OPTION value='' selected></OPTION>

											<?php
											$suburbid 	= '';
											$suburbSelect = $Suburb;
											$suburbname 	= '';
											foreach($dataSuburbs as $row)
											{
												$suburbid   = $row['suburbid'];
												$suburbname = $row['suburbname'];
												$popularname = $row['popularname'];
												$ext = $row['ext'];

												if($suburbid == $suburbSelect)
												{
												echo "<OPTION value=$ext selected>$popularname</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$ext>$popularname</OPTION>";
												}
											}
										
											if(empty($dataSuburbs))
											{
												echo "<OPTION value=0>No Suburbs</OPTION>";
											}
											?>
			</SELECT>
			<p><b>ward</b></p>
									<SELECT class="form-control" id="ward" name="ward" size="1">
																		<OPTION value='' selected></OPTION>

											<?php
											$wardid 	= '';
											$wardSelect = $ward;
											$wardname 	= '';
											foreach($dataWards as $row)
											{
												$wardid   = $row['wardid'];
												$wardname = $row['wardname'];

												if($wardid == $wardSelect)
												{
												echo "<OPTION value=$wardid selected>$wardname</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$wardid>$wardname</OPTION>";
												}
											}
										
											if(empty($dataWards))
											{
												echo "<OPTION value=0>No Wards</OPTION>";
											}
											?>
			</SELECT>
		<table>
		<tr>
			<td>
			   <div>
				<button type="submit" class="btn btn-primary">Update SMS Recipient</button>
				<a class="btn btn-primary" href="smsrecipients">Back</a>
			   </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>

