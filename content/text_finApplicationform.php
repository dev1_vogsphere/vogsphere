<?php
$B_IntUserCode = getsession('IntUserCode');
//echo 'tEST'.$IntUserCode;
////////////////////////////////////////////////////////////////////////
// 06/05/2022..
//log File
$log_file = "/var/log/httpd/debug_log";

if(!function_exists('get_tenant_logo'))
{
	function get_tenant_logo($IntUserCode)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'Seq_Generator_fin';			
		$sql = "select * from $tbl where Client_Id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
if(!function_exists('get_ffms_premium_amnt'))
{
	function get_ffms_premium_amnt($Policy_id)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$PackageOption_id = get_ffms_package_option($Policy_id);
		//print_r( $PackageOption_id);
		$data = null;
		$tbl  = 'premium';			
		$sql = "select * from $tbl where PackageOption_id = ? and Years = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($PackageOption_id,1));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);
		return number_format($PackageOption_id);		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_package_option'))
{
	function get_ffms_package_option($Policy_id)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'policy';			
		$sql = "select * from $tbl where Policy_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($Policy_id));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data['PackageOption_id'];		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_benefits'))
{
	function get_ffms_benefits($Policy_id)
	{
////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$PackageOption_id = get_ffms_package_option($Policy_id);
		$data = null;
		$tbl  = 'packageoption';			
		$sql = "select * from $tbl where PackageOption_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($PackageOption_id));
		//error_log($q,0);
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return number_format($data['Benefits'],2);		
	}
}
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021
require  $filerootpath.'/ffms/ffms_classess.php';
$im_value = 0;
$source_of_income = new Source_of_incomeDAL();
$source_of_incomeList = $source_of_income->Source_of_income($im_value);

$tenant_array = get_tenant_logo($B_IntUserCode);

// BOC -- Premimum & Benef Amnt -- //
$premiumAmnt	= 0.00;
$futureBenefits = 0.00;
// EOC -- Premimum & Benef Amnt -- //

$Occupation = new OccupationDAL();
$im_occupationList  = $Occupation->Occupation($im_value);
//print_r($im_occupationList );
$im_occupation_id_1 = '';

// -- MaritalDAL
$Marital = new MaritalStatusDAL();
$MaritalList = $Marital->MaritalStatus($im_value);

// -- MarriageDAL
$Marriage = new MarriageDAL();
$MarriageList = $Marriage->marriage($im_value);

// -- BrokersDAL
$Broker = new BrokerDAL();
$brokerList = $Broker->Broker_IntUserCode($B_IntUserCode);
//print_r($brokerList);
// -- Payment Method DAL
$PaymentMethod = new PaymentMethodDAL();
$PaymentMethodList = $PaymentMethod->PaymentMethod_IntUserCode($B_IntUserCode);
//print_r($PaymentMethodList);
// -- Comms Method DAL
$CommsMethod = new CommsMethodDAL();
$CommsMethodList = $CommsMethod->CommsMethod_IntUserCode($B_IntUserCode);
// -- BranchesDAL
$Branch = new BranchDAL();
$BranchList = $Branch->Branch_IntUserCode($B_IntUserCode);
//print_r($BranchList);


// -- PolicyDAL options
$Policy = new policyDAL();
$PolicyList = $Policy->policy_IntUserCode($B_IntUserCode);

// -- MemberDAL and BLL 
$MemberDAL = new MemberDAL();
$MemberBO  = new MemberBLL();

// -- AddressDAL and BLL
$AddressDAL = new AddressDAL();
$AddressBO = new AddressBLL();

/* -- Database Declarations and config:*/
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM bank';
$databank= $pdo->query($sql);

$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM entry_class';
$dataentry_class = $pdo->query($sql);

require 'database_ffms.php';
$pdo_ffms = db_connect::db_connection();
$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM province';
$q = $pdo_ffms->prepare($sql);
$q->execute(array());
$provinceArr = $q->fetchAll(PDO::FETCH_ASSOC);

$notification = array();
$notificationsList = array();

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
//$Account_Holder='';
$Account_Type  ='';
$Account_Number='';
$Branch_Code   ='';
$No_Payments   ='';
$Amount        ='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class    ='';
$Bank_Reference ='';
$contact_number = '';
$Notify 		= '';
//$paymentmethod  = '';

$entry_classSelect = '';
$message = '';
/* -- dropdown selection. */
$Client_ID_TypeArr = array();
//$Client_ID_TypeArr[] = '';
// -- Member not responsible for account payemnts-  added 2023/07/09
$debtor_identification 			 = '';
$debtor_id_type 			 = '';
$debtor_contact_number 			 = '';
$debtor_email 			 = '';
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_desc'];
}

/*Title Read tile from Db */ 
$im_titleArr[] = ' '.'|'.' ';
$im_titleArr[] = 'Mr'.'|'.'Mr';
$im_titleArr[] = 'Mrs'.'|'.'Mrs';
$im_titleArr[] = 'Ms'.'|'.'Ms';
$im_titleArr[] = 'Dr'.'|'.'Dr';
$im_titleArr[] = 'Prof'.'|'.'Prof';

foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

/*Country*/
$countryArr[] = 'South Africa'.'|'.'South Africa';

/*City*/
$cityArr[] 	  = 'Johannesburg'.'|'.'Johannesburg';
$cityArr[] 	  = 'Pretoria'.'|'.'Pretoria';
$cityArr[] 	  = 'Polokwane'.'|'.'Polokwane';
$cityArr[] 	  = 'Bloemfontein'.'|'.'Bloemfontein';
$cityArr[] 	  = 'Botshabelo'.'|'.'Botshabelo';
$cityArr[] 	  = 'Brandfort'.'|'.'Brandfort';
$cityArr[] 	  = 'Thaba-Nchu'.'|'.'Thaba-Nchu';


$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$bankArr = array();
foreach($databank as $row)
{
	$bankArr[] = $row['bankid'].'|'.$row['bankname'];
}

$dataentry_classArr = Array();
foreach($dataentry_class as $row)
{
	$dataentry_classArr[] = $row['identry_class'].'|'.$row['Entry_Class_Des'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
$FrequencyArr[] = 'Last Working day of the Month';

//$paymentmethodArr[] = 'Debit Order';
//$paymentmethodArr[] = 'Debit Check';
//$paymentmethodArr[] = 'Persal';


$NotifyArr[] = 'Yes';
$NotifyArr[] = 'No';

$arrayInstruction = null;

$UserCode = null;
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}
//-------------------------------------------------------------------
//           				DECLARATIONS-update 	    			-
//-------------------------------------------------------------------	
	$emailFlag = '';
	$tenantid = '';
	$customerid = '';
	$ApplicationId = '';
// ================================ BOC - tenantid =========================
//$tenantid = 'UFRF';
if ( !empty($_GET['tenantid']))
	{
		$tenantid = $_REQUEST['tenantid'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['tenantid']))
	 {
	    $tenantid = $_POST['tenantid'];
	 }
	}
   if( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['ApplicationId']))
	 {
	    $ApplicationId = $_POST['ApplicationId'];
	 }
	}	
	
	//echo $tenantid;
// ================================ EOC - tenantid =========================
//$customerid = '84080355450866'; //'620103554586';//
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		/*if(isurlencode($customerid))
		{
			$customerid = base64_decode(urldecode($customerid));
		}*/
	}
	else
	{
		if(isset($_POST['customerid']))
		  {
		  	$customerid = $_POST['customerid'];
			/*if(isurlencode($customerid))
			{
				$customerid = base64_decode(urldecode($customerid));
			}*/
		  }
	}
// -- UserCode Check for Page access(Restricted)
//if($IntUserCode != 'UFAC')
//{
	//if($IntUserCode != 'UFEC')
	//{
		//$location = "index.php?hash=".md5(date("h:i:sa"));
		//echo "<script>window.open('".$location."','_self')</script>";
//	}
//}
// -- Validate finApplications...
// Declarations
$im_member_id = '';
$im_title 				  = '';
$im_initials 			  = '';
$im_first_name  		  = '';
$im_policy_number_old	  = ''; //Member Migration--2024/04/14
$im_Middle_name 		  = ''; 
$im_surname 			  = '';
$im_gender 			  = '';
$im_Date_of_birth 		  =''; 
$im_maritalStatus =''; 
$im_marriage =''; 
$im_spouse_id =''; 
$im_address =''; 
$im_Life_State =''; 
$im_language_id ='';

// Spouse
$im_spousetitle ='';
$im_spousefirst_name ='';
$im_spouseMiddle_name ='';
$im_spouseinitials ='';
$im_spouseinitials2 = '';
$im_spousesurname =''; 
$im_spousegender =''; 
$im_spouseDate_of_birth =''; 
$im_maritalStatus =''; 
$im_marriage =''; 
$im_spouseaddress  ='';
$im_spouseLife_State ='';
$im_spouselanguage_id ='';

// address
$im_address_id ='';
$im_suburb_id ='';
$im_line_1='';
$im_line_2='';
$im_line_3='';
$im_line_4='';
$im_line_5='';
$im_line_6='';
$im_line_7='';
$im_line_8='';

// -- contact details...
$im_email='';
$im_fax='';
$im_cellphone='';
$im_work_tel='';
$im_home_tel='';
$im_comm_preference='';
 
// -- Source of income..
$Income_provider_Email 		 = '';
$Income_provider_name        = '';
$Income_provider_Contact_No  = '';
$source_of_income_2          = '';
$im_occupation_id_1          = '';

// -- Bank Details
$Account_number 			 = '';	
$Account_type 	 			 = '';
$Bank	 					 = '';
$Branch_name 	 			 = '';
$branch_name				 = '';
$Branch_code 				 = '';
$Personal_number 			 = '';	
$Debit_date 				 = '';
$Commence_date				 = ''; 
$product_service 			 = '1';
$Account_holder  			 = '';
$backHTML 					 = "display:none";
$tableChildren 				 = '';
$tableExtendedFamilyMembers  = '';
$tableBeneficiaries 		 = '';
$tableChildrenConfirm 				= '';
$tableExtendedFamilyMembersConfirm  = '';

// -- Member not responsible for account payemnts-  added 2023/07/09
$debtor_identification 			 = '';
$debtor_id_type 			 = '';
$debtor_contact_number 			 = '';
$debtor_email 			 = '';
//if(!isset($tableBeneficiariesConfirm)){$tableBeneficiariesConfirm			= '';}
//-------------------------------------------------------------------	
// --- Check if we are updating...
if(!empty($tenantid) && !empty($customerid))
{
	require  $filerootpath.'/ffms_update.php';
	$backHTML = "display:block";
 /*varchar(45) 
 varchar(45) 
 varchar(45) 
 varchar(45) 
 int 
 date 
 int 
 int 
 varchar(20) 
 int 
 int 
 int 
FILEIDDOC varchar(165) 
FILECONTRACT varchar(165) 
FILEBANKSTATEMENT varchar(165) 
FILEPROOFEMP varchar(165) 
FILEFICA varchar(165) 
FILECREDITREP varchar(165) 
FILECONSENTFORM varchar(165) 
FILEOTHER varchar(165) 
FILEOTHER2 varchar(165) 
FILEDEBITFORM varchar(165) 
FILEOTHER3 varchar(165) 
FILEOTHER4 varchar(165) 
IntUserCode varchar(45)*/
	// -Member--Declarations
	$im_member_id 			  = $main_member_data['Member_id'];
	$im_title 				  = $main_member_data['Title'];
	$im_initials 			  = $main_member_data['Initials'];
	$im_first_name  		  = $main_member_data['First_name'];
	$im_Middle_name 		  = $main_member_data['Middle_name']; 
	$im_surname 			  = $main_member_data['Surname'];
	$im_gender 			      = $main_member_data['Gender_id'];
	$im_Date_of_birth 		  = $main_member_data['Date_of_birth']; 
	$im_maritalStatus 		  = $main_member_data['MaritualStatus_id']; 
	$im_marriage 			  = $main_member_data['Marriage_id']; 
	$im_spouse_id 			  = $main_member_data['Spouse_id']; 
	$im_address 			  = $main_member_data['Address_id']; 
	$im_Life_State		      = $main_member_data['Life_State_id']; 
	$im_language_id 		  = $main_member_data['Language_id'];
	$im_policy_number_old	  = $main_member_data['Policy_id']; // Migration--2024/04/14
	// Spouse
	if(isset($dataMembership['spouse_id']))
	{
		$im_spousetitle 		  = $dataMembership['spousetitle'];
		$im_spousefirst_name 	  = $dataMembership['spousefirst_name'];
		$im_spouseMiddle_name 	  = $dataMembership['spousemiddle_name'];
		$im_spouseinitials 		  = $dataMembership['spouseinitials'];
		$im_spouseinitials2 	  = $dataMembership['spouseinitials'];
		$im_spousesurname 		  = $dataMembership['spousesurname']; 
		$im_spousegender 		  = $dataMembership['spousegender']; 
		$im_spouseDate_of_birth   = $dataMembership['spousedate_of_birth']; 
		//$im_maritalStatus 		  = $dataMembership['maritalStatus']; 
		//$im_marriage 			  = $dataMembership['marriage']; 
		$im_spouseaddress  		  = $dataMembership['spouseaddress'];
		$im_spouseLife_State 	  = $dataMembership['spouselife_State'];
		$im_spouselanguage_id 	  = $dataMembership['spouselanguage_id'];
	}
	// address
	$im_address_id 			  = $dataMembership['Address_id'];
	$im_suburb_id 			  = $dataMembership['Suburb_id'];
	$im_line_1				  = $dataMembership['Line_1'];
	$im_line_2				  = $dataMembership['Line_2'];
	$im_line_3				  = $dataMembership['Line_3'];
	$im_line_4				  = $dataMembership['Line_4'];
	$im_line_5				  = $dataMembership['Line_5'];
	$im_line_6				  = $dataMembership['Line_6'];
	$im_line_7				  = $dataMembership['Line_7'];
	$im_line_8				  = $dataMembership['Line_8'];
	// -- contact details...   
	$im_email				  = $dataMembership['Member_email'];
	$im_fax					  = $dataMembership['Fax'];
	$im_cellphone			  = $dataMembership['Cellphone'];
	$im_work_tel			  = $dataMembership['Work_tel'];
	$im_home_tel			  = $dataMembership['Home_tel'];
	$im_comm_preference	      = $dataMembership['Comm_preference'];
	
	// -- Source of income..
	$Income_provider_Email 		 = $dataMembership['Income_provider_email_address'];
	$Income_provider_name        = $dataMembership['Income_provider_name'];
	$Income_provider_Contact_No  = $dataMembership['Telelphone_nr'];
	$source_of_income_2          = $dataMembership['Source_of_income_name'];
	$im_occupation_id_1          = $dataMembership['Occupation_id'];

	// -- Bank Details
	if(isset( $dataMembership['Account_number'])){
		$Account_number 			 = $dataMembership['Account_number'];	
		$Account_type 	 			 = $dataMembership['Account_type'];
		$Bank	 					 = $dataMembership['Bank'];
		$Branch_name 	 			 = $dataMembership['Branch_name'];
		$Branch_code 				 = $dataMembership['Branch_code'];
		$Personal_number 			 = $dataMembership['Personal_number'];	
		$Debit_date 				 = $dataMembership['Debit_date'];
		$Commence_date				 = $dataMembership['Commence_date']; 
		$product_service 			 = $dataMembership['Policy_id'];
		$Account_holder  			 = $dataMembership['Account_holder'];	
		$Notify 					 = $dataMembership['notify'];
	  }
	}
	if(isset($_SESSION['tableBeneficiariesConfirm'])){
	//echo $_SESSION['tableBeneficiariesConfirm'];
	}
	
if (!empty($_POST))
   {
	//$tableBeneficiariesConfirm = $tableBeneficiariesConfirm;
//echo 'Modise';
// -- New Address Object
//$NewAddress = new AddressBLL($im_address_id, $im_suburb_id, $im_line_1, $im_line_2, $im_line_3, $im_email, $im_fax, $im_cellphone, $im_work_tel, $im_home_tel, $im_comm_preference);

// -- New Member Object
//$MemberBO  = new MemberBLL($im_member_id, $im_title, $im_first_name, $im_Middle_name, $im_initials, $im_surname, $im_gender, $im_Date_of_birth, $im_maritalStatus, $im_marriage, $im_spouse_id, $im_address, $im_Life_State, $im_language_id);


// -- New Object	
// -- if member is married Spouse details
//$MemberSpouseBO  = new MemberBLL($im_spouse_id, $im_spousetitle, $im_spousefirst_name, $im_spouseMiddle_name, $im_spouseinitials, $im_spousesurname, $im_spousegender, $im_spouseDate_of_birth, $im_maritalStatus, $im_marriage, $im_member_id, $im_spouseaddress, $im_spouseLife_State, $im_spouselanguage_id);
   }
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>
<div class="container background-white bottom-border">
<br/>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
          <a id="step1"  href="#step-1" type="button" class="btn btn-primary btn-circle inactiveLink">1</a>
					<p><strong>Product</strong></p>

        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
            <p><strong>Customer Data</strong></p>
        </div>
        <div class="stepwizard-step">
            <a id="step3" href="#step-3" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
            <p><strong>Dependants</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">4</a>
            <p><strong>Method of Payment</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-5" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">5</a>
            <p><strong>Supporting Docs</strong></p>
        </div>
				<div class="stepwizard-step">
						<a href="#step-6" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
						<p><strong>Activation</strong></p>
				</div>
		<div class="stepwizard-step">
			<a href="#step-7" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
			<p><strong>Complete</strong></p>
		</div>
    </div>
</div>
</br>
<form name="ffms_finApplicationform" id="ffms_finApplicationform" method="POST" action="#" style="display:block" role="form">
<?php include $filerootpath.'/step1_ffms_application.php'; ?>
<?php include $filerootpath.'/step2_ffms_application.php'; ?>
<?php include $filerootpath.'/step3_ffms_application.php'; ?>
<?php include $filerootpath.'/step4_ffms_application.php'; ?>
<?php include $filerootpath.'/step5_ffms_application.php'; ?>
<div class="row setup-content" id="step-6">
 <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_confirmation" name="message_confirmation" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div></div></div>
<?php include $filerootpath.'/confirm_ffms_application.php'; ?>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Activate</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
 <div class="row setup-content" id="step-7">
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Create new application</strong></button>
			  </div>			  
			  <div class="col-md-6">
			    <a name="ffms_link" id="ffms_link" class="btn btn-info"  target="_blank" href="#">Policy Schedule</a>
			  </div>
			</div>
			</div>
			<br/>
			</div></div>
			 <br/>
			 <!--<a href="finApplicationform" class="btn btn-primary nextBtn btn-lg pull-right" name="PayNow" id="PayNow" type="button" ><strong>Create new application</strong></a>-->
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary btn-lg pull-right" name="Exit" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>
</div>

</form>
</div>
<script>
//Dependants
// -- 18 September 2021 -- //
function servicetypeselect(sel)
{
			var message_info   = document.getElementById('rulealert');
			message_info.className  = '';
			message_info.innerHTML  = '';

			var Service_type = 'TWO DAY';//sel.options[sel.selectedIndex].value;
			if(Service_type == 'TWO DAY')
			{
				// -- Call the rules
				applyrules(sel);
			}
			// -- 13.11.2021
			if(Service_type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}
}
function applyrules_sameday(sel)
{
		
}
function applyrules(sel)
{
		/*var selectedService_Type = 'SAMEDAY';//document.getElementById('Service_Type').value;
		var Action_Date_EL = document.getElementById("Debit_date");
		var Action_Date_B4 = Action_Date_EL.value;
		var message_info   = document.getElementById('rulealert');

		var list =
		{'Action_Date'			: Action_Date_EL.value,
		 'Type' : selectedService_Type}

		if(selectedService_Type == 'TWO DAY')
		{
			//alert(selectedService_Type);
		// -- jQuery for PHP file
		jQuery(document).ready(function()
		{
			jQuery.ajax({  type: "POST",
					   url:"script_rule_two_day_service.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							if(feedback.includes("Proposed"))
							{
							  var res = feedback.split("|");
							  Action_Date_EL.value = res[1];
							  message_info.className  = 'alert alert-warning';
							  message_info.innerHTML  = 'Action Date :'+Action_Date_B4+' is a weekend or holiday. system auto generated action date:'+res[1];
							}
							else
							{
								message_info.className  = '';
							    message_info.innerHTML  = '';
								Action_Date_EL.value = feedback;
							}
						 }
			});
		});
		}
		//script_rule_two_day_service.php

				// -- 13.11.2021
			if(selectedService_Type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}*/
}

function valueselect(sel)
 {
		var Service_Mode = sel.options[sel.selectedIndex].value;
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);
							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});
			  return false;
  }

  $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
		allpreviousBtn = $('.previousBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

	allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;
		//alert(objToString(allpreviousBtn));

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

 });


    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-6')
			{
				var message_info 	   = document.getElementById('message_info');
				message_info.innerHTML = '';
				message_info.className = '';
				
				/*var message_confirmation 	   = document.getElementById('message_confirmation');
				message_confirmation.innerHTML = '';
				message_confirmation.className = '';*/
				
				ret2 = 0;
				ret2 = check_duplicate();
				//alert(ret2);
				if(ret2 === 1)
				{
					isValid = false;
				}
						add_confirm_instruction();
			}
			// -- files uploads
			if(curStepBtn == 'step-5')
			{
				//if(!validateFiles())
			//	{
				//	isValid = false;
			//	}
	
			}
			// -- Dependant/Extend Family/Benefiaciary...
			if(curStepBtn == 'step-3')
			{
				//if(check_dependent())
				//{
				//	isValid = false;
				//}

				//check_dependent();
			}
        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus();
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');

		add_confirm_instruction();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});


function maxLengthNumber(el,maxlength)
{
	//alert(maxlength);
	if(el.value.length == maxlength)
	{
		return false;
	}
}
function toStep1()
{
location.reload();
document.getElementById("step1").click();
}



function toStep3()
{
	//alert("go to three");
document.getElementById("step3").click();
}
//hide and show fields
function debtor(z)
{
//alert(z);
	if(z==0)
	{
	document.getElementById('Debtor').style.display='block';
	}
	else
	{
	document.getElementById('Debtor').style.display='none';
	}
return;
}
//hide and show fields- Member Migration--2024/04/14
function policystatus(z)
{
//alert(z);
	if(z==0)
	{
	document.getElementById('MemberStatus').style.display='block';
	}
	else
	{
	document.getElementById('MemberStatus').style.display='none';
	}
return;
}


function clear_tables()
{
// 1-- main_member
   $("#main_member").find("tr:gt(0)").remove();

  var tablemain_member = document.getElementById("main_member");
  var main_member_row = tablemain_member.insertRow(); 
  var fullname = main_member_row.insertCell(0);
  var idno     = main_member_row.insertCell(1);
  var premium  = main_member_row.insertCell(2);
// -- Data..
	fullname.innerHTML  = "Surname and Name";
    idno.innerHTML      = "ID No";
    premium.innerHTML   = "Premium";
// 2-- spouse
   $("#spouse").find("tr:gt(0)").remove();

  var tableSpouse 	 = document.getElementById("spouse");
  var Spouse_row = tableSpouse.insertRow();
  var fullname = Spouse_row.insertCell(0);
  var idno     = Spouse_row.insertCell(1);
  var Date_Joined = Spouse_row.insertCell(2);
// -- Data..
	fullname.innerHTML  	= "Surname and Name";
    idno.innerHTML      	= "ID No";
    Date_Joined.innerHTML   = "Date Joined";
// 3-- Children
     $("#Children").find("tr:gt(0)").remove();

  var tableChildren	 = document.getElementById("Children");
  var Spouse_row = tableChildren.insertRow();
  var fullname = Spouse_row.insertCell(0);
  var idno     = Spouse_row.insertCell(1);
  var Date_Joined = Spouse_row.insertCell(2);
// -- Data..
	fullname.innerHTML  	= "Surname and Name";
    idno.innerHTML      	= "ID No";
    Date_Joined.innerHTML   = "Date Joined";
    // 4-- Extended
     $("#Extended").find("tr:gt(0)").remove();
		
// -- Extended  confirm adding...
var tableExtended = document.getElementById("Extended");
var Extended_row = tableExtended.insertRow();
// -- Extended_row -- //
	var fullname = Extended_row.insertCell(0);
    var idno     = Extended_row.insertCell(1);
    var Waiting_Period = Extended_row.insertCell(2);
    var Benefits = Extended_row.insertCell(3);
    var Date_Joined = Extended_row.insertCell(4);
// -- Data..
	fullname.innerHTML  	  = "Surname and Name";
    idno.innerHTML      	  = "ID No";
    Waiting_Period.innerHTML  = "Waiting_Period";
    Benefits.innerHTML  	  = "Benefits";
    Date_Joined.innerHTML     = "Date Joined";
// 5-- benefiary
   $("#benefiary").find("tr:gt(0)").remove();

  var tablebenefiary = document.getElementById("benefiary");
  var main_member_row = tablebenefiary.insertRow(); 
  var fullname = main_member_row.insertCell(0);
  var idno     = main_member_row.insertCell(1);
// -- Data..
	fullname.innerHTML  = "Surname and Name";
    idno.innerHTML      = "ID No";
}

//var doc = new jsPDF();

function add_confirm_instruction()
{
var date_joined  = <?php echo "'".date('Y-m-d')."'"; ?>;
var today  = <?php echo "'".date('Y-m-d')."'"; ?>;
var policyid     = "pending activation";
var province     = document.getElementById('im_line_6').value;

var packageOpS = <?php echo "'".$product_service."'";?>; 
var packageOp = <?php echo "'".get_ffms_package_option($product_service)."'";?>;
//var premiumAmnt   = <?php $Policy2 = ''; echo "'".get_ffms_premium_amnt($Policy2)."'";?>;
var benefitsAmnt  = <?php echo "'".get_ffms_benefits($product_service)."'";?>;
//alert(premiumAmnt);
var IntUserCode = <?php echo "'".$IntUserCode."'";?>;
//alert(IntUserCode);

var logoConfigured = <?php echo "'".$tenant_array['logo']."'"; ?>;
var logoElement = document.getElementById("logo"); //images/rflogo.jpg
logoElement.src = logoConfigured;

var ffms_link = document.getElementById('ffms_link');
var ffms_header = document.getElementById("ffms_header");
var ffms_header_db = <?php echo json_encode($tenant_array['header']); ?>;

var ffms_footer = document.getElementById("ffms_footer");
var ffms_footer_db = <?php echo  json_encode($tenant_array['footer']) ; ?>;
var ffms_ems  = <?php echo  json_encode($tenant_array['ffms_ems']) ; ?>;
var ffms_email  = <?php echo  json_encode($tenant_array['ffms_email']) ; ?>;
var ffms_phone  = <?php echo  json_encode($tenant_array['ffms_phone']) ; ?>;
var Org_Name  = <?php echo  json_encode($tenant_array['Org_Name']) ; ?>;

//Get Selected Package 
var x = document.getElementById("product_service").selectedIndex;
var y = document.getElementById("product_service").options;
//alert("Index: " + y[x].index + " is " + y[x].text);
var text =y[x].text;
const myArray = text.split("-");
let word = myArray[0];
let word2 = myArray[1];
var subcription  = word;
var product_subsciption = word2;
var premiumAmnt   =word2;
//Get Member Title
var w = document.getElementById("im_title").selectedIndex;
var t = document.getElementById("im_title").options;
var im_title  = t[w].text;
//Get Last Name
var im_surname = document.getElementById("im_surname").value;

// replace placeholders.
ffms_footer_db = ffms_footer_db.replaceAll("{member_policyid}",policyid);
ffms_footer_db = ffms_footer_db.replaceAll("{member_province}", province);
ffms_footer_db = ffms_footer_db.replaceAll("{Org_Name}", Org_Name);
//ffms_footer_db = ffms_footer_db.replaceAll("{member_premium}", premiumAmnt);
ffms_footer_db = ffms_footer_db.replaceAll("{ffms_ems}", ffms_ems);
ffms_footer_db = ffms_footer_db.replaceAll("{ffms_email}", ffms_email);
ffms_footer_db = ffms_footer_db.replaceAll("{ffms_phone}", ffms_phone);
ffms_footer_db = ffms_footer_db.replaceAll("{member_debitdate}", document.getElementById('Debit_date').value);
ffms_footer_db = ffms_footer_db.replaceAll("{member_benefits}", benefitsAmnt);

ffms_header_db = ffms_header_db.replaceAll("{member_policyid}", policyid);
ffms_header_db = ffms_header_db.replaceAll("{member_province}", province);
ffms_header_db = ffms_header_db.replaceAll("{Org_Name}", Org_Name);
ffms_header_db = ffms_header_db.replaceAll("{premiumAmnt}", premiumAmnt);
ffms_header_db = ffms_header_db.replaceAll("{ffms_ems}", ffms_ems);
ffms_header_db = ffms_header_db.replaceAll("{ffms_email}", ffms_email);
ffms_header_db = ffms_header_db.replaceAll("{ffms_phone}", ffms_phone);
ffms_header_db = ffms_header_db.replaceAll("{member_debitdate}", document.getElementById('Debit_date').value);
ffms_header_db = ffms_header_db.replaceAll("{member_benefits}", benefitsAmnt);
ffms_header_db = ffms_header_db.replaceAll("{subcription}", subcription);
ffms_header_db = ffms_header_db.replaceAll("{im_title}", im_title);
ffms_header_db = ffms_header_db.replaceAll("{im_surname}", im_surname);
ffms_header_db = ffms_header_db.replaceAll("{product_subsciption}", product_subsciption);
ffms_im_member_id = document.getElementById("im_member_id").value;

ffms_footer.innerHTML = ffms_footer_db;
ffms_header.innerHTML = ffms_header_db;
var confirm_ffms_contents = document.getElementById('confirm_ffms').innerHTML;
//alert(confirm_ffms_contents);						
//encodeURI(confirm_ffms_contents);//
//var encoded = btoa(confirm_ffms_contents);
//alert(encoded);

////////////////////////////////////////////////////////////////////////

// -- Clear Tables...
	clear_tables();
	
// -- main_member  confirm adding...
  var tablemain_member = document.getElementById("main_member");
  var main_member_row = tablemain_member.insertRow();
  var member_id = document.getElementById('im_member_id').value;
  var First_name = document.getElementById('im_first_name').value;	
  var Surname    = document.getElementById('im_surname').value;	
  var member_fullname = First_name+ " "+Surname;
 
	var fullname = main_member_row.insertCell(0);
    var idno     = main_member_row.insertCell(1);
    var premium = main_member_row.insertCell(2);

   ffms_link.href  = "effms_confirmation?tenantid=<?php echo "".$tenant_array['Client_Id'].""; ?>&customerid="+member_id;

// -- Data..
	fullname.innerHTML  = member_fullname;
    idno.innerHTML      = member_id;
    premium.innerHTML  = premiumAmnt;	
////////////////////////////////////////////////////////////////////////
var spouse_id = document.getElementById('im_spouse_id').value;			 
var spousefirst_name = document.getElementById('im_spousefirst_name').value;
var spousesurname = document.getElementById('im_spousesurname').value;

// -- Spouse  confirm adding...
  var table = document.getElementById("spouse");
  var Spouse_row = table.insertRow();

	var fullname = Spouse_row.insertCell(0);
    var idno     = Spouse_row.insertCell(1);
    var Date_Joined = Spouse_row.insertCell(2);

// -- Data..
	fullname.innerHTML  = spousefirst_name+" "+spousesurname;
    idno.innerHTML      = spouse_id;
	if(spouse_id != '')
    {Date_Joined.innerHTML  = date_joined;}
////////////////////////////////////////////////////////////////////////
// -- Children  confirm adding...
  var tableChildren = document.getElementById("Children");
// -- Dependant -- //
var table = document.getElementById("dependent");
var tbodyRowCount = $("#dependent > tbody > tr").length;
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) 
{
    var Children_row = tableChildren.insertRow();
	var fullname = Children_row.insertCell(0);
    var idno     = Children_row.insertCell(1);
    var Date_Joined = Children_row.insertCell(2);

// -- Data..
	if(tableDependent.rows[i].cells[3].innerHTML != '')
	{
		fullname.innerHTML  = tableDependent.rows[i].cells[0].innerHTML+" "+tableDependent.rows[i].cells[2].innerHTML;
        idno.innerHTML      = tableDependent.rows[i].cells[3].innerHTML;
        Date_Joined.innerHTML  = date_joined;
	}
}
////////////////////////////////////////////////////////////////////////
// -- Extended  confirm adding...
  var tableExtended = document.getElementById("Extended");
// -- Extended_row -- //
var table = document.getElementById("extendend");
var tbodyRowCount = $("#extendend > tbody > tr").length;
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) 
{
	var Extended_row = tableExtended.insertRow();
	var fullname = Extended_row.insertCell(0);
    var idno     = Extended_row.insertCell(1);
    var Waiting_Period = Extended_row.insertCell(2);
    var Benefits = Extended_row.insertCell(3);
    var Date_Joined = Extended_row.insertCell(4);
// -- Data..
	if(tableDependent.rows[i].cells[3].innerHTML != ''){
	fullname.innerHTML  = tableDependent.rows[i].cells[0].innerHTML+" "+tableDependent.rows[i].cells[2].innerHTML;
    idno.innerHTML      = tableDependent.rows[i].cells[3].innerHTML;
    Waiting_Period.innerHTML  = "6 Months";
    Benefits.innerHTML  = benefitsAmnt;
	Date_Joined.innerHTML  = date_joined;}
}
////////////////////////////////////////////////////////////////////////
// -- Benefiaciary confirm adding...
  var tablebenefiary = document.getElementById("benefiary");
// -- benefiaciary -- //
var table = document.getElementById("benefiaciary");
var tbodyRowCount = $("#benefiaciary > tbody > tr").length;
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) 
{
	var ben_row = tablebenefiary.insertRow();
	var fullname = ben_row.insertCell(0);
    var idno     = ben_row.insertCell(1);
// -- Data..
	fullname.innerHTML  = tableDependent.rows[i].cells[0].innerHTML+" "+tableDependent.rows[i].cells[2].innerHTML;
    idno.innerHTML      = tableDependent.rows[i].cells[3].innerHTML;
}
////////////////////////////////////////////////////////////////////////
	// -- Details -- //
	document.getElementById('member_applicationdate_Lbl').innerHTML = today;
	document.getElementById('member_name_Lbl').innerHTML 	  = member_fullname;
	document.getElementById('member_province_Lbl').innerHTML  = document.getElementById('im_line_6').value;
	document.getElementById('member_contact_Lbl').innerHTML   = document.getElementById('im_cellphone').value;
	//document.getElementById('member_debitdate_Lbl').innerHTML = '';//document.getElementById('Debit_date').value;
	//document.getElementById('member_premium_Lbl').innerHTML   = premiumAmnt;//document.getElementById('Debit_date').value;
	//document.getElementById('member_policy_Lbl').innerHTML   = policyid;


	
}
function checkIfDuplicateExists(arr) {
    return new Set(arr).size !== arr.length
}
function check_dependent()
{
var ret = false;
var dependents_pk = [];
var message_info = document.getElementById('message_extended');
var row = 0;
message_info.innerHTML = '<br/>';
message_info.className = '';							 

////////////////////////////////////////////////////////////////////////
// -- Dependant
var table = document.getElementById("dependent");
var tbodyRowCount = $("#dependent > tbody > tr").length;
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{
	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'3',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	row = row + 1;

	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							 message_info.innerHTML = '<br/>'+feedback;
							 message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates DEPENDANT DETIALS Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}	
/////////////////////////////////////////////////////////////////////////////
// -- Extetended family
////////////////////////////////////////////////////////////////////////
if(!ret){
var table = document.getElementById("extendend");
var tbodyRowCount = $("#extendend > tbody > tr").length;
var tableDependent = table.tBodies[0];
var dependents_pk = [];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{

	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'4',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	
	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates EXTENDED FAMILY DETAILS Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}}	
/////////////////////////////////////////////////////////////////////////////
// -- benefiaciary 
////////////////////////////////////////////////////////////////////////
if(!ret){
var table = document.getElementById("benefiaciary1");
var tbodyRowCount = $("#benefiaciary > tbody > tr").length;
var tableDependent = table.tBodies[0];
var dependents_pk = [];
for (let i = 0; i < tbodyRowCount; i++) 
{
	dependents_pk.push(tableDependent.rows[i].cells[3].innerHTML);
}

// -- Checkj if duplicates exist...
if(!checkIfDuplicateExists(dependents_pk)){
//alert(tbodyRowCount);
for (let i = 0; i < tbodyRowCount; i++) 
{
	//alert(tableDependent.rows[i].cells[3].innerHTML);
	var list =
	{//3 - Dependent
		'Ext_member_id'	:tableDependent.rows[i].cells[3].innerHTML,	
		'Ext_memberType_id'	:'4',						
		'Main_member_id'	:document.getElementById('im_member_id').value								
	}
	jQuery.ajax({  type: "POST",
					async: "false",
					   url:"script_checkduplicate_dependent_ffms.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback 	= data;
							if(feedback === '')
							{
							 ret = false;
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = '';							 
							}
							else
							{
							ret = true;		
							message_info.innerHTML = '<br/>'+feedback;
							message_info.className = 'alert alert-error';
							toStep3();
							}
						 }
			});
}}
else
{
	ret = true;
	message_info.innerHTML = '<br/>'+'There are duplicates NOMINATED BENEFIACIARY Identity Number.Please correct!';
	message_info.className = 'alert alert-error';
}}	
////////////////////////////////////////////////////////////////////////
//alert(message_info.innerHTML );
			return ret; 
}
function check_duplicate()
{
// 1 -- MemberID	
var member =
{ 'Member_id' 		    :document.getElementById('im_member_id').value};

				var feedback = '';
				var ret = 0;
var update1 = <?php echo "'".$backHTML."'"; ?>;
//alert(update1);
var update = {'update':update1}

				//--script_checkduplicate_ffms.php
				jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"script_checkduplicate_ffms.php",
									   data:{member:member,update:update},
										success: function(data)
										{
										 feedback = data;
										 
										 if(feedback === '')
										 {
											 feedback  = 'Are you sure you want to create a new member?';
											 ret = 0;
										 }
										 else
										 {
											ret = 1;											 
											var message_info       = document.getElementById('message_info');
											message_info.innerHTML = feedback;
											message_info.className = 'alert alert-error';
										 }
										if(ret == 0)
										{
										if (confirm(feedback))
										{
											//-- Call call_script_ffms_application!											  
											  
												call_script_ffms_application();
										}
										else
										{
										  //-- Do nothing!
										    toStep1();
										}}
						}});
				});
				return ret;
}
function validateFiles()
{
	var ret = true;
	var fileInput1 = document.getElementById('files').value;
	var fileInput2 = document.getElementById('files2').value;
	var fileInput3 = document.getElementById('files3').value;
	var fileInput4 = document.getElementById('files4').value;
	var fileInput5 = document.getElementById('files5').value;
    var message_info = document.getElementById('message_infoUploadfile');

	
	var file1 = 'ID Document';
	var file2 = 'Contract Agreement';
	var file3 = 'Current Bank statement';
	var file4 = 'Proof of income';
	var file5 = 'Proof of residence';

	 	message_info.innerHTML = '';
		message_info.className  = '';
	
  if(!validFileExt(fileInput1))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file1 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;	  
  }
  else if(!validFileExt(fileInput2))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file2 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput3))
  {
	 	message_info.innerHTML = 'Invalid file format for '+ file3 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput4))
  {
		message_info.innerHTML = 'Invalid file format for '+ file4 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  else if(!validFileExt(fileInput5))
  {
		message_info.innerHTML = 'Invalid file format for '+ file5 +' please upload .pdf';
		message_info.className  = 'alert alert-error';
		ret = false;
  }
  
if(ret){
	filesize1 = document.getElementById('files').files[0].size;
	filesize2 = document.getElementById('files2').files[0].size;
	filesize3 = document.getElementById('files3').files[0].size;
	filesize4 = document.getElementById('files4').files[0].size;
	filesize5 = document.getElementById('files5').files[0].size;
	
	if(!validFileSize(filesize1))
	{
	 message_info.innerHTML = file1 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize2))
	{
	 message_info.innerHTML = file2 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize3))
	{
	 message_info.innerHTML = file3 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize4))
	{
	 message_info.innerHTML = file4 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
	else if(!validFileSize(filesize5))
	{
	 message_info.innerHTML = file5 +' file is too large!. More than 10MB';
	 message_info.className  = 'alert alert-error';		
	 ret = false;
	}
}
	return ret; 
}
function validFileSize(filesize)
{
	var max_file_size = 1024*10000; //10MB
	if (filesize > max_file_size)
	{
		return false;
	}else{return true;}
}
function validFileExt(file)
{
    var extension = file.substr( (file.lastIndexOf('.') +1) );
	//alert(extension);
	switch(extension) {
        case 'pdf':
		   return true;
		default:
           return false;
	}
}
function change_marital(value)
{
	var marital_status = document.getElementById('im_maritalStatus').value;
	//alert(marital_status);
}	

function id_to_dob()
{
	var member_id = document.getElementById('im_member_id').value;
    var current_year = new Date().getFullYear().toString().substr(-2);
	var current_year_prefix = new Date().getFullYear().toString().substr(0,2);
	var get_typed_year  = member_id.substring(0,2);
	var get_typed_month   = member_id.substring(2,4);
	var get_typed_day   = member_id.substring(4,6);
	
	var yob = "";
	var dob = "";
	if(current_year >= get_typed_year)
	{
		yob = current_year_prefix + get_typed_year;
	}
	else
	{
		 yob = "19" + get_typed_year;
	}
	//dob = yob+"-"+get_typed_month+"-"+get_typed_day;
	//alert(yob+"-"+get_typed_month+"-"+get_typed_day);
	document.getElementById("im_Date_of_birth").value = yob+"-"+get_typed_month+"-"+get_typed_day;
}

function get_memberid()
{
  return document.getElementById('im_member_id').value;
}
function call_script_ffms_application()
{
var confirm_ffms_contents = document.getElementById('confirm_ffms').innerHTML;
//alert(confirm_ffms_contents);						
var userid = <?php echo "'".$userid."'"; ?>;
var UserCode = <?php echo "'".$UserCode."'"; ?>;
var IntUserCode = <?php echo "'".$tenant_array['Client_Id']."'"; ?>;
var policy_number_existing=document.getElementById('im_policy_number_existing').value;
var update = <?php echo "'".$backHTML."'"; ?>;
var Member_Policy_id  = '';
var Address_id        = '';
var Member_income_id  = '';
var FILEIDDOC 		  = '';
var FILECONTRACT 	  = ''; 
var FILEBANKSTATEMENT = ''; 
var FILEPROOFEMP 	  = ''; 
var FILEFICA          = '';
var Member_bank_account_id = '';
if(update == 'display:block')
{
	Member_Policy_id = <?php if(isset($dataMembership)){echo "'".$dataMembership['Member_Policy_id']."'";}else{echo "''";} ?>;
	Address_id = <?php if(isset($dataMembership)){echo "'".$dataMembership['Address_id']."'";}else{echo "''";} ?>;	
	Member_income_id = <?php if(isset($dataMembership)){echo "'".$dataMembership['Member_income_id']."'";}else{echo "''";} ?>;	
	FILEIDDOC 		  = <?php if(isset($dataMembership)){echo "'".$dataMembership['FILEIDDOC']."'";}else{echo "''";} ?>;	
	FILECONTRACT 	  = <?php if(isset($dataMembership)){echo "'".$dataMembership['FILECONTRACT']."'";}else{echo "''";} ?>;	 
	FILEBANKSTATEMENT = <?php if(isset($dataMembership)){echo "'".$dataMembership['FILEBANKSTATEMENT']."'";}else{echo "''";} ?>;	
	FILEPROOFEMP 	  = <?php if(isset($dataMembership)){echo "'".$dataMembership['FILEPROOFEMP']."'";}else{echo "''";} ?>;	 
	FILEFICA          = <?php if(isset($dataMembership)){echo "'".$dataMembership['FILEFICA']."'";}else{echo "''";} ?>;	
	Member_bank_account_id = <?php if(isset($dataMembership)){echo "'".$dataMembership['Member_bank_account_id']."'";}else{echo "''";} ?>;
}
//alert(Member_income_id);
var branchcode = '';
var ffms_branch_name = document.getElementById("branch_name").value;
//alert(ffms_branch_name+document.getElementById('Account_holder').value);
//alert(ffms_branch_name+document.getElementById('im_line_1').value);
//alert(ffms_branch_name+document.getElementById('im_line_2').value);
//alert(ffms_branch_name+document.getElementById('im_line_3').value);
//alert(ffms_branch_name+document.getElementById('im_line_4').value);
//alert(ffms_branch_name+document.getElementById('im_line_5').value);
//alert(ffms_branch_name+document.getElementById('im_line_6').value);
//alert(ffms_branch_name+document.getElementById('im_line_7').value);
//alert(ffms_branch_name+document.getElementById('im_line_8').value);
//alert(ffms_branch_name+document.getElementById('im_email').value);
//alert(ffms_branch_name+document.getElementById('Income_provider_Email').value);



var member_gender_value = '';
var member_gender = document.getElementsByName('im_gender');

for(var i = 0; i < member_gender.length; i++){
    if(member_gender[i].checked){
        member_gender_value = member_gender[i].value;
    }
}

	//alert(member_gender_value);
var feedback = '';
var ret = 0;
var file = document.getElementById('files').files[0];
var file2 = document.getElementById('files2').files[0];
var file3 = document.getElementById('files3').files[0];
var file4 = document.getElementById('files4').files[0];
var file5 = document.getElementById('files5').files[0];

var form = $('ffms_finApplicationform')[0];
var formData = new FormData(form);
formData.append("update", update);
formData.append("Member_Policy_id", Member_Policy_id);
formData.append("Address_id", Address_id);
formData.append("FILEIDDOC",FILEIDDOC);
formData.append("FILECONTRACT",FILECONTRACT);
formData.append("FILEBANKSTATEMENT",FILEBANKSTATEMENT);
formData.append("FILEPROOFEMP",FILEPROOFEMP);
formData.append("FILEFICA",FILEFICA);
formData.append("Member_bank_account_id",Member_bank_account_id);
formData.append("Member_income_id",Member_income_id);

// Files
formData.append("File", file);
formData.append("File2", file2);
formData.append("File3", file3);
formData.append("File4", file4);
formData.append("File5", file5);

// 1 -- Member	
formData.append('Member_id' 		    ,document.getElementById('im_member_id').value);		
formData.append('Title' 			 	,document.getElementById('im_title').value);	
formData.append('First_name' 			,document.getElementById('im_first_name').value);	
formData.append('Middle_name' 			,document.getElementById('im_Middle_name').value);	
formData.append('Initials' 	 			,document.getElementById('im_initials').value);	
formData.append('Surname' 				,document.getElementById('im_surname').value);	
formData.append('Gender_id' 			,member_gender_value);	
formData.append('Date_of_birth'			,document.getElementById('im_Date_of_birth').value);	 	
formData.append('MaritualStatus_id'		,document.getElementById('im_maritalStatus').value);			
formData.append('Marriage_id'			,document.getElementById('im_marriage').value);	
formData.append('Spouse_id'				,document.getElementById('im_spouse_id').value);	
formData.append('Life_State_id' 		,'1');//document.getElementById('im_Life_State').value,		
formData.append('Language_id' 			,'2');
formData.append('policy_number_existing',document.getElementById('im_policy_number_existing').value);
// 2 -- Spouse
formData.append('spouse_id' 			,document.getElementById('im_spouse_id').value);			 
formData.append('spousetitle'			,document.getElementById('im_spousetitle').value);
formData.append('spousefirst_name' 		,document.getElementById('im_spousefirst_name').value);
formData.append('spousemiddle_name' 	,document.getElementById('im_spouseMiddle_name').value);	
formData.append('spouseinitials' 		,document.getElementById('im_spouseinitials2').value);
formData.append('spousesurname' 		,document.getElementById('im_spousesurname').value);
formData.append('spousegender' 			,'');//document.getElementById('im_spousegender').value,
formData.append('spousedate_of_birth' 	,'');//document.getElementById('im_spouseDate_of_birth').value,
formData.append('spouseaddress' 		,'');//document.getElementById('im_spouseaddress').value,
formData.append('spouselife_State' 		,'1');//document.getElementById('im_spouseLife_State').value,
formData.append('spouselanguage_id' 	,'2');//document.getElementById('im_spouselanguage_id').value};	

// 3 - address
formData.append('Suburb_id'				,'10');//document.getElementById('im_suburb_id').value,	  
formData.append('Line_1'				,document.getElementById('im_line_1').value);	//Postal addresss
formData.append('Line_2'				,document.getElementById('im_line_2').value);	//Surbub
formData.append('Line_3'				,document.getElementById('im_line_3').value);	//Town
formData.append('Line_4'				,document.getElementById('im_line_4').value);	//Province

formData.append('Line_5'				,document.getElementById('im_line_5').value);	//Physical addresss
formData.append('Line_6'				,document.getElementById('im_line_6').value);	//Surbub
formData.append('Line_7'				,document.getElementById('im_line_7').value);	//Town
formData.append('Line_8'				,document.getElementById('im_line_8').value);	//Province


formData.append('Member_email'			,document.getElementById('im_email').value);	
formData.append('Fax'					,'');//document.getElementById('im_fax').value,	
formData.append('Cellphone'				,document.getElementById('im_cellphone').value);	
formData.append('Work_tel'				,document.getElementById('im_work_tel').value);	
formData.append('Home_tel'				,document.getElementById('im_home_tel').value);	
formData.append('Comm_preference'		,'');//document.getElementById('im_comm_preference').value};	

// 4-source of income
formData.append('Source_of_income_id'	,document.getElementById('source_of_income_2').value);						
formData.append('Occupation_id'			,document.getElementById('im_occupation_id_1').value);			
formData.append('Member_id'				,document.getElementById('im_member_id').value);		
formData.append('Income_provider_name'	,document.getElementById('Income_provider_name').value);					        
formData.append('Telelphone_nr'			,document.getElementById('Income_provider_Contact_No').value);			        
formData.append('Income_provider_email_address'	,document.getElementById('Income_provider_Email').value);

//5-banking_details
formData.append('Main_member_id' 	,document.getElementById('im_member_id').value); 
formData.append('Account_holder' 	,document.getElementById('Account_holder').value); 
formData.append('Account_number' 	,document.getElementById('Account_number').value); 	
formData.append('Account_type' 	 	,document.getElementById('Account_type').value); 
formData.append('Bank'	 			,document.getElementById('Bank').value); 
formData.append('Personal_number' 	,document.getElementById('Personal_number').value); 	
formData.append('Debit_date' 		,document.getElementById('Debit_date').value); 
formData.append('Commence_date'		,document.getElementById('Commence_date').value); 

// 6. -- Dependent Details ...
formData.append('createdby' 		, userid);
formData.append('IntUserCode'		, IntUserCode);
formData.append('UserCode' 			, ffms_branch_name);

// 7 -- member policy
formData.append('Member_id'  ,document.getElementById('im_member_id').value);
formData.append('Policy_id'  ,document.getElementById('product_service').value); 
formData.append('Active' 	 ,'0');
formData.append('Remarks' 	 ,'successfully created');
//alert(document.getElementById('im_member_id').value);
// 8. -- Dependent Details ...
var table = document.getElementById("dependent");
var tbodyRowCount = $("#dependent > tbody > tr").length;
if(tbodyRowCount > 0)
{
	var tableDependent = table.tBodies[0];
	var row = 0;
	for (let i = 0; i < tbodyRowCount; i++) {
		//alert(tableDependent.rows[i].cells[0].innerHTML);
		formData.append('Ext_member_name'+row,tableDependent.rows[i].cells[0].innerHTML);
		formData.append('middlename'+row,tableDependent.rows[i].cells[1].innerHTML);
		formData.append('Ext_member_surname'+row,tableDependent.rows[i].cells[2].innerHTML);
		formData.append('Ext_member_id'+row,tableDependent.rows[i].cells[3].innerHTML);
		formData.append('Relationship'+row,tableDependent.rows[i].cells[4].innerHTML);
		formData.append('Remarks'+row,tableDependent.rows[i].cells[5].innerHTML);
		formData.append('Active'+row,'0');
		formData.append('Ext_memberType_id'+row,'3');//Dependent
		formData.append('Life_state_id'+row,'1');
		formData.append('Main_member_id'+row,document.getElementById('im_member_id').value);
		formData.append('Percentage'+row,'');		
		row = row + 1;
	}
}
// 9. -- Extended Dependent Details ...
var table		   = document.getElementById("extendend");
var tbodyRowCount  = $("#extendend > tbody > tr").length;
if(tbodyRowCount > 0)
{
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) {
	//alert(tableDependent.rows[i].cells[0].innerHTML);
    formData.append('Ext_member_name'+row,tableDependent.rows[i].cells[0].innerHTML);
    formData.append('middlename'+row,tableDependent.rows[i].cells[1].innerHTML);
    formData.append('Ext_member_surname'+row,tableDependent.rows[i].cells[2].innerHTML);
	formData.append('Ext_member_id'+row,tableDependent.rows[i].cells[3].innerHTML);
    formData.append('Relationship'+row,tableDependent.rows[i].cells[4].innerHTML);
	formData.append('Remarks'+row,tableDependent.rows[i].cells[5].innerHTML);
    formData.append('Active'+row,'0');
    formData.append('Ext_memberType_id'+row,'4');//Extended family--
    formData.append('Life_state_id'+row,'1');
    formData.append('Main_member_id'+row,document.getElementById('im_member_id').value);
	formData.append('Percentage'+row,'');	
	row = row + 1;
}}

// formData.append('Ext_memberType_id'+i,tableDependent[4][i]);
// 10. -- Benefiaciary Details ...
var table = document.getElementById("benefiaciary");
var tbodyRowCount = $("#benefiaciary > tbody > tr").length;
if(tbodyRowCount > 0)
{
var tableDependent = table.tBodies[0];
for (let i = 0; i < tbodyRowCount; i++) {
	//alert(tableDependent.rows[i].cells[0].innerHTML);
    formData.append('Ext_member_name'+row,tableDependent.rows[i].cells[0].innerHTML);
    formData.append('middlename'+row,tableDependent.rows[i].cells[1].innerHTML);
    formData.append('Ext_member_surname'+row,tableDependent.rows[i].cells[2].innerHTML);
	formData.append('Ext_member_id'+row,tableDependent.rows[i].cells[3].innerHTML);
    formData.append('Relationship'+row,tableDependent.rows[i].cells[4].innerHTML);
    formData.append('Percentage'+row,tableDependent.rows[i].cells[5].innerHTML);
    formData.append('Remarks'+row,tableDependent.rows[i].cells[6].innerHTML);	
    formData.append('Active'+row,'0');
    formData.append('Ext_memberType_id'+row,'2');//Benefiaciary--
    formData.append('Life_state_id'+row,'1');
    formData.append('Main_member_id'+row,document.getElementById('im_member_id').value);
	row = row + 1;
}
}
// -- Total Rows of Extended_member table..
 formData.append('rows',row);
 formData.append('Client_ID_Type',document.getElementById('Client_ID_Type').value);
 //alert(document.getElementById('Notify').value);
 formData.append('Notify',document.getElementById('Notify').value);
 var c= document.getElementById("Methodofcommunication_name").selectedIndex;
 var d = document.getElementById("Methodofcommunication_name").options;
 formData.append('Methodofcommunication_name',d[c].text);
//alert(document.getElementById('paymentmethod_name').innerHTML);															  
//alert(document.getElementById('paymentmethod').value);
 var a = document.getElementById("paymentmethod_name").selectedIndex;
 var b = document.getElementById("paymentmethod_name").options;
  //alert("Index: " + y[x].index + " is " + y[x].text);
 formData.append('paymentmethod',document.getElementById('paymentmethod_name').value);
 formData.append('paymentmethodname',b[a].text);											
 formData.append('Account_holder',document.getElementById('Account_holder').value);
 //Main member nto resposible for account paymenys -2023/07/09
 formData.append('debtor_identification',document.getElementById('debtor_identification').value);
 formData.append('debtor_id_type',document.getElementById('debtor_id_type').value);
 formData.append('debtor_contact_number',document.getElementById('debtor_contact_number').value);
 formData.append('debtor_email',document.getElementById('debtor_email').value);
 
// 6. -- Dependent Details ...
var list =
{
'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode,
'policy_number_existing': policy_number_existing};

// 7 -- member policy

//var formdata = new FormData(this);
jfinApplication = jQuery.noConflict( true );
// -- Check Duplicate Payment to benefiaciary.
		jfinApplication(document).ready(function()
		{
			
		jfinApplication.ajax({  type: "POST",
					   url:"script_create_ffms_application.php",
				       data:/*{member:member,spouse:spouse,
							 address:address,source_of_income:source_of_income,
							 banking_details:banking_details,list:list,member_policy:member_policy
							},*/
							formData,
							 //  dataType:'json', // -- 1.Comment out for debugging
							contentType:false,
							cache:false,
							 processData:false,
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

//PayNow = document.getElementById("PayNow");
//Exit = document.getElementById("PayNow");
backtoconfirm = document.getElementById("backtoconfirm");
//addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								var res = feedback.split("|");
								//alert(feedback);
								var update = <?php echo "'".$backHTML."'"; ?>;
								if(update == 'display:none'){message_info.innerHTML = 'Membership created successfully for '+document.getElementById('im_member_id').value+'.';}
								else{message_info.innerHTML = 'Membership updated successfully for '+document.getElementById('im_member_id').value+'.';}	
								message_info.className  = 'status';
								//PayNow.style.visibility = '';
								//Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								//addbeneficiary.style.visibility = '';
								if(typeof res[2] === 'undefined')
								{}else{message_info.innerHTML = message_info.innerHTML + '<br/> Member policy number is:'+(String(res[2]).padStart(4, '0'));}
								document.getElementById('bid').value = (String(res[1]).padStart(4, '0'));
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								//alert(feedback); --debugging
								//PayNow.style.visibility = 'hidden';
								//Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								//addbeneficiary.style.visibility = 'hidden';
							}
 
 
						 }
					});
				});

}

//Dependants
const $tableID = $('#dependent1'); 
const $tableID2 = $('#extendend1'); 
const $tableID3 = $('#benefiaciary1'); 


const $BTN = $('#export-btn'); 
const $EXPORT = $('#export');
const newTr =  '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';
const newTr3 = '<tr class="hide"><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td class="pt-3-half" contenteditable="true"></td><td><span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light">Remove</button></span></td></tr>';

  $('.table-add').on('click', 'i', () => {
	const $clone = $tableID.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
		$tableID.find('#dependent').append($clone);
});
$('.table2-add').on('click', 'i', () => {

	const $clone = $tableID2.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID2.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
	$tableID2.find('#extendend').append($clone);
});
$('.table3-add').on('click', 'i', () => {
	const $clone1 = $tableID3.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID3.find('tbody tr ').length ===0) {
	$('tbody').append(newTr);
	}
	$tableID3.find('#benefiaciary').append($clone1);
});

$tableID.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID2.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID3.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID2.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID3.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    }); 
$tableID2.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });
$tableID3.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });  
// A few jQuery helpers for exporting only jQuery.fn.pop= [].pop;
jQuery.fn.shift = [].shift;
$BTN.on('click', () => {
    const $rows =$tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = []; // Get the headers(add special header logic here)
	 $($rows.shift()).find('th:not(:empty)').each(function() {
        headers.push($(this).text().toLowerCase());
    }); // Turn all existing rows into a loopable array
	 $rows.each(function() {
        const $td = $(this).find('td');
        const h = {}; // Use the headers from earlier to name our hash keys
		 headers.forEach((header, i) => {
            h[header] = $td.eq(i).text();
        });
        data.push(h);
    }); // Output the result
    $EXPORT.text(JSON.stringify(data));
});

</script>
</div>