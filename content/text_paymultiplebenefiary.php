<?php
require 'database.php';

if(!function_exists('get_beneficiaries'))
{
	function get_beneficiaries($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'benefiary';			

			$sql = "select * from $tbl_2 where IntUserCode = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['IntUserCode']));
			$data = $q->fetchAll(PDO::FETCH_ASSOC);			  
			
		return $data;
	}
}
$dataAll = null;
$customerid  = '';
$IntUserCode = '';

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

if(!empty($_POST))
{
	$customerid  = getpost('customerid');
	$IntUserCode = getsession('IntUserCode');
}
else
{
	$dataFilter['IntUserCode'] = $IntUserCode;
	$dataAll = get_beneficiaries($dataFilter);
}	

?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
</style>

<div class="container background-white bottom-border">
     <br />	
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
    </div>
</div>
<div class="row setup-content" id="step-1">		
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

  <div class="clearfix"></div>
<form class='login-page' style="padding: 10px;" method='post' onsubmit="return fiterByDate();">
  <div class='row'>
   <div class="col-md-3">	
    <div class="control-group>"> 
	    <div class="form-group mx-sm-3 mb-2">
            <input class="form-control" style='z-index:0' id='customerid' name='customerid' placeholder='Reference' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>
	</div>	
   </div>
   <div class="col-md-3">	
    <div class="control-group>"> 
	    <div class="form-group mx-sm-3 mb-2">
          <button type="submit" class="btn btn-primary mb-2">Search</button>
		</div>
	</div>	
   </div>   
  </div>	
</form>
	  
<div class="table-responsive">
<table id="benefiaries" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
		      <th></th>
              <th>#</th>
              <th>Benefiary</th>
              <th>Last transaction</th>
              <th>Client_Reference</th>
              <th>My Reference</th>
              <th>Amount</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="6" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>
      </tfoot>
      <?php

        foreach($dataAll as $row)
        {
			$id = $row["benefiaryid"];
			$benefiary = $id.' - '.$row["Account_Holder"].' - '.$row["Account_Number"];
			$idckh = 'chk'.$id;
			$idamnt = 'amount'.$id;
			$idclient_reference = 'client_reference'.$id ;
			$idbank_reference = 'bank_reference'.$id;
			
		 echo '
          <tr>
            <td width="10%"><input id='.$idckh.' name='.$idckh.' type="checkbox" /></td>		  
            <td width="10%">'.$id.'</td>
            <td width="10%">'.$benefiary.'</td>
            <td width="10%">'.$row["lasttransaction"].'</td>			
            <td width="10%"><input id = '.$idclient_reference.' name = '.$idclient_reference.' type="text"  /></td>
            <td width="10%"><input id = '.$idbank_reference.' name = '.$idbank_reference.' type="text"  /></td>
            <td width="10%"><input step="0.01" id = '.$idamnt.' name = '.$idamnt.' type="number"  /></td>
          </tr>
          ';
        }
      ?>
  </table>
    </div>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>		   	
</div>
<div class="row setup-content" id="step-2">
	<div class="col-xs-12">
		<div class="col-md-12">
			<h3> Step 2</h3>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>		   
		</div>
	</div>
</div>
<div class="row setup-content" id="step-3">
	<div class="col-xs-12">
		<div class="col-md-12">
			<h3> Step 3</h3>
			<button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
		</div>
	</div>
</div>
</div>
<script>
/* BOC */
// -- 
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
			curInputsNum = curStep.find("input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;
//alert(curInputs.length);
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
		//alert('curInputsNum :'+curInputsNum.length);

		// -- Required number.
		for(var i=0; i<curInputsNum.length; i++){
            if (!curInputsNum[i].validity.valid){
                isValid = false;
                $(curInputsNum[i]).closest(".form-group").addClass("has-error");
            }
        }
		//alert('curInputsDate = '+curInputsDate.length);

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
            if (!curInputsDate[i].validity.valid){
                isValid = false;
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }
		//alert('Test1 - '+isValid);
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
	
	// -- Next Step Validated
	navListItems.click( function(e) 
	{	
	/*$(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
    */
		//e.preventDefault(); /*your_code_here;*/ 
  var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

		$(".form-group").removeClass("has-error");
		//alert(curStepBtn);
        for(var i=0; i<curInputs.length; i++){
			
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
		
		                isValid = false;

		//alert('Test2 - '+nextStepWizard.attr("id"));
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
		
		return false; 
	} );
});

/* EOC */
/* Table of Benefiaries */
$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[7]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#benefiaries').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,

buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 6)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                //return intVal(a) + intVal(b);
								var start = b.indexOf("<");
				var end = b.indexOf(">") + 1;
				b = b.substring(end);
				//alert(b);
//                return intVal(a) + intVal(b);
				var start = b.indexOf("<");
				var end = b.indexOf(">") + 1;
				b = b.substring(end);
				//alert(b);
                return intVal(a) + intVal(b);


            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
//                return intVal(a) + intVal(b);
				var start = b.indexOf("<");
				var end = b.indexOf(">") + 1;
				b = b.substring(end);
				//alert(b);
                return intVal(a) + intVal(b);

            }, 0 );

            // Update footer
      $( api.column( 6 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});

table.buttons().container().appendTo('#benefiaries_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});
</script>