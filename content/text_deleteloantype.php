<?php 
	require 'database.php';
	
	$loantypeid = null;
	
	if ( !empty($_GET['loantypeid'])) 
	{
		$loantypeid = $_REQUEST['loantypeid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$loantypedi = $_POST['loantypeid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "SET FOREIGN_KEY_CHECKS=0;DELETE FROM loantype WHERE loantypeid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($loantypedi));
			Database::disconnect();
			echo("<script>location.href='loantype.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a Loan Type
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="loantypeid" value="<?php echo $loantypeid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="loantype.php">No</a>
						</div>
					</form>
				</div>
	</div>			
</div> <!-- /container -->
