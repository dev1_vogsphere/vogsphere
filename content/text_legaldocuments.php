<!-- Loan Calculator --->
<div class="container background-white bottom-border">
<div class="margin-vert-30">
<div class="headline">
  <h1 align=center>Privacy Policy</h1>
</div>
  <h5 align=center>Updated 23 October 2021</h5>
<hr>
            <!-- === BEGIN CONTENT ===
                <div class="container background-white">-->
                    <div class="row margin-vert-40">
                        <!-- Begin Sidebar Menu -->
                      <!--  <div class="col-md-3">
                            <ul class="list-group sidebar-nav" id="sidebar-nav">
                                <!-- Typography
                                <li class="list-group-item list-toggle">
                                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-typography">General Terms And Conditions</a>
                                    <ul id="collapse-typography" class="collapse">
                                        <li>
                                            <a href="features-typo-basic.html">
                                                <i class="fa fa-sort-alpha-asc"></i>Basic Typography</a>
                                        </li>
                                        <li>
                                            <a href="features-typo-blockquotes.html">
                                                <i class="fa fa-magic"></i>Blockquotes</a>
                                        </li>
                                    </ul>
                                </li> -->
                                <!-- End Typography -->
                                <!-- Components
                                <li class="list-group-item list-toggle">
                                    <a class="accordion-toggle" href="#collapse-components" data-toggle="collapse" aria-expanded="true">Privacy Policy</a>
                                    <ul id="collapse-components" class="collapse show in" style="" aria-expanded="true">
                                        <li>
                                            <!-- <span class="badge">New</span>
                                        <a href="#section1"><i class="fa fa-tags"></i>1.Personal Information We May Collect From You</a>
                                        </li>
                                        <li>
                                            <a href="#section2">
                                                <i class="fa fa-align-left"></i>2.IP Address</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-columns"></i>3.Where We Store Your Personal Data</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-arrows-h"></i>4.Uses Made of the Information</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-arrows-h"></i>5.	DISCLOSURE OF YOUR INFORMATION</a>
                                        </li>
                                      <li>
                                            <a href="#">
                                                <i class="fa fa-arrows-h"></i>6.	YOUR RIGHTS</a>
                                        </li>
                                      <li>
                                            <a href="#">
                                                <i class="fa fa-arrows-h"></i>7.	ACCESS TO INFORMATION</a>
                                        </li>
                                      <li>
                                            <a href="#">
                                                <i class="fa fa-arrows-h"></i>8.	CHANGES TO OUR PRIVACY POLICY</a>
                                        </li>

                                    </ul>
                                </li>
                                <!-- End Components -->
                                <!-- Icons
                                <li class="list-group-item list-toggle">
                                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-icons">Icons</a>
                                    <ul id="collapse-icons" class="collapse">
                                        <li>
                                            <span class="badge badge-u">New</span>
                                            <a href="features-icons.html">
                                                <i class="fa fa-chevron-circle-right"></i>Icon Styling</a>
                                        </li>
                                        <li>
                                            <a href="features-icons-font-awesome.html">
                                                <i class="fa fa-chevron-circle-right"></i>Font Awesome Icons</a>
                                        </li>
                                        <li>
                                            <a href="features-icons-social.html">
                                                <i class="fa fa-chevron-circle-right"></i>Social Icons</a>
                                        </li>
                                        <li>
                                            <a href="features-icons-glyphicons.html">
                                                <i class="fa fa-chevron-circle-right"></i>Glyphicons</a>
                                        </li>
                                    </ul>
                                </li> -->
                                <!-- End Icons -->
                                <!-- Testimonials
                                <li class="list-group-item">
                                    <a href="features-testimonials.html">Testimonials</a>
                                </li>
                                <!-- End Testimonials -->
                                <!-- Accordion and Tabs
                                <li class="list-group-item">
                                    <a href="features-accordions-tabs.html">Accordions & Tabs</a>
                                </li>
                                <!-- End Accordion and Tabs
                                <!-- Buttons
                                <li class="list-group-item">
                                    <a href="features-buttons.html">Buttons</a>
                                </li>
                                <!-- End Buttons
                                <!-- Carousels
                                <li class="list-group-item">
                                    <a href="features-carousels.html">Carousels</a>
                                </li>
                                <!-- End Accordion and Tabs
                                <!-- Animate On Scroll
                                <li class="list-group-item">
                                    <a href="features-animate-on-scroll.html">Animate On Scroll</a>
                                </li>
                                <!-- End Animate On Scroll
                                <!-- Grid System -
                                <li class="list-group-item">
                                    <a href="features-grid.html">Grid System</a>
                                </li>
                                <!-- End Grid System
                            </ul>
                        </div>-->
                        <!-- End Sidebar Menu -->
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-9">
                          <!--  <h2>Privacy Policy</h2>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="p1">
                                      eCashMeUp understands that information is our blood, and we should protect it. Your privacy continues to be a top priority for us, which is why this privacy policy has been created in order to demonstrate our firm commitment to your privacy. <br/><br/>
                                      This policy (together with our terms of service) sets out the basis on which any personal data eCashMeUp collects from you, or that you provide to us, “Personal Information” shall be defined as detailed in the Protection of Personal Information ACT 4 of 2013 (“POPIA”).
                                    </p>
                                    <br/>
                                    <div class="headline">
                                        <h2 id="section1">1.	Personal Information We May Collect From You</h2>
                                    </div>
									                      <p class="p1">We may collect and process the following data about you:</p>
								                        <ul class="p1">
                                        <li>Information that you provide by filling in forms on our website. This includes information provided at the time of registering to use our website, subscribing to our services, requesting, applying for or entering further services (including, but not limited to, a competition or promotion), making an enquiry or reporting a problem. This may include, but is not limited to, information such as your personal information such as race, gender, nationality marital status etc., contact details such as your name, email address, postal or physical address and banking details.</li>
										                    <li>In order to improve our communications with you as well as improve our products and services, we may ask you to provide us with additional information about you such as your professional interests and your experiences with us as well as more detailed contact preferences. When you provide us with any personal information you consent to us collecting and using that information according to your preferences.</li>
                                        <li>Account login credentials, such as usernames and passwords, password hints and similar security information.</li>
                                        <li>If you contact us, we may keep a record of that correspondence.</li>
                                        <li>We may ask you to complete surveys that we use for research purposes, although you are not obligated to respond to them.</li>
										                    <li>Details of transactions you carry out through our website and of the fulfilment of your service.</li>
                                        <li>Interests and communication preferences, including preferred language.</li>
                                        <li>Details of your visits to our website and the resources that you access and/or download.</li>
                                    </ul>
                                </div>
                            </div>
                            <br/>

						<!--	<h2 id="section2">2.	IP Address</h2>-->

                <div class="headline">
  							<h2>2.	IP Address</h2>
                </div>
                <ul class="p1">
    							<li>The Service may automatically collect information about how you and your device interact with the Service, including: Computer, device and connection information, such as IP address, browser type and version, operating system and other software installed on your device, mobile platform and unique device identifier and other technical identifiers, error reports and performance data;</li>
                  <li>Usage data, such as the features you used, the settings you selected, your URL click stream data, including date and time stamp and referring and exit pages, search terms you used, and pages you visited or searched for on the Service;</p>
                  <li>For educational Services, the course modules and test questions you view, answer or complete; and/or.</li>
                  <li>For educational Services, the course modules and test questions you view, answer or complete; and/or.</li>
                  <li>For location-aware Services, the region, city or town where your device is located in order to provide you with more relevant content for where you are in the world.</li>
                  <li>We collect this data through our servers and the use of cookies and other technologies. Cookies are small text files that can be read by a web server in the domain that put the cookie on your hard drive. We may use cookies and other technologies to store your preferences and settings, help you with signing in, provide targeted ads and analyse site operations. You can control cookies through your browser’s settings and other tools. However, if you block certain cookies, you may not be able to register, login, or access certain parts or make full use of the Service. For more details, see the cookie policy of the Service.</li>
                </ul>
              <br/>

              <div class="headline">
							<h2>3.	Where We Store Your Personal Data</h2>
              </div>
							<ul class="p1">
								<li>The data that we collect from you may not be transferred to, and stored at a destination outside the Country without a written consent from you. It may be processed by our affiliates partners operating in South Africa. The affiliates partners may be engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing.</li>
								<li>We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</li>
                <li>We implement technical and organisational measures to seek to ensure a level of security appropriate to the risk to the personal information we process. These measures are aimed at ensuring the integrity, confidentiality, and availability of personal information.</li>
                <li>All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted.</li>
								<li>Where we have given you (or where you have chosen) a password which enables you to access certain parts of our website, you are responsible for keeping this password confidential. It remains your responsibility that you do not to share a password with anyone.</li>
								<!-- <li></li><br/> -->
							</ul>
              <br/>
              <div class="headline">
							<h2>4.	Uses Made of the Information</h2>
              </div>
              <p class="p1">We use information held about you in the following ways:</p>
							<ul class="p1">
								<li>To ensure that content from our website is presented in the most effective manner for you and for your computer. </li>
								<li>To provide you with information, products or services that you request from us or which we feel may interest you, where you have consented to be contacted for such purposes. </li>
								<li>To share with other companies within our group (as defined in section 5 below) and reputable third party organisations for marketing purposes provided we have your consent to do so.</li>
								<li>To notify you about changes or updates to our service. </li>
                <li>Respond to your requests, inquiries, comments and concerns.</li>
                <li>To carry out our obligations arising from any contracts entered into between you and us. </li>
								<li>To allow you to participate in interactive features of our service, when you choose to do so. </li>
								<li>If you are an existing customer, we will only contact you by electronic means (e-mail or SMS) with information about goods and services similar to those which were the subject of a previous sale to you or which you have shown an interest in. </li>
								<li>If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. </li>
                <li>Notify you about changes, updates and other announcements related to the Service and our other products and services.</li>
                <li>If you do not want us to use your data in this way, or to pass your details on to third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data. </li>
							</ul>
              <br/>
              <div class="headline">
							<h2>5.	Disclosure of your Information</h2>
              </div>
							<ul class="p1">
								<li>We may disclose your personal information to any of our subsidiaries, our parent company and any of our parent company’s other subsidiaries (“our group”).
								<li>We may disclose your personal information to third parties:
								<li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.
								<li>If we or substantially all of our assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.
								<li>If said third party provide services for us, limited to the following purposes:
								<ul class="p1">
									<li>Understanding the use of our website and making improvements</li>
									<li>Administering a mailing list for customers that have registered for such list</li>
									<li>Responding to requests from or providing any necessary notices to you</li>
									<li>Protecting the security or integrity of our website</li>
									<li>Otherwise administering or managing our website software. </li>
								</ul>
								</li>
								<li>If we are under a duty to disclose or share your personal data in order to comply with any applicable law, regulation, legal processes, legal obligation, or in order to enforce or apply our terms of use and other agreements; or to protect the rights, property or safety of us, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction. </li>
							</ul>
              <br/>
              <div class="headline">
							<h2>6.	Your Rights</h2>
              </div>
							<ul class="p1">
								<li>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at info@ecashmeup.com </li><br/>
								<li>Our website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Their inclusion cannot be taken to imply any endorsement or validation by us of the content of said website. </li>
							</ul>
              <br/>
              <div class="headline">
							<h2>7.	Access to Information</h2>
              </div>
							<ul class="p1">
								<li>The Data Protection Act 1998 (the “Act”) gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee (prescribed in PAIA) on request to meet our costs in providing you with details of the information we hold about you. </li>
							</ul>
              <br/>
              <div class="headline">
							<h2>8.	Changes to Our Privacy Policy</h2>
              </div>
							<ul class="p1">
								<li>We will update this privacy policy from time to time. Any changes will be posted on this page with an updated revision date. If we make any material changes, we will provide notice through the Service or by other means.</li><br/>
							</ul>
              <hr class="short">
              </div>
              </div>
             <!--   </div>
             === END CONTENT === -->
<hr>
</div>
</div>
