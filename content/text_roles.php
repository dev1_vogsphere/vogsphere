<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 $tbl_name="role"; // Table name 
 $role = '';
  
 // -- Get roles - 23/01/2022
if(!function_exists('getroles'))
{
  function getroles($pdo)
  {
	$sql = "select * from role";
	$dataclient = $pdo->query($sql);

	$rolesArr = array();
	foreach($dataclient as $row)
	{
		$rolesArr[] = $row['role'];
	}
	return $rolesArr;
}}
  
  
// -- Roles...
// BOC -- Get sub branches 17/01/2022
$rolesArr = array();
$pdo = Database::connect();
$rolesArr = getroles($pdo);
 
if ( !empty($_POST)) 
{
    $role = $_POST['role'];
}
 
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Roles</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
											</span>
                                    <SELECT style='width:150px;height:30px;z-index:0;' class='form-control' id='role' name='role' size='1' style='z-index:0'>";
			    
					$roleSelect = $role;
					foreach($rolesArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $roleSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				
			echo "</SELECT>";
									echo "</div>                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $role = $_POST['role'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($role))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE role = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($role));	
						$data = $q->fetchAll();  
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newrole.php>Add New Role</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Role</b></td>"; 
						echo "<th>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['role']) . "</td>";  
						
						// -- Admin,superuser,customer must never be deleted.
							if(nl2br( $row['role']) == 'admin')
							{
							}
							elseif($row['role'] == 'superuser')
							{}
							elseif($row['role'] == 'customer')
							{}
							else
							{
								echo "<td><a class='btn btn-danger' href=deleterole.php?role={$row['role']}>Delete</a></td> "; 
							}
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div></div>
   		<!---------------------------------------------- End Data ----------------------------------------------->