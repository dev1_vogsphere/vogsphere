<?php
// -- Constant Values
$text_delete = "<span class='glyphicon glyphicon-trash'></span>";
$text_edit   = "<span class='glyphicon glyphicon-pencil'></span>";
$text_save   = "<span class='glyphicon glyphicon-floppy-disk'></span>";
$text_cancel = "<span class='glyphicon glyphicon-ban-circle'></span>";

if(!function_exists('get_beneficiaries'))
{
	function get_beneficiaries($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'benefiary';			
			
			if($dataFilter['role'] == 'admin')
			{
				/* All Beneficiaries */
				$sql = "SELECT * from $tbl_2";				
				$q = $pdo->prepare($sql);
				$q->execute();								
			}
			else
			{
				/* All Beneficiaries For Internal User Code */
				$sql = "SELECT * from $tbl_2 WHERE IntUserCode = ?";				
				$q = $pdo->prepare($sql);
				$q->execute(array($dataFilter['IntUserCode']));				
			}
			$data = $q->fetchAll(PDO::FETCH_ASSOC);		
			return $data;
	}
}
////////////////////////////////////////////////////////////////////////

if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
////////////////////////////////////////////////////////////////////////
// 							Declarations
////////////////////////////////////////////////////////////////////////
$benfiaryid			   = '';
$changedby 			   = getsession('username');
$changedon 			   = date('Y-m-d');
$changedat	           = date('h:m:s');

$collectionid 	= '';
$FromDate		= '';
$ToDate			='';
$sessionrole	='';

$Allstatus 		= '';
$status    		= '';

$AllServiceMode = '';
$ServiceMode    = '';

$AllServiceType = '';
$ServiceType    = '';
$message 		= '';
$userid 		= '';
$UserCode		= '';
$IntUserCode 	= '';
$role			= '';
////////////////////////////////////////////////////////////////////////
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
////////////////////////////////////////////////////////////////////////
// 			Lookups	
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}
$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}
$NotifyArr[] = 'None';					 
$NotifyArr[] = 'SMS';
$NotifyArr[] = 'EMAIL';
////////////////////////////////////////////////////////////////////////
// -- Sessions
// -- Logged User Details.
if(getsession('username')){$userid = getsession('username');}
// -- User Code.
if(getsession('UserCode')){$UserCode = getsession('UserCode');}
// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');}

// -- Role.
if(getsession('role')){$role = getsession('role');}
////////////////////////////////////////////////////////////////////////
// -- Parameters
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	
	if(empty($IntUserCode))
	{
		$IntUserCode = $params[0];
	}
	
	if(empty($UserCode))
	{
		$UserCode = $params[1];
	}
	
	if(empty($userid))
	{
		$userid = $params[2];
	}
	if(empty($role))
	{
		$role = $params[3];
	}	
	
}

////////////////////////////////////////////////////////////////////////
//$list = getpost('list');
$dataFilter['IntUserCode'] 	= $IntUserCode;
$dataFilter['UserCode']		= $UserCode;
$dataFilter['userid'] 		= $userid;
$dataFilter['role'] 		= $role;
////////////////////////////////////////////////////////////////////////
$results 			= get_beneficiaries($dataFilter);	

?>
<style>
#wrapper 
		{
			width: 50%;
			position: fixed;
			z-Index:1;
			top:43%;
		}
</style>
		
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">
  <div class='row margin-vert-30'>
	  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<h2 align="center">Edit Beneficiaries</h2>  
	  </div>
  </div>
<div class="table-responsive">
<div class="messages" id="message" name="message">
  		 <?php
		 if (isset($message)) 	
		 {
			 if($message == 'updated successfully')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);	
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);	
			 }		
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}					
			}				
		 }
		 ?>
  </div>  
  
  <div id="wrapper" name="">
				<!--<div  id='saved' name='saved' class='alert alert-success fade in'>
				<a href='#' class='close' data-dismiss='alert'>×</a>report here</div>-->			
  </div> 
			
<table id="editbeneficiary1" class="table table-striped table-bordered" style="font-size: 10px">

      <thead>
          <tr>
              <th>Initials</th>
              <th>Account Holder</th>
              <th>Account Number</th>
              <th>Branch Code</th>
			  <th>Account Type</th>
			  <th>My Reference</th>
			  <th>Beneficiary Reference</th>
			  <th>Payment Notice by</th>
			  <th>Contact Number</th>	
			  <th>Contact Email</th>			  
			  <th>Action</th>
          </tr>
      </thead>
    <tfoot>
		 <tr>
              <th>Initials</th>
              <th>Account Holder</th>
              <th>Account Number</th>
              <th>Branch Code</th>
			  <th>Account Type</th>
			  <th>My Reference</th>
			  <th>Beneficiary Reference</th>
			  <th>Payment Notice by</th>
			  <th>Contact Number</th>	
			  <th>Contact Email</th>			  
			  <th>Action</th>
          </tr>
      </tfoot>
      <?php
if(!empty($results)){
        foreach($results as $row )
		{			
			$id ="{$row["benefiaryid"]}";
			$idInitials 		= "Initials$id";			
			$idAccount_Holder 	= "Account_Holder$id";
			$idAccount_Number 	= "Account_Number$id";
			$idBranch_Code 		= "Branch_Code$id";
			$idAccount_Type		= "Account_Type$id";
			$idClient_Reference_1   	= "Client_Reference_1$id";
			$idBank_Reference   		= "Bank_Reference$id";
			$idNotify   				= "Notify$id";
			$idbenefiarycontactnumber   = "benefiarycontactnumber$id";
			$idbenefiaryemail   		= "benefiaryemail$id";
			
			$idAmount = "Amount$collectionid";
			$idAction_Date = "Action_Date$collectionid";
			$idNo_Payments  = "No_Payments$id";
						
			$idAction_Date = "Action_Date$id";

			$idStatus_Code  = "Status_Code$id";
			$idStatus_Description  = "Status_Description$id";


		  echo '<tr id = "r'.$id.'" name = "r'.$id.'" >';
          echo 
		  '
            <td width="3%"><input style="font-size: 10px;width:70%" maxlength="3" type ="text" id = "'.$idInitials.'" value="'.$row["Initials"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Holder.'" value="'.$row["Account_Holder"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Number.'" value="'.$row["Account_Number"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idBranch_Code.'" value="'.$row["Branch_Code"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idAccount_Type.'" value="'.$row["Account_Type"].'" readonly /></td>
			
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idClient_Reference_1.'" value="'.$row["Client_Reference_1"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="text" id = "'.$idBank_Reference.'" value="'.$row["Bank_Reference"].'" readonly /></td>
            <td width="3%"><input style="font-size: 10px;width:70%" type ="text" id = "'.$idNotify.'" value="'.$row["Notify"].'" readonly /></td>
            <td width="60%"><input style="font-size: 10px;width:100%" type ="number" id = "'.$idbenefiarycontactnumber.'" value="'.$row["benefiarycontactnumber"].'" readonly /></td>
            <td width="10%"><input style="font-size: 10px;width:100%" type ="email" id = "'.$idbenefiaryemail.'" value="'.$row["benefiaryemail"].'" readonly /></td>
			';
			// -- Actions
			echo '<td><a class="btn btn-success" id = "'.$id.'" onclick="toggleUpdate(this);" style="display:inline">'.$text_edit.'</a>';
			echo '<br/>';
			echo '<br/>';
			echo '<a class="btn btn-danger" id = "c'.$id.'" name = "c'.$id.'" onclick="cancelUpdate(this);" style="display:none" >'.$text_cancel.'</a>';
			echo '<br/>';
			echo '<a   class="btn btn-danger" id = "Stop'.$id.'" name = "Stop'.$id.'" onclick="StopUpdate(this);" style="display:inline" >'.$text_delete.'</td>';
			echo '</td></tr>';
        }
}	
      ?>


  </table>
    </div>
    </br>
</div>

<script>


/*$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[5]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
        //alert(Action_Date);
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);
*/
$(document).ready(function(){
var table = $('#editbeneficiary1').DataTable({
lengthChange: true,
autoFill:true,
//buttons: ['copy', 'excel','pdf','csv', 'print'],
"lengthMenu": [ [10, 25, 50, 100,-1], [10, 25, 50,100, "All"] ]
});

table.buttons().container().appendTo('#editbeneficiary1_wrapper .col-sm-6:eq(0)')
// -- Event listner
$('#min,#max').change( function() {
  table.draw();
  });
});

// -- #myInput is a <input type="text"> element
function toggleUpdate(ele) 
{
	bid   = "Branch_Code"+ele.id;
	acid  = "Account_Type"+ele.id;
	achid = "Account_Holder"+ele.id;
	acnid 	 = "Account_Number"+ele.id;
	
	myrefid = "Client_Reference_1"+ele.id;
	brefid 	= "Bank_Reference"+ele.id;
	notifyid = "Notify"+ele.id;
	cnumid = "benefiarycontactnumber"+ele.id;
	cemail = "benefiaryemail"+ele.id;
	Initialsid = "Initials"+ele.id;
	
	// -- Constant Values
	text_delete = '<span class="glyphicon glyphicon-trash"></span>';
	text_edit   = '<span class="glyphicon glyphicon-pencil"></span>';
	text_save   = '<span class="glyphicon glyphicon-floppy-disk"></span>';
	text_cancel = '<span class="glyphicon glyphicon-ban-circle"></span>';

	bvalue = '';
	var id = ele.id;
	
	if(ele.innerHTML == text_save || ele.innerHTML == text_cancel || ele.innerHTML == text_delete )
	{  
		isvalid = true;
		Notify = document.getElementById(notifyid);	
		benefiarycontactnumber = document.getElementById(cnumid);

		isvalid = validate_contactnumber2(benefiarycontactnumber,Notify.value);//validate_contactnumber2();

		// -- Validate Initials
		Initials = document.getElementById(Initialsid);
		if(!isvalid){ el = Initials; isvalid = validate_required(el);}
		// -- Acount holder
		Account_Holder = document.getElementById(achid);
		if(!isvalid){ el = Account_Holder; isvalid = validate_required(el);}

		// -- Account Number
		Account_Number = document.getElementById(acnid);
		if(!isvalid){ el = Account_Number; isvalid = validate_required(el);}

		// -- My Reference 
		Client_Reference_1 = document.getElementById(myrefid);
		if(!isvalid){ el = Client_Reference_1; isvalid = validate_required(el);}

		// -- Beneficiary Reference
		Bank_Reference = document.getElementById(brefid);
		if(!isvalid){ el = Bank_Reference; isvalid = validate_required(el);}

		if(!isvalid)
		{
		//alert(ele.innerHTML+" - "+text_save);
		branchcode = document.getElementById(bid);
		bvalue = branchcode.value;
		// -- Branch Code outerhtml
		branchcode.readOnly = true;
		branchcode.outerHTML = "<input style='font-size: 10px;width:100%' type ='text' id = '"+bid+"' value="+bvalue+" readOnly />";

		// -- Account Type
		Account_Type = document.getElementById(acid);
		acvalue = Account_Type.value;
		Account_Type.readOnly = true;
		Account_Type.outerHTML = "<input style='font-size: 10px;width:100%' type ='text' id = '"+acid+"' value="+acvalue+" readOnly />";
		if(ele.innerHTML == text_save || ele.innerHTML == text_delete)
		{updatecollection(id);} 
	
		// -- Account Holder
		ahvalue = Account_Holder.value;
		Account_Holder.readOnly = true;	
		
		// Account_Number
		Account_Number.readOnly = true;	
		
		// Client_Reference_1
		Client_Reference_1.readOnly = true;	
		
		// Bank_Reference
		Bank_Reference.readOnly = true;	
		
		// Notify
		nvalue = Notify.value;		
		Notify.outerHTML = "<input style='font-size: 10px;width:70%' type ='text' id = '"+notifyid+"' value="+nvalue+" readOnly />";		
		Notify.readOnly = true;	

		// benefiarycontactnumber
		benefiarycontactnumber.readOnly = true;	

		// benefiaryemail
		benefiaryemail = document.getElementById(cemail);
		benefiaryemail.readOnly = true;	
		
		// Initials
		Initials.readOnly = true;
		
		// -- none display of cancel.
		id_ = "c"+ele.id;
		document.getElementById(id_).style.display = 'none';

		// -- display of Stop.
		id_ = "Stop"+ele.id;
		document.getElementById(id_).style.display = 'inline';
		// -- stop payment was actioned remove the edit button & stop buttons
		if(ele.innerHTML == text_delete)
		{
			ele.style.display = 'none';
			document.getElementById(id_).style.display = 'none';
		}
		
		ele.innerHTML = text_edit;
		}
		else
		{ ele.innerHTML = text_save;}
	}
	else 
	{
		ele.innerHTML = text_save;
		// -- Branch code outerhtml
		branchcode = document.getElementById(bid);
		bvalue = branchcode.value;
		branchcode.readOnly = false;
		id_ = bid;
		
		Arr = <?php echo json_encode($Branch_CodeArr); ?>;
		new_html_el = "";
		new_html_el = "<select style='font-size: 10px;width:80px' class='form-control' id='"+id_+"' name='"+id_+"'>";
		var value = ""+document.getElementById(id_).value;
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			 
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
	   
		new_html_el = new_html_el+"</select>";
		branchcode.outerHTML = new_html_el;
		
		// -- Account Type
		Account_Type = document.getElementById(acid);				
		Account_Type.readOnly = false;
		acvalue = Account_Type.value;
		value = acvalue;
		Arr = <?php echo json_encode($Account_TypeArr); ?>;
		new_html_el = "";
		new_html_el = "<select style='font-size: 10px;width:80px' class='form-control' id='"+acid+"' name='"+acid+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split('|');
			 
		  if(value === row[0])	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row[0]+"' selected>"+row[1]+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row[0]+"'>"+row[1]+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		Account_Type.outerHTML = new_html_el;
		
		// -- Account Holder
		Account_Holder = document.getElementById(achid);
		ahvalue = Account_Holder.value;
		Account_Holder.readOnly = false;	

		// Account_Number
		Account_Number = document.getElementById(acnid);
		Account_Number.readOnly = false;	
		
		// Client_Reference_1
		Client_Reference_1 = document.getElementById(myrefid);
		Client_Reference_1.readOnly = false;	
		
		// Bank_Reference
		Bank_Reference = document.getElementById(brefid);	
		Bank_Reference.readOnly = false;	
		
		// -- Notify
		Notify = document.getElementById(notifyid);	
		Notify.readOnly = false;	
		nvalue = Notify.value;		
		value = nvalue;
		Arr = <?php echo json_encode($NotifyArr); ?>;
		new_html_el = "";
		new_html_el = "<select style='font-size: 10px;width:70px' class='form-control' id='"+notifyid+"' name='"+notifyid+"'>";
		for (var key in Arr)
		{
			row = new Array();
			row = Arr[key].split(',');
		  if(value == row)	// -- selected
		  {new_html_el = new_html_el+"<option value='"+row+"' selected>"+row+"</option>";}
		  else{new_html_el = new_html_el+"<option value='"+row+"'>"+row+"</option>";}
		}
		new_html_el = new_html_el+"</select>";
		Notify.outerHTML = new_html_el;

		// benefiarycontactnumber
		benefiarycontactnumber = document.getElementById(cnumid);
		benefiarycontactnumber.readOnly = false;	

		// benefiaryemail
		benefiaryemail = document.getElementById(cemail);
		benefiaryemail.readOnly = false;	
		
		// -- display of cancel.
		id_ = "c"+ele.id;
		//style="font-size: 10px;" 
		//document.getElementById(id_).style.fontSize  = '10px';
		document.getElementById(id_).style.display = 'inline';

		// Initials
		Initials = document.getElementById(Initialsid);
		Initials.readOnly = false;

		// -- none display of Stop.
		id_ = "Stop"+ele.id;
		document.getElementById(id_).style.display = 'none';
	}
} 


function updatecollection(id) 
{
	var userid = <?php echo "'".$userid."'"; ?>;
	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;
	var UserCode = <?php echo "'".$UserCode."'"; ?>;
	var collectionid 	 = id;
	var Account_Holder   = document.getElementById("Account_Holder"+id).value;
	var Account_Type	 = document.getElementById("Account_Type"+id).value;
	var Account_Number   = document.getElementById("Account_Number"+id).value;
	var Branch_Code		 = document.getElementById("Branch_Code"+id).value;	
	var Initials =  document.getElementById('Initials'+id).value;
	var Bank_Reference = document.getElementById('Bank_Reference'+id).value;
    var Notify = document.getElementById('Notify'+id).value;
    var benefiarycontactnumber = document.getElementById('benefiarycontactnumber'+id).value;
	var benefiaryemail = document.getElementById('benefiaryemail'+id).value;

var Client_Reference_1  = document.getElementById('Client_Reference_1'+id).value;

	var list =					
{'benefiaryid': id,
'Initials': Initials,
'Account_Holder': Account_Holder,
'Account_Type': Account_Type,
'Account_Number': Account_Number,
'Bank_Reference': Bank_Reference,
'Client_Reference_1': Client_Reference_1,
'Notify': Notify,
'benefiarycontactnumber': benefiarycontactnumber,
'benefiaryemail' : benefiaryemail,
'changedby' : userid,
'IntUserCode': IntUserCode,
'UserCode' : UserCode,'Branch_Code':Branch_Code};

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"script_updatebenefiary.php",
				       data:{list:list},
						success: function(data)
						 {	
							var className  = 'alert alert-success fade in';								  
							var text = data;
							add_reporthere(className,text);
						 }
					});
				});				  
			  return false;
}

function cancelUpdate(ele)
{
	var id = ele.id;//substr(ele.id,1);
	id = id.substr(1);
	
	// -- Edit.
	ele2 = document.getElementById(id);
	document.getElementById(id).innerHTML = text_cancel;
	
	toggleUpdate(ele2);
}

function StopUpdate(ele)
{
	var id = ele.id;//substr(ele.id,1);
	id = id.substr(4);
	var 	text_delete = '<span class="glyphicon glyphicon-trash"></span>';
var rowChanged  = document.getElementById("r"+id);		
var table = document.getElementById("editbeneficiary1");
	
	var list =					
	{'benefiaryid': id};

feedback  = 'Are you sure you want to remove this beneficiary?';
		if (confirm(feedback)) 
		{
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"script_deletebenefiary.php",
				       data:{list:list},
						success: function(data)
						 {	
							var className  = 'alert alert-success fade in';								  
							var text = data;
							add_reporthere(className,text);
							table.deleteRow(rowChanged.rowIndex);							
						 }
					});
				});	
		 }
			  return false;
}
function add_reporthere(className,text)
{
	// -- CREATE DIV	
	html = "<div id='saved' name='saved' class='"+className+"'>"
		  +"<a href='#' class='close' data-dismiss='alert'>×</a>"
		  +text
		  +"</div>";
	document.getElementById("wrapper").innerHTML = html;
}
function validate_contactnumber2(el,notify)
{
			//alert('Notify :'+document.getElementById('Notify').value);
		var req	= null;
		if(notify === 'SMS')
		{
			el.required = true;//setAttribute('required','required');
			el.focus();
			if(el.value === '')
			{req = true;}else{req = false;}
		}
		else
		{
			el.required = false;
			req = false;
		}
		return req;
}
function validate_required(el)
{
	var req	= null;

	if(el.value === '')
	{
		el.required = true;
		el.focus();
		req = true;
	}
	else
	{
		req = false;
	}
	return req;
}
</script>
