<?php 
	require 'database.php';
	
	$userid = null;
	$failedAttempt = null;
	
// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$UserIdDecoded = "";
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	
	if ( !empty($_GET['userid'])) 
	{
		$userid = $_REQUEST['userid'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$userid = base64_decode(urldecode($userid)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	
	if ( !empty($_GET['failedAttempt'])) 
	{
		$failedAttempt = $_REQUEST['failedAttempt'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$failedAttempt = base64_decode(urldecode($failedAttempt)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$userid = $_POST['userid'];
		$failedAttempt = $_POST['failedAttempt'];

		// Unlock & Lock account.
		if($failedAttempt > 3)
		{
			$failedAttempt = 0; // -- Unlock the Account.
		}
		else
		{
			$failedAttempt = 100; // -- Lock the Account.
		}
		
		echo "<h3>$failedAttempt - $userid</h3>";
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
		$sql = "UPDATE user SET failedAttempt = ? WHERE userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($failedAttempt,$userid));
		Database::disconnect();
		
		//echo("<script>location.href = 'users.php';</script>");
		header("Location: users");

	} 
	// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	else 
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "SELECT * FROM user WHERE userid = ? AND failedAttempt = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($userid,$failedAttempt));		
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(empty($data))
			{
				header("Location: index");
			}
	}
	// -- EOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Unlock/Lock User
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="userid" value="<?php echo $userid;?>"/>
					  <input type="hidden" name="failedAttempt" value="<?php echo $failedAttempt;?>"/>
					  
					  <p class="alert alert-error">Are you sure to Unlock/Lock User?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="users.php">No</a>
					  </div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
