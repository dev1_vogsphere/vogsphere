<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////  
 echo "<div class='container background-white bottom-border'>";		
   
   echo "<h2 align='center'>Database Backup of the following tables : </h2>";
?>  
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $provinceid = $_POST['provinceid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
				
						$sql = "SHOW TABLES";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						
				   
																	
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Table Name</b></td>"; 
						echo "<th>Backup</th>"; 
						echo "</tr>"; 

						  $count = count($data);
						foreach($data as $row)
						{ 
							foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
							echo "<tr>";  
							echo "<td valign='top'>".nl2br( $value ). "</td>";  
							echo "<td valign='top'><input type='checkbox' name='".$value."' checked></td>"; 
							echo "</tr>"; // nl2br( $value ) 
						} 
						echo "</table>"; 
						echo "<label>BackUp File Directory :</label><input type='text' value='C:\\DOWNLOAD\\' id='path' readonly/>";
						echo '<div style="text-align:center"><button align="right" type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="BackUpDatabase()">Run a Database Backup</button></div><br /><br />';
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
		
<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
    <div class="popup-inner">
        <h2>Database Backup - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
			
		</div>
		</br>
					<div id="divPaymentSchedules">
					
					</div>
	<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>				
</div>	
   		<!---------------------------------------------- End Data ----------------------------------------------->
<script>
function BackUpDatabase() 
{ 
	path = document.getElementById("path").value;
	
	jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"Rundatabasebackup.php",
					data:{path:path},
					success: function(data)
					{
					 var Message = "<p>Database BackedUp Succesfully - "+data+"!</p>";
					 jQuery("#divSuccess").html(Message);
					 		move();
				    }
				});
			});
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}	
</script>		