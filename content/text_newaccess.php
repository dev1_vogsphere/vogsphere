<?php
require 'database.php';
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_user="access"; // Table name

// -- 23/01/2022 Features
  $sql = 'SELECT * FROM features ORDER BY featureid';
  $datafeatures = $pdo->query($sql);

//1. ---------------------  Role ------------------- //
  $sql = 'SELECT * FROM role ORDER BY role';
  $dataRoles = $pdo->query($sql);

//2. ---------------------  Parent Pages ------------------- //
  $parentMark = 'X';
  $sql =  "select * from access where parent = 'X'";
  $dataParentPages = $pdo->query($sql);

//3. ---------------------  Child Pages ------------------- //
  $sql = 'SELECT * FROM page ORDER BY page';
  $dataChildPage = $pdo->query($sql);

//4. ---------------------  Menu Order Sequence ------------------- //
  $sql = 'SELECT * FROM ordersequence ORDER BY ordersequence';
  $dataordersequence = $pdo->query($sql);

  if (isset($_GET['accessid']) )
  {
	$accessid = $_GET['accessid'];
  }

// -- checkdefaultpage -- 30/01/2022
if(!function_exists('checkdefaultpagebyrole'))
{
function checkdefaultpagebyrole($pdo,$role)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$default  	  = 'X';

	$sql = 'SELECT * FROM access WHERE role = ? AND defaultpage = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($role,$default));
	$datadefaultpage    = $q->fetch(PDO::FETCH_ASSOC);
	$datadefaultpageArr = null;

	// -- array content..
	if (!isset($datadefaultpage['defaultpage']))
	{
		//-- Do nothing..
	}
	else
	{
	   // -- split features into array..
		$datadefaultpageArr['defaultpage'] = $datadefaultpage['defaultpage'];
	}
	return 	$datadefaultpageArr;
}}
// -- getfeaturesText
if(!function_exists('getfeaturesText'))
{
function getfeaturesText($pdo,$childpage)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$sql = 'SELECT * FROM page WHERE page = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($childpage));
	$dataPageFeatures = $q->fetch(PDO::FETCH_ASSOC);
	$checked = '';
	// --
	if (empty($dataPageFeatures))
	{
		$featuresText = "<td>No features</td>";
	}
	else
	{
	// -- split features into array..
		$dataPageFeaturesArr = explode(",", $dataPageFeatures['features']);
		$featuresText=$featuresText."<td>";
		foreach($dataPageFeaturesArr as $row)
		{
			$featuresText = $featuresText."<p><b>{$row}</b><input style='height:30px' type='checkbox' id='{$row}' name='{$row}' onClick='valuechecked(this);' value = 'X' {$checked} /></p>";
				//$html = $html."<p><b>{$row}</b></p>";
		}
			$featuresText=$featuresText."</td>";
	}
	return 	$featuresText;
}}
// -- getfeatures
if(!function_exists('getfeatures'))
{
function getfeatures($pdo,$childpage)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$sql = 'SELECT * FROM page WHERE page = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($childpage));
	$dataPageFeatures    = $q->fetch(PDO::FETCH_ASSOC);
	$dataPageFeaturesArr = null;

	// -- array content..
	if (empty($dataPageFeatures))
	{
		//-- Do nothing..
	}
	else
	{
	   // -- split features into array..
		$dataPageFeaturesArr = explode(",", $dataPageFeatures['features']);
		/*$featuresText=$featuresText."<td>";
		foreach($dataPageFeaturesArr as $row)
		{
			$featuresText = $featuresText."<p><b>{$row}</b><input style='height:30px' type='checkbox' id='{$row}' name='{$row}' /></p>";
				//$html = $html."<p><b>{$row}</b></p>";
		}
			$featuresText=$featuresText."</td>";*/
	}
	return 	$dataPageFeaturesArr;
}}
$count = 0;

$role		=	'';
$parent		=	'';
$parentpage	=	'';
$child    	=	'';
$childpage  =	'';
$label		=	'';

// -- Errors
$roleError		=	'';
$parentError		=	'';
$parentpageError	=	'';
$childError    	=	'';
$childpageError  =	'';
$labelError		=	'';

// -- CRUD ACCESS
$create  =	'';
$update  =	'';
$delete  =	'';
$default = '';

$ordersequence  =	0;

// -- Page content
$featuresText = '';
$temppage     = '';
$dataPageFeaturesArr = null;

if (!empty($_POST))
{   $count = 0;
	$labelError = '';
	$defaultError = '';
	$role		=	$_POST['role'];
	$parentpage	=	$_POST['parentpage'];
	$childpage  =	$_POST['childpage'];
	$label		=	$_POST['label'];
	$valid 		= true;
	$ordersequence  =	$_POST['ordersequence'];

	// -- featuresText 24/01/2022
	$featuresText = $_POST['featuresText'];

// -- Dynamic Variables 24/01/2022.
$length = 0;
if(isset($_POST['temppage']))
{
	$temppage = $_POST['temppage'];
}
//echo 'temppage :'.$temppage ;
//if($temppage != $childpage)
{
$dataPageFeaturesArr = getfeatures($pdo,$childpage);
$length 			 = count($dataPageFeaturesArr);
$temppage = $childpage;
}

if(!empty($dataPageFeaturesArr)){
foreach($dataPageFeaturesArr as $row)
{
	Global ${$row};
	//echo "modise : ".$row;
	if(isset($_POST["{$row}"]))
		{
			${$row} = $_POST["{$row}"];
			${$row} = 'X';
			//echo "modise : ".${$row};
		}
}}
	if(isset($_POST['parent']))
	{
		$parent = 'X';
		$parentpage = '';
	}
	else
	{
		$parent = '';
	}

	if(isset($_POST['child']))
	{
		$child = 'X';
		$parentpage	=	$_POST['parentpage'];
	}


	// CRUD ACCESS
	if(isset($_POST['create']))
	{
		$create = 'X';
	}
	else
	{
		$create = '';
	}

	if(isset($_POST['update']))
	{
		$update = 'X';
	}
	else
	{
		$update = '';
	}

	if(isset($_POST['delete']))
	{
		$delete = 'X';
	}
	else
	{
		$delete = '';
	}

	// -- 30/01/2022 - Default pages..
	if(isset($_POST['default']))
	{
		$default = 'X';
	}
	else
	{
		$default = '';
	}
	// -- Access Must be Indicate atleast parent or child.
	if(!isset($_POST['parent']) && !isset($_POST['child']))
	{
		$labelError = 'Please check either as parent or a child.<br/>'; $valid = false;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (empty($label)) { $labelError = $labelError.'Please enter menu label'; $valid = false;}

// -- Only one
	if (empty($label)) { $labelError = $labelError.'Please enter menu label'; $valid = false;}


	$objectArr = checkdefaultpagebyrole($pdo,$role);
	if(isset($objectArr['defaultpage'])) // -- New defaultpage...
	{
		if(!empty($default))
		{
		 $defaultError = 'Default page is already assign to a role.<br/>'; $valid = false;
		}else
		{
			$defaultError = '';
		}
	}else{$defaultError = '';}

	//echo $defaultError;
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								ACCESS  VALIDATION														  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($valid)
	{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO access(role, parent, parentpage,child,childpage,label,cancreate,canupdate,candelete,ordersequence,defaultpage) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

			$q = $pdo->prepare($sql);
			$q->execute(array($role,$parent,$parentpage,$child,$childpage,$label,$create,$update,$delete,$ordersequence,$default));
			//Database::disconnect();
			$count = $count + 1;
			$access_id = $pdo->lastInsertId();

			// -- Insert features 24/01/2022
			if(!empty($dataPageFeaturesArr)){
			foreach($dataPageFeaturesArr as $row)
			{
				Global ${$row};
				$sql = "INSERT INTO features(page, features, active,access_id) VALUES (?,?,?,?)";
				$q = $pdo->prepare($sql);
				if(isset($_POST["{$row}"])) // - is checked
					{
						${$row} = $_POST["{$row}"];
						${$row} = 'X';
						$q->execute(array($childpage,$row,'X',$access_id));
						//echo "modise inserted: ".${$row};
					}
					else // -- Not checked
					{
						$q->execute(array($childpage,$row,'',$access_id));
					}
					Database::disconnect();
			}}
	}

}
else
{
	// -- 24/01/2022
	$childpage = 'access';
	$featuresText = getfeaturesText($pdo,$childpage);
}
?>
<div class="container background-white bottom-border">
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Access Control Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message))
		{
			foreach ($message as $msg)
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Access Control successfully added.</p>");
		}
?>

</div>
			<p><b>Parent</b><br />
			<input style="height:30px" type='checkbox' name='parent' value="<?php echo !empty($parent)?$parent:'';?>"
						<?php if(!empty($parent)){echo'checked';}?> onclick="parentmenu()"/>
			</p>

			<p><b>Order Sequence</b><br />
				<SELECT class="form-control" id="ordersequence" name="ordersequence" size="1">
					<?php
						$ordersequenceLoc = '';
						$ordersequenceSelect = $ordersequence;
						foreach($dataordersequence as $row)
						{
							$ordersequenceLoc = $row['ordersequence'];

							if($ordersequenceLoc == $ordersequenceSelect)
							{
								echo "<OPTION value='".$ordersequenceLoc."' selected>$ordersequenceLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$ordersequenceLoc."'>$ordersequenceLoc</OPTION>";
							}
						}

						if(empty($dataordersequence))
						{
							echo "<OPTION value=0>No Order Sequence</OPTION>";
						}


					?>

					</SELECT>
			</p>

		 <div class="controls" id="parentmenu" name="parentmenu" <?php if(empty($parent)){echo "style='display:block'";}else{echo "style='display:none'";}?>>
			<p><b>Parent Menu Item</b><br />
					<SELECT class="form-control" id="parentpage" name="parentpage" size="1">
					<?php
						$parentpageLoc = '';
						$parentpageSelect = $parentpage;
						foreach($dataParentPages as $row)
						{
							$parentpageLoc = $row['label'];

							if($parentpageLoc == $childpageSelect)
							{
								echo "<OPTION value='".$parentpageLoc."'selected>$parentpageLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$parentpageLoc."'>$parentpageLoc</OPTION>";
							}
						}

						if(empty($dataChildPage))
						{
							echo "<OPTION value=0>No Pages</OPTION>";
						}
					?>
					</SELECT>
			</p>
		</div>

			<p><b>Child</b><br />
			<input style="height:30px" type='checkbox' name='child' value="<?php echo !empty($child )?$child :'';?>"
			<?php if(!empty($child)){echo'checked';}?> />
			</p>

			<p><b>Page</b><br />
				<div class="controls">
					<SELECT class="form-control" id="childpage" name="childpage" size="1" onChange="valueselect(this);">
					<!-------------------- BOC 2017.05.27 - Charge Details --->
					<?php
						$childpageLoc = '';
						$childpageSelect = $childpage;
						foreach($dataChildPage as $row)
						{
							$childpageLoc = $row['page'];

							if($childpageLoc == $childpageSelect)
							{
								echo "<OPTION value='".$childpageLoc."' selected>$childpageLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$childpageLoc."'>$childpageLoc</OPTION>";
							}
						}

						if(empty($dataChildPage))
						{
							echo "<OPTION value=0>No Pages</OPTION>";
						}
					?>
					<!-------------------- EOC 2017.05.27 - Charge Details  --->
					</SELECT>
				</div>
			</p>

			<p><b>Role</b><br />
				<div class="controls">
					<SELECT class="form-control" id="role" name="role" size="1">
					<?php
						$roleLoc = '';
						$roleSelect = $role;
						foreach($dataRoles as $row)
						{
							$roleLoc = $row['role'];

							if($roleLoc == $roleSelect)
							{
								echo "<OPTION value=$roleLoc selected>$roleLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$roleLoc>$roleLoc</OPTION>";
							}
						}

						if(empty($dataRoles))
						{
							echo "<OPTION value=0>No Roles</OPTION>";
						}
					?>
					</SELECT>
				</div>
			</p>

			<p><b>Menu Label</b><br />
			<input style="height:30px" type='text' name='label' value="<?php echo !empty($label )?$label :'';?>"/>
			<?php if (!empty($labelError)): ?>
			<span class="help-inline"><?php echo $labelError;?></span>
			<?php endif; ?>
			</p>

			<!--<td><p><b>create</b>
			<input style="height:30px" type='checkbox' name='create' value="<?php echo !empty($create)?$create:'';?>"
									<?php if(!empty($create)){echo'checked';}?> />
			</p>

			<p><b>update</b>
			<input style="height:30px" type='checkbox' name='update' value="<?php echo !empty($update)?$update:'';?>"
						<?php if(!empty($update)){echo'checked';}?> />
			</p>

			<p><b>delete</b>
			<input style="height:30px" type='checkbox' name='delete' value="<?php echo !empty($delete)?$delete:'';?>"
						<?php if(!empty($delete)){echo'checked';}?> />
			</p></td> -->
			<td>
			<p><b>Default Page</b>
			<input style="height:30px" type='checkbox' name='default' value="<?php echo !empty($default)?$default:'';?>"
						<?php if(!empty($default)){echo'checked';}?> />
			<?php if (!empty($defaultError)): ?>
				<span class="help-inline" style="color:red"><?php echo $defaultError;?></span>
			<?php endif; ?>
			</p></td>
			<tr>
			<input style="height:30px;display:none" type='text' id="temppage" name='temppage' value="<?php echo !empty($temppage)?$temppage:'';?>" />
			<h2 class="panel-title"><b>Features</b></h2>
			<div id="features" name="features"><?php echo $featuresText; ?></div>
				<input style="display:none" id="featuresText" name="featuresText" value="<?php echo $featuresText; ?>" />
			</tr>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'access';

			if(isset($_SESSION["GlobalTheme"]))
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];
			}

			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>
		     </div></td>
		</tr>
		</table>
		</form>
	</div>
</div>
</div>
<script>
function parentmenu()
{
	var ele = document.getElementById("parentmenu");
	var text = document.getElementById("parentmenu");
	if(ele.style.display == "block")
	{
    		ele.style.display = "none";
  	}
	else
	{
		ele.style.display = "block";
	}
}

// -- 23.01.2022 - Access ---
 function valuechecked(sel)
 {
	// alert(sel.id+' - '+sel.checked);
	 		//var check = sel.options[sel.selectedIndex].value;
			if(sel.checked)
			{
				document.getElementById(sel.id).value = 'X';
			}
			else
			{
				document.getElementById(sel.id).value = '';
			}
 }
 function valueselect(sel)
 {
		var page = sel.options[sel.selectedIndex].value;
		//alert(page);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectPageFeatures.php",
				       data:{page:page},
						success: function(data)
						 {
							//alert(data);
							//jQuery("#features").html(data);
							document.getElementById("features").innerHTML = data;
							document.getElementById("featuresText").value = data;
							//alert(data);
						 }
					});
				});
			  return false;
  }
</script>
