<?php
/// ------------------------------- BOC Send OTP sms to Clients --------------------------- //// 
/*$phonenumber = "";
$companyname = "";
$otp 		 = "";
$message = "";

$generateResponse  = "";
$ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';

if(!empty($_POST))
{
	$phonenumber = $_POST['phonenumber'];
	$companyname = $_POST['companyname'];
	$message = $_POST['message'];
	
	echo "<h3>Modise 1</h3>";
	$ch = curl_init();

	$options = array(
		CURLOPT_URL            => 'https://api.cmtelecom.com/v1.0/otp/generate',
		CURLOPT_HTTPHEADER     => array(
			'Content-Type: application/json',
			'X-CM-ProductToken: f84364fb-717a-4bee-beb0-8f9ac43bf3ec',
		),
		CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => json_encode(array(
			'recipient' => '0027731238839',
			'sender' => 'eCashMeUp',
		)),
		CURLOPT_RETURNTRANSFER => true
	);
	echo "<h3>Modise 2</h3>";

	curl_setopt_array($ch, $options);
	$response = curl_exec($ch);
	curl_close($ch);
	
	echo "<h3>Modise 3</h3>";

	$generateResponse = json_decode($response);
		
	echo "<h3>Modise - $response</h3>";

	if(empty($generateResponse))
	{
		$otp = $generateResponse;
	}
	else
	{
		$otp = "Error - No OTP Generated";
	}
}*/
//echo "<h3>$generateResponse</h3>";
/// ------------------------------- EOC Send OTP sms to Clients --------------------------- ////
?>

<?php
// -- Data File.
require 'database.php';

// -- Functions File.
require 'functions.php';

// -- Data Model Functions.
if(!function_exists('GetTenantTemplatesByIntUserCode'))
{
	function GetTenantTemplatesByIntUserCode($tenantid,$smstemplate,$templatetype)
	{
			Global $pdo;
			$tbl_name="smstemplate"; // Table name 
			if(!empty($smstemplate))
			{
			  // -- SMSTemplate Record 
		 	 $sql = "SELECT * FROM $tbl_name WHERE IntUserCode = ? and templatetype = ? AND smstemplatename = ?";
			 $q = $pdo->prepare($sql);
			 $q->execute(array($tenantid,$templatetype,$smstemplate));	
			 $data = $q->fetchAll(); 				
			}
			else
			{
			  // -- All Records ~ 
		 	 $sql = "SELECT * FROM $tbl_name WHERE IntUserCode = ? and templatetype = ?";
			 $q = $pdo->prepare($sql);
			 $q->execute(array($tenantid,$templatetype));	
			 $data = $q->fetchAll(); 
			}
			return $data;
	}
}
$tenantid = '';
$templatetype = 'sms';

 // -- Read SESSION tenantid.
 if(isset($_SESSION['bulksmsclientid']))
 {
	$tenantid = $_SESSION['bulksmsclientid'];
	
 }
 
$tenantid = getsession('IntUserCode');
 
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="smstemplate"; // Table name 

//1. ----------------- Tenant Customers (SQL Inner Join) --------------- //
$dataCustomers = GetBulkSMSClientCustomers($tenantid);

 //echo "<script>alert(".$tenantid.")</script>";
			$senderid = '' ;
			if(isset($_SESSION['username'])){$senderid = $_SESSION['username'];}
//Bulk cost Center Account
			$costCenterAccount = '';
			if(isset($_SESSION['account'])){$costCenterAccount = $_SESSION['account'];}
// -- Customers 
$ArrayCustomers = null;
$indexInside = 0;
 
// -- Array Customer 
foreach($dataCustomers as $row)
{
$ArrayCustomers[$indexInside][0] = $row['CustomerId'];
$ArrayCustomers[$indexInside][1] = $row['Title'];
$ArrayCustomers[$indexInside][2] = $row['FirstName'];
$ArrayCustomers[$indexInside][3] = $row['LastName'];
$ArrayCustomers[$indexInside][4] = $row['Street'];
$ArrayCustomers[$indexInside][5] = $row['Suburb'];
$ArrayCustomers[$indexInside][6] = $row['City'];
$ArrayCustomers[$indexInside][7] = $row['PostCode'];
$ArrayCustomers[$indexInside][8] = $row['Dob'];
$ArrayCustomers[$indexInside][9] = $row['phone'];
// -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($row['CustomerId']);
if(!empty($row['refnumber']))
{
	$ArrayCustomers[$indexInside][10] = $row['refnumber'];
}
else
{
	$ArrayCustomers[$indexInside][10] = $ReferenceNumber;
}
$ArrayCustomers[$indexInside][11] = $row['accountholdername'];
$ArrayCustomers[$indexInside][12] = $row['bankname'];
$ArrayCustomers[$indexInside][13] = $row['accountnumber'];
$ArrayCustomers[$indexInside][14] = $row['branchcode'];
$ArrayCustomers[$indexInside][15] = $row['accounttype'];
$ArrayCustomers[$indexInside][16] = $row['email2'];
$ArrayCustomers[$indexInside][17] = $row['phone2'];

$ArrayCustomers[$indexInside][18] = $row['signature'];
$ArrayCustomers[$indexInside][19] = $row['createdby'];
$ArrayCustomers[$indexInside][20] = $row['changedby'];
$ArrayCustomers[$indexInside][21] = $row['createdon'];
$ArrayCustomers[$indexInside][22] = $row['leadid'];
$ArrayCustomers[$indexInside][23] = $row['changedon'];
$ArrayCustomers[$indexInside][24] = $row['status'];
$ArrayCustomers[$indexInside][25] = $row['comments'];
$ArrayCustomers[$indexInside][26] = $row['nextofkiname'];
$ArrayCustomers[$indexInside][27] = $row['nextofkinphone'];
$ArrayCustomers[$indexInside][28] = $row['spousename'];
$ArrayCustomers[$indexInside][29] = $row['spousephone'];
$ArrayCustomers[$indexInside][30] = $row['employername'];
$ArrayCustomers[$indexInside][31] = $row['employerphone'];
$ArrayCustomers[$indexInside][32] = $row['occupation'];
$ArrayCustomers[$indexInside][33] = $row['creditcheck'];
$ArrayCustomers[$indexInside][34] = $row['debitorder'];
$ArrayCustomers[$indexInside][35] = $row['marketinformartion'];
$ArrayCustomers[$indexInside][36] = $row['salary'];
$ArrayCustomers[$indexInside][37] = $row['salarydate'];
$ArrayCustomers[$indexInside][38] = $row['otherincome'];
$ArrayCustomers[$indexInside][39] = $row['UserCode'];
$ArrayCustomers[$indexInside][40] = $row['IntUserCode'];
$ArrayCustomers[$indexInside][41] = $row['subgroupext'];
$ArrayCustomers[$indexInside][42] = $row['bulksmsclientid'];
$ArrayCustomers[$indexInside][43] = $row['bulksmsclientname'];
$ArrayCustomers[$indexInside][44] = $row['alias'];
$ArrayCustomers[$indexInside][45] = $row['subgroupext2'];

$indexInside = $indexInside + 1;
}			
$indexInside = 0;
  
// ---------------------  Placeholder ------------------- //
//$datasmstemplates = GetTenantSmsTemplates($tenantid,'');
$templatetype = 'sms';
$datasmstemplates = GetTenantTemplatesByIntUserCode($tenantid,'',$templatetype); 
$smstemplate = "";
$customer = "";
$phonenumberLoc  = "";
 $groupid = "";
 
// --- Send SMSs via CM Telcoms -- //
$phonenumber = "";
$companyname = "";
$otp 		 = "";
$generateResponse  = "";
$message = "";
$messageLoc = "";

$messageError = '';
$messageLog = '';
$phonenumberError = '';
$valid = true;

//1. --------------------- All Groups ------------------- //
  $sql = 'SELECT * FROM bulksmsgroup';
  $datagroups = $pdo->query($sql);

//1. --------------------- All Groups ------------------- //
  $sql = 'SELECT * FROM bulksmsgroup';
  $datagroups = $pdo->query($sql);  

// -- number_format for e.g.: '0027731238839'
if(!empty($_POST))
{
	$messageError = '';
	$phonenumber = $_POST['phonenumber'];
//	$companyname = $_POST['companyname'];
	$message = $_POST['message'];
	$messageLoc = $message;
	
	$smstemplate = $_POST['smstemplate'];
	$customer = $_POST['customer'];
	
	$phonenumbers = explode(',', $phonenumber);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if (empty($phonenumber)) { $phonenumberError = $phonenumberError.'Please enter or select phone number'; $valid = false;}
	if (empty($message)) { $messageError = $messageError.'Please enter or select message template'; $valid = false;}
// -- count number of characters sms, 255 - 16.12.2018
	if (strlen($message)> 255) { $messageError = $messageError.'The message must not be greater than 255 characters.'; $valid = false;}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
		//var_dump($phonenumbers);

	  $ArraySize =  sizeof($phonenumbers);;
	//echo $ArraySize;
	  for ($i = 0; $i < $ArraySize; $i++) 
	  {
		$phonenumberLoc = $phonenumbers[$i];
		
		// -- Get Placeholders
		$indexInside 		= 0;
		$CustomerId 		= '';
		$Title 	 	 		= '';
		$FirstName   		= '';
		$LastName 	 		= '';
		$Street 	 		= '';
		$Suburb 	 		= '';
		$City 		 		= '';
		$PostCode 	 		= '';
		$Dob 		 		= '';
		$phone 	     		= '';
		$refnumber   		= '';
		$accountholdername  = '';
		$bankname  		    = '';
		$accountnumber      = '';
		$branchcode  	    = '';
		$accounttype  	    = '';
		$email  		    = '';
		$phone2 		    = '';
$signature 			= '';
$createdby 			= '';
$changedby 			= ''; 
$createdon 			= '';
$leadid 			= '';
$changedon 			= '';
$status 			= '';	
$comments 			= ''; 
$nextofkiname 		= ''; 
$nextofkinphone 	= ''; 
$spousename 		= ''; 
$spousephone 		= ''; 
$employername 		= ''; 
$employerphone 		= ''; 
$occupation 		= ''; 
$creditcheck 		= ''; 
$debitorder 		= ''; 
$marketinformartion = ''; 
$salary 			= '';
$salarydate 		= ''; 
$otherincome 		= ''; 
$UserCode 			= ''; 
$IntUserCode 		= '';
$subgroupext 		= ''; 
$bulksmsclientid 	= '';
$bulksmsclientname 	= ''; 
$alias 				= ''; 
$subgroupext2 		= '';

	//  echo "Phone : ".$phonenumberLoc;

	if(!empty($phonenumberLoc))
	{
		// --  Customer Data
		/////////////////////// ---- BOC
		for ($j=0;$j<sizeof($ArrayCustomers);$j++)
		{
				$phone = $ArrayCustomers[$j][17];
									//echo 'phonenumberLoc = '.$phonenumberLoc.'phone = '.$phone;

				if($phonenumberLoc == $phone)
				{
						//echo 'phonenumberLoc = '.$phonenumberLoc.'phone = '.$phone;
					$message 	= $messageLoc;
					$CustomerId = $ArrayCustomers[$j][0];
					$Title 		= $ArrayCustomers[$j][1];
					$FirstName  = $ArrayCustomers[$j][2];
					$LastName 	= $ArrayCustomers[$j][3];
					$Street 	= $ArrayCustomers[$j][4];
					$Suburb 	= $ArrayCustomers[$j][5];
					$City 		= $ArrayCustomers[$j][6];
					$PostCode 	= $ArrayCustomers[$j][7];
					$Dob 		= $ArrayCustomers[$j][8];
					$phone 		= $ArrayCustomers[$j][9];
					$refnumber  = $ArrayCustomers[$j][10];
					$accountholdername = $ArrayCustomers[$j][11];
					$bankname  		   = $ArrayCustomers[$j][12];
					$accountnumber     = $ArrayCustomers[$j][13];
					$branchcode  	   = $ArrayCustomers[$j][14];
					$accounttype  	   = $ArrayCustomers[$j][15];
					$email  		   = $ArrayCustomers[$j][16];
					$phone2 		   = $ArrayCustomers[$j][17];
					
					$signature 			= $ArrayCustomers[$j][18];
					$createdby 			= $ArrayCustomers[$j][19];
					$changedby 			= $ArrayCustomers[$j][20];
					$createdon 			= $ArrayCustomers[$j][21];
					$leadid 			= $ArrayCustomers[$j][22];
					$changedon 			= $ArrayCustomers[$j][23];
					$status 			= $ArrayCustomers[$j][24];
					$comments 			= $ArrayCustomers[$j][25];
					$nextofkiname 		= $ArrayCustomers[$j][26];
					$nextofkinphone 	= $ArrayCustomers[$j][27];
					$spousename 		= $ArrayCustomers[$j][28];
					$spousephone 		= $ArrayCustomers[$j][29];
					$employername 		= $ArrayCustomers[$j][30];
					$employerphone 		= $ArrayCustomers[$j][31];
					$occupation 		= $ArrayCustomers[$j][32];
					$creditcheck 		= $ArrayCustomers[$j][33];
					$debitorder 		= $ArrayCustomers[$j][34];
					$marketinformartion = $ArrayCustomers[$j][35];
					$salary 			= $ArrayCustomers[$j][36];
					$salarydate 		= $ArrayCustomers[$j][37];
					$otherincome 		= $ArrayCustomers[$j][38];
					$UserCode 			= $ArrayCustomers[$j][39];
					$IntUserCode 		= $ArrayCustomers[$j][40];
					$subgroupext 		= $ArrayCustomers[$j][41];
					$bulksmsclientid 	= $ArrayCustomers[$j][42];
					$bulksmsclientname 	= $ArrayCustomers[$j][43];
					$alias 				= $ArrayCustomers[$j][44];
					$subgroupext2 		= $ArrayCustomers[$j][45];
				}
		}
	// -- Placeholders
$message = str_replace('[CustomerId]',$CustomerId ,$message);
$message = str_replace('[Title]',$Title ,$message);
$message = str_replace('[FirstName]',$FirstName ,$message);
$message = str_replace('[LastName]',$LastName ,$message);
$message = str_replace('[Street]',$Street ,$message);
$message = str_replace('[Suburb]',$Suburb ,$message);
$message = str_replace('[City]',$City ,$message);
$message = str_replace('[Dob]',$Dob ,$message);
$message = str_replace('[phone]',$phone ,$message);
$message = str_replace('[refnumber]',$refnumber ,$message);
$message = str_replace('[accountholdername]',$accountholdername ,$message);
$message = str_replace('[bankname]',$bankname ,$message);
$message = str_replace('[accountnumber]',$accountnumber ,$message);
$message = str_replace('[branchcode]',$branchcode ,$message);
$message = str_replace('[accounttype]',$accounttype ,$message);
$message = str_replace('[email]',$email ,$message);
$message = str_replace('[phone2]',$phone2 ,$message);
$message = str_replace('[signature]'	 	 ,$signature 			,$message);			 
$message = str_replace('[createdby]'	 	 ,$createdby 			,$message);
$message = str_replace('[changedby]'	 	 ,$changedby 			,$message);
$message = str_replace('[createdon]'	 	 ,$createdon 			,$message);
$message = str_replace('[leadid]'   	 	 ,$leadid 			    ,$message);
$message = str_replace('[changedon]'	 	 ,$changedon 			,$message);
$message = str_replace('[status]'   	 	 ,$status 			    ,$message);
$message = str_replace('[comments]' 	 	 ,$comments 			,$message); 
$message = str_replace('[nextofkiname]'  	 ,$nextofkiname 		,$message); 
$message = str_replace('[nextofkinphone]'	 ,$nextofkinphone 	    ,$message);
$message = str_replace('[spousename]'	 	 ,$spousename 		    ,$message);
$message = str_replace('[spousephone]'	 	 ,$spousephone 		    ,$message);
$message = str_replace('[employername]'	 	 ,$employername 		,$message); 
$message = str_replace('[employerphone]' 	 ,$employerphone 		,$message);
$message = str_replace('[occupation]'	 	 ,$occupation 		    ,$message);
$message = str_replace('[creditcheck]' 	 	 ,$creditcheck 		    ,$message);
$message = str_replace('[debitorder]'	 	 ,$debitorder 		    ,$message);
$message = str_replace('[marketinformartion]',$marketinformartion   ,$message);
$message = str_replace('[salary]'		 	 ,$salary 			    ,$message);
$message = str_replace('[salarydate]'	 	 ,$salarydate 		    ,$message);
$message = str_replace('[otherincome]'	 	 ,$otherincome 		    ,$message);
$message = str_replace('[UserCode]'		 	 ,$UserCode 			,$message);
$message = str_replace('[IntUserCode]'	 	 ,$IntUserCode 		    ,$message);
$message = str_replace('[subgroupext]'	 	 ,$subgroupext 		    ,$message);
$message = str_replace('[bulksmsclientid]'	 ,$bulksmsclientid 	    ,$message);
$message = str_replace('[bulksmsclientname]' ,$bulksmsclientname 	,$message); 
$message = str_replace('[alias]'		     ,$alias 				,$message);
$message = str_replace('[subgroupext2]'      ,$subgroupext2 		,$message);
				
$phonenumberLoc = $phonenumberLoc;
// -- Add Country code : 0027 
$phonenumberLoc = trim("0027".substr($phonenumberLoc,1));

/////////////////////// ---- EOC
//		echo "Number : ".$i;
		
			//echo "Well Done";
			//$messageLog = $messageLog .'<h2>'.CMSMS::sendMessage($phonenumberLoc, $message).'</h2><br/>';
			$smsRef = new CMSMS();
			$response = $smsRef->sendMessage($phonenumberLoc, $message);
			

//		echo CMSMS::sendMessage($phonenumberLoc, $message);
// -- Log Email sent to recipients.		
$status = 'success';//'failed';//$response;//'success';

			$tenantid = $senderid ;
//print_r($message.$CustomerId.$refnumber.$tenantid.$status.$email.$phonenumberLoc.$costCenterAccount);
			
if($email ==  'NULL'){$email = '';}
AddCommunicatioHistoryBulk($message,$CustomerId,$refnumber,$tenantid,$status,$email,$phonenumberLoc,$costCenterAccount);
		//$messageLog = '<p>'.$message.' - successfully sent to '.$phonenumberLoc.'</p><br/>';
				$messageLog = '<p>Message - successfully sent to '.$phonenumberLoc.'</p><br/>';
		//echo $messageLog;
	  }
	}
  }
}
else
{
	$smstemplate = 'modisetemplate';
}
if(!class_exists('CMSMS')){
  class CMSMS
  {
	function __construct()
	{
		
	}
     public function buildMessageXml($recipient, $message) 
	{
			  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';

      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', $ProductToken);

      $msg = $xml->addChild('MSG');
      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);
      $msg->addChild('BODY', $message);


      return $xml->asXML();
    }

     public function sendMessage($recipient, $message) 
	{
      $xml = self::buildMessageXml($recipient, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);

	  	  echo '<h3>'.curl_error($ch).'</h3>';

	  	$info = curl_getinfo($ch);

      curl_close($ch);

      return $info;
    }
  }
} 
  // Show all information, defaults to INFO_ALL
//phpinfo();
?>
<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="sendsms">SendSMS</a></li>
		</ol>
	</div>
</div> 
-->

<!-- <html> -->
		<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<script src="plugins/jquery/jquery-2.1.0.min.js"></script>

<!-- <body> -->
	<!-- <form METHOD="post"  id="form1" name="form1" class="signup-page"  action="">-->
<div class='container background-white bottom-border'>
 <div class="row">	
<?php if(!empty($_POST))
{
		echo "<div class='alert alert-success fade in'>
            <a href='#' class='close' data-dismiss='alert'>×</a>".
				$messageLog
			."</div>";
}
 ?>
 <form METHOD="post"  id="form1" name="form1" class="signup-page"  action=""> 
 <p><b>Groups</b><br /></p>
			<div class="controls">
					<div class="controls">
						<SELECT class="form-control" id="groupid" name="groupid" size="1" onChange="itemselect(this);" 
						onload="itemselect(this);">
						<?php
							$groupidTemp = '';
							$groupidSelect = $groupid;
							$Arraygroups = null;
							$indexInside = 0;
							
							echo "<OPTION value='' selected>All</OPTION>";
							
							foreach($datagroups as $row)
							{
								$groupidTemp = $row['name'];

								$Arraygroups[$indexInside][0] = $row['name'];
															
								if($groupidTemp == $groupidSelect)
								{
								   echo "<OPTION value=".$groupidTemp."selected>".$groupidTemp."</OPTION>";
								}	
								else
								{
									echo "<OPTION value=$groupidTemp>$groupidTemp</OPTION>";
								}
								$indexInside = $indexInside + 1;
							}
										
							if(empty($datagroups))
							{
								echo "<OPTION value=0>No Groups</OPTION>";
							}
					?>
					</SELECT>
					</div>
</div>

<p><b>Sub Groups</b><br />			
			<div class="control-group">
				<div class="controls">
					<SELECT class="form-control" id="item" name="item" size="1" onChange="selected(this)">	
						<OPTION value='' selected>All</OPTION>
					</SELECT>
					
					<SELECT class="form-control" id="itemArray" multiple name="itemArray[]" size="5" hidden>	
					
					</SELECT>
				</div>
			</div>
			
			</p>

 <p><b>Customers</b><br />
				<div class="control-group">
				<div class="controls">
					<SELECT class="form-control" id="customer" name="customer" size="1" >	
						<OPTION value='' selected>All</OPTION>
					</SELECT>
					
					<SELECT class="form-control" id="itemArray1" multiple name="itemArray1[]" size="5" hidden style="display: none;">	
					
					</SELECT>
				</div>
			</div>
			</p>
	<div class="form-group <?php if (!empty($phonenumberError)){ echo "has-error";}?>">						
			<p>
			Click to add <input  class='btn btn-primary' onclick='input()' type='button' value='Phone number' id='button'>Click to add ALL<input  class='btn btn-primary' onclick='AllPhones()' type='button' value='ALL' id='button'>Click to Remove ALL<input  class='btn btn-primary' onclick='removeALL()' type='button' value='Remove ALL' id='button'>Click to Remove 1 by 1 from Last one first<input  class='btn btn-primary' onclick='remove()' type='button' value='Remove' id='button'><br>						
			<input style="height:30px"  class="form-control margin-bottom-20"  id="phonenumber" name="phonenumber" type="text"  placeholder="Phone Number" value="<?php echo !empty($phonenumber)?$phonenumber:'';?>" readonly>
  			<?php if (!empty($phonenumberError)): ?>
			<small class="help-block"><?php echo $phonenumberError;?></small>
			<?php endif; ?>
			</p>
	</div>
  
<p><b>SMS Template</b><br />
				<SELECT class="form-control" id="smstemplate" name="smstemplate" size="1" onChange="valueselect(this);" onload="valueselect(this);">
					<?php
						$smstemplateLoc = '';
						$smstemplateSelect = $smstemplate;
						foreach($datasmstemplates as $row)
						{
							$smstemplateLoc = $row['smstemplatename'];

							if($smstemplateLoc == $smstemplateSelect)
							{
								echo "<OPTION value='".$smstemplateLoc."' selected>".$smstemplateLoc."</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$smstemplateLoc."'>".$smstemplateLoc."</OPTION>";
							}
						}
												
						if(empty($datasmstemplates))
						{
							echo "<OPTION value=0>No smstemplates</OPTION>";
						}		
					?>
					
					</SELECT>
			</p> 

			<div class="form-group <?php if (!empty($messageError)){ echo "has-error";}?>">									
				  <p>
				  <textarea class="form-control" rows="10" name="message" id="message" placeholder="Message"><?php echo !empty($message)?$message:'';?></textarea>  
								<?php if (!empty($messageError)): ?>
							<small class="help-block"><?php echo $messageError;?></small>
							<?php endif; ?>
				</p>
			</div>

 <?php if (!empty($otp)): ?>
	 <span class="help-inline"><?php echo $otp; ?></span>
  <?php endif; ?>
  <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Send SMS</button><!-- Generate OTP -->
 
 <input type="text" name="IntUserCode" id="IntUserCode" style="display:none"
 value="<?php if(isset($_SESSION['IntUserCode'])){echo $_SESSION['IntUserCode'];}?>" />

 <!-- Display message Log -->
  <?php if (!empty($messageLog)): ?>
	<p>
		<?php 	$messageLogs = explode('<br/>', $messageLog);
		 $ArraySize =  sizeof($messageLogs);;
	//echo $ArraySize;
	  for ($i = 0; $i < $ArraySize; $i++) 
	  {
		//echo $messageLogs[$i];
	  }
				//echo $messageLog; ?>
	</p>	 
  <?php endif; ?>
  </form> 
  </div>
</div> 
<script>
// jQuery
 $(document).ready(function () 
 {
 						var smstemplate = document.getElementById('smstemplate');
var smstemplatename = smstemplate.options[smstemplate.selectedIndex].value; 

 //alert("Modise-Modise");
       valueselect(smstemplate);
	   
	   var item = document.getElementById('item');
		var itemID = item.options[item.selectedIndex].value; 

 //alert("Modise"+item.selectedIndex);
       itemselect(document.getElementById("groupid"));
	   //selected(document.getElementById("customer"));
	   
    });

function AllPhones()
{
		var phones = document.getElementById('customer');
		var phone = '';
	for(var i=0; i < phones.length; i++)
     {
       cell = phones.options[i].value;
	   phone = document.forms.form1.phonenumber.value;
	   if(phone != '')
	   {
		   phone = phone+",";
	   }
	   document.forms.form1.phonenumber.value = phone + cell;
     }
}	
function input()
{
	var title = document.getElementById('customer').value;
	var area = document.forms.form1.phonenumber.value;
	 if(area != '')
	   {
		   //area = "027"+area.substring(1);
		   area = area+",";
	   }
    document.forms.form1.phonenumber.value = area + title;
}    
// -- Removing the last one added.. 16.12.2018
function remove()
{
	var title = document.getElementById('customer').value;
	var area = document.forms.form1.phonenumber.value;
	 if(area != '')
	   {
		   area = area+",";
	   }
	   else
	   {
		   
	   }
	str  = document.forms.form1.phonenumber.value;
	str = str.slice(0, -11);   
    document.forms.form1.phonenumber.value = str;
}

// -- Removing ALL the last one added.. 16.12.2018
function removeALL()
{
    document.forms.form1.phonenumber.value = '';
}

 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var smstemplate = sel.options[sel.selectedIndex].value;      
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectSMSTemplates.php",
				       data:{smstemplate:smstemplate},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#message").html(data);
						 }
					});
				});				  
			  return false;
  }
  
// jQuery
 $(document).ready(function () {
 						
    });
 // -- Enter to capture comments   ---
 function itemselect(sel) 
 {
		var groupid = sel.options[sel.selectedIndex].value;      
		var IntUserCode = document.getElementById('IntUserCode').value;
        // -- load customers
	    selected(document.getElementById("customer"));
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectSubGroup.php",
				       data:{groupid:groupid,IntUserCode:IntUserCode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#item").html(data);
							jQuery("#itemArray").html(data);
							
						var earrings = document.getElementById('itemArray');
						earrings.style.display = 'none';//visibility
						
						// -- Only assist if all is selected.
						jQuery('#itemArray option').prop('selected', true);
						
						 }
					});
				});				  
			  return false;
  }
  
  function selected2(sel) 
  {
	  var ext = '';//sel.options[sel.selectedIndex].value;  
	  if (typeof sel.options[sel.selectedIndex] !== 'undefined')
	  {
		ext = sel.options[sel.selectedIndex].value;
	    sel.selectedIndex  = sel.selectedIndex;
	  }
	 		var IntUserCode = document.getElementById('IntUserCode').value;
alert(IntUserCode + ext);

	 jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBulkSMSClientCustomers.php",
				       data:{ext:ext,IntUserCode:IntUserCode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#customer").html(data);
							jQuery("#itemArray1").html(data);
							
						var earrings = document.getElementById('itemArray1');
						earrings.style.display = 'none';//visibility
						
						// -- Only assist if all is selected.
						jQuery('#itemArray1 option').prop('selected', true);
						
						 }
					});
				});	
	 //alert(sel.selectedIndex);
	 			  return false;

  }
  
  function selected(sel) 
  {
	 //var ext = sel.options[sel.selectedIndex].value;  
	 //sel.selectedIndex  = sel.selectedIndex;
	 
	 var ext = '';//sel.options[sel.selectedIndex].value;  
	  if (typeof sel.options[sel.selectedIndex] !== 'undefined')
	  {
		ext = sel.options[sel.selectedIndex].value;
	    sel.selectedIndex  = sel.selectedIndex;
	  }
	 		var IntUserCode = document.getElementById('IntUserCode').value;
	 		var group = document.getElementById('groupid').value;
     
//alert('IntUserCode :'+IntUserCode+'ext : '+ext+'group :'+group);

	 jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBulkSMSClientCustomers.php",
				       data:{ext:ext,IntUserCode:IntUserCode,group:group},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#customer").html(data);
							jQuery("#itemArray1").html(data);
							
						var earrings = document.getElementById('itemArray1');
						earrings.style.display = 'none';//visibility
						
						// -- Only assist if all is selected.
						jQuery('#itemArray1 option').prop('selected', true);
						
						 }
					});
				});	
	 //alert(sel.selectedIndex);
	 			  return false;

  }
  selected(this)
  
</script> 
<!-- </body>
</html> -->