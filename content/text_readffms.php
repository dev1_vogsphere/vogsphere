<!-- Debit Order Schedule 2018.06.02 -->
<!----------- debitorderschedule.js ----------------->
<script src="assets/js/cashpaymentschedule.js"></script>
<!-- End Debit Order Schedule -->
<?php
	require 'database_ffms.php';
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	require 'database.php';
    $pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	// -- Database Declarations and config:
	$pdo2 = Database::connectDB();
	$pdo2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$tbl_member = "member";
	$tbl_member_policy = "member_policy";
	$tbl_address = 'address';
	$dbname = "eloan";

	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
	$$sequenceNumber ='';

    $IntUserCodeB = getsession('UserCode');
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}

	if ( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data..
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}

	if ( null==$customerid )
	    {
		header($location);
		}
		else if( null==$ApplicationId )
		{
		header($location);
		}
	 else {

		$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
		AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id
		AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id";
		//echo $sql;
		$q = $pdo_ffms->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		//print_r($customerid);
		//print_r($ApplicationId);
		//print_r($q);
		$data = $q->fetch(PDO::FETCH_ASSOC);
		//echo (print_r($data,True));
		//print_r($data);
		
		if ( !empty($data['Bank'])) //20231116
		{
		$sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($data['Bank'],$data['Branch_code']));
		$dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);
		}
		//1. ---------------------- BOC - Account Types ------------------------------- //
		// 15.04.2017 -
		
		if ( !empty($data['Account_type']))//20231116
		{
		$sql = 'SELECT * FROM accounttype WHERE accounttypeid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($data['Account_type']));
		$dataAccountType = $q->fetch(PDO::FETCH_ASSOC);
		$data['accounttype'] = $dataAccountType['accounttypedesc'];
		}
		
		if ( !empty($data['payment_method']))//20231116
		{
		//2. ---------------------- BOC - Payment Method ------------------------------- //
		$sql = 'SELECT * FROM method_of_contribution where Method_of_contribution_id=? and IntUserCode=?';
		$q = $pdo_ffms->prepare($sql);
		$q->execute(array($data['payment_method'],$IntUserCodeB));
		$dataPaymentMethod = $q->fetch(PDO::FETCH_ASSOC);
		$data['payment_methoddesc'] = $dataPaymentMethod['Method_of_contribution_name'];
		}
		//echo  $IntUserCodeB;
		//get sequenceNumber
		try {
			//$query = "SELECT idSeq FROM fin.Seq_Generator_PayAt WHERE Client_Id = ?";
			$query = "SELECT idSeq FROM fin.Seq_Generator_PayAt";
			$q = $pdo_ffms->prepare($query);
			$q->execute();
			$results = $q->fetchAll(PDO::FETCH_ASSOC);

			if (count($results) > 0) {
			foreach ($results as $row) {
				$sequenceNumber = str_pad($row['idSeq'], 6, "0", STR_PAD_LEFT);
				//echo "Sequence Number: " . $sequenceNumber . "<br>";
			}
			}
			else
			{
			echo "No records found for Client ID: $IntUserCodeB";
			}
			} 
		catch (PDOException $e) 
		{
			echo "Error: " . $e->getMessage();
		}
		$reference=$data['Member_Policy_id'];
		$referenceid=$data['Member_id'];
/* -payment method
 $sql = 'SELECT * FROM method_of_contribution WHERE bankid = ? AND branchcode = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['Bank'],$data['Branch_code']));
		  $dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);
*/
		  // $data['branchcode'] = $dataBankBranches['branchdesc'];
		  //$data['bankname'] = $dataBanks['bankname'];
		  
		/*if(empty($data))
		{
			header($location);
		}

		if( isset($data) && ($data!==null) )
		{
		}
		else
		{
			header($location);

		}

		$sql = 'SELECT * FROM user WHERE userid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
	    $dataUserEmail = $q->fetchAll();

		// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //

		// --------------------- BOC 2017.04.15 ------------------------- //
		//1. --------------------- Bank and Branch code ------------------- //
		  $sql = 'SELECT * FROM bank WHERE bankid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname']));
		  $dataBanks = $q->fetch(PDO::FETCH_ASSOC);


		  $sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname'],$data['branchcode']));
		  $dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);

		  // $data['branchcode'] = $dataBankBranches['branchdesc'];
		  $data['bankname'] = $dataBanks['bankname'];

		//2. ---------------------- BOC - Provinces ------------------------------- //
		// 15.04.2017 -
		  $sql = 'SELECT * FROM province WHERE provinceid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['State']));
		  $dataProvince = $q->fetch(PDO::FETCH_ASSOC);
		  $data['State'] = $dataProvince['provincename'];

		//3. ---------------------- BOC - Account Types ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM accounttype WHERE accounttypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['accounttype']));
		 $dataAccountType = $q->fetch(PDO::FETCH_ASSOC);
		 $data['accounttype'] = $dataAccountType['accounttypedesc'];

		// ---------------------- EOC - Account Types ------------------------------- //

		//4. ---------------------- BOC - Payment Method ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM paymentmethod WHERE paymentmethodid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['paymentmethod']));
		 $dataPaymentMethod =$q->fetch(PDO::FETCH_ASSOC);
		 $data['paymentmethod'] = $dataPaymentMethod['paymentmethoddesc'];

		// ---------------------- EOC - Payment Method ------------------------------- //

		//5. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM paymentfrequency WHERE paymentfrequencyid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['paymentfrequency']));
		 $dataPaymentFrequency = $q->fetch(PDO::FETCH_ASSOC);
		 $data['paymentfrequency'] = $dataPaymentFrequency['paymentfrequencydesc'];

		// ---------------------- EOC - Payment Frequency ------------------------------- //

		//6. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 -
		 $sql = 'SELECT * FROM loantype WHERE loantypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['loantypeid']));
		 $dataLoanType = $q->fetch(PDO::FETCH_ASSOC);
		 $data['loantypeid'] = $dataLoanType['loantypedesc'];

		// ---------------------- EOC - Payment Frequency ------------------------------- //
		// BOC -- 15.04.2017 -- Updated.
		$BankName  = '';
		$BranchCode = '';
		// EOC -- 15.04.2017 -- Updated.
		// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
*/
		Database::disconnect();

		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;
	}
	
// -- BOC 13/12/2022 - Page Features Authorization...	
// -- Authorization of features
//$role = $dataUserEmail['role'];//
$dataPageFeaturesArr = null;
$currentpageArr = null;
$currentpageArr = explode("?",basename($_SERVER['REQUEST_URI'],".php"));
$currentpage = $currentpageArr[0];
$authorized_featuresArr = null;
$role = $_SESSION['role'];

$displaygenerate_payments='display:inline-block';
$displaydownloadcontract ='display:inline-block';
$displayupload_document ='display:inline-block';

$authorized_featuresArr   = authorize_features($pdo,$currentpage,$role);
//print_r($authorized_featuresArr);
$displaygenerate_payments = $authorized_featuresArr['displaygenerate_payments'];
$displaydownloadcontract  = $authorized_featuresArr['displaydownloadcontract'];
$displayupload_document   = $authorized_featuresArr['displayupload_document'];
// -- EOC 13/12/2022 - Page Features Authorization...	

// -- BOC CR1002 - 31/01/2023 - Only show generate payment button on APPROVE..	
if (!empty($data['Active'])){
if ($data['Active'] != '0')
{
	$displaygenerate_payments  = 'display:none';
}
if ($data['Active'] == '0')
{
	$displayupload_document    = 'display:inline-block';
	$displaydownloadcontract   = 'display:inline-block';
}
// -- EOC 31/01/2023 - Only show generate payment button on APPROVE..	
$displaygenerate_payments  = 'display:none';
}

$phone= $data['Cellphone'];
/*if (!empty($data['Cellphone']))
{
//$phone=echo $data['Cellphone'];
$phone='0813073233';
}
else{
$phone='0813073233';
}; */

?>
<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div class="row">
<!-- Login Box -->
<form class="login-page" action="read?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

									<table class=   "table table-user-information">
						<!-- Customer Details -->
<!-- Title,FirstName,LastName -->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>
</td></div>

</tr>

<tr>
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Title</strong></label>
		<div class="controls">
		  	<?php if (!empty($data['Title'])){echo $data['Title'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>First Name</strong></label>
		<div class="controls">
			<?php if (!empty($data['First_name'])){echo $data['First_name'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Last Name</strong></label>
		<div class="controls">
			<?php if (!empty($data['Surname'])){echo $data['Surname'];}?>
		</div>
	</div>
	</td>
</tr>

<!-- Street,Suburb,City -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Address</strong></label>
		<div class="controls">
		   	<?php if (!empty($data['Line_5'])){echo $data['Line_5'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<?php if (!empty($data['Line_6'])){echo $data['Line_6'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label"><strong>Town</strong></label>
		<div class="controls">
		    <?php if (!empty($data['Line_7'])){echo $data['Line_7'];}?>
		</div>
	</div>
	</td>
</tr>

<!-- State,PostCode,Dob -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<?php
		// --------------------- BOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
			if (!empty($data['Line_8'])){echo $data['Line_8'];}
			?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Date of birth</strong></label>
		<div class="controls">
		    <?php if (!empty($data['Date_of_birth'])){echo $data['Date_of_birth'];}?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Contact Number</strong></label>
		<div class="controls">
		    <?php if (!empty($data['Cellphone'])){echo $data['Cellphone'];}?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Email</strong></label>
		<div class="controls">
		    <?php
			   if (!empty($data['Member_email'])){ echo $data['Member_email'];}
			?>
		</div>
	</div>
	</td>
</tr>
<!----------------------------------------------------------- Banking Details -------------------------------------------->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>
</td></div>

</tr>
<!-- Account Holder Name,Bank Name,Account number -->
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Holder Name</strong></label>
		<div class="controls">
			<?php if (!empty($data['Account_holder'])){echo $data['Account_holder'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Bank Name</strong></label>
		<div class="controls">
			<?php if (!empty($data['Bank'])){echo $data['Bank'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
		    <?php if (!empty($data['Account_number'])){echo $data['Account_number'];}?>
		</div>
	</div>
	</td>
</tr>
<!-- branch code,Account type -->
<tr>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Branch Code</strong></label>
		<div class="controls">
			<?php if (!empty($data['Branch_code'])){echo $data['Branch_code'].' - '.$dataBankBranches['branchdesc'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group">
		<label class="control-label"><strong>Account Type</strong></label>
		<div class="controls">
			<?php if (!empty($data['accounttype'])){echo $data['accounttype'];}?>
		</div>
	</div>
	</td>
</tr>

<!----------------------------------------------------------- End Banking Details ---------------------------------------->

<!----------------------------------------------------------- Loan Details -------------------------------------------->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->
<tr>
<td><div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Membership Details
                    </a>
                </h2>
</td></div>

</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Policy Number</strong></label>
		<div class="controls">
			<?php if(!empty($data['Member_Policy_id'])){echo $data['Member_Policy_id'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Source of Income</strong></label>
		<div class="controls">
			<?php if(!empty($data['Source_of_income_name'])){echo $data['Source_of_income_name'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Member ID</strong></label>
		<div class="controls">
		    <?php if(!empty($data['Member_id'])){echo $data['Member_id'];}?>
		</div>
	</div>
	</td>
</tr>

<!-- ReqLoadValue,DateAccpt,LoanDuration -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Benefit Amount</strong></label>
		<div class="controls">
		    <?php if(!empty($data['Benefits'])){echo number_format($data['Benefits'],2);}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Join Date</strong></label>
		<div class="controls">
		   <?php if(!empty($data['Creation_date'])){echo $data['Creation_date'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>Policy name</strong></label>
		<div class="controls">
		  <?php if(!empty($data['Policy_name'])){echo $data['Policy_name'];}?>
		</div>
	</div>
	</td>
</tr>

<!-- Spouse_id -->
<tr>
	<td>
	<div class="control-group <?php echo !empty($InterestRateError)?'error':'';?>">
		<label class="control-label"><strong>Spouse ID</strong></label>
		<div class="controls">
				<?php if(!empty($data['Spouse_id'])){echo $data['Spouse_id'];}?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<div class="controls">
		<?php
		if(!empty($data['Active'])){
		if($data['Active'] == 0)
								{
								echo 'Active';
								}
								if($data['Active'] != 0)
								{
								echo 'Inactive';
								}
								/*if($data['ExecApproval'] == 'CAN')
								{
								echo 'Cancelled';
								}
								if($data['ExecApproval'] == 'REJ')
								{
								echo 'Rejected';
								}
								if($data['ExecApproval'] == 'SET')
								{
								echo 'Settled';
								}
								// -- Defaulter - 20.06.2018.
								if($data['ExecApproval'] == 'DEF')
								{
								echo 'Defaulter';
								}

								// -- Legal - 07.11.2018.
								if($data['ExecApproval'] == 'LEGAL')
								{
								echo 'Legal';
								}

								// -- Rehabilitation - 07.11.2018.
								if($data['ExecApproval'] == 'REH')
								{
								echo 'Rehabilitation';
								}*/
		}
		?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group">
		<label class="control-label"><strong>Notify me</strong></label>
		<div class="controls">
		    <?php if(!empty($data['notify'])){echo $data['notify'];}?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Payment Method</strong></label>
				<div class="controls">
					<?php if(!empty($data['payment_methoddesc'])){echo $data['payment_methoddesc'];}?>
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Income Provider Name</strong></label>
				<div class="controls">
					<?php if(!empty($data['Income_provider_name'])){echo $data['Income_provider_name'];}?>
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Monthly subscription</strong></label>
				<div class="controls">
					<?php if(!empty($data['Premium_amount'])){echo $data['Premium_amount'];}?>
				</div>
		</div>
	</td>

</tr>
<tr>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>First Payment Date</strong></label>
				<div class="controls">
					<?php if(!empty($data['Commence_date'])){echo $data['Commence_date'];}?>
				</div>
		</div>
	</td>
	<td>
		<div class="control-group">
				<label class="control-label"><strong>Debit Order Date</strong></label>
				<div class="controls">
					<?php if(!empty($data['Debit_date'])){echo $data['Debit_date'];}?>
				</div>
		</div>
	</td>
		<td>
		<div class="control-group">
				<label class="control-label"><strong>Retail payments</strong></label>
				<div class="controls">
					<?php if(!empty($data['payat'])){echo $data['payat'];}?>
				</div>
		</div>
	</td>
</tr>
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
if(!empty($data['Remarks'])){
$rejectreason = $data['Remarks'];}
if(!empty($data['Active'])){
if ($data['Active'] == '3')
{
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div>
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}}
?>
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						  <a class="btn btn-primary" href="finfindmember">Back</a>
<!-- 13.06.2018 - Debit Order Schedule -->
		<span></span>
						<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
						 <?php
						 	if(isset($_SESSION))
							{
								$role = $_SESSION['role'];
							}

						 // -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);
if(!empty($data['Personal_number'])){$AccountHolder	 = $data['Personal_number'];}else{$AccountHolder='';}
if(!empty($data['Branch_code'])){$BranchCode	 = $data['Branch_code'];}else{$BranchCode='';}
if(!empty($data['Account_number'])){$AccountNumber	 =  $data['Account_number'];}else{$AccountNumber='';}
$Servicetype	 = 'NADREQ';
$ServiceMode	 = 'NAEDO';
$UserCode      = 'UFEC';
$IntUserCode   = 'UFEC';
$Entry_Class   = 'UFEC';
$Contact_No		 = 'UFEC';
$Notify				 = 'UFEC';
$createdby     = 'UFEC';
$createdon     = 'UFEC';
if(!empty($data['Account_type'])){$AccountType	 = $data['Account_type'];}else{$AccountType='';}
if(!empty($data['First_name'])){$fname 			 = $data['First_name'];}else{$fname='';}
if(!empty($data['Surname'])){$lname 			 = $data['Surname'];}else{$lname='';}
$frequency 		 = '';//$data['paymentfrequency'];
// Show if the role is administrator
//	if($role == 'admin'){}
	
					     $href = "'"."href=cashpaymentschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency&fname=$fname&lname=$lname"."'";
						 $Policy_Schedule="effms_confirmation.php?tenantid=$IntUserCodeB&customerid=$customerid";
						 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="cashpaymentschedule('.$href.')" style="'.$displaygenerate_payments.'">Cash Payments</button>';
					     echo '<span> </span>';
						 echo '<a class="btn btn-primary" href="'.$Policy_Schedule.'" target="_blank">Policy Schedule</a>';
						 echo '<span> </span>';
						 //echo '<button type="button" onclick="register_patat(ActivatePayAt(), \'' . $sequenceNumber . '\', \'' . $IntUserCodeB . '\', \'' . $phone . '\')">Activate eCashMeUp Pay</button>';
						 //echo '<button type="button" onclick="register_patat(ActivatePayAt(), \'' . $sequenceNumber . '\', \'' . $IntUserCodeB . '\', \'' . $phone . '\', \'' . $reference . '\', \'' . $referenceid . '\')">Activate Retail Pay</button>';
						if ($data['payat'] === "Registered") {
						echo '<button type="button" disabled title="Retail payment is already registered.">Activate Retail Pay</button>';
						} else {
						echo '<button type="button" onclick="register_patat(ActivatePayAt(), \'' . $sequenceNumber . '\', \'' . $IntUserCodeB . '\', \'' . $phone . '\', \'' . $reference . '\', \'' . $referenceid . '\')">Activate Retail Pay</button>';
						}
						//<!-- Feedback message -->
						 echo '<span> </span>';
						 echo '<div id="feedback" style="margin-top: 20px; color: green;"></div>';

						 ?>
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>

								</div>
								</br> -->
											<div id="divcashpaymentschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Cash Payments</h1>
											</div>
							<!-- <div id="divSuccess">

								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>
						</div>
					  </td>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
<!-- 13.06.2018 - Debit Order Schedule -->
	<?php
	if(!empty($data['Member_Policy_id'])){$ApplicationId = $data['Member_Policy_id'];}else{$ApplicationId='';}
	if(!empty($data['Member_id'])){$customerid = $data['Member_id'];}else{$customerid='';}
	if(!empty($data['IntUserCode'])){$IntUserCode = $data['IntUserCode'];}else{$IntUserCode ='';}
	// -- if no role mean user never logged in go back to login page.
	if (empty($role))
	{
			header("Location: customerLogin");
	}

	// -- end of login check.

	// Only show contract if eloan application has been approved.
	if(!empty($data['Active'])){
	if($data['Active'] == 0)// && $role == 'customer') 13/12/2022
	{
	  $hash="&guid=".md5(date("h:i:sa"));
	  // -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' style='".$displaydownloadcontract."' target = '_blank' href=effms_confirmation?customerid=$customerid&tenantid=$IntUserCode>Download Contract</a></div></td>";
	  // -- EOC Encode Change - 16.09.2017.
	}
	}
/* 13/12/2022 - 
	// Show if the role is administrator
	if($role == 'admin')
	{
		$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Download Contract</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.

	}
*/

	?>

						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php
	if(!empty($data['Member_Policy_id'])){$ApplicationId = $data['Member_Policy_id'];}else{$ApplicationId ='';}
	if(!empty($data['Member_id'])){$customerid = $data['Member_id'];}else{$customerid = '';}
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
	echo "<a class='btn btn-primary' style='".$displayupload_document."' href=uploadffms?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
						</table>
									</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>

   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
//$filerootpath = 'ffms/upload/';
	$filerootpath = '';
	//str_replace('content','',filerootpath()).
	// $pdo = Database::connectDB();
	$tbl_name="loanapp"; // Table name
	//$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	$check_user = "SELECT * FROM member WHERE Member_id = ?";

	$pdo = db_connect::db_connection();			
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 
	 $sql =  $check_user;
	 $q = $pdo->prepare($sql);
	 $q->execute(array($customerid));
	 $row = $q->fetch(PDO::FETCH_ASSOC);
	 $count = $q->rowCount();

	 // -- ID Document
	 $iddocument = '';

	 // -- Contract Agreement
	 $contract = '';

	 // -- Bank statement
	 $bankstatement = '';

	 // -- Proof of income
	 $proofofincome = '';

	 // -- Fica
	 $fica = '';

	 $consentform = '';
	 $other = '';
   	 $other2 = '';
	 // --

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = '';
	 $other3    = "";
	 $other4    = "";
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);


	//if($count >= 1)
    {

	//$row = mysql_fetch_array($result);


	$iddocument  =  $row['FILEIDDOC'];
	$contract = $row['FILECONTRACT'];
	$bankstatement = $row['FILEBANKSTATEMENT'];
	$proofofincome = $row['FILEPROOFEMP'];
	$fica = $row['FILEFICA'];

   // -- BOC 01.10.2017 - Upload Credit Reports.
   	$creditreport = $row['FILECREDITREP'];

	$consentform = $row['FILECONSENTFORM'];
	$other = $row['FILEOTHER'];
   	if(!empty($data['FILEOTHER2'])){$other2 = $data['FILEOTHER2'];}else{$other2 ='';};
	$NoFIle = "";
   // -- EOC 01.10.2017 - Upload Credit Reports.

   	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	 $debitform = $row['FILEDEBITFORM'];
	 $other3    = $row['FILEOTHER3'];
	 $other4    = $row['FILEOTHER4'];
	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.


   // -- BOC 01.10.2017 - Alternative if no file upload.
   if(empty($iddocument)){$iddocument = $NoFIle;}
   if(empty($contract)){ $contract = $NoFIle;}
   if(empty($bankstatement)){$bankstatement = $NoFIle;}
   if(empty($proofofincome)){$proofofincome = $NoFIle;}
   if(empty($fica)){ $fica = $NoFIle;}
   if(empty($creditreport)){  $creditreport = $NoFIle;}
   if(empty($consentform)){  $consentform = $NoFIle;}
   if(empty($other)){  $other = $NoFIle;}
   if(empty($other2)){  $other2 = $NoFIle;}

   if(empty($debitform)){$debitform = $NoFIle;}
   if(empty($other3)){  $other3 = $NoFIle;}
   if(empty($other4)){  $other4 = $NoFIle;}
   // -- EOC 01.10.2017 - Alternative if no file upload.

    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";

	$iddocumentHTML = '';
	if($iddocument == '')
	{
		$iddocumentHTML = "<tr>
         <td><a href='$filerootpath$iddocument' target='_blank'>$iddocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	else
	{
		$iddocumentHTML = "<tr>
         <td><a $classCSS href='$filerootpath$iddocument' target='_blank'>$ViewDocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	$contractHTML = '';
	if($contract == '')
	{
		$contractHTML = "<tr>


         <td><a href='$filerootpath$contract' target='_blank'>$contract</a></td>
         <td>Contract Agreement</td>
      </tr>
      ";
	}
	else
	{
		$contractHTML = "

      <tr>
         <td><a $classCSS href='$filerootpath$contract' target='_blank'>$ViewDocument</a></td>
         <td>Contract Agreement</td>
      </tr>";
	}

	$bankstatementHTML = '';
	if($bankstatement == '')
	{
		$bankstatementHTML = "<tr>
         <td><a href='$filerootpath$bankstatement' target='_blank'>$bankstatement</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	else
	{
		$bankstatementHTML = "<tr>
         <td><a $classCSS href='$filerootpath$bankstatement' target='_blank'>$ViewDocument</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}

	$proofofincomeHTML = '';
	if($proofofincome == '')
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a href='$filerootpath$proofofincome' target='_blank'>$proofofincome</a></td>
         <td>Proof of income</td>
      </tr>
	  ";
	}
	else
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$proofofincome' target='_blank'>$ViewDocument</a></td>

         <td>Proof of income</td>
      </tr>
	  ";
	}
	$ficaHTML = '';
	if($fica == '')
	{
		$ficaHTML = "

	  <tr>
         <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	else
	{
		$ficaHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$fica' target='_blank'>$ViewDocument</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	$creditreportHTML = '';
	if($creditreport == '')
	{
		$creditreportHTML = "
	  <tr>
         <td><a href='$filerootpath$creditreport' target='_blank'>$creditreport</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	else
	{
		$creditreportHTML = "
	  <tr>
         <td><a $classCSS href='$filerootpath$creditreport' target='_blank'>$ViewDocument</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	$consentformHTML = '';
	if($consentform == '')
	{
		$consentformHTML = "
	  <tr>
         <td><a href='$filerootpath$consentform' target='_blank'>$consentform</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	else
	{
		$consentformHTML = "
	  <tr>
         <td><a  $classCSS href='$filerootpath$consentform' target='_blank'>$ViewDocument</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	$otherHTML = '';
	if($other == '')
	{
		$otherHTML ="
	  <tr>
         <td><a href='$filerootpath$other' target='_blank'>$other</a></td>
         <td>Other</td>
      </tr>
	";
	}
	else
	{
		$otherHTML ="
	  <tr>
         <td><a $classCSS href='$filerootpath$other' target='_blank'>$ViewDocument</a></td>
         <td>Other</td>
      </tr>
	";
	}
	$other2HTML = '';
	if($other2 == '')
	{
		$other2HTML = "
  	  <tr>
         <td><a href='$filerootpath$other2' target='_blank'>$other2</a></td>
         <td>Other 2</td>
      </tr>
";
	}
	else
	{
		$other2HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other2' target='_blank'>$ViewDocument</a></td>
         <td>Other 2</td>
      </tr>";
	}


	$debitformHTML = '';
	if($debitform == '')
	{
		$debitformHTML = "
  	  <tr>
         <td><a href='$filerootpath$debitform' target='_blank'>$debitform</a></td>
         <td>Debit Form</td>
      </tr>
";
	}
	else
	{
		$debitformHTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$debitform' target='_blank'>$ViewDocument</a></td>
         <td>Debit Form</td>
      </tr>";
	}

	$other3HTML = '';
	if($other3 == '')
	{
		$other3HTML = "
  	  <tr>
         <td><a href='$filerootpath$other3' target='_blank'>$other3</a></td>
         <td>Other 3</td>
      </tr>
";
	}
	else
	{
		$other3HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other3' target='_blank'>$ViewDocument</a></td>
         <td>Other 3</td>
      </tr>";
	}

	$other4HTML = '';
	if($other4 == '')
	{
		$other4HTML = "
  	  <tr>
         <td><a href='$filerootpath$other4' target='_blank'>$other4</a></td>
         <td>Other 4</td>
      </tr>
";
	}
	else
	{
		$other4HTML = "
  	  <tr>
         <td><a $classCSS href='$filerootpath$other4' target='_blank'>$ViewDocument</a></td>
         <td>Other 4</td>
      </tr>";
	}


   echo "<tbody>"
      .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$consentformHTML.$creditreportHTML.$otherHTML.$other2HTML.$debitformHTML.$other3HTML.$other4HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->
</table>
<!------------------------------ End File List ------------------------------------>

<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------>
</form>


	<!-- End Login Box -->
</div>

</div>
<!-- === END CONTENT === -->
<script>

// Function to return sample data array
function ActivatePayAt() {
	
	let memberPolicyId = "<?php echo isset($data['Member_Policy_id']) ? htmlspecialchars($data['Member_Policy_id'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let title = "<?php echo isset($data['Title']) ? htmlspecialchars($data['Title'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let firstName = "<?php echo isset($data['First_name']) ? htmlspecialchars($data['First_name'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let surname = "<?php echo isset($data['Surname']) ? htmlspecialchars($data['Surname'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let memberId = "<?php echo isset($data['Member_id']) ? htmlspecialchars($data['Member_id'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let cellphone = "<?php echo isset($data['Cellphone']) ? htmlspecialchars($data['Cellphone'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let premiumAmount = "<?php echo isset($data['Premium_amount']) ? htmlspecialchars($data['Premium_amount'], ENT_QUOTES, 'UTF-8') : ''; ?>";
    let Active = "1";
    let combined = cellphone + ";" + memberId;
	
	return [
	['H', 'FULL', '1.0'],
	['D',memberPolicyId,title,firstName,surname,memberId,cellphone,premiumAmount,combined,Active],
	['T','1']
	];
}

</script>