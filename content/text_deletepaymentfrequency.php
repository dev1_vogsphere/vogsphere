<?php 
	require 'database.php';
	
	$paymentfrequencyid = null;
	
	if ( !empty($_GET['paymentfrequencyid'])) 
	{
		$paymentfrequencyid = $_REQUEST['paymentfrequencyid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$paymentfrequencydi = $_POST['paymentfrequencyid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM paymentfrequency WHERE paymentfrequencyid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($paymentfrequencydi));
			Database::disconnect();
			echo("<script>location.href='paymentfrequency.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="deletepaymentfrequency.php" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a Payment Frequency
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="paymentfrequencyid" value="<?php echo $paymentfrequencyid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="paymentfrequency.php">No</a>
					  </div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
