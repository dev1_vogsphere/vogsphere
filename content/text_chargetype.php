<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="user"; // Table name 
 $chargetypename = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search Charge Types</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='chargetypename' name='chargetypename' placeholder='Charge Type' class='form-control' type='text' value=$chargetypename>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $chargeoptionname = $_POST['name'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($chargeoptionname))
					    {
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM chargetype WHERE name = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($chargeoptionname));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM chargetype";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newchargetype.php>Add New Charge Type</a> <br/> <p></p>";
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Charge Type ID</b></td>"; 
						echo "<td><b>Charge Type Name</b></td>"; 
						echo "<td><b>Charge Type Description</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['chargetypeid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['name']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['chargetypedesc']) . "</td>";  						
						echo "<td valign='top'><a class='btn btn-success' href=editchargetype.php?chargetypeid={$row['chargetypeid']}>Edit</a></td><td>
						<a class='btn btn-danger' href=deletechargetype.php?chargetypeid={$row['chargetypeid']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div></div>
   		<!---------------------------------------------- End Data ----------------------------------------------->