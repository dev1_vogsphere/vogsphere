<div class='container background-white bottom-border'>
	<div class='row margin-vert-30'>
		 <div class="col-md-12 text-left">
			 <i><h2 class="fa fa-thumbs-o-up fa-3x color-primary padding-top-10 animate fadeIn"> NAEDO</h2></i>
		 </br>
		 </br>
			 <p class="p1" class="animate fadeIn">The NAEDO debit order service is a Non-Authenticated Early Debit Order that helps to track a customer’s account for up to 32 days and process your debit order when funds become available.</p>
			 <p>
			<button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Subscribe today</font></button>
			</p>
		 </div>
	 </div>
</div>
