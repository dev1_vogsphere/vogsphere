<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl="tenantcustomers"; // Table name 

//1. ---------------------  Users ------------------- //
  $sql = 'SELECT * FROM user';
  $datausers = $pdo->query($sql);

//2. ---------------------  Customers ------------------- //
  $sql = 'SELECT * FROM customer';
  $datacustomers = $pdo->query($sql);						   						 
   
$count 				= 0;
$customer			= '';
$user			= '';

// -- Errors
$customerError		=	'';
$userError		=	'';

$valid = true;

if (!empty($_POST)) 
{   $count = 0;

$customer		=	$_POST['customer'];
$user			=	$_POST['user'];

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION													  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	$valid = CheckTenantExist($user,$customer);
 if ($valid) 
  {	
			$count = AssignTenantCustomers($user,$customer);
  }
}

// -- Assign Tenant Customers.
function AssignTenantCustomers($user,$customer)
{
	Global 	$tbl;
	$count = 0;
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO $tbl(tenantid, customerid) VALUES (?,?)";
	$q = $pdo->prepare($sql);
	$q->execute(array($user,$customer));
	Database::disconnect();
	$count = $count + 1;
	return $count;
}

// -- Checked Assigned Customer to Tenant.
function CheckTenantExist($tenantid,$customerid)
{
	Global $customerError;
	Global 	$tbl;

	$lc_valid = true;	
	
	if($tenantid == $customerid)
	{$customerError = "<p class='status'>Tenant Customer Assignment cannot be the same.</p>"; $lc_valid = false;}

	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	// ---------------- Check TenantID --------------- //
	$query = "SELECT * FROM $tbl WHERE tenantid = ? AND customerid = ?";
	$q = $pdo->prepare($query);
	$q->execute(array($tenantid,$customerid));
	
	if ($q->rowCount() >= 1)
	{$customerError = "<div class='form-group has-error'><p class='status'>Tenant Customer Assignment already exists.</p></div>"; $lc_valid = false;}
	return $lc_valid;
}

?>
<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="assigntenantcustomers">Assign Tenant Customers</a></li>
		</ol>
	</div>
</div>-->
<div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
	  <div class="row">
<form name='form1' action='' method='POST' class="signup-page">   
<?php
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Assign Tenant Customers  successfully added.</p>");
		}
		else
		{
			printf($customerError);
		}
?>				
			<p><b>Tenant</b><br />
				<SELECT class="form-control" id="user" name="user" size="1">
					<?php
						$userLoc = '';
						$userSelect = $user;
						foreach($datausers as $row)
						{
							$userLoc = $row['userid'];

							if($userSelect == $userSelect)
							{
								echo "<OPTION value='".$userLoc."' selected>$userLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$userLoc."'>$userLoc</OPTION>";
							}
						}
												
						if(empty($datausers))
						{
							echo "<OPTION value=0>No users</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			<p>
			
			<p><b>Customer</b><br />
				<SELECT class="form-control" id="customer" name="customer" size="1">
					<?php
						$customerLoc = '';
						$customerSelect = $customer;
						foreach($datacustomers as $row)
						{
							$customerLoc = $row['CustomerId'];

							if($customerSelect == $customerSelect)
							{
								echo "<OPTION value='".$customerLoc."' selected>$customerLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$customerLoc."'>$customerLoc</OPTION>";
							}
						}
												
						if(empty($datacustomers))
						{
							echo "<OPTION value=0>No customers</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'unassigntenantcustomers';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			?>
  <button class='btn btn-primary' type='submit'>Save</button><!-- Generate OTP -->
  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->
			
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>		
<script>
function input()
{
	var title = document.getElementById('customer').value;
//	var area = document.forms.form1.smstemplatecontent.value;
  //  document.forms.form1.smstemplatecontent.value = area + title;
}    
</script>