<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_currency="currency"; // Table name 
$currencyidError = null;
$currencyid = '';

$currencynameError = null;
$currencyname = ''; 

$count = 0;

if (isset($_GET['currencyid']) ) 
{ 
$currencyid = $_GET['currencyid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$currencyid = $_POST['currencyid'];
	$currencyname = $_POST['currencyname'];
	
	$valid = true;
	
	if (empty($currencyname)) { $currencynameError = 'Please enter Currency Description.'; $valid = false;}
	
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE $tbl_currency SET currencyname = ? WHERE currencyid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($currencyname,$currencyid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_currency where currencyid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($currencyid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$currencyname = $data['currencyname'];
}

?>

<div class="container background-white bottom-border">   
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST'  class="signup-page"> 
		<h2 class='text-center'>
             Change Currency
        </h2>
		
		<div class="panel-heading">

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Currency successfully updated.</p>");
		}
?>
				
</div>				

				
			<p><b>Currency ID</b><br />
			<input style="height:30px" type='text' name='currencyid' value="<?php echo !empty($currencyid)?$currencyid:'';?>" readonly />
			</p>
			
			<p><b>Currency Name</b><br />
			<input style="height:30px" type='text' name='currencyname' value="<?php echo !empty($currencyname)?$currencyname:'';?>"/>
			<?php if (!empty($currencynameError)): ?>
			<span class="help-inline"><?php echo $currencynameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td>
				<div>
					<button type="submit" class="btn btn-primary">Update</button>
					<?php if($_SESSION['role'] == 'admin'){?>
					<a class="btn btn-primary" href="currency">Back</a>
					<?php }?>
				</div>
			</td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>