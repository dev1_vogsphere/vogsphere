<?php 
$IntUserCode = '';if(isset($_SESSION['IntUserCode'])){$IntUserCode = $_SESSION['IntUserCode'];}
$UserCode = '';if(isset($_SESSION['UserCode'])){$UserCode = $_SESSION['UserCode'];}
$userid = '';if(isset($_SESSION['username'])){$userid = $_SESSION['username'];}
$role1 = '';
if(isset($_SESSION['role'])){$role1 = $_SESSION['role'];}
$currenttab = '';

?>
<style>
.divider{
    width:5px;
    height:auto;
    display:inline-block;
}
</style>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">
     <br />
  <!-- Tab v1 -->
    <div class="tabs">
       <ul class="nav nav-tabs">
           <li class="active">
               <a href="#sample-1b" id="paybeneficiary" onclick="get_active_tab(this);" data-toggle="tab">Pay Single Beneficiary</a>
           </li>
			<li >
               <a href="#sample-1c"  id="uploadbatchpayment" onclick="get_active_tab(this);" id="1c" data-toggle="tab">Upload Batch Payments</a>
           </li>
           <li>
               <a href="#sample-1d" id="addbeneficiary" onclick="get_active_tab(this);" data-toggle="tab">Add Beneficiary</a>
           </li>
			<li>
               <a href="#sample-1e"  id="paymentsreport" onclick="get_active_tab(this);" data-toggle="tab">Payment History</a>
           </li>	
			<li>
               <a href="#sample-1f"  id="editbeneficiary" onclick="get_active_tab(this);" data-toggle="tab">Edit Beneficiary</a>
           </li>											   
       </ul>
       <div class="tab-content">
           <div class="tab-pane fade in active" id="sample-1b">
           </div>
           <div class="tab-pane fade in" id="sample-1c">
			 <?php include 'content/text_uploadbatchpayments.php'; ?>
		   </div>
           <div class="tab-pane fade in" id="sample-1d">
                <div class="clearfix"></div>
			</div>
			
			<div class="tab-pane fade in" id="sample-1e">
			</div>
			
			<div class="tab-pane fade in" id="sample-1f">
			</div>
	 </div>
       </div>
	   <input type="text" id="currenttab" value="<?php echo $currenttab; ?>" style="display:none" />
</div>
<script>
/*$('#Upload').click(function () {
        // do something you want ...
        //$('uploadbatchpayments').submit();
		//alert('sasasas');
		tab3 = document.getElementById('sample-1c');
		tab3.click();
    });*/
jQuery(document).ready(function()
{
	get_active_tab(document.getElementById('paybeneficiary'));
});
var benefiaryid = '';	
function clickTab(name)
{
	def = document.getElementById("bid");
	if(isValue(def))
	{
		benefiaryid = def.value;
	}
	tab1 = document.getElementById(name);	
	tab1.click();
	//get_active_tab(tab1);
}	

function isValue(def) 
{
    //if ( $.type(value) == 'null'
      //  || $.type(value) == 'undefined') 
        //|| $.trim(value) == ''
        //|| ($.type(value) == 'number' && !$.isNumeric(value))
       // || ($.type(value) == 'array' && value.length == 0)
        //|| ($.type(value) == 'object' && $.isEmptyObject(value)) ) 
	 return ($.type(def) != 'undefined') ? def : false;
	//else{ return true;}	
		
}

function handleChange()
{
	try
	{
	//value = 'null';//document.getElementById('Account_Holder').value;
		def = document.getElementById('Account_Holder');
	//alert($.type(def));
	//if(isValue(def))
	{
		//document.getElementById('Account_Holder').value = 'Modise';
		//document.getElementById('benefiaryid').value = '20';//sel.options[sel.selectedIndex].value; 
		//enter_benefiary();
		//alert('Defined');
	}	
	//alert(document.getElementById('Account_Holder').value);
	}
	catch(e)
	{
		
	}
	
}
function get_active_tab(el)
{
	//alert(el.id);
			//file = "content/text_addbenefiary.php";
		//jQuery("#sample-1d").load(file);
	tab2 = document.getElementById('sample-1b');
	tab3 = document.getElementById('sample-1c');
	tab4 = document.getElementById('sample-1d');
	tab5 = document.getElementById('sample-1e');
	tab6 = document.getElementById('sample-1f');
	
	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;
	if(el.id == 'paybeneficiary')
	{
		tab1 = document.getElementById('sample-1b');
		//alert(tab1.innerHTML.trim());
		if(!tab1.innerHTML.trim())
		{
			file = "content/text_paysinglebenefiary.php?benefiaryid="+benefiaryid+"&param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
			jQuery("#sample-1b").load(file);	
		}
		benefiaryid = '';	
		tab2 = document.getElementById('sample-1d');
		tab2.innerHTML = '';

		tab3 = document.getElementById('sample-1e');
		tab3.innerHTML = '';
	
		//alert(document.getElementById('Account_Holder'));
//jQuery('#sample-1b').on('change', handleChange).trigger('change');
		//jQuery(window).on("load", handleChange).trigger('load');			

	} 
	else if(el.id == 'addbeneficiary')
	{
		//tab3.innerHTML = '';
		tab1 = document.getElementById('sample-1b');
		tab1.innerHTML = '';
		file = "content/text_addbenefiary.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1d").load(file);	
	}
	else if(el.id == 'uploadbatchpayment')
	{ 
		//tab2.innerHTML = '';
		//tab4.innerHTML = '';
		//tab6.innerHTML = '';
		// Load the Batch Upload.
		//alert(tab3.innerHTML);
		//if(!tab3.innerHTML.trim())
		//{
			//alert(tab3.innerHTML.trim());
			//file = "content/text_uploadbatchpayments.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
			//jQuery("#sample-1c").load(file);
			//alert(tab3.innerHTML);
		//}	
	}	
	else if(el.id == 'paymentsreport')
	{
		//tab3.innerHTML = '';
		file = "content/text_billpayments.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1e").load(file);	
		
		//tab2 = document.getElementById('paymentsreport');
		//tab2.click();		
	}
	else if(el.id == 'editbeneficiary')
	{
		//tab3.innerHTML = '';
		tab2 = document.getElementById('editbeneficiary');
		file = "content/text_editbenefiary.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1f").load(file);	
	}
	else
	{
		//tab3.innerHTML = '';
		tab1 = document.getElementById('sample-1b');
		tab1.innerHTML = '';
		file = "content/text_addbenefiary.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1d").load(file);
		
		tab2 = document.getElementById('addbeneficiary');
		tab2.click();

	}
	document.getElementById("currenttab").value = el.innerHTML;
}
</script>