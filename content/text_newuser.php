<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//1. ---------------------  Role ------------------- //
$sql = 'SELECT * FROM role ORDER BY role';
$dataRoles = $pdo->query($sql);	

$IntCode  = '';
$UserCode = '';			
  
$tbl_user="user"; // Table name 
$passwordError = null;
$password = '';

$emailError = null;
$email = ''; 

$role = '';

$count = 0;
$userid = '';
$useralias = '';

$Original_email = '';

$Title 		= "";
$FirstName  = "";
$LastName   = "";

$TitleError 	= null;
$FirstNameError = null;

$LastNameError  = null;
$account = '';
$cust_phone = '';
$cust_phoneError = null; 
$createdby = getsession('username');
$debicheckusercode = '';
$approver = '';
$usercodePayments = '';
$tenantid = '';
if(isset($_SESSION['bulksmsclientid'])){$tenantid = $_SESSION['bulksmsclientid'];}

if (!empty($_POST)) 
{   $count = 0;
	$password = getpost('password');
	$email = getpost('email');
	$userid = getpost('userid');
	$role = getpost('role');
	$IntCode  = getpost('IntCode');
	$UserCode = getpost('UserCode');
	$useralias = getpost('useralias');
	$cust_phone = getpost('cust_phone');
	$createdby = getsession('username');
	$debicheckusercode = $_POST['debicheckusercode'];
	$usercodePayments = $_POST['usercodePayments'];
	// -- Get approver checkbox
	if(isset($_POST['approver']))
	{$approver = 'X';}
	else
	{$approver = '';}

	$valid = true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$Title 		= getpost('Title');
	$FirstName  = getpost('FirstName');
	$LastName 	= getpost('LastName');
	$account    = getpost('account');
				
	if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
	if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
	
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);
		
		if (empty($userid)) { 
		$useridError = 'Please enter ID/Passport number'; 
		$valid = false;
		}
		// SA ID pattern (13 digits)
		 $saIdPattern= "/^\d{13}$/";

		// Passport pattern (alphanumeric, usually 9 characters, but varies)
		$passportPattern = "/^[A-Z0-9]{6,9}$/i";
		
		if (preg_match($saIdPattern, $userid)) {
		//echo "Valid South African ID.";
		$useridError = 'Valid South African ID.'; $valid = true;
		} elseif (preg_match($passportPattern, $userid)) {
		//echo "Valid Passport number.";
		$useridError = 'Valid Passport number.'; $valid = true;
		} else {
		//echo "Invalid ID or Passport number.";
		$useridError = 'Invalid ID or Passport number.';
		$valid = false;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$Title 		= getpost('Title');
	$FirstName  = getpost('FirstName');
	$LastName 	= getpost('LastName');
	$UserCode   = getpost('UserCode');
	if (empty($cust_phone)) { $cust_phoneError = 'Please enter cell phone number'; $valid = false;}

	if (empty($password)) { $passwordError = 'Please enter password Description.'; $valid = false;}
    if (empty($userid)) { $useridError = 'Please enter User ID.'; $valid = false;}	
	else
	{
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($userid));
			if ($q->rowCount() >= 1)
			{$useridError = 'User ID already in use.'; $valid = false;}
	}

		// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}	
	
	if (empty($email)) 
	{ $emailError = 'Please enter e-mail'; $valid = false;}
	else
	{
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}		
	 }
	 
	 
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO  user (userid,password,email,role,bulksmsclientid,alias,account,approver) VALUES (?,?,?,?,?,?,?,?)"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$password = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($userid,$password,$email,$role,$tenantid,$useralias,$account,$approver));// -- Changed 16.09.2017 - MD5 Encryption.
			// -- Add as Customer.
			$arrayCollections[1] = $userid;
			$arrayCollections[2] = $Title;
			$arrayCollections[3] = $FirstName;
			$arrayCollections[4] = $LastName;
			$arrayCollections[5] = 'MP';
			$arrayCollections[6] = '';
			$arrayCollections[7] = $cust_phone;// -- phone number2 - customer
			$arrayCollections[8] = date('Y-M-D');
			$arrayCollections[9] = 'Active';
			$arrayCollections[10] = '';
			$arrayCollections[11] = '';
			$arrayCollections[12] = $tenantid;
			$arrayCollections[13] = '';
			$arrayCollections[14] = $useralias;
			$arrayCollections[15] = $UserCode;//15
			$arrayCollections[16] = $createdby;//16
			$arrayCollections[17] = $email;// -- email2 - customer
			$arrayCollections[18] = $debicheckusercode;// --  - debicheckusercode
			$arrayCollections[19] = $IntCode;
			$arrayCollections[20] = $usercodePayments;
			AddNewCustomer($arrayCollections);
			//AddCustomer($userid,$Title,$FirstName,$LastName,$IntCode,$UserCode,);
			$smstemplatename = 'welcomeuser';
			$applicationid = 0;
			//auto_email($userid,$smstemplatename,$applicationid);
			auto_sms($userid,$smstemplatename,$applicationid);			
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">
    <div class="row margin-vert-30">
        <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
            <form action="" method="POST" class="signup-page">
                <div class="panel-heading">
                    <h2 class="panel-title text-center mb-4">Let’s Onboard a New User!</h2>
					</br>
                    <?php 
                    if (isset($message)) {
                        foreach ($message as $msg) {
                            echo "<p class='alert alert-danger'>$msg</p>";
                        }
                    }
                    if (!empty($count)) {
                        echo "<p class='alert alert-success'>User successfully added.</p>";
                    }
                    ?>
                </div>

                <!-- User Title -->
                <div class="form-group">
                    <label for="Title">Title</label>
                    <select id="Title" class="form-control" name="Title" required>
                        <?php
                        $titles = ['Mr', 'Mrs', 'Miss', 'Ms', 'Dr'];
                        foreach ($titles as $title) {
                            $selected = (!empty($Title) && $Title == $title) ? 'selected' : '';
                            echo "<option value='$title' $selected>$title</option>";
                        }
                        ?>
                    </select>
                </div>

                <!-- First Name -->
                <div class="form-group">
                    <label for="FirstName">First Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="FirstName" placeholder="First Name" value="<?php echo htmlspecialchars($FirstName ?? ''); ?>" required>
                    <?php if (!empty($FirstNameError)): ?>
                        <small class="text-danger"><?php echo $FirstNameError; ?></small>
                    <?php endif; ?>
                </div>

                <!-- Last Name -->
                <div class="form-group">
                    <label for="LastName">Last Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="LastName" placeholder="Last Name" value="<?php echo htmlspecialchars($LastName ?? ''); ?>" required>
                    <?php if (!empty($LastNameError)): ?>
                        <small class="text-danger"><?php echo $LastNameError; ?></small>
                    <?php endif; ?>
                </div>

                <!-- User ID (RSA ID or Passport) -->
                <div class="form-group">
                    <label for="userid">ID Number / Passport <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="userid" oninput="validateDocument(this.value)" value="<?php echo htmlspecialchars($userid ?? ''); ?>" required>
                    <small id="doc_feedback" class="text-danger"></small>
                    <?php if (!empty($useridError)): ?>
                        <small class="text-danger"><?php echo $useridError; ?></small>
                    <?php endif; ?>
                </div>

                <!-- User Alias -->
                <div class="form-group">
                    <label for="useralias">User Alias<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="useralias" value="<?php echo htmlspecialchars($useralias ?? ''); ?>">
                </div>

                <!-- Password -->
                <div class="form-group">
                    <label for="password">Password <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" required>
                    <?php if (!empty($passwordError)): ?>
                        <small class="text-danger"><?php echo $passwordError; ?></small>
                    <?php endif; ?>
                </div>

                <!-- Email -->
                <div class="form-group">
                    <label for="email">Email Address<span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="email" value="<?php echo htmlspecialchars($email ?? ''); ?>" required>
                    <?php if (!empty($emailError)): ?>
                        <small class="text-danger"><?php echo $emailError; ?></small>
                    <?php endif; ?>
                </div>

                <!-- Role Selection -->
                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" id="role" name="role">
                        <?php if (!empty($dataRoles)): ?>
                            <?php foreach ($dataRoles as $row): ?>
                                <option value="<?php echo htmlspecialchars($row['role']); ?>" <?php echo ($row['role'] == $role) ? 'selected' : ''; ?>>
                                    <?php echo htmlspecialchars($row['role']); ?>
                                </option>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <option value="0">No Roles Available</option>
                        <?php endif; ?>
                    </select>
                </div>

                <!-- Additional Fields -->
                <?php
                $fields = [
                    'account' => 'Account <span class="text-danger">*</span>', 
                    'IntCode' => 'Internal User Code <span class="text-danger">*</span>',
                    'UserCode' => 'User Code <span class="text-danger">*</span>',
                    'cust_phone' => 'Cell Number <span class="text-danger">*</span>',
                    'debicheckusercode' => 'DebiCheck User Code <span class="text-danger">*</span>',
                    'usercodePayments' => 'Payments User Code <span class="text-danger">*</span>'
                ];

                foreach ($fields as $name => $label) {
                    echo "
                    <div class='form-group'>
                        <label for='$name'>$label</label>
                        <input type='text' class='form-control' name='$name' value='" . htmlspecialchars($$name ?? '') . "'>
                    </div>";
                }
                ?>

                <!-- Approver Checkbox -->
                <div class="form-group">
                    <label for="approver">Is an Approver?</label>
                    <input type="checkbox" id="approver" name="approver" <?php echo !empty($approver) ? 'checked' : ''; ?>>
                </div>
				</br>
                <!-- Buttons -->
				<div class="form-group d-flex justify-content-end">
				<button type="submit" class="btn btn-primary" style="font-size: 1.5rem; padding: 10px 20px;">Save</button>
				<a href="users" class="btn btn-secondary" style="font-size: 1.5rem; padding: 10px 20px; margin-left: 10px;">Back</a>
				</div>
				
            </form>
        </div>
    </div>
</div>

