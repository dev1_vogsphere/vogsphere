<!-- Loan Calculator --->
<div class="container background-white bottom-border">
<div class="margin-vert-30">
  <h1 align=center><strong>Loan Calculator</strong></h1>
<hr>
<div class="container">
      <div class="price-box">
        <div class="row">
          <div class="col-sm-6">
                <form class="form-horizontal form-pricing" role="form">
                  <div class="p1" class="price-slider">
                    <h4 class="great">Amount</h4>
                    <span>Minimum R100 is required</span>
                    <div class="col-sm-12">
                      <div id="slider_amirol"></div>
                    </div>
                  </div>
                </br>
              </br>
            </br>
                  <div class="p1"class="price-slider">
                    <h4 class="great">Duration</h4>
                    <span>Please choose one</span>
                    <div class="btn-group btn-group-justified">
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month active-month selected-month" id="1month">1 Month</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="2month">2 Months</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="3month">3 Months</button>
                      </div>
                    </div>
                  </div>
                  <div class="price-slider">
                  <!--  <h4 class="great">Term</h4>-->
                  <!--    <span >Please choose one</span>-->
                      <input name="sliderVal" type="hidden" id="sliderVal" value="0" readonly="readonly" />
                      <input name="month" type="hidden" id="month" value="1month" readonly="readonly" />

				            <input name="term" type="hidden" id="term" value="monthly" readonly="readonly" />
                      <div class="btn-group btn-group-justified">
                        <!--<div class="btn-group btn-group-lg">
                    <button type="button" class="btn btn-primary btn-lg btn-block term" id="quarterly">Quarterly</button>
                  </div>-->
                        <div class="btn-group btn-group-lg">
                        <!--  <button type="button" class="btn btn-primary btn-lg btn-block term active-term selected-term" id="monthly">Monthly</button>-->
                        </div>
                       <!-- <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term" id="weekly">Weekly</button>
                        </div> -->
                      </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="price-form">

                  <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Annually (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">-->
                            <!-- <p class="price lead" id="total"></p>
                            <input class="price lead" name="totalprice" type="text" id="total" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div class="p1" class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label  for="amount_amirol" class="control-label">Monthly Instalment(R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                      <!--  </div>-->
                      <!--  <div class="col-sm-4">-->
                            <input type="hidden" id="amount_amirol" class="form-control">
                             <!-- <p class="price lead" id="total12"></p>-->
                            <input class="price lead" name="totalprice12" type="text" id="total12" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>
                    <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Weekly (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             --><!-- <p class="price lead" id="total52"></p>
                            <input class="price lead" name="totalprice52" type="text" id="total52" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div style="margin-top:30px"></div>
                    <!-- <hr class="style"> -->

                  <div class="form-group">
                      <div  class="col-sm-12">
                        <a href="applicationForm" class="btn btn-primary btn-lg btn-block">Apply Now</a>
                      </div>
                  </div>
           </br>
              </br>
                 </br>
				   <div class="col-md-9">
						<h3>Required Documents</h3>
						<ul class="p1" class="tick animate fadeInRight" style="width:280px">
						<li>ID Document</li>
						<li>Latest 3 months Bank Statement</li>
						<li>Proof of Employment/Income</li>
						<li>Proof of Residence</li>
						</ul>
						<p></p>
						<br>
					</div>

                    <!-- <div class="form-group">
                      <div class="col-sm-12">
                          <img src="https://github.com/AmirolAhmad/Bootstrap-Calculator/blob/master/images/payment.png?raw=true" class="img-responsive payment" />
                      </div>
                    </div> -->

                  </div>

                </form>
            </div>
            <!-- <p class="text-center" style="padding-top:10px;font-size:12px;color:#2c3e50;font-style:italic;">Created by <a href="https://twitter.com/AmirolAhmad" target="_blank">AmirolAhmad</a></p> -->
        </div>

          </div>

      </div>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
 <!-- -- EOC Loan Calculator -->
<hr>
</div>
</div>
  <!-- Portfolio
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
			<!--	<h3 style=" margin: 0px 25% 0 30%;">Our Partners Bringing Value To Our Business</h3>
                    <div class="row padding-top-40">
                        <ul class="portfolio-group">
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="https://www.mercantile.co.za">
                                    <figure class="animate fadeInLeft">
                                        <img alt="image1" src="assets/img/mercantile.jpg">
                                        <figcaption>
                                            <h3>Mercantile Bank</h3>
											Mercantile Bank provides a wide range of international and local banking services to the business community with a segment focus on the Portuguese market.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li> -->
                            <!-- //Portfolio Item// -->
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="http://www.vogsphere.co.za">
                                    <figure class="animate fadeIn">
                                        <img alt="image2" src="assets/img/vogsphere.png">
                                        <figcaption>
                                            <h3>Vogsphere (Pty) Ltd</h3>
											Vogsphere (Pty) Ltd is a custom software development company based in Krugersdorp, South Africa.
											The company was established in 2010 and registered in 2012.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            End Portfolio -->
