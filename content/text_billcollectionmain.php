<?php 
$currenttab = ''; 
$IntUserCode = '';if(isset($_SESSION['IntUserCode'])){$IntUserCode = $_SESSION['IntUserCode'];}
$UserCode = '';if(isset($_SESSION['UserCode'])){$UserCode = $_SESSION['UserCode'];}
$userid = '';
if(isset($_SESSION['username'])){$userid = $_SESSION['username'];}
$role1 = '';
if(isset($_SESSION['role'])){$role1 = $_SESSION['role'];}
if(!empty($_POST))
{
	if(isset($_POST['currenttab']))
	{
		$currenttab = $_POST['currenttab'];
	}
}
?>
<script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>

<script type="text/javascript" src="assets/js/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.bootstrap.min.js"></script>

<script type="text/javascript" src="assets/js/datatables/jszip.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/vfs_fonts.js"></script>

<script type="text/javascript" src="assets/js/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.colVis.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/sum%28%29.js"></script>

<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
</style>
<div class="container background-white bottom-border">
     <br />
	 </br>
  <!-- Tab v1 -->
    <div class="tabs">
        <ul class="nav nav-tabs">
		
            <li class="active">
                <a href="#sample-1a" id="scheduledebitorder" onclick="get_active_tab(this);" data-toggle="tab">Schedule Single Debit Order</a>
            </li>
            <li>
                <a href="#sample-1b" id="uploadbatchdebitorders" onclick="get_active_tab(this);" data-toggle="tab">Upload Batch Debit Orders</a>
            </li>
			
			<!--<li>
                <a href="#sample-1f" id="registermandate" onclick="get_active_tab(this);" data-toggle="tab">Debi-Check Mandate</a>
            </li>-->
	
			<li>
                <a href="#sample-1c" id="futuredebitorders" onclick="get_active_tab(this);"  data-toggle="tab">Scheduled Debit Orders</a>
            </li>
            <li>
                <a href="#sample-1d" id="debitordersreport" onclick="get_active_tab(this);" data-toggle="tab">Debit Orders History</a>
            </li>
			<li>
                <a href="#sample-1e" id="recon" onclick="get_active_tab(this);" data-toggle="tab">Recon</a>
            </li>									
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="sample-1a">
            </div>
            <div class="tab-pane fade in" id="sample-1b">
				<?php  include 'content/text_uploadbatchinstructions.php'; ?>
            </div>
            <div class="tab-pane fade in" id="sample-1c">
            </div>
            <div class="tab-pane fade in" id="sample-1d">
			</div>
			<div class="tab-pane fade in" id="sample-1e">
	     	</div>
			 <div class="tab-pane fade in" id="sample-1f">
			 <?php  include 'content/text_adddebicheckmandate.php'; ?>
	     	</div>
	     </div>
           </div>
		   <form method="POST">
	   <input type="text" name="currenttab" id="currenttab" value="<?php echo $currenttab; ?>" style="display:none" />		   
	   
	   <input type="text" name="tab3" id="tab3" value="" style="display:none" />		   
	   <input type="text" name="tab4" id="tab4" value="" style="display:none" />		   
		 </form>
</div>
<script>
/*
https://bbbootstrap.com/snippets/multi-step-form-wizard-30467045
https://bootsnipp.com/snippets/yaZa
*/
jQuery(document).ready(function()
{
	get_active_tab(document.getElementById('scheduledebitorder'));

});
function clickTab(name)
{
	tab1 = document.getElementById(name);	
	tab1.click();
}	
function handleChange()
{

}
function get_active_tab(el)
{
	tab1 = document.getElementById('sample-1a');
	tab2 = document.getElementById('sample-1b');
	tab3 = document.getElementById('sample-1c');
	tab4 = document.getElementById('sample-1d');
	tab5 = document.getElementById('sample-1e');
	tab6 = document.getElementById('sample-1f');

	if(el.id == 'scheduledebitorder')
	{
		if(!tab1.innerHTML.trim())
		{
			file = "content/text_billInstruction.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
			jQuery("#sample-1a").load(file);	
		}
		//tab2.innerHTML = '';
		tab3.innerHTML = '';
		tab4.innerHTML = '';
		tab5.innerHTML = '';		
		//jQuery(window).on("load", handleChange).trigger('load');			
	} 
	else if(el.id == 'uploadbatchdebitorders')
	{
		tab1.innerHTML = '';
		tab3.innerHTML = '';
		tab4.innerHTML = '';
		tab5.innerHTML = '';
		file = "content/text_uploadbatchinstructions.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1b").load(file);	
	}
	else if(el.id == 'futuredebitorders')
	{ 
		tab1.innerHTML = '';
		//tab2.innerHTML = '';
		tab4.innerHTML = '';
		tab5.innerHTML = '';
		file = "content/text_billFutureCollection.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1c").load(file);
		//jQuery("#sample-1c").on("load", handleChange).trigger('load');			
		//tab3.click();
			/*beforeSend: function(){
                        $("#spinner").show();
                    };
                    
                    complete: function(){
                        $("#spinner").hide();
                    };*/
	}	
	else if(el.id == 'debitordersreport')
	{
		tab1.innerHTML = '';
		//tab2.innerHTML = '';
		tab3.innerHTML = '';
		tab5.innerHTML = '';
		file = "content/text_billCollection.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		jQuery("#sample-1d").load(file);	
	}
	else if(el.id == 'recon')
	{
		var monthselection = "";
		tab1.innerHTML = '';
		//tab2.innerHTML = '';
		tab3.innerHTML = '';
		tab4.innerHTML = '';
		if(tab5.innerHTML.trim())
		{
			var month = document.getElementById("month").value;
			var tomonth = document.getElementById("tomonth").value;	
			var srUserCode = document.getElementById("srUserCode").value;
			monthselection = "|"+month+"|"+tomonth+"|"+srUserCode;
		}
		file = "content/text_collectionrecon.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>"+monthselection;
		jQuery("#sample-1e").load(file);
		tab5.click();
	}
	else if(el.id == 'registermandate')
	{
		tab1.innerHTML = '';
		//tab2.innerHTML = '';
		tab3.innerHTML = '';
		tab5.innerHTML = '';
		//file = "content/text_adddebicheckmandate.php?param=<?php echo $IntUserCode.'|'.$UserCode.'|'.$userid.'|'.$role1; ?>";
		//jQuery("#sample-1f").load(file);	
	}
	document.getElementById("currenttab").value = el.id;
}
</script>