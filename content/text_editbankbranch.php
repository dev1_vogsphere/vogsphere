<?php 
require 'database.php';
$branchdescError = null;
$branchdesc = '';

$branchcodeError = null;
$branchdesc = '';

$bankidError = null;
$BankName = '';

$count = 0;
$branchcode = 0;

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 
	Database::disconnect();	
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

if (isset($_GET['branchcode']) ) 
{ 
$branchcode = $_GET['branchcode']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$branchdesc = $_POST['branchdesc'];
	$BankName = $_POST['BankName'];

	$valid = true;
	
	if (empty($branchdesc)) { $branchdescError = 'Please enter Bank Branch Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE bankbranch SET branchdesc = ?, bankid = ? WHERE branchcode = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($branchdesc,$BankName,$branchcode));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from bankbranch where branchcode = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($branchcode));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$branchdesc = $data['branchdesc'];
		$BankName = $data['bankid'];
}

?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
  
<div class="panel-heading">
<h2 class="text-center">
                         Change Bank Branch Details
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Bank Branch successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Bank Branch</b><br />
			<input style="height:30px" type='text' name='branchdesc' value="<?php echo !empty($branchdesc)?$branchdesc:'';?>"/>
			<?php if (!empty($branchdescError)): ?>
			<span class="help-inline"><?php echo $branchdescError;?></span>
			<?php endif; ?>
			</p> 
			<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="bankbranch">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>