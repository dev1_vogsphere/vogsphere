<?php
// -- Constant Values
$text_view = "<span class='glyphicon glyphicon-eye-open'></span>";
$text_email = "<span class='glyphicon glyphicon-envelope'></span>";
$eMailInvoices = array(); 
$indexInvoiceSent = 0;
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM bankresponsecodes';
$databankresponsecode = $pdo->query($sql);

$databankresponsecodeArr = array();
foreach($databankresponsecode as $row)
{
	$databankresponsecodeArr[] = $row['bankresponsecodedesc'];
}
//print_r($databankresponsecodeArr);

$FromDate= '';
$ToDate='';
$sessionrole='';

 $Allstatus = '';
 $status    = '';

 $AllServiceMode = '';
 $ServiceMode    = '';

 $AllServiceType = '';
 $ServiceType    = '';

if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

$customerid = '';
$Client_Id = '';
if(isset($_SESSION['IntUserCode'])){$Client_Id=$_SESSION['IntUserCode'];}
////////////////////////////////////////////////////////////////////////
// -- From Single Point Access 
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	
	if(empty($Client_Id)){$Client_Id = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
	if(empty($sessionrole)){$sessionrole = $params[3];}	
}	
	//echo $Client_Id.' - '.$UserCode.' - '.$userid.' - '.$sessionrole; 
////////////////////////////////////////////////////////////////////////	
//echo $param;

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

    if($sessionrole=="admin")
    {// -- AND Status_Description !='pending'
      $sql ="SELECT * FROM payments ORDER BY Action_Date AND Status_Description DESC";
      $result = mysqli_query($connect,$sql);
    }
    else{
		// --  WHERE Status_Description!='pending'
      $sql ="SELECT * FROM payments WHERE IntUserCode='$Client_Id'  ORDER BY Action_Date AND Status_Description DESC ";
	  $result = mysqli_query($connect,$sql);
    }
	
?>
<style>
.divider{
    width:5px;
    height:auto;
    display:inline-block;
}

.dataTables_processing
{
	z-index: 105px;
}

</style>

<!-- cdn js  --> 
<script type="text/javascript" src="assets/js/3.3.1/jquery-3.3.1.js"></script>
<script type="text/javascript" src="assets/js/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.bootstrap.min.js"></script>

<!-- cdnjs js -->
<script type="text/javascript" src="assets/js/datatables/jszip.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/vfs_fonts.js"></script>

<!-- cdn js  -->
<script type="text/javascript" src="assets/js/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/buttons.colVis.min.js"></script>
<script type="text/javascript" src="assets/js/datatables/sum%28%29.js"></script>

<div class="container background-white bottom-border">

  <div class='row margin-vert-30'>
  <h2 align="center">Payment History</h2>
<div class="popup" data-popup="popup-1" style="z-index:1000">
    <div class="popup-inner">
        <h2>eMail Proof of payment - History</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
		</div>
		<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>  
<!-- Filter Buttons -->
<div class="portfolio-filter-container margin-top-20">
    <ul class="portfolio-filter">
        <li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            filter by:
        </li>
        <li>
		  <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='My Reference' class='form-control' type='text' value="<?php echo $customerid;?>" />
        </li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            From:
        </li>
        <li>
		 <input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="" />
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            To:
        </li>		
        <li>
         <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="" />
		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            Status:
        </li>		
        <li>
			<SELECT style='width:150px;height:30px' class="form-control" id="status" name="status" size="1" style='z-index:0'>
			<!-------------------- BOC 2017.04.15 -  Payment Method --->
				<OPTION value='' <?php echo $Allstatus;?>>All Status</OPTION>
				<OPTION value='pending' <?php echo $status;?>>Pending</OPTION>			
			    <?php
					$databankresponsecodeSelect = $status;
					foreach($databankresponsecodeArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $databankresponsecodeSelect)
					  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				?>
			<!-------------------- EOC 2017.04.15 - Payment Method. --->
			</SELECT>
		</li>
     </ul>
</div> 
  </div>
<div class="table-responsive" style="z-index:105px">
<table id="collections" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
              <th>Beneficiary Reference</th>
              <th>My Reference</th>			  
              <th>Account Holder</th>
              <th>Account Number</th>
              <th>Branch Code</th>
              <th>Amount</th>
              <th>Action Date</th>
              <th>Status</th>
              <th>Proof of payment</th>		  
          </tr>
      </thead>
    <tfoot>
                  <tr>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
				  <th></th>
                  <th style="text-align:right"></th>
                  <th></th>
                  </tr>
      </tfoot>
      <?php

        while($row = mysqli_fetch_array($result))
        {
			$quote = "'";
			$id = $row['collectionid'];
			$paymentid = urlencode(base64_encode($id));
			$yes = 'yes';
			$yes = urlencode(base64_encode($yes));
			$href = "eproofofpayment.php?paymentid=$paymentid";
			$paramsplit = $quote.$id."|".$row["Amount"]."|".$row["Contact_No"]."|".$row["IntUserCode"]."|".$row["Notify"].
						  "|".$row["Account_Holder"]."|".$row["Client_Reference_1"]."|".$row["Action_Date"].$quote;
		 echo '
          <tr>
            <td width="10%">'.trim($row["Bank_Reference"]).'</td>
            <td width="10%">'.$row["Client_Reference_1"].'</td>			
            <td width="10%">'.$row["Account_Holder"].'</td>
            <td width="10%">'.$row["Account_Number"].'</td>
            <td width="10%">'.$row["Branch_Code"].'</td>
            <td width="5%">'.$row["Amount"].'</td>
            <td width="10%">'.$row["Action_Date"].'</td>
            <td width="10%">'.$row["Status_Description"].'</td>';

			echo '<td width="20%"><a class="btn btn-success" id = "'.$id.'" href="'.$href.'" target="_blank" style="display:inline">'.$text_view.'</a>';
			echo '<div class="divider"></div>';
			if($row["Notify"] == 'EMAIL')
			{echo '<button type="button" id = "Stop'.$id.'" name = "Stop'.$id.'" class="btn btn-info" data-popup-open="popup-1"  onclick="emailproofofpayment('.$paramsplit.')">'.$text_email.'</button></td>';}
			echo '</td>';
			
            echo '</tr>';
        }
      ?>
  </table>
<!-- ======================== Build a Div of eMail URL to sent to Clients ============================ -->
		<div id="divWebsites" hidden>
			<?php 
			$count = count($eMailInvoices); 
			for($i=0;$i<$count;$i++)
				{
				 echo $eMailInvoices[$i];
				}
			?>	
			</div>
<!-- ======================== 2017.04.23 - Build a Div of eMail URL to sent to Clients =============== -->
  
    </div>
    </br>
</div>

<script>

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,
pageResize: true,
//"pageLength": 1000,
"lengthMenu": [ [10, 25, 50, 100,-1], [10, 25, 50,100, "All"] ],
// -- Format the export Excel 'excelHtml5'
// -- https://datatables.net/forums/discussion/34056/excel-column-width
// -- Hide/Remove Columns - https://datatables.net/forums/discussion/43928/exclude-a-column-during-excel-export-in-datatables
// -- https://datatables.net/reference/option/lengthMenu
// -- build In inline style - https://datatables.net/reference/button/excelHtml5
// -- $(col[3]).attr('width', 50);
buttons: [
{ 
	extend: 'excelHtml5', footer: true,header:true,
	title : 'Payment History',
	exportOptions : {
    modifier : {
                    // DataTables core
                    order  : 'index', // 'current', 'applied',
                    //'index', 'original'
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            },
	customize: function( xlsx ) 
	{
		var sheet   = xlsx.xl.worksheets['sheet1.xml'];
		$('row c', sheet).attr( 's', '50' );
        $('row:first c', sheet).attr( 's', '2' );
	    $('row:last c', sheet).attr( 's', '42' );
		var strLast = $('row:last t', sheet).text();
		var res     = '';
		var n 	    = strLast.indexOf("Grand Total");
		var resStr  = strLast.substr(n);
		//if(strLast.includes("Page Total"))
		{
			strLast = strLast.replace("Page Total", "Total");
			res 	= strLast.replace(resStr, "");	
			res 	= res.trim();			
			$('row:last t', sheet).text(res);			
		}
		var amountColLength = res.length;
		if(amountColLength == 0){amountColLength = 0;}
		var col = $('col', sheet);
/* All column col.each(function () {$(this).attr('width', 30);});*/
		 $(col[0]).attr('width', 21);
		 $(col[2]).attr('width', 13);
		 $(col[3]).attr('width', 16);			 
		 $(col[4]).attr('width', 12);	
		 $(col[5]).attr('width', amountColLength);	
		 $(col[6]).attr('width', 11);	
		 
		$('row', sheet).each(function(x) 
        {
		});
	}
// -- pdf to pdfHtml5
 }, 
 {
	 extend: 'pdfHtml5', footer: true,orientation: 'landscape',pageSize: 'LEGAL',
	 title : 'Payment History',
	 exportOptions : {
     modifier : {
                    order  : 'index', // 'current', 'applied',
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            }	
 },'csv', 
 // -- 'print'
 { 
	 extend: 'print',footer: true,orientation: 'landscape',pageSize: 'LEGAL',
	 title : 'Payment History',
	 exportOptions : {
     modifier : {
                    order  : 'index', // 'current', 'applied',
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [0,1, 2,3,4,5,6,7]
            }	
 }],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 5)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 5 ).footer() ).html(
      'Page Total  R'+pageTotal.toFixed(2)+'        '+'    Grand Total R'+ total.toFixed(2)
        );

    },
	initComplete: function()
	{
			$.fn.dataTable.ext.search.push(
			function (settings, data, dataIndex){
				var min = Date.parse( $('#min').val(), 10 );
				var max = Date.parse( $('#max').val(), 10 );
				var Action_Date=Date.parse(data[6]) || 0;

				if ( ( isNaN( min ) && isNaN( max ) ) ||
					 ( isNaN( min ) && Action_Date <= max ) ||
					 ( min <= Action_Date   && isNaN( max ) ) ||
					 ( min <= Action_Date   && Action_Date <= max ) )
				{
				  //  alert("True");
				  //location.reload();
				  $(document).ready();
					return true;

				}
				//alert("false");
				return false;

			}
			);
	}
	
});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function(){ table.draw();});

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

}); 

// #myInput is a <input type="text"> element

</script>

<script>
// -- e-mail an eStatement at a time.
function emailproofofpayment(href) 
{ 
	//alert(href);
    if (href) 
    { 
// -- Format : 
/*$paramsplit = $paymentid."|".
			  $row["Amount"]."|".
			  $row["Contact_No"]."|".
			  $row["IntUserCode"]."|".
			  $row["Notify"]."|".
			  $row["Account_Holder"]."|".
			  $row["Client_Reference_1"]
*/
	  var row = href.split('|');
	  var list =					
		{
		'paymentid':row[0],	
		'Amount'		    :row[1],
		'Contact_No'		:row[2],
		'IntUserCode'		:row[3],
		'UserCode' 			:row[3],
		'Notify'			:row[4],
		'Account_Holder'	:row[5],
		'Client_Reference_1':row[6],
		'Action_Date'		:row[7]};	
 		
	jQuery(document).ready(function()
	{
	jQuery.ajax({	
			type: "POST",
  			url:"script_emailproofofpayment.php",
			data:{list:list},
			success: function(data)
			{ 
			  var Message = "\n<p>Proof of payment was succesfull sent to benefiary</p>";
			  //alert(Message);
			  //alert(data);
			  jQuery("#divSuccess").html(Message);
			  move();
		    }
		});
	});
    }    
}  
// -- end e-mail.

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{

  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() 
  {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>

