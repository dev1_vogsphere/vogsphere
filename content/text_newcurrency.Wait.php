<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_currency="currency"; // Table name 
$currencyidError = null;
$currencyid = '';

$currencynameError = null;
$currencyname = ''; 

$count = 0;

if (!empty($_POST)) 
{   
 $count = 0;
	$currencyid = $_POST['currencyid'];
	$currencyname = $_POST['currencyname'];
	
	$valid = true;
	
	if (empty($currencyname)) { $currencynameError = 'Please enter Currency Description.'; $valid = false;}
	if (empty($currencyid)) { $currencyidError = 'Please enter Currency Symbol(for e.g. $).'; $valid = false;}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								Currency VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	else
	{
			$query = "SELECT * FROM $tbl_currency WHERE currencyid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($currencyid));
			if ($q->rowCount() >= 1)
			{$currencyidError = 'Currency Symbol already in use.'; $valid = false;}
	}
	
// -- Currency 
	if ($valid) 
	{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO  $tbl_currency (currencyid,currencyname) VALUES (?,?)"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($currencyid,$currencyname));
			Database::disconnect();
			$count = $count + 1;
	}
}

?>
<div class="container background-white bottom-border">   
	<div class='row margin-vert-30'>
		   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>

		<form action='' method='POST' class="signup-page"> 
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Currency Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Currency successfully added.</p>");
		}
?>
				
</div>				
	
			<p><b>Currency ID</b><br />
			<input style="height:30px" type='text' name='currencyid' value="<?php echo !empty($currencyid)?$currencyid:'';?>" />
			<?php if (!empty($currencyidError)): ?>
			<span class="help-inline"><?php echo $currencyidError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Currency Name</b><br />
			<input style="height:30px" type='text' name='currencyname' value="<?php echo !empty($currencyname)?$currencyname:'';?>"/>
			<?php if (!empty($currencynameError)): ?>
			<span class="help-inline"><?php echo $currencynameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-success">New Currency</button>
				<a class="btn btn-primary" href="currency">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>