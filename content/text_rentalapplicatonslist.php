<!-- Payment Schedule 2017.05.24 -->
<!----------- Runpaymentschedule.js -----------------> 
<script src="assets/js/Runpaymentschedule.js"></script>
<!-- End Payment Schedule -->

<?php

 //require 'database.php';
 include 'database.php';
 $webpage = "customerLogin.php";
 $lv_username ="root";
 $lv_password = "root";
 $tbl_name="user"; // Table name 
 $tbl_name2="paymentschedule"; // Table name 
 $constrole = 'renter';
 $const_app_table  = 'rentalapp';
 $ExecApproval = '';
 $To = '';
 $To = '';
 $From = '';

			// Id number
			$idnumber = $_SESSION['username'];
			// Title
			$Title = $_SESSION['Title'];	
			// First name
			$FirstName = $_SESSION['FirstName'];
			// Last Name
			$LastName = $_SESSION['LastName'];
			// Phone numbers
			$phone = $_SESSION['phone'];
			// Email
			$email = $_SESSION['email'];
			// Street
			$Street = $_SESSION['Street'];

			// Suburb
			$Suburb = $_SESSION['Suburb'];

			// City
			$City = $_SESSION['City'];
			// State
			$State = $_SESSION['State'];
			
			// PostCode
			$PostCode = $_SESSION['PostCode'];
			
			// Dob
			$Dob = $_SESSION['Dob'];
			
			// phone
			$phone = $_SESSION['phone'];
			
			$icamefrom = "";

			// -- BOC Encrypt 16.09.2017 - Parameter Data
			if(isset($_SERVER['HTTP_REFERER']))
			{	
				$icamefrom = $_SERVER['HTTP_REFERER'];
			}
			// -- EOC Encrypt 16.09.2017 - Parameter Data

if(!function_exists('get_customer'))
{
	function get_customer($customerid)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dataCustomer = null;
			$tbl_2  = 'customer';			

			$sql = "select * from $tbl_2 where CustomerId = ?"; 
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid));
			$dataCustomer = $q->fetch(PDO::FETCH_ASSOC);			  
			
			if(!isset($dataCustomer['CustomerId']))
			{
			  $sql = "select * from $tbl_2 where alias = ?"; 
			  $q = $pdo->prepare($sql);
			  $q->execute(array($customerid));
			  $dataCustomer = $q->fetch(PDO::FETCH_ASSOC);			  				
			}
			
		return $dataCustomer;
	}
}
if(!function_exists('get_rentalproperty'))
{
	function get_rentalproperty($id)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dataProperty = null;
			$tbl_2  = 'rentalproperty';			

			$sql = "select * from $tbl_2 where id = ?"; 
			$q = $pdo->prepare($sql);
			$q->execute(array($id));
			$dataProperty = $q->fetch(PDO::FETCH_ASSOC);			  

		return $dataProperty;
	}
}
if(!function_exists('search_applications'))
{
	function search_applications($filter)
	{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$constrole    = $filter['constrole'];
		$ExecApproval = $filter['ExecApproval'];
		$From 		  = $filter['From'];
		$To = $filter['To'];
		$username = $filter['username'];
		$const_app_table = $filter['const_app_table'];
		$customerid = $filter['customerid'];
		$const_app_table = $filter['const_app_table'];
		$sql = '';
		$data = null;
		$customerObj =  get_customer($username);
		$username    = $customerObj['CustomerId'];
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : Customer 										   //
/////////////////////////////////////////////////////////////////////////////////////////////					   
				if($_SESSION['role'] == $constrole)
				{ 
					   if(!empty($ExecApproval))
					   {
					   if($ExecApproval != "*")
					   {
					   //$data = '';
					     if(!empty($From))
						 {
					     $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE CustomerId = ? AND ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($username,$ExecApproval,$From,$To));
						 
						 }
						 else
						 {
						 $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE CustomerId = ? AND ExecApproval = ? ORDER BY CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($username,$ExecApproval));
						 }
						$data = $q->fetchAll();
					   }
					   else
					   {
						 if(!empty($From))
						 {
							 $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE CustomerId = ? AND DATE(DateAccpt) BETWEEN ? AND ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($username,$From,$To));
						 }
						 else
						 {
						  $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE CustomerId = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($username));				   
						 }
						$data = $q->fetchAll();
												
					   }
					   
					  }
					 }
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					//else if($_SESSION['role'] == 'admin')
					elseif(!empty($_SESSION['role']))	
					{
					  if(!empty($ExecApproval))
					   {
					   if($ExecApproval !== "*") // !== Not Identical Is Not *
					   {
					   					//	echo '<h2> CustomerId = '.$ExecApproval.'</h2>'; 

					   if(!empty($customerid))
						 {
						 
						  if(!empty($From))
						  {
						  $sql = 'SELECT * FROM '.$const_app_table.' WHERE CustomerId = ? AND  ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval,$From,$To));	
						  $data = $q->fetchAll(); 
						  }
						  else
						  {
						  $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE Customerid = ? AND  ExecApproval = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval));	
						  $data = $q->fetchAll();
						  }
						 }
					     else if(!empty($From))
						 {
					     $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($ExecApproval,$From,$To));
						 $data = $q->fetchAll();						 
						 }
						 else
						 {
						  $sql = 'SELECT * FROM '.$const_app_table.' WHERE ExecApproval = ?';
						  $q = $pdo->prepare($sql);
					      $q->execute(array($ExecApproval));							  
						  $data = $q->fetchAll(); 	
						 }
					   }
					   else // -- ExecApproval is *
					   {
						 if(!empty($customerid))
						 {
						 if(!empty($From))
						 {
						  $sql = 'SELECT * FROM '.$const_app_table.' WHERE CustomerId = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$From,$To));	
						  $data = $q->fetchAll(); 
						 }
						 else
						 {
						  $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE Customerid = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid));	
						  $data = $q->fetchAll(); 
						 }
						 }					
						 else if(!empty($From))
						 {
							 $sql = 'SELECT * FROM '.$const_app_table.' as L WHERE DATE(DateAccpt) BETWEEN ? AND ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($From,$To));
							 $data = $q->fetchAll();
						 }
						
						 else
						 {
							// -- All Records
							$sql = 'SELECT * FROM '.$const_app_table.' as L ORDER BY CustomerId DESC';
							$data = $pdo->query($sql);						  
						 }												
					   }
					   
					  }
					}
					return $data;
	}
}
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
{
// keep track validation errors		
		$From = null;
		$To = null;
		$ExecApproval = null;
	    $LastName = null;
		$FirstName = null;
		$customerid = null;
		
// Filter Records
if ( !empty($_POST)) 
{
// Customer role
	if($_SESSION['role'] == $constrole)
	{
		
		$From = $_POST['From'];
		$To = $_POST['To'];
		$ExecApproval = $_POST['ExecApproval'];
	}
  elseif(!empty($_SESSION['role']))// -- Admin & Support Roles
	{

	// keep track post values
		$LastName = $_POST['LastName'];
		$FirstName = $_POST['FirstName'];	
		$customerid = $_POST['customerid'];
		$ExecApproval = $_POST['ExecApproval'];
		$From = $_POST['From'];
		$To = $_POST['To'];
	}
	
}
else
{

// Coming From read & Update.
$ExecApproval = "*";
}

}

// -- BOC Dashboard Staus Selected if it come from the Dashboard.
if (empty($_POST))
{	
	if(isset($_GET['ExecApproval']))
	{
		if ( !empty($_GET['ExecApproval'])) 
		{
				$ExecApproval = $_REQUEST['ExecApproval'];
				$ExecApproval = base64_decode(urldecode($ExecApproval)); 
		}
	}
}
// -- EOC Dashboard -- //

// --  echo "<div class='container background-white'>";
   
   echo "<div class='container background-white bottom-border'> <!-- class='container background-white'> -->";
//<!-- Search Login Box -->

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
	{
	 $Approved = '';
	 $Pending = '';
	 $Cancelled = '';
	 $Rejected = '';
	 $All = '';
	 $Settled = '';
    // -- Defaulter - 20.06.2018.
	 $Defaulted = '';
	 
	// -- Legal & Rehab - 13.12.2018.
	 $REH = '';
	 $LEGAL = '';

	//<?php if ($ExecApproval == 'APPR') echo 'selected'; 
	if($ExecApproval == 'SET')
		{
		  $Settled = 'selected';
		}

	if($ExecApproval == 'APPR')
		{
		  $Approved = 'selected';
		}
	if($ExecApproval == 'PEN')
		{
		   $Pending = 'selected';
		}
	if($ExecApproval == 'CAN')
		{
			$Cancelled = 'selected';
		}
	if($ExecApproval == 'REJ')
		{
		   $Rejected = 'selected';
		}
	if($ExecApproval == 'All')
		{
		   $All = 'selected';
		}

// -- Defaulter - 20.06.2018.		
	if($ExecApproval == 'DEF')
		{
		   $Defaulted = 'selected';
		}	
	
// -- Legal & Rehab - 13.12.2018.
	if($ExecApproval == 'REH')
		{
		   $REH = 'selected';
		}	
		
		if($ExecApproval == 'LEGAL')
		{
		   $LEGAL = 'selected';
		}	
		
	if($ExecApproval == '')
		{
		   $All = 'selected';
		}
		
	  if($To == "")
	   {
		$To = "9999-12-31";
	   }		
	
	
	// Customer role
	if($_SESSION['role'] == $constrole)
	{
		echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2  class='text-center'>Filter Rental Applications</h2>
                                    </div>
									
									<div class='row'>
									 <div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
												<OPTION value='DEF'  $Defaulted>Defaulter</OPTION>
												<OPTION value='REH'  $REH>Rehabilitation</OPTION>
												<OPTION value='LEGAL'  $LEGAL>Legal</OPTION>												
										 </SELECT>
									</div>
									
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value=$To>
                                    </div>
									
                                   </div>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Filter</button>
                                        </div>
                                    </div>                                    
                                </form>
								
								
                            </div>";
	}
  //else 	if($_SESSION['role'] == 'admin')
  elseif(!empty($_SESSION['role']))// -- Admin & Support Roles
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Filter Rental Applications</h2>
                                    </div>
									
									<div class='row'>
									<div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
												<OPTION value='DEF'  $Defaulted>Defaulter</OPTION>	
												<OPTION value='REH'  $REH>Rehabilitation</OPTION>
												<OPTION value='LEGAL'  $LEGAL>Legal</OPTION>												
										 </SELECT>
									</div>
                                    <div class='col-sm-6'>
                                 
                                        <input style='height:30px' name='LastName' placeholder='Last Name' id='LastName' class='form-control margin-bottom-20' type='hidden' value=$LastName>
                                    </div>
                                    <div class='col-sm-6'>
 
 
                                        <input style='height:30px' name='FirstName' placeholder='First Name' id='age' class='form-control margin-bottom-20' type='hidden' value=$FirstName>
                                    </div>
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-50' type='Date' value=$To>
                                    </div>
									
                                   </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span style='height:5px' class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='ID' class='form-control' type='text' value=$customerid >
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  
}							
?>  
            </div> <!-- Search Login Box -->
<!---------------------------------------------- Start Data ----------------------------------------------->
<div> 
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
		              <thead>
							<tr> 
								<th><b>ID</b></th>
								<th>Title</th>
								<th><b>Name</b></th>
								<th><b>Suname</b></th>
								<th><b>Monthly Rental</b></th>
								<th><b>PropertyName</b></th>
								<th><b>PropertyType</b></th>
								<th><b>City</b></th>
								<th><b>Lease Duration</b></th>								
								<th><b>Status</b></th>
								<th>Action</th>
		                </tr>
		              </thead>
		              <tbody>
		              <?php 
					  // include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					   $username = $_SESSION['username'];
					   
					    //-- BOC Check if logged in, if not logged In.
						 if(isset($_SESSION['role']))
						 {
							 // -- Do not redirect.
						 }
						 else
						 {
							 // -- its okay, navigate to login back. 
							 redirect($webpage);
						 }
					   // -- EOC Check if logged in, if not logged In. 
						
					if($_SESSION['role'] != $constrole)
					{	 
						if ( !empty($_POST)) 
						   {
							 $username = $_POST['customerid'];
						   }
					}					   
					   $sql = "";
					   $q = "";
				$filter = array();
				$filter['constrole'] = $constrole;
				$filter['ExecApproval'] = $ExecApproval;
				$filter['From'] 	 = $From;
				$filter['To'] 		    = $To;
				$filter['username'] = $username ;
				$filter['const_app_table'] = $const_app_table ;
				$filter['customerid'] = $customerid;
				$filter['const_app_table'] = $const_app_table;		
				$data = search_applications($filter);
						$display = '';

	 				  foreach ($data as $row) 
					  {

					  // -- Get Customer Data name.
							 $sql = 'SELECT * FROM customer WHERE CustomerId = ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($row['CustomerId']));
							 $dataCustomer = $q->fetchAll();
							 foreach ($dataCustomer as $rowCustomer) 
							 {
								$FirstName = $rowCustomer['FirstName'];
								$LastName = $rowCustomer['LastName'];
								$Title = $rowCustomer['Title'];
							 }
							 
							 $dataProperty = get_rentalproperty($row['property']);
						   		echo '<tr>';
								echo '<td>'. $row['CustomerId'] . '</td>';
							   	echo '<td>'. $Title . '</td>';								
							    echo '<td>'. $FirstName. '</td>';
							   	echo '<td>'. $LastName. '</td>';
							   	echo '<td>R'.$dataProperty['rentalpropertyprice'] . '</td>';
							   	echo '<td>'. $dataProperty['rentalpropertyname']  . '</td>';
							   	echo '<td>'. $dataProperty['rentalpropertytype'] . '</td>';
							   	echo '<td>'. $dataProperty['rentalpropertycity'] . '</td>';								
							   	echo '<td>'. $row['leasedurarion'] . ' Months</td>';								
								// ENUM('APPR', 'PEN', 'CAN', 'REJ')
								if($row['ExecApproval'] == 'APPR')
								{
								echo '<td>Approved</td>';
								}
								if($row['ExecApproval'] == 'PEN')
								{
								echo '<td>Pending</td>';
								}
								if($row['ExecApproval'] == 'CAN')
								{
								echo '<td>Cancelled</td>';
								}
								if($row['ExecApproval'] == 'REJ')
								{
								echo '<td>Rejected</td>';
								}
								if($row['ExecApproval'] == 'SET') // -- Settled
								{
								echo '<td>Settled</td>';
								}
															// -- Defaulter - 20.06.2018.								
								if($row['ExecApproval'] == 'DEF') // -- Defaulted
								{
									echo '<td>Defaulter</td>';
								}

							// -- Legal & Rehabilitation 13.12.2018								
								if($row['ExecApproval'] == 'REH') 
								{
									echo '<td>Rehabilitation</td>';
								}
								if($row['ExecApproval'] == 'LEGAL') 
								{
									echo '<td>Legal</td>';
								}
								
								// -- Encrypt 16.09.2017 - Parameter Data.
								$CustomerIdEncoded = urlencode(base64_encode($row['CustomerId']));
								$ApplicationIdEncoded = urlencode(base64_encode($row['rentalappid']));
								// -- Encrypt 16.09.2017 - Parameter Data.
								//echo $_SESSION['role'];
								// --- if Role = CUSTOMER & Approval status is not pending(Customer cannot cancel it.)	
								if($_SESSION['role'] == $constrole AND $row['ExecApproval'] != 'PEN')
								{								  

										echo '<td width=250>';
										echo '<a class="btn btn-info" href="readrentalapplicaton?customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">Details</a>';
										echo '&nbsp;';
										
										if($row['ExecApproval'] != 'REJ')	
										{
										$repaymentEncoded = '0.00';//urlencode(base64_encode($row['Repayment']));

									/*echo '&nbsp;';	
										echo '<a class="btn btn-info" href="eInvoice.php?repayment='.$repaymentEncoded.'&customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">eStatement Details</a>';
										echo '&nbsp;';											
									*/	
									//	echo '<a class="btn btn-info" href="payment?repayment='.$row['Repayment'].'&customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '">Payment Details</a>';
										echo '</td>';
										
										echo '</tr>';
										}
								}
							elseif($_SESSION['role'] != $constrole AND $_SESSION['role'] != 'Administrator')
								{								  

										echo '<td width=250>';
										echo '<a class="btn btn-info" href="readrentalapplicaton?customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">Details</a>';
										echo '&nbsp;';
										
										if($row['ExecApproval'] != 'REJ')	
										{
										$repaymentEncoded = '0.00';

								echo '&nbsp;';	
								echo '<a class="btn btn-info" href="eInvoice.php?repayment='.$repaymentEncoded.'&customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">eStatement Details</a>';
								echo '&nbsp;';											
										echo '</td>';
										
										echo '</tr>';
										}
								}
								else
								{
								$customername = $Title.' '.$FirstName.' '.$LastName;								
							   	echo '<td width=250>';
								if(getsession('role') != 'renter')
								{echo '<a class="btn btn-info" href="creditreport?customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">Credit Check</a>';}
							   	echo '&nbsp;';
								echo '<a class="btn btn-info" href="readrentalapplicaton?customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">Details</a>';
							   	echo '&nbsp;';
							   	echo '<a class="btn btn-success" href="updaterentalapplicaton?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded.'">Update</a>';
							   	echo '&nbsp;';
							   	echo '<a class="btn btn-danger" href="rentalapplicationcancel?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded.'">Cancel</a>';
								echo '&nbsp;';
							   	echo '</td>';
							   	echo '</tr>';
								}
								
					   } 
					   Database::disconnect();
					  ?>
				      </tbody>
	            </table>
					<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
						<div class="popup-inner">
							<div id="divPaymentSchedules" style="top-margin:100px;font-size: 10px;">
							</div>
							<p><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>
					</div>			
				</div>
    	</div>
    	</div>
		
   		<!---------------------------------------------- End Data ----------------------------------------------->		