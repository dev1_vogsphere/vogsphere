<!-- Start Update the eLoan - Application Data -->
<?php 
	
require 'database.php';
	$customerid = null;
	$ApplicationId = null;
	$count = 0;
	
	if ( !empty($_GET['customerid'])) {
		$customerid = $_REQUEST['customerid'];
	}
	
		if ( !empty($_GET['ApplicationId'])) {
		$ApplicationId = $_REQUEST['ApplicationId'];
	}
	if ( null==$customerid ) {
		//header("Location: custAuthenticate.php");
	}
	
	if ( null==$ApplicationId ) {
		//header("Location: custAuthenticate.php");
	}

if ( !empty($_POST)) 
{

// keep track validation errors - 		// Customer Details
$TitleError = null;
$FirstNameError = null; 
$LastNameError = null;
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$DobError = null;
$phoneError = null;
$emailError = null;
		
// keep track validation errors - 		// Loan Details
$MonthlyExpenditureError = null; 
$TotalAssetsError = null; 
$ReqLoadValueError = null; 
$DateAccptError = null; 
$LoanDurationError = null; 
$StaffIdError = null; 
$InterestRateError = null; 
$ExecApprovalError = null; 
$MonthlyIncomeError = null; 

$RepaymentError = null; 
$paymentmethodError = null; 
$suretyError = null; 
$monthlypaymentError = null; 
$FirstPaymentDateError = null; 
$lastPaymentDateError = null; 

// keep track post values
// Customer Details
$Title = $_POST['Title'];
$FirstName = $_POST['FirstName'];
$LastName = $_POST['LastName'];
$Street = $_POST['Street'];
$Suburb = $_POST['Suburb'];
$City = $_POST['City'];
$State = $_POST['State'];
$PostCode = $_POST['PostCode'];
$Dob		= $_POST['Dob'];
$phone = $_POST['phone'];
$email =  $_POST['email'];

// Loan Details
$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
$TotalAssets  = $_POST['TotalAssets'];
$ReqLoadValue = $_POST['ReqLoadValue'];
$DateAccpt  = $_POST['DateAccpt'];
$LoanDuration  = $_POST['LoanDuration'];
$InterestRate = $_POST['InterestRate'];
$ExecApproval  = $_POST['ExecApproval'];
$MonthlyIncome  = $_POST['MonthlyIncome'];

$Repayment = $_POST['Repayment']; 
$paymentmethod = $_POST['paymentmethod']; 
$surety = $_POST['surety']; 
$monthlypayment = $_POST['monthlypayment']; 
$FirstPaymentDate = $_POST['FirstPaymentDate']; 
$lastPaymentDate = $_POST['lastPaymentDate']; 

// validate input - 		// Customer Details
		$valid = true;
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $nameError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $nameError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $nameError = 'Please enter province'; $valid = false;}
		
		if (empty($PostCode)) { $nameError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter Date of birth'; $valid = false;}
		
				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		if (empty($MonthlyExpenditure)) { $nameError = 'Please enter MonthlyExpenditure'; $valid = false;}
		if (empty($TotalAssets)) { $nameError = 'Please enter Total Assets'; $valid = false;}
		if (empty($ReqLoadValue)) { $nameError = 'Please enter Req Loan Value'; $valid = false;}
		if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		if (empty($DateAccpt)) { $nameError = 'Please enter Date Accpt'; $valid = false;}
		if (empty($LoanDuration)) { $nameError = 'Please enter Loan Duration'; $valid = false;}
		if (empty($InterestRate)) { $nameError = 'Please enter Interest Rate'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($firstpayment)) { $firstpaymentError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'.is_float($MonthlyExpenditure); $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter Required Loan Value'; $valid = false;}
		
		
		
		// update data
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE customer c inner join loanapp l ON c.customerid = l.customerid";
			$sql = $sql." SET Title = ?, FirstName = ?,LastName = ?,Street = ?,Suburb = ?,City = ?,State = ?,PostCode = ?,Dob = ?,";
			$sql = $sql."MonthlyIncome = ?,MonthlyExpenditure = ?,TotalAssets = ?,ReqLoadValue = ?,";
			$sql = $sql."ExecApproval =?,DateAccpt = ?,InterestRate = ?,LoanDuration = ?";
			$sql = $sql." WHERE c.customerid = ? AND l.ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,
			$ReqLoadValue,$ExecApproval,$DateAccpt,$InterestRate,$LoanDuration,$customerid,$ApplicationId));
			Database::disconnect();
			$count++;
		}
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from Customer, LoanApp where Customer.CustomerId = ? AND ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- UserEmail Address
		$sql =  "select * from user where userid= ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
		$dataEmail = $q->fetch(PDO::FETCH_ASSOC);

		// Customer Details

$Title = $data['Title'];
$FirstName = $data['FirstName'];
$LastName = $data['LastName'];
$Street = $data['Street'];
$Suburb = $data['Suburb'];
$City = $data['City'];
$State = $data['State'];
$PostCode = $data['PostCode'];
$Dob		= $data['Dob'];

$phone = $data['phone'];
$email =  $dataEmail['email'];

		// Loan Details
$MonthlyExpenditure = $data['MonthlyExpenditure'];
$TotalAssets  = $data['TotalAssets'];
$ReqLoadValue = $data['ReqLoadValue'];
$DateAccpt  = $data['DateAccpt'];
$LoanDuration  = $data['LoanDuration'];
$InterestRate = $data['InterestRate'];
$ExecApproval  = $data['ExecApproval'];
$MonthlyIncome  = $data['MonthlyIncome'];

$Repayment = $data['Repayment']; 
$paymentmethod = $data['paymentmethod']; 
$surety = $data['surety']; 
$monthlypayment = $data['monthlypayment']; 
$FirstPaymentDate = $data['FirstPaymentDate']; 
$lastPaymentDate = $data['lastPaymentDate']; 		
		
		Database::disconnect();
	}
?>
<!-- End Update the eLoan - Application Data -->

<div class="container background-white bottom-border">   
    <style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background: #e7edee;
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] {
	cursor:pointer;
	width:100%;
	border:none;
	background:#991D57;
	background-image:linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-moz-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-webkit-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	color:#FFF;
	font-weight: bold;
	margin: 20px 0;
	padding: 10px;
	border-radius:5px;
}
input[type="submit"]:hover {
	background-image:linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-moz-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-webkit-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}
</style>
		<div class="row">
 
 <!-- <div class="span10 offset1"> -->
  <form class="login-page" action="update.php?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
    <div class="table-responsive">

					  				<table class=   "table table-user-information">
						<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->		
<tr>
<td>
<div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>
</div>	

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Client information updated successfully!</p>\n", $count);
		}
		?>
				
</td>						
		
</tr>

<tr>						
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label">Title</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="Title" type="text"  placeholder="Title" value="<?php echo !empty($Title)?$Title:'';?>"> -->
			
			<SELECT class="form-control" id="Title" name="Title" size="1">
						<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
						<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
						<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
						<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
						<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
			</SELECT>
			
			<?php if (!empty($TitleError)): ?>
			<span class="help-inline"><?php echo $TitleError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td> 

	<td> 
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label">First Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
			<?php if (!empty($FirstNameError)): ?>
			<span class="help-inline"><?php echo $FirstNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label">Last Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
			<?php if (!empty($LastNameError)): ?>
			<span class="help-inline"><?php echo $LastNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
 
</tr>	

<!-- Street,Suburb,City -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label">Street</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
			<?php if (!empty($StreetError)): ?>
			<span class="help-inline"><?php echo $StreetError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label">Suburb</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
			<?php if (!empty($SuburbError)): ?>
			<span class="help-inline"><?php echo $SuburbError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label">City</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
			<?php if (!empty($CityError)): ?>
			<span class="help-inline"><?php echo $CityError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- State,PostCode,Dob -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label">Province</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="State" type="text"  placeholder="Province" value="<?php echo !empty($State)?$State:'';?>"> -->
			 <SELECT class="form-control" name="State" size="1">
			    <!-- <OPTION value="<?php //echo !empty($State)?$State:'';?>"><?php //echo !empty($State)?$State:'';?></OPTION> -->
				<OPTION value="KZ" <?php if ($State == 'KZ') echo 'selected'; ?>>Kwazulu Natal</OPTION>
				<OPTION value="EC" <?php if ($State == 'EC') echo 'selected'; ?>>Eastern Cape</OPTION>
				<OPTION value="FS" <?php if ($State == 'FS') echo 'selected'; ?>>Free State</OPTION>
				<OPTION value="GP" <?php if ($State == 'GP') echo 'selected'; ?>>Gauteng</OPTION>
				<OPTION value="LP" <?php if ($State == 'LP') echo 'selected'; ?>>Limpopo</OPTION>
				<OPTION value="MP" <?php if ($State == 'MP') echo 'selected'; ?>>Mpumalanga</OPTION>
				<OPTION value="NW" <?php if ($State == 'NW') echo 'selected'; ?>>North West</OPTION>
				<OPTION value="NC" <?php if ($State == 'NC') echo 'selected'; ?>>Northern Cape</OPTION>
				<OPTION value="WC" <?php if ($State == 'WC') echo 'selected'; ?>>Western Cape</OPTION>
			</SELECT>
			<?php if (!empty($StateError)): ?>
			<span class="help-inline"><?php echo $StateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label">Postal Code</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
			<?php if (!empty($PostCodeError)): ?>
			<span class="help-inline"><?php echo $PostCodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label">Date of birth</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  value="<?php echo !empty($Dob)?$Dob:'';?>">
			<?php if (!empty($DobError)): ?>
			<span class="help-inline"><?php echo $DobError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<tr>	
<td>
<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
</td>
<td>
<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">Email
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="email" type="text"  placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
</td>
</tr>	

<!-- Loan Details -->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->	
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Loan Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label">Monthly Income</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="MonthlyIncome" type="text"  placeholder="Monthly Income" value="<?php echo !empty($MonthlyIncome)?$MonthlyIncome:'';?>">
			<?php if (!empty($MonthlyIncomeError)): ?>
			<span class="help-inline"><?php echo $MonthlyIncomeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label">Monthly Expenditure</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="MonthlyExpenditure" type="text"  placeholder="Monthly Expenditure" value="<?php echo !empty($MonthlyExpenditure)?$MonthlyExpenditure:'';?>">
			<?php if (!empty($MonthlyExpenditureError)): ?>
			<span class="help-inline"><?php echo $MonthlyExpenditureError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label">Total Assets</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="TotalAssets" type="text"  placeholder="Total Assets" value="<?php echo !empty($TotalAssets)?$TotalAssets:'';?>">
			<?php if (!empty($TotalAssetsError)): ?>
			<span class="help-inline"><?php echo $TotalAssetsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- ReqLoadValue,DateAccpt,LoanDuration -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label">Requested Loan Value</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="ReqLoadValue" type="text"  placeholder="Req Loan Value" value="<?php echo !empty($ReqLoadValue)?$ReqLoadValue:'';?>">
			<?php if (!empty($ReqLoadValueError)): ?>
			<span class="help-inline"><?php echo $ReqLoadValueError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label">Date Accepted</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="DateAccpt" type="Date" value="<?php echo !empty($DateAccpt)?$DateAccpt:'';?>">
			<?php if (!empty($DateAccptError)): ?>
			<span class="help-inline"><?php echo $DateAccptError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label">Loan Duration</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LoanDuration" type="text"  placeholder="Loan Duration" value="<?php echo !empty($LoanDuration)?$LoanDuration:'';?>">
			<?php if (!empty($LoanDurationError)): ?>
			<span class="help-inline"><?php echo $LoanDurationError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	
<!-- Surety, Repayment, Monthly Payment -->
<tr>	
	<td>
		<div class="control-group <?php echo !empty($SuretyError)?'error':'';?>">
											<label class="control-label">Surety Assets(e.g. Golf GTi , iPhone 5)</label>
											<div class="controls">
												<input  style="height:30px" class="form-control margin-bottom-10" id="Surety" name="Surety" type="text"  placeholder="Surety Assets(e.g. Golf GTi , iPhone 5)" value="<?php echo !empty($Surety)?$Surety:'';?>">
												<?php if (!empty($SuretyError)): ?>
												<span class="help-inline"><?php echo $SuretyError;?></span>
												<?php endif; ?>
											</div>
		</div>
	</td>
	<td>
	 <label>Total Repayment Amount
      </label>
		<div class = "input-group">
			<span class = "input-group-addon">R</span>	  
             <input  style="height:30px" readonly id="Repayment" name="Repayment" value="" class="form-control margin-bottom-10" type="text">
		</div>
	</td>
	<td>
		<label>Monthly Payment Amount
           </label>
			 <div class = "input-group">
			  <span class = "input-group-addon">R</span>	  
                 <input  style="height:30px" readonly id="monthlypayment" name="monthlypayment" value="" class="form-control margin-bottom-10" type="text">
			 </div>
	</td>
	
</tr>	
<!-- InterestRate,ExecApproval -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($InterestRateError)?'error':'';?>">
		<label class="control-label">Interest Rate(%)</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="InterestRate" type="text"  placeholder="Interest Rate" value="<?php echo !empty($InterestRate)?$InterestRate:'';?>">
			<?php if (!empty($InterestRateError)): ?>
			<span class="help-inline"><?php echo $InterestRateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label">Status</label>
		 <SELECT class="form-control" name="ExecApproval" size="1">
				<OPTION value="APPR" <?php if ($ExecApproval == 'APPR') echo 'selected'; ?>>Approved</OPTION>
				<OPTION value="REJ" <?php if ($ExecApproval == 'REJ') echo 'selected'; ?>>Rejected</OPTION>
				<OPTION value="PEN" <?php if ($ExecApproval == 'PEN') echo 'selected'; ?>>Pending</OPTION>
				<OPTION value="CAN" <?php if ($ExecApproval == 'CAN') echo 'selected'; ?>>Cancelled</OPTION>
		 </SELECT>
	</div>
	</td>
</tr>	
<!-- Buttons -->
						<tr>
							<td><div class="form-actions">
							  <button type="submit" class="btn btn-success">Update</button>
						      <a class="btn btn-primary" href="custAuthenticate.php">Back</a>
							</div></td>
						</tr>
						</table>
						</div>
					</form>
				</div>
				
    </div> <!-- /container -->