CREATE TABLE `eloan`.`loantype` (
  `loantypeid` INT NOT NULL,
  `loantypedesc` ENUM('Personal Loan') NULL,
  `percentage` INT NULL,
  PRIMARY KEY (`loantypeid`));

ALTER TABLE `eloan`.`loantype` 
CHANGE COLUMN `loantypeid` `loantypeid` INT(11) NOT NULL AUTO_INCREMENT ;
