<?php 
require 'database.php';

$count = 0;
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $userid 			= '';
// -- Read SESSION userid.
// -- Logged User Details.
if(isset($_SESSION['userid']))
{
	$userid = $_SESSION['username'];
}
 $status = '';
	 $open = '';
	 $closed = '';
	 $finalised = '';

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 
  
//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);
 
//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM customer';
$dataCustomers = $pdo->query($sql);		
 
// BOC -- 15.04.2017 -- Updated.
$tbl_name = 'customer';

// -- Personal Details
$CustomerId = '';
$Title 		= '';
$FirstName  = '';
$LastName 	= '';
$Dob 		= '';
$refnumber  = '';

// -- Contact Details.
$phone 		= '';
$refnumber  = '';
$backLinkName = 'customers';

// -- Residential Address.
$Street 	= '';
$Suburb 	= '';
$City 		= '';
$PostCode 	= '';

// -- Banking Details.
$AccountHolder = '';
$AccountType   = '';
$AccountNumber = '';
$BranchCode    = '';
$BankName      = '';

// -- Required fields
$AccountHolderError ='';
$AccountNumberError	='';
$BranchCodeError	='';
$bankError			='';
$phoneError			='';
$FirstNameError 	= '';
$LastNameError 		= '';
$tbl_customer 		= 'customer';
$valid = true;
$email = '';
$message = '';

// -- CustomerId.
if (isset($_GET['CustomerId']) ) 
{ 
	$CustomerId = $_GET['CustomerId']; 
}

// -- Post.
if (!empty($_POST)) 
{
		$count = 0;
// -- Personal Details
		$Title = $_POST['Title'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$Dob		= $_POST['Dob'];
		$idnumber = $_POST['idnumber'];
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
// -- Contact Details
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		// validate e-mail
		if (!empty($email)){if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}}
		if (empty($phone)) { $phoneError = 'Please enter cell phone number'; $valid = false;}

// -- Residential Address
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		$status = $_POST['status'];
		
// -- Banking Details.
		$BranchCode = $_POST['BranchCode'];
		$BankName = $_POST['BankName'];
		$AccountHolder = $_POST['AccountHolder'];
		$AccountNumber = $_POST['AccountNumber'];
		$AccountType = $_POST['AccountType'];
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}

		if ($valid) 
		{
			$number = $dataCustomers->rowCount() + 1;
			$size = 13;

			if(empty($idnumber))
			{
				$refnumber = number_pad($number,$size);
				$idnumber = $refnumber;
			}
			
			if(empty($Dob))
			{
				$Dob = '9999-01-01';
			}
			
			// --- Customer Details
			$sql = "UPDATE $tbl_customer SET Title = ?,FirstName = ?,LastName = ?,Street = ?,Suburb = ?,City = ?,State = ?,PostCode = ?,Dob = ?,phone = ?,changedby = ?,
					accountholdername = ?,
					bankname = ?,
					accountnumber = ?,
					branchcode = ?,
					accounttype = ?,email2 = ?,changedon = ?,status = ? WHERE CustomerId = ?";

			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone,$userid,
			$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$email,date('Y-m-d'),$status,$idnumber));
			$count = $count + 1;
			$message = $idnumber;
			Database::disconnect();
		}
}
else
{
// -- All CustomerID
	$sql = "SELECT * FROM $tbl_name WHERE CustomerId = ?";
    $q = $pdo->prepare($sql);
	$q->execute(array($CustomerId));	
	$data = $q->fetch(PDO::FETCH_ASSOC);

// -- Personal Details
		$Title     = $data['Title'];
		$FirstName = $data['FirstName'];
		$LastName  = $data['LastName'];
		$Dob	   = $data['Dob'];
		$idnumber  = $data['CustomerId'];		
// -- Contact Details
		$phone     = $data['phone'];
		$email     = $data['email2'];
	
// -- Residential Address
		$Street   = $data['Street'];
		$Suburb   = $data['Suburb'];
		$City     = $data['City'];
		$State    = $data['State'];
		$PostCode = $data['PostCode'];
		
// -- Banking Details.
		$BranchCode 	= $data['branchcode'];
		$BankName 		= $data['bankname'];
		$AccountHolder  = $data['accountholdername'];
		$AccountNumber  = $data['accountnumber'];
		$AccountType    = $data['accounttype'];
		$status = $data['status'];
// -- Default Bankname - ABSA
  $BankName = "ABSA";
    
}

if($status == 'open')
		{
		  $open = 'selected';
		}
		
		if($status == 'closed')
		{
		  $closed = 'selected';
		}
		
		if($status == 'finalised')
		{
		  $finalised = 'selected';
		}

// -- Generate Reference Number.
function number_pad($number,$n) 
{
	return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
}


?>
<div class='container background-white bottom-border'>		
  <div class='row margin-vert-30'>
	<div class="col-xs-12 col-sm-12">
			<div class="box-content">
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
				<?php # error messages
					//if (isset($message)) 
					{
						//foreach ($message as $msg) 
						//{
						//	printf("<p class='status'>%s</p></ br>\n", $msg);
						//}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Customer successfully updated.Reference Number :$message</p>");
					}
			?>
				<h4 class="page-header">Pesonal Information Details</h4>
				<div class="form-group">
						<label class="col-sm-2 control-label">Title</label>
						<div class="col-sm-4">
							<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
							</SELECT>
						</div>

						<label class="col-sm-2 control-label">ID/Passport</label>
						<div class="col-sm-4">
										<input class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()" readonly>
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
						</div>
					</div>
					
				<div class="form-group <?php if(!empty($FirstNameError)){ echo "has-error";} if(!empty($LastNameError) && empty($FirstNameError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">First name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
								<?php if (!empty($FirstNameError)): ?>
								<small class="help-block" style=""><?php echo $FirstNameError; ?></small>								
								<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Last name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
							<?php if (!empty($LastNameError)): ?>
								<small class="help-block" style=""><?php echo $LastNameError; ?></small>								
							<?php endif; ?>	
						</div>
					</div>
				<div class="form-group <?php if (!empty($DobError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Date of birth</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<small class="help-block" style=""><?php echo $DobError; ?></small>																	
									<?php endif; ?>
						</div>
					</div>
					<h4 class="page-header">Contact Details</h4>
					<div class="form-group <?php if (!empty($phoneError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<small class="help-block" style=""><?php echo $phoneError; ?></small>																					
										<?php endif; ?>

							</div>
						<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<small class="help-block" style=""><?php echo $emailError; ?></small>																													
										<?php endif; ?>
							</div>
					</div>

					<h4 class="page-header">Residential Address</h4>

					<div class="form-group <?php if (!empty($StreetError)){ echo "has-error";} if (!empty($SuburbError) && empty($StreetError)){ echo "has-error";} ?>">
						<label class="col-sm-2 control-label">Street</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<small class="help-block" style=""><?php echo $StreetError; ?></small>																																							
										<?php endif; ?>

						</div>
						<label class="col-sm-2 control-label">Suburb</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<small class="help-block" style=""><?php echo $SuburbError; ?></small>																																																		
											<?php endif; ?>

						</div>
					</div>
					<div class="form-group <?php if (!empty($CityError)){ echo "has-error";}?>">
					<label class="col-sm-2 control-label">City/Town/Township</label>
						<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<small class="help-block" style=""><?php echo $CityError; ?></small>																																																		
										<?php endif; ?>
						</div>
						
						<label class="col-sm-2 control-label">Province</label>
						<div class="col-sm-4">
						<SELECT class="form-control" id="State" name="State" size="1">
									<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->
								</SELECT>

						</div>
						
					</div>
					<div class="form-group <?php if (!empty($PostCodeError)){ echo "has-error";}?>">
					<label class="col-sm-2 control-label">Postal Code</label>
						<div class="col-sm-2">
							<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
							<?php if (!empty($PostCodeError)): ?>
								<small class="help-block" style=""><?php echo $PostCodeError; ?></small>																																																		
							<?php endif; ?>
						</div>
					</div>	
				<h4 class="page-header">Banking Details</h4>
					<div class="form-group <?php if (!empty($AccountHolderError)){ echo "has-error";}?>">					
						<label class="col-sm-2">Account Holder Name</label>
						<div class="col-sm-4">
						<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
							<?php if (!empty($AccountHolderError)): ?>
								<small class="help-block" style=""><?php echo $AccountHolderError; ?></small>																																																												
							<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Bank Name</label>						
						<div class="col-sm-4">
							<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
						</div>
					</div>
					<div class="form-group <?php if (!empty($AccountNumberError)){ echo "has-error";}?>">					
						<label class="col-sm-2 control-label">Account Number</label>
						<div class="col-sm-4">
						 <input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />		
							<?php if (!empty($AccountNumberError)): ?>
								<small class="help-block" style=""><?php echo $AccountNumberError; ?></small>																																																												
							<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Account Type</label>						
						<div class="col-sm-4">
							<SELECT class="form-control" id="AccountType" name="AccountType" size="1" >
								<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Account type --->
							</SELECT>
						</div>
					</div>
					<div class="form-group"> <!-- has-error has-feedback"> -->
						<label class="col-sm-2 control-label">Branch Code</label>
						<div class="col-sm-4">
							<SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
						</div>
					</div>
				<?php echo "<br/><div class='form-group'> 
						<label class='col-sm-2 control-label'>Lead Status</label>
						<div class='col-sm-4'>
										 <SELECT class='form-control' id='status' name='status' size='1'>
												<OPTION value='open' $open>Open</OPTION>
												<OPTION value='closed'  $closed>Closed</OPTION>
												<OPTION value='finalised'  $finalised>Finalised</OPTION>
										 </SELECT>
									</div></div>";	?>
									
				<h4 class="page-header"></h4>					
	
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-2">
							<!--<a type="cancel" class="btn btn-default btn-label-left">
							<span><i class="fa fa-clock-o txt-danger"></i></span>
								Cancel
							</button> -->
							  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->

						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-primary btn-label-left">
							
							  <!-- <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button> -->

							
							<span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						</div>
					</div>
				</form>
			</div>
	</div>
  </div>
</div>
<script type="text/javascript">
/*
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
*/

// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

</script>
