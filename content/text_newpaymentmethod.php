<?php 
require 'database.php';
$paymentmethoddescError = null;
$paymentmethoddesc = '';
$count = 0;
$paymentmethodid = 0;

if (!empty($_POST)) 
{   $count = 0;
	$paymentmethoddesc = $_POST['paymentmethoddesc'];
	$paymentmethodid = $_POST['paymentmethodid'];
	
	$valid = true;
	
	if (empty($paymentmethodid)) { $paymentmethodidError = 'Please enter Payment Method ID.'; $valid = false;}
	if (empty($paymentmethoddesc)) { $paymentmethoddescError = 'Please enter Payment Method Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO paymentmethod (paymentmethodid,paymentmethoddesc) VALUES(?,?)"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($paymentmethodid,$paymentmethoddesc));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">  
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">  
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Payment Method Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Payment Method successfully added.</p>");
		}
?>
				
</div>				
			<p><b>Payment Method ID</b><br />
			<input style="height:30px" type='text' name='paymentmethodid' value="<?php echo !empty($paymentmethodid)?$paymentmethodid:'';?>"/>
			<?php if (!empty($paymentmethodidError)): ?>
			<span class="help-inline"><?php echo $paymentmethodidError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Payment Method Description</b><br />
			<input style="height:30px" type='text' name='paymentmethoddesc' value="<?php echo !empty($paymentmethoddesc)?$paymentmethoddesc:'';?>"/>
			<?php if (!empty($paymentmethoddescError)): ?>
			<span class="help-inline"><?php echo $paymentmethoddescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'paymentmethod';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
