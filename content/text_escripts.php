<?php 
	 require 'database.php';
	 $tbl_name = 'escripts';
	 $data     = null;
	 $pdo = Database::connect();
	 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 if(getsession('role') == 'admin')
	 {

		// -- All Records
		$sql = "SELECT * FROM $tbl_name";
		$q = $pdo->prepare($sql);
		$q->execute();	
		$data = $q->fetchAll(); 
	 }				

?>
<div class='container background-white bottom-border'>
 <div class="row margin-vert-30">
    <div class="col-md-12">
      <h2 class="margin-bottom-10">Scripts</h2>
		<hr class="margin-bottom-30">
          <div class="row">
			<?php 
			foreach($data as $row)
			{ 
				foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
				echo '<div class="col-md-6"><div class="row"><div class="col-sm-4">'; 
				echo '<span class="fa-stack fa-2x margin-vert-30 margin-horiz-40 hidden-xs animate fadeInLeft">';
				echo '<i class="fa fa-circle fa-stack-2x color-green"></i>';	
				echo '<a href='. nl2br( $row['name_path']) .' target="_blank"><i class="fa fa-cogs fa-stack-1x fa-inverse color-white"></i></a></span></div>';
				echo '<div class="col-sm-8">';
				echo '<a href='. nl2br( $row['name_path']) .' target="_blank"><h3 class="margin-vert-10">'.nl2br( $row['name_path']).'</h3></a>';
				echo '<p>'.nl2br( $row['description']).'</p>';
				echo '<p>Cronjob Status : '.nl2br( $row['cron_jobstatus']).'</p>';
				echo '</div></div>';
				echo '</div>'; 	
			} 
			   Database::disconnect();
			?>
		 </div>
    </div>
       <hr class="margin-top-40">
</div>
</div>
</div>
