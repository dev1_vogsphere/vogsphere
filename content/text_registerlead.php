<?php 
require 'database.php';
$backLinkName = 'leads';
$userid 			= '';
$status = 'open';
// -- Read SESSION userid.
// -- Logged User Details.
if(isset($_SESSION['username']))
{
	$userid = $_SESSION['username'];
}
//echo $userid;

 $status = '';
	 $open = '';
	 $closed = '';
	 $finalised = '';

//1. Judgements
	 $Judgements = '';

//2. Defaults
	 $Defaults = '';

//3. Notices
	$Notices = '';

//4. Tracealerts
	$Tracealerts = '';

//5. Paymentprofile
$Paymentprofile = '';

//6. Enquires
$Enquires = '';

	$crrolesarray[1] = "'crmanager'";
	$crrolesarray[2] = "'crconsultant'";
	$cragents = GetCRAgents($crrolesarray);
		$whereArr = array();

	$agents = implode(",",$cragents);
	$whereArr[] = "CustomerId IN ({$agents})";
	$dataAgents = search_dynamic_customers($whereArr);


$count = 0;
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 
//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM customer';
$dataCustomers = $pdo->query($sql);		
 
// BOC -- 15.04.2017 -- Updated.

// -- Personal Details
$Title 		= '';
$FirstName  = '';
$LastName 	= '';
$refnumber  = '';
$customerid = '';
$customeridError = '';

// -- Contact Details.
$phone 		= '';

$phoneError			='';
$FirstNameError 	= '';
$LastNameError 		= '';
$tbl_lead 		= 'lead';
$valid = true;
$email = '';
$message = '';
$idnumberError = '';
$emailError = '';
$phoneError = '';

$tbl				= "tenantcustomers"; // Table name 
$customerError		= '';

$agentid = '';
$comments  = '';

if (!empty($_POST)) 
{
	
$comments  	= $_POST['comments'];
if(isset($_POST['agentid']))
{$agentid = $_POST['agentid'];}

 $status = $_POST['status'];
	
	$count = 0;
// -- Personal Details
		$Title = $_POST['Title'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		$customerid = getpost('customerid');
		
// -- Contact Details
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		// validate e-mail
		if (!empty($email)){if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}}
		
		if (empty($phone)){if(empty($email)){$phoneError = 'Please enter either cell phone number and/or email'; $valid = false;}else{}}

// -- Communication Error.
if (!empty($phone)) {CheckCommunication($phone,$email);}		

if (!empty($email)) {CheckCommunication($phone,$email);}	

// -- Check if customer exist.
if(!empty($customerid))
{
	$queryArr = array();
	$queryArr[] = "CustomerId = {$customerid}";
	if(!empty(GetCustomerDetails($queryArr)))
	{
	  $customeridError = 'CustomerId already exist in the system as customer.'; $valid = false;	
	}
	if(empty($customeridError))
	{
		$queryArr = null;
		$queryArr[] = "customerid = {$customerid}";
		if(search_dynamic_leads($queryArr)->rowCount() > 0)
		{
		  $customeridError = 'CustomerId already exist in the system as lead.'; $valid = false;	
		}
	}
}
		
// -- Phone..		
if(!empty($phoneError)){$valid = false;}

// -- email
if(!empty($emailError)){$valid = false;}
		
		if ($valid) 
		{
		// --- Customer Details
			$sql = "INSERT INTO $tbl_lead (title,name,surname,status,phone,email,comments,createdby,createdon,agentid,assignedon,customerid)";
			$today = date('Y-m-d');
			
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$status,$phone,$email,$comments,$userid,$today,$agentid,$today,$customerid));      
		
			$message = $Title.' '.$FirstName.' '.$LastName;
			$count = 1; 
			Database::disconnect();
		}
}
?>
<div class='container background-white bottom-border'>		
<div class='row margin-vert-30'>
	<div class="col-xs-12 col-sm-12">
		<!-- <div class="box"> -->
<!--			<div class="box-content">-->
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
					<!-- <div class="box-header">
						<div class="box-name">
							<span>Registration form</span>
						</div>
					</div> -->
				<?php # error messages
					if (isset($message)) 
					{
						//foreach ($message as $msg) 
						{
							//printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Lead $message was successfully created.</p>");
					}
			   ?>
				<h4 class="page-header">Pesonal Information Details</h4>
				<div class="form-group <?php if (!empty($idnumberError)){ echo "has-error";} if(!empty($customeridError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Title</label>
						<div class="col-sm-4">
							<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
							</SELECT>
						</div>
						<label class="col-sm-2 control-label">Customer ID</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="customerid" name="customerid" type="text"  placeholder="customerid" value="<?php echo !empty($customerid)?$customerid:'';?>">
								<?php if (!empty($customeridError)): ?>
								<small class="help-block" style=""><?php echo $customeridError; ?></small>
								<?php endif; ?>
						</div>

				</div>
				<br/>	
				<div class="form-group <?php if(!empty($FirstNameError)){ echo "has-error";} if(!empty($LastNameError) && empty($FirstNameError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">First name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
								<?php if (!empty($FirstNameError)): ?>
								<small class="help-block" style=""><?php echo $FirstNameError; ?></small>
								<?php endif; ?>
						</div>
						<label class="col-sm-2 control-label">Last name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
							<?php if (!empty($LastNameError)): ?>
										<small class="help-block" style=""><?php echo $LastNameError; ?></small>	
							<?php endif; ?>	
						</div>
				</div>
			 <h4 class="page-header">Contact Details</h4>
					<div class="form-group <?php if (!empty($phoneError)){ echo "has-error";} if (!empty($emailError)){ echo "has-error";}?>">
						<label class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<small class="help-block" style=""><?php echo $phoneError; ?></small>	
										<?php endif; ?>
							</div>
						<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<small class="help-block" style=""><?php echo $emailError; ?></small>											
										<?php endif; ?>
							</div>
					</div>
	<h4 class="page-header">Agent Details</h4>	
				<div class="form-group <?php if (!empty($AccountHolderError)){ echo "has-error";}?>">					
				<?php echo " 
						<label class='col-sm-2 control-label'>Lead Status</label>
						<div class='col-sm-4'>
										 <SELECT class='form-control' id='status' name='status' size='1'>
												<OPTION value='open' $open>Open</OPTION>
												<OPTION value='closed'  $closed>Closed</OPTION>
												<OPTION value='Judgements'  $Judgements>Judgements</OPTION>
												<OPTION value='Defaults'  $Defaults>Defaults</OPTION>
												<OPTION value='Notices'  $Notices>Notices</OPTION>
												<OPTION value='Tracealerts'  $Tracealerts>Trace Alerts</OPTION>
												<OPTION value='Paymentprofile'  $Paymentprofile>Payment profile</OPTION>
												<OPTION value='Enquires'  $Enquires>Enquires</OPTION>											
												<OPTION value='finalised'  $finalised>Finalised</OPTION>
										 </SELECT>
									</div>";	?>
				<?php   if($_SESSION['role'] != 'crconsultant'){?>
				<label class="col-sm-2 control-label">Assign Agent</label>
						<div class="col-sm-4">
							<SELECT class="form-control" id="agentid" name="agentid" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$agentid = '';
											$agentSelect = $agentid;
											$agentname = '';
											foreach($dataAgents as $row)
											{
											  // -- Selected Agent.
												$agentname = $row['FirstName']." ".$row['LastName'];
												$agentid = $row['CustomerId'];
													
												// -- Select the Selected Bank 
											  if($agentid == $agentid)
											   {
													echo "<OPTION value=$agentid selected>$agentid - $agentname</OPTION>";
											   }
											   else
											   {
													 echo "<OPTION value=$agentid>$agentid - $agentname</OPTION>";
											   }
										
											if(empty($dataAgents))
											{
												echo "<OPTION value=0>No Agents</OPTION>";
											}
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
							</SELECT>
						</div>
				<?php }?>	
				</div>
				<br/>
				<div class="form-group">
					<label class="col-sm-2 control-label">comments</label>
						<!--<div class="col-sm-2">
						</div>-->
					<div class="col-sm-10">	
						<textarea class="form-control" rows="3" name="comments" id="comments" placeholder="comments"><?php echo $comments;?></textarea>	
					</div>
				</div>		
				
				<h4 class="page-header"></h4>					
	
					<div class="clearfix"></div>
					<div class="form-group">
												<div class="col-sm-offset-2 col-sm-2">
							<!--<a type="cancel" class="btn btn-default btn-label-left">
							<span><i class="fa fa-clock-o txt-danger"></i></span>
								Cancel
							</button> -->
							  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->

						</div>

						<div class="col-sm-2">
							<button type="submit" class="btn btn-primary btn-label-left">
							
							  <!-- <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button> -->

							
							<span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						</div>
					</div>
				</form>
			<!-- </div> 298 -->
		</div>
	</div>
</div>
<script type="text/javascript">
/*
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
*/

// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

</script>
