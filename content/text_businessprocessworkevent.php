<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="process_event"; // Table name 
 $role = '';
  $EV_PROCNAME = '';
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Event by Process Name</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='procname' name='procname' placeholder='business process name' class='form-control' type='text' value=$EV_PROCNAME>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $role = $_POST['role'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($role))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE EV_PROCNAME = ? ORDER BY EV_NAME";
						$q = $pdo->prepare($sql);
					    $q->execute(array($role));	
						$data = $q->fetchAll();  
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name ORDER BY EV_NAME";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newbusinessprocessworkevent.php>Add New Event</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Event Name</b></td>"; 
						echo "<td><b>Business Process</b></td>"; 
						//echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ $EV_NAME = '';
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						
						echo "<tr>";  						
						echo "<td valign='top'>" . nl2br( $row['EV_NAME']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['EV_PROCNAME']) . "</td>";  
						$EV_NAME = $row['EV_NAME'];
						/* -- Logout must never be deleted.
							//if(nl2br( $row['label']) == 'Logout')
							{
							}
							//else
							{
								echo "<td valign='top'><a class='btn btn-success' href=editaccess.php?accessid={$row['EV_NAME']}>Edit</a></td>";

								echo "<td valign='top'>
								
								<a class='btn btn-danger' href=deleteaccess.php?accessid={$row['EV_NAME']}>Delete</a></td> "; 
								echo "</tr>";
							}*/
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->
	    	</div>
	