<?php 
	require 'database.php';
	
	$customerid = null;
	$ApplicationId = null;
	
	// -- BOC Encrypt 16.09.2017.
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$dbname = "eloan";
	// -- EOC Encrypt 16.09.2017.

	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}
	
		if ( !empty($_GET['ApplicationId'])) {
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.		
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
	if ( !empty($_GET['customerid'])) {
		$id = $_REQUEST['customerid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$customerid = $_POST['customerid'];
		$ApplicationId = $_POST['ApplicationId'];
		$_SESSION['ApplicationId'] = $ApplicationId;

		$ExecApproval = 'CAN';
		$DateAccpt = date("Y-m-d");
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE loanapp";
			$sql = $sql." SET ExecApproval = ?, DateAccpt = ?";
			$sql = $sql." WHERE customerid = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ExecApproval,$DateAccpt,$customerid,$ApplicationId));
			Database::disconnect();
		// -- Email send.
			SendEmailpage('content/text_emailLoanStatusCancelled.php');			
			echo("<script>location.href = 'custAuthenticate.php';</script>");
		}
	   // -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	  else 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid,$ApplicationId));		
			$data = $q->fetch(PDO::FETCH_ASSOC);
			
			// -- Get the user details.
			$sql =  "select * from user where userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid));		
			$dataUser = $q->fetch(PDO::FETCH_ASSOC);
			$_SESSION['email'] = $dataUser['email'];
			$_SESSION['FirstName'] = $data['FirstName'];
			$_SESSION['LastName'] = $data['LastName'];
			$_SESSION['Title'] = $data['Title'];

			//print_r($dataUser);echo "<br/>";
			//print_r($data);
			if(empty($data))
			{
				header("Location: custAuthenticate");
			}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}	
?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Cancel a loan application
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="customerid" value="<?php echo $customerid;?>"/>
					  <input type="hidden" name="ApplicationId" value="<?php echo $ApplicationId;?>"/>
					  <p class="alert alert-error">Are you sure to cancel ?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="custAuthenticate.php">No</a>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
