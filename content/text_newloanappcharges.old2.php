<?php 
require 'database.php';
$chargeid = 0;
$applicationid = 0;
$item = 0;
$count = 0;

$applicationidError = null;
$itemError = null;

// -- Globa Setting.
$currency = $_SESSION['currency'];

// --------------------- BOC 2017.04.15 ------------------------- //
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Loan Application Charges ------------------- //
  $sql = 'SELECT * FROM charge ORDER BY chargename';
  $dataCharges = $pdo->query($sql);		

  $sql = 'SELECT * FROM charge ORDER BY chargename';
  $dataChargesArr = $pdo->query($sql);		
  
//2. --------------------- Loan Application ------------------- //
  $sql = 'SELECT * FROM loanapp';
  $dataLoanApplications = $pdo->query($sql);	
  
//3. -- Get Customer Data name.
  $sql = 'SELECT * FROM customer';
  $dataCustomers = $pdo->query($sql);	

  // -- Customers 
  $ArrayCustomers = null;
  $indexInside = 0;
  
   // -- Items 
  $ArrayItems = null;
  $indexItems = 0;
  
  // -- Charges
  $ArraCharge = null;
  $indexCharges = 0;												
  $message = '';
  
  //1. -- Customers - Execute Once.
	foreach($dataCustomers as $rowCustomer)
	{
	    $ArrayCustomers[$indexInside][0] = $rowCustomer['CustomerId'];
		$ArrayCustomers[$indexInside][1] = $rowCustomer['Title'];		
		$ArrayCustomers[$indexInside][2] = $rowCustomer['FirstName'];
		$ArrayCustomers[$indexInside][3] = $rowCustomer['LastName'];
		$indexInside = $indexInside + 1;
	}
	$indexInside = 0;
	
	//2. -- dataChargesArr - Execute Once.
	foreach($dataChargesArr as $rowChargeArr)
	{
		$ArraCharge[$indexInside][0] = $rowChargeArr['chargeid'];
		$ArraCharge[$indexInside][1] = $rowChargeArr['chargename'];
		$indexInside = $indexInside + 1;
	}
	$indexInside = 0;
  
  Database::disconnect();	
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

if (!empty($_POST)) 
{   $count = 0;
	$chargeid = $_POST['chargeid'];
	$applicationid = $_POST['applicationid'];
	$item = $_POST['item'];
	
	$ArrayItems = $_POST['itemArray'];
	
	$valid = true;
//  echo '<h2>'.$item." ".$chargeid.'</h2>';

		// -- All Items and All Charges. 
		if($item == '' AND $chargeid == '')
		{
		// -- Array Items
				for ($i=0;$i<sizeof($ArrayItems);$i++)
				{
					// -- Array Charges.
					for ($j=0;$j<sizeof($ArraCharge);$j++)
					{
						// -- ChargeID and Item.
						$chargeid = $ArraCharge[$j][0];
						$item = $ArrayItems[$i];
						$message = "Item :".$item." AND Charge ID :".$chargeid." ";

						// -- Check if already exist. 
						$valid = CheckFunction($applicationid,$chargeid,$item);
					    if($valid == false)
						{$message = $message."Loan Application Charges already exist.";}
						else
						{
						 $count = AddCharges($applicationid,$chargeid,$item);
						 $message = $message.' successfully.';
						}
					}
				}
				
				//$message = sizeof($ArraCharge);
		}
		// -- Only All Items
		elseif($item == '')
		{
		 // -- one Charge and All Items.
		 // -- Array Items
				for ($i=0;$i<sizeof($ArrayItems);$i++)
				{
					// -- ChargeID and Item.
						$item = $ArrayItems[$i];

						// -- Check if already exist. 
						$valid = CheckFunction($applicationid,$chargeid,$item);
					    if($valid == false)
						{$message = "Loan Application Charges already exist.";}
						else
						{
						 $count = AddCharges($applicationid,$chargeid,$item);
						}
				}
		 
		}
		// -- Only All Charges
		elseif($chargeid == '') 
		{
		 // -- one Item and All Charges.
		 // -- Array Charges.
					for ($j=0;$j<sizeof($ArraCharge);$j++)
					{
						// -- ChargeID and Item.
						$chargeid = $ArraCharge[$j][0];
						
						// -- Check if already exist. 
						$valid = CheckFunction($applicationid,$chargeid,$item);
					    if($valid == false)
						{$message = "Loan Application Charges already exist.";}
						else
						{
						 $count = AddCharges($applicationid,$chargeid,$item);
						}
					}
		
		}
		// -- if Non if Non is Selected.
		else
		{
		 // -- Check Payment Charges.
			$valid = CheckFunction($applicationid,$chargeid,$item);
			if($valid == false)
			 {$message = "Loan Application Charges already exist.";}
			else
			{
			  $count = AddCharges($applicationid,$chargeid,$item);
			}
		}	
}

// -- Add LoanCharges
function AddCharges($applicationid,$chargeid,$item)
{
	if($chargeid == ''){$chargeid = 0;}
	
	if($item == ''){$item = 0;}
	
	if($applicationid == ''){$applicationid = 0;}
	
		$count = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO loanappcharges  (chargeid,applicationid, item) VALUES (?,?,?)"; 	
			
		$q = $pdo->prepare($sql);
		$q->execute(array($chargeid,$applicationid,$item));
		Database::disconnect();
		$count = $count + 1;
			
	return $count;
}

// -- Check if Charges Exist.
function CheckFunction($applicationid,$chargeid,$item)
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	if($chargeid == ''){$chargeid = 0;}
	
	if($item == ''){$item = 0;}
	
	if($applicationid == ''){$applicationid = 0;}
	
	$valid = true;
  
 // -- Check Payment Charges.
	$sql = 'SELECT * FROM loanappcharges WHERE ApplicationId = ? AND item = ? AND chargeid = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($applicationid,$item,$chargeid));
    $dataloanappcharges = $q->fetchAll();	
  
	Database::disconnect();
  
  // -- SizeOf an Array.
	if(sizeof($dataloanappcharges) > 0)
	{
		$valid = false;
	}
  return $valid;
}
?>
<div class="container background-white bottom-border">   
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 

<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Loan Application Charges Details
                    </a>
                </h2>
<?php # error messages
		/*if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}*/
		
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Loan Application Charges successfully added.</p>");
		}
		else
		{
		    printf("<p class='status'>$message</p>");
		}
?>
				
</div>				
			<p><b>Application ID</b><br /></p>
			<div class="controls">
					<div class="controls">
						<SELECT class="form-control" id="applicationid" name="applicationid" size="1" onChange="itemselect(this);" 
						onload="itemselect(this);">
						<?php
							$applicationidTemp = '';
							$applicationidSelect = $applicationid;
							$customerid = '';
							$customername = '';
							$ArrayApplications = null;
							$indexInside = 0;
							$status = '';
							$dateAccepted = '';
							
							foreach($dataLoanApplications as $row)
							{
								$applicationidTemp = $row['ApplicationId'];
								$customerid = $row['CustomerId'];

								$ArrayApplications[$indexInside][0] = $row['ApplicationId'];
								$ArrayApplications[$indexInside][1] = $row['CustomerId'];
								
								$status = $row['ExecApproval'];
								$dateAccepted = $row['DateAccpt'];
							
								// -- Get the title,firstname,surname.
								for ($i=0;$i<sizeof($ArrayCustomers);$i++)
								{
									if($ArrayCustomers[$i][0] == $row['CustomerId'])
									{
										$customername = $ArrayCustomers[$i][1].'.'.$ArrayCustomers[$i][2].' '.$ArrayCustomers[$i][3];
									}
								}
								
								if($applicationidTemp == $applicationidSelect)
								{
								 echo "<OPTION value=$applicationidTemp selected>$applicationidTemp - $customerid ($customername)- $status- $dateAccepted</OPTION>";
								}	
								else
								{
									echo "<OPTION value=$applicationidTemp>$applicationidTemp - $customerid ($customername) - $status- $dateAccepted</OPTION>";
								}
								$indexInside = $indexInside + 1;
							}
										
							if(empty($dataCharges))
							{
								echo "<OPTION value=0>No Applications</OPTION>";
							}
					?>
					</SELECT>
					</div>
			</div>

			<p><b>Payment Schedule(Item)</b><br />			
			<div class="control-group">
				<div class="controls">
					<SELECT class="form-control" id="item" name="item" size="1" onChange="selected(this)">	
						<OPTION value='' selected>All</OPTION>
					</SELECT>
					
					<SELECT class="form-control" id="itemArray" multiple name="itemArray[]" size="5" hidden>	
					
					</SELECT>
				</div>
			</div>
			
			</p> 
			<label>Charges</label>
			<!------------------------------------------------->
				<div class="controls">
					<SELECT class="form-control" id="chargeid" name="chargeid" size="1">
						<?php
							$chargeidTemp = '';
							$chargeidSelect = $chargeid;
							$chargename2 = '';
							$ArraCharge = null;
							$indexInside = 0;
							$amount = '';
				
							// -- Add the All Option.
							echo "<OPTION value='' selected>All</OPTION>";
											
											foreach($dataCharges as $row)
											{
												$chargeidTemp = $row['chargeid'];
												$chargename2 = $row['chargename'];
												$amount = $row['amount'];
												
												if($chargeidTemp == $chargeidSelect)
												{
												echo "<OPTION value=$chargeidTemp selected>$chargeidTemp - $chargename2 - ($currency$amount)</OPTION>";
												}
												//elseif($chargeidSelect == '')
												//{
												  //echo "<OPTION value='' selected>All</OPTION>";
												//}
												else
												{
												 echo "<OPTION value=$chargeidTemp>$chargeidTemp - $chargename2 - ($currency$amount)</OPTION>";
												}
											}
										
											if(empty($dataCharges))
											{
												echo "<OPTION value=0>No Charges</OPTION>";
											}
											?>
					</SELECT>
					<!------------------------------------------------->
					</div>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'loanappcharges';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
<script  type="text/javascript">
// jQuery
 $(document).ready(function () {
 						var item = document.getElementById('item');
var itemID = item.options[item.selectedIndex].value; 

 //alert("Modise"+item.selectedIndex);
       itemselect(document.getElementById("applicationid"));
    });
 // -- Enter to capture comments   ---
 function itemselect(sel) 
 {
		var applicationid = sel.options[sel.selectedIndex].value;      
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectPaymentScheduleItems.php",
				       data:{applicationid:applicationid},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#item").html(data);
							jQuery("#itemArray").html(data);
							
						var earrings = document.getElementById('itemArray');
						earrings.style.display = 'none';//visibility
						
						// -- Only assist if all is selected.
						jQuery('#itemArray option').prop('selected', true);
						
						 }
					});
				});				  
			  return false;
  }
  
  function selected(sel) 
  {
	 var applicationid = sel.options[sel.selectedIndex].value;  
	 sel.selectedIndex  = sel.selectedIndex;
	 
	 //alert(sel.selectedIndex);
	 			  return false;

  }
  selected(this)
</script>
