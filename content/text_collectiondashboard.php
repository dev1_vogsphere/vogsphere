<?php
// -- Include the database file.
include 'database.php';
if(!function_exists('getmainstatus'))
{
	function getmainstatus($dataFilter,$databankresponsecodeArr)
	{
		$branchcode = '';
		$value0     = '';
		$value1		= '';

		foreach($databankresponsecodeArr as $row)
		{
			$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
			if(isset($rowData[0]))
			{
				$value0 = $rowData[0];
			}
			if($value0 == $dataFilter['Status_Description'])
			{
				$value1 = $rowData[1];
			}
		}
		return $value1;
	}
}

if(!function_exists('getsubstatus'))
{
	function getsubstatus()
	{
		// -- Database Declarations and config:
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = 'SELECT * FROM bankresponsecodes';
		$databankresponsecode = $pdo->query($sql);

		$databankresponsecodeArr = array();
		foreach($databankresponsecode as $row)
		{
			$databankresponsecodeArr[] = $row['bankresponsecodedesc']."|".$row['mainstatus'];
		}

		return $databankresponsecodeArr;
	}
}

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pdo2 = Database::connect();
$pdo2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tablename = 'collection';
$dbnameobj = 'ecashpdq_paycollections';
$fulltime   = "";
$dashTitle 	= "Monthly Collection Status for ";
$clientname = "";
$TotalPaidAmnt = '';
$arrName = null;
$userCode = '';
$IntUserCode = getsession('IntUserCode');

$PendingAmount 		= 0;
$UnpaidAmount 		= 0;
$SuccessfulAmount 	= 0;
$OverallMTD=0;
$OverallPendingAmount =0;
$OverallUnpaidAmount =0;
$OverallSuccessfulAmount =0;


$CurrentMonth = date('Y-m');
$From = date('Y-m');

// -- View Sub Statuses.
$chksubstatus = '';

if(isset($_SESSION['Title']))
{
	$clientname = $_SESSION['Title'].' '.$_SESSION['FirstName'].' '.$_SESSION['LastName'];
}

if(isset($_SESSION['UserCode']))
{
	$userCode = $_SESSION['UserCode'];
}

////////////////////////////////// Post ///////////////////////////////
$companyname = '';
$srUserCode = '';

// -- BOC 26/01/2022 - Page Features Authorization...
$display= 'display:inline-block';
$displayTranslist= 'display:inline';
$displayTransMDT= 'display:block';
$dataPageFeaturesArr = null;
$currentpageArr = null;
$currentpageArr = explode("?",basename($_SERVER['REQUEST_URI'],".php"));
$currentpage = $currentpageArr[0];
$authorized_featuresArr = null;
$role = $_SESSION['role'];
// BOC -- Get sub branches 17/01/2022
$branchesArr = array();
$pdo = Database::connectDB();
$branchesArr = getsubbranches($pdo,$IntUserCode);
// EOC -- Get sub branches 17/01/2022
if(!empty($_POST))
{
$whereArray = null;
$From = $_POST['From'];
$CurrentMonth = $From;

$chksubstatus = getpost("chksubstatus");
$srUserCode = getpost('srUserCode');
//$display = getpost('display');
if(!empty($branchesArr ))
{$IntUserCode = $srUserCode;}
}

// -- Get Company Name 17/01/2022
foreach($branchesArr as $row)
{
	$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
	  if($rowData[0] == $IntUserCode)
	  {
		  $companyname = $rowData[1];
	  }
}
		//echo $IntUserCode.$srUserCode;
$fulltime   = "'".$dashTitle.$IntUserCode."'";

//echo $userCode.' - no data to present on the chart';
//$whereArray[]    = "Client_Id = '{$userCode}'"; // -- Client Reference.
$whereArray[]    = "IntUserCode = '{$IntUserCode}'";
$whereArray[]    = "Action_Date LIKE '%{$CurrentMonth}%'"; // -- Client Reference.

$queryFields[] = 'Status_code';
$queryFields[] = 'Status_Description';
$queryFields[] = 'Amount';

//print_r($whereArray);
$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);

$data 	   = array(); //array("'success'"=>$success,"'failed'"=>$failed,"'submitted'"=>$submitted);
$dataTemp  = null;
$dataCount = 0;
//print($collectionObj->rowCount() );

// ---------------------------- Collection Dashboard ----------------------------- //
// -- Get sub statuses
$databankresponsecodeArr = getsubstatus();

$PendingAmount 		= 0;
$UnpaidAmount 		= 0;
$SuccessfulAmount 	= 0;

if($collectionObj->rowCount() > 0)
{  $mainstatus = '';
	foreach($collectionObj as $rowPaid)
	{
		$dataFilter['Status_Description'] = $rowPaid["Status_Description"];
		$mainstatus = getmainstatus($dataFilter,$databankresponsecodeArr);

		// -- Main Statuses
		if(empty($chksubstatus))
		{
			// -- Get Paid & Unpaid ONLY Dashboard.
			if($rowPaid["Status_Description"] != 'pending' or $rowPaid["Status_Description"] == 'Pending')
			{
				$dataFilter['Status_Description'] = $rowPaid["Status_Description"];
				$mainstatus = getmainstatus($dataFilter,$databankresponsecodeArr);
			}
			else
			{
				$mainstatus = 'pending';
			}

			if(isset($data[$mainstatus]))
			{
				$data[$mainstatus] = $data[$mainstatus] + 1;
			}
			else
			{
				$data[$mainstatus] = 1;
			}
		}
		// -- Sub Statuses
		else
		{
			if(isset($data[$rowPaid['Status_Description']]))
			{
				$data[$rowPaid['Status_Description']] = $data[$rowPaid['Status_Description']] + 1;
			}
			else
			{
					$data[$rowPaid['Status_Description']] = 1;
			}
		}

			if($rowPaid["Status_Description"] == 'pending' or $rowPaid["Status_Description"] == 'Pending')
			{$PendingAmount = $PendingAmount + $rowPaid['Amount'];}

			// -- Main Status
			if($mainstatus == 'Successful')
			{
				$SuccessfulAmount = $SuccessfulAmount + $rowPaid['Amount'];
			}
			if($mainstatus == 'UnPaid')
			{
				$UnpaidAmount = $UnpaidAmount + $rowPaid['Amount'];
			}

		// -- echo $rowPaid['Status_Description'];
	}

	$UnpaidAmount 	  = number_format($UnpaidAmount, 2);
	$SuccessfulAmount = number_format($SuccessfulAmount, 2);
	$PendingAmount    = number_format($PendingAmount, 2);

//		print_r($collectionObj);
		//array_push($data,$dataTemp);
}
// -------------------------------------------------------------------------------- //
function searchForId($name,$arr)
{
	foreach($arr as $x=>$x_value)
	{
						  		if(is_array($x_value))
								{
																if(isset($x_value[$name])){
		$key = array_search ($x_value[$name], $x_value);

		if($key == $name)
		{
			return $key;
								}}}
		return null;
	  //echo "[" . $x . ",". $x_value."],";
	}
}

// -- Get Total Collections for a month
if(!function_exists('GetTotalCollections'))
{
 function GetTotalCollections($pdo,$usercode,$month)
 {
			$month = '%'.$month.'%';
			$sql = "SELECT count(*) as count FROM collection where IntUserCode = ? and Action_Date LIKE ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($usercode,$month));
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(isset($data['count']))
			{
				return $data['count'];
			}
			else
			{
				return '0';
			}
			Database::disconnect();
 }
}
// -- Get Total Collections Successful
if(!function_exists('GetcompanyTotalCollectionsSuccessful'))
{

 function GetcompanyTotalCollectionsSuccessful($pdo,$userCode,$month)
 {
	 	$OverallSuccessfulAmount=0;
			$month = '%'.$month.'%';
			$sql = "SELECT Sum(Amount) as amount FROM collection where UserCode = ? and Action_Date LIKE ? and Status_Description IN (?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($userCode,$month,'Transaction Successful','Submitted'));
			$data2 = $q->fetch(PDO::FETCH_ASSOC);

			if(isset($data2['amount']))
			{
				$OverallSuccessfulAmount=$OverallSuccessfulAmount + $data2['amount'];
				return number_format($OverallSuccessfulAmount, 2);
			}
			else
			{
				return '0';
			}
			Database::disconnect();
 }
}
// -- Get Total Collections MTD
if(!function_exists('GetcompanyTotalCollectionsMTD'))
{

 function GetcompanyTotalCollectionsMTD($pdo,$userCode,$month)
 {
			$month = '%'.$month.'%';
			$sql = "SELECT count(*) as count FROM collection where UserCode = ? and Action_Date LIKE ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($userCode,$month));
			$data2 = $q->fetch(PDO::FETCH_ASSOC);

			if(isset($data2['count']))
			{
				return $OverallMTD=$data2['count'];
			}
			else
			{
				return '0';
			}
			Database::disconnect();
 }
}


// -- Get Aggregate limits
if(!function_exists('GetAggregateLimits'))
{
  function GetAggregateLimits($pdo1,$usercode)
 {
$sql =  "select * from client where usercode = ?";
$q1 = $pdo1->prepare($sql);
$q1->execute(array($usercode));
$data1 = $q1->fetch(PDO::FETCH_ASSOC);

if(empty($data1)){ // not a debit order client
					$debitorderclientstyle = 'none';
				 }
if($data1)
{
$ID 						 = $data1['ID'];
$Description 			     = $data1['Description'];
$Username 				     = $data1['username'];
$Password 				     = $data1['password'];

$aggregate_limits_debits     = $data1['aggregate_limits_debits'];
$aggregate_limits_naedo 	 = $data1['aggregate_limits_naedo'];
$aggregate_limits_cards 	 = $data1['aggregate_limits_cards'];
$aggregate_limits_credits    = $data1['aggregate_limits_credits'];

if(empty($aggregate_limits_debits)){$aggregate_limits_debits = '0.00';}
if(empty($aggregate_limits_naedo)){$aggregate_limits_naedo = '0.00';}
if(empty($aggregate_limits_cards)){$aggregate_limits_cards = '0.00';}
if(empty($aggregate_limits_credits)){$aggregate_limits_credits = '0.00';}

$item_limits_debits 		 = $data1['item_limits_debits'];
$item_limits_naedo 		     = $data1['item_limits_naedo'];
$item_limits_cards 		     = $data1['item_limits_cards'];
$item_limits_credits 	     = $data1['item_limits_credits'];

if(empty($item_limits_debits)){$item_limits_debits = '0.00';}
if(empty($item_limits_naedo)){$item_limits_naedo = '0.00';}
if(empty($item_limits_cards)){$item_limits_cards = '0.00';}
if(empty($item_limits_credits)){$item_limits_credits = '0.00';}

$changedby 					 = $data1['changedby'];
$changedon 					 = $data1['changedon'];
$changedat					 = $data1['changedat'];

return $data1;
}
}}
//echo 'IntUserCode:'.$IntUserCode.'<br/>'.'userCode:'.$userCode;
// 05/02/2022 - commented out below..
// $AggregateLimits = GetAggregateLimits($pdo,$IntUserCode);
$AggregateLimits = GetAggregateLimits($pdo,$IntUserCode);
$MTDTransaction  = GetTotalCollections($pdo,$IntUserCode,$CurrentMonth);
$OverallSuccessfulAmount = GetcompanyTotalCollectionsSuccessful($pdo,$userCode,$CurrentMonth);
$OverallMTD=GetcompanyTotalCollectionsMTD($pdo,$userCode,$CurrentMonth);

// User Code & Customer id
$CustomerIdEncoded =  urlencode(base64_encode(getsession('username')));
$UserCodeEncoded =  urlencode(base64_encode(getsession('UserCode')));
//--<strong>Welcome ".$_SESSION['Title']." ".$_SESSION['FirstName']." ".$_SESSION['LastName']."</strong>
$userprofileurl = "<a id='userprofile' name='userprofile' href='userprofile?customerid=".$CustomerIdEncoded."&usercode=".$UserCodeEncoded."' class='fa fa-2x fa-user'></a>";
//echo "<strong>&nbsp;&nbsp;Email:</strong>&nbsp;".$_SESSION['email']."";


// BOC -- Branches: 17/01/2022
if(!empty($branchesArr))
{
		$display='display:inline-block';
		}else{$display='display:none';}
// BOC -- Branches: 17/01/2022

// -- Authorization of features
$authorized_featuresArr = authorize_features($pdo2,$currentpage,$role);
$display = $authorized_featuresArr['display'];
$displayTranslist = $authorized_featuresArr['displayTranslist'];
$displayTransMDT  = $authorized_featuresArr['displayTransMDT'];
// -- EOC 26/01/2022 - Page Features Authorization...

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class='container background-white bottom-border'>
	<div class='row margin-vert-30'>
	                            <!-- Login Box 17/01/2022 -->
		<div class='col-md-12 col-md-offset-0 pull-left col-sm-offset-6'>

			<form class='login-page' method='post' onsubmit=' return addRow()'>
				<div >
						<h2 align="center">Debit Order Analytics</h2>
						</br>
						<h4 align="center">Branch name : <?php echo $companyname; ?><name></h4>
						<br>
			  </div>
				<div class="portfolio-filter-container margin-top-20" margin: 25px>
						 <ul class="portfolio-filter">
								<li style="height:30px;z-index:0" class="portfolio-filter-label label label-primary">
										Filter by Month:
								</li>
			        	&nbsp;
								<li style="height:30px;z-index:0">
			        		<input style='height:30px' id='From' name='From' placeholder='Date From' class='form-control' type='Month' value=<?php echo $From;?>>
								</li>
			        	&nbsp;
								<li style="height:30px;z-index:0" class="portfolio-filter-label label label-primary">
									View SUB Statuses:
								</li>
			        	&nbsp;
								<li style="height:30px;z-index:0">
								  <label class="checkbox">
									<input type="checkbox" name="chksubstatus" id="chksubstatus" value="chksubstatus" <?php if ($chksubstatus == 'chksubstatus') echo 'checked'; ?>></input>
								  </label>
								</li>
			          &nbsp;
							<!-- BOC Branches 17/01/2022 -->
							<li style="<?php echo $display; ?>;height:30px;z-index:0;" class="portfolio-filter-label label label-primary">
			            Branches:
			        </li>
			          &nbsp;
								<li style="<?php echo $display; ?>;height:30px;z-index:0;">
					   				<SELECT style="width:150px;height:30px;z-index:0;" class="form-control" id="srUserCode" name="srUserCode" size="1" style='z-index:0'>
						    <?php
								$datasrUserCodeSelect = $srUserCode;
								foreach($branchesArr as $row)
								{
								  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
								  if($rowData[0] == $datasrUserCodeSelect)
								  {
									  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
								  	  $companyname = $rowData[1];
								  }
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
								}
							?>
						</SELECT>
			        </li>
			        &nbsp;  &nbsp;  &nbsp;
							<!-- EOC Branches 17/01/2022 -->
							<li style="height:23px;z-index:0">
							    <button class='btn btn-primary pull-right' type='submit'>Search</button>
							</li>
					</ul>
			  </div>

				<div style="display:none">
							<a id="futurecollection" name="futurecollection" href="billFutureCollection">Future Collection</a>
							<a id="billCollection" name="billCollection" href="billCollection">Bill Collection</a>
						  <input style='height:30px' id='display' name='display' placeholder='Display' class='form-control' type='text' value=<?php echo $display;?>>
				</div>

			</form>

		</div>
	</div>

	<div class="container" style="<?php echo $displayTransMDT; ?>">
		<div class="row">

			<div class="col-md-3">
				<div class="card text-white bg-primary">
					<div class="card-body" align="center">
											 <!-- Title -->
										<h3 class="text-uppercase text-muted mb-2" style="color:white"; >
												 Transactions
										</h3>
											 <!-- Heading -->
										 <span class="h2 mb-0" >
												<strong><?php echo $MTDTransaction;?></strong>
										 </span>
											<!-- Badge
											<span class="badge bg-success-soft mt-n1">
											</span>-->
						</div>
					</div>
				</div>

				<div class="col-md-3">
					<div class="card text-white bg-primary">
						<div class="card-body" align="center">
												 <!-- Title -->
												<h3 class="text-uppercase text-muted mb-2" style="color:white";>
													 Pending
												</h3>
												 <!-- Heading -->
											 <span class="h2 mb-0">
													<strong>R<?php echo $PendingAmount;?></strong>
											 </span>
											 <!-- Badge
											 <span class="badge bg-success-soft mt-n1">

											 </span>-->
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="card text-white bg-primary">
							<div class="card-body" align="center">
													 <!-- Title -->
													 <h3 class="text-uppercase text-muted mb-2" style="color:white";>
														Unpaid
													</h3>
													 <!-- Heading -->
												 <span class="h2 mb-0">
												<strong>R<?php echo $UnpaidAmount;?></strong>
												 </span>
												 <!-- Badge
 												<span class="badge bg-success-soft mt-n1">

 												</span>-->
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="card text-white bg-primary">
								<div class="card-body" align="center">
														 <!-- Title -->
														 <h3 class="text-uppercase text-muted mb-2" style="color:white";>
															 Successful
														 </h3>
														 <!-- Heading -->
													 <span class="h2 mb-0">
															<strong>R<?php echo $SuccessfulAmount;?></strong>
													 </span>
													 <!-- Badge
													 <span class="badge bg-success-soft mt-n1">

													 </span>-->
									</div>
								</div>
							</div>


			<!--end-->
		</div>
	</div>
 <br>
 <br>
	<div class="container">
	</div>

	<div class="container">
  <div class="row">
    <div class="col-md-3" id="piechart2" style="height: 300px;<?php echo $displayTranslist; ?>">
			<h3>Transaction <span class="label label-default">Limits</span></h3>
			<table>
				<tr><td><h5><span class="label label-default">EFT</span></h5></td></tr>
				<tr><td><strong>Aggregate limit :</strong></td><td> R <?php if(isset($AggregateLimits['aggregate_limits_debits'])){echo number_format ($AggregateLimits['aggregate_limits_debits'],2);}else{ echo '0.00';}?></td><td></tr>
				<tr><td><strong>Item limit :</strong></td><td>R <?php if(isset($AggregateLimits['item_limits_debits'])){echo number_format ($AggregateLimits['item_limits_debits'],2);}else{ echo '0.00';}?></td></tr>
				<tr><td><h5><span class="label label-default">NAEDO</span></h5></td></tr>
				<tr><td><strong>Aggregate limit :</strong></td><td> R <?php if(isset($AggregateLimits['aggregate_limits_naedo'])){echo number_format ($AggregateLimits['aggregate_limits_naedo'],2);}else{ echo '0.00';}?></td></tr>
				<tr><td><strong>Item limit :</strong></td><td> R <?php if(isset($AggregateLimits['item_limits_naedo'])){echo number_format ($AggregateLimits['item_limits_naedo'],2);}else{ echo '0.00';}?></td></tr>
				<tr><td><h5><span class="label label-default">AC</span></h5></td></tr>
				<tr><td><strong>Aggregate limit :</strong></td><td>R <?php echo '0.00';?></td></tr>
				<tr><td><strong>Item limit :</strong></td><td>R <?php /*if(isset($AggregateLimits['aggregate_limits_debits'])){echo $AggregateLimits['aggregate_limits_debits'];}else{*/ echo '0.00';?></td></tr>
			</table>
    </div>
    <div class="col-md-6" id="piechart" style="height: 350px;">
    </div>
		<div class="col-md-3" id="transoverview" style="height: 300px;<?php echo $displayTranslist; ?>">
			<h3>Summary <span class="label label-default">Overall</span></h3>
			<table>
				<tr><td><strong>MTD Overall: </strong><?php echo $OverallMTD; ?></td></tr>
				<tr><td><strong>Overall Revenue: </strong>R <?php echo $OverallSuccessfulAmount; ?></td></tr>
				<!--span class="label label-default"></span>-->
				<!--<span class="label label-default"></span>-->
			</table>
		</div>

  </div>
</div>


</div><!--end-->

	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart()
	  {

        var data = google.visualization.arrayToDataTable(
		[['Task', 'Hours per Day'],
			<?php
			 foreach($data as $x=>$x_value)
			  {
								echo "[" . "'".$x."'". ",".$x_value."],";
			  }
			 ?>
		  ]);

        var options = {title: <?php echo $fulltime; ?>,'is3D': true,color:'#555555',fontName:'Century Gothic'
				,fontSize:14,legend: {position: 'none'}};
		            //pieHole: 0.4};

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

	   function selectHandler()
	   {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var topping = data.getValue(selectedItem.row, 0);
            // -- alert('The user selected ' + topping);
			if(topping === 'pending')
			{
				document.getElementById('futurecollection').click();
			}
			else
			{
				document.getElementById('billCollection').click();
			}
          }
        }

	  google.visualization.events.addListener(chart, 'select', selectHandler);

        chart.draw(data,options);
      }


    </script>
