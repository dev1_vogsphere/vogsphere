<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_name 		  = "client"; // Table name 
$mainusercode     = '';
$role 			  = '';

// -- Get roles - 23/01/2022
if(!function_exists('getroles'))
{
  function getusercodes($pdo)
  {
	$sql = "select distinct usercode,Description from client";
	$dataclient = $pdo->query($sql);

	$usercodesArr = array();
	foreach($dataclient as $row)
	{
				$usercodesArr[] = $row['usercode']."|".$row['Description'];
	}
	return $usercodesArr;
}}
  
// -- Roles...
// BOC -- Get sub branches 17/01/2022
$usercodesArr = array();
$pdo = Database::connectDB();
$usercodesArr = getusercodes($pdo);

if ( !empty($_POST)) 
{
   $mainusercode = $_POST['mainusercode'];
}
  
 echo "<div class='container background-white bottom-border'>";		   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Clients by Main User Code</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                    <SELECT style='width:350px;height:30px;z-index:0;' class='form-control' id='mainusercode' name='mainusercode' size='1' style='z-index:0'>";
			    
					$mainusercodeSelect = $mainusercode;
					foreach($usercodesArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $mainusercodeSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
					}
				
			echo "</SELECT>";
									echo "</div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
						
					/*if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $role = $_POST['role'];
						   }
					}*/					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($mainusercode))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE Intusercode = ? ORDER BY usercode";
						$q = $pdo->prepare($sql);
					    $q->execute(array($mainusercode));	
						$data = $q->fetchAll();  
						
						if($q->rowCount() <= 0)
						{
							$sql = "SELECT * FROM $tbl_name WHERE usercode = ? ORDER BY usercode";
							$q = $pdo->prepare($sql);
							$q->execute(array($mainusercode));	
							$data = $q->fetchAll();  	
						}
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name ORDER BY usercode";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newclient.php>Add New Client</a><p></p>"; 
						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>"; 
						echo "<tr>"; 
						echo "<td><b>Intusercode</b></td>"; 						
						echo "<td><b>Description</b></td>"; 
						echo "<td><b>username</b></td>"; 
						echo "<td><b>password</b></td>"; 
						echo "<td><b>usercode</b></td>"; 
						echo "<td><b>debicheckusercode</b></td>"; 
						echo "<td><b>aggregate_limits_debits</b></td>"; 
						echo "<td><b>aggregate_limits_naedo</b></td>"; 
						echo "<td><b>aggregate_limits_cards</b></td>"; 
						echo "<td><b>aggregate_limits_credits</b></td>"; 
						echo "<td><b>item_limits_debits</b></td>"; 
						echo "<td><b>item_limits_naedo</b></td>"; 
						echo "<td><b>item_limits_cards</b></td>"; 
						echo "<td><b>item_limits_credits</b></td>"; 
						echo "<td><b>changedby</b></td>"; 
						echo "<td><b>changedon</b></td>"; 
						echo "<td><b>changedat</b></td>"; 
						echo "<td><b>lockedforchanges</b></td>"; 
						echo "<td><b>urlqadebicheck</b></td>"; 
						echo "<td><b>urlproddebicheck</b></td>"; 
						echo "<td><b>urlqamercantile</b></td>"; 
						
						echo "<td><b>urlprodmercantile</b></td>"; 
						echo "<td><b>abbreviatedshortname</b></td>"; 

						echo "<td><b>security</b></td>"; 
						echo "<td><b>shortname</b></td>"; 						
						echo "<td><b>paymentusercode</b></td>"; 

						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
												
						echo "<tr>"; 
						echo "<td valign='top'>" . nl2br( $row['Intusercode']) . "</td>";  							
						echo "<td valign='top'>" . nl2br( $row['Description']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['username']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['password']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['usercode']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['debicheckusercode']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['aggregate_limits_debits']) . "</td>";  	
			
						echo "<td valign='top'>" . nl2br( $row['aggregate_limits_debits']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['aggregate_limits_naedo']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['aggregate_limits_credits']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['item_limits_debits']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['item_limits_naedo']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['item_limits_cards']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['item_limits_credits']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['changedby']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['changedon']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['changedat']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['lockedforchanges']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['urlqadebicheck']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['urlproddebicheck']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['urlqamercantile']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['urlprodmercantile']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['abbreviatedshortname']) . "</td>";  	

						echo "<td valign='top'>" . nl2br( $row['security']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['shortname']) . "</td>";  	
						echo "<td valign='top'>" . nl2br( $row['paymentusercode']) . "</td>";  	
						
			
						echo "<td valign='top'><a class='btn btn-success' href=editclient.php?ID={$row['ID']}>Edit</a></td>";
						echo "<td valign='top'></td>";//echo "<td valign='top'><a class='btn btn-danger' href=deleteclient.php?accessid={$row['ID']}>Delete</a></td> "; 
						echo "</tr>";
						echo "</tr>";	
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
		<?php 
			echo "</div>";
		?>
   		<!---------------------------------------------- End Data ----------------------------------------------->