<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div>
                            <!-- Login Box -->
				<form class="login-page" action="update.php?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

<table class=   "table table-user-information">
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Loan Payment Details
                    </a>
                </h2>						
</td></div>							
		
</tr>		
</table>
									</div>
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				<table class="table table-striped table-bordered">
		              <thead>
							<tr> 
								<th><b>Loan Application ID</b></th>
								<th><b>Invoice ID</b></th>
								<th>Payment Date</th>
								<th><b>Amount</b></th>
								<th><b>Proof Payment</b></th>
								<th>Action</th>
		                    </tr>
		              </thead>
		              <tbody>
		              <?php 

					// -- Total Income
					$tot_outstanding = 0.00;
					$tot_paid = 0.00;
					$tot_repayment = 0.00;
					
	 				//require 'database_invoice2.php';
					$customerid = null;
					$ApplicationId = null;
					$dataUserEmail = '';
					$role = '';
					$repaymentAmount = null;
							
					if ( !empty($_GET['repayment'])) 
					{
						$repaymentAmount = $_REQUEST['repayment'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						//$repaymentAmount = base64_decode(urldecode($repaymentAmount)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( !empty($_GET['customerid'])) 
					{
						$customerid = $_REQUEST['customerid'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						//$customerid = base64_decode(urldecode($customerid)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( !empty($_GET['ApplicationId'])) 
					{
						$ApplicationId = $_REQUEST['ApplicationId'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						//$ApplicationId = base64_decode(urldecode($ApplicationId)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( null==$customerid ) 
						{
						header("Location: custAuthenticate.php");
						}
						else if( null==$ApplicationId ) 
						{
						header("Location: custAuthenticate.php");
						}
					 else 
					 {
					   
// ----------------------------------------------------------------------------------- //
// 					BEGIN - IMS Web Service URL & Read the Data From IMS 			   //
// ----------------------------------------------------------------------------------- //				 
// set feed URL
$WebServiceURL = "http://www.vogsphere.co.za/eloan/ws_Payments.php?repayment=".$repaymentAmount."&customerid=".$customerid."&ApplicationId=".$ApplicationId."";

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// pre tags to format nicely
//echo '<pre>';
//print_r($sxml);
//echo '</pre>';
$path  = '';
// And if you want to fetch multiple Invoice IDs:
foreach($sxml->invoice as $invoice)
{
	$iddocument = $invoice->paymentfile;
	$paymentid  = $invoice->paymentid;
	echo '<tr>';
							   	echo '<td>'.$invoice->loanappid.'</td>';
								echo '<td>'.$invoice->invoiceid. '</td>';								
							    echo '<td>'.$invoice->date. '</td>';
							   	echo '<td>'.$invoice->amount. '</td>';
							   	echo "<td><a href=uploads/$path target='_blank'>".$iddocument."</a></td>";
							   	echo '<td width=150>';
							   	echo "<a class='btn btn-info' href=upload_payment.php?repayment=$repaymentAmount&customerid=$customerid&paymentid=$paymentid&ApplicationId=$ApplicationId>Upload Proof of Payment</a></td>";
							   	echo '&nbsp;';
							   	echo '</tr>';
}

$tot_paid = $sxml->totalpaid;
$tot_outstanding = $sxml->totaloutstanding;
$RemainingTerm = $sxml->RemainingTerm;
$repaymentAmount = $sxml->totaldebt;

if($data != null)
{
	$tot_outstanding = $repaymentAmount - $tot_paid;
	echo '<table class="table">';
	echo "<tr class='bg-danger'><td><b>Total Repayment Amount(Excl. Interest & Penalty)</b></td><td><b>$currency".$repaymentAmount."<b></td></tr>";
	echo "<tr class='bg-success'><td><b>Total Paid Amount</b></td><td><b>$currency".$tot_paid."</b></td></tr>";
	echo "<tr  class='bg-info'><td><b>Remaining Term(In months)</b></td><td><b>$RemainingTerm</b></td></tr>";
	echo '</table>';
}
// ----------------------------------------------------------------------------------- //
// 					END - IMS Web Service URL & Read the Data From IMS 			       //
// ----------------------------------------------------------------------------------- //				 
						} // -- Exception
						?>
				      </tbody>

</table>
			<table class=   "table table-user-information">
			<tr>
				<td><div>
<!-- IF PAGES COME FROM TotalsReport TO BACK IT IF ELSE -->
				<?php 
					$icamefrom = "";
					// -- BOC Encrypt 16.09.2017.
					if(isset($_SERVER['HTTP_REFERER']))
					{
						$icamefrom = $_SERVER['HTTP_REFERER'];
					}
					else
					{
						header("Location: custAuthenticate.php");
					}
					// -- EOC Encrypt 16.09.2017.

					if (!isset($_SESSION['icamefrom']))
					{
					  	$_SESSION['icamefrom'] = $icamefrom;
					}
					else
					{
					    $icamefrom = $_SESSION['icamefrom'];
					}
					echo "<a class='btn btn-primary' href=$icamefrom>Back</a>";
				?>
				</div></td>
				</div></td>
			</tr>
			</table>
				
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->							
 <!------------------------------ End File List ------------------------------------>
	
<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------> 	
					</form>			
						
				
                            <!-- End Login Box -->							
                        </div>
					
                </div>
            <!-- === END CONTENT === -->