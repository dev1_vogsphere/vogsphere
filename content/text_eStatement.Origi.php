<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<?php
// sample array data
$data = array();
$FirstName = '';

 //require 'database.php';
 $webpage = "customerLogin.php";
 $lv_username ="root";
 $lv_password = "root";
 $tbl_name="user"; // Table name
 $ExecApproval = '';
 $To = '';
 $From = '';
 $Filecontent = '';
 $eMailInvoices = array(); // -- 2017.04.23
 
// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
require_once 'database.php';
					   
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $q = $pdo->prepare($sql);
  $q->execute();
  $dataBanks = $q->fetchAll();						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $q = $pdo->prepare($sql);
 $q->execute();
 $dataAccountType = $q->fetchAll();//$pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

	// -- Export to Exce.
if(isset($_POST["ExportType"]))
{
// -- Filecontent data.
if (isset($_SESSION['Filecontent']))
{
	$data = $_SESSION['Filecontent'];
}
	switch($_POST["ExportType"])
    {
        case "export-to-excel" :
            // Submission from
			$filename = $_POST["ExportType"] . ".xls";
            header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			ExportFile($data);
			//$_POST["ExportType"] = '';
            exit();
		case "export-to-csv" :
            // Submission from
			$filename = $_POST["ExportType"] . ".csv";
/*echo $filename; */
		//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Expires: 0");
			ExportCSVFile($data);
			//$_POST["ExportType"] = '';
            exit();
        default :
            die("Unknown action : ".$_POST["action"]);
            break;
    }
	// -- flush ob.
	ob_end_flush();
}
function ExportCSVFile($records) 
{
	// create a file pointer connected to the output stream
	$fh = fopen( 'php://output', 'w' );
	$heading = false;
		if(!empty($records))
		  foreach($records as $row) {
			if(!$heading) {
			  // output the column headings
			  fputcsv($fh, array_keys($row));
			  $heading = true;
			}
			// loop over the rows, outputting them
			 fputcsv($fh, array_values($row));
		  }
		  fclose($fh);
}
function ExportFile($records) {
	$heading = false;
	if(!empty($records))
	  foreach($records as $row) {
		if(!$heading) {
		  // display field/column names as a first row
		  echo implode("\t", array_keys($row)) . "\n";
		  $heading = true;
		}
		echo implode("\t", array_values($row)) . "\n";
	  }
	exit;
}
// -- End Export Excel.

			// Id number
			$idnumber = $_SESSION['username'];
			// Title
			$Title = $_SESSION['Title'];
			// First name
			$FirstName = $_SESSION['FirstName'];
			// Last Name
			$LastName = $_SESSION['LastName'];
			// Phone numbers
			$phone = $_SESSION['phone'];
			// Email
			$email = $_SESSION['email'];
			// Street
			$Street = $_SESSION['Street'];
			// Suburb
			$Suburb = $_SESSION['Suburb'];
			// City
			$City = $_SESSION['City'];
			// State
			$State = $_SESSION['State'];
			// PostCode
			$PostCode = $_SESSION['PostCode'];
			// Dob
			$Dob = $_SESSION['Dob'];
			// phone
			$phone = $_SESSION['phone'];
			
			$icamefrom = "";
			
			if(isset($_SERVER['HTTP_REFERER']))
			{
				$icamefrom = $_SERVER['HTTP_REFERER'];
			}
			
	
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
{
// keep track validation errors
		$From = null;
		$To = null;
		$ExecApproval = null;
	    $LastName = null;
		$FirstName = null;
		$customerid = null;
// Filter Records
if ( !empty($_POST))
{
// Customer role
	if($_SESSION['role'] == 'customer')
	{
		$From = $_POST['From'];
		$To = $_POST['To'];
		$ExecApproval = $_POST['ExecApproval'];
	}
  else 	if($_SESSION['role'] == 'admin')
	{
	// keep track post values
		$LastName = $_POST['LastName'];
		$FirstName = $_POST['FirstName'];
		$customerid = $_POST['customerid'];
		$ExecApproval = $_POST['ExecApproval'];
		$From = $_POST['From'];
		$To = $_POST['To'];
	}
}
else
{
// Coming From read & Update.
$ExecApproval = "*";
}
}
//}
 echo "<div class='container background-white bottom-border'>";		
//<!-- Search Login Box -->
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
	{
	 $Approved = '';
	 $Pending = '';
	 $Cancelled = '';
	 $Rejected = '';
	 $All = '';
	  $Settled = '';
	 if($ExecApproval == 'SET')
		{
		  $Settled = 'selected';
		}
	//<?php if ($ExecApproval == 'APPR') echo 'selected';
	if($ExecApproval == 'APPR')
		{
		  $Approved = 'selected';
		}
	if($ExecApproval == 'PEN')
		{
		   $Pending = 'selected';
		}
	if($ExecApproval == 'CAN')
		{
			$Cancelled = 'selected';
		}
	if($ExecApproval == 'REJ')
		{
		   $Rejected = 'selected';
		}
	if($ExecApproval == 'All')
		{
		   $All = 'selected';
		}
	if($ExecApproval == '')
		{
		   $All = 'selected';
		}
	  if($To == "")
	   {
		$To = "9999-12-31";
	   }
	// Customer role
	if($_SESSION['role'] == 'customer')
	{
		echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Filter Loan Application</h2>
                                    </div>
									<div class='row'>
									 <div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
										 </SELECT>
									</div>
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value=$To>
                                    </div>
                                   </div>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>";
	}
  else 	if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Loan Accounts</h2>
                                    </div>
									<div class='row'>
									<div class='col-sm-6'>
										<label class='control-label'>Status</label>
										 <SELECT class='form-control' id='ExecApproval' name='ExecApproval' size='1'>
												<OPTION value='*'    $All>All</OPTION>
												<OPTION value='APPR' $Approved>Approved</OPTION>
												<OPTION value='REJ'  $Rejected>Rejected</OPTION>
												<OPTION value='PEN'  $Pending>Pending</OPTION>
												<OPTION value='CAN'  $Cancelled>Cancelled</OPTION>
												<OPTION value='SET'  $Settled>Settled</OPTION>
										 </SELECT>
									</div>
                                    <div class='col-sm-6'>
                                        <input style='height:30px' name='LastName' placeholder='Last Name' id='LastName' class='form-control margin-bottom-20' type='hidden' value=$LastName>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input style='height:30px' name='FirstName' placeholder='First Name' id='age' class='form-control margin-bottom-20' type='hidden' value=$FirstName>
                                    </div>
									<div class='col-sm-6'>
                                        <label>Date From
                                        </label>
                                        <input style='height:30px' name='From' placeholder='Date From' class='form-control' type='Date' value=$From>
                                    </div>
									<div class='col-sm-6'>
                                        <label>To
                                        </label>
                                        <input style='height:30px' name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value=$To>
                                    </div>
                                   </div>
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='customerid' name='customerid' placeholder='ID' class='form-control' type='text' value=$customerid>
                                    </div>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>";
	}
}
?>
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<!------------        Start Data -------->
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title" style="line-height:35px;">eStatements : export to Excel or CSV file  <div class="btn-group pull-right">
						  <button type="button" class="btn btn-info">Action</button>
						  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						    <span class="caret"></span>
						    <span class="sr-only">Toggle Dropdown</span>
						  </button>
						  <ul class="dropdown-menu" role="menu" id="export-menu">
						    <li id="export-to-excel"><a href="#">Export to excel</a></li>
							<li id="export-to-csv"><a href="#">Export to csv</a></li>
						  </ul>
						</div>
					</h3>
				</div>
				<div class="panel-body">
					<form action="eStatements" method="post" id="export-form">
						<input type="hidden" value='' id='hidden-type' name='ExportType'/>
				  	</form>
				</div>	
			</div>
		</div>
	</div>
</div>

<!-- ===================================================== Send An Email 23.04.2017 =========================== -->
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="panel panel-default">
			<div class="panel-heading">
<h3 class="panel-title" style="line-height:35px;">eMail all the eStatements of APPROVED statuses for <?php $dateForm = date('F')."  ".date('Y'); echo $dateForm; ?><div class="btn-group pull-right">
			<button type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="setupLinks()">eMail eStatements</button>
			</div>
			</div>
		</div>
	</div>
</div>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
        <h2>eMail eStatements - Report</h2>
		<div id="myProgress">
			<div id="myBar">10%</div>
		</div>
		<div id="divSuccess">
		
		</div>
        <p><a data-popup-close="popup-1" href="#">Close</a></p>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
<!-- ===================================================== End Send An Email ======================= -->
<script  type="text/javascript">
	$(document).ready(function() {
		jQuery('#export-menu li').bind("click", function()
		{
			var target = $(this).attr('id');
			//alert("Bona fela"+target);
			switch(target) {
				case 'export-to-excel
				
				
				.' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
				case 'export-to-csv' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
			}
		});
    });
</script>
		<!------------- End ------------------------->
		<div class="row" style="overflow-x:auto;">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p>
				  <div class="table-responsive" width="100%">
				<table class="table table-striped table-bordered" cellspacing="0" width="200px">
<table id="example" class="display nowrap" cellspacing="0" width="100%">-->
	                 <table id="" class="table table-striped table-bordered" style="white-space: nowrap;">
		              <thead>
							<tr>
								<th><b>eMail Statement</b></th> <!-- 0. 23.04.2017 - added to e-mail a client -->
								<th><b>eStatement</b></th> <!-- 0. -->
								<th><b>ePayment</b></th> <!-- 0. -->
								<th><b>eLoan status</b></th> <!-- 0. -->
								<th><b>Acc</b></th> <!-- 1. -->
								<th>Title</th><!-- 2. -->
								<th><b>Surname</b></th><!-- 3. -->
								<th><b>First Name</b></th><!-- 4. -->
								<th><b>Intials</b></th><!-- 5. -->
								<th><b>Instalment Amount</b></th><!-- 6. -->
								<th><b>Language</b></th><!-- 7. -->
								<th><b>Cause Date</b></th><!-- 8. -->
								<th><b>Loan Duration</b></th><!-- 9. -->
								<th><b>Interest Rate</b></th><!-- 10. -->
								<th>Interest start Date</th><!-- 11. -->
								<th><b>Bank Branch Code</b></th><!-- 12. -->
								<th><b>Bank Acc Number</b></th><!-- 13. -->
								<th><b>Client</b></th><!-- 14. -->
								<th><b>Contarct No</b></th><!-- 15. -->
								<th><b>Branch Contarct Signed</b></th><!-- 16. -->
								<th><b>Amount Paid</b></th><!-- 17. -->
								<th><b>Settlement</b></th><!-- 18. -->
								<th><b>Description of service</b></th><!-- 19. -->
								<th><b>Acc type</b></th><!-- 20. -->
								<th><b>Empty</b></th><!-- 21. -->
								<th><b>Empty</b></th><!-- 22. -->
								<th><b>Empty</b></th><!-- 23. -->
								<th><b>Contact 1</b></th><!-- 24. -->
								<th><b>Relation</b></th><!-- 25. -->
								<th><b>Telno</b></th><!-- 26. -->
								<th><b>Contact 2</b></th><!-- 27. -->
								<th><b>Relation</b></th><!-- 28. -->
								<th><b>Telno</b></th><!-- 29. -->
								<th><b>Contact 3</b></th><!-- 30. -->
								<th><b>Relation</b></th><!-- 31. -->
								<th><b>Telno</b></th><!-- 32. -->
								<th><b>Contact 4</b></th><!-- 33. -->
								<th><b>Relation</b></th><!-- 34. -->
								<th><b>Telno</b></th><!-- 35. -->
								<th><b>Contact 5</b></th><!-- 36. -->
								<th><b>Relation</b></th><!-- 37. -->
								<th><b>Telno</b></th><!-- 38. -->
								<th><b>Add Field 1</b></th><!-- 39. -->
								<th><b>Add Field 2</b></th><!-- 40. -->
								<th><b>Add Field 3	(Office use only)</b></th><!-- 41. -->
								<th><b>Client Structure (Office use only)</b></th><!-- 42. -->
								<th><b>(Office Use Only)</b></th><!-- 43. -->
								<th><b>Bank Name, eg ABSA / FNB</b></th><!-- 44. -->
								<th><b>Acc type, eg. Cheque / Savings</b></th><!-- 45. -->
								<th><b>Acc type code, Cheque =1 / Savings = 2</b></th><!-- 46. -->
		                </tr>
		              </thead>
		              <tbody>
					  
		              <?php
					 //  include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					   $username = $_SESSION['username'];
					if ( !empty($_POST))
					   {
					     $username = $_POST['customerid'];
					   }
					   $sql = "";
					   $q = "";
					   // Filter Approval
	/*				   else
 					   echo '<h2> CustomerId = '.$username.'</h2>';  */
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : Customer 										   //
/////////////////////////////////////////////////////////////////////////////////////////////
				if($_SESSION['role'] == 'customer')
				{
					   if(!empty($ExecApproval))
					   {
					   if($ExecApproval != "*")
					   {
					   //$data = '';
					     if(!empty($From))
						 {
					     $sql = 'SELECT * FROM loanapp as L WHERE Customerid = ? AND ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($username,$ExecApproval,$From,$To));
						 }
						 else
						 {
						 $sql = 'SELECT * FROM customer as C, loanapp as L WHERE C.Customerid = ? AND ExecApproval = ? ORDER BY C.CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($username,$ExecApproval));
						 }
						//$data = $q->fetchAll();
					   }
					   else
					   {
						 if(!empty($From))
						 {
							 $sql = 'SELECT * FROM loanapp as L WHERE Customerid = ? AND DATE(DateAccpt) BETWEEN ? AND ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($username,$From,$To));
						 }
						 else
						 {
						  $sql = 'SELECT * FROM loanapp as L WHERE Customerid = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($username));
						 }
						$data = $q->fetchAll();
					   }
					  }
					 }
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////
					else if($_SESSION['role'] == 'admin')
					{
					//echo '<h2> '.$ExecApproval.'</h2>';
					  if(!empty($ExecApproval))
					   {
					   if($ExecApproval !== "*") // !== Not Identical Is Not *
					   {
					   					//	echo '<h2> CustomerId = '.$ExecApproval.'</h2>';
					   if(!empty($customerid))
						 {
						  if(!empty($From))
						  {
						  $sql = 'SELECT * FROM loanapp WHERE CustomerId = ? AND  ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval,$From,$To));
						  $data = $q->fetchAll();
						  }
						  else
						  {
						  $sql = 'SELECT * FROM loanapp as L WHERE Customerid = ? AND  ExecApproval = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$ExecApproval));
						  $data = $q->fetchAll();
						  }
						 }
					     else if(!empty($From))
						 {
					     $sql = 'SELECT * FROM loanapp as L WHERE ExecApproval = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($ExecApproval,$From,$To));
						 $data = $q->fetchAll();
						 }
						 else
						 {
						  $sql = 'SELECT * FROM loanapp WHERE ExecApproval = ?';
						  $q = $pdo->prepare($sql);
					      $q->execute(array($ExecApproval));
						  $data = $q->fetchAll();
						/*  $sql = 'SELECT * FROM customer as C, loanapp as L WHERE C.Customerid = ? AND ExecApproval = ? ORDER BY C.CustomerId DESC ';
					     $q = $pdo->prepare($sql);
					     $q->execute(array($username,$ExecApproval));
						 $data = $q->fetchAll(); */
						
						 }
					   }
					   else // -- ExecApproval is *
					   {
						 if(!empty($customerid))
						 {
						 if(!empty($From))
						 {
						  $sql = 'SELECT * FROM loanapp WHERE CustomerId = ? AND DATE(DateAccpt) BETWEEN ? AND ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid,$From,$To));
						  $data = $q->fetchAll();
						 }
						 else
						 {
						  $sql = 'SELECT * FROM loanapp WHERE CustomerId = ? ORDER BY CustomerId DESC ';
					      $q = $pdo->prepare($sql);
					      $q->execute(array($customerid));
						  $data = $q->fetchAll();
						 }
						 }
						 else if(!empty($From))
						 {
							 $sql = 'SELECT * FROM loanapp WHERE DATE(DateAccpt) BETWEEN ? AND ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($From,$To));
							 $data = $q->fetchAll();
						 }
						 else
						 {
					/* 	  $sql = 'SELECT * FROM loanapp WHERE ExecApproval = ?';
						  $q = $pdo->prepare($sql);
					      $q->execute(array($ExecApproval));
						  $data = $q->fetchAll(); 	 */
							// -- All Records
							$sql = 'SELECT * FROM loanapp as L ORDER BY CustomerId DESC';
							$data = $pdo->query($sql);
						 }
					   }
					  }
					}
					// -- Total Income
					$tot_income = 0.00;
					$tot_loan = 0.00;
					$tot_repayment = 0.00;
					// -- Total Income
					$tot_outstanding = 0.00;
					$tot_paid = 0.00;
					$tot_repaymentInvoice = 0.00;
					$repaymentAmount = null;
					$phone = '';
					$indexCount = 0;
			   	    $indexInvoiceSent = 0;

	 				  foreach ($data as $row)
					  {

					  // -- Get Customer Data name.
							 $sql = 'SELECT * FROM customer WHERE CustomerId = ?';
							 $q = $pdo->prepare($sql);
							 $q->execute(array($row['CustomerId']));
							 $dataCustomer = $q->fetchAll();
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //
					  $customerid = $row['CustomerId'];
					 // -- Get Payment Invoices from common Tab Data.
					  $sql =  "select * from common where customer_identification = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($customerid));
					  $dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
					 $dataInvoice = null;
					 $ApplicationId = $row['ApplicationId'];
			   // $dataCommon brings all the Invoices related to a client.
			   
			   foreach($dataCommon as $rowCommon)
			   {
					 $invoice_id = $rowCommon['id'];
 					 // -- Get Payment Details Data.
					  $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($invoice_id ,$ApplicationId));
					  $dataInvoice = $q->fetchAll(PDO::FETCH_ASSOC);
				// For each Invoice Get the Payments related to the Loan Application Id
					 foreach ($dataInvoice as $rowInvoice)
					  {
					  $paymentid = $rowInvoice['id'];
					  $iddocument = $rowInvoice['FILE'];
					  $path=str_replace(" ", '%20', $iddocument);
					  $tot_paid = $tot_paid + $rowInvoice['amount'];
					   }
			}	// End all Customer Invoices
					 if($dataInvoice != null)
					   {
					     $tot_outstanding = $repaymentAmount - $tot_paid;
					   }
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //
							 foreach ($dataCustomer as $rowCustomer)
							 {
								$FirstName = $rowCustomer['FirstName'];
								$LastName = $rowCustomer['LastName'];
								$Title = $rowCustomer['Title'];
								$phone = $rowCustomer['phone'];
							 }
							 
								
								
								
$typecode = null;
if($row['accounttype'] == 'Cheque'){$typecode=1;}else{$typecode=2;}

// -----------------------------  16.04.2017 ------------------
// -- Bank Name & Account Type Name 
// ------------------------------------------------------------	
$conv_bankname    = '';
$conv_accounttype = '';

$BankName = $row['bankname'];
$AccountType = $row['accounttype'];
$accounttySelect = 0;
$accounttypeid = '';
$accounttySelect = $AccountType;
$accounttypedesc = '';
$count = 0; 

foreach($dataAccountType as $rowA)
{   
	$accounttypeid = $rowA['accounttypeid'];
	$accounttypedesc = $rowA['accounttypedesc'];
	// Select the Selected Role 
	if($accounttypeid == $accounttySelect)
	{
	   $conv_accounttype = $accounttypedesc;
	}
	$count= $count + 1;
}
//$conv_accounttype = $dataAccountType->rowCount();//$accounttySelect;//$dataAccountType->rowCount();//$AccountType ;
// -- Get the Account Type.
if(empty($conv_accounttype))
 {
	$conv_accounttype = "No Account Type";
 }
 
// -- Banks ----- //
$bankid = '';
$bankSelect = $BankName;
$bankdesc = '';

foreach($dataBanks as $rowB)
{
$bankid = $rowB['bankid'];
$bankdesc = $rowB['bankname'];
												// Select the Selected Role 
		if($bankid == $bankSelect)
		{
		   $conv_bankname = $bankdesc;
		}
}
										
if(empty($conv_bankname))
{
    $conv_bankname = 'No Banks';
} 
											
// -----------------------------  16.04.2017 ------------------
// -- Bank Name & Account Type Name 
// ------------------------------------------------------------								
							 // -- add to array.
							 // -- echo"<script>alert(".$indexCount.")</script>";
$Filecontent1 = array( ''.$indexCount.'' => array('Acc'=> $row['CustomerId'], 'Title' =>$Title, 'Surname'=>$LastName, 'First Name'=>$FirstName,'Intials'=>substr($FirstName,0,1),
'Instalment Amount'=>$row['monthlypayment'],'Language'=>'English','Cause Date'=>$row['FirstPaymentDate'],
'Loan Duration'=>$row['LoanDuration'],'Interest Rate'=>number_format($row['InterestRate']),'Interest start Date'=>$row['FirstPaymentDate'],
'Bank Branch Code'=>$row['branchcode'],'Bank Acc Number'=>$row['accountnumber'],'Client'=>'Vogsphere (Pty) Ltd','Contarct No'=>$row['ApplicationId'],'Branch Contarct Signed'=>'by the client','Amount Paid'=>$tot_paid,'Settlement'=>
$tot_outstanding,'Description of service'=>'Personal Loan','Acc type'=>$conv_accounttype,'Empty'=>'001','Empty'=>'001','Empty'=>'001',
'Contact 1'=>$phone,'Relation'=>'001','Telno'=>'001','Contact 2'=>'001','Relation'=>'001','Telno'=>'001',
'Contact 3'=>'001','Relation'=>'001','Telno'=>'001','Contact 4'=>'001','Relation'=>'001','Telno'=>'001',
'Contact 5'=>'001','Relation'=>'001','Telno'=>'001','Add Field 1'=>'001','Add Field 2'=>'001','Add Field 3 	(Office use only)'=>'001',
'Client Structure (Office use only)'=>'001','Bank Name, eg ABSA / FNB'=>'001','(Office Use Only)'=>'001','Bank Name, eg ABSA / FNB'=>$conv_bankname,'Acc type, eg. Cheque / Savings'=>$conv_accounttype,'Acc type code, Cheque =1 / Savings = 2'=>$typecode));

//=========================== Store Links to Email Invoice to APR Accounts ================================ //
$eMailInvoice = '';
if($row['ExecApproval'] == 'APPR')
{
$eMailInvoices[$indexInvoiceSent] = 
'<a class="btn btn-info" href="eInvoice.php?repayment='.$row['Repayment'].'&customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '&email=yes">eMail eStatement</a>';
$indexInvoiceSent = $indexInvoiceSent + 1;
//$eMailInvoices = array_merge($eMailInvoice,$eMailInvoices);
}
// ========================================== 2017.04.23 =================================================== // 
if($indexCount == 0)
{
$Filecontent  = $Filecontent1;
}
else
{
  $Filecontent = array_merge($Filecontent1,$Filecontent);
}
$indexCount = $indexCount + 1;

// -- Encode Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = urlencode(base64_encode($row['CustomerId']));
$ApplicationIdEncoded = urlencode(base64_encode($row['ApplicationId']));
$repaymentEncoded = urlencode(base64_encode($row['Repayment']));
// -- Encode Encrypt 16.09.2017 - Parameter Data.

$href = "'".'href=eInvoice.php?repayment='.$row['Repayment'].'&customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '&email=yes'."'";
						   		echo '<tr>';
								// ======================= send an eStatement - 23.04.2017 ============================//
								echo '<td width=150>';
								echo '<button type="button" class="btn btn-info" data-popup-open="popup-1"  onclick="setupLinks('.$href.')">eMail eStatements</button></td>';
								// ======================= send an eStatement - 23.04.2017 ============================//
								echo '&nbsp;';	
								echo '<td width=150>';
								echo '<a class="btn btn-info" href="eInvoice.php?repayment='.$repaymentEncoded.'&customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">eStatement Details</a></td>';
								echo '&nbsp;';	
								echo '<td width=150>';
							   	echo '<a class="btn btn-info" href="payment.php?repayment='.$repaymentEncoded.'&customerid='.$CustomerIdEncoded. '&ApplicationId='.$ApplicationIdEncoded. '">ePayment Details</a></td>';
								echo '&nbsp;';
							   	if($row['ExecApproval'] == 'APPR')
								{
								echo '<td>Approved</td>';
								}
								if($row['ExecApproval'] == 'PEN')
								{
								echo '<td>Pending</td>';
								}
								if($row['ExecApproval'] == 'CAN')
								{
								echo '<td>Cancelled</td>';
								}
								if($row['ExecApproval'] == 'REJ')
								{
								echo '<td>Rejected</td>';
								}
								if($row['ExecApproval'] == 'SET')
								{
								echo '<td>Settled</td>';
								}
								echo '&nbsp;';
								echo '<td>'. $row['CustomerId'] . '</td>'; // 1.
							   	echo '<td>'. $Title . '</td>';			// 2.
							    echo '<td>'. $FirstName. '</td>';// 3.
							   	echo '<td>'. $LastName. '</td>';// 4.
							   	echo '<td>'. substr($FirstName,0,1). '</td>';//$row['MonthlyIncome'] 5.
							   	echo '<td>'.$row['monthlypayment']. '</td>';//$row['ReqLoadValue']  6.
								echo '<td>English</td>';// $row['Repayment'] 7.
							   	echo '<td>'. $row['FirstPaymentDate'] . '</td>';// 8.
							   	echo '<td>'. $row['LoanDuration'] . 'Months</td>';// 9.
							   	echo '<td>'. number_format($row['InterestRate']) . '% </td>';// 10.
							   	echo '<td>'. $row['FirstPaymentDate'] .'</td>';// 11.
							   	echo '<td>'. $row['branchcode'] . '</td>';// 12.
							   	echo '<td>'. $row['accountnumber'] . '</td>';// 13
							   	echo '<td>Vogsphere (Pty) Ltd</td>';// 14.
							   	echo '<td>'. $row['ApplicationId'] . '</td>';// 8.
							   	echo '<td>By client</td>';// 8.
							   	echo '<td>R'.$tot_paid.'</td>';// 8.
							   	echo '<td>R'. $tot_outstanding. '</td>';// 8.
							   	echo '<td>eloan</td>';// 8.
							   	echo '<td>'.$conv_accounttype.'</td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td>'. $phone . '</td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td> </td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td></td>';// 8.
							   	echo '<td>'.$conv_bankname.'</td>';// 8. // -- 2017.04.16
							   	echo '<td>'.$conv_accounttype.'</td>';// $row['accounttype'] 8. // -- 2017.04.16
							   	echo '<td>'. $typecode.'</td>';// 8.
								
								// -- Totals
								$tot_income = $tot_income + $row['MonthlyIncome'];
								$tot_loan = $tot_loan + $row['ReqLoadValue'];
								$tot_repayment = $tot_repayment + $row['Repayment'];
								
								// --- if Role = CUSTOMER & Approval status is not pending(Customer cannot cancel it.)
								if($_SESSION['role'] == 'customer' AND $row['ExecApproval'] != 'PEN')
								{
										echo '<td width=250>';
										echo '<a class="btn btn-info" href="read.php?customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '">Details</a>';
										echo '&nbsp;';
										echo '</tr>';
								}
								else
								{
							   	//echo '<td width=150>';
							   	//echo '<a class="btn btn-info" href="payment.php?repayment='.$row['Repayment'].'&customerid='.$row['CustomerId']. '&ApplicationId='.$row['ApplicationId']. '">eStatement Details</a>';
							   	//echo '&nbsp;';
							   	echo '</tr>';
								//$data[0] = '';
								}
					   }
						// Total Profit
					   $profit = $tot_repayment - $tot_loan;
					   $_SESSION["Filecontent"] = $Filecontent;
					   Database::disconnect();
					  ?>
				      </tbody>
	            </table>
<!-- ======================== Build a Div of eMail URL to sent to Clients ============================ -->
		<div id="divWebsites" hidden>
			<?php 
			$count = count($eMailInvoices); 
			for($i=0;$i<$count;$i++)
				{
				 echo $eMailInvoices[$i];
				}
			?>	
			</div>
<!-- ======================== 2017.04.23 - Build a Div of eMail URL to sent to Clients =============== -->
				
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->
		
<!--=================================== Javascript for Multiple e-mail send 23.04.2017 ========================-->
<script>
function setupLinks() 
{ 
    var webSites = document.getElementById("divWebsites"); 

    if (webSites) 
    { 
        //assign window.open event to divWebsites links 
        var links = webSites.getElementsByTagName("a"); 
        var i, href, title; 
		
		repayment = 0;
		customerid = 0;
		ApplicationId = 0;
		Email = 0;
		
		indexofEqual = 0;
		MailArray = '';
	    indexCount = 0;
		var Message = '';
		
        for (i=0; i<links.length; i++) 
        { 	
            href = links[i].getAttribute("href"); 
            title = links[i].getAttribute("title"); 
			str  = links[i];
			//alert(href);
			Startpostion = href.indexOf("repayment");
			//alert(Startpostion);
			
			urlString = href.substr(Startpostion); 
			var res = urlString.split("&");
			//alert(res[0]+res[1]+res[2]+res[3]);
			
			repayment = res[0].substr(res[0].indexOf("=") + 1);
			customerid = res[1].substr(res[1].indexOf("=") + 1);
			ApplicationId = res[2].substr(res[2].indexOf("=") + 1);
			Email = res[3].substr(res[3].indexOf("=") + 1);
			
			jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"eInvoice.php",
					data:{repayment:repayment,customerid:customerid,ApplicationId:ApplicationId,email:Email},
					success: function(data)
					{
					  Message = Message+"\n<p>Mail Succesfull sent to customerid :"+customerid+" and For ApplicationId :"+ApplicationId+"</p>";
					 // alert(Message);
					 jQuery("#divSuccess").append(Message);
					 			move();

					 
				    }
				});
			});
        } 
		
    }     
}  

// -- e-mail an eStatement at a time.
function setupLinks(href) 
{ 
//alert(href);
    if (href) 
    { 
        //assign window.open event to divWebsites links 
			var i, href, title; 
		
			repayment = 0;
			customerid = 0;
			ApplicationId = 0;
			Email = 0;
			
			indexofEqual = 0;
			MailArray = '';
			indexCount = 0;
			var Message = '';
		
			str  = href;
			Startpostion = href.indexOf("repayment");
			
			urlString = href.substr(Startpostion); 
			var res = urlString.split("&");
			
			repayment = res[0].substr(res[0].indexOf("=") + 1);
			customerid = res[1].substr(res[1].indexOf("=") + 1);
			ApplicationId = res[2].substr(res[2].indexOf("=") + 1);
			Email = res[3].substr(res[3].indexOf("=") + 1);
						
			jQuery(document).ready(function()
			{
			jQuery.ajax({	
					type: "POST",
  					url:"eInvoice.php",
					data:{repayment:repayment,customerid:customerid,ApplicationId:ApplicationId,email:Email},
					success: function(data)
					{
					  Message = Message+"\n<p>Mail Succesfull sent to customerid :"+customerid+" and For ApplicationId :"+ApplicationId+"</p>";
					 // -- alert(Message);
					 jQuery("#divSuccess").html(Message);
					 			move();

					 
				    }
				});
			});
        
		
    }     
}  
// -- end e-mail.

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		  var elem = document.getElementById("myBar");   
  var width = 10;
        elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() 
{
  var elem = document.getElementById("myBar");   
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }
}
</script>
<!-- ===========================================================================================================-->		