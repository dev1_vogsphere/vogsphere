<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021
require  $filerootpath.'/ffms/ffms_classess.php';
$im_value = 0;

$source_of_income = new Source_of_incomeDAL();
$source_of_incomeList = $source_of_income->Source_of_income($im_value);

$Occupation = new OccupationDAL();
$im_occupationList  = $Occupation->Occupation($im_value);
//print_r($im_occupationList );
$im_occupation_id_1 = '';

// -- MaritalDAL
$Marital = new MaritalStatusDAL();
$MaritalList = $Marital->MaritalStatus($im_value);

// -- MarriageDAL
$Marriage = new MarriageDAL();
$MarriageList = $Marriage->marriage($im_value);

// -- BrokersDAL
$Broker = new BrokerDAL();
$brokerList = $Broker->Broker($im_value);

// -- BranchesDAL
$Branch = new BranchDAL();
$BranchList = $Branch->Branch($im_value);


// -- PolicyDAL options
$Policy = new policyDAL();
$PolicyList = $Policy->policy($im_value);

// -- MemberDAL and BLL 
$MemberDAL = new MemberDAL();
$MemberBO  = new MemberBLL();

// -- AddressDAL and BLL
$AddressDAL = new AddressDAL();
$AddressBO = new AddressBLL();

/* -- Database Declarations and config:*/
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM bank';
$databank= $pdo->query($sql);

$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM entry_class';
$dataentry_class = $pdo->query($sql);


$notification = array();
$notificationsList = array();

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
$Account_Holder='';
$Account_Type  ='';
$Account_Number='';
$Branch_Code   ='';
$No_Payments   ='';
$Amount        ='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class   ='';
$Bank_Reference='';
$contact_number = '';
$Notify = '';

$entry_classSelect = '';
$message = '';
/* -- dropdown selection. */
$Client_ID_TypeArr = array();
$Client_ID_TypeArr[] = 'South African Identification Number';
$Client_ID_TypeArr[] = 'Passport';
$Client_ID_TypeArr[] = 'South African Business Registration Number';
$Client_ID_TypeArr[] = 'Foreign Passport';
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

/*Title*/
$im_titleArr[] = 'Dr'.'|'.'Dr';
$im_titleArr[] = 'Prof'.'|'.'Prof';
$im_titleArr[] = 'Mr'.'|'.'Mr';
$im_titleArr[] = 'Mrs'.'|'.'Mrs';
$im_titleArr[] = 'Ms'.'|'.'Ms';
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

/*Country*/
$countryArr[] = 'South Africa'.'|'.'South Africa';

/*City*/
$cityArr[] 	  = 'Johannesburg'.'|'.'Johannesburg';
$cityArr[] 	  = 'Pretoria'.'|'.'Pretoria';
$cityArr[] 	  = 'Polokwane'.'|'.'Polokwane';

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$bankArr = array();
foreach($databank as $row)
{
	$bankArr[] = $row['bankid'].'|'.$row['bankname'];
}

$dataentry_classArr = Array();
foreach($dataentry_class as $row)
{
	$dataentry_classArr[] = $row['identry_class'].'|'.$row['Entry_Class_Des'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
$FrequencyArr[] = 'Last Working day of the Month';

$NotifyArr[] = 'No';
$NotifyArr[] = 'Yes';

$arrayInstruction = null;

$UserCode = null;
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}
// -- Validate finApplications...
// Declarations
$im_member_id = '87080355450866';
$im_title 				  = 'Mr';
$im_initials 			  = 'Tm';
$im_first_name  		  = 'Tumelo';
$im_Middle_name 		  = ''; 
$im_surname 			  = 'Modise';
$im_gender 			  = 'Male';
$im_Date_of_birth 		  ='2022-05-25'; 
$im_maritalStatus =''; 
$im_marriage =''; 
$im_spouse_id ='91080355450866'; 
$im_address =''; 
$im_Life_State =''; 
$im_language_id ='';

// Spouse
$im_spousetitle ='Ms';
$im_spousefirst_name ='Augusta';
$im_spouseMiddle_name ='';
$im_spouseinitials ='A';
$im_spousesurname ='Modise'; 
$im_spousegender ='Female'; 
$im_spouseDate_of_birth =''; 
$im_maritalStatus =''; 
$im_marriage =''; 
$im_spouseaddress  ='';
$im_spouseLife_State ='';
$im_spouselanguage_id ='';

// address
$im_address_id ='';
$im_suburb_id ='';
$im_line_1='8474';
$im_line_2='';
$im_line_3='8700';

$im_line_4='8686786';
$im_line_5='1019';
$im_line_6='';
$im_line_7='';
$im_line_8='101923';

$im_email='tumelo@gmail.com';


$im_fax='';
$im_cellphone='0731238839';
$im_work_tel='0731238839';
$im_home_tel='0731238839';
$im_comm_preference='';
 
// -- Source of income..
$Income_provider_Email 		 = 'tumelo@gmail.com';
$Income_provider_name        = 'dadad';
$Income_provider_Contact_No  = '01222222222';
$source_of_income_2          = '';
$im_occupation_id_1          = '';

// -- Bank Details
$Account_number 			 = '4065555';	
$Account_type 	 			 = '';
$Account_holder				 = 'Tumelo';
$Bank	 					 = 'ABSA';
$Branch_name 	 			 = '';
$Branch_code 				 = '';
$Personal_number 			 = '232323';	
$Debit_date 				 = '2022-05-29';
$Commence_date				 = '2022-05-29'; 

if (!empty($_POST))
   {

// -- New Address Object
//$NewAddress = new AddressBLL($im_address_id, $im_suburb_id, $im_line_1, $im_line_2, $im_line_3, $im_email, $im_fax, $im_cellphone, $im_work_tel, $im_home_tel, $im_comm_preference);

// -- New Member Object
//$MemberBO  = new MemberBLL($im_member_id, $im_title, $im_first_name, $im_Middle_name, $im_initials, $im_surname, $im_gender, $im_Date_of_birth, $im_maritalStatus, $im_marriage, $im_spouse_id, $im_address, $im_Life_State, $im_language_id);


// -- New Object	
// -- if member is married Spouse details
//$MemberSpouseBO  = new MemberBLL($im_spouse_id, $im_spousetitle, $im_spousefirst_name, $im_spouseMiddle_name, $im_spouseinitials, $im_spousesurname, $im_spousegender, $im_spouseDate_of_birth, $im_maritalStatus, $im_marriage, $im_member_id, $im_spouseaddress, $im_spouseLife_State, $im_spouselanguage_id);
   }
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>
<div class="container background-white bottom-border">
<br/>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
          <a id="step1"  href="#step-1" type="button" class="btn btn-primary btn-circle inactiveLink">1</a>
					<p><strong>Product</strong></p>

        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
            <p><strong>Customer Data</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
            <p><strong>Dependants</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">4</a>
            <p><strong>Method of Payment</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-5" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">5</a>
            <p><strong>Supporting Docs</strong></p>
        </div>
				<div class="stepwizard-step">
						<a href="#step-6" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
						<p><strong>Activation</strong></p>
				</div>
		<div class="stepwizard-step">
			<a href="#step-7" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">6</a>
			<p><strong>Complete</strong></p>
		</div>
    </div>
</div>
</br>
<form id="contact-form2" method="POST" action="#" style="display:block" role="form">
<div class="row setup-content" id="step-1">
	<div class="controls">
		<div class="container-fluid" style="border:1px solid #ccc ">
             </br>
			 <div class="panel-body">


				<div class="row">

					<div class="col-md-3">
				 		<div class="form-group">
				 			<label for="form_branch_name">Branch Name</label>
				 				<select class="form-control" id="branch_name" name="branch_name">
				 					<?php
				 					$idBranchSelect = $branch_name;
				 					foreach($BranchList as $key => $rowData)
				 					{
				 						if($rowData->idBranch  == $idBranchSelect)
				 						{echo  '<option value="'.$rowData->idBranch.'" selected>'.$rowData->branchusercode.' '.$rowData->branchname.'</option>';}
				 						else
				 						{echo  '<option value="'.$rowData->idBranch.'">'.$rowData->branchusercode.' '.$rowData->branchname.'</option>';}
				 					}
				 					if(empty($BranchList ))
				 					{echo  '<option value="0" selected>No branch</option>';}

				 					?>
				 					</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
				  </div>

						 <div class="col-md-3">
						 	 <div class="form-group">
						 		 <label for="form_broker_name">Broker Name</label>
						 			 <select class="form-control" id="broker_name" name="broker_name">
						 				 <?php
						 				 $brokerSelect = $broker_name;
										foreach($brokerList as $key => $rowData )
										{
						 					 if($rowData->idBroker == $brokerSelect)
						 					 {echo  '<option value="'.$rowData->idBroker.'" selected>'.$rowData->brokerfirstname.' '.$rowData->brokersurname.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->idBroker.'">'.$rowData->brokerfirstname.' '.$rowData->brokersurname.'</option>';}
										 }
										 if(empty($brokerList ))
										 {echo  '<option value="0" selected>No branch</option>';}

						 				 ?>
						 				 </select>
						 						<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
						 				 <div class="help-block with-errors"></div>
						 		 </div>
						 </div>

                 <div class="col-md-3">
		                <div class="form-group">
		                  <label for="form_product_service">Product/Service</label>
		                    <select class="form-control" id="product_service" name="product_service">
												  <?php
												  $Policy_idSelect = $product_service;
												  foreach($PolicyList as $key => $rowData)
												  {
													 if($rowData->Policy_id  == $Policy_idSelect)
													  {echo  '<option value="'.$rowData->Policy_id.'" selected>'.$rowData->Policy_name.' - '.$rowData->Policy_desc.'</option>';}
													  else
													  {echo  '<option value="'.$rowData->Policy_id.'">'.$rowData->Policy_name.' - '.$rowData->Policy_desc.'</option>';}
												  }
												  if(empty($PolicyList ))
												  {echo  '<option value="0" selected>No product/service</option>';}

												  ?>
		                      </select>
		                         <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
		                      <div class="help-block with-errors"></div>
		                  </div>
                 </div>

        </div>


           </div>
     </div>
 </div>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
</div>
<br/>

<div class="row setup-content" id="step-2">
	<div class="col-xs-12">
		<div class="col-md-12">
			 <div class="container-fluid" style="border:1px solid #ccc ">
				 <fieldset class="border" >
					 <legend class ="text-left">Main Member Details</legend>
				</fieldset>
         <div class="row">
							 <!--Title-->
							 <div class="col-md-3">
							 	 <div class="form-group">
							 			 <label for="form_Title:">Title<span style="color:red">*</span></label>
							 					 <select class="form-control" id="im_title" name="im_title">
							 						 <?php
							 							 $im_titleSelect = $im_title;
							 						 $rowData = null;
							 						 foreach($im_titleArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $im_titleSelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							 					 </select>
							 											 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 		 <div class="help-block with-errors"></div>
							 	 </div>
							 </div>
							 <!--Initials-->

							 <div class="col-md-3">
							 		<div class="form-group">
							 				<label for="form_Full_Names">Initials<span style="color:red">*</span></label>
							 				<input id="im_initials" type="text" name="im_initials" class="form-control" placeholder="" required="required" maxlength="30" data-error="initialsis required." value = "<?php echo $im_initials;?>">
							 				<div class="help-block with-errors"></div>
							 		</div>
							 </div>

							 <!--Full Names-->
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Full_Names">First Name<span style="color:red">*</span></label>
                         <input id="im_first_name" type="text" name="im_first_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="first_name is required." value = "<?php echo $im_first_name;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
								 <div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Middle_Name">Middle Name</label>
												 <input id="im_Middle_name" type="text" name="im_Middle_name" class="form-control" placeholder="" maxlength="30" value = "<?php echo $im_Middle_name;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
								<div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Account_Holder">Surname<span style="color:red">*</span></label>
												 <input id="im_surname" type="text" name="im_surname" class="form-control" placeholder=""  maxlength="30" data-error="im_surname is required." value = "<?php echo $im_surname;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
									 <label for="form_Gender">Gender<span style="color:red">*</span></label>
									 <label class="radio-inline"><input type="radio" name="im_gender" required="required" value="2" checked />Female</label>
									 <label class="radio-inline"><input type="radio" name="im_gender" value="1"/>Male</label>
									 <div class="help-block with-errors"></div>
								    </div>
							    </div>
							 <div class="col-md-3">
		 						<div class="form-group">
		 							<label for="form_Client_ID_Type">Identification Type</label>
		 									<select class="form-control" id="Client_ID_Type" name="Client_ID_Type">
		 										 <?php
		 										 $Client_ID_TypeSelect = $Client_ID_Type;
		 										 foreach($Client_ID_TypeArr as $row)
		 										 {
		 											 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
		 											 if($rowData[0] == $Client_ID_TypeSelect)
		 											 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
		 											 else
		 											 {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
		 										 }
		 										 ?>
		 										</select>
		 												<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
		 										<div class="help-block with-errors"></div>
		 							</div>
		 					</div>

							<div class="col-md-3">
								 <div class="form-group">
										 <label for="form_IdentificationNumber">Identification Number<span style="color:red">*</span></label>
										 <input id="im_member_id" type="text" name="im_member_id" class="form-control" placeholder="" required="required" maxlength="30" data-error="member_id is required." value = "<?php echo $im_member_id;?>">
										 <div class="help-block with-errors"></div>
								 </div>
							</div>

								 <div class="col-md-3">
									 <div class="form-group">

										<label for="form_Date_of_Birth:">Date of Birth<span style="color:red">*</span></label>
										<input id="im_Date_of_birth" min="<?php echo date('2001-m-d'); ?>" type="date"  required="required" name="im_Date_of_birth" class="form-control" placeholder="aaa" data-error="Date_of_birth is required." value = "<?php echo $im_Date_of_birth;?>">
																<div class="help-block with-errors">
										<div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
											<span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
										</div>
										</div>
									</div>
								 </div>

								<div class="col-md-3">
								 <div class="form-group">
									 <label for="form_Client_ID_Type">Marital Status</label>
											 <select class="form-control" id="im_maritalStatus" name="im_maritalStatus">
													<?php
													$im_maritalStatusSelect = $im_maritalStatus;
		
													foreach($MaritalList as $key => $rowData )
										{
						 					 if($rowData->MaritalStatus_id == $im_maritalStatusSelect)
						 					 {echo  '<option value="'.$rowData->MaritalStatus_id.'" selected>'.$rowData->MaritalStatus_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->MaritalStatus_id.'">'.$rowData->MaritalStatus_name.'</option>';}
										 }
										 if(empty($MaritalList ))
										 {echo  '<option value="0" selected>No MaritalStatus</option>';}
													?>
												 </select>
														 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
												 <div class="help-block with-errors"></div>
									 </div>
							 </div>

							 <div class="col-md-3">
								<div class="form-group">
									<label for="form_Client_ID_Type">Type of Marriage</label>
											<select class="form-control" id="im_marriage" name="im_marriage">
												 <?php
												 $im_marriageSelect = $im_marriage;
												 foreach($MarriageList as $key => $rowData )
										{
						 					 if($rowData->marriage_id == $im_marriageSelect)
						 					 {echo  '<option value="'.$rowData->marriage_id.'" selected>'.$rowData->marriage_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->marriage_id.'">'.$rowData->marriage_name.'</option>';}
										 }
										 if(empty($MarriageList ))
										 {echo  '<option value="0" selected>No marriage</option>';}
												 ?>
												</select>
														<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
												<div class="help-block with-errors"></div>
									</div>
							</div>

				</div>
			</br>

				<div class="row">
             </div>

           </div>
           </br>

					<div class="container-fluid" style="border:1px solid #ccc ">
					<fieldset class="border" >
					<legend class ="text-left">Main Member Contact Details</legend>
					</fieldset>
						</br>
						 <div class="row">

						 <div class="col-md-3">
							 <div class="form-group">
									 <label for="form_Contact_Number">Cellphone Number<span style="color:red">*</span></label>
									 <input id="im_cellphone" type="text" name="im_cellphone" class="form-control" placeholder="" required="required" maxlength="30" data-error="im_cellphone is required." value = "<?php echo $im_cellphone;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>

						<div class="col-md-3">
			 			 <div class="form-group">
			 					 <label for="form_Contact_Number">Work Number<span style="color:red">*</span></label>
			 					 <input id="im_work_tel" type="text" name="im_work_tel" class="form-control" placeholder="" required="required" maxlength="30" data-error="work_tel is required." value = "<?php echo $im_work_tel;?>">
			 					 <div class="help-block with-errors"></div>
			 			 </div>
			 		</div>

					<div class="col-md-3">
		 			 <div class="form-group">
		 					 <label for="form_Contact_Number">Home Number<span style="color:red">*</span></label>
		 					 <input id="im_home_tel" type="text" name="im_home_tel" class="form-control" placeholder="" required="required" maxlength="30" data-error="im_home_tel is required." value = "<?php echo $im_home_tel;?>">
		 					 <div class="help-block with-errors"></div>
		 			 </div>
		 		</div>

				<div class="col-md-3">
	 			 <div class="form-group">
	 					 <label for="form_Contact_Number">Email Address<span style="color:red">*</span></label>
	 					 <input id="im_email" type="text" name="im_email" class="form-control" placeholder="" required="required" maxlength="30" data-error="email is required." value = "<?php echo $im_email;?>">
	 					 <div class="help-block with-errors"></div>
	 			 </div>
	 		</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">PO Box</label>
							<input id="im_line_1" type="text" name="im_line_1" class="form-control" placeholder="" required="required" maxlength="30" data-error="P.O.Box is required." value = "<?php echo $im_line_1;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Town/City</label>
		 					 <select class="form-control" id="im_line_2" name="im_line_2">
							 						 <?php
							 						 $citySelect = $im_line_2;
							 						 $rowData = null;
							 						 foreach($cityArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $citySelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							</select>					
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Country</label>
		 					 <select class="form-control" id="im_line_3" name="im_line_3">
							 						 <?php
							 							 $countrySelect = $im_line_3;
							 						 $rowData = null;
							 						 foreach($countryArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $countrySelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							</select>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Postal</label>
							<input id="im_line_4" type="text" name="im_line_4" class="form-control" placeholder="" required="required" maxlength="30" data-error="Postal is required." value = "<?php echo $im_line_4;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Street/Home number</label>
							<input id="im_line_5" type="text" name="im_line_5" class="form-control" placeholder="" required="required" maxlength="30" data-error="Street is required." value = "<?php echo $im_line_5;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Town/City</label>
		 					 <select class="form-control" id="im_line_6" name="im_line_6">
							 						 <?php
							 						 $citySelect1 = $im_line_6;
							 						 $rowData = null;
							 						 foreach($cityArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $citySelect1)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							</select>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Country</label>
		 					 <select class="form-control" id="im_line_7" name="im_line_7">
							 						 <?php
							 						 $countrySelect1 = $im_line_7;
							 						 $rowData = null;
							 						 foreach($countryArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $countrySelect1)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							</select>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Postal</label>
							<input id="im_line_8" type="text" name="im_line_8" class="form-control" placeholder="" required="required" maxlength="30" data-error="Postal is required." value = "<?php echo $im_line_8;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>



		</div>

		<div class="row">
				 </div>

			 </div>
			 </br>

			 <!--Spouse Details-->
			<div class="container-fluid" style="border:1px solid #ccc ">
			<fieldset class="border" >
			<legend class ="text-left">Source of Income</legend>
			</fieldset>
				</br>
				 <div class="row">

					 <div class="col-md-3">
						 <div class="form-group">
								 <label for="form_Title:">Occupation<span style="color:red">*</span></label>
										 <select class="form-control" id="im_occupation_id_1" name="im_occupation_id_1">
											 <?php
												 $im_occupation_id_1Select = $im_occupation_id_1;
										foreach($im_occupationList as $key => $rowData )
										{
						 					 if($rowData->Occupation_id == $im_occupation_id_1Select)
						 					 {echo  '<option value="'.$rowData->Occupation_id.'" selected>'.$rowData->Occupation_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->Occupation_id.'">'.$rowData->Occupation_name.'</option>';}
										 }
										 if(empty($im_occupationList ))
										 {echo  '<option value="0" selected>No Occupation</option>';}

											 ?>
										 </select>
																 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>

					 <div class="col-md-3">
						 <div class="form-group">
								 <label for="form_Title:">Source of Income<span style="color:red">*</span></label>
										 <select class="form-control" id="source_of_income_2" name="source_of_income_2">
											 <?php
											  $source_of_income_2Select = $source_of_income_2;
										foreach($source_of_incomeList as $key => $rowData )
										{
						 					 if($rowData->Source_of_income_id == $source_of_income_2Select)
						 					 {echo  '<option value="'.$rowData->Source_of_income_id.'" selected>'.$rowData->Source_of_income_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->Source_of_income_id.'">'.$rowData->Source_of_income_name.'</option>';}
										 }
										 if(empty($source_of_incomeList ))
										 {echo  '<option value="0" selected>No Source of income</option>';}

											?>
										 </select>
																 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Income provider name<span style="color:red">*</span></label>
									<input id="Income_provider_name" type="text" name="Income_provider_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="Income_provider_name is required." value = "<?php echo $Income_provider_name;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Income provider Contact No<span style="color:red">*</span></label>
									<input id="Income_provider_Contact_No" type="text" name="Income_provider_Contact_No" class="form-control" placeholder="" required="required" maxlength="30" data-error="Income_provider_Contact_No is required." value = "<?php echo $Income_provider_Contact_No;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Income provider Email Address<span style="color:red">*</span></label>
									<input id="Income_provider_Email" type="text" name="Income_provider_Email" class="form-control" placeholder="" required="required" maxlength="30" data-error="Income provider Email is required." value = "<?php echo $Income_provider_Email;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>

				</div>

		<div class="row">
				 </div>

			 </div>
			 </br>

			 <!--Spouse Details-->
			<div class="container-fluid" style="border:1px solid #ccc ">
			<fieldset class="border" >
			<legend class ="text-left">Spouse Details</legend>
			</fieldset>
				</br>
				 <div class="row">

					 <div class="col-md-3">
						 <div class="form-group">
								 <label for="form_Title:">Title<span style="color:red">*</span></label>
										 <select class="form-control" id="im_spousetitle" name="im_spousetitle">
											 <?php
												 $im_spousetitleSelect = $im_spousetitle;
											 $rowData = null;
											 foreach($im_titleArr as $row)
											 {
												 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												 if($rowData[0] == $im_spousetitleSelect)
												 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
												 else
												 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
											 }?>
										 </select>
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>
					 <!--Initials-->
					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Initials<span style="color:red">*</span></label>
									<input id="im_spouseinitials" type="text" name="im_spouseinitials" class="form-control" placeholder="" required="required" maxlength="30" data-error="im_spouseinitials is required." value = "<?php echo $im_spouseinitials;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>
					 <!--Full Names-->
						 <div class="col-md-3">
								 <div class="form-group">
										 <label for="form_Full_Names">First Name<span style="color:red">*</span></label>
										 <input id="im_spousefirst_name" type="text" name="im_spousefirst_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="im_spousefirst_name is required." value = "<?php echo $im_spousefirst_name;?>">
										 <div class="help-block with-errors"></div>
								 </div>
						 </div>
						 <div class="col-md-3">
								 <div class="form-group">
										 <label for="form_Middle_Name">Middle Name</label>
										 <input id="im_spouseMiddle_name" type="text" name="im_spouseMiddle_name" class="form-control" placeholder="" maxlength="30" value = "<?php echo $im_spouseMiddle_name;?>">
								 </div>
						 </div>
						 <div class="col-md-3">
								 <div class="form-group">
										 <label for="form_Account_Holder">Surname<span style="color:red">*</span></label>
										 <input id="im_spousesurname" type="text" name="im_spousesurname" class="form-control" placeholder=""  maxlength="30" data-error="im_spousesurname is required." value = "<?php echo $im_spousesurname;?>">
										 <div class="help-block with-errors"></div>
								 </div>
						 </div>
						 <div class="col-md-3">
							<div class="form-group">
								<label for="form_Client_ID_Type">Identification Type</label>
										<select class="form-control" id="Client_ID_Type" name="Client_ID_Type">
											 <?php
											 $Client_ID_TypeSelect = $Client_ID_Type;
											 foreach($Client_ID_TypeArr as $row)
											 {
												 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												 if($rowData[0] == $Client_ID_TypeSelect)
												 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
												 else
												 {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
											 }
											 ?>
											</select>
													<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
											<div class="help-block with-errors"></div>
								</div>
						</div>

						<div class="col-md-3">
							 <div class="form-group">
									 <label for="form_IdentificationNumber">Identification Number<span style="color:red">*</span></label>
									 <input id="im_spouse_id" type="text" name="im_spouse_id" class="form-control" placeholder="" required="required" maxlength="30" data-error="spouse_id is required." value = "<?php echo $im_spouse_id;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>
				</div>



	</div>
	</br>
        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>

<div class="row setup-content" id="step-3">
	<div class="col-xs-12">
		<div class="col-md-12">
		<div class="card">
	  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	    Dependant Detials
	  </h3>
	  <div class="card-body">
	    <div id="table" class="table-editable">
	      <span class="table-add float-right mb-3 mr-2"
	        ><a href="#!" class="text-success"
	          ><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a
	      ></span>
	      <table class="table table-bordered table-responsive-md table-striped text-center">
	        <thead>
	          <tr>
	            <th class="text-center">First Name</th>
	            <th class="text-center">Middle Name</th>
	            <th class="text-center">Surname</th>
	            <th class="text-center">Identity Number</th>
	            <th class="text-center">Relationship</th>
	            <th class="text-center">Remove</th>
	          </tr>
	        </thead>
	        <tbody>
	          <tr>
	            <td class="pt-3-half" contenteditable="true">Aurelia</td>
	            <td class="pt-3-half" contenteditable="true">Vega</td>
	            <td class="pt-3-half" contenteditable="true">Baloyi</td>
	            <td class="pt-3-half" contenteditable="true">9912265761086</td>
	            <td class="pt-3-half" contenteditable="true">Daughter</td>
	            <td>
	              <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
	                  Remove
	                </button></span
	              >
	            </td>
	          </tr>
	          <!-- This is our clonable table line -->
	          <tr>
	            <td class="pt-3-half" contenteditable="true">Guerra</td>
	            <td class="pt-3-half" contenteditable="true">Cortez</td>
	            <td class="pt-3-half" contenteditable="true">Insectus</td>
	            <td class="pt-3-half" contenteditable="true">9912265761086</td>
	            <td class="pt-3-half" contenteditable="true">Son</td>
	            <td>
	              <span class="table-remove"
	                ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
	                  Remove
	                </button></span
	              >
	            </td>
	          </tr>
	          <!-- This is our clonable table line -->
	          <tr>
	            <td class="pt-3-half" contenteditable="true">Guadalupe </td>
	            <td class="pt-3-half" contenteditable="true">House</td>
	            <td class="pt-3-half" contenteditable="true">Isotronic</td>
	            <td class="pt-3-half" contenteditable="true">9912265761086</td>
	            <td class="pt-3-half" contenteditable="true">Brother</td>
	            <td>
	              <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
	                  Remove
	                </button></span
	              >
	            </td>
	          </tr>
	          <!-- This is our clonable table line -->
	          <tr class="hide">
	            <td class="pt-3-half" contenteditable="true">Elisa </td>
	            <td class="pt-3-half" contenteditable="true">Gallagher</td>
	            <td class="pt-3-half" contenteditable="true">Portica</td>
	            <td class="pt-3-half" contenteditable="true">9812265761086</td>
	            <td class="pt-3-half" contenteditable="true">Sister</td>
	            <td>
	              <span class="table-remove"><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
	                  Remove
	                </button></span
	              >
	            </td>
	          </tr>
	        </tbody>
	      </table>

	    </div>
	  </div>
 <!--Extended family-->
 <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	 Extended Family Details
 </h3>
 <div class="card-body">
	 <div id="table2" class="table-editable">
		 <span class="table2-add float-right mb-3 mr-2"
			 ><a href="#!" class="text-success" ><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a
		 ></span>
		 <table id="table2" class="table table-bordered table-responsive-md table-striped text-center">
			 <thead>
				 <tr>
					 <th class="text-center">First Name</th>
					 <th class="text-center">Middle Name</th>
					 <th class="text-center">Surname</th>
					 <th class="text-center">Identity Number</th>
					 <th class="text-center">Relationship</th>
					 <th class="text-center">Remove</th>
				 </tr>
			 </thead>
			 <tbody>
				 <tr>
					 <td class="pt-3-half" contenteditable="true">Aurelia</td>
					 <td class="pt-3-half" contenteditable="true">Vega</td>
					 <td class="pt-3-half" contenteditable="true">Baloyi</td>
					 <td class="pt-3-half" contenteditable="true">9912265761086</td>
					 <td class="pt-3-half" contenteditable="true">Daughter</td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>
				 <!-- This is our clonable table line -->
				 <tr>
					 <td class="pt-3-half" contenteditable="true">Guerra</td>
					 <td class="pt-3-half" contenteditable="true">Cortez</td>
					 <td class="pt-3-half" contenteditable="true">Insectus</td>
					 <td class="pt-3-half" contenteditable="true">9912265761086</td>
					 <td class="pt-3-half" contenteditable="true">Son</td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>
				 <!-- This is our clonable table line -->
				 <tr>
					 <td class="pt-3-half" contenteditable="true">Guadalupe </td>
					 <td class="pt-3-half" contenteditable="true">House</td>
					 <td class="pt-3-half" contenteditable="true">Isotronic</td>
					 <td class="pt-3-half" contenteditable="true">9912265761086</td>
					 <td class="pt-3-half" contenteditable="true">Brother</td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>
				 <!-- This is our clonable table line -->
				 <tr class="hide">
					 <td class="pt-3-half" contenteditable="true">Elisa </td>
					 <td class="pt-3-half" contenteditable="true">Gallagher</td>
					 <td class="pt-3-half" contenteditable="true">Portica</td>
					 <td class="pt-3-half" contenteditable="true">9812265761086</td>
					 <td class="pt-3-half" contenteditable="true">Sister</td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>
			 </tbody>
		 </table>
	 </div>
 </div>

 <!--Nominated benefiaciary-->
 <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	Nominated Benefiaciary
 </h3>
 <div class="card-body">
	 <div id="table3" class="table-editable">
		 <span class="table3-add float-right mb-3 mr-2"
			 ><a href="#!" class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a
		 ></span>
		 <table id="table3" class="table table-bordered table-responsive-md table-striped text-center">
			 <thead>
				 <tr>
					 <th class="text-center">First Name</th>
					 <th class="text-center">Middle Name</th>
					 <th class="text-center">Surname</th>
					 <th class="text-center">Identity Number</th>
					 <th class="text-center">Relationship</th>
					 <th class="text-center">Percentage</th>
					 <th class="text-center">Remove</th>
				 </tr>
			 </thead>
			 <tbody>
				 <tr>
					 <td class="pt-3-half" contenteditable="true">Aurelia</td>
					 <td class="pt-3-half" contenteditable="true">Vega</td>
					 <td class="pt-3-half" contenteditable="true">Baloyi</td>
					 <td class="pt-3-half" contenteditable="true">9912265761086</td>
					 <td class="pt-3-half" contenteditable="true">Daughter</td>
					 <td class="pt-3-half" contenteditable="true">100%</td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>

			 </tbody>
		 </table>
	 </div>
 </div>

	</div>	<!--CLose card-->

		</br>
    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>




<div class="row setup-content" id="step-4">

	<div class="container-fluid" style="border:1px solid #ccc ">
		</br>
		 <div class="panel-body">


				<div class="row">
						<div class="col-md-3">
						<div class="form-check">
							<label class="form-check-label" for="flexCheckDefault">
								EFT
							</label>
						  <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
						</div>
						</div>

						<div class="col-md-3">
						<div class="form-check">
							<label class="form-check-label" for="flexCheckDefault">
								Debit Order
							</label>
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">

						</div>
						</div>

						<div class="col-md-3">
						<div class="form-check">
							<label class="form-check-label" for="flexCheckDefault">
								Debicheck
							</label>
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">

						</div>
						</div>

						<div class="col-md-3">
						<div class="form-check">
							<label class="form-check-label" for="flexCheckDefault">
								Persal
							</label>
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
						</div>
						</div>

				</div>



				</br>

				<div class="row">

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account holder<span style="color:red">*</span></label>
								 <input id="Account_holder " type="text" name="Account_holder " class="form-control" placeholder="" required="required" maxlength="30" data-error="Account_holder  is required." value = "<?php echo $Account_holder;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
				 		<div class="form-group">
				 			<label for="form_branch_name">Account Type</label>
				 				<select class="form-control" id="Account_type" name="Account_type">
				 					<?php
				 					$Account_typeSelect = $Account_type;
				 					foreach($Account_TypeArr as $key => $row)
				 					{
										$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
				 						if($rowData[0]  == $Account_typeSelect)
				 						{echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
				 						else
				 						{echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
				 					}
				 					if(empty($Account_TypeArr ))
				 					{echo  '<option value="0" selected>No Account_types</option>';}

				 					?>
				 					</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
				  </div>

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account Number<span style="color:red">*</span></label>
								 <input id="Account_number" type="text" name="Account_number" class="form-control" placeholder="" required="required" maxlength="30" data-error="Account_number is required." value = "<?php echo $Account_number;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
						 	 <div class="form-group">
						 		 <label for="form_broker_name">Bank</label>
						 			 <select class="form-control" id="Bank" name="Bank">
						 				 <?php
						 				 $BankSelect = $Bank;
										foreach($bankArr as $key => $row )
										{
										      $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
						 					 if($rowData[0] == $BankSelect)
						 					 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										 }
										 if(empty($bankArr ))
										 {echo  '<option value="0" selected>No Banks</option>';}

						 				 ?>
						 				 </select>
						 						<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
						 				 <div class="help-block with-errors"></div>
						 		 </div>
						 </div>
					 </div>

			<div class="row">

						<div class="col-md-3">
							 <div class="form-group">
									 <label for="form_IdentificationNumber">Reference no<span style="color:red">*</span></label>
									 <input id="Personal_number" type="text" name="Personal_number" class="form-control" placeholder="" required="required" maxlength="30" data-error="Reference no. is required." value = "<?php echo $Personal_number;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>

						<div class="col-md-3">
							<div class="form-group">

							 <label for="form_Date_of_Birth:">Commencement date<span style="color:red">*</span></label>
							 <input id="Commence_date" min="<?php echo date('Y-m-d'); ?>" type="date"  name="Commence_date" class="form-control" placeholder="aaa" data-error="Commence_date is required." value = "<?php echo $Commence_date;?>">
						 </div>
						</div>

						<div class="col-md-4">
							<div class="form-group">

							 <label for="form_Date_of_Birth:">Please debit my account on<span style="color:red">*</span></label>
							 <input id="Debit_date" min="<?php echo date('Y-m-d'); ?>" type="date"  name="Debit_date" class="form-control" placeholder="aaa" data-error="Debit_date is required." value = "<?php echo $Debit_date;?>"
							 onChange="applyrules(this)">
													 <div class="help-block with-errors">
							 <div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
								 <span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
							 </div>
							 </div>
						 </div>
						</div>

				</div>

		<div class="row">
			<div class="col-md-3">
									<div class="form-group">
											<label for="Notify:"><strong>Notify:</strong></label>
											<select class="form-control" id="Notify" name="Notify">
						 <?php
						 $NotifySelect = $Notify;

					 foreach($NotifyArr as $row)
					 {
						 if($row == $NotifySelect)
						 {echo  '<option value="'.$row.'" selected>'.$row.'</option>';}
						 else
						 {echo  '<option value="'.$row.'">'.$row.'</option>';}
					 }
					 ?>
										 </select>
										 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
											<div class="help-block with-errors"></div>
									</div>
							</div>
		</div>




			</div>
	</div>

</br>
</br>

<!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Save</button>-->
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>



<div class="row setup-content" id="step-5">
		 <div class="controls">
		 <div class="container-fluid" style="border:1px solid #ccc ">
		 </br>
			 <?php
	 		$ApplicationId = '';//$data['ApplicationId'];
	 		$customerid = '';//$data['CustomerId'];
	 		$hash="&guid=".md5(date("h:i:sa"));
			$ApplicationIdEncoded = '';
	 		// -- BOC Encode Change - 16.09.2017.
	 		echo "<a class='btn btn-primary' href=upload?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	 		// -- EOC Encode Change - 16.09.2017.
	 		?>
		</br>

     <div class="row">
			 <table class = "table table-hover">
			    <caption>
			    <h3><b>Uploaded Documents</b></h3></caption>

			    <thead>
			       <tr style ="background-color: #f0f0f0">
					  <th>Upload</th>
			          <!-- <th>View Document</th>-->
			          <th>Type</th>
			       </tr>
			    </thead>
			 <!----------------- Read AppLoans document names ----------------------------->
			  <?php

			     // $pdo = Database::connectDB();
			 	 //$tbl_name="loanapp"; // Table name
			 	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
			 	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid= ? and ApplicationId=?";

			 	 // -- ID Document
			 	 $iddocument = '';

			 	 // -- Contract Agreement
			 	 $contract = '';

			 	 // -- Bank statement
			 	 $bankstatement = '';

			 	 // -- Proof of income
			 	 $proofofincome = '';

			 	 // -- Fica
			 	 $fica = '';

			 	 $consentform = '';
			 	 $other = '';
			    	 $other2 = '';
			 	 // --

			 	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
			 	 $debitform = '';
			 	 $other3    = "";
			 	 $other4    = "";
			 	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

			 	$NoFIle = "";

			 	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
			 	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

			 // Mysql_num_row is counting table row
			 	//$count = mysql_num_rows($result);


			 		/*$pdo = Database::connect();
			 		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			 		$sql =  $check_user;
			 		$q = $pdo->prepare($sql);
			 		$q->execute(array($customerid,$ApplicationId));
			 		$row = $q->fetch(PDO::FETCH_ASSOC);
			 		$count = $q->rowCount();*/
				$count = 1;
			 	if($count >= 1)
			     {

			 	//$row = mysql_fetch_array($result);

/*
			 	$iddocument  =  $row['FILEIDDOC'];
			 	$contract = $row['FILECONTRACT'];
			 	$bankstatement = $row['FILEBANKSTATEMENT'];
			 	$proofofincome = $row['FILEPROOFEMP'];
			 	$fica = $row['FILEFICA'];

			    // -- BOC 01.10.2017 - Upload Credit Reports.
			    	$creditreport = $row['FILECREDITREP'];

			 	$consentform = $row['FILECONSENTFORM'];
			 	$other = $row['FILEOTHER'];
			    	$other2 = $row['FILEOTHER2'];
			    // -- EOC 01.10.2017 - Upload Credit Reports.

			    	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
			 	 $debitform = $row['FILEDEBITFORM'];
			 	 $other3    = $row['FILEOTHER3'];
			 	 $other4    = $row['FILEOTHER4'];
			 	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.
*/

			    // -- BOC 01.10.2017 - Alternative if no file upload.
			    if(empty($iddocument)){$iddocument = $NoFIle;}
			    if(empty($contract)){ $contract = $NoFIle;}
			    if(empty($bankstatement)){$bankstatement = $NoFIle;}
			    if(empty($proofofincome)){$proofofincome = $NoFIle;}
			    if(empty($fica)){ $fica = $NoFIle;}
			    if(empty($creditreport)){  $creditreport = $NoFIle;}
			   /* if(empty($consentform)){  $consentform = $NoFIle;}
			    if(empty($other)){  $other = $NoFIle;}
			    if(empty($other2)){  $other2 = $NoFIle;}

			    if(empty($debitform)){$debitform = $NoFIle;}
			    if(empty($other3)){  $other3 = $NoFIle;}
			    if(empty($other4)){  $other4 = $NoFIle;}*/
			    // -- EOC 01.10.2017 - Alternative if no file upload.
			     $ViewDocument = 'View Document';
			 	$classCSS = "class='btn btn-warning'";

			 	$iddocumentHTML = '';
			 	if($iddocument == '')
			 	{
			 		$iddocumentHTML = "<tr>
					  <td><input type='file' name='files[]' multiple='multiple' accept='*'></td>
			          <!-- <td>a</td> -->
			          <td>ID Document</td>
			 		</tr>";
			 	}
			 	else
			 	{
			 		$iddocumentHTML = "<tr>
					  <td><input type='file' name='files[]' multiple='multiple' accept='*'></td>					
			          <!-- <td><a $classCSS href='uploads/$iddocument' target='_blank'>$ViewDocument</a></td> -->
			          <td>ID Document</td>
			 		</tr>";
			 	}
			 	$contractHTML = '';
			 	if($contract == '')
			 	{
			 		$contractHTML = "<tr>
					  <td><input type='file' name='files2[]' multiple='multiple' accept='*'></td>					
			          <!-- <td><a href='uploads/$contract' target='_blank'>$contract</a></td> -->
			          <td>Contract Agreement</td>
			       </tr>
			       ";
			 	}
			 	else
			 	{
			 		$contractHTML = "
			       <tr>
					  <td><input type='file' name='files2[]' multiple='multiple' accept='*'></td>					
			          <!-- <td><a $classCSS href='uploads/$contract' target='_blank'>$ViewDocument</a></td> -->
			          <td>Contract Agreement</td>
			       </tr>";
			 	}

			 	$bankstatementHTML = '';
			 	if($bankstatement == '')
			 	{
			 		$bankstatementHTML = "<tr>
					  <td><input type='file' name='files3[]' multiple='multiple' accept='*'></td>										
			          <!-- <td><a href='uploads/$bankstatement' target='_blank'>$bankstatement</a></td> -->
			          <td>Current Bank statement</td>
			       </tr>";
			 	}
			 	else
			 	{
			 		$bankstatementHTML = "<tr>
					  <td><input type='file' name='files3[]' multiple='multiple' accept='*'></td>															
			          <!-- <td><a $classCSS href='uploads/$bankstatement' target='_blank'>$ViewDocument</a></td> -->
			          <td>Current Bank statement</td>
			       </tr>";
			 	}

			 	$proofofincomeHTML = '';
			 	if($proofofincome == '')
			 	{
			 		$proofofincomeHTML = "
			 	  <tr>
					  <td><input type='file' name='files4[]' multiple='multiple' accept='*'></td>															\				  
			          <!-- <td><a href='uploads/$proofofincome' target='_blank'>$proofofincome</a></td> -->
			          <td>Proof of income</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$proofofincomeHTML = "
			 	  <tr>
					  <td><input type='file' name='files4[]' multiple='multiple' accept='*'></td>															\				  
				  
			          <!-- <td><a $classCSS href='uploads/$proofofincome' target='_blank'>$ViewDocument</a></td> -->

			          <td>Proof of income</td>
			       </tr>
			 	  ";
			 	}
			 	$ficaHTML = '';
			 	if($fica == '')
			 	{
			 		$ficaHTML = "

			 	  <tr>
					  <td><input type='file' name='files5[]' multiple='multiple' accept='*'></td> -->
			          <!-- <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
			          <td>Proof of residence</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$ficaHTML = "
			 	  <tr>
					  <td><input type='file' name='files5[]' multiple='multiple' accept='*'></td>				  
			          <!-- <td><a $classCSS href='uploads/$fica' target='_blank'>$ViewDocument</a></td> -->
			          <td>Proof of residence</td>
			       </tr>
			 	  ";
			 	}
			 	/*$creditreportHTML = '';
			 	if($creditreport == '')
			 	{
			 		$creditreportHTML = "
			 	  <tr>
			          <td><a href='uploads/$creditreport' target='_blank'>$creditreport</a></td>
			          <td>Credit Report</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$creditreportHTML = "
			 	  <tr>
			          <td><a $classCSS href='uploads/$creditreport' target='_blank'>$ViewDocument</a></td>
			          <td>Credit Report</td>
			       </tr>
			 	  ";
			 	}
			 	$consentformHTML = '';
			 	if($consentform == '')
			 	{
			 		$consentformHTML = "
			 	  <tr>
			          <td><a href='uploads/$consentform' target='_blank'>$consentform</a></td>
			          <td>Credit Consent Form</td>
			       </tr>";
			 	}
			 	else
			 	{
			 		$consentformHTML = "
			 	  <tr>
			          <td><a  $classCSS href='uploads/$consentform' target='_blank'>$ViewDocument</a></td>
			          <td>Credit Consent Form</td>
			       </tr>";
			 	}
			 	$otherHTML = '';
			 	if($other == '')
			 	{
			 		$otherHTML ="
			 	  <tr>
			          <td><a href='uploads/$other' target='_blank'>$other</a></td>
			          <td>Other</td>
			       </tr>
			 	";
			 	}
			 	else
			 	{
			 		$otherHTML ="
			 	  <tr>
			          <td><a $classCSS href='uploads/$other' target='_blank'>$ViewDocument</a></td>
			          <td>Other</td>
			       </tr>
			 	";
			 	}
			 	$other2HTML = '';
			 	if($other2 == '')
			 	{
			 		$other2HTML = "
			   	  <tr>
			          <td><a href='uploads/$other2' target='_blank'>$other2</a></td>
			          <td>Other 2</td>
			       </tr>
			 ";
			 	}
			 	else
			 	{
			 		$other2HTML = "
			   	  <tr>
			          <td><a $classCSS href='uploads/$other2' target='_blank'>$ViewDocument</a></td>
			          <td>Other 2</td>
			       </tr>";
			 	}


			 	$debitformHTML = '';
			 	if($debitform == '')
			 	{
			 		$debitformHTML = "
			   	  <tr>
			          <td><a href='uploads/$debitform' target='_blank'>$debitform</a></td>
			          <td>Debit Form</td>
			       </tr>
			 ";
			 	}
			 	else
			 	{
			 		$debitformHTML = "
			   	  <tr>
			          <td><a $classCSS href='uploads/$debitform' target='_blank'>$ViewDocument</a></td>
			          <td>Debit Form</td>
			       </tr>";
			 	}

			 	$other3HTML = '';
			 	if($other3 == '')
			 	{
			 		$other3HTML = "
			   	  <tr>
			          <td><a href='uploads/$other3' target='_blank'>$other3</a></td>
			          <td>Other 3</td>
			       </tr>
			 ";
			 	}
			 	else
			 	{
			 		$other3HTML = "
			   	  <tr>
			          <td><a $classCSS href='uploads/$other3' target='_blank'>$ViewDocument</a></td>
			          <td>Other 3</td>
			       </tr>";
			 	}

			 	$other4HTML = '';
			 	if($other4 == '')
			 	{
			 		$other4HTML = "
			   	  <tr>
			          <td><a href='uploads/$other4' target='_blank'>$other4</a></td>
			          <td>Other 4</td>
			       </tr>
			 ";
			 	}
			 	else
			 	{
			 		$other4HTML = "
			   	  <tr>
			          <td><a $classCSS href='uploads/$other4' target='_blank'>$ViewDocument</a></td>
			          <td>Other 4</td>
			       </tr>";
			 	}*/

			    echo "<tbody>"
			       .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML."
			    </tbody>";
			    } ?>
			 <!------------------ End AppLoans documents ----------------------------------->
			 </table>

		 </div>




		 </div>
	 		</div>
</br>

<!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Save</button>-->
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>



<div class="row setup-content" id="step-6">
<?php include $filerootpath.'/confirm_ffms_application.php'; ?>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Activate</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
 <div class="row setup-content" id="step-7">
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Create new application</strong></button>
			  </div>
			</div>
			</div>
			<br/>
			</div></div>
			 <br/>
			 <a href="finApplicationform" class="btn btn-primary nextBtn btn-lg pull-right" name="PayNow" id="PayNow" type="button" ><strong>Create new application</strong></a>
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary btn-lg pull-right" name="Exit" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>
</div>

</form>
</div>
<script>
//Dependants
// -- 18 September 2021 -- //
function servicetypeselect(sel)
{
			var message_info   = document.getElementById('rulealert');
			message_info.className  = '';
			message_info.innerHTML  = '';

			var Service_type = 'TWO DAY';//sel.options[sel.selectedIndex].value;
			if(Service_type == 'TWO DAY')
			{
				// -- Call the rules
				applyrules(sel);
			}
			// -- 13.11.2021
			if(Service_type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}
}
function applyrules_sameday(sel)
{
		/*var selectedService_Type = 'TWO DAY';//document.getElementById('Service_Type').value;
		var Action_Date_EL = document.getElementById("Action_Date");
		var Action_Date_B4 = Action_Date_EL.value;
		var message_info   = document.getElementById('rulealert');

		var list =
		{'Action_Date'			: Action_Date_EL.value,
		 'Type' 	            : selectedService_Type
		}

		if(selectedService_Type == 'SAMEDAY')
		{
		// -- alert(selectedService_Type);
		// -- jQuery for PHP file
		jQuery(document).ready(function()
		{
			jQuery.ajax({  type: "POST",
					   url:"script_rule_two_day_service.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							if(feedback.includes("Proposed"))
							{
							  var res = feedback.split("|");
							  Action_Date_EL.value = res[1];
							  message_info.className  = 'alert alert-warning';
							  message_info.innerHTML  = 'Action Date :'+Action_Date_B4+' is a weekend or holiday. system auto generated action date:'+res[1];
							}
							else
							{
								message_info.className  = '';
							    message_info.innerHTML  = '';
								Action_Date_EL.value = feedback;
							}
						 }
			});
		});
		}*/
}
function applyrules(sel)
{
		/*var selectedService_Type = 'SAMEDAY';//document.getElementById('Service_Type').value;
		var Action_Date_EL = document.getElementById("Debit_date");
		var Action_Date_B4 = Action_Date_EL.value;
		var message_info   = document.getElementById('rulealert');

		var list =
		{'Action_Date'			: Action_Date_EL.value,
		 'Type' : selectedService_Type}

		if(selectedService_Type == 'TWO DAY')
		{
			//alert(selectedService_Type);
		// -- jQuery for PHP file
		jQuery(document).ready(function()
		{
			jQuery.ajax({  type: "POST",
					   url:"script_rule_two_day_service.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							if(feedback.includes("Proposed"))
							{
							  var res = feedback.split("|");
							  Action_Date_EL.value = res[1];
							  message_info.className  = 'alert alert-warning';
							  message_info.innerHTML  = 'Action Date :'+Action_Date_B4+' is a weekend or holiday. system auto generated action date:'+res[1];
							}
							else
							{
								message_info.className  = '';
							    message_info.innerHTML  = '';
								Action_Date_EL.value = feedback;
							}
						 }
			});
		});
		}
		//script_rule_two_day_service.php

				// -- 13.11.2021
			if(selectedService_Type == 'SAMEDAY')
			{
				// -- Call the rules
				applyrules_sameday(sel);
			}*/
}

function valueselect(sel)
 {
		var Service_Mode = sel.options[sel.selectedIndex].value;
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);

							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});
			  return false;
  }

  $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
		allpreviousBtn = $('.previousBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

	allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;
		//alert(objToString(allpreviousBtn));

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

 });


    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-6')
			{
				var message_info = document.getElementById('message_info');
				message_info.innerHTML = '';
				message_info.className  = '';
				ret2 = check_duplicate();
				if(ret2 === 1)
				{
					isValid = false;
				}
			}

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }




		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus();
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');

		add_confirm_instruction();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});


function maxLengthNumber(el,maxlength)
{
	alert(maxlength);
	if(el.value.length == maxlength)
	{
		return false;
	}
}
function toStep1()
{
document.getElementById("step1").click();
}
function add_confirm_instruction()
{
	// -- confirmation.
	/*document.getElementById('InitialsLbl').innerHTML = document.getElementById('Initials').value;
	document.getElementById('Account_HolderLbl').innerHTML = document.getElementById('Account_Holder').value;
	document.getElementById('Account_NumberLbl').innerHTML = document.getElementById('Account_Number').value;
	var Account_Type = document.getElementById('Account_Type').value;
	var Account_TypeDesc = '';
	var branchcode = document.getElementById('Branch_Code').value;
	var bankname = '';
	var tracking     = document.getElementById('entry_class').value;
	var trackingdesc = '';

	Branch_CodeArr  = <?php echo json_encode($Branch_CodeArr);?>;
	Arr = <?php echo json_encode($Account_TypeArr);?>;
	dataentry_classArr = <?php echo json_encode($dataentry_classArr);?>;

	for (var key in Arr)
	{
	   row = new Array();
	   row = Arr[key].split('|');
	   if(Account_Type == row[0])
	   {
		   Account_TypeDesc = row[1];
	   }
	}

	for (var key in Branch_CodeArr)
	{
	   row = new Array();
	   row = Branch_CodeArr[key].split('|');
	   if(branchcode == row[0])
	   {
		   bankname = row[1];
	   }
	}

	for (var key in dataentry_classArr)
	{
	   row = new Array();
	   row = dataentry_classArr[key].split('|');
	   if(tracking == row[0])
	   {
		   trackingdesc = row[1];
	   }
	}

	if(trackingdesc == ''){trackingdesc = 'No tracking'; }

	document.getElementById('Account_TypeLbl').innerHTML = Account_TypeDesc;
	document.getElementById('Branch_CodeLbl').innerHTML = bankname;
	document.getElementById('Reference_1Lbl').innerHTML = document.getElementById('Reference_1').value;
	document.getElementById('Reference_2Lbl').innerHTML = document.getElementById('Reference_2').value;
	document.getElementById('Bank_ReferenceLbl').innerHTML = document.getElementById('Bank_Reference').value;
	document.getElementById('NotifyLbl').innerHTML = document.getElementById('Notify').value;
	document.getElementById('contact_numberLbl').innerHTML = document.getElementById('contact_number').value;

document.getElementById('Client_ID_TypeLbl').innerHTML = document.getElementById('Client_ID_Type').value;
document.getElementById('Client_ID_NoLbl').innerHTML = document.getElementById('Client_ID_No').value;
document.getElementById('No_PaymentsLbl').innerHTML = document.getElementById('No_Payments').value;
document.getElementById('entry_classLbl').innerHTML = trackingdesc;
document.getElementById('Service_TypeLbl').innerHTML = document.getElementById('Service_Type').value;
document.getElementById('Service_ModeLbl').innerHTML = document.getElementById('Service_Mode').value;
document.getElementById('FrequencyLbl').innerHTML = document.getElementById('Frequency').value;
document.getElementById('Action_DateLbl').innerHTML = document.getElementById('Action_Date').value;

document.getElementById('companybranchLbl').innerHTML = document.getElementById('srUserCode').value;
document.getElementById('AmountLbl').innerHTML = document.getElementById('Amount').value;
*/
}

function check_duplicate()
{
// 1 -- MemberID	
var member =
{ 'Member_id' 		    :document.getElementById('im_member_id').value};

				var feedback = '';
				var ret = 0;

				//--script_checkduplicate_ffms.php
				jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"script_checkduplicate_ffms.php",
									   data:{member:member},
										success: function(data)
										{
										 feedback = data;
										 if(feedback === '')
										 {feedback  = 'Are you sure you want to create a new member?';}
											if (confirm(feedback))
											{
											  // Save it!
												ret = 0;
												call_script_ffms_application();
											}
											else
											{
											  // Do nothing!
											    toStep1();
											    ret = 1;
											}

						}});
				});

				//return ret;
}

function call_script_ffms_application()
{
	var userid = <?php echo "'".$userid."'"; ?>;
	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;
	var UserCode = <?php echo "'".$UserCode."'"; ?>;
	var branchcode = '';
	var bank = document.getElementById('Bank').value;
	//alert(bank);
	//alert(document.getElementById('product_service').value);
	if(bank == 'ABSA')
	{branchcode = '632005';	}

if('SBSAbranchcode'){ branchcode = '051001';	            }
if('Capitecbranchcode'){ branchcode =   '470010';	        }
if('Nedbankbranchcode'){ branchcode =   '198765';	        }
if('FNBbranchcode'){ branchcode =   '250655';	            }
if('MercantileBankbranchcode'){ branchcode  = '450905';	}
if('Postbankbranchcode'){ branchcode =  '460005';	        }
if('Investecbranchcode'){ branchcode =  '580105';	        }
if('TymeBankbranchcode'){ branchcode =  '678910';	        }
if('Discoverybranchcode'){ branchcode =  '679000';	    }
if('Bank of Athensbranchcode'){ branchcode  = '410506';	}
if('African Bankbranchcode'){ branchcode  = '430000';	    }
if('Habibbranchcode'){ branchcode =  '701505';	        }
if('SBSAbranchcode'){ branchcode =  '51001';	            }
if('Albarakabranchcode'){ branchcode =  '800000';	        }
if('HBZbranchcode'){ branchcode =  '570105';	            }
if('MakuruBankbranchcode'){ branchcode  = '410506';	    }
if('Finbondbranchcode'){ branchcode =  '589000';	        }
if('Ubankbranchcode'){ branchcode =  '431010';	        }
if('Old Mutualbranchcode'){ branchcode =  '462005';	    }
if('Bidvest Bank'){ branchcode  = '462005';	}
	
var member_gender = document.getElementsByName('im_gender');

var member_gender_value = '';
for(var i = 0; i < member_gender.length; i++){
    if(member_gender[i].checked){
        member_gender_value = member_gender[i].value;
    }
}
	//alert(member_gender_value);
// 1 -- MemberID	
			var member =
{

'Member_id' 		    :document.getElementById('im_member_id').value,		
'Title' 			 	:document.getElementById('im_title').value,	
'First_name' 			:document.getElementById('im_first_name').value,	
'Middle_name' 			:document.getElementById('im_Middle_name').value,	
'Initials' 	 			:document.getElementById('im_initials').value,	
'Surname' 				:document.getElementById('im_surname').value,	
'Gender_id' 			:member_gender_value,	
'Date_of_birth'			:document.getElementById('im_Date_of_birth').value,	 	
'MaritualStatus_id'		:document.getElementById('im_maritalStatus').value,			
'Marriage_id'			:document.getElementById('im_marriage').value,	
'Spouse_id'				:document.getElementById('im_spouse_id').value,	
//'Address_id' 	 		:'0',//document.getElementById('im_address').value,	
'Life_State_id' 		:'1',//document.getElementById('im_Life_State').value,		
'Language_id' 			:'2'};//document.getElementById('im_language_id').value};	

// 2 -- Spouse
var spouse =
{
'spouse_id' 			:document.getElementById('im_spouse_id').value,			 
'spousetitle'			:document.getElementById('im_spousetitle').value,
'spousefirst_name' 		:document.getElementById('im_spousefirst_name').value,
'spousemiddle_name' 	:document.getElementById('im_spouseMiddle_name').value,	
'spouseinitials' 		:document.getElementById('im_spouseinitials').value,
'spousesurname' 		:document.getElementById('im_spousesurname').value,
'spousegender' 			:'',//document.getElementById('im_spousegender').value,
'spousedate_of_birth' 	:'',//document.getElementById('im_spouseDate_of_birth').value,
'spouseaddress' 		:'',//document.getElementById('im_spouseaddress').value,
'spouselife_State' 		:'1',//document.getElementById('im_spouseLife_State').value,
'spouselanguage_id' 	:'2'};//document.getElementById('im_spouselanguage_id').value};	

// 3 - address
var address =
{
'Suburb_id'				:'1',//document.getElementById('im_suburb_id').value,	  
'Line_1'				:document.getElementById('im_line_1').value,	
'Line_2'				:document.getElementById('im_line_2').value,	
'Line_3'				:document.getElementById('im_line_3').value,	
'Email'					:document.getElementById('im_email').value,	
'Fax'					:'',//document.getElementById('im_fax').value,	
'Cellphone'				:document.getElementById('im_cellphone').value,	
'Work_tel'				:document.getElementById('im_work_tel').value,	
'Home_tel'				:document.getElementById('im_home_tel').value,	
'Comm_preference'		:''};//document.getElementById('im_comm_preference').value};	

// 4-source of income
var source_of_income =
{
'Source_of_income_id'	:document.getElementById('source_of_income_2').value,						
'Occupation_id'			:document.getElementById('im_occupation_id_1').value,			
'Member_id'				:document.getElementById('im_member_id').value,		
'Income_provider_name'	:document.getElementById('Income_provider_name').value,					        
'Telelphone_nr'			:document.getElementById('Income_provider_Contact_No').value,			        
'Email'					:document.getElementById('Income_provider_Email').value};

var banking_details =
{
'Main_member_id' 	:document.getElementById('im_member_id').value,
'Account_number' 	:document.getElementById('Account_number').value,	
'Account_type' 	 	:document.getElementById('Account_type').value,
'Bank'	 			:bank,//document.getElementById('Bank').value,
'Branch_name' 	 	:branchcode,//document.getElementById('Branch_name').value,
'Branch_code' 		:branchcode,//document.getElementById('Branch_code').value,
'Personal_number' 	:document.getElementById('Personal_number').value,	
'Debit_date' 		:document.getElementById('Debit_date').value,
'Commence_date'		:document.getElementById('Commence_date').value}; 

// 6. -- Dependent Details ...
var list =
{
'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode};

// 7 -- member policy

var member_policy = 
{'Member_id' :document.getElementById('im_member_id').value,
'Policy_id'  :document.getElementById('product_service').value, 
'Active' 	 :'0',
'Remarks' 	 :'successfully created'};

					//alert(document.getElementById('Initials').value);
var feedback = '';
var ret = 0;

// -- Check Duplicate Payment to benefiaciary.
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"script_create_ffms_application.php",
				       data:{member:member,spouse:spouse,
							 address:address,source_of_income:source_of_income,
							 banking_details:banking_details,list:list,member_policy:member_policy
							},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

//PayNow = document.getElementById("PayNow");
//Exit = document.getElementById("PayNow");
backtoconfirm = document.getElementById("backtoconfirm");
//addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								//alert(feedback);
								message_info.innerHTML = 'Membership created successfully for '+document.getElementById('im_member_id').value+'.';
								message_info.className  = 'status';
								//PayNow.style.visibility = '';
								//Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								//addbeneficiary.style.visibility = '';
								var res = feedback.split("|");
								if(typeof res[2] === 'undefined')
								{}else{message_info.innerHTML = message_info.innerHTML + '<br/>'+res[2];}
								document.getElementById('bid').value = res[1];
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								//PayNow.style.visibility = 'hidden';
//Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								//addbeneficiary.style.visibility = 'hidden';
							}

						 }
					});
				});

}

//Dependants
const $tableID = $('#table'); 
const $tableID2 = $('#table2'); 
const $tableID3 = $('#table3'); 
const $BTN = $('#export-btn'); 
const $EXPORT = $('#export');
const newTr = `
  <tr class="hide">
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half">
      <span class="table-up"
        ><a href="#!" class="indigo-text"
          ><i class="fas fa-long-arrow-alt-up" aria-hidden="true"></i></a
      ></span>
      <span class="table-down"
        ><a href="#!" class="indigo-text"
          ><i class="fas fa-long-arrow-alt-down" aria-hidden="true"></i></a
      ></span>
    </td>
    <td>
      <span class="table-remove"
        ><button
          type="button"
          class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light"
        >
          Remove
        </button></span
      >
    </td>
  </tr>
  `;
  const newTr3 = `
  <tr class="hide">
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half" contenteditable="true">Example</td>
	<td class="pt-3-half" contenteditable="true">Example</td>
    <td class="pt-3-half">
      <span class="table-up"
        ><a href="#!" class="indigo-text"
          ><i class="fas fa-long-arrow-alt-up" aria-hidden="true"></i></a
      ></span>
      <span class="table-down"
        ><a href="#!" class="indigo-text"
          ><i class="fas fa-long-arrow-alt-down" aria-hidden="true"></i></a
      ></span>
    </td>
    <td>
      <span class="table-remove"
        ><button
          type="button"
          class="btn btn-danger btn-rounded btn-sm my-0 waves-effect waves-light"
        >
          Remove
        </button></span
      >
    </td>
  </tr>
  `;
  $('.table-add').on('click', 'i', () => {
	const $clone = $tableID.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
		$tableID.find('table').append($clone);
});
$('.table2-add').on('click', 'i', () => {

	const $clone = $tableID2.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
	if ($tableID2.find('tbody tr ').length ===0) {
        $('tbody').append(newTr);
    }
	$tableID2.find('#table2').append($clone);
});
$('.table3-add').on('click', 'i', () => {
const $clone = $tableID3.find('tbody tr ').last().clone(true).removeClass('hide table - line ');
if ($tableID3.find('tbody tr ').length ===0) {
	$('tbody').append(newTr3);
}
$tableID3.find('#table3').append($clone);
});

$tableID.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID2.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID3.on('click', '.table-remove', function() {
    $(this).parents('tr').detach();
});
$tableID.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID2.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID3.on('click', '.table-up', function() {
    const $row = $(this).parents('tr');
    if ($row.index() === 0) {
        return;
    }
    $row.prev().before($row.get(0));
});
$tableID.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    }); 
$tableID2.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });
$tableID3.on('click','.table-down',
    function() {
        const $row = $(this).parents('tr');
        $row.next().after($row.get(0));
    });  
// A few jQuery helpers for exporting only jQuery.fn.pop= [].pop;
jQuery.fn.shift = [].shift;
$BTN.on('click', () => {
    const $rows =$tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = []; // Get the headers(add special header logic here)
	 $($rows.shift()).find('th:not(:empty)').each(function() {
        headers.push($(this).text().toLowerCase());
    }); // Turn all existing rows into a loopable array
	 $rows.each(function() {
        const $td = $(this).find('td');
        const h = {}; // Use the headers from earlier to name our hash keys
		 headers.forEach((header, i) => {
            h[header] = $td.eq(i).text();
        });
        data.push(h);
    }); // Output the result
    $EXPORT.text(JSON.stringify(data));
});

</script>
