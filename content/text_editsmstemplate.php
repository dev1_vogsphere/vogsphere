<?php
require 'database.php';
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_user="smstemplate"; // Table name

//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM placeholder';
$dataPlaceholders = $pdo->query($sql);

$count = 0;

$placeholder		=	'';
$smstemplatename		=	'';
$smstemplatecontent	=	'';

// -- Errors
$placeholderError		=	'';
$smstemplatenameError		=	'';
$smstemplatecontentError	=	'';
$valid = true;
$smstemplateid  = '';
//$message = '';

if (isset($_GET['smstemplateid']) )
  {
	$smstemplateid = $_GET['smstemplateid'];
  }

if (!empty($_POST))
{   $count = 0;

//$placeholder		=	getpost('placeholder');
$smstemplatename	=	getpost('smstemplatename');
$smstemplatecontent	=	getpost('smstemplatecontent');

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (empty($smstemplatename)) { $smstemplatenameError = $smstemplatenameError.'Please enter sms template name'; $valid = false;}

	if (empty($smstemplatecontent)) { $smstemplatecontentError = $smstemplatecontentError.'Please enter sms template content'; $valid = false;}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($valid)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE smstemplate SET smstemplatename = ?, smstemplatecontent = ? WHERE smstemplateid = ?";
			$q = $pdo->prepare($sql);
			//$smstemplatecontent =''.$smstemplatecontent.'' ;
			$q->execute(array($smstemplatename,$smstemplatecontent,$smstemplateid));
			Database::disconnect();
			$count = $count + 1;
	}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from smstemplate where smstemplateid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($smstemplateid));
		$data = $q->fetch(PDO::FETCH_ASSOC);

		$smstemplatename = $data['smstemplatename'];
		$smstemplatecontent = $data['smstemplatecontent'];
}

?>
<!--<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="smstemplates.php">SMS Templates</a></li>
		</ol>
	</div>
</div>-->
<div class='container background-white bottom-border'>
	<div class='row margin-vert-30'>
	  <div class="row">
		<form name='form1' action='' method='POST' class="signup-page">
<?php # error messages
		if (isset($message))
		{
			foreach ($message as $msg)
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>SMS template updated successfully</p>");
		}
?>
			<p><b>SMS Template Name</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='smstemplatename' id="smstemplatename" style='z-index:-1' value="<?php echo !empty($smstemplatename)?$smstemplatename :'';?>" readonly />
			<?php if (!empty($smstemplatenameError)): ?>
			<span class="help-inline"><?php echo $smstemplatenameError;?></span>
			<?php endif; ?>
			</p>
		<!--	<p><b>Placeholders</b><br />
				<SELECT class="form-control" id="placeholder" name="Place Holder" size="1">-->
					<?php
					/*	$placeholderLoc = '';
						$placeholderSelect = $placeholder;
						foreach($dataPlaceholders as $row)
						{
							$placeholderLoc = $row['placeholder'];

							if($placeholderLoc == $placeholderSelect)
							{
								echo "<OPTION value='".$placeholderLoc."' selected>$placeholderLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$placeholderLoc."'>$placeholderLoc</OPTION>";
							}
						}

						if(empty($dataPlaceholders))
						{
							echo "<OPTION value=0>No placeholders</OPTION>";
						}*/


					?>

				<!--	</SELECT>
			</p>-->
			<div class="form-group <?php if (!empty($smstemplatecontentError)){ echo "has-error";}?>">
				<p>
				<!--	Click to add <input onclick='input()' type='button' value='Place holder' id='button'><br>-->
					<textarea class="form-control" rows="10" onkeyup="countChars(this);" name='smstemplatecontent' id="smstemplatecontent"><?php echo !empty($smstemplatecontent )?$smstemplatecontent :'';?></textarea>
					<?php if (!empty($smstemplatecontentError)): ?>
							<small class="help-block"><?php echo $smstemplatecontentError;?></small>
					<?php endif; ?>
				</p>
        <p id="charNum"> <b>160 Characters Remaining</b></p>
			</div>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'smstemplates';

			if(isset($_SESSION["GlobalTheme"]))
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];
			}

			//echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				//<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>
		     </div>
			   <button class='btn btn-primary' type='submit'>Save</button><!-- Generate OTP -->
  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->

			 </td>
		</tr>
		</table>
		</form>
  </div>
	</div>	</div>

<script>
function input()
{
	var title = document.getElementById('placeholder').value;
	var area = document.forms.form1.smstemplatecontent.value;
    document.forms.form1.smstemplatecontent.value = area + title;
}

function countChars(smstemplatecontent){
  var maxLength =160;
  var strLength=document.forms.form1.smstemplatecontent.value.length;
  var charRemain=(maxLength - strLength);

  if(charRemain < 0)
  {
    document.getElementById('charNum').innerHTML= '<span style="color:red;"><b>You have exceeded the limit of</b> '+maxLength+'<b> characters</b></span>';
  }else{
    document.getElementById('charNum').innerHTML=charRemain+' <b>Characters Remaining</b>';
  }
}
</script>
