<?php // --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
require_once 'database.php';
$tbl_name="user"; // Table name
$tbl_customer ="customer"; // Table name

$username = '';
$Title = '';
$FirstName = '';
$LastName = '';
$password1 = '';
$idnumber = '';
$email = 'info@vogsphere';

// -- BOC Encrypt 16.09.2017.
$adminemail = "";
$companyname = "";

// -- BOC Captcha 20.09.2017.
$captcha = '';
$captchaError = null;
$modise = '';
$valid = true;

// -- EOC Captcha 20.09.017.

if(isset($_SESSION))
{
	$adminemail = $_SESSION['adminemail'];
	$companyname = $_SESSION['companyname'];
}
// -- EOC Encrypt 16.09.2017.

$idnumberError = '';
$successMessage = 'Your password recovery key has been sent to your e-mail address.';
$count = 0;

// -- Database Declarations and config:
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


   /* echo "<h3> PHP List All Session Variables</h3>";
    foreach ($_SESSION as $key=>$val)
    echo $key." ".$val."<br/>";*/

if(isset($_POST) & !empty($_POST))
{
	$username = $_POST['username'];
	$sql = "SELECT * FROM $tbl_name Inner Join $tbl_customer on $tbl_name.userid = $tbl_customer.customerid  WHERE userid='$username'";
	$q = $pdo->prepare($sql);
	$q->execute(array($username));
	$dataUser = $q->fetch(PDO::FETCH_ASSOC);

	// -- BOC Captcha Error Validation. 20.09.2017.
	$modise  = $_SESSION['captcha'];
	$captcha = $_POST['captcha'];
	//echo $modise.' - '. $captcha;
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}
	// -- EOC Captcha Error Validation. 20.09.2017.

	if(!empty($dataUser['userid']))
	{
		if($valid)
		{
			$idnumberError = "";
			$count = $count + 1;

			// -- Store Value in Session to send via an e-mail.
			$_SESSION['Title'] 		= $dataUser['Title'];
			$_SESSION['FirstName'] 	= $dataUser['FirstName'];
			$_SESSION['LastName'] 	= $dataUser['LastName'];
			$_SESSION['password'] 	= $dataUser['password'];
			$_SESSION['idnumber']  	= $dataUser['userid'];
			$_SESSION['email'] = $dataUser['email'];

			//  -- BOC encrypt password - 16.09.2017.
			$hashUrl = Database::encryptPassword($dataUser['email'].$_SESSION['password']);
			//  -- EOC encrypt password - 16.09.2017.
			$_SESSION['hashUrl'] = $hashUrl;

			// -- SendEmail for Recovered Password --- //
			SendEmailForgotPassword();
		}
	}
	else
	{
		$idnumberError = "User name does not exist in database";
		$count = 0;
	}
}

?>
<!-- === BEGIN CONTENT === -->

<div class="container background-white bottom-border"> <!-- class="container background-white"> -->
   <div class="row margin-vert-30">
		<div class="col-md-6 col-md-offset-3 col-sm-offset-3">
			<form class="login-page" method="POST">
			<?php # error messages
					if($count !=0)
					{printf("<p class='status'>".$successMessage."</p>\n");}
			?>
			<h2 class="text-center">Forgotten Your Password</h2>
			<br>
			<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
				<h4>Please enter your details below</h4>
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1"></span>
				  <input style="height:30px"  class="form-control margin-bottom-20"  type="text" name="username" placeholder="ID / Passport number"
				  value ="<?php echo !empty($username)?$username:'';?>" required >
				</div>
			</div>
			 <?php if (!empty($idnumberError)): ?>
				<span class="help-inline"><?php echo $idnumberError; ?></span>
			  <?php endif; ?>
			<br />

			<!-- New Captcha -->
			<h4>Please enter the verification code shown below.</h4>
				<div id='captcha-wrap'>
					<!-- <img src="data:image/png;base64,".base64_encode(Image())" alt='' id='captcha' /> -->
						<?php echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' />
											<img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>";
						?>
				</div>

				<div class="control-group <?php echo !empty($captchaError)?'error':'';?>">
					<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value="<?php echo !empty($captcha)?$captcha:'';?>">
						<?php if (!empty($captchaError)): ?>
							<span class="help-inline"><?php echo $captchaError; ?></span>
						<?php endif; ?>

				</div>
			<!-- End New Captcha -->

			<!-- BOC New Buttons -->
			<div class="row">
                <div class="col-md-6">
                                            <!-- <label class="checkbox">
                                                <input type="checkbox">Stay signed in</label> -->
                </div>
                <div class="col-md-6">
					<a class="btn btn-primary pull-right" style="margin-left:10px"  href="customerLogin">Login</a>
					<button class="btn btn-primary pull-right" type="submit">Reset Password</button>
					<!-- <button class="btn btn-success pull-right" value="login" name="login" type="submit">Login</button> -->
                </div>
             </div>
			<!-- EOC New Buttons -->

			</form>
		</div>
	</div>
</div>
