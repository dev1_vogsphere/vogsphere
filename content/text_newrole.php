<?php 
require 'database.php';

$roleError = null;
$role = '';
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$role = $_POST['role'];

	$valid = true;
	
	if (empty($role)) { $roleError = 'Please enter role name.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO role (role) VALUES(?) "; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($role));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">   
	<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
		<div class="panel-heading">
			<h2 class="panel-title">
								<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
									 New role Details
								</a>
							</h2>
			<?php # error messages
					if (isset($message)) 
					{
						foreach ($message as $msg) 
						{
							printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>role successfully added.</p>");
					}
			?>
							
			</div>				

			<p><b>Role</b><br />
			<input style="height:30px" type='text' name='role'/>
			<?php if (!empty($roleError)): ?>
			<span class="help-inline"><?php echo $roleError;?></span>
			<?php endif; ?>
			</p>
			
		<table>
		<tr>
			<td><div class="form-actions">
				<button type="submit" class="btn btn-success">New role</button>
				<a class="btn btn-primary" href="roles">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>