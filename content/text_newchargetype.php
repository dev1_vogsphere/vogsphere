<?php 
require 'database.php';
$tablename = 'chargetype';
$chargeoptionnameError = null;
$chargeoptiondescError = null;

$chargetypename = '';
$chargetypedesc = '';
$count = 0;
$chargetypeid = '';

if (!empty($_POST)) 
{   $count = 0;
	$chargetypename = $_POST['chargetypename'];
	$chargetypedesc = $_POST['chargetypedesc'];;
	$valid = true;
	
	if (empty($chargetypename)) { $chargetypenameError = 'Please enter Charge Type name.'; $valid = false;}
	if (empty($chargetypedesc)) { $chargetypedescError = 'Please enter Charge Type description.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO $tablename (name,chargetypedesc) VALUES (?,?)"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($chargetypename,$chargetypedesc));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Charge Type Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>$tablename successfully created.</p>");
		}
?>
				
</div>				
			<p><b>Charge Type</b><br />
		<input style="height:30px" type='text' name='chargetypename' value="<?php echo !empty($chargetypename)?$chargetypename:'';?>"/>
			<?php if (!empty($chargetypenameError)): ?>
			<span class="help-inline"><?php echo $chargetypenameError;?></span>
			<?php endif; ?>
			</p> 	
		
		<p><b>Charge Type Description</b><br />
		<input style="height:30px" type='text' name='chargetypedesc' value="<?php echo !empty($chargetypedesc)?$chargetypedesc:'';?>"/>
			<?php if (!empty($chargetypedescError)): ?>
			<span class="help-inline"><?php echo $chargetypedescError;?></span>
			<?php endif; ?>
			
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'chargetype';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>