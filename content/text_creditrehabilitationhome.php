<?php
// -- Database Declarations and config:
  include 'database.php';
  $tbl_loanapp = "loanapp";
  $db_name = "ecashpdq_eloan";
  $cragents = null;
  $agentid = '';
  
// -- Leads
  $tbl_name = 'lead';
  $whereLeads = array();
  $whereSales = array();
  
 // -- Bucket Open Leads
$openLeads = 0;

 // -- Closed Leads 
$closedLeads = 0;
 

 // -- Finalised Leads 
$finalisedLeads = 0;

//1. Judgements
$Judgements = 0;

//2. Defaults
$Defaults = 0;

//3. Notices
$Notices = 0;

//4. Tracealerts
$Tracealerts = 0;

//5. Paymentprofile
$Paymentprofile = 0;

//6. Enquires
$Enquires = 0;

// -- End of Leads -- //
 
// -- current sales
$currentSales = 0;

// -- posted sales
$postedSales = 0;
 
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);

  // -- Loan Types (27.01.2017)
	foreach ($data as $row)
	{
		if($row['loantypeid'] == 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];

// ----------- BOC Global Settings 16.04.2017 -------------------
// -- Currency & Company Global Details.
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	$q->execute();
	$dataGlobal = $q->fetch(PDO::FETCH_ASSOC);

	if($dataGlobal['globalsettingsid'] >= 1)
	{
		$_SESSION['currency'] = $dataGlobal['currency'];// -- 10.02.2017
	}
	else
	{
	  $_SESSION['currency'] = 'R';
	}
// ------ EOC Global Settings 16.04.2014 -------------------------

	Database::disconnect();
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
		{
				$loggedUser = $_SESSION['username'];
				$Title = $_SESSION['Title'];
				$FirstName = $_SESSION['FirstName'];
				$LastName = $_SESSION['LastName'];
$agentid = $loggedUser;
				// -- Add Dashboard to crmanager.
				// -- 14.01.2018
				if($_SESSION['role'] == 'crmanager' || $_SESSION['role'] == 'admin')
				{
				// -- Read all user with crmanager & crconsultant role add into an array.. use the array to search through out.
				$crrolesarray[1] = "'crmanager'";
				$crrolesarray[2] = "'crconsultant'";
				$cragents = GetCRAgents($crrolesarray);
				
// -- All Leads - customer created by credit rehabilitation agent.
$TotalLeads = GetTotalLeads($cragents);
// --- Dynamic Dashboard Builders.
$dashtitle = 'Total'; 
$panelclass = "panel panel-primary";
$url = "leads"; 
$Headtitle = "";
$dynamicDashTotalLeads = dynamicDashboard($TotalLeads,$panelclass,$url,$Headtitle,$dashtitle);

// -- Leads per Agent
$agents = implode(",",$cragents);
				
// -- Bucket Open Leads
$dashtitle = 'Open'; 
$c_status ="'open'";
$c_open = 'open';
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$openLeads = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_open.""; 
$Headtitle = "";
$dynamicDashOpen = dynamicDashboard($openLeads,$panelclass,$url,$Headtitle,$dashtitle);

 // -- Closed Leads 
$dashtitle = 'Closed'; 
$c_status ="'closed'";
$c_closed = 'closed';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$closedLeads = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_closed.""; 
$Headtitle = "";
$dynamicDashClosed = dynamicDashboard($closedLeads,$panelclass,$url,$Headtitle,$dashtitle);

 // -- Finalised Leads 
$dashtitle = 'Finalised'; 
$c_status ="'finalised'";
$c_finalised = 'finalised';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$finalisedLeads = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_finalised.""; 
$Headtitle = "";
$dynamicDashfinalised = dynamicDashboard($finalisedLeads,$panelclass,$url,$Headtitle,$dashtitle);

//1. Judgements Leads
$dashtitle = 'Judgements'; 
$c_status ="'Judgements'";
$c_Judgements = 'Judgements';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Judgements = search_dynamic_leads($whereLeads)->rowCount();

// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Judgements.""; 
$Headtitle = 'Credit Restoration';
$dynamicDashJudgements = dynamicDashboard($Judgements,$panelclass,$url,$Headtitle,$dashtitle);

//2. Defaults
$dashtitle = 'Defaults'; 
$c_status ="'Defaults'";
$c_Defaults = 'Defaults';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Defaults = search_dynamic_leads($whereLeads)->rowCount();

// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Defaults.""; 
$Headtitle = "";
$dynamicDashDefaults = dynamicDashboard($Defaults,$panelclass,$url,$Headtitle,$dashtitle);

//3. Notices
$dashtitle = 'Notices'; 
$c_status ="'Notices'";
$c_Notices = 'Notices';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Notices = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Notices.""; 
$Headtitle = "";
$dynamicDashNotices = dynamicDashboard($Notices,$panelclass,$url,$Headtitle,$dashtitle);

//4. Tracealerts
$dashtitle = 'Trace Alerts'; 
$c_status ="'Tracealerts'";
$c_Tracealerts = 'Tracealerts';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Tracealerts = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Tracealerts.""; 
$Headtitle = "";
$dynamicDashTracealerts = dynamicDashboard($Tracealerts,$panelclass,$url,$Headtitle,$dashtitle);

//5. Paymentprofile
$dashtitle = 'Payment Profile'; 
$c_status ="'Paymentprofile'";
$c_Paymentprofile = 'Paymentprofile';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Paymentprofile = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Paymentprofile.""; 
$Headtitle = "";
$dynamicDashPaymentprofile = dynamicDashboard($Paymentprofile,$panelclass,$url,$Headtitle,$dashtitle);
//6. Enquires
$dashtitle = 'Enquires'; 
$c_status ="'Enquires'";
$c_Enquires = 'Enquires';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$Enquires = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-primary";
$url = "leads?status=".$c_Enquires.""; 
$Headtitle = "";
$dynamicDashEnquires = dynamicDashboard($Enquires,$panelclass,$url,$Headtitle,$dashtitle);

//7. Partial Complete
$dashtitle = 'Partial Complete'; 
$c_status ="'partialcomplete'";
$c_partialcomplete = 'partialcomplete';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$partialcomplete = search_dynamic_leads($whereLeads)->rowCount();
// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-yellow";
$url = "leads?status=".$c_partialcomplete.""; 
$Headtitle = "";
$dynamicDashpartialcomplete = dynamicDashboard($partialcomplete,$panelclass,$url,$Headtitle,$dashtitle);

//8. Debit Review
$dashtitle = 'Debit Review'; 
$c_status ="'debitreview'";
$c_debitreview = 'debitreview';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$debitreview = search_dynamic_leads($whereLeads)->rowCount();

// --- Dynamic Dashboard Builders.
$panelclass = "panel panel-yellow";
$url = "leads?status=".$c_debitreview.""; 
$Headtitle = "";
$dynamicdebitreview = dynamicDashboard($debitreview,$panelclass,$url,$Headtitle,$dashtitle);


// -- END of Leads -- //

// -- 1st & last day of the month.
$lastDayThisMonth = "'".date("Y-m-t")."'";
$firstDayThisMonth = "'".date("Y-m-01")."'";

// -- posted sales, exclude current month.
$whereLeads = null;
$whereLeads[] = "createdby IN ({$agents})";
$whereLeads[] = "lastPaymentDate > {$lastDayThisMonth}";
$postedSales = search_dynamic_loanapp($whereLeads)->rowCount();

// -- All Sales - credit rehabilitation programm subscribed by customer.
$whereLeads = null;
$whereLeads[] = "createdby IN ({$agents})";
$TotalSales = search_dynamic_loanapp($whereLeads)->rowCount();
$panelclass = "panel panel-yellow";
$url = "creditrehabilitationsales"; 
$Headtitle = "";
$dashtitle = 'Total'; 
$dynamicDashTotalSales = dynamicDashboard($TotalSales,$panelclass,$url,$Headtitle,$dashtitle);

// -- current sales
$c_status ="'REH'";
$whereLeads = null;
$whereLeads[] = "createdby IN ({$agents})";
$whereLeads[] = "lastPaymentDate >= {$firstDayThisMonth}";
$whereLeads[] = "lastPaymentDate <= {$lastDayThisMonth}";
$whereLeads[] = "ExecApproval = {$c_status}";
$currentSales = search_dynamic_loanapp($whereLeads)->rowCount();
// -- 1st & last day of the month.
$lastDayThisMonth   = date("Y-m-t");
$firstDayThisMonth  = date("Y-m-01");
$panelclass = "panel panel-yellow";
$url = 'creditrehabilitationsales?from='.$firstDayThisMonth.'&to='.$lastDayThisMonth.'&ExecApproval='.$c_status; 
$Headtitle = "";
$dashtitle = 'Current'; 
$dynamicDashCurrentSales = dynamicDashboard($currentSales,$panelclass,$url,$Headtitle,$dashtitle);
 
// -- 1st & last day of the month.
$lastDayThisMonth = "'".date("Y-m-t")."'";
$firstDayThisMonth = "'".date("Y-m-01")."'";

// -- posted sales, exclude current month.
$c_status ="'REH'";
$whereLeads = null;
$whereLeads[] = "createdby IN ({$agents})";
$whereLeads[] = "lastPaymentDate > {$lastDayThisMonth}";
$whereLeads[] = "ExecApproval = {$c_status}";
$postedSales = search_dynamic_loanapp($whereLeads)->rowCount();
$panelclass = "panel panel-yellow";
// -- 1st & last day of the month.
$lastDayThisMonth   = date("Y-m-t");
$firstDayThisMonth  = date("Y-m-01");
//$url = "creditrehabilitationsales?; 
$url = 'creditrehabilitationsales?from='.$lastDayThisMonth.'&ExecApproval='.$c_status; 
$Headtitle = "";
$dashtitle = 'Posted'; 
$dynamicDashPostedSales = dynamicDashboard($postedSales,$panelclass,$url,$Headtitle,$dashtitle);

// -- Cancelled Sales
// -- All Sales - credit rehabilitation programm subscribed by customer.
$c_status ="'CAN'";
$whereLeads = null;
$whereLeads[] = "createdby IN ({$agents})";
$whereLeads[] = "ExecApproval = {$c_status}";
$TotalSales = search_dynamic_loanapp($whereLeads)->rowCount();
$panelclass = "panel panel-red";
$url = "creditrehabilitationsales?ExecApproval=".$c_status; 
$Headtitle = "";
$dashtitle = 'Cancelled'; 
$dynamicDashCancelledSales = dynamicDashboard($TotalSales,$panelclass,$url,$Headtitle,$dashtitle);

// -- Sale per Agent
									 echo "<div class='container background-white bottom-border'>
														<div class='margin-vert-30'>
															<!-- Main Text -->

													<div id='page-wrapper'>
														<div>
															<div class='col-lg-12'>
																<h1 class='page-header'>Leads - Credit Restoration</h1>
															</div>
														</div>
													</div>".'

<!------------------------------------- Dashboards ----------------------------------->
<!-------- Leads --------->'.$dynamicDashTotalLeads.
'<!---------- Open Bucket Leads ---->'.$dynamicDashOpen.				
'<!---------- Closed Bucket Leads ---->'.
		$dynamicDashClosed
		.'<div>
			<div id="page-wrapper">
				<div>
					<div class="col-lg-12">
						<h1 class="page-header">Sales - Credit Restoration</h1>
					</div>
				</div>
			</div>
		</div>											
<!-------- Sales ------>'.
$dynamicDashTotalSales
			.'
<!-------- Current Sales ------>'.$dynamicDashCurrentSales.
'<!-------- Posted Sales ------>'.
$dynamicDashPostedSales.'<!-- Cancelled Sales -->'.
$dynamicDashCancelledSales
.$dynamicDashJudgements.$dynamicDashDefaults.$dynamicDashNotices.$dynamicDashTracealerts.$dynamicDashPaymentprofile.
$dynamicDashEnquires.$dynamicDashfinalised.$dynamicDashpartialcomplete.$dynamicdebitreview
.'</div></div>';

}
// -- Credit Rehabilitation crconsultant.
elseif($_SESSION['role'] == 'crconsultant')   // -- Dynamic Menu 13.11.2017
{				
				// -- Read all user with crmanager & crconsultant role add into an array.. use the array to search through out.
				$crrolesarray[1] = "'crmanager'";
				$crrolesarray[2] = "'crconsultant'";
				$cragents = GetCRAgents($crrolesarray);
				
				// -- All Leads - customer created by credit rehabilitation agent.
				$TotalLeads = GetTotalLeads($cragents);

				// -- Sale per Agent
				$TotalSales = GetTotalAgentLeadsSales($agentid);

								// -- Leads per Agent
$agents = implode(",",$cragents);
				
// -- Bucket Open Leads
$c_status ="'open'";
$c_open = 'open';
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$openLeads = search_dynamic_leads($whereLeads)->rowCount();

 // -- Closed Leads 
$c_status ="'closed'";
$c_closed = 'closed';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$closedLeads = search_dynamic_leads($whereLeads)->rowCount();

 // -- Finalised Leads 
$c_status ="'finalised'";
$c_finalised = 'finalised';
$whereLeads = null;
$whereLeads[] = "status = {$c_status}";
$whereLeads[] = "createdby IN ({$agents})";
$finalisedLeads = search_dynamic_leads($whereLeads)->rowCount();

// -- 1st & last day of the month.
$lastDayThisMonth = "'".date("Y-m-t")."'";
$firstDayThisMonth = "'".date("Y-m-01")."'";

// -- current sales
$whereLeads = null;
$whereLeads[] = "createdby = {$agentid}";
$whereLeads[] = "lastPaymentDate >= {$firstDayThisMonth}";
$whereLeads[] = "lastPaymentDate <= {$lastDayThisMonth}";
$currentSales = search_dynamic_loanapp($whereLeads)->rowCount();

// -- posted sales, exclude current month.
$whereLeads = null;
$whereLeads[] = "createdby = {$agentid}";
$whereLeads[] = "lastPaymentDate > {$lastDayThisMonth}";
$postedSales = search_dynamic_loanapp($whereLeads)->rowCount();

// -- All Sales - credit rehabilitation programm subscribed by customer.
$whereLeads = null;
$whereLeads[] = "createdby = {$agentid}";
$TotalSales = search_dynamic_loanapp($whereLeads)->rowCount();
 
// -- 1st & last day of the month.
$lastDayThisMonth   = date("Y-m-t");
$firstDayThisMonth  = date("Y-m-01");

									 echo "<div class='container background-white bottom-border'>
														<div class='margin-vert-30'>
															<!-- Main Text -->

													<div id='page-wrapper'>
														<div>
															<div class='col-lg-12'>
																<h1 class='page-header'>Leads - Credit Restoration</h1>
															</div>
														</div>
													</div>".'

<!------------------------------------- Dashboards ----------------------------------->
<!-------- Leads --------->
<div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Total</h3>
                                </div>
                            </div>
                        </div>
                        <a href="leads">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!---------- Open Bucket Leads ---->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$openLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Open</h3>
                                </div>
                            </div>
                        </div>
                        <a href="leads?status='.$c_open.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>				
<!---------- Closed Bucket Leads ---->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$closedLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Closed</h3>
                                </div>
                            </div>
                        </div>
                        <a href="leads?status='.$c_closed.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
<!---------- Finalised Bucket Leads ---->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$finalisedLeads.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
                                    <h3 style="color:white">Finalised</h3>
                                </div>
                            </div>
                        </div>
                        <a href="leads?status='.$c_finalised.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>				
		</div>
		<div>
													<div id="page-wrapper">
														<div>
															<div class="col-lg-12">
																<h1 class="page-header">Sales - Credit Restoration</h1>
															</div>
														</div>
													</div>
		</div>														
<!-------- Sales ------>
<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$TotalSales.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Sales</h3>
                                </div>
                            </div>
                        </div>
                        <a href="creditrehabilitationsales?createdby='.$agentid.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
</div> 
<!-------- Current Sales ------>
			<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$currentSales.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Current</h3>
                                </div>
                            </div>
                        </div>
                        <a href="creditrehabilitationsales?from='.$firstDayThisMonth.'&to='.$lastDayThisMonth.'"&createdby='.$agentid.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
            </div>
			
			


	</div>
<!-------- Posted Sales ------>
			<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
									<br/><i class="fa-5x">'.$postedSales.'</i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" style="color:white"></div>
									<h3 style="color:white">Posted</h3>
                                </div>
                            </div>
                        </div>
                        <a href="creditrehabilitationsales?from='.$lastDayThisMonth.'"&createdby='.$agentid.'">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
            </div>
</div>
<hr/>
<!-------------------------------------- EOC Dashboard -------------------------------->

									</div></div>';
}
		}
		else
		{
									// -- BCO Loan Calculator --- //
									echo '<div class="container background-white bottom-border">
									<div class="margin-vert-30">'.

														$dataGlobal['homepage']
														.'</div>
														<!-- <h3 style=" margin: 0px 40% 0 40%;">Loan Calculator</h3> -->
														<hr>
									<div class="container">
      <div class="price-box">
        <div class="row">
          <div class="col-sm-6">
                <form class="form-horizontal form-pricing" role="form">

                  <div class="price-slider">
                    <h4 class="great">Amount</h4>
                    <span>Minimum R100 is required</span>
                    <div class="col-sm-12">
                      <div id="slider_amirol"></div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Duration</h4>
                    <span>Please choose one</span>
                    <div class="btn-group btn-group-justified">
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month active-month selected-month" id="1month">1 Month</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="2month">2 Months</button>
                      </div>
                      <div class="btn-group btn-group-lg">
                        <button type="button" class="btn btn-primary btn-lg btn-block month" id="3month">3 Months</button>
                      </div>
                    </div>
                  </div>
                  <div class="price-slider">
                    <h4 class="great">Term</h4>
                    <span>Please choose one</span>
                      <input name="sliderVal" type="hidden" id="sliderVal" value="0" readonly="readonly" />
                <input name="month" type="hidden" id="month" value="1month" readonly="readonly" />

				<input name="term" type="hidden" id="term" value="monthly" readonly="readonly" />
                      <div class="btn-group btn-group-justified">
                        <!--<div class="btn-group btn-group-lg">
                    <button type="button" class="btn btn-primary btn-lg btn-block term" id="quarterly">Quarterly</button>
                  </div>-->
                        <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term active-term selected-term" id="monthly">Monthly</button>
                  </div>
                       <!-- <div class="btn-group btn-group-lg">
                          <button type="button" class="btn btn-primary btn-lg btn-block term" id="weekly">Weekly</button>
                        </div> -->
                      </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="price-form">

                  <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Annually (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">-->
                            <!-- <p class="price lead" id="total"></p>
                            <input class="price lead" name="totalprice" type="text" id="total" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Monthly Instalment(R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             <!-- <p class="price lead" id="total12"></p>-->
                            <input class="price lead" name="totalprice12" type="text" id="total12" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>
                    <!--<div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label for="amount_amirol" class="control-label">Weekly (R): </label>
                          <span class="help-text">Amount that you need to pay</span>
                        </div>
                        <div class="col-sm-6">
                            <input type="hidden" id="amount_amirol" class="form-control">
                             --><!-- <p class="price lead" id="total52"></p>
                            <input class="price lead" name="totalprice52" type="text" id="total52" disabled="disabled" style="" />
                        </div>
                    </div>
                    </div>-->
                    <div style="margin-top:30px"></div>
                    <!-- <hr class="style"> -->

                  <div class="form-group">
                      <div class="col-sm-12">
                        <a href="applicationForm" class="btn btn-primary btn-lg btn-block">Apply Now<span class="glyphicon glyphicon-chevron-right"></span></a>
                      </div>
                  </div>

				   <div class="col-md-6">
						<h3 class="padding-vert-10">Required Documents</h3>
						<ul class="tick animate fadeInRight" style="width:280px">
						<li>ID Document</li>
						<li>Latest 3 months Bank Statement</li>
						<li>Proof of Employment/Income</li>
						<li>Proof of Residence</li>
						</ul>
						<p></p>
						<br>
					</div>

                    <!-- <div class="form-group">
                      <div class="col-sm-12">
                          <img src="https://github.com/AmirolAhmad/Bootstrap-Calculator/blob/master/images/payment.png?raw=true" class="img-responsive payment" />
                      </div>
                    </div> -->

                  </div>

                </form>
            </div>
            <!-- <p class="text-center" style="padding-top:10px;font-size:12px;color:#2c3e50;font-style:italic;">Created by <a href="https://twitter.com/AmirolAhmad" target="_blank">AmirolAhmad</a></p> -->
        </div>

          </div>

      </div>

    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>';
									// -- EOC Loan Calculator -- //
									echo "<hr>
														</div>";

}

// -- Partners. -- //
echo '
  <!-- Portfolio -->
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
			<!--	<h3 style=" margin: 0px 25% 0 30%;">Our Partners Bringing Value To Our Business</h3>-->
                    <div class="row padding-top-40">
                        <ul class="portfolio-group">
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="https://www.mercantile.co.za">
                                    <figure class="animate fadeInLeft">
                                        <img alt="image1" src="assets/img/mercantile.jpg">
                                        <figcaption>
                                            <h3>Mercantile Bank</h3>
											Mercantile Bank provides a wide range of international and local banking services to the business community with a segment focus on the Portuguese market.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li> -->
                            <!-- //Portfolio Item// -->
                            <!-- Portfolio Item
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="http://www.vogsphere.co.za">
                                    <figure class="animate fadeIn">
                                        <img alt="image2" src="assets/img/vogsphere.png">
                                        <figcaption>
                                            <h3>Vogsphere (Pty) Ltd</h3>
											Vogsphere (Pty) Ltd is a custom software development company based in Krugersdorp, South Africa.
											The company was established in 2010 and registered in 2012.
                                        </figcaption>
                                    </figure>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Portfolio -->

';
// -- End Partners	-- //
?>
