<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
		
        <!-- Favicon -->
        <link href="http://vogsphere.co.za/eloan/assets/img/Icon_vogs.ico" rel="shortcut icon">
		
	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->
	
	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->
	
	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->
	
	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>
<body bgcolor="#f0f0f0" width="100%" style="margin: 0;">
    <center style="width: 100%; background: ##f0f0f0;">

        <!-- Visually Hidden Preheader Text : BEGIN 
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            (Optional) This text will appear in the inbox preview, but not the email body.
        </div>-->
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Email Header : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
			<!-- <tr>
				<td style="padding: 20px 0; text-align: center">
					<img src="http://placehold.it/200x50" width="200" height="50" alt="alt_text" border="0">
				</td>
			</tr> -->
        </table>
        <!-- Email Header : END -->
        
        <!-- Email Body : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
				<td bgcolor="#f0f0f0">
					<img src="http://vogsphere.co.za/eloan/assets/img/logo-theone.png" width="600" height="" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px;">
				</td>
            </tr>
            <!-- Hero Image, Flush : END -->

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
<?php 
									
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
require 'database.php';

$Title 			= '';
$FirstName 		= '';
$LastName 		= '';
$password 		= '';
									
// Id number
$idnumber 		= '';
$clientname 	= $Title.''.$FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= '';	
// Email
$email 			= '';	
// Street
$Street 		= '';	
// Suburb
$Suburb 		= '';	
// City
$City 			= '';	
// State
$State 			= '';	
// PostCode
$PostCode 		= '';	
// Dob
$Dob 			= '';	
// phone
$phone 			= '';	

$ApplicationId = '';
	
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
$loandate 		= '';	
$loanamount 	= '';	
$loanpaymentamount = '';	
$loanpaymentInterest = "";
$FirstPaymentDate = '';	
$LastPaymentDate  = '';	
$applicationdate  = '';	
$status 		  = '';	
$FirstName 		= 'E-loan';
$LastName 		= 'Admin';

if(isset($_SESSION))
{	
// -- BOC 27.01.2017
$personalloan = getsession('personalloan');
$loantype = getsession('loantypedesc');
$loanpaymentInterest = $personalloan."%";
// -- EOC 27.01.2017

$Title 			= getsession('Title');
$FirstName 		= getsession('FirstName');
$LastName 		= getsession('LastName');
$AdminFirstName = 'E-loan';
$AdminLastName 	= 'Admin';
$AdminTitle     = '';	
$password 		= getsession('password');
									
// Id number
$idnumber 		= getsession('idnumber');
$clientname 	= $FirstName.''.$LastName;
$suretyname 	= $clientname;
$name 			= $suretyname;		
// Phone numbers
$phone 			= getsession('phone');	
// Email
$email 			= getsession('email');	
// Street
$Street 		= getsession('Street');	
// Suburb
$Suburb 		= getsession('Suburb');	
// City
$City 			= getsession('City');	
// State
$State 			= getsession('State');	
// PostCode
$PostCode 		= getsession('PostCode');	
// Dob
$Dob 			= getsession('Dob');	
// phone
$phone 			= getsession('phone');		
//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
/* $loandate 		= getsession('LastName');	
$loanamount 	= getsession('LastName');	
$loanpaymentamount = getsession('LastName');	
$loanpaymentInterest = "20%";
$FirstPaymentDate = getsession('FirstPaymentDate');	
$LastPaymentDate  = getsession('LastPaymentDate');	
$applicationdate  = getsession('applicationdate');	
$status 		  = getsession('status');	
//$ApplicationId    =  getsession('ApplicationId');
 */
$loandate 		= getsession('loandate');	
$loanamount 	= getsession('loanamount');	
$loanpaymentamount = getsession('loanpaymentamount');	
//$loanpaymentInterest = "20%"; COMMENT IT OUT.
$FirstPaymentDate = getsession('FirstPaymentDate');	
$LastPaymentDate  = getsession('LastPaymentDate');	
$applicationdate  = getsession('applicationdate');	
$status 		  = getsession('status');	

$monthlypayment = getsession('monthlypayment');// Monthly Payment Amount 
$LoanDuration = getsession('LoanDuration');// Loan Duration
}

//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
$companyname 	= "Vogsphere Pty Ltd";
$companyStreet 	= "43 Mulder Street";
$companySuburb 	= "The reeds";
$companyPostCode = "0157";
$companyCity 	= "Centurion";
$companyphone 	= '0793401754/0731238839';	
$Appdate = date("Y-m-d");

if(getsession('status'] == 'APPR')
{
$status = 'Approved';
}
if(getsession('status'] == 'PEN')
{
$status = 'Pending';
}
if(getsession('status'] == 'CAN')
{
$status = 'Cancelled';
}
if(getsession('status'] == 'REJ')
{
$status = 'Rejected';
}
		
			
				echo "<h3>Dear $AdminTitle $AdminFirstName $AdminLastName - DEVELOPMENT TESTING ONLY!</h3>
						<p>New eloan application has been registered on <b>$Appdate</b><br> 
						 That requires your action to review, Approve or Reject.</p>
																<p>Take note : E-loan offers flexible, short-term loans from as minimun as R500.00 up to R5000.00 for new Customers.</p>
	
															<h3><b>CUSTOMER E-LOAN APPLICATION DETAILS</b></h3>
															<p>Below is a summary of customer plan details. Kindly check that all your details are correct; should there be any amendments, we ask that you update them and upload required documents via the website : <a href = 'http://www.vogsphere.co.za/eloan/customerLogin.php'>www.vogsphere.co.za/eloan/customerLogin.php</a>.</p>
															
<p>After logged in to action the eloan application go to <br>E-LOAN ADMINISTRATION > SEARCH BY DATE OR ID>UPDATE(Change Status from PENDING)</p> 
															<br>
															
															<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent;
															  margin-bottom: 20px;'>
															  
															   <tr 
															   style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;'>
															    
																<th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>LOGIN DETAILS</th>
															   </tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>ID number:</td><td><b>$idnumber</b></td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Password:</td><td><b>$password</b></td></tr>
																
															  </table>
																
															   <br>	
															<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent;margin-bottom: 20px;'>
															   <tr 
															   style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;'>
															   <th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>COMPANY PHYSICAL ADDRESS</th></tr>
																
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Company Name:</td><td><b>$companyname</b></td></tr>
																
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;text-align: left;'><td>Physical address: </td><td><b>$companyStreet</b></td></tr>
															   
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;text-align: left;'><td></td><td><b>$companySuburb</b></td></tr>
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;text-align: left;'><td></td><td><b>$companyCity</b></td></tr>
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;text-align: left;'><td></td><td><b>$companyPostCode</b></td></tr>
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;text-align: left;'><td>Contact Number: </td><td><b>$companyphone</b></td></tr>
															</table>
				<br>
				<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent;
															  margin-bottom: 20px;'>
															   <tr style = 'padding: 8px;line-height: 1.428571429;vertical-align: top;border-top: 1px solid #dddddd;'><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2' >CLIENT PHYSICAL ADDRESS</th></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Company Client:</td><td><b>$clientname</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Physical address:</td><td><b>$Street</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$Suburb</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$City</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td></td><td><b>$PostCode</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Contact Number: </td><td><b>$phone</b></td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Company Client ID: </td><td><b>$idnumber</b></td></tr>
															</table>
															<br>
										<table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent; margin-bottom: 20px;'>
															   <tr><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>TOTAL COST AND INTEREST</th></tr>
															   
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Requested Amount</td><td><b>$loanamount</td></tr>
																
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Interest Percentage</td><td><b>$loanpaymentInterest</td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Total amount repayment</td><td><b>$loanpaymentamount</td></tr>
															   
															   <tr><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>REPAYMENT ARRANGEMENTS</th></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Frequency of payment</td><td><b>Monthly</td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Method of payment</td><td><b>EFT</td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>First payment date</td><td><b>$FirstPaymentDate</td></tr>
															   <tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Last payment date</td><td><b>$LastPaymentDate</td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Application date </td><td><b>$applicationdate</td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: left;'><td>Application status</td><td><b>$status</td></tr>
																
															</table>
															<BR>
															
															  <table style = 'padding: 5px;background-color: #ffffff;border: 1px solid #dddddd;border-bottom-width: 2px;max-width: 100%;background-color: transparent;
															  margin-bottom: 20px;'>
																<tr><th style ='background-color: #f0f0f0 !important;border: 1px solid #ddd !important;
															    text-align: center;' colspan='2'>CUSTOMER REQUIRED DOCUMENTS</th></tr>
															  
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: center;'><td>ID Document</td></tr>
															
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: center;'><td>Latest Bank Statement</td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: center;'><td>Proof of Employment</td></td></tr>
																
																<tr style ='background-color: #fff !important;border: 1px solid #ddd !important;
															    text-align: center;'><td>Proof of Residence</td></tr>
															  </table>
															  ";
															
?>
															<p>Thank you.</p>
															<p><b>Kind Regards</b></p>
															<p><b>VOGSPHERE (PTY) LTD Team</b></p>
                    <!-- Button : Begin -->
                   
                    <!-- Button : END -->
                </td>
            </tr>
        </table>
          
        <!-- Email Footer : BEGIN -->
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
            <tr>
                <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                    <webversion style="color:#cccccc; text-decoration:underline; font-weight: bold;">View as a Web Page</webversion>
                    <br><br>
                    Vogsphere (Pty) Ltd<br><span class="mobile-link--footer">43 Mulder street 31 SEOUL, Centurion,Gauteng, 0158</span><br><span class="mobile-link--footer">(079)-340-1754</span>
                    <br><br> 
                    <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe>
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->

    </center>
</body>
</html>

