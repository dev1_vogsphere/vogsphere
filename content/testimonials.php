<?php
require 'database.php';
$dbnameobj    = 'ecashpdq_eloan';
$tablename = 'customer_reviews';
$queryFields = null;
$whereArray = null;
$customer_reviews = null;
// -- connect to db & table
$queryFields[] = '*';
$customer_reviews = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
$active = 'active';
?>
<div align=center ><h1><strong>Testimonials</strong></h1></div>
<div class="col-md-12">
<div class="row">
                                <!-- Testimonials - Default Full Width -->
                                <div class="col-md-12">
                                    <div class="carousel slide testimonials" id="testimonials1">
                                        <ol class="carousel-indicators">
                                            <li class="active" data-slide-to="0" data-target="#testimonials-rotate">
                                            </li>
                                            <li data-slide-to="1" data-target="#testimonials1">
                                            </li>
                                            <li data-slide-to="2" data-target="#testimonials1">
                                            </li>
                                        </ol>
                                        <div class="carousel-inner">
										<?php foreach($customer_reviews as $row){?>
                                            <div class="item <?php echo $active;?>">
                                                <div class="col-md-12">
                                                    <p class="p1">
                                                        <?php echo $row['review'];?>
                                                    </p>
                                                    <div class="testimonial-info">
                                                        <img alt="" src="<?php echo $row['picture'];?>" class="img-circle img-responsive" />
                                                        <span class="testimonial-author">
                                                            <?php echo $row['reviewer_name'];?>
                                                            <em class="p1">
																<?php echo $row['title'];?>
                                                            </em>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
												<hr/>
                                            </div>
										<?php $active = '';}?>
                                        </div>
                                    </div>
                                    <div class="testimonials-arrows pull-right">
                                        <a class="left" href="#testimonials1" data-slide="prev">
                                            <span class="fa fa-arrow-left"></span>
                                        </a>
                                        <a class="right" href="#testimonials1" data-slide="next">
                                            <span class="fa fa-arrow-right"></span>
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
</div></div>
