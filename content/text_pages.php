<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 include 'database.php';
 $pdo = Database::connect();
 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
 $tbl_name="page"; // Table name 
 $page = '';
 
 // -- Get pages - 23/01/2022
if(!function_exists('getpages'))
{
  function getpages($pdo)
  {
	$sql = "select * from page";
	$dataclient = $pdo->query($sql);

	$pagesArr = array();
	foreach($dataclient as $row)
	{
		$pagesArr[] = $row['page'];
	}
	return $pagesArr;
}}
  
// -- Pages...
// BOC -- Get pages 23/01/2022
$pagesArr = array();
$pdo = Database::connect();
$pagesArr = getpages($pdo);

if ( !empty($_POST)) 
{
 $page = $_POST['page'];
}
						   
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' >
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Pages</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                    <SELECT style='width:150px;height:30px;z-index:0;' class='form-control' id='page' name='page' size='1' style='z-index:0'>";
			    
					$pageSelect = $page;
					foreach($pagesArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $pageSelect)
					  {
						  echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';
					  }
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
					}
				
			echo "</SELECT>";
									echo "</div>	

                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $page = $_POST['page'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($page))
					    {
						$sql = "SELECT * FROM page WHERE page = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($page));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
						// -- All Records
						$sql = "SELECT * FROM page";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newpage.php>Add New Page</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Page</b></td>"; 
						echo "<td><b>Features</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['page']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['features']) . "</td>";  

						echo "<td valign='top'><a class='btn btn-success' href=editpage.php?page={$row['page']}>Edit</a></td>";						
						echo "<td><a class='btn btn-danger' href=deletepage.php?page={$row['page']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div></div>
   		<!---------------------------------------------- End Data ----------------------------------------------->