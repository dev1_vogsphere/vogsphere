<?php 
require 'database.php';
$provincenameError = null;
$provincename = '';
$count = 0;
$provinceid = 0;

if (isset($_GET['provinceid']) ) 
{ 
$provinceid = $_GET['provinceid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$provincename = $_POST['provincename'];
	$valid = true;
	
	if (empty($provincename)) { $provincenameError = 'Please enter Account Type Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE province SET provincename = ? WHERE provinceid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($provincename,$provinceid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from province where provinceid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($provinceid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$provincename = $data['provincename'];
}

?>
<div class="container background-white bottom-border">  
<div class='margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
 
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change Province Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Province successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>Province</b><br />
			<input style="height:30px" type='text' name='provincename' value="<?php echo !empty($provincename)?$provincename:'';?>"/>
			<?php if (!empty($provincenameError)): ?>
			<span class="help-inline"><?php echo $provincenameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-success">Update</button>
				<a class="btn btn-primary" href="province">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>
