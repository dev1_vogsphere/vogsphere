<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="user"; // Table name 
$passwordError = null;
$password = '';

$confirmpasswordError = null;
$confirmpassword = '';

$emailError = null;
$email = ''; 
$hash = '';
$hashError = '';

$role = '';

$count = 0;
$userid = 0;

if (isset($_GET['userid']) ) 
{ 
$userid = $_GET['userid']; 
// -- BOC Encrypt 16.09.2017 - Parameter Data.
//$userid = base64_decode(urldecode($userid)); 
// -- EOC Encrypt 16.09.2017 - Parameter Data.
}

if (isset($_GET['hash']) ) 
{ 
	$hash = $_GET['hash']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$password = $_POST['password'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];
	$confirmpassword = $_POST['confirmpassword'];
	$hashcrypto = $_POST['hashcrypto'];
	
	$valid = true;
	
	if (empty($password)) { $passwordError = 'Please enter password.'; $valid = false;}
	if (empty($confirmpassword)) { $confirmpasswordError = 'Please enter comfirm password.'; $valid = false;}

		// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}	
	
	if (empty($email)) 
	{ $emailError = 'Please enter e-mail'; $valid = false;}

	if ($password != $confirmpassword)
	{ $confirmpassword = "Your password's do not match."; $valid = false;}

	if (!password_verify($email.$hashcrypto, $hash))
	{
		$hashError = "Your password reset key is invalid.";$valid = false;
	}					
	
	if ($valid) 
	{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE user SET password = ?,email = ? WHERE userid = ?"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$passwordEncrypted = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($passwordEncrypted,$email,$userid)); 
			Database::disconnect();
			$count = $count + 1;
	}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from user where userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$email = $data['email'];
		$hashcrypto = $data['password'];
		if (!password_verify($email.$hashcrypto, $hash))
		{
			$hashError = "Your password reset key is invalid.</br>Please go to login and request a password reset.";$valid = false;
		}					
}

?>
<div class="container background-white bottom-border">   
	<!-- <div class="row"> -->
	<div class="row margin-vert-30">
		<div class="col-md-6 col-md-offset-3 col-sm-offset-3">	
		<form action='' class="signup-page" method='POST'> 
			<div class="panel-heading">
				<h2 class="panel-title">
									<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
										 Reset User Password
									</a>
								</h2>
				<?php # error messages
						if (isset($message)) 
						{
							foreach ($message as $msg) 
							{
								printf("<p class='status'>%s</p></ br>\n", $msg);
							}
						}
						# success message
						if($count !=0)
						{
							printf("<p class='status'>Your password has been successfully reset.</p>");
						}
						
						if(empty($hashError)){}else{printf("<p class='alert alert-error'>$hashError</p>");}
						
				?>				
			</div>				

			<p <?php if(empty($hashError)){}else{echo "hidden";}?>><b>User ID</b><br />
			<input style="height:30px" type='text' name='userid' value="<?php echo !empty($userid)?$userid:'';?>" readonly />
			</p>
			
			<p <?php if(empty($hashError)){}else{echo "hidden";}?>><b>E-mail</b><br />
			<input style="height:30px" type='text' name='email' value="<?php echo !empty($email)?$email:'';?>" readonly/>
			<?php if (!empty($emailError)): ?>
			<span class="help-inline"><?php echo $emailError;?></span>
			<?php endif; ?>
			</p>
			
			<p class="error" <?php if(empty($hashError)){}else{echo "hidden";}?>><b>New Password</b><br />
			<input style="height:30px" type='password' name='password' value="<?php echo !empty($password)?$password:'';?>"/>
			<?php if (!empty($passwordError)): ?>
			<span class="help-inline"><?php echo $passwordError;?></span>
			<?php endif; ?>
			</p>

			<p <?php if(empty($hashError)){}else{echo "hidden";}?>><b>Confirm Password</b><br />
			<input style="height:30px" type='password' name='confirmpassword' value="<?php echo !empty($confirmpassword)?$confirmpassword:'';?>"/>
			<?php if (!empty($confirmpasswordError)): ?>
			<span class="help-inline"><?php echo $confirmpasswordError;?></span>
			<?php endif; ?>
			</p>						 
		
		<input style="height:30px" type='hidden' name='hashcrypto' value="<?php echo !empty($hashcrypto)?$hashcrypto:'';?>"   />

		<table <?php if(empty($hashError)){}else{echo "hidden";}?>>
		<tr>
			<td><div >
				<button type="submit" class="btn btn-primary">Reset Password</button>
				</div>
			</td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>