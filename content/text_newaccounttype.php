<?php 
require 'database.php';
$accounttypedescError = null;
$accounttypedesc = '';
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$accounttypedesc = $_POST['accounttypedesc'];
	$valid = true;
	
	if (empty($accounttypedesc)) { $accounttypedescError = 'Please enter Account Type Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO accounttype (accounttypedesc) VALUES(?) "; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($accounttypedesc));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">   
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 

<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Account Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Account Type successfully added.</p>");
		}
?>
				
</div>				
			<p><b>Account Type</b><br />
			<input style="height:30px" type='text' name='accounttypedesc'/>
			<?php if (!empty($accounttypedescError)): ?>
			<span class="help-inline"><?php echo $accounttypedescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'accounttype';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
