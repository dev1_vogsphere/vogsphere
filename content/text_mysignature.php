<?php
// -- Library usage
$rootpath = $_SERVER['DOCUMENT_ROOT'];
$basepath = str_replace('mysignature.php','signature.php',$_SERVER['PHP_SELF']);
$basepath = $rootpath.$basepath;
require $basepath;
$dataSignatures = null;

// -- Declarations
$data = array();
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$userid 	 = getsession('username');
$sessionrole = getsession('role');
$message 	 = '';
$signcount   = 0;
	
if(!empty($_POST))
{
}
else
{
	$data['userid'] = $userid;
	$dataSignatures = db_get_signatures($data);
}

$blank = "iVBORw0KGgoAAAANSUhEUgAAASwAAABkCAYAAAA8AQ3AAAADNElEQVR4Xu3YsY2EUBBEwb8+JEC2mywJgM9KZ510zpn7RkUE/atHbfB6nudZPgIECHy/wPtlsL6/JQkJEPgRMFgOgQCBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBGwGBlqhKUAAGD5QYIEMgIGKxMVYISIGCw3AABAhkBg5WpSlACBAyWGyBAICNgsDJVCUqAgMFyAwQIZAQMVqYqQQkQMFhugACBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBGwGBlqhKUAAGD5QYIEMgIGKxMVYISIGCw3AABAhkBg5WpSlACBAyWGyBAICNgsDJVCUqAgMFyAwQIZAQMVqYqQQkQMFhugACBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBGwGBlqhKUAAGD5QYIEMgIGKxMVYISIGCw3AABAhkBg5WpSlACBAyWGyBAICNgsDJVCUqAgMFyAwQIZAQMVqYqQQkQMFhugACBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBGwGBlqhKUAAGD5QYIEMgIGKxMVYISIGCw3AABAhkBg5WpSlACBAyWGyBAICNgsDJVCUqAgMFyAwQIZAQMVqYqQQkQMFhugACBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBGwGBlqhKUAAGD5QYIEMgIGKxMVYISIGCw3AABAhkBg5WpSlACBAyWGyBAICNgsDJVCUqAgMFyAwQIZAQMVqYqQQkQMFhugACBjIDBylQlKAECBssNECCQETBYmaoEJUDAYLkBAgQyAgYrU5WgBAgYLDdAgEBG4O9gXde1zvNc931nXiEoAQKzBLZtW8dxrH3ffz/MYM2q2WsIzBD492DNeK5XECAwUMA/rIGlehKBqQIGa2qz3kVgoIDBGliqJxGYKmCwpjbrXQQGChisgaV6EoGpAgZrarPeRWCggMEaWKonEZgqYLCmNutdBAYKvD8TU9NIU2QjigAAAABJRU5ErkJggg==";
?>
		<link href="assets/css/jquery.signaturepad.css" rel="stylesheet">
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/signature.numeric-1.2.6.min.js"></script> 
		<script src="assets/js/signature.bezier.js"></script>
		<script src="assets/js/jquery.signaturepad.js"></script> 
		<script type='text/javascript' src="assets/js/signature.html2canvas.js"></script>
		<script src="assets/js/signature.json2.min.js"></script>
		<style type="text/css">
			body{
				font-family:monospace;
				text-align:center;
			}
			
			#btnSaveSign {
				color: #fff;
				background: #f99a0b;
				padding: 5px;
				border: none;
				border-radius: 5px;
				font-size: 20px;
				margin-top: 10px;
			}
			#btnClearSign 
			{
				color: #fff;
				background: #f99a0b;
				padding: 5px;
				border: none;
				border-radius: 5px;
				font-size: 20px;
				margin-top: 10px;
			}
			#signArea{
				width:304px;
				margin: 50px auto;
			}
			.sign-container {
				width: 60%;
				margin: auto;
			}
			.sign-preview {
				width: 150px;
				height: 50px;
				border: solid 1px #CFCFCF;
				margin: 10px 5px;
			}
			.tag-ingo {
				font-family: cursive;
				font-size: 12px;
				text-align: left;
				font-style: oblique;
			}
</style>
<div class="container background-white bottom-border">
 <div class='row margin-vert-30'>
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<h2>Signature Pad</h2>
		<div id='saved' name='saved'>
		</div>
		<div id="signArea" >
			<h2 class="tag-ingo">Put signature below,</h2>
			<div class="sig sigWrapper" style="height:auto;">
				<div class="typed"></div>
				<canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
				<textarea id="blank" style="display:none"><?php echo $blank; ?></textarea>
			</div>
		</div>
		<button id="btnSaveSign">Save Signature</button>
		<button id="btnClearSign">Reset Signature</button>
		<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
    </div>
			<div id="sign-preview" name="sign-preview" class="sign-container">
		<?php
		//$image_list = glob("./doc_signs/*.png");
		$index = 0;
		foreach($dataSignatures as $image)
		{
			if($index < 2)
			{
		?>
		<img src="<?php echo $image['usersignfile']; ?>" class="sign-preview" />
		<?php
		$index = $index + 1;
			}
		}
		?>
		</div>
		<input type="hidden" name="signcount" id="signcount" value="<?php echo $signcount; ?>" />

  </div>
		
		<script>
			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90,bgColour :'#ffffff',penColor:'#145394'});
											
			});
			
			$("#btnSaveSign").click(function(e){
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						var blankcanvas = document.getElementById("blank").value;						
						var saved = document.getElementById("saved");
						var userid = document.getElementById("userid").value;
						var signatures = document.getElementById("sign-preview");
						var signcount  = document.getElementById("signcount").value;
						//--alert(signatures.innerHTML);
						
						if(blankcanvas === img_data)
						{
							//alert("Not Saved");
							saved.className  = 'alert alert-error';			
							saved.innerHTML = "You cannot save blank signature";								   
						}
						else
						{
							//ajax call to save image inside folder
							jQuery.ajax({
								type: 'post',
								url: 'save_sign.php',
								data: { img_data:img_data,userid:userid,signcount:signcount },
								success: function (response) 
								{
								  arr = response.split("|");
								  								//alert(response);

								  signatures.innerHTML = arr[1] + signatures.innerHTML; 
								  saved.className  = 'alert alert-success fade in';								  
								  saved.innerHTML = "signature successfully saved.";
								  src = document.getElementById('myimg'+signcount);								  
								  //src = $("#myimg"+signcount).attr("src");
								  //alert(arr[0]);
								  previous = parseInt(signcount) - 1;
								  if(previous >= 0)
								  {$("#myimg"+previous).remove();}
							  
								  $("#myimg"+signcount).attr("src",arr[0]);
								  signcount = signcount + 1;
								  document.getElementById("signcount").value = signcount;

								  //window.location.reload();
								}
							});
						}
					}
				});
			});
			
			$("#btnClearSign").click(function(e){
					$('#signArea').signaturePad().clearCanvas ();
					var saved = document.getElementById("saved");
					saved.className  = '';			
					saved.innerHTML = "";
			});
		  </script>   
</div>
