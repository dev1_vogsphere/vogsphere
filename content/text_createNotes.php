<?php 
include 'database_ffms.php';
$pdo = db_connect::db_connection();			
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$customerid = null;
$ApplicationId = null;
$notesbody=null;
$rejectionreasons='';
$changedby=$_SESSION['username'];	
$tbl  = 'notes';
$noteStatus='';
$errorStatus='';

if ( !empty($_GET['customerid'])) 
{
	$customerid = $_REQUEST['customerid'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$customerid = base64_decode(urldecode($customerid)); 
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}

if ( !empty($_GET['ApplicationId'])) 
{
	$ApplicationId = $_REQUEST['ApplicationId'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.		
	$ApplicationId = base64_decode(urldecode($ApplicationId));
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}
if ( !empty($_GET['CustomerName'])) 
{
	$CustomerName = $_REQUEST['CustomerName'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.		
	$CustomerName = base64_decode(urldecode($CustomerName));
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}
if(isset($_POST['note']))
{
$notesbody = $_POST['note'];
}
//Create a note
//Get Cutsomer Notes
if($_POST)
{
try 
{
	$pdo = db_connect::db_connection();			
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO $tbl (notesbody, Member_id,createdby) VALUES ('$notesbody','$customerid','$changedby')";
	// use exec() because no results are returned
	$pdo->exec($sql);
	//echo "New record created successfully";
	$noteStatus="Note successfully created!. Return to notes view to see the newly created note.";
	$errorStatus="alert alert-success";
	Database::disconnect();
} catch(PDOException $e) 
{
	echo $sql . "<br>" . $e->getMessage();
	$noteStatus="Oops! Something went wrong. Please try again.";
	$errorStatus="alert alert-danger";
}
}
$CustomerIdEncoded = urlencode(base64_encode($customerid));
$ApplicationIdEncoded = urlencode(base64_encode($ApplicationId));
$CustomerNameEncoded = urlencode(base64_encode($CustomerName));
		
?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<!--<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Create a note
						    </a>
					    </h2>
						</div>-->						 
				   </tr>
			   
			
					<div class="form-group">
					<h3>
				 	<label for="comment">Create a note for: <?php echo $CustomerName;?></label>
					</h3>
					<textarea class="form-control" id="note" name="note" rows="5" id="comment" required="required"></textarea>
				
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 	<!--<div class="help-block with-errors"></div>-->
					<div class="<?php echo $errorStatus;?>">
					<?php echo $noteStatus;?>
					</div>
				 	</div>
    
					  <div class="form-actions">
						<?php
						echo '<button type="submit" class="btn btn-danger">Save</button>';
						echo '<a class="btn" href="notes.php?customerid='.$CustomerIdEncoded.'&CustomerName='.$CustomerNameEncoded.'&ApplicationId='.$ApplicationIdEncoded.'">Back</a>';
						?>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->