<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="user"; // Table name 
$passwordError = null;
$password = '';

$emailError = null;
$email = ''; 

$role = '';

$count = 0;
$userid = 0;

$Original_email = '';

// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$UserIdDecoded = "";
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.

if (isset($_GET['userid']) ) 
{ 
$userid = $_GET['userid']; 
// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$userid = base64_decode(urldecode($userid)); 
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
}

if (!empty($_POST)) 
{   $count = 0;
	$password = $_POST['password'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];
	$Original_email = $_POST['Original_email'];
	
	$valid = true;
	
	if (empty($password)) { $passwordError = 'Please enter password Description.'; $valid = false;}
		// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}	
	
	if (empty($email)) 
	{ $emailError = 'Please enter e-mail'; $valid = false;}
	else
	{
	//echo '<h2>modise'.$Original_email.'</h2>';
	// User - E-mail.
		if ($valid and ($Original_email != $email))
		{
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
		}
	 }
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE user SET password = ?,email = ? WHERE userid = ?"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$password = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($password,$email,$userid)); 
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from user where userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header("Location: custAuthenticate");
		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		$password = $data['password'];
		$email = $data['email'];
		$Original_email = $data['email'];

}

?>
<div class="container background-white bottom-border">
    <div class="row margin-vert-30">
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="text-center">Change User Details</h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>User successfully updated.</p>");
		}
?>
				
</div>		
  <div class="center-block">				
		<div class="control-group">					
			<label>User ID</label> 
			<div class="controls">
				<input style="height:30px"  type='text' name='userid' value="<?php echo !empty($userid)?$userid:'';?>" readonly />
			</div>
		</div>	
		<div class="control-group <?php echo !empty($passwordError)?'error':'';?>">
		   <label >Password<span style="color:red">*</span></label> 
		   <div class="controls">
			<input style="height:30px"  type='password' name='password' value="<?php echo !empty($password)?$password:'';?>"/>
			<?php if (!empty($passwordError)): ?>
			<span class="help-inline"><?php echo $passwordError;?></span>
			<?php endif; ?>
		   </div> 
		</div>	
		<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
		  <label>E-mail<span style="color:red">*</span></label> 
		   <div class="controls">
			<input style="height:30px" type='text' name='email' value="<?php echo !empty($email)?$email:'';?>"/>
			<?php if (!empty($emailError)): ?>
			<span class="help-inline"><?php echo $emailError;?></span>
			<?php endif; ?>
		   </div>	
			<p>			
				<input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
			</p>
		</div>	
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update User</button>
				<?php if($_SESSION['role'] == 'admin'){?>
				<a class="btn btn-primary" href="users">Back To Listing</a>
				<?php }?>
				</div></td>
		</tr>
		</table>	
		</div>
		</form> 
</div></div></div>
