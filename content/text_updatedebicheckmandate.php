<?php
require 'database.php';
// -- redirect if user has been logged off.
$customerLogin = 'customerLogin';

// -- its okay, navigate to login back.
if(empty(getsession('role')))
{
	redirect($customerLogin);
}
$reference 						='';
if (isset($_GET['reference']) ) 
{ 
	$reference = $_GET['reference']; 
}

$changedby = getsession('username');

// -- MASTER DATA.
$tracking_indicatorArr 			   = getsession('tracking_indicatorArr');
$Client_ID_TypeArr 	   			   = getsession('Client_ID_TypeArr');
$Account_TypeArr 	   			   = getsession('Account_TypeArr');
$date_adjustment_rule_indicatorArr = getsession('date_adjustment_rule_indicatorArr');
$instalment_occurenceArr 		   = getsession('instalment_occurenceArr');
$Branch_CodeArr 				   = getsession('Branch_CodeArr');
$FrequencyArr 					   = getsession('FrequencyArr');
$debtor_auth_codeArr 			   = getsession('debtor_auth_codeArr');
$datadebtor_adjustment_categoryArr = getsession('datadebtor_adjustment_categoryArr'); 
$collection_day_Arr 			   = getsession('collection_day_Arr'); 
$debit_value_typeArr 			   = getsession('debit_value_typeArr');
 
// -- Database Declarations and config:  
// -- Database Declarations and config:  
/*$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
//$datadocument_type = $pdo->query($sql);

//$sql = 'SELECT * FROM bankbranch';
//$databankbranch = $pdo->query($sql);

// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);
$dataentry_class = array();

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_debitvalue_types';
$datadebtor_debitvalue_types = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_entry_class_codes';
$datadebtor_entry_class_codes = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_collection_codes';
$datacollection_day_codes = $pdo->query($sql);


$sql = 'SELECT * FROM debtor_adjustment_category';
$datadebtor_adjustment_category = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_auth_codes';
$datadebtor_auth_code = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_instalment_occurrence';
$datadebtor_instalment_occurrence = $pdo->query($sql);
*/
$notification = array();
$notificationsList = array();
// -- Declare -- //
$contract_reference 			='';
$tracking_indicator 			='';
$debtor_auth_code 				='';
$auth_type 						='';
$instalment_occurence 			='';
$mandate_initiation_date 		='';
$first_collection_date 			='';
$collection_amount_currency 	='ZAR';
$collection_amount 				='';
$maximum_collection_currency 	='ZAR';
$maximum_collection_amount 		='';
$entry_class 					='';
$debtor_account_name 			='';
$debtor_identification 			='';
$debtor_account_number 			='';
$debtor_account_type 			='';
$debtor_branch_number 			='';
$debtor_contact_number 			='';
$debtor_email 					='';
$collection_date 				='';
$date_adjustment_rule_indicator  ='';
$adjustment_category 			='';
$adjustment_rate 				='';
$adjustment_amount_currency 	='ZAR';
$adjustment_amount 				='';
$first_collection_amount_currency = 'ZAR'; 
$first_collection_amount 		='';
$debit_value_type 				='';
$cancellation_reason 			='';
$amended 						='';
$status_code 					='1';
$status_desc 					='pending';
$document_type 					='';
$amendment_reason_md16 			='';
$amendment_reason_text 			='';
$tracking_cancellation_indicator ='';
// -- EOC Declare -- //

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
$Account_Holder='';
$Account_Type  ='';
$Account_Number='';
$Branch_Code   ='';
$No_Payments   ='';
$Amount        ='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class   ='';
$Bank_Reference='';
$contact_number = '';
$Notify = '';
$collection_day = '';
$collection_day_codeSelect = '';
$entry_classSelect = '';

$adjustment_amount_style = 'none';
$adjustment_rate_style = 'none';
$debit_type_style = 'none';

$message = '';
	

/* -- dropdown selection.
$Client_ID_TypeArr = array();
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}

$tracking_indicatorArr[] = 'Y|Yes';
$tracking_indicatorArr[] = 'N|No';

$date_adjustment_rule_indicatorArr[] = 'Y|Yes';
$date_adjustment_rule_indicatorArr[] = 'N|No';

$instalment_occurenceArr = array();
foreach($datadebtor_instalment_occurrence as $row)
{
	$instalment_occurenceArr[] = $row['instalment_occurrence'].'|'.$row['instalment_occurrence_desc'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}

$debtor_auth_codeArr = array();
$count = 0;
foreach($datadebtor_auth_code as $row)
{
	// -- first on list will be default auth_type.
	if($count == 0){$auth_type = $row['debtor_auth_type'];}
	$debtor_auth_codeArr[] = $row['debtor_auth_code'].'|'.$row['debtor_auth_code'].' - '.$row['debtor_auth_type'];
	$count = $count + 1;
}

$debit_value_typeArr = array();
foreach($datadebtor_debitvalue_types as $row)
{
	$debit_value_typeArr[] = $row['debtor_debitvalue_type'].'|'.$row['debitvalue_desc'];
}

$entry_classArr = array();
foreach($datadebtor_entry_class_codes as $row)
{
	$entry_classArr[] = $row['debtor_entry_class'].'|'.$row['debtor_entry_class_desc'];
}

$datadebtor_adjustment_categoryArr = array();
foreach($datadebtor_adjustment_category as $row)
{
	$datadebtor_adjustment_categoryArr[] = $row['category'].'|'.$row['category_desc'];
}
*/
/*$entry_classArr = array();
foreach($datadebtor_collection_codes as $row)
{
	$entry_classArr[] = $row['collection_day_code'].'|'.$row['collection_desc'];
}
*/


$NotifyArr[] = 'No';					 
$NotifyArr[] = 'Yes';
						  
$arrayInstruction = null;
					
$UserCode = null; 
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}
	
	$debicheckusercode = '';
	// -- debicheckusercode
	if(getsession('debicheckusercode'))
	{
		$debicheckusercode = getsession('debicheckusercode');
	}
	// -- Overwrite User code with field debicheckusercode
	$UserCode = $debicheckusercode;

// -- Validation
$maximum_collection_amountError = '';
$first_collection_dateError = '';
$first_collection_amountError = '';

$valid = true;	
	
//// --- IF $_POST NOT EMPTY	
   if (!empty($_POST)) 
   {
	   // styles
	    $adjustment_amount_style = getpost('adjustment_amount_style');
		$adjustment_rate_style = getpost('adjustment_rate_style');
		$debit_type_style = getpost('debit_type_style');
		$auth_type = getpost('auth_type');
	   // -- Logged User Details.
		if(getsession('username'))
		{
			$userid = getsession('username');
		}
		// -- get $_POST values
		$arrayInstruction[1] = $reference 							= getpost('reference');
		$arrayInstruction[2] = $contract_reference 				= getpost('contract_reference');
		$arrayInstruction[3] = $tracking_indicator 				= getpost('tracking_indicator');
		$arrayInstruction[4] = $debtor_auth_code 					= getpost('debtor_auth_code');
		// -- auth_type first 9  characters.
		$auth_type = substr($auth_type,0,9);
		$auth_type = trim($auth_type);

		$arrayInstruction[5] = $auth_type; 							//= getpost('auth_type');
		$arrayInstruction[6] = $instalment_occurence 				= getpost('instalment_occurence');
		$arrayInstruction[7] = $Frequency 			= getpost('Frequency');
		$arrayInstruction[8] = $mandate_initiation_date 			= getpost('mandate_initiation_date');
		$arrayInstruction[9] = $first_collection_date 				= getpost('first_collection_date');
		$arrayInstruction[10] = $collection_amount_currency 		= getpost('collection_amount_currency');
		$arrayInstruction[11] = $collection_amount 					= getpost('collection_amount');
		$arrayInstruction[12] = $maximum_collection_currency 		= getpost('maximum_collection_currency');
		$arrayInstruction[13] = $maximum_collection_amount 			= getpost('maximum_collection_amount');
		$arrayInstruction[14] = $entry_class 						= getpost('entry_class');
		$arrayInstruction[15] = $debtor_account_name 				= getpost('debtor_account_name');
		$arrayInstruction[16] = $debtor_identification 				= getpost('debtor_identification');
		$arrayInstruction[17] = $debtor_account_number 				= getpost('debtor_account_number');
		$arrayInstruction[18] = $debtor_account_type 				= getpost('debtor_account_type');
		$arrayInstruction[19] = $debtor_branch_number 				= getpost('debtor_branch_number');
		$arrayInstruction[20] = $debtor_contact_number 				= getpost('debtor_contact_number');
		$arrayInstruction[21] = $debtor_email 						= getpost('debtor_email');
		// -- We to get the day.
		$collection_day 					= getpost('collection_day');
		// -- Validate CollectionDay
		if(empty($collection_day))
		{
			$message = 'Please choose collection day';$valid = false;
		}
		
		$db_collection_day = '';
		if($valid)
		{
			if(!empty($collection_day) && ($Frequency != 'WEEK' and $Frequency != 'ADHO'))
			{
				$col_date=date_create($collection_day);
						//echo $col_date;

				$col_date = DateTime::createFromFormat("Y-m-d", $collection_day);
				$db_collection_day = $col_date->format("d");
				$arrayInstruction[22] = $db_collection_day;
			}
			else
			{
				$arrayInstruction[22] = $collection_day;
			}
		}
		
		//echo $arrayInstruction[22];
		$status_code  = '1';
		$status_desc  = 'pending';
		$arrayInstruction[23] = $date_adjustment_rule_indicator 	= getpost('date_adjustment_rule_indicator');
		$arrayInstruction[24] = $adjustment_category 				= getpost('adjustment_category');
		$arrayInstruction[25] = $adjustment_rate 					= getpost('adjustment_rate');
		$arrayInstruction[26] = $adjustment_amount_currency 		= getpost('adjustment_amount_currency');
		$arrayInstruction[27] = $adjustment_amount 					= getpost('adjustment_amount');
		$arrayInstruction[28] = $first_collection_amount_currency   = getpost('first_collection_amount_currency'); 
		$arrayInstruction[29] = $first_collection_amount 			= getpost('first_collection_amount');
		$arrayInstruction[30] = $debit_value_type 					= getpost('debit_value_type');
		$arrayInstruction[31] = $cancellation_reason 				= getpost('cancellation_reason');
		$arrayInstruction[31] = ' ';		
		$arrayInstruction[32] = $amended 							= getpost('amended');
		$arrayInstruction[33] = $status_code;// 						= getpost('status_code');
		//$arrayInstruction[33] = ' ';
		$arrayInstruction[34] = $status_desc;// 						= getpost('status_desc');
		//$arrayInstruction[34] = ' ';		
		$arrayInstruction[35] = $document_type 						= getpost('Client_ID_Type');
		$arrayInstruction[36] = $amendment_reason_md16 				= getpost('amendment_reason_md16');
		$arrayInstruction[36] = ' ';
		$arrayInstruction[37] = $amendment_reason_text 				= getpost('amendment_reason_text');
		$arrayInstruction[37] = ' ';
		$arrayInstruction[38] = $tracking_cancellation_indicator 	= getpost('tracking_cancellation_indicator');		
		$arrayInstruction[39] = $UserCode;
		$arrayInstruction[40] = $userid;
		$arrayInstruction[41] = date('Y-m-d');
		
		
		// -- Populate an array with values.
		//echo $Frequency;
	   // -- Default CollectionDay Code
		$sql = "SELECT * FROM debtor_collection_codes where frequency_code = '".$Frequency."'";	  
		$datacollection_day_codes = $pdo->query($sql);

// -- Data Validation.
// 1
		if( $maximum_collection_amount > ($collection_amount * 1.5))
		{
			$maximum_collection_amountError = 'Maximum value is 1.5 times Collection amount';$valid = false;
		}
		
		if($maximum_collection_amount < $collection_amount)
		{
			$maximum_collection_amountError = 'Maximum value cannot less than Collection amount';$valid = false;
		}
// -- Instalment Occurrence:
// 2
if($instalment_occurence == 'OOFF')
{
	if(!empty($first_collection_amount))
	{$first_collection_amountError = 'first_collection_amount must be blank';$valid = false;}
	
	if(!empty($first_collection_date))
	{$first_collection_dateError = 'first_collection_date must be blank'; $valid = false;}

}else
{
		if(empty($first_collection_amount))
	{$first_collection_amountError = 'first_collection_amount is required';$valid = false;}
	
	if(empty($first_collection_date))
	{$first_collection_dateError = 'first_collection_date is required'; $valid = false;}
}

// -- adjacent rate/amount default.
if(empty($adjustment_rate))
{
	$adjustment_rate = '000.00';
}

if(empty($adjustment_amount))
{
	$adjustment_amount = '00000000000.00';
}		

// -- Tel or Email

		if($valid)
		{
		    $message = auto_create_debicheck_mandate($arrayInstruction); 
			if($message == 'success')
			{
				$smstemplatename = 'single_debitorder_uploaded';
				$applicationid = 0;								
				// -- Sent to info@ecashmeup.com
				auto_email_collection('8707135963082',$smstemplatename,$applicationid,$reference,$Action_Date);				
			}								
		}				
			
   }
   else
   {
	   // -- Default Mode
	  $Frequency = "WEEK";

	  	$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from mandates where reference = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($reference));		
		$dataMandates = $q->fetch(PDO::FETCH_ASSOC);
		
	    $sql = "SELECT * FROM debtor_collection_codes where frequency_code = '".$Frequency."'";	  
	    $datacollection_day_codes = $pdo->query($sql);
	  
	  // -- Read from database.
		$auth_type = $dataMandates['auth_type'];//getpost('auth_type');
	   // -- Logged User Details.
		if(getsession('username'))
		{
			$userid = getsession('username');
		}
		// -- get $_POST values
		//$arrayInstruction[1] = $reference 						= getpost('reference');
		$arrayInstruction[2] = $contract_reference 				= $dataMandates['contract_reference'];
		$arrayInstruction[3] = $tracking_indicator 				= $dataMandates['tracking_indicator'];
		$arrayInstruction[4] = $debtor_auth_code 				= $dataMandates['debtor_auth_code'];
		// -- auth_type first 9  characters.
		$auth_type = substr($auth_type,0,9);
		$auth_type = trim($auth_type);

		$arrayInstruction[5] = $auth_type; 							//= getpost('auth_type');
		$arrayInstruction[6] = $instalment_occurence 				= $dataMandates['instalment_occurence'];
		$arrayInstruction[7] = $Frequency 							= $dataMandates['frequency'];
		$arrayInstruction[8] = $mandate_initiation_date 			= $dataMandates['mandate_initiation_date'];
		$arrayInstruction[9] = $first_collection_date 				= $dataMandates['first_collection_date'];
		$arrayInstruction[10] = $collection_amount_currency 		= $dataMandates['collection_amount_currency'];
		$arrayInstruction[11] = $collection_amount 					= $dataMandates['collection_amount'];
		$arrayInstruction[12] = $maximum_collection_currency 		= $dataMandates['maximum_collection_currency'];
		$arrayInstruction[13] = $maximum_collection_amount 			= $dataMandates['maximum_collection_amount'];
		$arrayInstruction[14] = $entry_class 						= $dataMandates['entry_class'];
		$arrayInstruction[15] = $debtor_account_name 				= $dataMandates['debtor_account_name'];
		$arrayInstruction[16] = $debtor_identification 				= $dataMandates['debtor_identification'];
		$arrayInstruction[17] = $debtor_account_number 				= $dataMandates['debtor_account_number'];
		$arrayInstruction[18] = $debtor_account_type 				= $dataMandates['debtor_account_type'];
		$arrayInstruction[19] = $debtor_branch_number 				= $dataMandates['debtor_branch_number'];
		$arrayInstruction[20] = $debtor_contact_number 				= $dataMandates['debtor_contact_number'];
		$arrayInstruction[21] = $debtor_email 						= $dataMandates['debtor_email'];
		$amendment_reason_text = $dataMandates['amendment_reason_text'];
		
		$arrayInstruction[23] = $date_adjustment_rule_indicator 	= $dataMandates['date_adjustment_rule_indicator'];
		$arrayInstruction[24] = $adjustment_category 				= $dataMandates['adjustment_category'];
		$arrayInstruction[25] = $adjustment_rate 					= $dataMandates['adjustment_rate'];
		//$arrayInstruction[26] = $adjustment_amount_currency 		= $dataMandates['adjustment_amount_currency'];
		$arrayInstruction[27] = $adjustment_amount 					= $dataMandates['adjustment_amount'];
		$arrayInstruction[29] = $first_collection_amount 			= $dataMandates['first_collection_amount'];

		if($adjustment_rate != '000.00')
		{
			$adjustment_rate_style 	 = 'block';
			$adjustment_amount_style = 'block';	
			$debit_type_style 		 = 'block';				
		}
		
		if($adjustment_amount  != '00000000000.00')
		{
			$adjustment_amount_style = 'block';
			$adjustment_rate_style 	 = 'block';
			$debit_type_style 		 = 'block';	
		}
		
		// -- We to get the day.
		$collection_day 					= $dataMandates['collection_date'];
		if($dataMandates['frequency'] != 'WEEK' and $dataMandates['frequency'] != 'ADHO')
		{
			$collection_day  = $dataMandates['collection_date_text'];;
		}
		//echo $Frequency;
	 
   }
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}
</style>
<div class="container background-white bottom-border">
     <form id="contact-form" method="POST" action="javascript:Update1(this);"  role="form">
	 			<?php if($menu_item != 1000){  ?>			
		<h2 class="text-center">Update DebiCheck Mandate</h2>
					<?php }  ?>
         <div class="messages">
		 <?php
		 if (isset($message)) 	
		 {
			 if($message == 'success')
			 {
				 printf("<p class='status'>%s</p></ br>\n", $message);	
			 }
			elseif($message == 'Duplicate record already exist.')
			 {
				 printf("<p class='alert alert-warning'>%s</p></ br>\n", $message);	
			 }		
			else
			{
				if(!empty($message))
				{printf("<p class='alert alert-error'>%s</p></ br>\n", $message);}					
			}				
		 }

									?>
		 </div>

         <div class="controls">

           <!--Contract Details-->
           <div class="container-fluid" style="border:1px solid #ccc ">
             <div class="row">
			 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Amount">Collection Amount : <span style="color:red">*</span></label>
                         <input id="collection_amount" type="number" name="collection_amount" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" required="required" data-error="collection_amount is required." value = "<?php echo $collection_amount;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3" style="display:none">
                     <div class="form-group">
                         <label for="form_Reference_1">Reference: <span style="color:red">*</span></label>
                         <input id="reference" type="text" name="reference" class="form-control" placeholder="" required="optional" data-error="Reference  is required." maxlength="9" value ="<?php echo $reference;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="first_collection_amount">1st Collection Amount :</label>
						 <div class="control-group <?php echo !empty($first_collection_amountError)?'error':'';?>">
						  <div class="controls"> 
                         <input id="first_collection_amount" type="number" name="first_collection_amount" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" value = "<?php echo $first_collection_amount;?>">
                         <div class="help-block with-errors">
						  <?php if (!empty($first_collection_amountError)): ?>
							<span class="help-inline"><?php echo $first_collection_amountError; ?></span>
						<?php endif; ?>							 
						 </div></div></div>
                     </div>
                 </div>
                 <div class="col-md-3" style="display:none">
                     <div class="form-group">
                         <label for="form_Reference_2">Contract Reference: </label>
                         <input id="contract_reference" type="text" name="contract_reference" class="form-control" placeholder=""  value = "<?php echo $contract_reference;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                <div class="col-md-3">
                     <div class="form-group">
                       <label for="form_Frequency">Tracking Indicator:<span style="color:red">*</span></label>
                       <select class="form-control" id="tracking_indicator" name="tracking_indicator">
  						  <?php
						  	$tracking_indicatorSelect = $tracking_indicator;
						  foreach($tracking_indicatorArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $tracking_indicatorSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  
						  }
						  ?>

                      </select>
                       <div class="help-block with-errors"></div>
					   </div>
			    </div>	
				<div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Client_ID_Type">Document type: </label>
                         <select class="form-control" id="document_type" name="document_type">
						  <?php
						  	$Client_ID_TypeSelect = $document_type;

						  foreach($Client_ID_TypeArr as $row)
						  {
							  if(!empty($row)){
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $Client_ID_TypeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	
							  }						  
						  }
						  ?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>						
             </div>
           </div>
           <!-- 04 -  05 -->		   
		   <div class="container-fluid" style="border:1px solid #ccc;display:none">
             <div class="row">
					<div class="col-md-4" style="display:none">
                     <div class="form-group">
						<label for="form_Frequency">Debtor Authentication Code:<span style="color:red">*</span></label>
                       <select class="form-control" id="debtor_auth_code" name="debtor_auth_code" onChange="authentication_type(this);">
  						  <?php
						  	$debtor_auth_codeSelect = $debtor_auth_code;

						  foreach($debtor_auth_codeArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $debtor_auth_codeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  

						  }
						  ?>

                      </select>
                       <div class="help-block with-errors"></div>
                        
                     </div>
				    </div>
					<div class="col-md-4" style="display:none">
                     <div class="form-group">
                       <label for="form_Frequency">Instalment Occurrence:<span style="color:red">*</span></label>
                       <select class="form-control" id="instalment_occurence" name="instalment_occurence">
  						  <?php
						  	$instalment_occurenceSelect = $instalment_occurence;

						  foreach($instalment_occurenceArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $instalment_occurenceSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  
						  }
						  ?>

                      </select>
                       <div class="help-block with-errors"></div></div>
					</div>
			
                   </div>
           </div>
           <!-- 08 -  09 -->		   
			 <div class="container-fluid" style="border:1px solid #ccc ">
				 <div class="row">
					   <div class="col-md-3">
						 <div class="form-group">
						 <label for="form_Action_Date">Initiation Date:<span style="color:red">*</span></label>
                        <input style="height:30px" id="mandate_initiation_date" placeholder="yyyy-mm-dd" type="date" name="mandate_initiation_date" class="form-control" placeholder="" required="required" data-error="mandate_initiation_date is required." value = "<?php echo $mandate_initiation_date;?>">
                        <!-- <div class="help-block with-errors"></div> -->
						 </div>
					   </div>	 
					   <div class="col-md-3">
						 <div class="form-group">
						 <label for="form_Action_Date">First Collection Date:</label>
						<div class="control-group <?php echo !empty($first_collection_dateError)?'error':'';?>">
						  <div class="controls">                       
							<input style="height:30px" id="first_collection_date" type="date" placeholder="yyyy-mm-dd" name="first_collection_date" class="form-control" placeholder="" value = "<?php echo $first_collection_date;?>">
                        <div class="help-block with-errors">
						  <?php if (!empty($first_collection_dateError)): ?>
							<span class="help-inline"><?php echo $first_collection_dateError; ?></span>
						<?php endif; ?>	
						</div>
						 </div></div>
					   </div></div>	
					    <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Amount">Max Amount: <span style="color:red">*</span></label>
                         <div class="control-group <?php echo !empty($maximum_collection_amountError)?'error':'';?>">
									<div class="controls">
						 <input id="maximum_collection_amount" type="number" name="maximum_collection_amount" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" required="required" data-error="maximum_collection_amount is required." value = "<?php echo $maximum_collection_amount;?>">
                         <div class="help-block with-errors">
						 <?php if (!empty($maximum_collection_amountError)): ?>
							<span class="help-inline"><?php echo $maximum_collection_amountError; ?></span>
						<?php endif; ?>	
						</div></div></div>
                     </div>
                 </div>	
				 <div class="col-md-3">
                     <div class="form-group">
						<label for="form_Frequency">Frequency:<span style="color:red">*</span></label>
                       <select class="form-control" id="Frequency" name="Frequency" size="1" onChange="valueselect2(this);">
  						  <?php
						  	$FrequencySelect = $Frequency;

						  foreach($FrequencyArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $FrequencySelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  
						  }
						  ?>

                      </select>
                       <div class="help-block with-errors"></div>
                     </div>
				     </div>	
                     <div class="col-md-4" hidden>
                     <div class="form-group">
                         <label for="form_Initials">Collection Amount Currency: <span style="color:red">*</span></label>
                         <input id="collection_amount_currency" type="text" name="collection_amount_currency" class="form-control" placeholder="" readonly value = "<?php echo $collection_amount_currency;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
					   
				 </div>
			 </div>
           <!-- 10 -  11 -->
		   <div class="container-fluid" style="border:1px solid #ccc;display:none">
             <div class="row">
                 <div class="col-md-4" hidden>
                     <div class="form-group">
                         <label for="form_Account_Holder">Max Collection Currency<span style="color:red">*</span></label>
                         <input id="maximum_collection_currency" type="text" name="maximum_collection_currency" class="form-control" placeholder="" readonly  value = "<?php echo $maximum_collection_currency;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 			 
			 </div>
		   </div>
           <!-- 14 -  15 -->	
		   <div class="container-fluid" style="border:1px solid #ccc ">
             <div class="row">
			     <div class="col-md-4" style="display:none">
			        <div class="form-group">
			 		   <label for="form_Frequency">Entry Class:<span style="color:red">*</span></label>
                       <select class="form-control" id="entry_class" name="entry_class">
  						  <?php
						  	$entry_classSelect = $entry_class;

						  foreach($entry_classArr as $row)
						  {
							  if(!empty($row)){
							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
							  if($rowData[0] == $entry_classSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}	}						  
						  }
						  ?>
                      </select>
                       <div class="help-block with-errors"></div>
					   </div>
				  </div> 
				  <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Holder">Account Name<span style="color:red">*</span></label>
                         <input id="debtor_account_name" type="text" name="debtor_account_name" class="form-control" placeholder="" required="required" data-error="debtor_account_name is required." value = "<?php echo $debtor_account_name;?>">
                         <div class="help-block with-errors"></div>
                     </div>
				  </div>
				   <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Number">Account Number:<span style="color:red">*</span></label>
                         <input id="debtor_account_number" type="text" name="debtor_account_number" class="form-control" placeholder="" required="required" data-error="debtor_account_number is required." maxlength="13" value = "<?php echo $debtor_account_number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				  <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Type:">Account Type:<span style="color:red">*</span></label>
                         <select class="form-control" id="debtor_account_type" name="debtor_account_type">
						  <?php 
						  	$Account_TypeSelect = $debtor_account_type;
							$rowData = null;
						  foreach($Account_TypeArr as $row)
						  {
							  if(!empty($row)){
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							  if($rowData[0] == $Account_TypeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							  }
						  }?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                </div>		
				<div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Branch_Code">Branch :<span style="color:red">*</span></label>
                         <select class="form-control" id="debtor_branch_number" name="debtor_branch_number">
						 <?php 
						  	$Branch_CodeSelect = $debtor_branch_number;
							$rowData = null;
							foreach($Branch_CodeArr as $row)
							{
								if(!empty($row)){
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $Branch_CodeSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
								}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
			 </div>
		   </div>
           <!-- 16 -  17 -->	
		   
           <!--Client Indetification-->
           <div class="container-fluid" style="border:1px solid #ccc;display:none">
             <div class="row">
			  
                
						 
             </div>
           </div>
           <!--Bank Account Details-->
           <div class="container-fluid" style="border:1px solid #ccc ">
             <div class="row">                 
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Client_ID_No">Identification:<span style="color:red">*</span></label>
                         <input id="debtor_identification" type="text" name="debtor_identification" class="form-control" placeholder="" required="required" data-error="debtor_identification is required." value = "<?php echo $debtor_identification;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="debtor_contact_number">Telephone Contact</label>
                         <input id="debtor_contact_number" type="text" name="debtor_contact_number" class="form-control" value = "<?php echo $debtor_contact_number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>	
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="debtor_email">Email Contact</label>
                         <input id="debtor_email" type="text" name="debtor_email" class="form-control" value = "<?php echo $debtor_email;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>		

				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="amendment_reason_text">Amendment Reason:<span style="color:red">*</span></label>
                         <input required="required" data-error="amendment reason is required." id="amendment_reason_text" type="text" name="amendment_reason_text" class="form-control" value = "<?php echo $amendment_reason_text;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>	
             </div>
           </div>
           <!--Collection Generation Details-->
           <div class="container-fluid" style="border:1px solid #ccc ">
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
					 <label for="entry_class">Collection Day:</label>
						  <?php if($Frequency != 'WEEK' and $Frequency != 'ADHO'){?>
							<input style='height:30px' id='collection_day' name='collection_day' placeholder='yyyy-mm-dd' class='form-control' type='date' value ="<?php echo $collection_day; ?>" />
						 <?php }else{?>
						 <SELECT class="form-control" id="collection_day" name="collection_day" size="1" >
							<?php
							$collection_day_codeSelect = $collection_day;
							$frequency_codeSelect = $Frequency;
							$collection_day_Des = '';
							$count = 0;
							//print_r($Frequency);
							foreach($datacollection_day_codes as $row)
							{
							  // -- Service_Type of a selected Service_Mode
								$frequency_code 	   = $row['frequency_code'];
								$collection_day_Des = $row['collection_desc'];
								$collection_day_code 	   = $row['collection_day_code'];
									//echo $entry_class.' == '.$entry_classSelect;
								// -- Select the Selected Service_Mode 
							  if($frequency_code == $frequency_codeSelect)
							   {
									if($collection_day_codeSelect == $collection_day_code)
									{
									echo "<OPTION value=$collection_day_code selected>$collection_day_code - $collection_day_Des</OPTION>";
									}
									else
									{
									 echo "<OPTION value=$collection_day_code>$collection_day_code - $collection_day_Des</OPTION>";
									}
							   }
							   $count = $count + 1;
							}
							
							if(empty($count))
							{
								echo "<OPTION value=0>No collection day codes</OPTION>";
							}
							?>
							</SELECT>	
						 <?php }?>	                        
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="date_adjustment_rule_indicator">Adjustment Indicator:<span style="color:red">*</span></label>
                         <select class="form-control" id="date_adjustment_rule_indicator" name="date_adjustment_rule_indicator">
						 <?php 
						  	$date_adjustment_rule_indicatorSelect = $date_adjustment_rule_indicator;
							$rowData = null;
							foreach($date_adjustment_rule_indicatorArr as $row)
							{
								if(!empty($row)){
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $date_adjustment_rule_indicatorSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
								}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Branch_Code">Adjustment Category:<span style="color:red">*</span></label>
                         <select class="form-control" id="adjustment_category" name="adjustment_category" onChange="adjustment_category1(this);">
						 <?php 
						  	$adjustment_categorySelect = $adjustment_category;
							$rowData = null;
							foreach($datadebtor_adjustment_categoryArr as $row)
							{
								if(!empty($row)){
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $adjustment_categorySelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
								}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <div class="col-md-3">				 
                 <div class="form-group" id='debit_typ' name='debit_typ' style='display:<?php echo $debit_type_style;?>'>
                         <label for="form_Branch_Code">Debit Value Type:<span style="color:red">*</span></label>
                         <select class="form-control" id="debit_value_type" name="debit_value_type" onChange="debitvalue_types(this);">
						 <?php 
						  	$debit_value_typeSelect = $debit_value_type;
							$rowData = null;
							foreach($debit_value_typeArr as $row)
							{
								if(!empty($row))
								{
									if(!empty($row)){
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $debit_value_typeSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
									}
								}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
			 </div>
                 <div class="col-md-6">
                     <div class="form-group">
                         <input id="User_Code" type="hidden" name="User_Code" class="form-control" value="<?php echo getsession('debicheckusercode'); ?>" placeholder="" required="required" data-error="debicheck usercode is required.">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
             </div>
           </div>
		   <div class="container-fluid" style="border:1px solid #ccc ">
             <div class="row">
			    <div class="col-md-4">
                     <div class="form-group" id='adj_rate' name='adj_rate' style="display:<?php echo $adjustment_rate_style;?>">
                         <label for="form_Amount">Adjustment Rate:</label>
                         <input id="adjustment_rate" type="number" name="adjustment_rate" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" value = "<?php echo $adjustment_rate;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-4" hidden>
                     <div class="form-group">
                         <label for="form_Account_Holder">Adjustment Amount Currency:</label>
                         <input id="adjustment_amount_currency" type="text" name="adjustment_amount_currency" class="form-control" placeholder=""  readonly value = "<?php echo $adjustment_amount_currency;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <div class="col-md-4">
                     <div class="form-group" id='adj_amnt' name='adj_amnt' style="display:<?php echo $adjustment_amount_style;?>">
                         <label for="form_Amount">Adjustment Amount:</label>
                         <input id="adjustment_amount" type="number" name="adjustment_amount" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="" value = "<?php echo $adjustment_amount;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>				 
			 </div>
		   </div>
		   <div class="container-fluid" style="border:1px solid #ccc;display:none">
             <div class="row">
                 <div class="col-md-4" hidden>
                     <div class="form-group">
                         <label for="first_collection_amount_currency">First Collection Amount Currency</label>
                         <input id="first_collection_amount_currency" type="text" name="first_collection_amount_currency" class="form-control" placeholder="" readonly  value = "<?php echo $first_collection_amount_currency;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>

		   </div></div>
		   <div class="container-fluid" style="border:1px solid #ccc" hidden >
             <div class="row">
                 <div class="col-md-4">
                     <div class="form-group">
                         <label for="tracking_cancellation_indicator">Tracking_cancellation_indicator</label>
                         <input id="tracking_cancellation_indicator" type="text" name="tracking_cancellation_indicator" class="form-control" placeholder="" readonly  value = "N">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
			</div>
			 <div class="row">
                 <div class="col-md-4">
                     <div class="form-group">
                         <label for="auth_type">Authentication Type</label>
                         <input id="auth_type" type="text" name="auth_type" class="form-control" placeholder="" readonly value="<?php echo $auth_type; ?>"/>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
			</div>
			</div>
			<!-- style amount & rate & debit_type -->
			<div class="container-fluid" style="border:1px solid #ccc" hidden >
			 <input hidden id="adjustment_amount_style" type="text" name="adjustment_amount_style" class="form-control" placeholder="" readonly value="<?php echo $adjustment_amount_style; ?>"/>
             <input hidden id="adjustment_rate_style" type="text" name="adjustment_rate_style" class="form-control" placeholder="" readonly value="<?php echo $adjustment_rate_style; ?>"/>
             <input hidden id="debit_type_style" type="text" name="debit_type_style" class="form-control" placeholder="" readonly value="<?php echo $debit_type_style; ?>"/>
			</div>
            <!--Generate Paymentbuttons -->
             <div class="row">
             <!-- </br></br> -->
                 <div class="col-md-3">
                       <button style="margin: 8px 0;" type="submit" class='btn btn-primary' value="Generate Payments" >Submit to the bank</button>
                 </div>
				 <div class="col-md-6">							
					<p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
				 </div>
				 <div class="col-md-6" style="display:none">
					<input id="refdynamic" type="text" name="refdynamic" class="form-control" placeholder="" readonly />
					<input id="amendment_reason_md16" type="text" name="amendment_reason_md16" class="form-control" placeholder="" value="<?php echo $amendment_reason_md16; ?>" readonly />					
				 </div> 
<input style = "display:none" id="changedby" type="text"  name="changedby" class="form-control" placeholder="" readonly value ="<?php echo $changedby;?>" />				 
             </div> 

         </div>
     </form>

</div>
<script>
function authentication_type(sel) 
{
	var authentication_code = sel.options[sel.selectedIndex];  
	var auth_type = authentication_code.innerHTML; 
	auth_type = auth_type.substring(7,16);	
	document.getElementById('auth_type').value = auth_type;
	//alert(auth_type);
}
 
function valueselect2(sel) 
 {
		var frequency = sel.options[sel.selectedIndex].value;    
		//alert(sel.id);		
		var selectedCollection_Day = document.getElementById('collection_day').value;
		//alert(frequency);
		//alert(arrayFromPHP.toString());
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectCollectionDays.php",
				       data:{frequency:frequency,collection_day:selectedCollection_Day},
						success: function(data)
						 {	
						 //alert(data);
							//var book = data;//JSON.parse(data);
							
							jQuery("#collection_day").replaceWith(data);
						 }
					});
				});				  
			  return false;
  }
  
 function adjustment_category1(sel) 
 {
		var adjustment_category = sel.options[sel.selectedIndex].value;   
		var adjustment_amnt = document.getElementById('adj_amnt');
		var adjustment_amnt_style = document.getElementById('adjustment_amount_style');
		var adjustment_rate = document.getElementById('adj_rate');
		var adjustment_rate_style = document.getElementById('adjustment_rate_style');
		
		var adjustment_rate_v = document.getElementById('adjustment_rate');
		var adjustment_amnt_v = document.getElementById('adjustment_amount');

		var debit_typ = document.getElementById('debit_typ');
		var debit_typ_style = document.getElementById('debit_type_style');
		
		var debit_value_type = document.getElementById('debit_value_type');

	if(adjustment_category == 'N')
	{
	  	adjustment_amnt.style.display = "none";
		adjustment_amnt_style.value = "none";

	    adjustment_rate.style.display = "none";
		adjustment_rate_style.value = "none";

		debit_typ.style.display = "none";
		debit_typ_style.value = "none";
		
		debit_value_type.selectedIndex = 0; //Default to Fixed contract
		adjustment_rate_v.value = '0,00';
		adjustment_amnt_v.value = '0,00';
	}
	else
	{
	  	adjustment_amnt.style.display = "block";	  	
	  	adjustment_amnt_style.value = "block";	  	

		adjustment_rate.style.display = "block";
		adjustment_rate_style.value = "block";
		
		debit_typ.style.display = "block";
		debit_typ_style.value = "block";
		
		debit_value_type.selectedIndex = 1; //Default to VARIABLE contract

	}	
  			  return false;
 }
 function debitvalue_types(sel) 
 {
		var debitvalue_type = sel.options[sel.selectedIndex].value;
		var adjustment_category = document.getElementById('adjustment_category');//sel.options[sel.selectedIndex].value; 
		
		var adjustment_amnt = document.getElementById('adj_amnt');
		var adjustment_amnt_style = document.getElementById('adjustment_amount_style');
		var adjustment_rate = document.getElementById('adj_rate');
		var adjustment_rate_style = document.getElementById('adjustment_rate_style');
		
		var adjustment_rate_v = document.getElementById('adjustment_rate');
		var adjustment_amnt_v = document.getElementById('adjustment_amount');

		var debit_typ = document.getElementById('debit_typ');
		var debit_typ_style = document.getElementById('debit_type_style');
		
		var debit_value_type = document.getElementById('debit_value_type');
		
	if(debitvalue_type == 'FIXED')
	{

	  	adjustment_amnt.style.display = "none";
		adjustment_amnt_style.value = "none";

	    adjustment_rate.style.display = "none";
		adjustment_rate_style.value = "none";
		
		adjustment_rate_v.value = '0,00';
		adjustment_amnt_v.value = '0,00';
		
		adjustment_category.selectedIndex = 0; // -- Default to Never Adjustment Rate or Amount.

	}
	else
	{
	  	adjustment_amnt.style.display = "block";	  	
	  	adjustment_amnt_style.value = "block";	  	

		adjustment_rate.style.display = "block";
		adjustment_rate_style.value = "block";

	}	
  			  return false;
 }
 function Update1(elem)
 {
	 //alert("Modise-3"+elem);
	 var feedback = "";
	 
	 var reference = document.getElementById('reference');
	 var refdynamic = document.getElementById('refdynamic');
	 if( refdynamic !==  null)
	 {
		refdynamic.id = reference.value;
	 } else
	 {
		 refdynamic = document.getElementById(reference.value);
	 }
	// alert(refdynamic.id );
	 refdynamic.innerHTML = 'Edit';
	// toggleUpdate(refdynamic);

// -- Declarations
	var id 								 = refdynamic.id;
	var idtracking_indicator		  	 = "tracking_indicator";
	var idFrequency                  	 = "Frequency";
	var idmandate_initiation_date	  	 = "mandate_initiation_date";
	var idfirst_collection_date      	 = "first_collection_date";
	var idmaximum_collection_amount  	 = "maximum_collection_amount";
	var iddebtor_account_name 	      	 = "debtor_account_name";
	var iddebtor_identification 	  	 = "debtor_identification";
	var iddocument_type	          		 = "document_type";
	var iddebtor_account_number  	  	 = "debtor_account_number";
	var iddebtor_account_type	      	 = "debtor_account_type";
	var iddebtor_branch_number  	  	 = "debtor_branch_number";
	var iddebtor_contact_number	  		 = "debtor_contact_number";
	var iddebtor_email               	 = "debtor_email";
	var idcollection_date            	 = "collection_day";
	var iddate_adjustment_rule_indicator = "date_adjustment_rule_indicator";
	var idadjustment_category        	 = "adjustment_category";
	var idadjustment_rate            	 = "adjustment_rate";
	var idadjustment_amount          	 = "adjustment_amount";
	var idfirst_collection_amount 	  	 = "first_collection_amount";
	var iddebit_value_type           	 = "debit_value_type";
	var idamendment_reason_text          = "amendment_reason_text";
    var idamendment_reason_md16          = "amendment_reason_md16";
	var idcollection_amount 			 = "collection_amount";
	var instalment_occurence			 = "instalment_occurence";
    var value 							 = '';
	var adjustment_amount 				 = '';
	var adjustment_rate 				 = '';
	var adjustment_category	 			 = '';
	var valid 						 	 = true;
	var maximum_collection_amount 		 = 0;
	var collection_amount 				 = 0;
	var debit_value_type				 = '';
	var message_info					 = document.getElementById('message_info');
	// -- Validate Required if Empty.
	// -- Update debicheck mandate.
		var objects_array_blank = null;
	
		var objects_array =
		{'tracking_indicator' 			: idtracking_indicator
		,'Frequency' 					: idFrequency
		,'mandate_initiation_date' 		: idmandate_initiation_date
		//,'first_collection_date' 		: idfirst_collection_date
		,'maximum_collection_amount' 	: idmaximum_collection_amount
		,'debtor_account_name' 			: iddebtor_account_name
		,'debtor_identification' 		: iddebtor_identification
		,'document_type' 				: iddocument_type
		,'debtor_account_number' 		: iddebtor_account_number
		,'debtor_account_type' 			: iddebtor_account_type
		,'debtor_branch_number' 		: iddebtor_branch_number
		//,'debtor_contact_number' 		: iddebtor_contact_number
		//,'debtor_email' 				: iddebtor_email
		,'collection_date' 				: idcollection_date
		,'date_adjustment_rule_indicator' 	: iddate_adjustment_rule_indicator
		,'adjustment_category' 			: idadjustment_category
		//,'adjustment_rate' 			: idadjustment_rate
		//,'adjustment_amount' 			: idadjustment_amount
		//,'first_collection_amount' 		: idfirst_collection_amount
		,'debit_value_type' 			: iddebit_value_type
		,'amendment_reason_text' 		: idamendment_reason_text
		};

		// -- Instalment Occurrence:
		value = document.getElementById('instalment_occurence').value;

if(value == 'OOFF')
{
	objects_array_blank = {'first_collection_date':idfirst_collection_date,
						   'first_collection_amount': idfirst_collection_amount};
}
else
{
	objects_array['first_collection_date'] 	 = idfirst_collection_date;
	objects_array['first_collection_amount'] = idfirst_collection_amount;
}

// -- Rule 0
// -- adjacent rate/amount default.
adjustment_amount = document.getElementById(idadjustment_amount).value;
if(CheckIsEmpty(adjustment_amount))
{
	document.getElementById(idadjustment_amount).value = '00000000000.00';
}
adjustment_rate = document.getElementById(idadjustment_rate).value; 
if(CheckIsEmpty(adjustment_rate))
{
	document.getElementById(idadjustment_rate).value = '000.00';
}	

// Rule 1 -- U can only enter either adjustment amount or rate not both.				
		 // --ignoire defaults
		adjustment_category 	 = document.getElementById(idadjustment_category).value;
		debit_value_type 		 = document.getElementById(iddebit_value_type).value;
		if(adjustment_category !== 'N')
		{			//alert(debit_value_type);
			if(debit_value_type !== 'FIXED')
			{	
				if(adjustment_amount === '00000000000.00'  && adjustment_rate === '000.00')
				{
						message = 'Please enter either adjustment amount or rate not both.'; 
						valid = false;
						document.getElementById(idadjustment_rate).focus();
						//alert(message);	
						message_info.innerHTML = message;	
				}
				/*else
				// Rule 2 - If Never, do no validate 
				{
					if(adjustment_category !== 'N')
					{
						message = 'Please enter either adjustment amount or rate.'; alert(message); valid = false;
						document.getElementById(idadjustment_rate).focus();
					}				
				}*/
			}
		 }
		// -- Rules 3 - email or telephone number
		debtor_email = document.getElementById(iddebtor_email).value;
		debtor_contact_number = document.getElementById(iddebtor_contact_number).value;
		if(CheckIsEmpty(debtor_email) && CheckIsEmpty(debtor_contact_number))
		{
			message = 'Please enter either telephone number or email.';  valid = false;
			message_info.innerHTML = message;
			message_info.className  = 'alert alert-error';
			//alert(message);
		}

// -- Rule 4
// -- Data Validation.
maximum_collection_amount = document.getElementById(idmaximum_collection_amount).value;
collection_amount = document.getElementById(idcollection_amount).value;

		if( maximum_collection_amount > (collection_amount * 1.5))
		{
			message = 'Maximum value is 1.5 times Collection amount';valid = false;
			document.getElementById(idmaximum_collection_amount).focus();
			//alert(message);
			message_info.innerHTML = message;
			message_info.className  = 'alert alert-error';
		}
		
		if(maximum_collection_amount < collection_amount)
		{
			message = 'Maximum value cannot less than Collection amount'; valid = false;
			document.getElementById(idmaximum_collection_amount).focus();
			message_info.innerHTML = message;
			message_info.className  = 'alert alert-error';
		}		

	if(valid)
	 {
		if(IsEmpty(objects_array))
		{
				if(IsNotEmpty(objects_array_blank))
				{ 
					//e = document.getElementById(idFrequency);
					//feq = <?php echo '"'.$Frequency.'"'; ?>;
					//alert(feq+ e.options[e.selectedIndex].value);
					//alert(document.getElementById(idFrequency).value);
					// -- Values to updated.
					var values_array =
					{'tracking_indicator' 				: document.getElementById(idtracking_indicator).value
					,'Frequency' 						: document.getElementById(idFrequency).value
					,'mandate_initiation_date' 			: document.getElementById(idmandate_initiation_date).value
					,'first_collection_date' 			: document.getElementById(idfirst_collection_date).value
					,'maximum_collection_amount' 		: document.getElementById(idmaximum_collection_amount).value
					,'debtor_account_name' 				: document.getElementById(iddebtor_account_name).value
					,'debtor_identification' 			: document.getElementById(iddebtor_identification).value
					,'document_type' 					: document.getElementById(iddocument_type).value
					,'debtor_account_number' 			: document.getElementById(iddebtor_account_number).value
					,'debtor_account_type' 				: document.getElementById(iddebtor_account_type).value
					,'debtor_branch_number' 			: document.getElementById(iddebtor_branch_number).value
					,'debtor_contact_number' 			: document.getElementById(iddebtor_contact_number).value
					,'debtor_email' 					: document.getElementById(iddebtor_email).value
					,'collection_date' 					: document.getElementById(idcollection_date).value
					,'date_adjustment_rule_indicator' 	: document.getElementById(iddate_adjustment_rule_indicator).value
					,'adjustment_category' 				: document.getElementById(idadjustment_category).value
					,'adjustment_rate' 					: document.getElementById(idadjustment_rate).value
					,'adjustment_amount' 				: document.getElementById(idadjustment_amount).value
					,'first_collection_amount' 			: document.getElementById(idfirst_collection_amount).value
					,'debit_value_type' 				: document.getElementById(iddebit_value_type).value
					,'amendment_reason_text' 			: document.getElementById(idamendment_reason_text).value
					,'amendment_reason_md16'			: document.getElementById(idamendment_reason_md16).value 
					,'collection_amount'				: document.getElementById(idcollection_amount).value,
					'changedby':document.getElementById('changedby').value};
					// -- Pass reference & array of values to updated.
					feedback = updatedebicheckmandate(id,values_array);					
				}
		}
     }
 }
</script>