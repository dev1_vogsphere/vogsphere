<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
require  $filerootpath.'/functions.php'; // -- 18 September 2021
$today=date("Y/m/d");
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);

$sql = 'SELECT * FROM document_type';
$datadocument_type = $pdo->query($sql);

// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'SELECT * FROM service_type ORDER BY idservice_type';
$dataservice_type = $pdo->query($sql);

$sql = 'SELECT * FROM service_mode ORDER BY idservice_mode';
$dataservice_mode = $pdo->query($sql);

$sql = 'SELECT * FROM debtor_frequency_codes';
$datafrequency = $pdo->query($sql);

$sql = 'SELECT * FROM entry_class';
$dataentry_class = $pdo->query($sql);


$notification = array();
$notificationsList = array();

$Service_Mode = '';
$Service_Type = '';
$entry_class = '';
$Client_ID_No  ='';
$Client_ID_Type = '';
$Initials      ='';
$Account_Holder='';
$Account_Type  ='';
$Account_Number='';
$Branch_Code   ='';
$No_Payments   ='';
$Amount        ='';
$Frequency     ='';
$Action_Date   ='';
$Service_Mode  ='';
$Service_Type  ='';
$entry_class   ='';
$Bank_Reference='';
$contact_number = '';
$Notify = '';

$entry_classSelect = '';
$message = '';
/* -- dropdown selection. */
$Client_ID_TypeArr = array();
$Client_ID_TypeArr[] = 'South African Identification Number';
$Client_ID_TypeArr[] = 'Passport';
$Client_ID_TypeArr[] = 'South African Business Registration Number';
$Client_ID_TypeArr[] = 'Foreign Passport';

/*
foreach($datadocument_type as $row)
{
	$Client_ID_TypeArr[] = $row['document_type'].'|'.$row['document_desc'];
}
*/

$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$dataentry_classArr = Array();
foreach($dataentry_class as $row)
{
	$dataentry_classArr[] = $row['identry_class'].'|'.$row['Entry_Class_Des'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
$FrequencyArr[] = 'Last Working day of the Month';
/*
foreach($datafrequency as $row)
{
	$FrequencyArr[] = $row['frequency_code'].'|'.$row['frequency_codes_desc'];
}
*/

$NotifyArr[] = 'No';
$NotifyArr[] = 'Yes';

$arrayInstruction = null;

$UserCode = null;
$IntUserCode = null;
$Reference_1 = '';
$Reference_2 = '';

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}

// BOC -- Get sub branches 22/02/2022
$branchesArr = array();
$pdoObj = Database::connectDB();
$branchesArr = getsubbranches($pdoObj,$IntUserCode);

// -- Default Mode
$Service_Mode = "EFT";
$sql = "SELECT * FROM entry_class where service_mode = '".$Service_Mode."'";
$dataentry_class = $pdo->query($sql);
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>

<div class="container background-white bottom-border">

</br>
<h3>Capture Payment</h3>
<br/>
<div class="" id="alermainscreen" role="alert">
<span id ="msg_feedback"></span>
</div>
</br>
<form id="contact-form2" method="POST" action="#" style="display:block" role="form">
				<form>

				<div class="col-md-3">
				<div class="form-group">
				<input style="height:40px;z-index:0" required id="Client_Reference" type="text" name="Client_Reference" class="form-control" placeholder="Client Reference"  data-error="Initials is required." value = "">
				<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group">
					<SELECT style="height:40px;z-index:0" class="form-control" id="srUserCode" name="srUserCode" size="1" style='z-index:0'>
					<?php
						$datasrUserCodeSelect = $srUserCode;
						foreach($branchesArr as $row)
						{
						$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
						if($rowData[0] == $datasrUserCodeSelect)
						{
							echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';
							$companyname = $rowData[1];
						}
						else
						{echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
						}
					?>
					</SELECT>
						
				</div>
			</br>
				<div class="form-group">
				<input style="height:40px;z-index:0" id="Sales_Agent" type="text" name="Sales_Agent" class="form-control" placeholder="Sales Agent" readonly data-error="Reference 1 is required." value ="<?php echo $userid; ?>" />
				<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group">
				<input id="im_payment_date" type="date"  name="im_payment_date" class="form-control" readonly value = "<?php echo $today;?>">
				<div class="help-block with-errors">
				<div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
				<span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
				</div>
				</div>
				</div>
			
				<div class="form-check">
				<input style="height:40px;z-index:0" id="amount" type="typeNumber" name="amount" class="form-control" placeholder="Amount Paid" value = "">
				<div class="help-block with-errors"></div>
				</div>
				<button type="submit" class="btn btn-primary  pull-right" onclick="submitpayment();">Submit</button>';
				</br>
				</br>
				</div>
				</br>
				</form>
		</br>

	<br/>
</form>
</div>
<script>
function submitpayment(){
	var feedback="sucessful";
	var clientreference=document.getElementById("Client_Reference").value;
    var branch=document.getElementById("srUserCode").value;
    var agent=document.getElementById("Sales_Agent").value;
	var paymentdate=document.getElementById("im_payment_date").value;
	var amount=document.getElementById("amount").value;
	var delayInMilliseconds = 5000; //1 second
	var message = '';
	var message_info = document.getElementById('msg_feedback');
	var str_data = '';
	var valid 		 = true;
	//alert(clientreference);
	//alert(branch);
	//alert(agent);
	//alert(paymentdate);
	//alert(amount);
	if(valid)
	{
	var list = {clientreference:clientreference,branch:branch,agent:agent,paymentdate:paymentdate,amount:amount};
	var datarequestfinal=JSON.stringify(list);
	var  urlString="https://demoecashmeup.dedicated.co.za:9443/springboot-ecmu-0.0.2-SNAPSHOT/api/notifycontribution";
	//var  urlString="http://localhost:8080/api/notifycontribution";
	//alert(id);			
	jQuery(document).ready(function()
	{
	jQuery.ajax({  type: 'POST',
	   url:urlString,
	   data:datarequestfinal,
	   contentType:'application/json',
		success: function(data)
		 {				
			feedback = data;
			console.log(feedback);
		    //$("#alermainscreen").addClass("alert alert-success");
			//document.getElementById("msg_feedback").innerHTML=feedback;
			alert(feedback);
			
		 },
		 error: function(data) {
		 
			var error='Error Message:' + ' Code:'+JSON.stringify(error.status)+' status: '+JSON.stringify(error.statusText);
			//alert(error);
			//$("#alermainscreen").addClass ("alert alert-danger");
			//document.getElementById("msg_feedback").innerHTML=error;
			alert(feedback);
		 }			

	});
	});
	}
	else
	{
	message_info.className  = 'alert alert-error';
	}	
	return str_data;		

}

function delayFunc(){
 //setTimeout(Querystatus, 1000);
 $(this).delay(1000000).queue(function (){
	  $(this).text(' 1 complated');
	  $(this).dequeue();
 });
	 
 location.reload(true);
}
</script>
