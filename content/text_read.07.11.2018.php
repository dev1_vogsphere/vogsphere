<!-- Debit Order Schedule 2018.06.02 -->
<!----------- debitorderschedule.js -----------------> 
<script src="assets/js/debitorderschedule.js"></script>
<!-- End Debit Order Schedule -->
<?php 
	require 'database.php';
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$dbname = "eloan";

	$customerid = null;
	$ApplicationId = null;
	$dataUserEmail = '';
	$role = '';//
	
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$location = "Location: customerLogin";
	$rejectreason = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
	}
	
	if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
	if ( null==$customerid ) 
	    {
		header($location);
		}
		else if( null==$ApplicationId ) 
		{
		header($location);
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";	
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		  
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header($location);
		}

		if( isset($data) && ($data!==null) )	
		{
		}
		else
		{
						header($location);

		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		$sql = 'SELECT * FROM user WHERE userid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
	    $dataUserEmail = $q->fetchAll();
		
		// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //

		// --------------------- BOC 2017.04.15 ------------------------- //
		//1. --------------------- Bank and Branch code ------------------- //
		  $sql = 'SELECT * FROM bank WHERE bankid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname']));
		  $dataBanks = $q->fetch(PDO::FETCH_ASSOC);						   						 
		 
		  
		  $sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['bankname'],$data['branchcode']));
		  $dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);			
		  
		  // $data['branchcode'] = $dataBankBranches['branchdesc'];
		  $data['bankname'] = $dataBanks['bankname'];
		  
		//2. ---------------------- BOC - Provinces ------------------------------- //
		// 15.04.2017 - 
		  $sql = 'SELECT * FROM province WHERE provinceid = ?';
		  $q = $pdo->prepare($sql);
		  $q->execute(array($data['State']));
		  $dataProvince = $q->fetch(PDO::FETCH_ASSOC);						   						 
		  $data['State'] = $dataProvince['provincename'];
		  
		//3. ---------------------- BOC - Account Types ------------------------------- //
		// 15.04.2017 - 
		 $sql = 'SELECT * FROM accounttype WHERE accounttypeid = ?';
		 $q = $pdo->prepare($sql);
		 $q->execute(array($data['accounttype']));
		 $dataAccountType = $q->fetch(PDO::FETCH_ASSOC);						   						
		 $data['accounttype'] = $dataAccountType['accounttypedesc']; 	
		  
		// ---------------------- EOC - Account Types ------------------------------- //  

		//4. ---------------------- BOC - Payment Method ------------------------------- //
		// 15.04.2017 - 
		 $sql = 'SELECT * FROM paymentmethod WHERE paymentmethodid = ?';
		 $q = $pdo->prepare($sql);	
		 $q->execute(array($data['paymentmethod']));		 
		 $dataPaymentMethod =$q->fetch(PDO::FETCH_ASSOC);							   						 
		 $data['paymentmethod'] = $dataPaymentMethod['paymentmethoddesc'];
		 
		// ---------------------- EOC - Payment Method ------------------------------- //  

		//5. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 - 
		 $sql = 'SELECT * FROM paymentfrequency WHERE paymentfrequencyid = ?';
		 $q = $pdo->prepare($sql);	
		 $q->execute(array($data['paymentfrequency']));
		 $dataPaymentFrequency = $q->fetch(PDO::FETCH_ASSOC);						   						 
		 $data['paymentfrequency'] = $dataPaymentFrequency['paymentfrequencydesc'];
		 
		// ---------------------- EOC - Payment Frequency ------------------------------- //  

		//6. ---------------------- BOC - Payment Frequency ------------------------------- //
		// 15.04.2017 - 
		 $sql = 'SELECT * FROM loantype WHERE loantypeid = ?';
		 $q = $pdo->prepare($sql);		 
		 $q->execute(array($data['loantypeid']));
		 $dataLoanType = $q->fetch(PDO::FETCH_ASSOC);	
		 $data['loantypeid'] = $dataLoanType['loantypedesc'];		 
		 
		// ---------------------- EOC - Payment Frequency ------------------------------- //  
		// BOC -- 15.04.2017 -- Updated.
		$BankName  = '';
		$BranchCode = '';
		// EOC -- 15.04.2017 -- Updated.	
		// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
		
		Database::disconnect();
		
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;
	}
?>
<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div class="row">
<!-- Login Box -->
<form class="login-page" action="read?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

									<table class=   "table table-user-information">
						<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->		
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>						
</td></div>							
		
</tr>

<tr>						
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label"><strong>Title</strong></label>
		<div class="controls">
		  	<?php echo $data['Title'];?>
		</div>
	</div>
	</td> 

	<td> 
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label"><strong>First Name</strong></label>
		<div class="controls">
			<?php echo $data['FirstName'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label"><strong>Last Name</strong></label>
		<div class="controls">
			<?php echo $data['LastName'];?>
		</div>
	</div>
	</td>
 
</tr>	

<!-- Street,Suburb,City -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label"><strong>Street</strong></label>
		<div class="controls">
		   	<?php echo $data['Street'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label"><strong>Suburb</strong></label>
		<div class="controls">
			<?php echo $data['Suburb'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label"><strong>City</strong></label>
		<div class="controls">
		    <?php echo $data['City'];?>
		</div>
	</div>
	</td>
</tr>	

<!-- State,PostCode,Dob -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label"><strong>Province</strong></label>
		<div class="controls">
			<?php
		// --------------------- BOC 2017.04.15 ----------------------------------------------------------- //
		//   					Update  Details Dynamic Table 												//
		// ------------------------------------------------------------------------------------------------ //
			echo $data['State'];
			?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label"><strong>Postal Code</strong></label>
		<div class="controls">
		  	<?php echo $data['PostCode'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label"><strong>Date of birth</strong></label>
		<div class="controls">
		    <?php echo $data['Dob'];?>
		</div>
	</div>
	</td>
</tr>	
<tr>
<td> 
	<div class="control-group">
		<label class="control-label"><strong>Contact Number</strong></label>
		<div class="controls">
		    <?php echo $data['phone'];?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Email</strong></label>
		<div class="controls">
		    <?php
			foreach(  $dataUserEmail as $rowEmail)
			{
			   echo $rowEmail['email'];
			}	
			?>
		</div>
	</div>
	</td>
</tr>
<!----------------------------------------------------------- Banking Details -------------------------------------------->
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<!-- Account Holder Name,Bank Name,Account number -->	
<tr>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account Holder Name</strong></label>
		<div class="controls">
			<?php echo $data['accountholdername'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Bank Name</strong></label>
		<div class="controls">
			<?php echo $data['bankname'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account number</strong></label>
		<div class="controls">
		    <?php echo $data['accountnumber'];?>
		</div>
	</div>
	</td>
</tr>	
<!-- branch code,Account type -->	
<tr>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Branch Code</strong></label>
		<div class="controls">
			<?php echo $data['branchcode'].' - '.$dataBankBranches['branchdesc'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Account Type</strong></label>
		<div class="controls">
			<?php echo $data['accounttype'];?>
		</div>
	</div>
	</td>
</tr>	

<!----------------------------------------------------------- End Banking Details ---------------------------------------->	

<!----------------------------------------------------------- Loan Details -------------------------------------------->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Loan Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Income</strong></label>
		<div class="controls">
			<?php echo $data['MonthlyIncome'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label"><strong>Monthly Expenditure</strong></label>
		<div class="controls">
			<?php echo $data['MonthlyExpenditure'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label"><strong>Total Assets</strong></label>
		<div class="controls">
		    <?php echo $data['TotalAssets'];?>
		</div>
	</div>
	</td>
</tr>	

<!-- ReqLoadValue,DateAccpt,LoanDuration -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label"><strong>Requested Loan Value</strong></label>
		<div class="controls">
		    <?php echo $data['ReqLoadValue'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label"><strong>Date Accepted</strong></label>
		<div class="controls">
		   <?php echo $data['DateAccpt'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label"><strong>Loan Duration</strong></label>
		<div class="controls">
		  <?php echo $data['LoanDuration'];?>
		</div>
	</div>
	</td>
</tr>	

<!-- InterestRate,ExecApproval -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($InterestRateError)?'error':'';?>">
		<label class="control-label"><strong>Interest Rate(%)</strong></label>
		<div class="controls">
				<?php echo $data['InterestRate'];?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label"><strong>Status</strong></label>
		<div class="controls">
		<?php 
		if($data['ExecApproval'] == 'APPR')
								{
								echo 'Approved';
								}
								if($data['ExecApproval'] == 'PEN')
								{
								echo 'Pending';
								}
								if($data['ExecApproval'] == 'CAN')
								{
								echo 'Cancelled';
								}
								if($data['ExecApproval'] == 'REJ')
								{
								echo 'Rejected';
								}
								if($data['ExecApproval'] == 'SET')
								{
								echo 'Settled';
								}
								// -- Defaulter - 20.06.2018.
								if($data['ExecApproval'] == 'DEF')
								{
								echo 'Defaulter';
								}

		?>
		</div>
	</div>
	</td>
	<td> 
	<div class="control-group">
		<label class="control-label"><strong>Total Repayment Amount</strong></label>
		<div class="controls">
		    <?php echo $data['Repayment'];?>
		</div>
	</div>
	</td>
</tr>	
<tr>
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Payment Method</strong></label>
				<div class="controls">
					<?php echo $data['paymentmethod'];?>
				</div>
		</div>
	</td> 
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Surety</strong></label>
				<div class="controls">
					<?php echo $data['surety'];?>
				</div>
		</div>
	</td> 
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Monthly Payment Amount</strong></label>
				<div class="controls">
					<?php echo $data['monthlypayment'];?>
				</div>
		</div>
	</td> 

</tr>	
<tr>
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>First Payment Date</strong></label>
				<div class="controls">
					<?php echo $data['FirstPaymentDate'];?>
				</div>
		</div>
	</td> 
	<td> 
		<div class="control-group">
				<label class="control-label"><strong>Last Payment Date</strong></label>
				<div class="controls">
					<?php echo $data['lastPaymentDate'];?>
				</div>
		</div>
	</td>  
</tr>	
<?php
$errorClass = "control-group";
$formClass = "form-control margin-bottom-10";
$rejectreason = $data['rejectreason'];
if ($data['ExecApproval'] == 'REJ') 
{ 	
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label><strong>Rejection Reason</strong></label>
		<div class='controls'>$rejectreason</div>
	</div> 
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>						
						<tr>
					  <td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
						  <a class="btn btn-primary" href="custAuthenticate">Back</a>
<!-- 13.06.2018 - Debit Order Schedule -->						
		<span></span>						 
						<!-- <a class="btn btn-primary" href="#">Schedule Debit Orders</a> -->
						 <?php 
						 
						 // -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);
$AccountHolder	 = $data['accountholdername'];
$BranchCode	 = $data['branchcode'];
$AccountNumber	 =  $data['accountnumber'];	
$Servicetype	 = 'Debit Order';
$ServiceMode	 = 'TWO DAY';
$AccountType	 = $dataAccountType['accounttypeid'];//$data['accounttype'];
$fname 			 = $data['FirstName'];
$lname 			 = $data['LastName'];
$frequency 		 = $data['paymentfrequency'];

$href = "'"."href=debitorderschedule.php?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded&ReferenceNumber=$ReferenceNumber&AccountHolder=$AccountHolder&BranchCode=$BranchCode&AccountNumber=$AccountNumber&Servicetype=$Servicetype&ServiceMode=$ServiceMode&AccountType=$AccountType&frequency=$frequency&fname=$fname&lname=$lname"."'";
						 echo '<button type="button" class="btn btn-primary" data-popup-open="popup-1"  onclick="debitorderschedule('.$href.')">Generate Payments</button>';
						 ?>						
						<div class="popup" data-popup="popup-1" style="overflow-y:auto;">
							<div class="popup-inner">
								<!-- <p><b>Customer Loan Payment Schedule - Report</b></p>
								<div id="myProgress">
									<div id="myBar">10%</div>
									
								</div> 
								</br> -->
											<div id="divdebitorderschedule" style="top-margin:100px;font-size: 10px;">
											<h1>Debit Order Schedules</h1>
											</div>
							<!-- <div id="divSuccess">
								
								</div> -->
								<p><a data-popup-close="popup-1" href="#">Close</a></p>
								<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
							</div>
					    </div>	
						</div>
					  </td>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
<!-- 13.06.2018 - Debit Order Schedule -->						
	<?php 
	$ApplicationId = $data['ApplicationId'];
	$customerid = $data['CustomerId'];
	
	if(isset($_SESSION))
	{
		$role = $_SESSION['role'];
	}
	
	// -- if no role mean user never logged in go back to login page.
	if (empty($role)) 
	{
			header("Location: customerLogin");
	}
	
	// -- end of login check.
	
	// Only show contract if eloan application has been approved.
	if($data['ExecApproval'] == 'APPR' && $role == 'customer')
	{
				$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded>Download Contract</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.
	} 
	
	// Show if the role is administrator
	if($role == 'admin')
	{
		$hash="&guid=".md5(date("h:i:sa"));
		// -- BOC Encode Change - 16.09.2017.
	  echo "<td><div>
		<a class='btn btn-primary' href=contract?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Download Contract</a></div></td>";
		// -- EOC Encode Change - 16.09.2017.

	}
	
	?>
						
						<td><div>
						  <!-- <button type="submit" class="btn btn-success">Update</button> -->
	<?php 
	$ApplicationId = $data['ApplicationId'];
	$customerid = $data['CustomerId'];
	$hash="&guid=".md5(date("h:i:sa"));
	// -- BOC Encode Change - 16.09.2017.
	echo "<a class='btn btn-primary' href=upload?customerid=$CustomerIdEncoded&ApplicationId=$ApplicationIdEncoded$hash>Upload Documents</a>";
	// -- EOC Encode Change - 16.09.2017.
	?>					  
						<!--  <a class="btn btn-primary" href="upload.php">Upload Documents</a> -->
						</div></td>
						</tr>
						</table>
									</div>
<!-------------------------------------- UPLOADED DOCUMENTS ---------------------------------------->
<!------------------------------ File List ------------------------------------>
<table class = "table table-hover">
   <caption>
   <h3><b>Uploaded Documents</b></h3></caption>
   
   <thead>
      <tr style ="background-color: #f0f0f0">
         <th>Document Name</th>
         <th>Type</th>
      </tr>
   </thead>
<!----------------- Read AppLoans document names ----------------------------->
 <?php 
 
    // $pdo = Database::connectDB();
	 $tbl_name="loanapp"; // Table name 
	 //$check_user = "SELECT * FROM $tbl_name WHERE customerid='$customerid' and ApplicationId='$ApplicationId'";
	 $check_user = "SELECT * FROM $tbl_name WHERE customerid= ? and ApplicationId=?";
	 
	 // -- ID Document
	 $iddocument = '';
	 
	 // -- Contract Agreement
	 $contract = '';
	 
	 // -- Bank statement
	 $bankstatement = '';
	 
	 // -- Proof of income
	 $proofofincome = '';
	 
	 // -- Fica
	 $fica = '';
	 
	 $consentform = '';
	 $other = '';
   	 $other2 = '';	
	 // -- 
	 
	 //mysql_select_db($dbname,$pdo)  or die(mysql_error());
	 //$result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	//$count = mysql_num_rows($result);
	
	
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  $check_user;
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$row = $q->fetch(PDO::FETCH_ASSOC);
		$count = $q->rowCount();
	
	if($count >= 1)
    {  

	//$row = mysql_fetch_array($result);
	
	
	$iddocument  =  $row['FILEIDDOC']; 
	$contract = $row['FILECONTRACT'];
	$bankstatement = $row['FILEBANKSTATEMENT'];
	$proofofincome = $row['FILEPROOFEMP'];
	$fica = $row['FILEFICA'];
	
   // -- BOC 01.10.2017 - Upload Credit Reports.
   	$creditreport = $row['FILECREDITREP'];	
	
	$consentform = $row['FILECONSENTFORM'];
	$other = $row['FILEOTHER'];
   	$other2 = $row['FILEOTHER2'];	
	$NoFIle = "";
   // -- EOC 01.10.2017 - Upload Credit Reports.
   
   // -- BOC 01.10.2017 - Alternative if no file upload.
   if(empty($iddocument)){$iddocument = $NoFIle;}
   if(empty($contract)){ $contract = $NoFIle;}
   if(empty($bankstatement)){$bankstatement = $NoFIle;}
   if(empty($proofofincome)){$proofofincome = $NoFIle;}
   if(empty($fica)){ $fica = $NoFIle;}
   if(empty($creditreport)){  $creditreport = $NoFIle;}
if(empty($consentform)){  $consentform = $NoFIle;}
   if(empty($other)){  $other = $NoFIle;}
   if(empty($other2)){  $other2 = $NoFIle;}

   
   // -- EOC 01.10.2017 - Alternative if no file upload.
    $ViewDocument = 'View Document';
	$classCSS = "class='btn btn-warning'";
	
	$iddocumentHTML = '';
	if($iddocument == '')
	{
		$iddocumentHTML = "<tr>



         <td><a href='uploads/$iddocument' target='_blank'>$iddocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	else
	{
		$iddocumentHTML = "<tr>
         <td><a $classCSS href='uploads/$iddocument' target='_blank'>$ViewDocument</a></td>
         <td>ID Document</td>
		</tr>";
	}
	$contractHTML = '';
	if($contract == '')
	{
		$contractHTML = "<tr>


         <td><a href='uploads/$contract' target='_blank'>$contract</a></td>
         <td>Contract Agreement</td>
      </tr>
      ";
	}
	else
	{
		$contractHTML = "

      <tr>
         <td><a $classCSS href='uploads/$contract' target='_blank'>$ViewDocument</a></td>
         <td>Contract Agreement</td>
      </tr>";
	}
	
	$bankstatementHTML = '';
	if($bankstatement == '')
	{
		$bankstatementHTML = "<tr>
         <td><a href='uploads/$bankstatement' target='_blank'>$bankstatement</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	else
	{
		$bankstatementHTML = "<tr>
         <td><a $classCSS href='uploads/$bankstatement' target='_blank'>$ViewDocument</a></td>
         <td>Current Bank statement</td>
      </tr>";
	}
	
	$proofofincomeHTML = '';
	if($proofofincome == '')
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a href='uploads/$proofofincome' target='_blank'>$proofofincome</a></td>
         <td>Proof of income</td>
      </tr>
	  ";
	}
	else
	{
		$proofofincomeHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$proofofincome' target='_blank'>$ViewDocument</a></td>

         <td>Proof of income</td>
      </tr>
	  ";
	}
	$ficaHTML = '';
	if($fica == '')
	{
		$ficaHTML = "

	  <tr>
         <td><a href='uploads/$fica' target='_blank'>$fica</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	else
	{
		$ficaHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$fica' target='_blank'>$ViewDocument</a></td>
         <td>Proof of residence</td>
      </tr>
	  ";
	}
	$creditreportHTML = '';
	if($creditreport == '')
	{
		$creditreportHTML = "
	  <tr>
         <td><a href='uploads/$creditreport' target='_blank'>$creditreport</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	else
	{
		$creditreportHTML = "
	  <tr>
         <td><a $classCSS href='uploads/$creditreport' target='_blank'>$ViewDocument</a></td>
         <td>Credit Report</td>
      </tr>
	  ";
	}
	$consentformHTML = '';
	if($consentform == '')
	{
		$consentformHTML = "
	  <tr>
         <td><a href='uploads/$consentform' target='_blank'>$consentform</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	else
	{
		$consentformHTML = "
	  <tr>
         <td><a  $classCSS href='uploads/$consentform' target='_blank'>$ViewDocument</a></td>
         <td>Credit Consent Form</td>
      </tr>";
	}
	$otherHTML = '';
	if($other == '')
	{
		$otherHTML ="
	  <tr>
         <td><a href='uploads/$other' target='_blank'>$other</a></td>
         <td>Other</td>
      </tr>
	";
	}
	else
	{
		$otherHTML ="
	  <tr>
         <td><a $classCSS href='uploads/$other' target='_blank'>$ViewDocument</a></td>
         <td>Other</td>
      </tr>
	";
	}
	$other2HTML = '';
	if($other2 == '')
	{
		$other2HTML = "
  	  <tr>
         <td><a href='uploads/$other2' target='_blank'>$other2</a></td>
         <td>Other 2</td>
      </tr>
";
	}
	else
	{
		$other2HTML = "
  	  <tr>
         <td><a $classCSS href='uploads/$other2' target='_blank'>$ViewDocument</a></td>
         <td>Other 2</td>
      </tr>";
	}
	
   echo "<tbody>"
      .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$consentformHTML.$creditreportHTML.$otherHTML.$other2HTML."
   </tbody>";
   } ?>
<!------------------ End AppLoans documents ----------------------------------->   
</table>								
 <!------------------------------ End File List ------------------------------------>
	
<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------> 	
					</form>			
						
				
                            <!-- End Login Box -->							
                        </div>
					
                </div>
            <!-- === END CONTENT === -->