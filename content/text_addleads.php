   	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

   	<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>-->

<?php
$service 		 = '';
$count 	 		 = '';
$nature  		 = '';
$debitordersfor	 = '';
$history		 = '';
$volume			 = '';
$address		 = '';
$message		 = '';
$im_key_first_name= '';
$im_key_surname= '';
$im_key_cellphone= '';
$im_key_email= '';
$im_key_company= '';
$im_servicetitle= '';
$backHTML = "display:none";



if(isset($_GET['service']))
{
	$service = $_GET['service'];
}

if(!empty($_POST))
{
	$service = $_POST['service'];
	$count = 1;
}
?>
			<!-- BOC - PopUp for Trial -->
						<div class='container background-white bottom-border'>
                    <div class="row margin-vert-30">

					 <!--<div class="popup" data-popup="popup-1" style="overflow-y:auto;z-index: 2;">
						<div class="popup-inner">-->

										<div id="divContent" style="top-margin:100px;font-size: 10px;">
<!-- BOC Form to enable capuring of Data
<div class='container background-white bottom-border'>
<div class='row margin-vert-30'>-->
	<div class="col-xs-12 col-sm-12">
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
				<?php # error messages
					if (isset($message))
					{
						//foreach ($message as $msg)
						{
							//printf("<p class="p1"  class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='p1' style='color:green'  class='status'>Thank you for reaching out! We have successfully received your request $message, and our team will be in touch with you shortly.</p>");
					}
			   ?>


<?PHP if($service=="DebiCheck"){ ?>
				<!--DebiCheck-->
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="DebiCheck"){echo 'Background';}?></h2>
				<p class="p1"  ><?php if($service=="DebiCheck"){echo "Businesses and Customers agree that debit orders are a convenient way to pay their accounts, as it saves them time and having to remember to make payments, thereby giving them peace of mind."; echo "<br><br>"; echo "However, on a monthly basis the banks collectively process about 31 million debit orders, 120 000 of which are disputed. Over the past number of years, debit order abuse has become a major issue in South Africa. There has been bad behaviour by some companies that process invalid debit orders to consumer bank accounts. In addition, there are consumers that avoid paying valid debit orders by unfairly disputing these with their banks. As a result, the Reserve Bank has asked PASA (Payments Association of South Africa), to find a solution.";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="DebiCheck"){echo 'What is DebiCheck?';}?></h2>
				<p class="p1"  ><?php if($service=="DebiCheck"){echo "DebiChecks are new debit orders electronically confirmed by your customers with the bank on a once-off basis, relating to a new contract that you have signed with your customer. This mean changes to the interactions you have with your customer. No worries the eCashMeUp platform has simplified the on-boarding process for you. Using DebiCheck your clients bank will now know the details of what you have agreed to and will not allow your DebiCheck to be processed outside the terms that you have confirmed. Sign up for DebiCheck debit orders with eCashMeUp and be in control.";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="DebiCheck"){echo 'Our Professional Service includes:';}?></h2>
				<p class="p1"  ><?php if($service=="DebiCheck"){echo "<ul class='p1'>";echo "<li>Training to get your company up and running.</li>";echo "<li>Monday to Sunday support when needed</li>";echo "<li>Debit order transaction monitoring</li>";echo "<li>Customer in the loop to keep your customers informed</li>";echo "<li>Data-Driven collections</li>";echo "<li>Pay-collection Analytics</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>On-going enablement</li>";echo "<li>Quarterly business review</li>";echo "<li>Weekly status calls </li>"; echo "</ul>";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="DebiCheck"){echo 'List of banks that allow DebiCheck transactions:';}?></h2>
				<p class="p1"  ><?php if($service=="DebiCheck"){echo "<ul class='p1'>";echo "<li>ABSA</li>";echo "<li>First National Bank</li>";echo "<li>Nedbank</li>";echo "<li>Capitec</li>";echo "<li>Standard bank</li>";echo "<li>Bidvest bank</li>";echo "<li>Ubank</li>";echo "<li>Finbond Mutual Bank</li>"; echo "</ul>";}?></p>

		<?PHP } ?>

		<?PHP if($service=="NAEDO"){ ?>

				<!--NAEDO-->
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="NAEDO"){echo 'Background';}?></h2>
				<p class="p1"  ><?php if($service=="NAEDO"){echo "NAEDO (Non-Authenticated Early Debit Order) is a debit order collection service that allows a company to track clients bank accounts. The debit order will take place when there are sufficient funds on the clients account.  The NAEDO service will give your company the ability to track your clients bank accounts over a period of time and this range from 1 day to 32 days";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="NAEDO"){echo 'Some of eCashMeUp NAEDO Service Key Features Include:';}?></h2>
				<p class="p1"  ><?php if($service=="NAEDO"){echo "<ul class='p1'>";echo "<li>Bulk or individual debit orders on-boarding</li>";echo "<li>Tracking up to 32 days </li>";echo "<li>NAEDO Data-Driven collections</li>";echo "<li>NAEDO collection Analytics</li>";echo "<li>Automated changing of action dates and dynamic tracking</li>";echo "<li>Automated Customer in the loop to keep your Customers informed</li>";echo "<li>Automated Transaction Monitoring</li>";echo "<li>Analytics</li>";echo "<li>Simplified and automated debit order on-boarding </li>";echo "<li>Systems Integration option (e.g SAP ERP, Oracle ERP, and CRM applications)</li>"; echo "</ul>";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="NAEDO"){echo 'NAEDO Service Coverage';}?></h2>
				<p class="p1"  ><?php if($service=="NAEDO"){echo "The NAEDO service covers all types of accounts held by participating banks. A list of participating banks can be obtained on request.";}?></p>
				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="NAEDO"){echo 'Our Professional Service Includes:';}?></h2>
				<p class="p1"  ><?php if($service=="NAEDO"){echo "<ul class='p1'>";echo "<li>Training to get your company up and running.</li>";echo "<li>Monday to Sunday support when needed</li>";echo "<li>Debit order transaction monitoring</li>";echo "<li>Customer in the loop to keep your customers informed</li>";echo "<li>Data-Driven collections</li>";echo "<li>Pay-collection Analytics</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>On-going enablement</li>";echo "<li>Quarterly business review</li>";echo "<li>Weekly status calls </li>"; echo "</ul>";}?></p>

			<?PHP } ?>


			<!-- EFT  -->
			<?PHP if($service=="Debit"){ ?>
			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Debit"){echo 'Background';}?></h2>
			<p class="p1"  ><?php if($service=="Debit"){echo "The EFT  service is provided by participating Banks, which enables companies and other bodies to process payments entirely by computerised means. The service can be used both for collecting payments from clients (debits) and for making payments (credits). Payments can be made for salaries, creditors wages etc. The user can lodge payments and collections through their bank to all participating banks.";}?></p>
			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Debit"){echo 'eCashMeUp EFT Service Key Features Include:';}?></h2>

			<p class="p1"  ><?php if($service=="Debit"){echo "<ul class='p1'>";echo "<li>Bulk or individual debit orders on-boarding</li>";echo "<li>EFT Data-Driven collections</li>";echo "<li>EFT collection Analytics</li>";echo "<li>Automated changing of action dates and dynamic tracking</li>";echo "<li>Automated Customer in the loop to keep your Customers informed</li>";echo "<li>Automated Transaction Monitoring</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>Simplified and Automated Debit Order on-boarding </li>";echo "</ul>";}?></p>
			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Debit"){echo 'EFT Service Coverage';}?></h2>
			<p class="p1"  ><?php if($service=="Debit"){echo "The EFT service covers all types of accounts held by participating banks. A list of participating banks can be obtained on request.";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Debit"){echo 'Processing Times:';}?></h2>

			<p class="p1"  ><?php if($service=="Debit"){echo "eCashMeUp will accept data Mondays to Fridays excluding public holidays. The cut-off times below relates to Debit Orders submitted to eCashMeUp (any data submitted after these times will incur a penalty).  Debit Orders will only be processed on Saturdays if a prior arrangement has been made: ";}?></p>


			<p class="p1"  ><?php if($service=="Debit"){echo "<ul class='p1'>";echo "<li>Two Day Service:- Debit Orders must be submitted before 11 am at least 2 days prior to the debit order collection date (i.e The debit orders must reach eCashMeUp 30 minutes before cut-off on Wednesday if required action date is Friday). </li>";echo "<li>One Day Service:- Debit Orders must be submitted before 11:00 am on the day prior to action date (i.e The debit order must reach eCashMeUp before cut-off on Wednesday if the required action date is Thursday. </li>";echo "<li>Same Day Service:- Debit Orders must be submitted before 12:00 am on action date.</li>";echo "</ul>";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Debit"){echo 'Our Professional Service Includes:';}?></h2>
			<p class="p1"  ><?php if($service=="Debit"){echo "<ul class='p1'>";echo "<li>Training to get your company up and running.</li>";echo "<li>Monday to Sunday support when needed</li>";echo "<li>Debit order transaction monitoring</li>";echo "<li>Customer in the loop to keep your customers informed</li>";echo "<li>Data-Driven collections</li>";echo "<li>Pay-collection Analytics</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>On-going enablement</li>";echo "<li>Quarterly business review</li>";echo "<li>Weekly status calls </li>"; echo "</ul>";}?></p>


			<?PHP } ?>

			<!-- Cash-Pay  -->
			<?PHP if($service=="Cash-Pay"){ ?>
			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Cash-Pay"){echo 'Welcome to the Future of payments.';}?></h2>

			<p class="p1"  ><?php if($service=="Cash-Pay"){echo "Car keys, Wallets, and Smartphone are the three essentials that people carry with them in this day and age. Your smartphone can make voice calls, connect to the internet and do other intelligence tasks. eCashMeUp has added more simplicity into your life by Introducing eCashMeUp Pay, the solution to enable you making secure, instant payments to participating eCashMeUp Pay merchants using your Airtime Wallet. </br></br> This product will be in public preview mid-2020,  and the eCashMeUp team is very excited to be launching this product to SA users. A list of participating merchants, Telecommunication providers, supported devices to be shared closer to the product launch.";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Cash-Pay"){echo 'Our Professional Service includes:';}?></h2>
			<p class="p1"  ><?php if($service=="Cash-Pay"){echo "<ul class='p1'>";echo "<li>Training to get your company up and running.</li>";echo "<li>Monday to Sunday support when needed</li>";echo "<li>Debit order transaction monitoring</li>";echo "<li>Customer in the loop to keep your customers informed</li>";echo "<li>Data-Driven collections</li>";echo "<li>Pay-collection Analytics</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>On-going enablement</li>";echo "<li>Quarterly business review</li>";echo "<li>Weekly status calls </li>"; echo "</ul>";}?></p>

			<?PHP } ?>

			<!-- Account Verification (AVS)  -->
			<?PHP if($service=="Account Verification Service (AVS)"){ ?>
			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Account Verification Service (AVS)"){echo 'Bank Account Verification Service (AVS)';}?></h2>

			<p class="p1"  ><?php if($service=="Account Verification Service (AVS)"){echo "Bank Account Verification Service (AVS) enables your company to verify your Customers bank account details are correct prior to the submission of a transaction.  </br></br> Customer bank account verification is a critical part of risk assessment for any company that electronically collect or make payments.  Understanding a prospects banking status and history can help your business or you as a business owner to reduce the risk of transaction rejections or making a payment to an incorrect account.  The AccountVerification Service (AVS) can also provide insights to customers accounts that may be delinquent";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Account Verification Service (AVS)"){echo 'Our Professional Service includes:';}?></h2>
			<p class="p1"  ><?php if($service=="Account Verification Service (AVS)"){echo "<ul class='p1'>";echo "<li>Training to get your company up and running.</li>";echo "<li>Monday to Sunday support when needed</li>";echo "<li>Debit order transaction monitoring</li>";echo "<li>Customer in the loop to keep your customers informed</li>";echo "<li>Data-Driven collections</li>";echo "<li>Pay-collection Analytics</li>";echo "<li>Systems Integration (e.g SAP ERP, Oracle ERP, and CRM applications)</li>";echo "<li>On-going enablement</li>";echo "<li>Quarterly business review</li>";echo "<li>Weekly status calls </li>"; echo "</ul>";}?></p>


			<?PHP } ?>




		<!-- Electronic Signatures  -->
			<?PHP if($service=="Imani Rest Assured"){ ?>

		   <h2 align="center" class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Imani Rest Assured"){echo 'Imani Rest Assured – The Complete Solution for Funeral Parlours';}?></h2>
		   <p class="p1"  ><?php if($service=="Imani Rest Assured"){echo "At eCashMeUp, we understand the sensitive nature of the funeral industry and the need for efficiency, compassion, and reliability. That’s why we’ve developed a powerful, all-in-one platform designed specifically to meet the operational needs of funeral parlours. we know that funeral parlours are not just businesses—they are the cornerstone of support during life’s most difficult moments. Families rely on you to guide them with care, empathy, and professionalism, and in turn, you need a reliable, efficient system that supports your operations seamlessly.";}?></p><br>
		   <p class="p1"  ><?php if($service=="Imani Rest Assured"){echo "That’s why we created Imani Rest Assured, a comprehensive platform designed specifically for funeral parlours. Whether you’re managing member onboarding, tracking payments, or communicating with families, our all-in-one solution empowers you to focus on what truly matters: delivering compassionate, high-quality service to your clients when they need it most.";}?></p><br>
		   <p class="p1"  ><?php if($service=="Imani Rest Assured"){echo "We understand the challenges you face—juggling multiple tasks, managing sensitive information, keeping up with regulations, and ensuring financial stability. With Imani Rest Assured, we bring all your critical operations under one roof, allowing you to work smarter, not harder.";}?></p><br>
		   <p class="p1"  ><?php if($service=="Imani Rest Assured"){echo "<h4>What Sets Imani Rest Assured Apart?</h4>";}?></p><br>
		   <p class="p1"  ><?php if($service=="Imani Rest Assured")
		   {echo "<ul class='p1'>";
			echo "<li>Tailored for Funeral Parlours: Unlike generic business solutions, Imani Rest Assured is built with the specific needs of funeral parlours in mind. Every feature has been carefully crafted to ensure it helps your business function efficiently while maintaining a compassionate touch.</li><br>";
			echo "<li>Designed for Simplicity and Efficiency: Our intuitive interface makes it easy to navigate complex tasks. From policy management to payment processing, everything is designed to reduce the time spent on administrative work so that you can focus on serving families.</li><br>";
			echo "<li>Empowering Growth and Excellence: With real-time insights, automated notifications, and powerful financial tools, Imani Rest Assured doesn’t just help you run your business—it helps you grow it. Whether you’re a small parlour or a growing business, we give you the tools you need to thrive.</li><br>";
		   echo "</ul>";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php if($service=="Electronic Signatures"){echo 'eSignature Legality Summary in South Africa';}?></h2>

			<p class="p1"  ><?php if($service=="Electronic Signatures"){echo "Electronic signatures has been recognized by law in South Africa since 202, with the passage of The Electronic Communications and Tranactions Act. Under South African law, a written signature is not necessarliy required for a valid contract- contracts are generally valid i legally competent parties reach an agreement, whether they agree verbally, electronically or in a physical paper document. Section 13(2) of The Electronic Communications and Transactions Act (ECTA) specifically confirms that contracts cannot be denied enforceability merely because they are concluded electronically or through data messages. To prove a valid contract, parties sometimes have to prevent evidence in court.  Leading digital tranaction management solutions can provide electronic records that are admissable in evidenece under section 15 of ECTA, to support the existence, authenticity and valid acceptance of a contract.";}?></p>


			<?PHP } ?>


		<h4 class="page-header" id="servicetitle" name="servicetitle" >Contact Us <?php if(!empty($service)){echo 'for '.$service;}?></h4>
		<p class="p1"  ><?php if($service=="Imani Rest Assured"){echo "We’d love to hear from you! Whether you have questions about Imani Rest Assured, would like to schedule a demo, or need further details about our features.<br><br> Please fill out the form below, and a member of our team will get back to you shortly.";}?></p><br>
				
				
		<!-- Register Box id="captcha-form" name="captcha-form" -->
		<div class="col-md-3 col-md-offset-1 col-sm-offset-1">			
			 <div class="form-group">
				 <label for="form_key_Full_Names">Name<span style="color:red">*</span></label>
				 <input style="height:45px" id="im_key_first_name" type="text" name="im_key_first_name" class="form-control" placeholder="" required="required" maxlength="30" value = "<?php echo $im_key_first_name;?>">
				 <div class="help-block with-errors"></div>
				 <span style="color:red" id="im_key_first_name-error" class="error-message"></span>
			 </div>
			<br>
			 <div class="form-group">
					 <label for="form_key_surname">Surname<span style="color:red">*</span></label>
					 <input style="height:45px" id="im_key_surname" type="text" name="im_key_surname" class="form-control" placeholder="" required="required"  maxlength="30"  value = "<?php echo $im_key_surname;?>">
					 <div class="help-block with-errors"></div>
					 <span  style="color:red" id="im_key_surname-error" class="error-message"></span>
			 </div>
			
			<br>	
			 <div class="form-group">
					 <label for="form_Contact_Number">Contact number<span style="color:red">*</span></label>
					 <input style="height:45px" id="im_key_cellphone" type="number" name="im_key_cellphone" class="form-control" placeholder="" required="required" maxlength="10" value = "<?php echo $im_key_cellphone;?>">
					 <div class="help-block with-errors"></div>
					  <span  style="color:red" id="im_key_cellphone-error" class="error-message"></span>
			 </div>
			<br>		
			 <div class="form-group">
				<label for="form_Email_Address">Email address<span style="color:red">*</span></label>
				<input style="height:45px" id="im_key_email" type="text" name="im_key_email" class="form-control" placeholder="" required="required"   maxlength="30" value = "<?php echo $im_key_email;?>">
				<div class="help-block with-errors"></div>
			    <span style="color:red" id="im_key_email-error" class="error-message"></span>
			 </div>
				<br>	
			 <div class="form-group">
				<label for="form_Company_Name">Company name<span style="color:red">*</span></label>
				<input style="height:45px" id="im_key_company" type="text" name="im_key_company" class="form-control" placeholder="" required="required" maxlength="30"  value = "<?php echo $im_key_company;?>">
				<div class="help-block with-errors"></div>
			    <span  style="color:red" id="im_key_company-error" class="error-message"></span>
				
			 </div>
			 
			 <div class="control-group">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			</div>
			 
			</br>
			</br>
			
		
			<div class="form-group">
			<button style="height:45px" type="submit" class="btn btn-primary btn-lg btn-block" id="Submit" name="Submit" onclick="validate_save()" >Submit</button>
			</div>
	

</div>

				
				
				
			<!--		
				
				
				
				<div id="addressError" class="form-group">
					<label class="p1" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-4">
						<input class="form-control margin-bottom-10" id="address" name="address" type="text"  placeholder="company physical address" value="">
					</div>
					<div class="col-sm-4">
						<small id="addressErrorText" class="help-block" style=""></small>
					</div>
				</div>

				<div id="natureError" class="form-group">
						<label class="p1" class="col-sm-2 control-label">Email Address</label>
						<div class="col-sm-4">
							<textarea placeholder="What is nature of your business?" class="form-control margin-bottom-10" id="nature" name="nature"></textarea>
						</div>

						<div class="col-sm-4">
							<small id="natureErrorText" class="help-block" style=""></small>
						</div>
				</div>
				<br/>
				<div id="companyError" class="form-group">
						<label class="p1" class="col-sm-2 control-label">Phone Number</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="customerid" name="customerid" type="text"  placeholder="registration number" value="">
						</div>
						<div class="col-sm-4">
							<small id="customeridErrorText" class="help-block" style=""></small>
						</div>
				</div>

				<div id="FirstNameError" class="form-group">
						<label class="p1" class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="FirstName" name="FirstName" type="text"  placeholder="Company name" value="">
						</div>
						<div class="col-sm-4">
							<small id="FirstNameErrorText" class="help-block" style=""></small>
						</div>
				</div>
				<div id="LastNameError" class="form-group">
						<label class="p1" class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="LastName" name="LastName" type="text"  placeholder="contact person" value="">
						</div>
						<div class="col-sm-4">
							<small id="LastNameErrorText" class="help-block" style=""></small>
						</div>
				</div>
				
				
				
				
				

			 <h2 class="page-header">Contact Details</h2>
				<div id="phoneError" class="form-group">
						<label class="p1" class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" id="Lphone" name="Lphone" type="text"  placeholder="phone number" value="">
							</div>
							<div class="col-sm-4">
								<small id="phoneErrorText" class="help-block" style=""></small>
							</div>
				</div>
				<div id="emailError" class="form-group">
					<label class="p1" class="col-sm-2 control-label">Email Address</label>
					 <div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="">
					 </div>
					 <div class="col-sm-4">
							<small id="emailErrorText" class="help-block" style=""></small>
					 </div>
				</div>
			 <h2 class="page-header">Additional Information</h2>
				<div id="debitordersforError" class="form-group">
					<label class="p1" class="col-sm-2 control-label">What will you require the service for?</label>
					 <div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="debitordersfor" name="debitordersfor" type="text"  placeholder="What will you require the service for?" value="">
					 </div>
					 <div class="col-sm-4">
							<small id="debitordersforErrorText" class="help-block" style=""></small>
					 </div>
				</div>
				<div id="historyError" class="form-group">
					<label class="p1" class="col-sm-2 control-label">Do you have processing history?</label>
					 <div class="col-sm-4">
					 <SELECT class="form-control margin-bottom-10" id="history" name="history" size="1">
					 	<option value="No">No</option>
						<option value="Yes">Yes</option>
					 </select>
					 </div>
					 <div class="col-sm-4">
							<small id="historyErrorText" class="help-block" style=""></small>
					 </div>
				</div>
				<div id="volumeError" class="form-group">
					<label class="p1" class="col-sm-2 control-label">What is your transactional volume?</label>
					 <div class="col-sm-4">
					 <SELECT class="form-control margin-bottom-10" id="volume" name="volume" size="1">
					 	<option value="0-100">1 - 100</option>
						<option value="101-200">101 - 200</option>
						<option value="201-300">201 - 300</option>
						<option value="301-400">301 - 200</option>
						<option value="401-500">401 - 200</option>
						<option value="500-More">500 - More</option>
					 </select>

					 </div>
					 <div class="col-sm-4">
							<small id="volumeErrorText" class="help-block" style=""></small>
					 </div>
				</div> -->

				</form>
			<!-- </div> 298 -->
		</div>
	<!--</div>
</div>-->
<!-- EOC Capuring data... -->
		</div>
<!--<p class="p1" ><a data-popup-close="popup-1" href="#">Close</a></p>
<a class="popup-close" data-popup-close="popup-1" href="#">x</a>-->
</div>
</div>
<!--<input class="form-control margin-bottom-10" id="service" name="service" type="hidden"  placeholder="service" value=""/>-->

<script>
/* jQuery(document).ready(function()
{
			jQuery('#btn_submit').click(function(e)
			{ 	e.preventDefault();
				validate_save();
			});
});
 */
/*function content(service)
{
		document.getElementById("service").value = service.id;
}
*/
function content(service)
{
	//alert(service.id);
	document.getElementById("service").value = service.id;
	document.getElementById("servicetitle").innerHTML = 'Company Information Details for '+service.id;
}

function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}


 // -- Enter to capture comments   ---
 function validate_save()
 {
		
		var im_key_first_name= document.getElementById('im_key_first_name').value;
		var im_key_surname= document.getElementById('im_key_surname').value;
		var im_key_cellphone= document.getElementById('im_key_cellphone').value;
		var im_key_email= document.getElementById('im_key_email').value;
		var im_key_company= document.getElementById('im_key_company').value;
	 	var im_servicetitle = <?php echo "'".$service ."'"; ?>;
		//alert(im_key_first_name);
		//alert(im_key_surname);
		//alert(im_key_cellphone);
		//alert(im_key_email);
		//alert(im_servicetitle);
		
		//check
		if(im_key_first_name == '')
		{
		 document.getElementById('im_key_first_name-error').innerHTML = 'Name is required';
		valid = false;
		}
		else
		{
			valid = true;
		}
		if(im_key_surname == '')
		{
		 document.getElementById('im_key_surname-error').innerHTML = 'Surname is required';
		valid = false;
		}
		else
		{
			valid = true;
		}
		if(im_key_cellphone == '')
		{
		 document.getElementById('im_key_cellphone-error').innerHTML = 'Phone number is required';
		valid = false;
		}
		else
		{
			valid = true;
		}
		if(im_key_email == '')
		{
		 document.getElementById('im_key_email-error').innerHTML = 'Email address is required';
		 valid = false;
		}
		if(im_key_company == '')
		{
		 document.getElementById('im_key_company-error').innerHTML = 'Company name is required';
		 valid = false;
		}
		else
		{
			valid = true;
		}
/* 		customerid   = document.getElementById('customerid').value;
		FirstName	  = document.getElementById('FirstName').value;
		LastName	  = document.getElementById('LastName').value;
		Lphone		  = document.getElementById('Lphone').value;
		email		  = document.getElementById('email').value;
		service	  = document.getElementById('service').value;

		history1 	  	= document.getElementById('history').value;
		nature       	= document.getElementById('nature').value;
		address	  	= document.getElementById('address').value;
		
		volume			= document.getElementById('volume').value;
 */		 		// alert(history1);//nature+''+address+''+debitordersfor+''+volume);
		

	/* 	if(im_key_first_name != '' && im_key_surname != '' && im_key_cellphone != ''
		&& im_key_email != '' && im_key_company != '')
		{

			if(phonenumber(Lphone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number';
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

    //alert("You have entered an invalid email address!")
			if(ValidateEmail(email) != true)
			{
				document.getElementById("emailError").setAttribute("class", "form-group has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email';
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group");
			 document.getElementById('emailErrorText').innerHTML = '';
			}

			if(customerid != '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = '';
			}

		    // -- Company name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			// -- Company contact name
			if(LastName != '')
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group");
			    document.getElementById('LastNameErrorText').innerHTML = '';
			} */

if(valid)
{		//jQuery(document).ready(function(){
		//alert("I am in");
		jQuery.ajax({  type: "POST",
					   url:"addlead.php",
				       data:{im_key_first_name:im_key_first_name,im_key_surname:im_key_surname,im_key_cellphone:im_key_cellphone,im_key_email:im_key_email,im_key_company:im_key_company,im_servicetitle:im_servicetitle},
					success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

							//alert(feedback);
							message_info.innerHTML = 'We have received your request will be in contact shortly';
							//alert(data);
							//jQuery("#divContent").html(data);
						 },

					error: function(xhr, textStatus, error){
							alert("error: "+xhr.statusText+textStatus+error);
						}
		});
				//});
}
				//data1 = "<p class="p1"  class='status'> Information was successfully submited for approval.</p>";
				//document.getElementById("divContent").innerHTML = data1;

	
		else
		{
			//alert("I am Out");
		/* 	// -- debitordersfor
			if(debitordersfor == '')
			{
				document.getElementById("debitordersforError").setAttribute("class", "form-group has-error");
				document.getElementById('debitordersforErrorText').innerHTML = 'Please enter the reasons for requesting the service';
			}
			else
			{
				document.getElementById("debitordersforError").setAttribute("class", "form-group");
				document.getElementById('debitordersforErrorText').innerHTML = '';
		    }

			// -- nature
			if(nature == '')
			{
				document.getElementById("natureError").setAttribute("class", "form-group has-error");
				document.getElementById('natureErrorText').innerHTML = 'Please enter the nature of your business';
			}
			else
			{
				document.getElementById("natureError").setAttribute("class", "form-group");
				document.getElementById('natureErrorText').innerHTML = '';
		    }

			// -- address
			if(address == '')
			{
				document.getElementById("addressError").setAttribute("class", "form-group has-error");
				document.getElementById('addressErrorText').innerHTML = 'Please enter the company physical address';
			}
			else
			{
				document.getElementById("addressError").setAttribute("class", "form-group");
				document.getElementById('addressErrorText').innerHTML = '';
		    }

			// -- CustomerId
			if(customerid == '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group has-error");
				document.getElementById('customeridErrorText').innerHTML = 'Please enter company registration';
			}
			else
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = '';
		    }

		    // -- Company name
			if(FirstName == '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('FirstNameErrorText').innerHTML = 'Please enter company name';
			}
			else
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			// -- Company contact name
			if(LastName == '')
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('LastNameErrorText').innerHTML = 'Please enter contact person name';
			}
			else
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group");
			    document.getElementById('LastNameErrorText').innerHTML = '';
			}

			// -- phone
			if(Lphone == '')
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error");
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter phone';
			}
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

			// -- email
			if(email == '')
			{	document.getElementById("emailError").setAttribute("class", "form-group has-error");
			 	document.getElementById('emailErrorText').innerHTML = 'Please enter email';
			}
			else
			{
				document.getElementById("emailError").setAttribute("class", "form-group");
				document.getElementById('emailErrorText').innerHTML = '';
			} */
		}
			  return false;
  }

function phonenumber(inputtxt)
{
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail)
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

</script>
