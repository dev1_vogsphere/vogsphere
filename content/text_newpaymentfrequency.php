<?php 
require 'database.php';
$paymentfrequencydescError = null;
$paymentfrequencydesc = '';
$count = 0;
$paymentfrequencyid = 0;

if (!empty($_POST)) 
{   $count = 0;
	$paymentfrequencydesc = $_POST['paymentfrequencydesc'];
	$paymentfrequencyid = $_POST['paymentfrequencyid'];

	$valid = true;
	
	if (empty($paymentfrequencydesc)) { $paymentfrequencydescError = 'Please enter Payment Frequency Description.'; $valid = false;}
	if (empty($paymentfrequencyid)) { $paymentfrequencyidError = 'Please enter Payment Frequency ID.'; $valid = false;}
	
	if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO paymentfrequency (paymentfrequencyid,paymentfrequencydesc) VALUES(?,?)"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($paymentfrequencyid,$paymentfrequencydesc));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border">   
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change Payment Frequency Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Payment Frequency successfully added.</p>");
		}
?>
				
</div>				
		<p><b>Payment Frequency ID</b><br />
			<input style="height:30px" type='text' name='paymentfrequencyid' value="<?php echo !empty($paymentfrequencyid)?$paymentfrequencyid:'';?>"/>
			<?php if (!empty($paymentfrequencyidError)): ?>
			<span class="help-inline"><?php echo $paymentfrequencyidError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Payment Frequency Description</b><br />
			<input style="height:30px" type='text' name='paymentfrequencydesc' value="<?php echo !empty($paymentfrequencydesc)?$paymentfrequencydesc:'';?>"/>
			<?php if (!empty($paymentfrequencydescError)): ?>
			<span class="help-inline"><?php echo $paymentfrequencydescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'paymentfrequency';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
			</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>