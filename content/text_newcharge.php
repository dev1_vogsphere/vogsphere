<?php 
require 'database.php';
$tablename = 'charge';
$chargenameError = null;

$chargename = '';
$chargetypeid = '';
$chargeoptionid = '';
$loantypeid = '';
$paymentmethodid = '';
$active = '';
$percentage = '';
$amount = '';
$min = '';
$max = '';
$graceperiod = ''; 
/* BOC 24.04.2018*/
$fromdate = '';
$todate   = '';						
/* End 24.04.2018*/
$count = 0;
$chargeid = '';

// --------------------------------- BOC 27.05.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.05.15 ------------------------- //
//1. --------------------- Charge Type ------------------- //
  $sql = 'SELECT * FROM chargetype ORDER BY name';
  $dataChargeType = $pdo->query($sql);						   						 
	
//2. --------------------- Charge Option ------------------- //
  $sql = 'SELECT * FROM chargeoption ORDER BY chargeoptionname';
  $dataChargeOption = $pdo->query($sql);						   						 

//3. ---------------------  Loan Type ------------------- //
  $sql = 'SELECT * FROM loantype ORDER BY loantypedesc';
  $dataLoanType = $pdo->query($sql);						   						 

//4. ---------------------  Payment Method ------------------- //
  $sql = 'SELECT * FROM paymentmethod ORDER BY paymentmethoddesc';
  $dataPaymentMethod = $pdo->query($sql);						   						 
  
Database::disconnect();	
// --------------------------------- EOC 27.05.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
/*if (isset($_GET['chargeid']) ) 
{ 
$chargeid = $_GET['chargeid']; 
}
*/

if (!empty($_POST)) 
{   $count = 0;
	$chargename = $_POST['chargename'];
	$valid = true;
	
		$chargename = $_POST['chargename'];
		$chargeoptionid = $_POST['chargeoptionid'];
		$chargetypeid = $_POST['chargetypeid'];
		
		$loantypeid = $_POST['loantypeid'];
		$paymentmethodid = $_POST['paymentmethodid'];
		
		/* BOC 24.04.2018*/
			$fromdate = $_POST['fromdate'];
			$todate   = $_POST['todate'];						
		/* EOC 24.04.2018*/

		if(isset($_POST['active']))
		{
			$active = 'X';
		}
		else
		{
			$active = '';
		}
		$percentage = $_POST['percentage'];
		$amount = $_POST['amount'];
		$min = $_POST['min'];
		$max = $_POST['max'];
		$graceperiod = $_POST['graceperiod'];	
	
	/* BOC 24.04.2018*/
	if (empty($fromdate)) { $fromdateError = 'Please enter/select from date.'; $valid = false;}
	if (empty($todate))   { $todateError = 'Please enter/select to date.';     $valid = false;}
	/* EOC 24.04.2018*/
	
	if (empty($chargename)) { $chargenameError = 'Please enter Charge Name.'; $valid = false;}

	if (empty($percentage)) { $percentage = 0;}
	if (empty($amount)) { $amount = 0;}
	if (empty($min)) { $min = 0;}
	if (empty($max)) { $max = 0;}
	if (empty($graceperiod)) { $graceperiod = 0;}

		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO $tablename  (chargename,
											chargetypeid,
											chargeoptionid,
											loantypeid,
											paymentmethodid,
											active,
											percentage,
											amount,
											min,
											max,
											graceperiod,fromdate,todate)
					VALUES(?, ?, ?,?, ?, ?, ?,?, ?, ?,?,?,?)";

			$q = $pdo->prepare($sql);
			$q->execute(array($chargename,$chargetypeid,$chargeoptionid,$loantypeid,$paymentmethodid,$active,
			$percentage,$amount,$min,$max,$graceperiod,$fromdate,$todate));
			Database::disconnect();
			$count = $count + 1;
		}
}

?>
<div class="container background-white bottom-border">  
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">  
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Charge Type Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>$tablename successfully created.</p>");
		}
?>
				
</div>				
		<p><b>Charge</b><br />
		<input style="height:30px" type='text' name='chargename' value="<?php echo !empty($chargename)?$chargename:'';?>"/>
			<?php if (!empty($chargenameError)): ?>
			<span class="help-inline"><?php echo $chargenameError;?></span>
			<?php endif; ?>
		</p> 	
		<p><b>Charge Type</b><br />
		<div class="controls">
			<SELECT class="form-control" id="chargetypeid" name="chargetypeid" size="1">
			<!-------------------- BOC 2017.05.27 - Charge Details --->	
			<?php
				$chargetypeidLoc = '';
				$chargetypeSelect = $chargetypeid;
				$chargetypedesc = '';
				foreach($dataChargeType as $row)
				{
					$chargetypeidLoc = $row['chargetypeid'];
					$chargetypedesc = $row['name'];
					// Select the Selected Role 
					if($chargetypeidLoc == $chargetypeSelect)
					{
						echo "<OPTION value=$chargetypeidLoc selected>$chargetypeidLoc - $chargetypedesc</OPTION>";
					}
					else
					{
						echo "<OPTION value=$chargetypeidLoc>$chargetypeidLoc - $chargetypedesc</OPTION>";
					}
				}
										
				if(empty($dataChargeType))
				{
					echo "<OPTION value=0>No Charge Types</OPTION>";
				}
			?>
			<!-------------------- EOC 2017.05.27 - Charge Details  --->												
			</SELECT>
		</div>
		
		<p><b>Charge Option</b><br />
		<div class="controls">
			<SELECT class="form-control" id="chargeoptionid" name="chargeoptionid" size="1">
			<!-------------------- BOC 2017.05.27 - Charge Details --->	
			<?php
				$chargeoptionidLoc = '';
				$chargeoptionSelect = $chargeoptionid;
				$chargeoptionname = '';
				foreach($dataChargeOption as $row)
				{
					$chargeoptionidLoc = $row['chargeoptionid'];
					$chargeoptionname = $row['chargeoptionname'];
					// Select the Selected Role 
					if($chargeoptionidLoc == $chargeoptionSelect)
					{
						echo "<OPTION value=$chargeoptionidLoc selected>$chargeoptionname</OPTION>";
					}
					else
					{
						echo "<OPTION value=$chargeoptionidLoc>$chargeoptionname</OPTION>";
					}
				}
										
				if(empty($dataChargeOption))
				{
					echo "<OPTION value=0>No Charge Option</OPTION>";
				}
			?>
			<!-------------------- EOC 2017.05.27 - Charge Details  --->												
			</SELECT>
		</div>
		
		<p><b>Loan Type ID</b><br />
		<div class="controls">
			<SELECT class="form-control" id="loantypeid" name="loantypeid" size="1">
			<!-------------------- BOC 2017.05.27 - Charge Details --->	
			<?php
				$loantypeidLoc = '';
				$loantypeidSelect = $loantypeid;
				$loantypedesc = '';
				foreach($dataLoanType as $row)
				{
					$loantypeidLoc = $row['loantypeid'];
					$loantypedesc = $row['loantypedesc'];
					// Select the Selected Role 
					if($loantypeidLoc == $loantypeidSelect)
					{
						echo "<OPTION value=$loantypeidLoc selected>$loantypedesc</OPTION>";
					}
					else
					{
						echo "<OPTION value=$loantypeidLoc>$loantypedesc</OPTION>";
					}
				}
										
				if(empty($dataLoanType))
				{
					echo "<OPTION value=0>No Loan Types</OPTION>";
				}
			?>
			<!-------------------- EOC 2017.05.27 - Charge Details  --->												
			</SELECT>
		</div>
		
		<p><b>Payment Method</b><br />
		<div class="controls">
			<SELECT class="form-control" id="paymentmethodid" name="paymentmethodid" size="1">
			<!-------------------- BOC 2017.05.27 - Charge Details --->	
			<?php
				$paymentmethodidLoc = '';
				$paymentmethodidSelect = $paymentmethodid;
				$paymentmethoddesc = '';
				foreach($dataPaymentMethod as $row)
				{
					$paymentmethodidLoc = $row['paymentmethodid'];
					$paymentmethoddesc = $row['paymentmethoddesc'];

					if($paymentmethodidLoc == $paymentmethodidSelect)
					{
						echo "<OPTION value=$paymentmethodidLoc selected>$paymentmethoddesc</OPTION>";
					}
					else
					{
						echo "<OPTION value=$paymentmethodidLoc>$paymentmethoddesc</OPTION>";
					}
				}
										
				if(empty($dataPaymentMethod))
				{
					echo "<OPTION value=0>No Payment Methods</OPTION>";
				}
			?>
			<!-------------------- EOC 2017.05.27 - Charge Details  --->												
			</SELECT>
		</div>
		
		<p><b>Active</b><br />
		    <input style="height:30px" type='checkbox' id='active' name='active' value="<?php echo !empty($active)?$active:'';?>" 
			<?php if(!empty($active)){echo'checked';}?> />
		</p> 
		
		<p><b>Percentage</b><br />
		<input style="height:30px" type='text' name='percentage' value="<?php echo !empty($percentage)?$percentage:'';?>"/>
		</p>
		
		<p><b>Amount</b><br />
		<input style="height:30px" type='text' name='amount' value="<?php echo !empty($amount)?$amount:'';?>"/>
		</p>
		
		<p><b>Min(Days)</b><br />
		<input style="height:30px" type='text' name='min' value="<?php echo !empty($min)?$min:'';?>"/>
		</p>
		
		<p><b>Max(Days)</b><br />
		<input style="height:30px" type='text' name='max' value="<?php echo !empty($max)?$max:'';?>"/>
		</p>
		
		<p><b>Grace Period(Days)</b><br />
		<input style="height:30px" type='text' name='graceperiod' value="<?php echo !empty($graceperiod)?$graceperiod:'';?>"/>
		</p>
		
		
		<p><b>From Date</b><br />
		<input style="height:30px" type='Date' placeholder="yyyy-mm-dd" name='fromdate' value="<?php echo !empty($fromdate)?$fromdate:'';?>"/>
		</p>

		<p><b>To Date</b><br />
		<input style="height:30px" type='Date' placeholder="yyyy-mm-dd" name='todate' value="<?php echo !empty($todate)?$todate:'';?>"/>
		</p>

		<table>
		<!--<tr>
			<td><div class="form-actions">
				<button type="submit" class="btn btn-success">New Charge</button>
				<a class="btn btn-primary" href="charge">Back To Listing</a>
				</div></td>
		</tr> -->
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Save</button>
				<a class="btn btn-primary" href="charge">Back</a>
				</div>
			</td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>