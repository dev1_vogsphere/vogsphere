<?php 
include 'database_ffms.php';
$pdo = db_connect::db_connection();			
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$customerid = null;
$ApplicationId = null;
$rejectionreasons='';
$changedby=$_SESSION['username'];
$tbl  = 'notes';	
$comingfrom = $_SERVER['HTTP_REFERER'];

if ( !empty($_GET['customerid'])) 
{
	$customerid = $_REQUEST['customerid'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$customerid = base64_decode(urldecode($customerid)); 
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}

if ( !empty($_GET['ApplicationId'])) 
{
	$ApplicationId = $_REQUEST['ApplicationId'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.		
	$ApplicationId = base64_decode(urldecode($ApplicationId));
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}
if ( !empty($_GET['CustomerName'])) 
{
	$CustomerName = $_REQUEST['CustomerName'];
	// -- BOC Encrypt 16.09.2017 - Parameter Data.		
	$CustomerName = base64_decode(urldecode($CustomerName));
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}

$CustomerIdEncoded = urlencode(base64_encode($customerid));
$ApplicationIdEncoded = urlencode(base64_encode($ApplicationId));
$CustomerNameEncoded = urlencode(base64_encode($CustomerName));


//Get Cutsomer Notes
$pdo = db_connect::db_connection();			
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "SELECT * FROM $tbl where Member_id=?" ;
$q = $pdo->prepare($sql);
$q->execute(array($customerid ));
$notes = $q->fetchAll();	
Database::disconnect();

//print_r($notes);
		
?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<label for="note">Notes for: <?php echo $CustomerName;?></label>
					    </h2>
 						</div>					 
				   </tr>
		<div class="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
		 <?php foreach($notes as $note) :?>
		 <li><?=$note['notesbody'].' :- '.'created by'.' '.$note['createdby'].' '.$note['createdon']?></li>
		 <?php endforeach; ?>
		
		</div>
		<div class="form-actions">
		<?php
		echo '<a class="btn" href="createNotes.php?customerid='.$CustomerIdEncoded.'&CustomerName='.$CustomerNameEncoded.'&ApplicationId='.$ApplicationIdEncoded.'">Create</a>';
		echo '<a class="btn" href="finfindmember.php">Back</a>';
		?>
		
		</div>
			
					</form>
				</div>
		</div>		
</div> <!-- /container -->