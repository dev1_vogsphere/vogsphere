<?php
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

if(!function_exists('get_beneficiaries'))
{
	function get_beneficiaries($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'benefiary';
			$sql = "select * from $tbl_2 where IntUserCode = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['IntUserCode']));
			$data = $q->fetchAll(PDO::FETCH_ASSOC);

		return $data;
	}
}
if(!function_exists('get_benefiary'))
{
	function get_benefiary($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'benefiary';
			$sql = "select * from $tbl_2 where
			Account_Number  = ? and Account_Type  = ? and Branch_Code = ? and IntUserCode = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['Account_Number'],$dataFilter['Account_Type'],
							  $dataFilter['Branch_Code'],$dataFilter['IntUserCode']));
			$data = $q->fetch(PDO::FETCH_ASSOC);

		return $data;
	}
}
if(!function_exists('get_benefiary_from_arr'))
{
	function get_benefiary_from_arr($id,$Benefiaciary_Arr)
	{
		$data = null;
		//$Benefiaciary_Arr
		foreach($Benefiaciary_Arr as $row)
		{
			if($id == $row['benefiaryid'])
			{
				$data = $row;
			}
		}
		return $data;
	}
}
////////////////////////////////////////////////////////////////////////
// -- Declarations -- //
$UserCode 			= null;
$IntUserCode 		= null;
$usercodePayments   = null;
$param 				= '';
$idB 				= '';
$benefiaryid 		= '';
$userid 			= '';
$Account_Number     = '';
$paysingle			= 'X';
////////////////////////////////////////////////////////////////////////
// -- Data From SESSIONS
// -- User Code.
if(getsession('UserCode')){$UserCode = getsession('UserCode');}
// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');}

// -- Int User Code.
if(getsession('usercodePayments')){$usercodePayments = getsession('usercodePayments');}

// -- Logged User Details.
if(getsession('username')){$userid = getsession('username');}
////////////////////////////////////////////////////////////////////////
/* -- Data get PARAMETERS
if(isset($_GET['IntUserCode'])){$IntUserCode = $_GET['IntUserCode'];}
if(isset($_GET['param'])){$param = $_GET['param'];}
if(empty($IntUserCode)){$IntUserCode = $param;}*/
////////////////////////////////////////////////////////////////////////
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);
// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//////////////////// -- Lookups  -- ////////////////////////////////////
// -- Account Type
$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}
// -- Branch_Codes
$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}
// -- Frequency
$FrequencyArr = array();
$FrequencyArr[] = 'Once Off';
$FrequencyArr[] = 'Weekly';
$FrequencyArr[] = 'Fortnightly';
$FrequencyArr[] = 'Monthly';
$FrequencyArr[] = 'First Working day of the Month';
// -- Notify
$NotifyArr = array();
$NotifyArr[] = 'None';
$NotifyArr[] = 'SMS';
$NotifyArr[] = 'EMAIL';

// -- From Payment Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);

	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode = $params[1];}
	if(empty($userid)){$userid = $params[2];}
}
$bid = '';

/* -- Benefiaciary --- */
$Benefiaciary_Arr 			  = array();
$dataFilterObj['IntUserCode'] = $IntUserCode;
$dataBenefiaryObj 			  = get_beneficiaries($dataFilterObj);
$Benefiaciary_Arr[] = 'Choose Benefiaciary';
$spipe = '|';
foreach($dataBenefiaryObj as $row)
{
	$Benefiaciary_Arr[] = $row['benefiaryid'].$spipe .$row['Account_Holder'].$spipe.$row['Account_Number'].
	$spipe.$row['Account_Type'].$spipe.$row['Branch_Code'].$spipe.$row['Bank_Reference'].$spipe.$row['Client_Reference_1'].$spipe.$row['UserCode'].$spipe.$row['IntUserCode'].$spipe.$row['Notify'].$spipe.$row['Initials'].$spipe.$row['benefiaryemail'].$spipe.$row['lasttransaction'].$spipe.$row['benefiarycontactnumber'].$spipe.$row['benefiaryusercode'];
}

//print_r($Benefiaciary_Arr);

// -- from payments
if(isset($_GET['benefiaryid']))
{
	$benefiaryid = $_GET['benefiaryid'];
}

?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}

.inactiveLink {
   pointer-events: none;
   cursor: default;
}

select{
  box-shadow: inset 20px 20px red
}
</style>
<div class="container background-white bottom-border">
     <br />
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a id="step1"  href="#step-a" type="button" class="btn btn-primary btn-circle">1</a>
            <p><strong>Beneficiary</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-b" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
			<p><strong>Review</strong></p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-c" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
			<p><strong>Complete</strong></p>
        </div>
    </div>
</div>
 <form id="contact-form1" method="POST" action="#" style="display:block" role="form">
    <div class="row setup-content" id="step-a">
         <div class="controls">
           <div class="container-fluid" style="border:1px solid #ccc ">
             </br>
             <div class="row">
			   <div class="control-group">
			   	<div class="col-md-3" style="display:block">
                     <div class="form-group">
                         <label for="form_Initials"><strong>Benefiaciary:</strong></label>
                         <select class="form-control" id="benefiaryid" name="benefiaryid" onChange="enter_benefiary();">
						  <?php
						  $benefiaryidSelect = $benefiaryid;
						  $choosenbenefiaryarr = null;
						  foreach($Benefiaciary_Arr as $row)
						  {
							  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
							  $value0 = '';
							  $value1 = '';
							  $value2 = '';
							  if(isset($rowData[0])){$value0 = $rowData[0];}
							  if(isset($rowData[1])){$value1 = '|'.$rowData[1];}
							  if(isset($rowData[2])){$value2 = '|'.$rowData[2];}
							  $value = $value0.$value1.$value2;
							if($rowData[0] == $benefiaryidSelect)
							  {
								  echo  '<option value="'.$value0.'" selected>'.$value.'</option>';
							  }
							  else
							  {
								  echo  '<option value="'.$rowData[0].'">'.$value.'</option>';
							  }
						  }
						  ?>
                        </select>
                     </div>
                </div>
				<div class="col-md-3" style="display:block">
                     <div class="form-group">
                         <label for="form_Initials"><strong>Initials:</strong></label>
                         <input  required id="Initials" type="text" name="Initials" class="form-control" placeholder=""  data-error="Initials is required." maxlength="3" readonly value = "" />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				<div class="col-md-3" style="display:block">
                     <div class="form-group">
						 <br/><br/>
                         <a  href="<?php if(empty($param)){ echo "addbenefiary";}else{ echo "javascript:get_active_tab(this);";} ?>" style="color:#a94442;">Add New Benefiaciary</a>
                     </div>
                 </div>
				 </div>
             </div>
             </br>
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Holder"><strong>Account Holder Name:</strong></label>
                         <input id="Account_Holder" type="text" name="Account_Holder" class="form-control" placeholder="" maxlength="30" required="required" data-error="Account Holder is required." value = "" readonly />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Type:"><strong>Bank Account Type:</strong></label>
                         <select class="form-control" id="Account_Type" name="Account_Type" readonly>
						  <?php
						  	$Account_TypeSelect = $Account_Type;
							$rowData = null;
						  foreach($Account_TypeArr as $row)
						  {
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							  if($rowData[0] == $Account_TypeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
						  }?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 </div>
		<div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Number"><strong>Bank Account Number:</strong></label>
                         <input id="Account_Number" type="number" name="Account_Number" class="form-control" placeholder="" required="required" data-error="Account Number is required." pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==13) return false;" readonly value = "<?php echo $Account_Number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Branch_Code"><strong>Bank Name:</strong></label>
                         <!--<input id="Branch_Code" type="text" name="Branch_Code" class="form-control" placeholder="" required="required" data-error="Branch Code is required.">-->
                         <select class="form-control" id="Branch_Code" name="Branch_Code" readonly>
						 <?php
						  	$Branch_CodeSelect = $Branch_Code;
							$rowData = null;
							foreach($Branch_CodeArr as $row)
							{
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $Branch_CodeSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div></div>
		<div class="row">
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Reference_1"><strong><strong>My Reference:</strong></strong></label>
                         <input id="Reference_1" type="text" name="Reference_1" class="form-control" placeholder="" required="optional" data-error="Reference 1 is required." maxlength="30" value =""/>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>

				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Bank_Reference"><strong>Beneficiary Reference:</strong></label>
                         <input id="Bank_Reference" type="text" name="Bank_Reference" class="form-control" placeholder="" required="required" data-error="Bank Reference is required." maxlength="30" value = "" />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div></div>
		<div class="row">
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="Notify:"><strong>Payment Notice:</strong></label>
                         <select class="form-control" id="Notify" name="Notify">
  						  <?php
						  	$NotifySelect = $Notify;

						  foreach($NotifyArr as $row)
						  {
							  if($row == $NotifySelect)
							  {echo  '<option value="'.$row.'" selected>'.$row.'</option>';}
							  else
							  {echo  '<option value="'.$row.'">'.$row.'</option>';}
						  }
						  ?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				  <div class="col-md-3">
                     <div class="form-group">
                         <label for="contact_number"><strong>Contact Number/Email:</strong></label>
                         <input id="contact_number" type="number" data-error="Contact Number/Email is required." pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" required="required" name="contact_number" class="form-control" value = ""/>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				<div class="col-md-3" style="display:block">
                     <div class="form-group">
                         <label for="contact_number"><strong>Beneficiary UserCode:</strong></label>
                         <input id="beneficiaryusercode" type="text" name="beneficiaryusercode" class="form-control" placeholder=""  maxlength="4" readonly value = "" />

					 </div>
				</div>
             </div>
           </div>
		</div>
           </br>
           <!--Collection Generation Details-->
           <div class="container-fluid" style="border:1px solid #ccc ">
             </br>
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Amount"><strong>Amount to Pay:</strong></label>
                         <input id="Amount" type="number" name="Amount" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;" class="form-control" placeholder="" required="required" data-error="Amount is required." value = ""  />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 <div class="col-md-3"	>
                        <label for="form_Action_Date"><strong>Action Date:</strong></label>
                        <input id="Action_Date" min="<?php echo date('Y-m-d');?>" type="date" name="Action_Date" class="form-control" required="required" data-error="Action_Date is required." value = "" />
                        <div class="help-block with-errors"></div>
				 </div>
				 </div>
				<div class="row" style="display:none">
                 <div class="col-md-6">
                     <div class="form-group">
                         <input id="User_Code" type="hidden" name="User_Code" class="form-control" value="<?php $_SESSION['UserCode']; ?>" placeholder="" required="required" data-error="Bank Reference is required.">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
             </div>
           </div>
		   <br/>
		       <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
		   </div>
		   <div class="row setup-content" id="step-b">
				<?php include $filerootpath.'/confirm_paysinglebenefiary.php'; ?>
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Confirm</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		   </div>
		   <div class="row setup-content" id="step-c">
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Add another Payment</strong></button>
			  </div>
			</div>
			</div>
			<br/>
			</div></div>
			 <br/>
			 <a href="<?php if(empty($param)){ echo "billpayments";}else{ echo "javascript:clickTab('paymentsreport');";}?>" class="btn btn-primary nextBtn btn-lg pull-right" id="PayNow" type="button" ><strong>Payments History</strong></a>
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary btn-lg pull-right" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>
		   </div>
		   <input style="display:none" type="text" id="bid" name="bid" value="<?php echo $bid; ?>">
     </form>
</div>
<script>
/*
https://bbbootstrap.com/snippets/multi-step-form-wizard-30467045
https://bootsnipp.com/snippets/yaZa
*/
jQuery(document).ready(function(){
//alert(document.getElementById('benefiaryid').value);
//enter_benefiary();
    });

jQuery(window).on('load',enter_benefiary());
function valueselect(sel)
 {
		var Service_Mode = sel.options[sel.selectedIndex].value;
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);

							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});
			  return false;
  }
function clear()
{
  	try
	{
	  // -- clear
	    document.getElementById('benefiaryid').value  = 'Choose Benefiaciary';
		document.getElementById('Initials').value  = '';
		document.getElementById('Account_Holder').value  = '';

		document.getElementById('Account_Type').value  = '';
		document.getElementById('Account_Number').value  = '';

		document.getElementById('Branch_Code').value  = '';
		document.getElementById('Reference_1').value  = '';

		document.getElementById('Bank_Reference').value  = '';
		document.getElementById('contact_number').value  = '';

		document.getElementById('Notify').value  = 'None';
		document.getElementById('Amount').value  = '';
		document.getElementById('Action_Date').value  = '';
	}
	catch(e)
	{

	}
}

function toStep1()
{
document.getElementById("step1").click();
}

function add_to_confirm_benefiary()
{
  try
  {
// --- Test -- //
	var bid = document.getElementById('bid').value;
	var amount = document.getElementById('Amount').value;

    var Account_Type = document.getElementById('Account_Type').value;
	var Account_TypeDesc = '';
	var branchcode = document.getElementById('Branch_Code').value;
	var bankname = '';
	Branch_CodeArr  = <?php echo json_encode($Branch_CodeArr);?>;
	Arr = <?php echo json_encode($Account_TypeArr);?>;
	for (var key in Arr)
	{
	   row = new Array();
	   row = Arr[key].split('|');
	   if(Account_Type == row[0])
	   {
		   Account_TypeDesc = row[1];
	   }
	}

	for (var key in Branch_CodeArr)
	{
	   row = new Array();
	   row = Branch_CodeArr[key].split('|');
	   if(branchcode == row[0])
	   {
		   bankname = row[1];
	   }
	}
// -- Test -- //
	//alert(document.getElementById('Notify').value);
	// -- confirmation.
	document.getElementById('beneficiaryLbl').innerHTML = bid+'-'+document.getElementById('Account_Holder').value+'-'+document.getElementById('Account_Number').value;
	document.getElementById('InitialsLbl').innerHTML = document.getElementById('Initials').value;
	document.getElementById('Account_HolderLbl').innerHTML = document.getElementById('Account_Holder').value;
	document.getElementById('Account_NumberLbl').innerHTML = document.getElementById('Account_Number').value;
	//document.getElementById('Account_TypeLbl').innerHTML = document.getElementById('Account_Type').value;
	//document.getElementById('Branch_CodeLbl').innerHTML = document.getElementById('Branch_Code').value;
	document.getElementById('Account_TypeLbl').innerHTML = Account_TypeDesc;
	document.getElementById('Branch_CodeLbl').innerHTML = bankname;

	document.getElementById('Reference_1Lbl').innerHTML = document.getElementById('Reference_1').value;
	document.getElementById('Bank_ReferenceLbl').innerHTML = document.getElementById('Bank_Reference').value;
	document.getElementById('NotifyLbl').innerHTML = document.getElementById('Notify').value;
	document.getElementById('contact_numberLbl').innerHTML = document.getElementById('contact_number').value;

// -- Changerable
	//document.getElementById('No_PaymentsLbl').innerHTML = document.getElementById('No_Payments').value;
	document.getElementById('AmountLbl').innerHTML 		= document.getElementById('Amount').value;
	//document.getElementById('FrequencyLbl').innerHTML 	= document.getElementById('Frequency').value;
	document.getElementById('Action_DateLbl').innerHTML = document.getElementById('Action_Date').value;

	document.getElementById('benefiaryusercodeLbl').innerHTML = document.getElementById('beneficiaryusercode').value;

  }
  catch(e)
  {
  }
}

function clear_to_confirm_benefiary()
{
	try
	{
	  // -- clear
		document.getElementById('Initials').value  = '';
		document.getElementById('Account_Holder').value  = '';
		document.getElementById('Account_Type').value  = '';
		document.getElementById('Account_Number').value  = '';
		document.getElementById('Branch_Code').value  = '';
		document.getElementById('Reference_1').value  = '';
		document.getElementById('Bank_Reference').value  = '';
		document.getElementById('contact_number').value  = '';
		document.getElementById('Notify').value  = 'None';
		document.getElementById('beneficiaryusercode').value = '';
	}
	catch(e)
	{

	}
}

function toogleEmailNumber()
{
  contact_number = document.getElementById("contact_number");
  Notify = document.getElementById('Notify').value;
  if(Notify === 'SMS')
  {
	 contact_number.outerHTML =
	 '<input id="contact_number" type="number" data-error="Contact Number is required." required="required" placeholder="Enter Mobile Number" name="contact_number" class="form-control" value = "" pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==10) return false;" />';
  }
  else if(Notify === 'EMAIL')
  {
	 contact_number.outerHTML =
	 '<input id="contact_number" type="email" data-error="Contact Email is required." placeholder="Enter Email" name="contact_number" class="form-control"   value = "" />';
  }
  else
  {
	 contact_number.outerHTML =
	 '<input id="contact_number" type="text" name="contact_number" class="form-control" value = "" />';
  }
}

function enter_benefiary()
{
	try
	{
	var id = document.getElementById('benefiaryid').value;//sel.options[sel.selectedIndex].value;

	// - Populate other values.
	//alert(id);
	if(id !== 'Choose Benefiaciary')
	{

	Arr = <?php echo json_encode($Benefiaciary_Arr); ?>;
	for (var key in Arr)
	{
	   row = new Array();
	   row = Arr[key].split('|');
	   if(id == row[0])
	   {
			//alert(row[0]+row[1]+row[2]+row[3]+row[4]+row[5]+row[6]+row[7]+row[8]+row[9]+row[10]+row[11]+row[12]+row[13]+row[14]+row[15]+row[16]);
			// -- Toggle between email & mobile number.
			document.getElementById('Notify').value = row[9];
			toogleEmailNumber();
			document.getElementById('Initials').value = row[10];
			document.getElementById('Account_Holder').value = row[1];
			document.getElementById('Account_Number').value = row[2];
			document.getElementById('Account_Type').value = row[3];
			document.getElementById('Branch_Code').value = row[4];
			document.getElementById('Reference_1').value = row[6];
			document.getElementById('Bank_Reference').value = row[5];
			//alert(row[11]);
			if(document.getElementById('Notify').value == 'SMS')
			{
			   document.getElementById('contact_number').value = row[13];
			}
			else if(document.getElementById('Notify').value == 'EMAIL')
			{
			   document.getElementById('contact_number').value = row[11];
			}
			document.getElementById('bid').value = row[0];
			document.getElementById('beneficiaryusercode').value = row[14];
	   }

	  // add_to_confirm_benefiary();

	}
  }
  else
  {
	 clear_to_confirm_benefiary();
  }
}
catch(e)
{}
}
// --

$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
			allpreviousBtn = $('.previousBtn');


		allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

	 allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;
		//alert(objToString(allpreviousBtn));

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

 });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
			curInputsNum = curStep.find("input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-b')
			{
				//call_script_paybeneficiary();
				var message_info = document.getElementById('message_info');
				message_info.innerHTML = '';
				message_info.className  = '';
				ret2 = check_duplicate_pay_to_beneficiary();
				if(ret2 === 1)
				{
					isValid = false;
				}
			}

//alert(curInputs.length);
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

	   // -- Validate contact number
		if(validate_contactnumber())
		{
			isValid = false;
		}

		// -- Required number.
		for(var i=0; i<curInputsNum.length; i++){
            if (!curInputsNum[i].validity.valid){
                isValid = false;
                $(curInputsNum[i]).closest(".form-group").addClass("has-error");
            }
        }
		//alert('curInputsDate = '+curInputsDate.length);

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus();
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

		add_to_confirm_benefiary();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');

	// -- Next Step Validated
	navListItems.click( function(e)
	{
	/*$(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
    */
		//e.preventDefault(); /*your_code_here;*/
  var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

		$(".form-group").removeClass("has-error");
		//alert(curStepBtn);
        for(var i=0; i<curInputs.length; i++){

            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

		                isValid = false;

		//alert('Test2 - '+nextStepWizard.attr("id"));
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

		return false;
	} );
});

function objToString (obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str += p + '::' + obj[p] + '\n';
        }
    }
    return str;
}
function validate_contactnumber()
{
			//alert('Notify :'+document.getElementById('Notify').value);
		var req	= null;
		if(document.getElementById('Notify').value === 'SMS')
		{
			document.getElementById('contact_number').required = true;//setAttribute('required','required');

			if(document.getElementById('contact_number').value === '')
			{req = true;}else{req = false;}
		}
		else
		{
			document.getElementById('contact_number').required = false;
			req = false;
		}

		if(document.getElementById('benefiaryid').value === 'Choose Benefiaciary')
		{
		  document.getElementById('benefiaryid').required = true;
		  req = true;
		  document.getElementById('benefiaryid').focus();
		  document.getElementById('benefiaryid').style="border-color: #a94442;"
		}
		else
		{
			document.getElementById('benefiaryid').required = false;
			req = false;
		    document.getElementById('benefiaryid').style="";
		}

		return req;
}


function check_duplicate_pay_to_beneficiary()
{

	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;

			var list =
{'benefiaryid'		: document.getElementById('benefiaryid').value,
'Initials'			: document.getElementById('Initials').value,
'Account_Holder'	: document.getElementById('Account_Holder').value,
'Account_Type'		: document.getElementById('Account_Type').value,
'Account_Number'	: document.getElementById('Account_Number').value,
'Branch_Code'		:document.getElementById('Branch_Code').value,
'Client_Reference_1': document.getElementById('Reference_1').value,
'Bank_Reference'	: document.getElementById('Bank_Reference').value,
'Notify'			: document.getElementById('Notify').value,
'Contact_No'		: document.getElementById('contact_number').value,
'Amount'		    : document.getElementById('Amount').value,
'Action_Date'		: document.getElementById('Action_Date').value,

'No_Payments'		: '1',
'Service_Mode'		: 'EFT',
'Service_Type'		: 'SAMEDAY',
'Frequency'			: 'Once Off',

'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode};

				//alert(document.getElementById('Initials').value);
				var feedback = '';
				var ret = 0;

				//--script_checkpaybeneficiary.php
				jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"script_checkpaybeneficiary.php",
									   data:{list:list},
										success: function(data)
										{
										 feedback = data;
										 if(feedback === '')
										 {feedback  = 'Are you sure you want to make this Payment?';}
											if (confirm(feedback))
											{
											  // Save it!
												ret = 0;
												call_script_paybeneficiary();
											}
											else
											{
											  // Do nothing!
											    toStep1();
											    ret = 1;
											}

						}});
				});

				//return ret;
}

function call_script_paybeneficiary()
{
	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;
	
	var usercodePayments = <?php echo "'".$usercodePayments."'"; ?>;;

	var beneficiaryusercode = document.getElementById('beneficiaryusercode').value;
	//alert(userid+' - '+IntUserCode);
			var list =
{'benefiaryid'		: document.getElementById('benefiaryid').value,
'Initials'			: document.getElementById('Initials').value,
'Account_Holder'	: document.getElementById('Account_Holder').value,
'Account_Type'		: document.getElementById('Account_Type').value,
'Account_Number'	: document.getElementById('Account_Number').value,
'Branch_Code'		:document.getElementById('Branch_Code').value,
'Client_Reference_1': document.getElementById('Reference_1').value,
'Bank_Reference'	: document.getElementById('Bank_Reference').value,
'Notify'			: document.getElementById('Notify').value,
'Contact_No'		: document.getElementById('contact_number').value,
'Amount'		    : document.getElementById('Amount').value,
'Action_Date'		: document.getElementById('Action_Date').value,
'Client_Id'		    : usercodePayments,

'No_Payments'		: '1',
'Service_Mode'		: 'EFT',
'Service_Type'		: 'SAMEDAY',
'Frequency'			: 'Once Off',

'createdby' 		: userid,
'IntUserCode'		: IntUserCode,
'UserCode' 			: UserCode,
'benefiaryusercode': beneficiaryusercode};

					//alert(document.getElementById('Initials').value);
var feedback = '';
var ret = 0;

// -- Check Duplicate Payment to benefiaciary.

		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"script_paybeneficiary.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');

PayNow = document.getElementById("PayNow");
Exit = document.getElementById("Exit");
backtoconfirm = document.getElementById("backtoconfirm");
addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								//alert(feedback);
								message_info.innerHTML = 'Payment made successfully.';
								message_info.className  = 'status';
								PayNow.style.visibility = '';
								Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								addbeneficiary.style.visibility = '';
								var res = feedback.split("|");
								if(typeof res[2] === 'undefined')
								{}else{message_info.innerHTML = message_info.innerHTML + '<br/>'+res[2];}
								document.getElementById('bid').value = res[1];
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								PayNow.style.visibility = 'hidden';
								Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								addbeneficiary.style.visibility = 'hidden';
							}

						 }
					});
				});

}
</script>
