<?php
///////////////////////////////////// SEARCH BOX //////////////////////////////////////////
//																						  //
////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$tbl_name="placeholder"; // Table name 
$placeholder = '';
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
//													   									   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
		if ( !empty($_POST))
		{
			$placeholder = $_POST['placeholder'];
		}
					//if($_SESSION['role'] == 'admin')
					{
					  if(!empty($placeholder))
					    {
						$sql = "SELECT * FROM placeholder WHERE placeholder = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($placeholder));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
						// -- All Records
						$sql = "SELECT * FROM placeholder";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					} 
					/*if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $page = $_POST['page'];
						   }
					}*/					   
					   $sql = "";
					   $q = "";
//////////////////////////////////////////////////// END SEARCH BOX //////////////////////
//																						//
//////////////////////////////////////////////////////////////////////////////////////////	
echo "<div class='container background-white bottom-border'>";						  
/*echo "
<div class='row'>
	<div id='breadcrumb' class='col-md-12'>
		<ol class='breadcrumb'>
			<li><a href='dashboard'>Dashboard</a></li>
			<li><a href='#'>Customer Management</a></li>
			<li><a href='#'>Register Customer</a></li>
		</ol>
	</div>
</div>";
*/
echo "<div class='col-xs-12 col-sm-12'>
";
 echo "<div class='row'>";		
//<!-- Search Login Box -->
//  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' >
                                    <div class='login-header margin-bottom-30'>
                                        <h2>Search Placeholders</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='placeholder' name='placeholder' placeholder='placeholder' class='form-control' type='text' value=$placeholder>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">				
		              <?php 
						echo "<a class='btn btn-info' href=newplaceholder.php>Add New Place Holder</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Placeholder</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['placeholder']) . "</td>";  
						echo "<td>
						<a class='btn btn-danger' href=deleteplaceholder.php?placeholderid={$row['placeholderid']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
    	</div>
    	</div>
    	</div>

   		<!---------------------------------------------- End Data ----------------------------------------------->