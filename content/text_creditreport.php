<?php 
require 'database.php';

// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// ----------------------  Declarations --------------
$Title 			= '';
$idnumber 		= '';
$FirstName 		= '';
$LastName 		= '';
$phone 			= '';
$email 			= '';
$Street			= '';
$Suburb 		= '';
$City 			= '';
$State 			= '';
$PostCode 		= '';
$Dob			= '';
$Surety			= '';
$firstpayment 	= '';
$loantypeid 	= 1;
$StaffId 		= 1;
// -------------------------  Banking Details - Initial Values ------------------------------------- //
$AccountHolder 	= '';
$AccountType 	= '';
$AccountNumber 	= '';
$BranchCode 	= '';
$BankName 		= '';
// -------------------------  Banking Details - Initial Values ------------------------------------- //
// -- EOC T.M Modise 20.06.2017
// -- Add Immediate Transfer Charge
// -- Add Insurance Charges
$immediatetransferfee = ''; // -- CheckBox.
$charge_fee_id = 1;			// -- Default in the system.
$dataImmediateTransfer = '';

$loanprotecionplan = '';   // -- CheclBox.
$charge_protecion_id = 4;  // -- Default in the system.
$dataProtectionplan = '';
// -- Read the credit Report from table first if not go transunion for report.
$format = '';
$key = 'UFEC';
// -- Id number.
$idnumber = '9606150675085';//$_SESSION['username'];
$CustomerIdEncoded  = '';
if ( !empty($_GET['customerid'])) 
{
	$idnumber = $_REQUEST['customerid'];
	// -- BOC Encrypt 16.09.2017 - P                                                 arameter Data.
	$CustomerIdEncoded = $idnumber;
	$idnumber = base64_decode(urldecode($idnumber)); 
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
}

$creditreportobj = get_creditreport($idnumber,$key,$format,$pdo);
$credit = json_decode($creditreportobj);

// -- Company Name:
$companyname = "";
if(isset($_SESSION['companyname']))
{
	$companyname = $_SESSION['companyname'];
}
else
{
	$companyname = "Vogsphere (PTY) LTD";
}

?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>

<div class="container background-white bottom-border"> 
  <div class="margin-vert-30">
  					<table class=   "table table-user-information">
					<tr>
						<td>
				<a class="btn btn-primary" href="<?php echo "qualify?customerid=$CustomerIdEncoded"; ?>">Back</a>
						</td>
					</tr></table>
<form METHOD="post" class="signup-page">
  <div class="signup-header">
    <h2 class="text-center">Consumer Profile</h2>
  </div>
<div class="tab-pane fade in" id="sample-301">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-down" id="IdvNI011" href="javascript:toggle('IdvNI011','toggleIdvNI01','Consumer reported on');">
              Consumer reported on
			  </a>
		</h2>		
</div>		
	<div id="toggleIdvNI01" style="display:none;overflow-y:auto;">
	<table id="IdvNI01">
        <thead>
            <tr>
             <!--<th data-field="IDVerifiedCode">IDVerifiedCode</th>
			 <th data-field="IDVerifiedDesc"      >IDVerifiedDesc</th>
             <th data-field="IDWarning"      >IDWarning</th>
             <th data-field="IDDesc"       >IDDesc</th>
             <th data-field="ConsumerNo">Transunion number</th>-->
			 <th data-field="IdentityNo1">Identity Number</th>
			 <th data-field="ConsumerNo">TransUnion Number</th>
             <th data-field="Surname">Surname</th>
             <th data-field="Forename1">First name</th>
             <th data-field="Forename2">Second name</th>
			 <th data-field="Gender">Gender</th>
			 <th data-field="MaritalStatusDesc">Marital Status</th>
			 <th data-field="Dependants">Dependants</th>
			 <th data-field="TelephoneNumbers">Telephone Numbers</th>
			 <th data-field="CellNumber">Cell Number</th>
			 <th data-field="EMail">E-Mail Address</th>
			 <th data-field="DeceasedDate">Deceased Date</th>
			 
             <!--<th data-field="DeceasedDate"       >DeceasedDate</th>-->
            </tr>
        </thead>
    </table></div></div>  

		<!-- enquiries information -->
	<div class="tab-pane fade in" id="sample-302">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="EnquiryNE511" href="javascript:toggle('EnquiryNE511','toggleEnquiryNE51','Consumer Scores');">
              Consumer Scores
			  </a>
		</h2>		
	</div>		
	<div id="toggleEnquiryNE51" style="display:none;overflow-y:auto;">
	
	<table id="tableenquire" style="white-space: nowrap;">
	
        <thead>
	        <tr>
                <th data-field="LongScore">Long-term Score</th>
                <th data-field="Aarc1Long">Adverse Action Reason Description 1</th>
				<th data-field="Aarc2Long">Adverse Action Reason Description 2</th>
				<th data-field="Aarc3Long">Adverse Action Reason Description 3</th>
				<th data-field="Aarc4Long">Adverse Action Reason Description 4</th>
                <!-- <th data-field="EnquiryTypeCode">EnquiryTypeCode</th>
                <th data-field="City">City</th>-->
                <!--<th data-field="EnquiryTypeDescription">Enquiry type</th>
                <th data-field="OwnAccount">Own account</th>-->
                <!--<th data-field="InformationMessage">InformationMessage</th>-->
            </tr>

			<p class="outset">CreditVision Personal Loans is an industry solution that combines four scorecards to help lenders make smarter risk
			assessments at acquisition. With improved predictive power across different loan terms - such as one-month micro loans, short,
			medium and long-term loans - lenders can offer the right products to the right customers with the right terms..</p>
			</br>	
			<p class="outset">Key Description</p>
			</br>
				<ul>
				  <li>Low Risk 719 - 999</li>
				  <li>Medium Risk 668 - 718</li>
				  <li>High Risk 627 - 667</li>
				  <li>Very High Risk 1 - 626</li>
				</ul>  
        </thead>
		</br>
		
	
	
	</table>
	
	
	
	
	</div></div>
	
	
				<div class="tab-pane fade in" id="sample-306">		
			<div class="panel-heading">
			<h2 class="panel-title">
			<a class="accordion-toggle fa-angle-up" id="Enquiries1" href="javascript:toggle('Enquiries1','toggleEnquiries','Enquiries');">
			Enquiries
			</a>
			</h2>		
			</div>		
			<div id="toggleEnquiries"  name="toggleEnquiries"  style="display:none;overflow-y:auto;">
			<table id="Enquiries">
			<thead>
			<tr>
			<!--<th data-field="AreaCode">AreaCode</th>-->
			<th data-field="ConsumerNo">ConsumerNo</th>
			<th data-field="date_of_enquiry">date_of_enquiry</th>
			<th data-field="enq_subscriber_name">enq_subscriber_name</th>
			<th data-field="enq_subscriber_contact">enq_subscriber_contact</th>
			<th data-field="enq_type_cd">enq_type_cd</th>
			<th data-field="enq_type_desc">enq_type_desc</th>
			<th data-field="own_account">own_account</th>
			<th data-field="loan_reason_cd">loan_reason_cd</th>
			<th data-field="loan_reason_desc">loan_reason_desc</th>
			<th data-field="enq_ref_no">enq_ref_no</th>
			<th data-field="industry_cd">industry_cd</th>
			<th data-field="industry_desc">industry_desc</th>
			<th data-field="message">message</th>
			</tr>
			</thead>
			</table></div></div>
			
	<div class="tab-pane fade in" id="sample-297">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="AddressNA081" href="javascript:toggle('AddressNA081','toggleAddressNA08','Address Information');">
              Address Information
			  </a>
		</h2>		
	</div>		
	<div id="toggleAddressNA08" style="display:none;overflow-y:auto;">
	<!-- telephones information -->
        <table id="table" style="white-space: nowrap;">
        <thead>
            <tr>
                <th data-field="ConsumerNo">Consumer No</th>
                <th data-field="InformationDate">Information Date</th>
                <th data-field="Line1">Street</th>
				<th data-field="Line2">House/Complex</th>
                <th data-field="Suburb">Suburb</th>
                <th data-field="City">City</th>
                <th data-field="PostalCode">Postal code</th>
                <!--<th data-field="ProvinceCode">ProvinceCode</th>-->
                <th data-field="Province">Province</th>
                <th data-field="AddressPeriod">Period</th>
                <th data-field="Province">Province</th>
                <th data-field="OwnerTenant">Owner/Tenant</th>
                <th data-field="AddressChanged">Address Changed</th>
            </tr>
        </thead>
     </table>
    </div>
</div>	
	<!-- AffordStandardBatchCharsXB01 -->
<!--<div class="tab-pane fade in" id="sample-299">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="AffordStandardBatchCharsXB011" href="javascript:toggle('AffordStandardBatchCharsXB011','toggleAffordStandardBatchCharsXB01','AffordStandardBatchCharsXB01');">
					 AffordStandardBatchCharsXB01
			  </a>
		</h2>		
	</div>		
	<div id="toggleAffordStandardBatchCharsXB01" style="display:none;overflow-y:auto;">		
	<table id="tableAffordStandardBatchCharsXB01" style="white-space: nowrap;">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="ML179">ML179</th>
             <th data-field="ML180">ML180</th>
             <th data-field="ML181">ML181</th>
             <th data-field="XC121">XC121</th>
             <th data-field="XC122">XC122</th>
             <th data-field="XC179">XC179</th>
             <th data-field="XC180">XC180</th>
             <th data-field="XC181">XC181</th>
             <th data-field="XH179">XH179</th>
             <th data-field="XH180">XH180</th>
             <th data-field="XH181">XH181</th>
             <th data-field="XI179">XI179</th>
             <th data-field="XI180">XI180</th>
             <th data-field="XI181">XI181</th>
             <th data-field="XL179">XL179</th>
             <th data-field="XL180">XL180</th>
             <th data-field="XL181">XL181</th>
             <th data-field="XO179">XO179</th>
             <th data-field="XO180">XO180</th>
             <th data-field="XO181">XO181</th>
             <th data-field="XP179">XP179</th>
             <th data-field="XP180">XP180</th>
             <th data-field="XP181">XP181</th>
             <th data-field="XR121">XR121</th>
             <th data-field="XR122">XR122</th>
             <th data-field="XR179">XR179</th>
             <th data-field="XR180">XR180</th>
             <th data-field="XR181">XR181</th>
             <th data-field="XS179">XS179</th>
             <th data-field="XS180">XS180</th>
             <th data-field="XS181">XS181</th>
            </tr>
        </thead>
    </table></div></div>
	
	<div class="tab-pane fade in" id="sample-300">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="ConsEnqTransInfo01021" href="javascript:toggle('ConsEnqTransInfo01021','toggleConsEnqTransInfo0102','ConsEnqTransInfo0102');">
					 ConsEnqTransInfo0102
			  </a>
		</h2>		
	</div>		
	<div id="toggleConsEnqTransInfo0102" style="display:none;overflow-y:auto;">
	<table id="ConsEnqTransInfo0102">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="DefiniteMatchCount"      >DefiniteMatchCount</th>
             <th data-field="PossibleMatchCount"      >PossibleMatchCount</th>
             <th data-field="MatchedConsumerNo"       >MatchedConsumerNo</th>
             <th data-field="PossibleConsumerNo"      >PossibleConsumerNo</th>
             <th data-field="PossibleAdverseIndicator">PossibleAdverseIndicator</th>
            </tr>
        </thead>
    </table></div></div>
	
	<div class="tab-pane fade in" id="sample-301">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="ConsumerCountersNC501" href="javascript:toggle('ConsumerCountersNC501','toggleConsumerCountersNC50','ConsumerCountersNC50');">
					 ConsumerCountersNC50
			  </a>
		</h2>		
	</div>	
	<div id="toggleConsumerCountersNC50" style="display:none;overflow-y:auto;">
	<table id="ConsumerCountersNC50">
        <thead>
            <tr>
             <th data-field="ConsumerNo"						>ConsumerNo						</th>
             <th data-field="OwnEnquiries1YrBack"				>OwnEnquiries1YrBack				</th>
             <th data-field="OwnEnquiries2YrsBack"				>OwnEnquiries2YrsBack				</th>
             <th data-field="OwnEnquiriesMoreThen2YrsBack"		>OwnEnquiriesMoreThen2YrsBack		</th>
             <th data-field="OtherEnquiries1YrBack"				>OtherEnquiries1YrBack				</th>
             <th data-field="OtherEnquiries2YrsBack"			>OtherEnquiries2YrsBack			</th>
             <th data-field="OtherEnquiriesMoreThen2YrsBack"	>OtherEnquiriesMoreThen2YrsBack	</th>
             <th data-field="Judgements1YrBack"					>Judgements1YrBack					</th>
             <th data-field="Judgements2YrsBack"				>Judgements2YrsBack				</th>
             <th data-field="JudgementsMoreThen2YrsBack"		>JudgementsMoreThen2YrsBack		</th>
             <th data-field="Notices1YrBack"					>Notices1YrBack					</th>
             <th data-field="Notices2YrsBack"					>Notices2YrsBack					</th>
             <th data-field="NoticesMoreThen2YrsBack"			>NoticesMoreThen2YrsBack"			</th>
             <th data-field="Defaults1YrBack"					>Defaults1YrBack"					</th>
             <th data-field="Defaults2YrsBack"					>Defaults2YrsBack"					</th>
             <th data-field="DefaultsMoreThen2YrsBack"			>DefaultsMoreThen2YrsBack"			</th>
             <th data-field="PaymentProfiles1YrBack"			>PaymentProfiles1YrBack"			</th>
             <th data-field="PaymentProfiles2YrsBack"			>PaymentProfiles2YrsBack"			</th>
             <th data-field="PaymentProfilesMoreThen2YrsBack"	>PaymentProfilesMoreThen2YrsBack"	</th>
             <th data-field="TraceAlerts1YrBack"				>TraceAlerts1YrBack"				</th>
             <th data-field="TraceAlerts2YrsBack"				>TraceAlerts2YrsBack"				</th>
             <th data-field="TraceAlertsMoreThen2YrsBack"		>TraceAlertsMoreThen2YrsBack"		</th>
            </tr>
        </thead>
    </table></div></div>-->	
	
<div class="tab-pane fade in" id="sample-310">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="Employment1" href="javascript:toggle('Employment1','toggleEmployment',' Employment History');">
              Employment History
			  </a>
		</h2>		
	</div>		
	<div id="toggleEmployment" style="display:none;overflow-y:auto;">
	<table id="Employment">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="InformationDate">InformationDate</th>
             <th data-field="Occupation">Occupation</th>
             <th data-field="EmployerName">EmployerName</th>
			 <th data-field="EmploymentPeriod">EmploymentPeriod</th>

            </tr>
        </thead>
    </table></div></div>
	
	<div class="tab-pane fade in" id="sample-310">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="IncomeEstimator1" href="javascript:toggle('IncomeEstimator1','toggleIncomeEstimator',' Income Estimator');">
              Income Estimator
			  </a>
		</h2>		
	</div>		
	<div id="toggleIncomeEstimator" style="display:none;overflow-y:auto;">
	<table id="IncomeEstimator">
        <thead>
            <tr>
             <th data-field="VariableName">VariableName</th>
			 <th data-field="VariableValue">VariableValue</th>
            </tr>
        </thead>
    </table></div></div>
	<!--Payment History -->
	<div class="tab-pane fade in" id="sample-310">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="EvolutionPaymentHistory1" href="javascript:toggle('EvolutionPaymentHistory1','toggleEvolutionPaymentHistory',' Payment History');">
              Payment History
			  </a>
		</h2>		
	</div>		
	<div id="toggleEvolutionPaymentHistory" style="display:none;overflow-y:auto;">
	<table id="EvolutionPaymentHistory">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="Lastupdatedate">Lastupdatedate</th>
			 <th data-field="SupplierName">SupplierName</th>
			 <th data-field="IndustryCode">IndustryCode</th>
			 <th data-field="Industry">Industry</th>
			 <th data-field="AccountTypeCode">AccountTypeCode</th>
			 <th data-field="AccountType">AccountType</th>
			 <th data-field="AccountNumber">AccountNumber</th>
			 <th data-field="BranchCode">BranchCode</th>
			 <th data-field="SupplierReferencenumber">SupplierReferencenumber</th>
			 <th data-field="DateOpened">DateOpened</th>
			 <th data-field="OpeningBalance">OpeningBalance</th>
			 <th data-field="Instalment">Instalment</th>
			 <th data-field="CurrentBalance">CurrentBalance</th>
			 <th data-field="Terms">Terms</th>
			 <th data-field="OverdueAmount">OverdueAmount</th>
			 <th data-field="OwnershipType">OwnershipType</th>
			 <th data-field="NumberParticipantsJointLoan">NumberParticipantsJointLoan</th>
			 <th data-field="PaymentType">PaymentType</th>
			 <th data-field="RepaymentFrequency">RepaymentFrequency</th>
			 <th data-field="LoanReasonCodeDescription">LoanReasonCodeDescription</th>
			 <th data-field="DeferredPaymentDate">DeferredPaymentDate</th>
			 <th data-field="AccSoldTo3rdParty">AccSoldTo3rdParty</th>
			 <th data-field="ThirdPartyName">ThirdPartyName</th>
			 </tr>
        </thead>
    </table></div></div>
	
	
	
	
	
	
	
	

<div class="tab-pane fade in" id="sample-302">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="HomeNumbers1" href="javascript:toggle('HomeNumbers1','toggleHomeNumbers',' Telephone History Home');">
              Telephone History Home
			  </a>
		</h2>		
	</div>		
	<div id="toggleHomeNumbers" style="display:none;overflow-y:auto;">
	<table id="HomeNumbers">
        <thead>
            <tr>
             <th data-field="AreaCode">AreaCode</th>
			 <th data-field="Number"      >DefiniteMatchCount</th>
             <th data-field="Date"      >Date</th>
             <th data-field="Years"       >Years</th>
            </tr>
        </thead>
    </table></div></div>
<!--	
<div class="tab-pane fade in" id="sample-303">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="HomeNumbers1" href="javascript:toggle('HomeNumbers1','toggleHomeNumbers','Telephone History Personal');">
              Telephone History Personal
			  </a>
		</h2>		
	</div>		
	<div id="toggleHomeNumbers" style="display:none;overflow-y:auto;">
	<table id="HomeNumbers">
        <thead>
            <tr>
             <th data-field="AreaCode">AreaCode</th>
			 <th data-field="Number"      >DefiniteMatchCount</th>
             <th data-field="Date"      >Date</th>
             <th data-field="Years"       >Years</th>
            </tr>
        </thead>
    </table></div></div>-->

<div class="tab-pane fade in" id="sample-303">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="CellNumbers1" href="javascript:toggle('CellNumbers1','toggleCellNumbers','Telephone History Personal');">
					 Telephone History Personal
			  </a>
		</h2>		
	</div>		
	<div id="toggleCellNumbers" style="display:none;overflow-y:auto;">
	<table id="CellNumbers">
        <thead>
            <tr>
             <!--<th data-field="AreaCode">AreaCode</th>-->
			 <th data-field="Number">Contact number</th>
             <th data-field="Date">Date</th>
             <th data-field="Years">Years</th>
            </tr>
        </thead>
    </table></div></div>
	
	<div class="tab-pane fade in" id="sample-304">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="WorkNumbers1" href="javascript:toggle('WorkNumbers1','toggleWorkNumbers','Telephone History Work');">
					 Telephone History Work
			  </a>
		</h2>		
	</div>		
	<div id="toggleWorkNumbers" style="display:none;overflow-y:auto;">
	<table id="WorkNumbers">
        <thead>
            <tr>
             <!--<th data-field="AreaCode">AreaCode</th>-->
			 <th data-field="Number">Contact number</th>
             <th data-field="Date">Date</th>
             <th data-field="Years">Years</th>
            </tr>
        </thead>
    </table></div></div>
	
<div class="tab-pane fade in" id="sample-305">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="FraudScoreFS011" href="javascript:toggle('FraudScoreFS011','toggleFraudScoreFS01','Fraud Score');">
					 Fraud Score
			  </a>
		</h2>		
	</div>		
	<div id="toggleFraudScoreFS01" style="display:none;overflow-y:auto;">
	<table id="FraudScoreFS01">
        <thead>
            <tr>
            <!-- <th data-field="RecordSequence">RecordSequence</th>
			 <th data-field="Part"      >Part</th>
             <th data-field="PartSequence"      >PartSequence</th>
             <th data-field="Consumer no">ConsumerNo</th>-->
             <th data-field="Rating">Rating</th>
             <th data-field="RatingDescription">Rating description</th>
             <!--<th data-field="ReasonCode"       >ReasonCode</th>
             <th data-field="ReasonDescription"       >ReasonDescription</th>-->
            </tr>
        </thead>
    </table></div></div>
	

	
	
<div class="tab-pane fade in" id="sample-307">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="DefaultsD701Part11" href="javascript:toggle('DefaultsD701Part11','toggleDefaultsD701Part1','Adverse Details');">
              Adverse Details
			  </a>
		</h2>		
	</div>		
	<div id="toggleDefaultsD701Part1" style="display:none;overflow-y:auto;">	
	<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="DefaultsD701Part111" href="javascript:toggle('DefaultsD701Part111','toggleDefaultsD701Part11','Defaults');">
              Defaults
			  </a>
		</h2>		
	<table id="DefaultsD701Part1">
        <thead><tr>
                <th data-field="RecordSequence">RecordSequence</th>
				<th data-field="Part">Part</th>
				<th data-field="PartSequence">PartSequence</th>
				<th data-field="ConsumerNo">ConsumerNo</th>
				<th data-field="ContactName">ContactName</th>
				<th data-field="TelephoneCode">TelephoneCode</th>
				<th data-field="TelephoneNumber">TelephoneNumber</th>
				<th data-field="InformationDate">InformationDate</th>
				<th data-field="SupplierName">SupplierName</th>
				<th data-field="AccountNo">AccountNo</th>
				<th data-field="SubAccount">SubAccount</th>
				<th data-field="Branch">Branch</th>
				<th data-field="DefaultTypeCode">DefaultTypeCode</th>
				<th data-field="DefaultType">DefaultType</th>
				<th data-field="DefaultAmount">DefaultAmount</th>
				<th data-field="WrittenOffDate">WrittenOffDate</th>
                <th data-field="Surname1">Surname1</th>
				<th data-field="Forename1">Forename1</th>
				<th data-field="Forename2">Forename2</th>
				<th data-field="Forename3">Forename3</th>
				<th data-field="IdentityNumber">IdentityNumber</th>
				<th data-field="DateOfBirth">DateOfBirth</th>
				<th data-field="AddressLine1">AddressLine1</th>
				<th data-field="AddressLine1">AddressLine2</th>
				<th data-field="AddressLine1">AddressLine3</th>
				<th data-field="AddressLine1">AddressLine4</th>
				<th data-field="AddressLine1">Postcode</th>
				<th data-field="Remarks1">Remarks1</th>
				<th data-field="Remarks2">Remarks2</th>
             </tr></thead></table>
	
	

	<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="JudgementsNJ071" href="javascript:toggle('JudgementsNJ071','toggleJudgementsNJ071','Judgements');">
              JudgementsNJ07
			  </a>
		</h2>		
	<table id="JudgementsNJ07">
        <thead><tr>
                <th data-field="RecordSeq">RecordSeq</th>
				<th data-field="Part">Part</th>
				<th data-field="PartSeq">PartSeq</th>
				<th data-field="ConsumerNo">ConsumerNo</th>
				<th data-field="JudgmentDate">JudgmentDate</th>
				<th data-field="Amount">Amount</th>
				<th data-field="Plaintiff">Plaintiff</th>
				<th data-field="JudgmentTypeCode">JudgmentTypeCode</th>
				<th data-field="JudgmentTypeDesc">JudgmentTypeDesc</th>
				<th data-field="CourtTypeCode">CourtTypeCode</th>
				<th data-field="CourtTypeDesc">CourtTypeDesc</th>
				<th data-field="CourtNameCode">CourtNameCode</th>
				<th data-field="CourtNameDesc">CourtNameDesc</th>
				<th data-field="CaseNo">CaseNo</th>
				<th data-field="NatureOfDebtCode">NatureOfDebtCode</th>
				<th data-field="NatureOfDebtDesc">NatureOfDebtDesc</th>
				<th data-field="Remarks">Remarks</th>
				<th data-field="CaptureDate">CaptureDate</th>
				<th data-field="Surname1">Surname1</th>
				<th data-field="Surname2">Surname2</th>
				<th data-field="Forename1">Forename1</th>
				<th data-field="Forename2">Forename2</th>
				<th data-field="Forename3">Forename3</th>
				<th data-field="Title">Title</th>
				<th data-field="DefendantNumber">DefendantNumber</th>
				<th data-field="Address">Address</th>
				<th data-field="PostSuburbCode">PostSuburbCode</th>
				<th data-field="TradingAsName">TradingAsName</th>
				<th data-field="DateOfBirth">DateOfBirth</th>
				<th data-field="IDNo">IDNo</th>
				<th data-field="DefendantMagisterialDistrict">DefendantMagisterialDistrict</th>
				<th data-field="AdministrationMonthlyAmount">AdministrationMonthlyAmount</th>
				<th data-field="AdministrationNumberOfMonths">AdministrationNumberOfMonths</th>
				<th data-field="AdministrationDate">AdministrationDate</th>
				<th data-field="BondPercentage">BondPercentage</th>
				<th data-field="AttorneyName">AttorneyName</th>
				<th data-field="AttorneyReference">AttorneyReference</th>
				<th data-field="AttorneyTelephone">AttorneyTelephone</th
				
				
								
				
				
				
			
             </tr></thead></table></div></div>
			 
			 
			 
			 
			 
			 
			 
			 
			 
<div class="tab-pane fade in" id="sample-308">		
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="NLRSummaryMY501" href="javascript:toggle('NLRSummaryMY501','toggleNLRSummaryMY50','Report Summary');">
              Report Summary
			  </a>
		</h2>		
	</div>		
	<div id="toggleNLRSummaryMY50" name="toggleNLRSummaryMY50" style="display:none;overflow-y:auto;">			
	<table id="NLRSummaryMY50">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="Curr_own_enq">Curr_own_enq</th>
             <th data-field="prev_own_enq">prev_own_enq</th>
             <th data-field="Curr_other_enq"> Curr_other_enq accounts24Mths</th>
            <!-- <th data-field="HighestActualMonths24Mths"       >HighestActualMonths24Mths</th>-->
             <th data-field="prev_other_enq">prev_other_enq</th>
             <th data-field="Curr_jdgmnt">Curr_jdgmnt</th>
             <th data-field="prev_jdgmnt">prev_jdgmnt</th>
			 <th data-field="other_jdgmnt">other_jdgmnt</th>
             <th data-field="Curr_notices">Curr_notices</th>
             <th data-field="prev_notices">prev_notices</th>
             <th data-field="other_notices">other_notices</th>
             <th data-field="Curr_default">Curr_default</th>
             <th data-field="prev_default">prev_default</th>
			 <th data-field="Curr_trace_alerts">Curr_trace_alerts</th>
			 <th data-field="prev_trace_alerts"       >prev_trace_alerts</th>
			 <th data-field="active_accounts">active_accounts</th>
			 <th data-field="curr_positive_loans"       >curr_positive_loans</th>
			 <th data-field="curr_higest_mia">curr_higest_mia</th>
			 <th data-field="prev_positive_accounts"       >prev_positive_accounts</th>
			 <th data-field="prev_higest_mia"       >prev_higest_mia</th>
			 <th data-field="closed_accounts"       >closed_accounts</th>
			 <th data-field="adverse_accounts"       >adverse_accounts</th>
			 <th data-field="mnths_in_arrears"       >mnths_in_arrears</th>
			 <th data-field="curr_balance"       >curr_balance</th>
			 <th data-field="curr_monthly_installment"       >curr_monthly_installment</th>
			 <th data-field="cummulative_arrears_amount"       >cummulative_arrears_amount</th>
			   
			 
            </tr>
        </thead>
    </table>
	</div>						
  </div><!--		
<div class="tab-pane fade in" id="sample-309">		
 <div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="PaymentProfileNP151" href="javascript:toggle('PaymentProfileNP151','togglePaymentProfileNP15','PaymentProfileNP15');">
					 PaymentProfileNP15
			  </a>
		</h2>		
	</div>	
	<div id="togglePaymentProfileNP15" name="togglePaymentProfileNP15" style="display:none;overflow-y:auto;">		
	<table id="PaymentProfileNP15">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="LastUpdateDate"      >LastUpdateDate</th>
             <th data-field="SupplierName"      >SupplierName</th>
             <th data-field="IndustryCode"       >IndustryCode</th>
             <th data-field="IndustryDesc"       >IndustryDesc</th>
             <th data-field="AccountTypeCode"       >AccountTypeCode</th>
             <th data-field="AccountTypeDesc"       >AccountTypeDesc</th>
             <th data-field="AccountNumber"       >AccountNumber</th>
			 <th data-field="SubAccount"      >SubAccount</th>
             <th data-field="DateOpened"      >DateOpened</th>
             <th data-field="OpeningBalance"       >OpeningBalance</th>
             <th data-field="Instalment"       >Instalment</th>
             <th data-field="CurrentBalance"       >CurrentBalance</th>
             <th data-field="Terms"       >Terms</th>
             <th data-field="Date"       >Date</th>
             <th data-field="Status"       >Status</th>
            </tr>
        </thead>
    </table>
	</div>						
  </div>	
<div class="tab-pane fade in" id="sample-310">	
<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="QualificationValidationNQ1" href="javascript:toggle('QualificationValidationNQ1','toggleQualificationValidationNQ','QualificationValidationNQ');">
					 QualificationValidationNQ
			  </a>
		</h2>		
	</div>								
	<div id="toggleQualificationValidationNQ" name="toggleQualificationValidationNQ" style="display:none;overflow-y:auto;">
	<table id="QualificationValidationNQ">
        <thead>
            <tr>
             <th data-field="ConsumerNo">ConsumerNo</th>
			 <th data-field="RecordNumber"      >RecordNumber</th>
             <th data-field="IssueDate"      >IssueDate</th>
             <th data-field="InstitutionName"       >InstitutionName</th>
             <th data-field="QualificationName"       >QualificationName</th>
             <th data-field="Status"       >Status</th>
             <th data-field="ResponseCode"       >ResponseCode</th>
             <th data-field="RecordsMatched"       >RecordsMatched</th>
            </tr>
        </thead>
    </table>	
	</div>						
  </div>
	
	<div class="tab-pane fade in" id="sample-3b">							
	<div class="panel-heading">
		<h2 class="panel-title">
			  <a class="accordion-toggle fa-angle-up" id="V1Segment1" href="javascript:toggle('V1Segment1','toggleV1Segment','V1Segment1');">
					 V1Segment1
			  </a>
		</h2>		
	</div>	
	<div id="toggleV1Segment" name="toggleV1Segment" style="display:none;overflow-y:auto;">
		<table id="V1Segment">
			<thead>
				<tr>
				 <th data-field="Code">Code</th>
				 <th data-field="Value">Value</th>
				</tr>
			</thead>
		</table>		
	</div>
	</div>		-->				

  </form>
<table class=   "table table-user-information">
					<tr>
						<td>
				<a class="btn btn-primary" href="<?php echo "qualify?customerid=$CustomerIdEncoded"; ?>">Back</a>
						</td>
					</tr></table>
</div>  </div>

</body>
<script>
         var $table = $('#table');
		 var mydata  = <?php echo $creditreportobj;//echo json_encode($character);?>;
		 var IdvNI01={};
		 var datarr1={};
		 var datarr2={};
		 var CellNumbers={};
		 var WorkNumbers={};
		 var HomeNumbers={};
		 var FraudScoreFS01={};
		 var Enquiries={};
		 var NLRSummaryMY50={};
		 var Employment={};
		 var IncomeEstimator={};
		 var EvolutionPaymentHistory={};
		 var DefaultsD701Part1={};
		 var JudgementsNJ07={};
		 
	try{
		mydataobjstring=JSON.stringify(mydata);
		
		if (mydataobjstring.includes("ConsumerInfoNO05")) {
		IdvNI01	= [mydata.ProcessRequestTrans04Result.ConsumerInfoNO05];
		}
		
		if (mydataobjstring.includes("AddressNA08")) {
		datarr1 = mydata.ProcessRequestTrans04Result.AddressNA08.AddressNA08;
		}
		if (mydataobjstring.includes("PLScorecardUL01")) {
		datarr2 =  [mydata.ProcessRequestTrans04Result.PLScorecardUL01.PLScorecardUL01];
		}
		if (mydataobjstring.includes("ConsumerTelephoneHistoryNW01")) {
		CellNumbers = mydata.ProcessRequestTrans04Result.ConsumerTelephoneHistoryNW01.CellNumbers.PhoneNumber;
		}
		if (mydataobjstring.includes("ConsumerTelephoneHistoryNW01")) {
		WorkNumbers = mydata.ProcessRequestTrans04Result.ConsumerTelephoneHistoryNW01.WorkNumbers.PhoneNumber;
		}
		if (mydataobjstring.includes("ConsumerTelephoneHistoryNW01")) {
		HomeNumbers = mydata.ProcessRequestTrans04Result.ConsumerTelephoneHistoryNW01.HomeNumbers.PhoneNumber;
		}
		if (mydataobjstring.includes("FraudScoreFS01")) {
		FraudScoreFS01 = [mydata.ProcessRequestTrans04Result.FraudScoreFS01];
		}
		if (mydataobjstring.includes("EvolutionEN01")) {
		Enquiries = mydata.ProcessRequestTrans04Result.EvolutionEN01.EvolutionEN01;
		}
		if (mydataobjstring.includes("EvolutionCC01")) {
		NLRSummaryMY50  = [mydata.ProcessRequestTrans04Result.EvolutionCC01.EvolutionCC01];	
		}
		if (mydataobjstring.includes("EmploymentNM04")) {
		Employment=	mydata.ProcessRequestTrans04Result.EmploymentNM04.EmploymentNM04;
		}
		if (mydataobjstring.includes("IncomeEstimatorI401")) {
		IncomeEstimator=mydata.ProcessRequestTrans04Result.IncomeEstimatorI401.IncomeEstimatorI4Variable.IncomeEstimatorVariableI401;
		}
		if (mydataobjstring.includes("EvolutionPO01")) {
		EvolutionPaymentHistory=mydata.ProcessRequestTrans04Result.EvolutionPO01.EvolutionPO01;
		}
		if (mydataobjstring.includes("DefaultsD701Part1")) {
			DefaultsD701Part1=mydata.ProcessRequestTrans04Result.DefaultsD701Part1.DefaultD701Part1;
		}
		if (mydataobjstring.includes("JudgementsNJ07")) {
			JudgementsNJ07=[mydata.ProcessRequestTrans04Result.JudgementsNJ07.JudgementsNJ07];
		}


		
		
}catch(e){
    if(e){
    // If fails, Do something else
    }
}

$(function () {
	$('#IdvNI01').bootstrapTable({
        data: IdvNI01
    });
	$('#Enquiries').bootstrapTable({
        data: Enquiries
    });
	
    $('#table').bootstrapTable({
        data: datarr1
    });
	

	 $('#tableenquire').bootstrapTable({
        data: datarr2
    });
	
	$('#CellNumbers').bootstrapTable({
        data: CellNumbers
    });
	
	$('#WorkNumbers').bootstrapTable({
        data: WorkNumbers
    });
	
	$('#HomeNumbers').bootstrapTable({
        data: HomeNumbers
    });
	
	$('#FraudScoreFS01').bootstrapTable({
        data: FraudScoreFS01
    });
		$('#NLRSummaryMY50').bootstrapTable({
        data: NLRSummaryMY50
    });
	
	$('#Employment').bootstrapTable({
        data: Employment
    });
		$('#IncomeEstimator').bootstrapTable({
        data: IncomeEstimator
    });
	$('#EvolutionPaymentHistory').bootstrapTable({
	
		data: EvolutionPaymentHistory
    });
	$('#JudgementsNJ07').bootstrapTable({
        data: JudgementsNJ07
    });
	$('#DefaultsD701Part1').bootstrapTable({
		 
		 data: DefaultsD701Part1
		
    });
	
	
});


function toggle(id,display,title) 
 {
	 
	var ele = document.getElementById(display);//toggleV1Segment
	var text = document.getElementById(id); //V1Segment1
	var eleA = '';
	//alert(display);
	idV = "'"+id+"'";
	displayV = "'"+display+"'";
	titleV = "'"+title+"'";
	//href="javascript:toggle('V1Segment1','toggleV1Segment','V1Segment1');"
	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		eleA = '<a class="accordion-toggle fa-angle-up" id="'+id+'" href="javascript:toggle('+idV+','+displayV+','+titleV+');">'+title+'</a>';
		//alert(eleA);
		text.outerHTML = eleA;

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		eleA = '<a class="accordion-toggle fa-angle-down" id="'+id+'" href="javascript:toggle('+idV+','+displayV+','+titleV+');">'+title+'</a>';
//text.outerHTML = '<a class="accordion-toggle fa-angle-down" id="displayAddress" href="javascript:toggleAddress('+'"sas",'+'"sasa",'+'"dadsda");">	Residential Address</a>';
		
				//alert(eleA);

		text.outerHTML = eleA;
	}
} 
</script>