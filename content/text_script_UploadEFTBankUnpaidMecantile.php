<?php
	require 'database.php';
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$tbl_lead = "lead";
	$dbname = "ecashpdq_eloan";
	$filecount = 0;
	$customerid = null;
	$ApplicationId = null;
			
	$FILELEADS = '';

	$fieldname = '';
	$filenameEncryp = '';

	$valid_formats = array("upl","txt");
	$max_file_size = 1024*10000; //10MB
	$path = "Files/Collections/In/"; // File directory
	$backuppath = "Files/Collections/Backup/In/"; // Backup directory
?>

<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Script to Upload EFT Bank Unpaid Mecantile via .upl/.txt File</h1>
		<?php
		# error messages
		if (isset($message)) {
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>%d files added successfully!</p>\n", $count);
		}
		?>
				<!-- Multiple file upload html form-->
		<form action="" method="post" enctype="multipart/form-data">
		<p>Max file size 10Mb, Valid formats upl</p>
		<br />				
		<br />
		<table class ="table table-user-information">
					<?php			
					if (is_dir($dir)) 
					{
						if ($dh = opendir($dir)) {
							while (($file = readdir($dh)) !== false) {
								echo "filename: .".$file."<br />";
							}
							closedir($dh);
						}
					}
					?>
				  <tr>
					<td>
						<div>
							<!-- <input type="submit" value="Upload"> -->
							<input type="submit" class="btn btn-primary" value="Upload">
						</div>
					</td>
				<td>
				<div>
				<?php echo '<a class="btn btn-primary" href="'.$location.'">Back</a>'; ?></div></td>
				</tr>
		 </table>	
			
			<?php if(isset($_POST))
			{
				
				if(!empty($filenameEncryp))
				{ ?>
				<div class="row">
				  <div class="table-responsive">
					<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
					  <tr>
					  <td>Action Date</td>
					  <td>Status Code</td>					  
					  <td>Account Number</td>
					  <td>Feedback</td>
					  </tr>					  
			<?php
					// ----- call ------ 
					$flatfiledata = null;
					$count = 0;
					$x=0;
					$z = 0;
					$whereArray = array();
					$collectionObj = null;
					$bankresponsecodeObj = null;
					
					$actiondate = '';
					$accountnumber = '';
					$statusCode = '';
					
					$ignoreHeaderLines = 11; // -- number of first lines
					$message = " ";
					$delimiter = " ";
					$flatfiledata = flatfile_in_array($filenameEncryp,$ignoreHeaderLines,$delimiter); 
					$count = count($flatfiledata); 
					//echo 'count = '.$count;	
					//print_r($flatfiledata);				
					for($x=0; $x<$count; $x++) 
						{ 
							$countInside = arraycolumncounter($flatfiledata);
							$actiondate    = '';
							$accountnumber = '';
							$statusCode    = '';
							
							echo"<tr>";
							for($z=-1;$z<$countInside;$z++)
							{
									// -- Action Date [0][9]	 isset($flatfiledata[$x][9]) && 
									if($z == 9 && isset($flatfiledata[$x][$z]))
									{	$actiondate = $flatfiledata[$x][$z];
										echo "<td>".$flatfiledata[$x][$z]."</td>";}
									// -- status code [0][16] isset($flatfiledata[$x][15]) && 
									if($z == 15 && isset($flatfiledata[$x][$z]))
									{   $statusCode = $flatfiledata[$x][$z];
										echo "<td>".$flatfiledata[$x][$z]."</td>";}
									// -- Client Reference. [0][24] isset($flatfiledata[$x][24]) && 
									if($z == 24 && isset($flatfiledata[$x][$z]))
									{   $accountnumber = $flatfiledata[$x][$z];
										echo "<td>".$flatfiledata[$x][$z]."</td>";
									}
							}
							// -- Call to update EFT Bank Unpaid Mecantile.
							if(!empty($accountnumber)){
								$whereArray = null;
								$arrayCollections = null;
								$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
								$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
								$collectionObj = search_dynamic('collection',$whereArray);
								$whereArray = null;
								$whereArray[] = "bankresponsecode = {$statusCode}"; // -- Bank Response Code.
								$bankresponsecodeObj = search_dynamic('bankresponsecodes',$whereArray);;
								if($collectionObj->rowCount() <= 0){$message = 'no record found to update';}
								else
								{								
									foreach($bankresponsecodeObj as $row)
									{
										$arrayCollections[1] = $row['bankresponsecode'];
										$arrayCollections[2] = $row['bankresponsecodedesc'];									
									}
									foreach($collectionObj as $row)
									{$arrayCollections[3] = $row['collectionid'];}
									
									//print_r($arrayCollections);
									$message = updatecollectionStatus($arrayCollections);
								}
							}
							if(empty($message)){$message = 'success';}
							if(!empty($accountnumber)){
							echo "<td>$message</td>";}
							echo"</tr>";
						}
					echo "</table>
				</div>
			</div>";				
				}
			}
				//echo nl2br(file_get_contents($filenameEncryp)); // get the contents, and echo it out.
			?>
		</form>
</div>
</div>

<!-- </body>
</html> -->