<!-- === BEGIN CONTENT === -->
<div class="container background-white bottom-border">
		<div>
                            <!-- Login Box -->
				<form class="login-page" action="update.php?customerid=<?php echo $customerid?>&ApplicationId=<?php echo $ApplicationId?>" method="post">
					  							<div class="table-responsive">

<table class=   "table table-user-information">
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Loan Payment Details
                    </a>
                </h2>						
</td></div>							
		
</tr>		
</table>
									</div>
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div class="row">
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				<table class="table table-striped table-bordered">
		              <thead>
							<tr> 
								<th><b>Invoice ID</b></th>
								<th>Payment Date</th>
								<th><b>Amount</b></th>
								<th><b>Proof Payment</b></th>
								<th>Action</th>
		                    </tr>
		              </thead>
		              <tbody>
		              <?php 

					// -- Total Income
					$tot_outstanding = 0.00;
					$tot_paid = 0.00;
					$tot_repayment = 0.00;
					
	 				require 'database_invoice2.php';
					$customerid = null;
					$ApplicationId = null;
					$dataUserEmail = '';
					$role = '';
					$repaymentAmount = null;
							
					if ( !empty($_GET['repayment'])) 
					{
						$repaymentAmount = $_REQUEST['repayment'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$repaymentAmount = base64_decode(urldecode($repaymentAmount)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( !empty($_GET['customerid'])) 
					{
						$customerid = $_REQUEST['customerid'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$customerid = base64_decode(urldecode($customerid)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( !empty($_GET['ApplicationId'])) 
					{
						$ApplicationId = $_REQUEST['ApplicationId'];
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
						$ApplicationId = base64_decode(urldecode($ApplicationId)); 
						// -- Decode Encrypt 17.09.2017 - Parameter Data.
					}
					
					if ( null==$customerid ) 
						{
						header("Location: custAuthenticate.php");
						}
						else if( null==$ApplicationId ) 
						{
						header("Location: custAuthenticate.php");
						}
					 else 
					 {
						 
						 try
						{
					 $pdo = Database2::connect();
					 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
					 // -- Get Payment Invoices from common Tab Data.
					  $sql =  "select * from common where customer_identification = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($customerid));
					  $dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
					 
					 $data = null;
					 
			   // $dataCommon brings all the Invoices related to a client.
			   foreach($dataCommon as $rowCommon)
			   {
					 $invoice_id = $rowCommon['id'];
					 
 					 // -- Get Payment Details Data.
					  $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($invoice_id ,$ApplicationId));
					  $data = $q->fetchAll(PDO::FETCH_ASSOC);
					  
				// For each Invoice Get the Payments related to the Loan Application Id			 
					 foreach ($data as $row) 
					  {
					  $paymentid = $row['id'];
					  $iddocument = $row['FILE'];
					  $path=str_replace(" ", '%20', $iddocument);
						   		echo '<tr>';
							   	echo '<td>'.$row['invoice_id']. '</td>';								
							    echo '<td>'.$row['date']. '</td>';
							   	echo '<td>'.number_format($row['amount'],2). '</td>';
							   	echo "<td><a href=uploads/$path target='_blank'>$iddocument</a></td>";
							   	echo '<td width=150>';
							   	echo "<a class='btn btn-info' href=upload_payment.php?repayment=$repaymentAmount&customerid=$customerid&paymentid=$paymentid&ApplicationId=$ApplicationId>Upload Proof of Payment</a></td>";
							   	echo '&nbsp;';
							   	echo '</tr>';
								$tot_paid = $tot_paid + $row['amount']; 
					   }
					   		  
			}	// End all Customer Invoices
			// -- Get the curreny 
			$currency = $_SESSION['currency'];
			
			  if($data != null)
					   {
					     $tot_outstanding = $repaymentAmount - $tot_paid;
					    echo '<table class="table">';
						echo "<tr class='bg-danger'><td><b>Total Repayment Amount</b></td><td><b>$currency".number_format($repaymentAmount,2)."<b></td></tr>";
					    echo "<tr class='bg-success'><td><b>Total Paid Amount</b></td><td><b>$currency".number_format($tot_paid,2)."</b></td></tr>";
						echo "<tr  class='bg-info'><td><b>Total Outstanding Amount</b></td><td><b>$currency".number_format($tot_outstanding,2)."</b></td></tr>";
						echo '</table>';
					   }
					  
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					 
					   Database::disconnect();
						} // -- Exception
						catch (PDOException $e)
						{
							echo $e->getMessage();
						}
			}
					  ?>
				      </tbody>

</table>
			<table class=   "table table-user-information">
			<tr>
				<td><div>
<!-- IF PAGES COME FROM TotalsReport TO BACK IT IF ELSE -->
				<?php 
					$icamefrom = "";
					// -- BOC Encrypt 16.09.2017.
					if(isset($_SERVER['HTTP_REFERER']))
					{
						$icamefrom = $_SERVER['HTTP_REFERER'];
					}
					else
					{
						header("Location: custAuthenticate.php");
					}
					// -- EOC Encrypt 16.09.2017.

					if (!isset($_SESSION['icamefrom']))
					{
					  	$_SESSION['icamefrom'] = $icamefrom;
					}
					else
					{
					    $icamefrom = $_SESSION['icamefrom'];
					}
					echo "<a class='btn btn-primary' href=$icamefrom>Back</a>";
				?>
				</div></td>
				</div></td>
			</tr>
			</table>
				
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->							
 <!------------------------------ End File List ------------------------------------>
	
<!-------------------------------------- END UPLOADED DOCUMENTS ------------------------------------> 	
					</form>			
						
				
                            <!-- End Login Box -->							
                        </div>
					
                </div>
            <!-- === END CONTENT === -->