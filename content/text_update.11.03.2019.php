<!-- Start Update the eLoan - Application Data -->
<?php 
	
require 'database.php';
$tbl_customer = "customer";
$tbl_loanapp = "loanapp";

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
$paymentfrequency = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

$customerid = null;
$ApplicationId = null;
$count = 0;

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$rejectreason = "";
$rejectreasonError = "";

// -- BOC Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = "";
$ApplicationIdEncoded = "";
$location = "Location: customerLogin";
// -- EOC Encrypt 16.09.2017 - Parameter Data.
		

	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
		if ( !empty($_GET['ApplicationId'])) 
		{
			$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
			$ApplicationIdEncoded = $ApplicationId;
			$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}
	if ( null==$customerid ) 
	{
		//header("Location: custAuthenticate.php");
	}
	
	if ( null==$ApplicationId ) {
		//header("Location: custAuthenticate.php");
	}

$Original_email = '';

if ( !empty($_POST)) 
{

// keep track validation errors - 		// Customer Details
$TitleError = null;
$FirstNameError = null; 
$LastNameError = null;
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$DobError = null;
$phoneError = null;
$emailError = null;
		
// keep track validation errors - 		// Loan Details
$MonthlyExpenditureError = null; 
$TotalAssetsError = null; 
$ReqLoadValueError = null; 
$DateAccptError = null; 
$LoanDurationError = null; 
$StaffIdError = null; 
$InterestRateError = null; 
$ExecApprovalError = null; 
$MonthlyIncomeError = null;
 
$RepaymentError = null; 
$paymentmethodError = null; 
$SuretyError = null; 
$monthlypaymentError = null; 
$FirstPaymentDateError = null; 
$lastPaymentDateError = null; 

// ------------------------- Declarations - Banking Details ------------------------------------- //
$AccountHolderError = null;
$AccountTypeError = null;
$AccountNumberError = null;
$BranchCodeError = null;
$BankNameError = null;
$rejectreasonError = null;
// -------------------------------- Banking Details ------------------------------------------ // 
 
// keep track post values
// Customer Details
$Title = $_POST['Title'];
$FirstName = $_POST['FirstName'];
$LastName = $_POST['LastName'];
$Street = $_POST['Street'];
$Suburb = $_POST['Suburb'];
$City = $_POST['City'];
$State = $_POST['State'];
$PostCode = $_POST['PostCode'];
$Dob		= $_POST['Dob'];
$phone = $_POST['phone'];
$email =  $_POST['email'];
$Original_email = $_POST['Original_email'];

// Loan Details
$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
$TotalAssets  = $_POST['TotalAssets'];
$ReqLoadValue = $_POST['ReqLoadValue'];
$DateAccpt  = $_POST['DateAccpt'];
$LoanDuration  = $_POST['LoanDuration'];
$InterestRate = $_POST['InterestRate'];
$ExecApproval  = $_POST['ExecApproval'];
$MonthlyIncome  = $_POST['MonthlyIncome'];
$paymentfrequency = $_POST['paymentfrequency']; // -- 16.04.2017 - paymentfrequency.
$Repayment = $_POST['Repayment']; 
$paymentmethod = $_POST['paymentmethod']; 
$Surety = $_POST['Surety']; 
$monthlypayment = $_POST['monthlypayment']; 
$FirstPaymentDate = $_POST['FirstPaymentDate']; 
$lastPaymentDate = $_POST['lastPaymentDate']; 

// ------------------------- $_POST - Banking Details ------------------------------------- //
$AccountHolder = $_POST['AccountHolder'];
$AccountType = $_POST['AccountType'];
$AccountNumber = $_POST['AccountNumber'];
$BranchCode = $_POST['BranchCode'];
$BankName = $_POST['BankName'];
// -------------------------------- Banking Details ------------------------------------------ //

// validate input - 		// Customer Details
		$valid = true;
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $nameError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $nameError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $nameError = 'Please enter province'; $valid = false;}
		
		if (empty($PostCode)) { $nameError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter Date of birth'; $valid = false;}
		
				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		if (empty($MonthlyExpenditure)) { $nameError = 'Please enter MonthlyExpenditure'; $valid = false;}
		if (empty($TotalAssets)) { $nameError = 'Please enter Total Assets'; $valid = false;}
		if (empty($ReqLoadValue)) { $nameError = 'Please enter Req Loan Value'; $valid = false;}
		if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		
		if (!empty($ExecApproval))
		{ 
			if($ExecApproval == 'REJ')
			{
				if(isset($_POST['rejectreason']))
				{
					$rejectreason = $_POST['rejectreason'];
				}

				// -- Validate Reason -- //
				if (empty($rejectreason)) 
				{ 
					$rejectreasonError = 'Please provide the reason for rejection separated by a period. <br/>For e.g. Account in arrears.In debited.You are broke'; $valid = false;
				}
			}
			else
			{
				$rejectreasonError = null;
			}
		}
		else
		{
			$rejectreasonError = null;
		}

		if (empty($DateAccpt)) { $nameError = 'Please enter Date Accpt'; $valid = false;}
		if (empty($LoanDuration)) { $nameError = 'Please enter Loan Duration'; $valid = false;}
		if (empty($InterestRate)) { $nameError = 'Please enter Interest Rate'; $valid = false;}
		
		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// -- validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		else
		{
		// -- User - E-mail.
			if ($valid and ($Original_email != $email))
			{
				$query = "SELECT * FROM user WHERE email = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use.'; $valid = false;}
			}
		}
		
		if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($FirstPaymentDate)) { $FirstPaymentDateError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'.is_float($MonthlyExpenditure); $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter Required Loan Value'; $valid = false;}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		// update data
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE $tbl_customer c inner join $tbl_loanapp l ON c.customerid = l.customerid";
			$sql = $sql." SET Title = ?, FirstName = ?,LastName = ?,Street = ?,Suburb = ?,City = ?,State = ?,PostCode = ?,Dob = ?,";
			$sql = $sql."MonthlyIncome = ?,MonthlyExpenditure = ?,TotalAssets = ?,ReqLoadValue = ?,";
			$sql = $sql."ExecApproval =?,DateAccpt = ?,InterestRate = ?,LoanDuration = ?,";
			$sql = $sql."surety = ?,Repayment = ?,monthlypayment = ?,paymentmethod = ?,paymentfrequency = ?,";
			$sql = $sql."FirstPaymentDate = ?,lastPaymentDate = ?,";
			$sql = $sql."l.accountholdername = ?,l.bankname = ?,l.accountnumber = ?,l.branchcode = ?,l.accounttype = ?,rejectreason = ?";//Banking Details
			$sql = $sql." WHERE c.customerid = ? AND l.ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,
			$ReqLoadValue,$ExecApproval,$DateAccpt,$InterestRate,$LoanDuration,$Surety,$Repayment,$monthlypayment,$paymentmethod,$paymentfrequency, $FirstPaymentDate,
			$lastPaymentDate,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$rejectreason, //Banking Details 29.09.2016
			$customerid,$ApplicationId));
			Database::disconnect();
			$count++;

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
			// Update User Email
			$sql = "UPDATE user SET email = ? WHERE userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($email,$customerid));
			Database::disconnect();
			$count++;
///////////////////////////// ---------------------------------- SESSION DECLARATIONS --------------------------/////////
// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = '';
												
			// Id number
			$_SESSION['idnumber']  = $customerid;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;		
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			$_SESSION['Street'] = $Street;
			// Suburb
			$_SESSION['Suburb'] = $Suburb;
			// City
			$_SESSION['City'] = $City;
			// State
			$_SESSION['State'] = $State;
			// PostCode
			$_SESSION['PostCode'] = $PostCode;
			// Dob
			$_SESSION['Dob'] = $Dob;
			// phone
			$_SESSION['phone'] = $phone;	
			//------------------------------------------------------------------- 
			//           				LOAN APP DATA							-
			//-------------------------------------------------------------------			
			$_SESSION['loandate'] = $DateAccpt;// "01-08-2016";
			$_SESSION['loanamount'] = $ReqLoadValue ;// "R2000";
			$_SESSION['loanpaymentamount'] = $Repayment  ; //"R2400";
			$_SESSION['loanpaymentInterest'] = $InterestRate ;// = "20%";
			$_SESSION['FirstPaymentDate'] = $FirstPaymentDate;// = ;//'01-08-2016';
			$_SESSION['LastPaymentDate'] = $lastPaymentDate;// = '01-08-2016';
			$_SESSION['applicationdate'] = $DateAccpt;// = '01-08-2016';
			$_SESSION['status'] = $ExecApproval;// = '01-08-2016';
			$_SESSION['monthlypayment'] = $monthlypayment;// Monthly Payment Amount 
			$_SESSION['LoanDuration'] = $LoanDuration;// Loan Duration
			$_SESSION['ApplicationId'] = $ApplicationId;
			
			// ------------------------- $_SESSION - Banking Details ------------------------------------- //
			$_SESSION['AccountHolder'] = $AccountHolder;
			$_SESSION['AccountType'] = $AccountType;
			$_SESSION['AccountNumber'] = $AccountNumber ;
			$_SESSION['BranchCode'] = $BranchCode;
			$_SESSION['BankName'] = $BankName;
		   // ------------------------------- $_SESSION Banking Details ------------------------------------------ //
		   // -- start - 16.04.2017 -- Regroup maintainable tables. ---- // 
			$_SESSION['paymentfrequency'] = $paymentfrequency;
		   // -- End - 16.04.2017 -- Regroup maintainable tables.   --- // 
		    $_SESSION['rejectreason'] = $rejectreason;
		   
///////////////////////////// ---------------------------------- END SESSION DECLARATIONS ----------------------/////////			
		
			// Sent Rejected or Approved status to customer provided
			if($_SESSION['ExecApproval'] != $ExecApproval)
			{
				// It means the status has been changed.
				
				// Rejected
				if($ExecApproval == 'REJ')
				{
				  SendEmailpage('text_emailLoanStatusRejected.php');	
				}
				// Approved
				if($ExecApproval == 'APPR')
				{
					SendEmailpage('text_emailLoanStatusAccepted.php');	
				}
				
				// -- Cancelled
				if($ExecApproval == 'CAN')
				{
					SendEmailpage('text_emailLoanStatusCancelled.php');	
				}
				
				// -- Settled.
				if($ExecApproval == 'SET')
				{
					SendEmailpage('text_emailLoanStatusSettled.php');	
				}
				// -- BOC 18.11.2018.				
				// -- Defaulter.
				if($ExecApproval == 'DEF')
				{
					SendEmailpage('text_emailLoanStatusDefaulted.php');	
				}

				// -- Legal.
				if($ExecApproval == 'LEGAL')
				{
					SendEmailpage('text_emailLoanStatusLegal.php');	
				}

				// -- Rehabilitation.
				if($ExecApproval == 'REH')
				{
					SendEmailpage('text_emailLoanStatusRehabilitation.php');	
				}
				// -- EOC 18.11.2018.				
			}
		}
	} else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));		
		$data = $q->fetch(PDO::FETCH_ASSOC);

		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header("Location: custAuthenticate");
		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		// -- UserEmail Address
		$sql =  "select * from user where userid= ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid));
		$dataEmail = $q->fetch(PDO::FETCH_ASSOC);

		// Customer Details

$Title = $data['Title'];
$FirstName = $data['FirstName'];
$LastName = $data['LastName'];
$Street = $data['Street'];
$Suburb = $data['Suburb'];
$City = $data['City'];
$State = $data['State'];
$PostCode = $data['PostCode'];
$Dob		= $data['Dob'];

$phone = $data['phone'];
$email =  $dataEmail['email'];
$Original_email = $dataEmail['email'];

		// Loan Details
$MonthlyExpenditure = $data['MonthlyExpenditure'];
$TotalAssets  = $data['TotalAssets'];
$ReqLoadValue = $data['ReqLoadValue'];
$DateAccpt  = $data['DateAccpt'];
$LoanDuration  = $data['LoanDuration'];
$InterestRate = $data['InterestRate'];
$ExecApproval  = $data['ExecApproval'];
$MonthlyIncome  = $data['MonthlyIncome'];

$Repayment = $data['Repayment']; 
$Surety = $data['surety']; 
$monthlypayment = $data['monthlypayment']; 
$FirstPaymentDate = $data['FirstPaymentDate']; 
$lastPaymentDate = $data['lastPaymentDate']; 		
$_SESSION['ExecApproval'] = $ExecApproval; // Store Approval Status for later user via e-mail to customer sending.		

// ------------------------- $data - Banking Details ------------------------------------- //
$AccountHolder = $data['accountholdername'];
$paymentmethod = $data['paymentmethod']; 
$AccountType = $data['accounttype'];
$AccountNumber = $data['accountnumber'];
$BranchCode = $data['branchcode'];
$BankName = $data['bankname'];
$rejectreason = $data['rejectreason'];

// -------------------------------- Banking Details ------------------------------------------ //

// -------------------------------- Start 16.04.2017 --- Regroup all the maintainable table ---//
$paymentfrequency = $data['paymentfrequency'];
// -------------------------------- End 16.04.2017 ---- Regroup all the maintainable table ---//
		Database::disconnect();
	}
?>
<!-- End Update the eLoan - Application Data -->

<div class="container background-white bottom-border">   
		<div class="row">
 
 <!-- <div class="span10 offset1"> -->
  <form  action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
    <div class="table-responsive">
<table class=   "table table-user-information">
<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->	

<tr>
	<td><div><a class="btn btn-primary" href="custAuthenticate">Back</a></div></td>
	<td><div><button type="submit" class="btn btn-primary">Update</button></div></td>
</tr>	
<tr>
<td colspan="3"> 
<div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>
</div>	

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Client information updated successfully!</p>\n", $count);
			// Sent Rejected or Approved status to customer provided
			if($_SESSION['ExecApproval'] != $ExecApproval)
			{	
				// Rejected
				if($ExecApproval == 'REJ')
				{
					printf("<p class='status'>The e-Loan application status is rejected. A rejected e-mail is sent to customer Inbox!</p>\n");

				}
				// Approved
				if($ExecApproval == 'APPR')
				{
					printf("<p class='status'>The e-Loan application status is successfully approved. An approved e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Settled
				if($ExecApproval == 'SET')
				{
					printf("<p class='status'>The e-Loan application status is successfully settled. An comfirmation e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Cancelled
				if($ExecApproval == 'CAN')
				{
					printf("<p class='status'>The e-Loan application status has been cancelled. An cancelation e-mail is sent to customer Inbox!</p>\n");
				}
			}
		}
		// -- if there any error display here.
		else
		{
				// keep track validation errors - 		// Customer Details
if(!empty($TitleError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($FirstNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($LastNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StreetError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($SuburbError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($CityError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($PostCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($DobError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($phoneError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($emailError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
		
// keep track validation errors - 		// Loan Details
if(!empty($MonthlyExpenditureError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($TotalAssetsError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($ReqLoadValueError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($DateAccptError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($LoanDurationError)){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($StaffIdError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($InterestRateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($ExecApprovalError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($MonthlyIncomeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
 
if(!empty($RepaymentError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($paymentmethodError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($SuretyError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($monthlypaymentError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($FirstPaymentDateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($lastPaymentDateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 

// ------------------------- Declarations - Banking Details ------------------------------------- //
if(!empty($AccountHolderError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($AccountTypeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($AccountNumberError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($BranchCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($BankNameError )){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}
		}
		?>
				
</td>						
		
</tr>

<tr>						
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label">Title</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="Title" type="text"  placeholder="Title" value="<?php echo !empty($Title)?$Title:'';?>"> -->
			
			<SELECT class="form-control" id="Title" name="Title" size="1">
						<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
						<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
						<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
						<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
						<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
			</SELECT>
			
			<?php if (!empty($TitleError)): ?>
			<span class="help-inline"><?php echo $TitleError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td> 

	<td> 
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label">First Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" 
			value="<?php echo !empty($FirstName)?$FirstName:'';?>">
			<?php if (!empty($FirstNameError)): ?>
			<span class="help-inline"><?php echo $FirstNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label">Last Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
			<?php if (!empty($LastNameError)): ?>
			<span class="help-inline"><?php echo $LastNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
 
</tr>	

<!-- Street,Suburb,City -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label">Street</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
			<?php if (!empty($StreetError)): ?>
			<span class="help-inline"><?php echo $StreetError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label">Suburb</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
			<?php if (!empty($SuburbError)): ?>
			<span class="help-inline"><?php echo $SuburbError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label">City</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
			<?php if (!empty($CityError)): ?>
			<span class="help-inline"><?php echo $CityError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- State,PostCode,Dob -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label">Province</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="State" type="text"  placeholder="Province" value="<?php echo !empty($State)?$State:'';?>"> -->
			    <SELECT class="form-control" name="State" size="1">
								<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->	
				</SELECT>
			<?php if (!empty($StateError)): ?>
			<span class="help-inline"><?php echo $StateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label">Postal Code</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
			<?php if (!empty($PostCodeError)): ?>
			<span class="help-inline"><?php echo $PostCodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label">Date of birth</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  value="<?php echo !empty($Dob)?$Dob:'';?>">
			<?php if (!empty($DobError)): ?>
			<span class="help-inline"><?php echo $DobError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<tr>	
<td>
<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
</td>
<td>
<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">Email
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="email" type="text"  placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
									<p>			
				<input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
									</p>
								</div>
</td>
</tr>	

<!-----------------------------------------------------------  Banking Details ---------------------------------------->	
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>						
</td></div>							
		
</tr>
<!-- Account Holder Name,Bank Name,Account number -->	
<tr>
<td> 
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
								<label>Account Holder Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td> 	
<td>
<!-- Bank Name -->
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
								<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
</td>							
<td>
<!-- Account Number  -->
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
								 <label>Account Number</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td>
</tr>
<tr>
<td>

<!-- Account Type  -->
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
								<label>Account Type</label>
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1">
									<!-------------------- BOC 2017.04.15 -  Account type --->	
											<?php
											$accounttypeid = '';
											$accounttySelect = $AccountType;
											$accounttypedesc = '';
											foreach($dataAccountType as $row)
											{
												$accounttypeid = $row['accounttypeid'];
												$accounttypedesc = $row['accounttypedesc'];
												// Select the Selected Role 
												if($accounttypeid == $accounttySelect)
												{
												echo "<OPTION value=$accounttypeid selected>$accounttypedesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$accounttypeid>$accounttypedesc</OPTION>";
												}
											}
										
											if(empty($dataAccountType))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
									<!-------------------- EOC 2017.04.15 - Account type --->
									</SELECT>
									</div>	
								</div>
</td>
<td>								
							<!-- Branch Code  -->
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
								<label class="control-label">Branch Code</label>
									<div class="controls">
										<SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->										
										  </SELECT>	
									</div>	
								</div>
							</div>

</td>

</tr>
<!----------------------------------------------------------- End Banking Details ---------------------------------------->	

<!-- Loan Details -->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->	
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Loan Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<tr>
	<td> 
	<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
		<label class="control-label">Monthly Income</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="MonthlyIncome" type="text"  placeholder="Monthly Income" value="<?php echo !empty($MonthlyIncome)?$MonthlyIncome:'';?>">
			<?php if (!empty($MonthlyIncomeError)): ?>
			<span class="help-inline"><?php echo $MonthlyIncomeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
		<label class="control-label">Monthly Expenditure</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="MonthlyExpenditure" type="text"  placeholder="Monthly Expenditure" value="<?php echo !empty($MonthlyExpenditure)?$MonthlyExpenditure:'';?>">
			<?php if (!empty($MonthlyExpenditureError)): ?>
			<span class="help-inline"><?php echo $MonthlyExpenditureError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($TotalAssetsError)?'error':'';?>">
		<label class="control-label">Total Assets</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="TotalAssets" type="text"  placeholder="Total Assets" value="<?php echo !empty($TotalAssets)?$TotalAssets:'';?>">
			<?php if (!empty($TotalAssetsError)): ?>
			<span class="help-inline"><?php echo $TotalAssetsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- ReqLoadValue,DateAccpt,LoanDuration -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
		<label class="control-label">Requested Loan Value</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="ReqLoadValue" name="ReqLoadValue" type="text"  placeholder="Req Loan Value" value="<?php echo !empty($ReqLoadValue)?$ReqLoadValue:'';?>" onchange="CalculateRepayment()">
			<?php if (!empty($ReqLoadValueError)): ?>
			<span class="help-inline"><?php echo $ReqLoadValueError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DateAccptError)?'error':'';?>">
		<label class="control-label">Date Accepted</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="DateAccpt" type="Date" value="<?php echo !empty($DateAccpt)?$DateAccpt:'';?>">
			<?php if (!empty($DateAccptError)): ?>
			<span class="help-inline"><?php echo $DateAccptError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
		<label class="control-label">Loan Duration</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="LoanDuration" name="LoanDuration" type="text"  placeholder="Loan Duration" value="<?php echo !empty($LoanDuration)?$LoanDuration:'';?>" onchange="LastPaymentDate()">
			<?php if (!empty($LoanDurationError)): ?>
			<span class="help-inline"><?php echo $LoanDurationError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- Surety, Repayment, Monthly Payment -->
<tr>	
	<td>
		<div class="control-group <?php echo !empty($SuretyError)?'error':'';?>">
											<label class="control-label">Surety Assets(e.g. Golf GTi , iPhone 5)</label>
											<div class="controls">
												<input  style="height:30px" class="form-control margin-bottom-10" id="Surety" name="Surety" type="text"  placeholder="Surety Assets(e.g. Golf GTi , iPhone 5)" value="<?php echo !empty($Surety)?$Surety:'';?>">
												<?php if (!empty($SuretyError)): ?>
												<span class="help-inline"><?php echo $SuretyError;?></span>
												<?php endif; ?>
											</div>
		</div>
	</td>
	<td>
	 <label>Total Repayment Amount
      </label>
		<div class = "input-group">
			<span class = "input-group-addon">R</span>	  
             <input  style="height:30px" readonly id="Repayment" name="Repayment" value="<?php echo !empty($Repayment)?$Repayment:'';?>" class="form-control margin-bottom-10" type="text">
		</div>
	</td>
	<td>
		<label>Monthly Payment Amount
           </label>
			 <div class = "input-group">
			  <span class = "input-group-addon">R</span>	  
                 <input  style="height:30px" readonly id="monthlypayment" name="monthlypayment" value="<?php echo !empty($monthlypayment)?$monthlypayment:'';?>" class="form-control margin-bottom-10" type="text">
			 </div>
	</td>
	
</tr>	
<!-- Payment Method, -->
<tr>
<td>
<label>Payment method</label>
<SELECT class="form-control" id="paymentmethod" name="paymentmethod" size="1">
								<!-------------------- BOC 2017.04.15 -  Payment Method --->	
											<?php
											$paymentmethodid = '';
											$paymentmethodSelect = $paymentmethod;
											$paymentmethoddesc = '';
											foreach($dataPaymentMethod as $row)
											{
												$paymentmethodid = $row['paymentmethodid'];
												$paymentmethoddesc = $row['paymentmethoddesc'];
												// Select the Selected Role 
												if($paymentmethodid == $paymentmethodSelect)
												{
												echo "<OPTION value=$paymentmethodid selected>$paymentmethoddesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentmethodid>$paymentmethoddesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentMethod))
											{
												echo "<OPTION value=0>No Payment Method</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Method. --->
								</SELECT>                                
</td>
<td>
<div class="control-group <?php echo !empty($FirstPaymentDateError)?'error':'';?>">
								<label class="control-label">First Payment Date
									<span class="color-red">*</span>
								</label>
									<div class="controls">
									<input  style="height:30px" id="FirstPaymentDate" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="FirstPaymentDate" type="Date"  value="<?php echo !empty($FirstPaymentDate)?$FirstPaymentDate:'';?>" onchange="LastPaymentDate()">
									<?php if (!empty($FirstPaymentDateError)): ?>
									<span class="help-inline"><?php echo $FirstPaymentDateError;?></span>
									<?php endif; ?>
									</div>
								</div>
</td>							   
<td>							   
							   <label>Payment Frequency 
							   </label>
 <SELECT class="form-control" id="paymentfrequency" name="paymentfrequency" size="1">
									<!-------------------- BOC 2017.04.15 -  Payment Frequency --->	
											<?php
											$paymentfrequencyid = '';
											$paymentfrequencySelect = $paymentfrequency;
											$paymentfrequencydesc = '';
											foreach($dataPaymentFrequency as $row)
											{
												$paymentfrequencyid = $row['paymentfrequencyid'];
												$paymentfrequencydesc = $row['paymentfrequencydesc'];
												// Select the Selected Role 
												if($paymentfrequencyid == $paymentfrequencySelect)
												{
												echo "<OPTION value=$paymentfrequencyid selected>$paymentfrequencydesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentfrequencyid>$paymentfrequencydesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentFrequency))
											{
												echo "<OPTION value=0>No Payment Frequency</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Frequency --->
</SELECT>                      

</td>
</tr>
<!-- InterestRate,ExecApproval -->		
<tr>
<td>							   
							   <label>Last Payment Date 
							   </label>
                               <input  style="height:30px" readonly id="lastPaymentDate" name="lastPaymentDate"  value="<?php echo !empty($lastPaymentDate)?$lastPaymentDate:'';?>" class="form-control margin-bottom-10" type="date" />

</td>
	<td> 
	<div class="control-group <?php echo !empty($InterestRateError)?'error':'';?>">
		<label class="control-label">Interest Rate(%)</label>
		<div class="controls">
			<input readonly style="height:30px" class="form-control margin-bottom-10" name="InterestRate" type="text"  placeholder="Interest Rate" value="<?php echo !empty($InterestRate)?$InterestRate:'';?>">
			<?php if (!empty($InterestRateError)): ?>
			<span class="help-inline"><?php echo $InterestRateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label">Status</label>
		<?php 
		$status = '';
		$statusAPPR = '';
		$statusREJ = '';
		$statusPEN = '';
		$statusCAN = '';
		$statusSET = '';
		// -- Defaulter - 20.06.2018.
		$statusDEF = '';

		// -- Legal - 07.11.2018.
		$statusLEGAL = '';
		
		// -- Rehabilitation - 07.11.2018.
		$statusREH = '';
		
		if ($ExecApproval == 'APPR'){ $statusAPPR = 'selected';}
		if ($ExecApproval == 'REJ') { $statusREJ = 'selected';}
		if ($ExecApproval == 'PEN') { $statusPEN = 'selected';}
		if ($ExecApproval == 'CAN') { $statusCAN = 'selected';}
		if ($ExecApproval == 'SET') { $statusSET = 'selected';}

		// -- Legal - 07.11.2018.
		if ($ExecApproval == 'LEGAL') { $statusLEGAL = 'selected';}
		
		// -- Rehabilitation - 07.11.2018.
		if ($ExecApproval == 'REH') { $statusREH = 'selected';}
		
		// -- Defaulter - 20.06.2018.
		if ($ExecApproval == 'DEF') { $statusDEF = 'selected';}

		if($_SESSION['role'] == 'admin')
		{
		echo "<div class='control-group'>
		 <SELECT class='form-control' name='ExecApproval' size='1'>
			   <OPTION value='APPR' $statusAPPR>Approved</OPTION>
				<OPTION value='REJ' $statusREJ>Rejected</OPTION>
				<OPTION value='PEN' $statusPEN>Pending</OPTION>
				<OPTION value='CAN' $statusCAN>Cancelled</OPTION>
				<OPTION value='SET' $statusSET>Settled</OPTION>
				<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
				<OPTION value='LEGAL' $statusLEGAL>Legal</OPTION>				
				<OPTION value='REH' $statusREH>Rehabilitation</OPTION>								
		 </SELECT>
	    </div>";
		}
		else
		{
		if ($ExecApproval == 'APPR'){ $status = 'Approved';}
		if ($ExecApproval == 'REJ') { $status = 'Rejected';}
		if ($ExecApproval == 'PEN') { $status = 'Pending';}
		if ($ExecApproval == 'CAN') { $status = 'Cancelled';}
		if ($ExecApproval == 'SET') { $status = 'Settled';}
		// -- Defaulter - 20.06.2018.
		if ($ExecApproval == 'DEF') { $status = 'Defaulter';}
		// -- Legal - 07.11.2018.
		if ($ExecApproval == 'LEGAL') { $status = 'Legal';}
		// -- Rehabilitation - 07.11.2018.
		if ($ExecApproval == 'REH') { $status = 'Rehabilitation';}

		echo "<input readonly style='height:30px' class=form-control margin-bottom-10' name='ExecApprovalDisplay' type='text'  placeholder='ExecApproval' value='$status'>";
		echo "<input type='hidden' readonly style='height:30px' class='form-control margin-bottom-10' name='ExecApproval' type='text'  placeholder='ExecApproval' value='$ExecApproval'>";
		}
		?>
		
	</div>
	</td>
</tr>	
<?php
$errorClass = "";
$formClass = "form-control margin-bottom-10";
$displayError = "";

if (!empty($rejectreasonError))
{			
	$displayError =  "<span class='help-inline'>$rejectreasonError</span>";
}

if(!empty($rejectreasonError))
{
	$errorClass = 'control-group error';
}
else
{
	$errorClass = 'control-group';
}

if ($ExecApproval == 'REJ') 
{ 	
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label>Rejection Reason</label>
		<input  style='height:30px' id='rejectreason' name='rejectreason'  class=$formClass type='text'  value='".$rejectreason ."'/>
		$displayError
	</div> 
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>
<!-- Buttons -->
						<tr>
							<td><div>
								<a class="btn btn-primary" href="custAuthenticate">Back</a>
							</div>
							</td>
							<td>
							<div>
							  	<button type="submit" class="btn btn-primary">Update</button>
							</div>
							</td>
						</tr>
						</table>
						</div>
					</form>
				</div>
				
    </div> <!-- /container -->
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  
</script> 				  	