<?php 
	require 'database.php';
	
	$tenantid = null;
	$customerid = null;
	
	if ( !empty($_GET['tenantid'])) 
	{
		$tenantid = $_REQUEST['tenantid'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$tenantid = base64_decode(urldecode($tenantid)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	if (!empty($_POST)) 
	{
		// keep track post values
		$tenantid = $_POST['tenantid'];
		$customerid = $_POST['customerid'];

		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM tenantcustomers WHERE customerid = ? AND tenantid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid,$tenantid));
			Database::disconnect();
			echo("<script>location.href='unassigntenantcustomers.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	else 
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "SELECT * FROM tenantcustomers WHERE tenantid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($tenantid));		
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(empty($data))
			{
				header("Location: index");
			}
	}
	// -- EOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> deletetenantcustomer.php -->
    			<div class="span10 offset1">
	    			<form class="login-page"  class="form-horizontal" action="" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Unassign Tenant Customer
						    </a>
					    </h2>
						</div>						
				   </tr>
	    			  <input type="hidden" name="tenantid" id="tenantid" value="<?php echo $tenantid;?>"/>
					  <input type="hidden" name="customerid" id="customerid" value="<?php echo $customerid;?>"/>
					  <p class="alert alert-error">Are you sure you want to unassign this tenant customer?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="unassigntenantcustomers.php">No</a>
						</div>
					</form>
				</div>
</div> <!-- /container -->
