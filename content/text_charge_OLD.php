<?php
include 'database.php';

// --------------------------------- BOC 27.05.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.05.15 ------------------------- //
//1. --------------------- Charge Type ------------------- //
  $sql = 'SELECT * FROM chargetype ORDER BY name';
  $dataChargeType = $pdo->query($sql);						   						 
	
//2. --------------------- Charge Option ------------------- //
  $sql = 'SELECT * FROM chargeoption ORDER BY chargeoptionname';
  $dataChargeOption = $pdo->query($sql);						   						 

//3. ---------------------  Loan Type ------------------- //
  $sql = 'SELECT * FROM loantype ORDER BY loantypedesc';
  $dataLoanType = $pdo->query($sql);						   						 

//4. ---------------------  Payment Method ------------------- //
  $sql = 'SELECT * FROM paymentmethod ORDER BY paymentmethoddesc';
  $q = $pdo->prepare($sql);
  $q->execute();
  $dataPaymentMethod = $q->fetchAll();//$pdo->query($sql);						   						 
  
Database::disconnect();	
// --------------------------------- EOC 27.05.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="user"; // Table name 
 $chargename = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search Charges</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='chargename' name='chargename' placeholder='Charge' class='form-control' type='text' value=$chargename>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $chargename = $_POST['chargename'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($chargename))
					    {
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM charge WHERE chargename = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($chargename));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM charge";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newcharge.php>Add New Charge</a><p></p>"; 
						echo "<table class='table table-striped table-bordered' style='white-space: nowrap;'>"; 
						echo "<tr>"; 
						echo "<td><b>Charge ID</b></td>"; 
						echo "<td><b>Charge Name</b></td>"; 
						echo "<td><b>Charge Type</b></td>"; 
						echo "<td><b>Charge Option</b></td>"; 
						echo "<td><b>Loan Type</b></td>"; 
						echo "<td><b>Payment Method</b></td>"; 
						echo "<td><b>Active</b></td>"; 
						echo "<td><b>Percentage</b></td>"; 
						echo "<td><b>Amount</b></td>"; 
						echo "<td><b>Min(Days)</b></td>"; 
						echo "<td><b>Max(Days)</b></td>"; 
						echo "<td><b>Grace Period(Days)</b></td>"; 						
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						$ChargeType = '';
						$ChargeOption = '';
						$LoanType = '';
						$PaymentMethod = '';
						// -- Two Dimensional Array - ChargeType 
						$ArraChargeType = null;
						$ArraChargeOption = null;
						$ArraLoanType = null;
						$ArraPaymentMethod = null;

						$index = 0;
						$indexInside = 0;
						
						//1. -- Charge Type $dataChargeType - Execute Once.
							foreach($dataChargeType as $rowChargeType)
							{
								$ArraChargeType[$indexInside][0] = $rowChargeType['chargetypeid'];
								$ArraChargeType[$indexInside][1] = $rowChargeType['name'];
								$indexInside = $indexInside + 1;
							}
						$indexInside = 0;
						
						//2. -- Charge Option
							foreach($dataChargeOption as $rowChargeOption)
							{
								$ArraChargeOption[$indexInside][0] = $rowChargeOption['chargeoptionid'];
								$ArraChargeOption[$indexInside][1] = $rowChargeOption['chargeoptionname'];
								$indexInside = $indexInside + 1;
							}			
						$indexInside = 0;
							
						//3. -- Loan Type
							foreach($dataLoanType as $rowLoanType)
							{
								$ArraLoanType[$indexInside][0] = $rowLoanType['loantypeid'];
								$ArraLoanType[$indexInside][1] = $rowLoanType['loantypedesc'];
								$indexInside = $indexInside + 1;
							}
						$indexInside = 0;	
						
						//4. -- PaymentMethod
						foreach($dataPaymentMethod as $rowPaymentMethod)
							{
								$ArraPaymentMethod[$indexInside][0] = $rowPaymentMethod['paymentmethodid'];
								$ArraPaymentMethod[$indexInside][1] = $rowPaymentMethod['paymentmethoddesc'];
								$indexInside = $indexInside + 1;
							}
							
						foreach($data as $row)
						{ 
							//1. -- Charge Types Names
							for ($i=0;$i<sizeof($ArraChargeType);$i++)
							{
								if($ArraChargeType[$i][0] == $row['chargetypeid'])
								{
									$ChargeType = $ArraChargeType[$i][1];
								}
							}
							
						//2. -- Charge Option Name
							for ($i=0;$i<sizeof($ArraChargeOption);$i++)
							{
								if($ArraChargeOption[$i][0] == $row['chargeoptionid'])
								{
									$ChargeOption = $ArraChargeOption[$i][1];
								}
							}
							
						//3. -- Loan Type
							for ($i=0;$i<sizeof($ArraLoanType);$i++)
							{
								if($ArraLoanType[$i][0] == $row['loantypeid'])
								{
									$LoanType = $ArraLoanType[$i][1];
								}
							}
						//4. -- Payment Method.
						
						for ($i=0;$i<sizeof($ArraPaymentMethod);$i++)
							{
								if($ArraPaymentMethod[$i][0] == $row['paymentmethodid'])
								{
									$paymentmethod = $ArraPaymentMethod[$i][1];
								}
							}
						//5. -- Active CheckBox 
						$active = '';
						if(!empty($row['active'])){$active = "<input style='height:30px' type='checkbox' id='active' checked disabled/>";}
						else{$active ="<input style='height:30px' type='checkbox' id='active' disabled/>";} 
						
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['chargeid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['chargename']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $ChargeType) . "</td>";  
						echo "<td valign='top'>" . nl2br( $ChargeOption) . "</td>";  						
						echo "<td valign='top'>" . nl2br($LoanType) . "</td>";  						
						echo "<td valign='top'>" . nl2br($paymentmethod) . "</td>";  						
						echo "<td valign='top'>" . $active. "</td>";  			
						
						echo "<td valign='top'>" . nl2br( $row['percentage']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['amount']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['min']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['max']) . "</td>";  						
						echo "<td valign='top'>" . nl2br( $row['graceperiod']) . "</td>";  						
						echo "<td valign='top'><a class='btn btn-success' href=editcharge.php?chargeid={$row['chargeid']}>Edit</a></td>";
						// -- Never Delete System Default Charges 1,2,3,4.
						if($row['chargeid'] != 1 AND $row['chargeid'] != 2 AND $row['chargeid'] != 3 AND $row['chargeid'] != 4)
						{echo "<td><a class='btn btn-danger' href=deletecharge.php?chargeid={$row['chargeid']}>Delete</a></td> ";} 
						else
						{echo "<td><p>Default Charge</p></td>";}
						
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->