<?php
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($_SESSION['role'] != 'admin')
{	
	//header("Location: customerLogin");
}

$FromDate= '';
$ToDate='';
$sessionrole='';

$To = '';
$From = '';

 $Allstatus = '';
 $ExecApproval    = '';

 $AllServiceMode = '';
 $ServiceMode    = '';

 $AllServiceType = '';
 $ServiceType    = '';

 $customerid = '';
 $customeridTemp = '';

// -- Generate Client ID from AccountNumber.
if(!function_exists('GenerateClientId'))	
{
	function GenerateClientId($customerid)
	{
	   $AccountNumber = '';

	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);

		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9);
	   }
	   return $AccountNumber;
	}
}
if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

// -- Filter Records.
if ( !empty($_POST)) 
{
	// -- Date Range.
	$From = getpost('From');
	$To   = getpost('To');
	// -- Status.
	$ExecApproval   = getpost('ExecApproval');
	// -- Account revered to be customerid.
	$customerid = getpost('customerid');
	if(!empty($customerid))
	{
		$customeridTemp = getpost('customerid');		
		$customerid = GenerateClientId($customerid);
	}
}
else
{
	/*$sql = "SELECT * FROM loanapp as LA join customer as C on LA.CustomerId = C.CustomerId where ExecApproval NOT IN ('REH') order by DateAccpt desc ";
	$q = $pdo->prepare($sql);
	$q->execute();	
	$data = $q->fetchAll(); 
	Database::disconnect();*/ 
}

// -- Reload the elements of the page.
	 $Approved = '';
	 $Pending = '';
	 $Cancelled = '';
	 $Rejected = '';
	 $All = '';
	 $Settled = '';
	 $Defaulted = '';
	 $REH = '';
	 $LEGAL = '';

	if($ExecApproval == 'SET')
		{
		  $Settled = 'selected';
		}

	if($ExecApproval == 'APPR')
		{
		  $Approved = 'selected';
		}
	if($ExecApproval == 'PEN')
		{
		   $Pending = 'selected';
		}
	if($ExecApproval == 'CAN')
		{
			$Cancelled = 'selected';
		}
	if($ExecApproval == 'REJ')
		{
		   $Rejected = 'selected';
		}
	if($ExecApproval == 'All')
		{
		   $All = 'selected';
		}

// -- Defaulter - 20.06.2018.		
	if($ExecApproval == 'DEF')
		{
		   $Defaulted = 'selected';
		}	
	
// -- Legal & Rehab - 13.12.2018.
	if($ExecApproval == 'REH')
		{
		   $REH = 'selected';
		}	
		
		if($ExecApproval == 'LEGAL')
		{
		   $LEGAL = 'selected';
		}	
		
	if($ExecApproval == '')
		{
		   $All = 'selected';
		}
		
	  if($To == "")
	   {
		$To = "9999-12-31";
	   }		
// -- Apply the actual filter/search.
$tablename = 'loanapp';
$dbnameobj = 'ecashpdq_eloan';
$whereArray = null;	
$queryFields[] = '*';

// -- status
if(!empty($ExecApproval))
{
	$whereArray[] = "ExecApproval = '{$ExecApproval}'";
}
else
{
	$whereArray[] = "ExecApproval NOT IN ('REH')";
}

// -- customerid
if(!empty($customerid))
{	
	$whereArray[] = "CustomerId LIKE '%{$customerid}'";
	$customerid = $customeridTemp;

}
// from date
if(!empty($From))
{
	// 03.06.2021
	//$whereArray[] = "DATE(DateAccpt) BETWEEN '{$From}' AND '{$To}'"; // -- Client Reference.	
}
// -- dynamic search.
$dataQ = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);

$data = $dataQ->fetchAll(PDO::FETCH_ASSOC);
//print_r($data);
// -- if record were brought.
// -- build a list of customerid.
$customerid_list = '';
foreach($data as $row)
{
	if(empty($customerid_list))
	{	$customerid_list = "'".$row['CustomerId']."'";}
    else
	{	$customerid_list = $customerid_list.","."'".$row['CustomerId']."'";}
}
// -- dynamic search for customer data.
//print($customerid_list);
$dataCustomer = null;
$dataCustomerQ = null;

if(!empty($customerid_list))
{
	$tablename = 'customer';
	$whereArray = null;	
	$queryFields = null;
	$queryFields[] = 'CustomerId';
	$queryFields[] = 'Title';
	$queryFields[] = 'FirstName';
	$queryFields[] = 'LastName';
	$whereArray[] = "CustomerId IN ({$customerid_list})";
	//print_r($whereArray);
	$dataCustomerQ = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
	$dataCustomer = $dataCustomerQ->fetchAll(PDO::FETCH_ASSOC);	
}
//print_r($dataCustomer);

//print_r($dataCustomer);

// -- Get Loan Charged Fees
if(!function_exists('getloanchargedfees'))	
{
	function getloanchargedfees($appid,$pdo)
	{
			$sql = "SELECT sum(amount) as amount FROM loanappcharges as LC join charge as C on LC.chargeid = C.chargeid  where applicationid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($appid));	
			$data = $q->fetch(PDO::FETCH_ASSOC);
			
			if(isset($data['amount']))
			{
				return $data['amount'];
			}
			else
			{
				return 0.00;
			}
			Database::disconnect();
	}
}
// -- Generate AccountNumber.
if(!function_exists('GenerateAccountNumber'))	
{
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';

	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);

		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9);
	   }
	   return $AccountNumber;
	}
}

?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">

  <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
    <h2 align="center">Loans Financial Year Report</h2>
    </div>
      <div class='row'>
	    <div class='input-group margin-bottom-20'>
            <span class='input-group-addon'>
                <i class='fa fa-user'></i>
            </span>
            <input style='height:30px;z-index:0' id='customerid' name='customerid' placeholder='Account' class='form-control' type='text' value=<?php echo $customerid;?>>
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>
			<SELECT class="form-control" id="ExecApproval" name="ExecApproval" size="1" style='z-index:0'>
				<OPTION value=''    <?php echo $All;?>>All</OPTION>
				<OPTION value='APPR' <?php echo $Approved;?>>Approved</OPTION>
				<OPTION value='REJ'  <?php echo $Rejected;?>>Rejected</OPTION>
				<OPTION value='PEN'  <?php echo $Pending;?>>Pending</OPTION>
				<OPTION value='CAN'  <?php echo $Cancelled;?>>Cancelled</OPTION>
				<OPTION value='SET'  <?php echo $Settled;?>>Settled</OPTION>
				<OPTION value='DEF'  <?php echo $Defaulted;?>>Defaulter</OPTION>
				<OPTION value='REH'  <?php echo $REH;?>>Rehabilitation</OPTION>
				<OPTION value='LEGAL'<?php echo $LEGAL;?>>Legal</OPTION>												
			</SELECT>
			</div>
		</div> 		
		<p></p>
		<h3 align="center">Payments Date range Search</h3>		
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="From" name='From' placeholder='Date From' class='form-control' type='Date' value="<?php echo $From;?>">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="To" name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo $To;?>">
        </div>
		

   <div class='row'>
        <div class='col-md-7'>
            <button class='btn btn-primary pull-right' type='submit' onclick="mySearch">Search</button>
        </div>
    </div>
 </div>
      </form>
    </div>
  </div>
</br>
</br>
<div class="table-responsive">
<table id="collections" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
		      <th>Application ID</th>		  		  		  
		      <th>Account</th>		  		  
		      <th>Applied Date</th>		  
              <th>Consumer Name</th>
              <th>Loan Value(Without interest) </th>
              <th>Paid amount</th>			  
              <th>Oustanding amount</th>
              <th>Fees</th>			  
              <th>Status</th>
              <th>Notification</th>
              <th>Lending Percentage</th>
              <th>Start Date</th>
              <th>End Date</th>			  
              <th>Comments</th>	
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="4" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>
      </tfoot>
      <?php
	  $statusNoOustanding = array("CAN", "REJ", "PEN");	//"SET"
	  $statusText = '';
	  $comments   = '';
	  $mindate = $From;
	  $maxdate = $To;

	  $account = '';	
	  $ApplicationId = '';
	    foreach($data as $row)
        {	
		
		   switch($row["ExecApproval"])
		   {
			   case 'SET':
			   $statusText = 'Settled';
			   break;
			    case 'APPR':
			   $statusText = 'Approved';
			   break;
			    case 'REJ':
			   $statusText = 'Rejected';
			   $comments = $row["rejectreason"];
			   break;
			    case 'PEN':
			   $statusText = 'Pending';
			   break;
			    case 'CAN':
			   $statusText = 'Cancelled';
			   break;
			   case 'DEF':
			   $statusText = 'Defaulter';
			   break;
			   case 'REH':
			   $statusText = 'Rehabilitation';
			   break;
			   case 'LEGAL':
			   $statusText = 'Legal';
			   break;
			   default:
			   $statusText = $row["ExecApproval"];
			   break;
		   }
			$ApplicationId = $row['ApplicationId'];
			$totalfeescharged = '0.00';
		    $totalOutstanding = '0.00';
			$loanvalue = $row["Repayment"];
			$loanvalueNoInt = $row["ReqLoadValue"];
			$notifications = 'email';
			$percentage = number_format($row["InterestRate"], 2, '.', '');
			$return = 'totalpaidamount';
			$totalpaidamount = '0.00';
			// -- status
			if(in_array($row["ExecApproval"],$statusNoOustanding))
		    {
				if($row["ExecApproval"] == 'SET')
				{
					$totalpaidamount = $loanvalue;
				    $totalfeescharged = getloanchargedfees($row['ApplicationId'],$pdo);	
				    $totalOutstanding = ($loanvalue - $totalpaidamount) + $totalfeescharged;					
				}
		    }
			else
			{
				// -- 03.06.2021
				// -- commented out - $totalpaidamount =  application_payments($row['CustomerId'],$row['ApplicationId'],$loanvalue,$return);
				$paymentArray =  application_payments_with_date($row['CustomerId'],$row['ApplicationId'],$loanvalue,$return,$mindate,$maxdate);
				$totalpaidamount = $paymentArray[0];
				$comments = $paymentArray[1];
				// -- 03.06.2021				
				$totalpaidamount = str_replace(',','',$totalpaidamount);				
				$totalfeescharged = getloanchargedfees($row['ApplicationId'],$pdo);
				$totalOutstanding = ($loanvalue - $totalpaidamount) + $totalfeescharged;
			}
			
			$totalOutstanding = number_format((float)$totalOutstanding,2);
			$totalOutstanding = str_replace(',','',$totalOutstanding);	
			$totalfeescharged = number_format($totalfeescharged, 2, '.', '');
			$account = GenerateAccountNumber($row['CustomerId']);
			// -- Customer Data.
			$Title = '';
			$FirstName = '';
			$LastName = '';
			foreach($dataCustomer as $row1)
			{
				//print_r($row1);
				if($row1['CustomerId'] == $row['CustomerId'])
				{
					$Title 	   = $row1["Title"];
					$FirstName = $row1["FirstName"];
					$LastName  = $row1["LastName"];
				 break;
				}
			}
			
          echo '
          <tr>
            <td width="10%">'.$ApplicationId.'</td>		  		  		  
            <td width="10%">'.$account.'</td>		  		  
            <td width="10%">'.$row["DateAccpt"].'</td>		  
            <td width="10%">'.$Title.' '.$FirstName.' '.$LastName.'</td>
            <td width="10%">'.$loanvalueNoInt.'</td>
            <td width="10%">'.$totalpaidamount.'</td>
            <td width="10%">'.$totalOutstanding.'</td>			
            <td width="5%">'.$totalfeescharged.'</td>						
            <td width="5%">'.$statusText.'</td>
            <td width="5%">'.$notifications.'</td>
            <td width="10%">'.$percentage.'%'.'</td>
            <td width="10%">'.$row["FirstPaymentDate"].'</td>
            <td width="10%">'.$row["lastPaymentDate"].'</td>
            <td width="10%">'.$comments.'</td>
          </tr>
          ';
		  
		  
        }
      ?>
  </table>
    </div>
    </br>
</div>

<script>

$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[9]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,
order: [[ 0, "desc" ]],
buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 4)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            // Update footer
      $( api.column( 4 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

    }


});

/*table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });*/

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});

// #myInput is a <input type="text"> element

</script>
