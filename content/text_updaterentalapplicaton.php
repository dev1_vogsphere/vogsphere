<!-- Start Update the eLoan - Application Data -->
<?php 
	
require 'database.php';
$tbl_customer = "customer";
$tbl_loanapp = "rentalapp";

if(!function_exists('emailapplicant'))
{
function emailapplicant($ExecApproval)
{
	// Sent Rejected or Approved status to customer provided
	if($_SESSION['ExecApproval'] != $ExecApproval)
	{
		// It means the status has been changed.
		
		// Rejected
		if($ExecApproval == 'REJ')
		{
		  SendEmailpage('content/text_emailLoanStatusRejected.php');	
		}
		// Approved
		if($ExecApproval == 'APPR')
		{
			SendEmailpage('content/text_emailLoanStatusAccepted.php');	
		}
		
		// -- Cancelled
		if($ExecApproval == 'CAN')
		{
			SendEmailpage('content/text_emailLoanStatusCancelled.php');	
		}
		
		// -- Settled.
		if($ExecApproval == 'SET')
		{
			SendEmailpage('content/text_emailLoanStatusSettled.php');	
		}
		// -- BOC 18.11.2018.				
		// -- Defaulter.
		if($ExecApproval == 'DEF')
		{
			SendEmailpage('content/text_emailLoanStatusDefaulted.php');	
		}

				// -- Legal.
				if($ExecApproval == 'LEGAL')
				{
					SendEmailpage('content/text_emailLoanStatusLegal.php');	
				}

				// -- Rehabilitation.
				if($ExecApproval == 'REH')
				{
					SendEmailpage('content/text_emailLoanStatusRehabilitation.php');	
				}
				// -- EOC 18.11.2018.				
			}
}
}
if(!function_exists('applicationFees'))
{
	function applicationFees($applicationFeeText,$values)
	{
		// -- Applicants Fees
		$rentalEFTInfo = $applicationFeeText[0];
		$rentalEFTInfo = str_replace('[amount]',$values['rentalapplicationfee'],$rentalEFTInfo);
		$rentalEFTInfo = str_replace('[email]',$values['rentalemail'],$rentalEFTInfo);
		
		$rentalDebitOrderInfo = $applicationFeeText[1];
		$rentalDebitOrderInfo = str_replace('[amount]',$values['rentalapplicationfee'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[email]',$values['rentalemail'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[debitorderdate]',$values['appfeedebitorderdate'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[AccountNumber]',$values['AccountNumber'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[AccountType]',$values['AccountType'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[Accountholder]',$values['AccountHolder'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[title]',$values['Title'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[name]',$values['FirstName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[surname]',$values['LastName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[ID]',$values['idnumber'],$rentalDebitOrderInfo);

		$rentalDebitOrderInfo = str_replace('[BankName]',$values['BankName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[BranchCode]',$values['BranchCode'],$rentalDebitOrderInfo);

		$applicationFeeText = null;
		$applicationFeeText[] = $rentalEFTInfo;
		$applicationFeeText[] = $rentalDebitOrderInfo;
		
		return $applicationFeeText;
	}
}
if(!function_exists('update_rentalapplication'))
{
	function update_rentalapplication($dataCustomer)
	{
		$tbl_loanapp = 'rentalapp';
		$tbl_customer = 'customer';
		//echo $dataCustomer['Title'];
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
		$sql = "UPDATE $tbl_customer c inner join $tbl_loanapp l ON c.customerid = l.customerid";
		$sql = $sql." SET l.Title = ?, l.FirstName = ?,l.LastName = ?,l.Street = ?,l.Suburb = ?,l.City = ?,l.province = ?,l.PostCode = ?,l.Dob = ?,";
		$sql = $sql."ExecApproval =?,optpayments = ?,";
		$sql = $sql."appfeedebitorderdate = ?,l.accountholder = ?,l.bankname = ?,l.accountnumber = ?,l.branchcode = ?,l.accounttype = ?,rejectreason = ?";//Banking Details
		$sql = $sql." WHERE c.customerid = ? AND l.rentalappid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataCustomer['Title'],$dataCustomer['FirstName'],$dataCustomer['LastName'],$dataCustomer['Street'],$dataCustomer['Suburb'],$dataCustomer['City'],$dataCustomer['province'],$dataCustomer['PostCode'],$dataCustomer['Dob'],
		$dataCustomer['ExecApproval'],$dataCustomer['optpayments'],
		$dataCustomer['appfeedebitorderdate'],$dataCustomer['AccountHolder'],$dataCustomer['BankName'],$dataCustomer['AccountNumber'],$dataCustomer['BranchCode'],$dataCustomer['AccountType'],$dataCustomer['rejectreason'], 
		$dataCustomer['customerid'],$dataCustomer['rentalappid']));
		Database::disconnect();
	}
}
$property = '';

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

$Account_TypeArr = array();
foreach($dataAccountType as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

$customerid = null;
$ApplicationId = null;
$count = 0;

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$rejectreason = "";
$rejectreasonError = "";

// -- BOC Encrypt 16.09.2017 - Parameter Data.
$CustomerIdEncoded = "";
$ApplicationIdEncoded = "";
$location = "Location: customerLogin";
// -- EOC Encrypt 16.09.2017 - Parameter Data.
		

	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
		if ( !empty($_GET['ApplicationId'])) 
		{
			$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
			$ApplicationIdEncoded = $ApplicationId;
			$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}
	if ( null==$customerid ) 
	{
		//header("Location: custAuthenticate.php");
	}
	
	if ( null==$ApplicationId ) {
		//header("Location: custAuthenticate.php");
	}

$Original_email = '';
$applicationfeeTextValue		= '';
$appfeedebitorderdate			= '';

$appfeedebitorderdateError		= '';
$applicationfeeTextValueError	= '';

  $sql = 'SELECT * FROM rentalproperty';
  $datarentalproperty = $pdo->query($sql);						   						 
  $propertyArr = array();
	foreach($datarentalproperty as $row)
	{
		$propertyArr[] = $row['id'].'|'.$row['rentalpropertyname'].','.$row['rentalpropertytype'].','.$row['rentalpropertyprice'].','.$row['rentalpropertycity'];
	}
$ExecApproval = '';


$rentalapplicationfee = '150.00';
$rentalemail		  = 'info@intelliproperty,com'; 

$rentalEFTInfo
= 'Because you opted for payment option EFT, please email proof of application Fee of R[amount] payment to [email] in order to process your application.
Please use ID number as a reference. Please note Application Fee of R[amount] is non-refundable. see terms & conditions below for more details.';

$rentalDebitOrderInfo
 = 'Because you opted for payment option Debit Order, please authorize below:
I [title] [name] [surname] with ID : [ID], hereby authorize a debit order payment of R[amount] on [debitorderdate]
from Bank :[BankName] Branch:[BranchCode] , Account Number:[AccountNumber] , Account Type :[AccountType] , Account holder :[Accountholder] . 
Due to fraud your Bank will send a you a debicheck confirmation prio to the debit order for 
approval or rejection of the request. Please If you did not authorize this please reject the request.   
Please note Application Fee of R[amount] is non-refundable. Please ensure that there are enough funds in your account.
see terms & conditions below for more details.';   

$rentalEFTInfo = str_replace('[amount]',$rentalapplicationfee,$rentalEFTInfo);
$rentalEFTInfo = str_replace('[email]',$rentalemail,$rentalEFTInfo);

$rentalDebitOrderInfo = str_replace($rentalapplicationfee,'[amount]',$rentalDebitOrderInfo);
$rentalDebitOrderInfo = str_replace($rentalemail,'[email]',$rentalDebitOrderInfo);

$applicationFeeText = array();
$applicationFeeText[] = $rentalEFTInfo;
$applicationFeeText[] = $rentalDebitOrderInfo;

if (!empty($_POST)) 
{

// keep track validation errors - 		// Customer Details
$TitleError = null;
$FirstNameError = null; 
$LastNameError = null;
$StreetError = null;
$SuburbError = null;
$CityError = null;
$StateError = null;
$PostCodeError = null;
$DobError = null;
$phoneError = null;
$emailError = null;
		$property = getpost('property');
// keep track validation errors - 		// Loan Details
$DateAccptError = null; 
$StaffIdError = null; 
$ExecApprovalError = null; 
$paymentmethodError = null; 

// ------------------------- Declarations - Banking Details ------------------------------------- //
$AccountHolderError = null;
$AccountTypeError = null;
$AccountNumberError = null;
$BranchCodeError = null;
$BankNameError = null;
$rejectreasonError = null;
// -------------------------------- Banking Details ------------------------------------------ // 
 
// keep track post values
// Customer Details
$Title = $_POST['Title'];
$FirstName = $_POST['FirstName'];
$LastName = $_POST['LastName'];
$Street = $_POST['Street'];
$Suburb = $_POST['Suburb'];
$City = $_POST['City'];
$State = $_POST['State'];
$PostCode = $_POST['PostCode'];
$Dob		= $_POST['Dob'];
$phone = $_POST['phone'];
$email =  $_POST['email'];
$Original_email = $_POST['Original_email'];

$ExecApproval  = $_POST['ExecApproval'];
$paymentmethod = $_POST['paymentmethod']; 
//$DateAccpt  = $_POST['DateAccpt'];

// Loan Details
/*
$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
$TotalAssets  = $_POST['TotalAssets'];
$ReqLoadValue = $_POST['ReqLoadValue'];
$LoanDuration  = $_POST['LoanDuration'];
$InterestRate = $_POST['InterestRate'];
$MonthlyIncome  = $_POST['MonthlyIncome'];
$paymentfrequency = $_POST['paymentfrequency']; // -- 16.04.2017 - paymentfrequency.
$Repayment = $_POST['Repayment']; 
$Surety = $_POST['Surety']; 
$monthlypayment = $_POST['monthlypayment']; 
$FirstPaymentDate = $_POST['FirstPaymentDate']; 
$lastPaymentDate = $_POST['lastPaymentDate']; */

// ------------------------- $_POST - Banking Details ------------------------------------- //
$AccountHolder = $_POST['AccountHolder'];
$AccountType = $_POST['AccountType'];
$AccountNumber = $_POST['AccountNumber'];
$BranchCode = $_POST['BranchCode'];
$BankName = $_POST['BankName'];

		$valid = true;
$applicationfeeTextValue		= getpost('applicationfeeTextValue');
$applicationfeeTextValueError	= '';
if (empty($applicationfeeTextValue)) { $appfeedebitorderdateError = 'required'; $valid = false;}
$appfeedebitorderdate			= getpost('appfeedebitorderdate');
$appfeedebitorderdateError		= '';
if (empty($appfeedebitorderdate)) { $appfeedebitorderdateError = 'required'; $valid = false;}
// -------------------------------- Banking Details ------------------------------------------ //

		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}

		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter Date of birth'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($ExecApproval)) { $ExecApprovalError = 'Please enter status'; $valid = false;}

		if (!empty($ExecApproval))
		{ 
			if($ExecApproval == 'REJ')
			{
				if(isset($_POST['rejectreason']))
				{
					$rejectreason = $_POST['rejectreason'];
				}

				// -- Validate Reason -- //
				if (empty($rejectreason)) 
				{ 
					$rejectreasonError = '<span color="red">Please provide the reason for rejection separated by a period. <br/>For e.g. Account in arrears.In debited.You are broke</span>'; $valid = false;
				}
			}
			else
			{
				$rejectreasonError = null;
			}
		}
		else
		{
			$rejectreasonError = null;
		}

		//if (empty($DateAccpt)) { $nameError = 'Please enter Date Accpt'; $valid = false;}
		
		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// -- validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		else
		{
		// -- User - E-mail.
			if ($valid and ($Original_email != $email))
			{
				$query = "SELECT * FROM user WHERE email = ?";
				$q = $pdo->prepare($query);
				$q->execute(array($email));
				if ($q->rowCount() >= 1)
				{$emailError = 'E-mail Address already in use.'; $valid = false;}
			}
		}
		// -- echo 'valid :'.$valid;

		/*if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($FirstPaymentDate)) { $FirstPaymentDateError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'.is_float($MonthlyExpenditure); $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter Required Loan Value'; $valid = false;}
		*/
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		// -- Database PostCode		
		$values = array();
		$values['rentalemail'] = $rentalemail ;
		$values['payer'] = '' ;
		$values['rentalapplicationfee'] = $rentalapplicationfee ;

		$values['appfeedebitorderdate'] = $appfeedebitorderdate;
		$values['BankName'] 	 = $BankName ;
		$values['BranchCode'] 	 = $BranchCode ;
		$values['AccountNumber'] = $AccountNumber ;
		$values['AccountHolder'] = $AccountHolder ;
		$values['Title'] = $Title ;
		$values['idnumber'] = $customerid;
		$values['FirstName'] = $FirstName ;
		$values['LastName'] = $LastName ;

	// -- Determine Account Description.
	foreach($Account_TypeArr as $row)
	{
	  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
	  if($rowData[0] == $AccountType)
	  { $values['AccountType'] = $rowData[1];}
	}
	
	if(!isset($values['AccountType'] ))
	{
		$values['AccountType']  = '';
	}

		
		$applicationFeeText = applicationFees($applicationFeeText,$values);
		if( $paymentmethod == "DebitOrder" || $paymentmethod == 'DTO')
		{
			$applicationfeeTextValue  = $applicationFeeText[1];
		}
		else
		{
			$applicationfeeTextValue  = $applicationFeeText[0];
		}
		
		// update data
		if ($valid) 
		{
			$datatoUpdate = array();
			$datatoUpdate['Title'] = $Title;
			$datatoUpdate['FirstName'] = $FirstName;
			$datatoUpdate['LastName'] = $LastName;
			$datatoUpdate['Street'] = $Street;
			$datatoUpdate['Suburb'] = $Suburb;
			$datatoUpdate['City'] = $City;
			$datatoUpdate['PostCode'] = $PostCode;
			$datatoUpdate['Dob'] = $Dob;
			$datatoUpdate['optpayments'] = $paymentmethod;
			$datatoUpdate['AccountNumber'] = $AccountNumber;
			$datatoUpdate['AccountHolder'] = $AccountHolder;
			$datatoUpdate['BankName'] = $BankName;
			$datatoUpdate['BranchCode'] = $BranchCode;
			$datatoUpdate['AccountType'] = $AccountType;
			$datatoUpdate['rejectreason'] = $rejectreason;
			$datatoUpdate['rentalappid'] = $ApplicationId;
			$datatoUpdate['customerid'] = $customerid;
			$datatoUpdate['appfeedebitorderdate'] = $appfeedebitorderdate;
			$datatoUpdate['ExecApproval'] = $ExecApproval ;
			$datatoUpdate['province'] = $State ;
			
						
			$datatoUpdate['customerid'] = $customerid;

			update_rentalapplication($datatoUpdate);
			$count++;
			
///////////////////////////// ---------------------------------- SESSION DECLARATIONS --------------------------/////////
// --------------------------- Session Declarations -------------------------//
	$_SESSION['Title'] = $Title;
	$_SESSION['FirstName'] = $FirstName;
	$_SESSION['LastName'] = $LastName;
	$_SESSION['password'] = '';
										
	// Id number
	$_SESSION['idnumber']  = $customerid;
	$clientname = $Title.' '.$FirstName.' '.$LastName ;
	$suretyname = $clientname;
	$name = $suretyname;		
	// Phone numbers
	$_SESSION['phone'] = $phone;
	// Email
	$_SESSION['email'] = $email ;
	// Street
	$_SESSION['Street'] = $Street;
	// Suburb
	$_SESSION['Suburb'] = $Suburb;
	// City
	$_SESSION['City'] = $City;
	// State
	$_SESSION['State'] = $State;
	// PostCode
	$_SESSION['PostCode'] = $PostCode;
	// Dob
	$_SESSION['Dob'] = $Dob;
	// phone
	$_SESSION['phone'] = $phone;	
	//------------------------------------------------------------------- 
	//           				LOAN APP DATA							-
	//-------------------------------------------------------------------			
	//$_SESSION['applicationdate'] = $DateAccpt;// = '01-08-2016';
	$_SESSION['status'] = $ExecApproval;// = '01-08-2016';
	$_SESSION['ApplicationId'] = $ApplicationId;
	
	// ------------------------- $_SESSION - Banking Details ------------------------------------- //
	$_SESSION['AccountHolder'] = $AccountHolder;
	$_SESSION['AccountType'] = $AccountType;
	$_SESSION['AccountNumber'] = $AccountNumber ;
	$_SESSION['BranchCode'] = $BranchCode;
	$_SESSION['BankName'] = $BankName;
  // ------------------------------- $_SESSION Banking Details ------------------------------------------ //
  // -- start - 16.04.2017 -- Regroup maintainable tables. ---- // 
  // -- End - 16.04.2017 -- Regroup maintainable tables.   --- // 
   $_SESSION['rejectreason'] = $rejectreason;
  
///////////////////////////// ---------------------------------- END SESSION DECLARATIONS ----------------------/////////			
		// -- emailapplicant($ExecApproval);
		}
	} 
else 
{
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND rentalappid = ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid,$ApplicationId));		
$data = $q->fetch(PDO::FETCH_ASSOC);

// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
if(empty($data))
{
	header("Location: rentalapplicatonslist");
}
// -- EOC Encrypt 16.09.2017 - Parameter Data.

// -- UserEmail Address
$sql =  "select * from user where userid= ?";
$q = $pdo->prepare($sql);
$q->execute(array($customerid));
$dataEmail = $q->fetch(PDO::FETCH_ASSOC);

// Customer Details
$property = $data['property'];
$Title = $data['Title'];
$FirstName = $data['FirstName'];
$LastName = $data['LastName'];
$Street = $data['Street'];
$Suburb = $data['Suburb'];
$City = $data['City'];
$State = $data['State'];
$PostCode = $data['PostCode'];
$Dob		= $data['Dob'];

$phone = $data['phone'];
$email =  $dataEmail['email'];
$Original_email = $dataEmail['email'];

		// Loan Details
$MonthlyExpenditure = '0.00';//$data['MonthlyExpenditure'];
$TotalAssets  = '0.00';//$data['TotalAssets'];
$ReqLoadValue = '0.00';//$data['ReqLoadValue'];
$DateAccpt  = $data['DateAccpt'];
$LoanDuration  = '0.00';//$data['LoanDuration'];
$InterestRate = '0.00';//$data['InterestRate'];
$ExecApproval  = $data['ExecApproval'];
$MonthlyIncome  = '0.00';//$data['MonthlyIncome'];

$Repayment = '0.00';//$data['Repayment']; 
$Surety = '0.00';//$data['surety']; 
$monthlypayment = '0.00';//$data['monthlypayment']; 
$FirstPaymentDate = '0.00';//$data['FirstPaymentDate']; 
$lastPaymentDate = '0.00';//$data['lastPaymentDate']; 		
$_SESSION['ExecApproval'] = $ExecApproval; // Store Approval Status for later user via e-mail to customer sending.		

// ------------------------- $data - Banking Details ------------------------------------- //
$AccountHolder = $data['AccountHolder'];
$paymentmethod = $data['optpayments']; 
$AccountType = $data['AccountType'];
$AccountNumber = $data['AccountNumber'];
$BranchCode = $data['BranchCode'];
$BankName = $data['BankName'];
$rejectreason = $data['rejectreason'];

$applicationfeeTextValue		= $data['applicationfeeTextValue'];
$appfeedebitorderdate			= $data['appfeedebitorderdate'];

// -------------------------------- Banking Details ------------------------------------------ //

// -------------------------------- Start 16.04.2017 --- Regroup all the maintainable table ---//
$paymentfrequency = '0.00';//$data['paymentfrequency'];
// -------------------------------- End 16.04.2017 ---- Regroup all the maintainable table ---//
		Database::disconnect();
		
// -- Database PostCode		
		$values = array();
		$values['rentalemail'] = $rentalemail ;
		$values['payer'] = '' ;
		$values['rentalapplicationfee'] = $rentalapplicationfee ;

		$values['appfeedebitorderdate'] = $appfeedebitorderdate;
		$values['BankName'] 	 = $BankName ;
		$values['BranchCode'] 	 = $BranchCode ;
		$values['AccountNumber'] = $AccountNumber ;
		$values['AccountHolder'] = $AccountHolder ;
		$values['Title'] = $Title ;
		$values['idnumber'] = $customerid;
		$values['FirstName'] = $FirstName ;
		$values['LastName'] = $LastName ;

	// -- Determine Account Description.
	foreach($Account_TypeArr as $row)
	{
	  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
	  if($rowData[0] == $AccountType)
	  { $values['AccountType'] = $rowData[1];}
	}
	
	if(!isset($values['AccountType'] ))
	{
		$values['AccountType']  = '';
	}

		
		$applicationFeeText = applicationFees($applicationFeeText,$values);
		if( $paymentmethod == "DebitOrder" || $paymentmethod == 'DTO')
		{
			$applicationfeeTextValue  = $applicationFeeText[1];
		}
		else
		{
			$applicationfeeTextValue  = $applicationFeeText[0];
		}
		
	}
?>
<!-- End Update the eLoan - Application Data -->

<div class="container background-white bottom-border">   
		<div class="row">
 
 <!-- <div class="span10 offset1"> -->
  <form  action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
    <div class="table-responsive">
<table class=   "table table-user-information">
<!-- Customer Details -->			
<!-- Title,FirstName,LastName -->	

<tr>
	<td><div><a class="btn btn-primary" href="rentalapplicatonslist">Back</a></div></td>
	<td><div><button type="submit" class="btn btn-primary">Update</button></div></td>
</tr>	
<tr>
<td colspan="3"> 
<div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Customer Details
                    </a>
                </h2>
</div>	

<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Client information updated successfully!</p>\n", $count);
			// Sent Rejected or Approved status to customer provided
			if($_SESSION['ExecApproval'] != $ExecApproval)
			{	
				// Rejected
				if($ExecApproval == 'REJ')
				{
					printf("<p class='status'>The rental application status is rejected. A rejected e-mail is sent to customer Inbox!</p>\n");

				}
				// Approved
				if($ExecApproval == 'APPR')
				{
					printf("<p class='status'>The rental application status is successfully approved. An approved e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Settled
				if($ExecApproval == 'SET')
				{
					printf("<p class='status'>The rental application status is successfully settled. An comfirmation e-mail is sent to customer Inbox!</p>\n");
				}
				
				// Cancelled
				if($ExecApproval == 'CAN')
				{
					printf("<p class='status'>The rental application status has been cancelled. An cancelation e-mail is sent to customer Inbox!</p>\n");
				}
			}
		}
		// -- if there any error display here.
		else
		{
				// keep track validation errors - 		// Customer Details
if(!empty($TitleError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($FirstNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($LastNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StreetError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($SuburbError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($CityError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($PostCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($DobError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($phoneError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($emailError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($paymentmethodError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
		
// keep track validation errors - 		// Loan Details
/*if(!empty($MonthlyExpenditureError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($TotalAssetsError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($ReqLoadValueError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($DateAccptError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($LoanDurationError)){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($StaffIdError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($InterestRateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($ExecApprovalError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($MonthlyIncomeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
 
if(!empty($RepaymentError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($SuretyError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($monthlypaymentError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($FirstPaymentDateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
if(!empty($lastPaymentDateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else 
*/
// ------------------------- Declarations - Banking Details ------------------------------------- //
if(!empty($AccountHolderError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($AccountTypeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($AccountNumberError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($BranchCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($BankNameError )){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}
		
		}
		?>
				
</td>						
		
</tr>

<tr>						
  <td>
	<div class="control-group <?php echo !empty($TitleError)?'error':'';?>">
		<label class="control-label">Title</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="Title" type="text"  placeholder="Title" value="<?php echo !empty($Title)?$Title:'';?>"> -->
			
			<SELECT class="form-control" id="Title" name="Title" size="1">
						<OPTION value="Mr" <?php if ($Title == 'Mr') echo 'selected'; ?>>Mr</OPTION>
						<OPTION value="Mrs" <?php if ($Title == 'Mrs') echo 'selected'; ?>>Mrs</OPTION>
						<OPTION value="Miss" <?php if ($Title == 'Miss') echo 'selected'; ?>>Miss</OPTION>
						<OPTION value="Ms" <?php if ($Title == 'Ms') echo 'selected'; ?>>Ms</OPTION>
						<OPTION value="Dr" <?php if ($Title == 'Dr') echo 'selected'; ?>>Dr</OPTION>
			</SELECT>
			
			<?php if (!empty($TitleError)): ?>
			<span class="help-inline"><?php echo $TitleError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td> 

	<td> 
	<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
		<label class="control-label">First Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" 
			value="<?php echo !empty($FirstName)?$FirstName:'';?>">
			<?php if (!empty($FirstNameError)): ?>
			<span class="help-inline"><?php echo $FirstNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
		<label class="control-label">Last Name</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
			<?php if (!empty($LastNameError)): ?>
			<span class="help-inline"><?php echo $LastNameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
 
</tr>	

<!-- Street,Suburb,City -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
		<label class="control-label">Street</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
			<?php if (!empty($StreetError)): ?>
			<span class="help-inline"><?php echo $StreetError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
		<label class="control-label">Suburb</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
			<?php if (!empty($SuburbError)): ?>
			<span class="help-inline"><?php echo $SuburbError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
		<label class="control-label">City</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
			<?php if (!empty($CityError)): ?>
			<span class="help-inline"><?php echo $CityError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<!-- State,PostCode,Dob -->		
<tr>
	<td> 
	<div class="control-group <?php echo !empty($StateError)?'error':'';?>">
		<label class="control-label">Province</label>
		<div class="controls">
			<!-- <input class="form-control margin-bottom-10" name="State" type="text"  placeholder="Province" value="<?php echo !empty($State)?$State:'';?>"> -->
			    <SELECT class="form-control" name="State" size="1">
								<!-------------------- BOC 2017.04.15 - Provices  --->	
											<?php
											$provinceid = '';
											$provinceSelect = $State;
											$provincename = '';
											foreach($dataProvince as $row)
											{
												$provinceid = $row['provinceid'];
												$provincename = $row['provincename'];
												// Select the Selected Role 
												if($provinceid == $provinceSelect)
												{
												echo "<OPTION value=$provinceid selected>$provincename</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$provinceid>$provincename</OPTION>";
												}
											}
										
											if(empty($dataProvince))
											{
												echo "<OPTION value=0>No Provinces</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Provices  --->	
				</SELECT>
			<?php if (!empty($StateError)): ?>
			<span class="help-inline"><?php echo $StateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
		<label class="control-label">Postal Code</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
			<?php if (!empty($PostCodeError)): ?>
			<span class="help-inline"><?php echo $PostCodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
		<label class="control-label">Date of birth</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  value="<?php echo !empty($Dob)?$Dob:'';?>">
			<?php if (!empty($DobError)): ?>
			<span class="help-inline"><?php echo $DobError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	

<tr>	
<td>
<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
</td>
<td>
<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">Email
									</label>
									<div class="controls">
										<input readonly  style="height:30px" class="form-control margin-bottom-10" name="email" type="text"  placeholder="email" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
									<p>			
				<input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
									</p>
								</div>
</td>
</tr>	

<!-----------------------------------------------------------  Banking Details ---------------------------------------->	
<tr>
<td><div class="panel-heading">
											
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Banking Details
                    </a>
                </h2>						
</td></div>							
		
</tr>
<!-- Account Holder Name,Bank Name,Account number -->	
<tr>
<td> 
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
								<label>Account Holder Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td> 	
<td>
<!-- Bank Name -->
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
								<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
</td>							
<td>
<!-- Account Number  -->
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
								 <label>Account Number</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
</td>
</tr>
<tr>
<td>

<!-- Account Type  -->
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
								<label>Account Type</label>
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1">
											<?php
											  $accounttySelect = $AccountType;
											  foreach($Account_TypeArr as $row)
											  {
												  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												  if($rowData[0] == $accounttySelect)
												  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
												  else
												  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
											  }

											if(empty($Account_TypeArr))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>

									</SELECT>
									</div>	
								</div>
</td>
<td>								
							<!-- Branch Code  -->
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
								<label class="control-label">Branch Code</label>
									<div class="controls">
										<SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->										
										  </SELECT>	
									</div>	
								</div>
							</div>

</td>
<td>
<label>Payment method</label>
<SELECT class="form-control" id="paymentmethod" name="paymentmethod" onchange="applicationFeeText()" size="1">
								<!-------------------- BOC 2017.04.15 -  Payment Method --->	
											<?php
											$paymentmethodid = '';
											$paymentmethodSelect = $paymentmethod;
											$paymentmethoddesc = '';
											foreach($dataPaymentMethod as $row)
											{
												$paymentmethodid = $row['paymentmethodid'];
												$paymentmethoddesc = $row['paymentmethoddesc'];
												// Select the Selected Role 
												if($paymentmethodid == $paymentmethodSelect)
												{
												echo "<OPTION value=$paymentmethodid selected>$paymentmethoddesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$paymentmethodid>$paymentmethoddesc</OPTION>";
												}
											}
										
											if(empty($dataPaymentMethod))
											{
												echo "<OPTION value=0>No Payment Method</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Payment Method. --->
								</SELECT>                                
</td>
<td>
</tr>
<!----------------------------------------------------------- End Banking Details ---------------------------------------->	

<!-- Loan Details -->
<!-- MonthlyIncome,MonthlyExpenditure,TotalAssets -->	
<tr>
<td><div class="panel-heading">
				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Application Fee Payment Details
                    </a>
                </h2>						
</td></div>							
		
</tr>	
<tr>
	<td colspan='2'> 
	<div class="control-group <?php echo !empty($applicationfeeTextValueError)?'error':'';?>">
		<label class="control-label">Application Fee Acceptance</label>
		<div class="controls">
			<textarea style="height:250px;resize: none;" readonly style="height:30px" class="form-control margin-bottom-10" id="applicationfeeTextValue" name="applicationfeeTextValue" type="text"><?php echo !empty($applicationfeeTextValue)?$applicationfeeTextValue:'';?></textarea>
			<?php if (!empty($applicationfeeTextValueError)): ?>
			<span class="help-inline"><?php echo $applicationfeeTextValueError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td> 
	<div class="control-group <?php echo !empty($appfeedebitorderdateError)?'error':'';?>">
		<label class="control-label">Application Fee Debit Date</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" name="appfeedebitorderdate" type="date" placeholder="yyyy-mm-dd" value="<?php echo !empty($appfeedebitorderdate)?$appfeedebitorderdate:'';?>">
			<?php if (!empty($appfeedebitorderdateError)): ?>
			<span class="help-inline"><?php echo $appfeedebitorderdateError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>	
<tr>
<td>
	<label class="control-label">Property you are applying for?</label>
	<div class="controls">
	<SELECT  id="property" class="form-control" name="property" size="1">
		<?php
		  $propertySelect = $property;
		  foreach($propertyArr as $row)
		  {
			  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
			  if($rowData[0] == $propertySelect)
			  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
			  else
			  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
		  }
		?>
	</SELECT>
	</div>
</td>
 <td> 
	<div class="control-group <?php echo !empty($ExecApprovalError)?'error':'';?>">
		<label class="control-label">Status</label>
		<?php 
		$status = '';
		$statusAPPR = '';
		$statusREJ = '';
		$statusPEN = '';
		$statusCAN = '';
		$statusSET = '';
		// -- Defaulter - 20.06.2018.
		$statusDEF = '';

		// -- Legal - 07.11.2018.
		$statusLEGAL = '';
		
		// -- Rehabilitation - 07.11.2018.
		$statusREH = '';
		
		if ($ExecApproval == 'APPR'){ $statusAPPR = 'selected';}
		if ($ExecApproval == 'REJ') { $statusREJ = 'selected';}
		if ($ExecApproval == 'PEN') { $statusPEN = 'selected';}
		if ($ExecApproval == 'CAN') { $statusCAN = 'selected';}
		if ($ExecApproval == 'SET') { $statusSET = 'selected';}

		// -- Legal - 07.11.2018.
		if ($ExecApproval == 'LEGAL') { $statusLEGAL = 'selected';}
		
		// -- Rehabilitation - 07.11.2018.
		if ($ExecApproval == 'REH') { $statusREH = 'selected';}
		
		// -- Defaulter - 20.06.2018.
		if ($ExecApproval == 'DEF') { $statusDEF = 'selected';}

		if($_SESSION['role'] == 'Administrator' or $_SESSION['role'] == 'Admin')
		{
		echo "<div class='control-group'>
		 <SELECT class='form-control' name='ExecApproval' size='1'>
			   <OPTION value='APPR' $statusAPPR>Approved</OPTION>
				<OPTION value='REJ' $statusREJ>Rejected</OPTION>
				<OPTION value='PEN' $statusPEN>Pending</OPTION>
				<OPTION value='CAN' $statusCAN>Cancelled</OPTION>
				<OPTION value='SET' $statusSET>Settled</OPTION>
				<OPTION value='DEF' $statusDEF>Defaulter</OPTION>		
				<OPTION value='LEGAL' $statusLEGAL>Legal</OPTION>				
				<OPTION value='REH' $statusREH>Rehabilitation</OPTION>								
		 </SELECT>
	    </div>";
		}
		else
		{
		if ($ExecApproval == 'APPR'){ $status = 'Approved';}
		if ($ExecApproval == 'REJ') { $status = 'Rejected';}
		if ($ExecApproval == 'PEN') { $status = 'Pending';}
		if ($ExecApproval == 'CAN') { $status = 'Cancelled';}
		if ($ExecApproval == 'SET') { $status = 'Settled';}
		// -- Defaulter - 20.06.2018.
		if ($ExecApproval == 'DEF') { $status = 'Defaulter';}
		// -- Legal - 07.11.2018.
		if ($ExecApproval == 'LEGAL') { $status = 'Legal';}
		// -- Rehabilitation - 07.11.2018.
		if ($ExecApproval == 'REH') { $status = 'Rehabilitation';}

		echo "<input readonly style='height:30px' class=form-control margin-bottom-10' name='ExecApprovalDisplay' type='text'  placeholder='ExecApproval' value='$status'>";
		echo "<input type='hidden' readonly style='height:30px' class='form-control margin-bottom-10' name='ExecApproval' type='text'  placeholder='ExecApproval' value='$ExecApproval'>";
		}
		?>
		
	</div>
	</td>
</tr>	
<?php
$errorClass = "";
$formClass = "form-control margin-bottom-10";
$displayError = "";

if (!empty($rejectreasonError))
{			
	$displayError =  "<span class='help-inline' style='color:red'>$rejectreasonError</span>";
}

if(!empty($rejectreasonError))
{
	$errorClass = 'control-group error';
}
else
{
	$errorClass = 'control-group';
}

if ($ExecApproval == 'REJ') 
{ 	
	echo "<div id='rejectID' style='display: inline'><tr>
	<td>
	<div class=$errorClass>
		<label>Rejection Reason</label>
		<input  style='height:30px' id='rejectreason' name='rejectreason'  class=$formClass type='text'  value='".$rejectreason ."'/>
		$displayError
	</div> 
	</td>
	<td></td>
	<td></td>
	</tr></div>";
}
?>
<!-- Buttons -->
						<tr>
							<td><div>
								<a class="btn btn-primary" href="rentalapplicatonslist">Back</a>
							</div>
							</td>
							<td>
							<div>
							  	<button type="submit" class="btn btn-primary">Update</button>
							</div>
							</td>
						</tr>
						</table>
						</div>
					</form>
				</div>
				
    </div> <!-- /container -->
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
  
  function applicationFeeText()
{
	 Arr = <?php echo json_encode($applicationFeeText); ?>;
	 DebitOrder = document.getElementById('paymentmethod').value;
	 alert(DebitOrder);
	 if(DebitOrder == 'DTO' || DebitOrder == 'DebitOrder')
	 {document.getElementById('applicationfeeTextValue').innerHTML = Arr[1];}
	 else
	 {document.getElementById('applicationfeeTextValue').innerHTML = Arr[0];}
 
}
// EOC -- Jquery. 
</script> 
