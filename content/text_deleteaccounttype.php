<?php 
	require 'database.php';
	
	$accounttypeid = null;
	
	if ( !empty($_GET['accounttypeid'])) 
	{
		$accounttypeid = $_REQUEST['accounttypeid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$accounttypeid = $_POST['accounttypeid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "DELETE FROM accounttype WHERE accounttypeid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($accounttypeid));
			Database::disconnect();
			echo("<script>location.href = 'accounttype.php';</script>");

	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="deleteaccounttype.php" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a Account Type 
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="accounttypeid" value="<?php echo $accounttypeid;?>"/>
					  <p class="alert alert-error">Are you sure to delete ?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="accounttype.php">No</a>
						</div>
					</form>
				</div>
	</div>			
</div> <!-- /container -->
