<?php 
require 'database.php';
$provinceidError = null;
$provinceid = '';

$provincenameError = null;
$provincename = '';
$count = 0;
$provinceid = 0;

if (!empty($_POST)) 
{   $count = 0;
	$provincename = $_POST['provincename'];
	$provinceid = $_POST['provinceid'];

	$valid = true;
	
	if (empty($provincename)) { $provincenameError = 'Please enter province name.'; $valid = false;}
	if (empty($provinceid)) { $provinceidError = 'Please enter province id.'; $valid = false;}
	
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO province (provinceid,provincename) VALUES(?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($provincename,$provinceid));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Province Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Province successfully added.</p>");
		}
?>
				
</div>				
			<p><b>Province ID</b><br />
			<input style="height:30px" type='text' name='provinceid' value="<?php echo !empty($provinceid)?$provinceid:'';?>"/>
			<?php if (!empty($provinceidError)): ?>
			<span class="help-inline"><?php echo $provinceidError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Province Name</b><br />
			<input style="height:30px" type='text' name='provincename' value="<?php echo !empty($provincename)?$provincename:'';?>"/>
			<?php if (!empty($provincenameError)): ?>
			<span class="help-inline"><?php echo $provincenameError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'province';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
