<?php 
	require 'database.php';
	$tab_name = "customer";
	$userid = null;
	$statuschange = null;
	
// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$UserIdDecoded = "";
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	
	if ( !empty($_GET['customerid'])) 
	{
		$userid = $_REQUEST['customerid'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$userid = base64_decode(urldecode($userid)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	
	if ( !empty($_GET['status'])) 
	{
		$statuschange = $_REQUEST['status'];
		$statuschange = base64_decode(urldecode($statuschange)); 
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$userid = $_POST['userid'];
		$statuschange = $_POST['statuschange'];

		// Unlock & Lock account.
		if($statuschange == 'Active')
		{
			$statuschange = 'Disabled'; // -- Unlock the Account.
		}
		else
		{
			$statuschange =  'Active'; // -- Lock the Account.
		}
		
		echo "<h3>$statuschange - $userid</h3>";
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
		$sql = "UPDATE $tab_name SET status = ? WHERE customerid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($statuschange,$userid));
		Database::disconnect();
		
		header("Location: notificationrecipients");

	} 
	else 
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "SELECT * FROM $tab_name WHERE customerid = ? AND status = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($userid,$statuschange));		
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(empty($data))
			{
				header("Location: index");
			}
	}
	// -- EOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Activate/Deactivate Notification Recipient
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="userid" value="<?php echo $userid;?>"/>
					  <input type="hidden" name="statuschange" value="<?php echo $statuschange;?>"/>
					  
					  <p class="alert alert-error">Are you sure to Activate/Deactivate Notification Recipient?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="notificationrecipients.php">No</a>
					  </div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
