<?php
// -- BOC Force All eCashMeUp pages not to cache on all browsers.14.10.2017
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

// -- checkdefaultpage -- 30/01/2022
if(!function_exists('checkdefaultpagebyrole'))
{
function checkdefaultpagebyrole($pdo,$role)
{
	// --24/01/2022 - Default page...
	// -- Select page features.
	$featuresText = '';
	$default  	  = 'X';

	$sql = 'SELECT * FROM access WHERE role = ? AND defaultpage = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($role,$default));
	$datadefaultpage    = $q->fetch(PDO::FETCH_ASSOC);
	$datadefaultpageArr = null;

	// -- array content..
	if (!isset($datadefaultpage['defaultpage']))
	{
		//-- Do nothing..
	}
	else
	{
	   // -- split features into array..
		$datadefaultpageArr['defaultpage'] = $datadefaultpage['defaultpage'];
		$datadefaultpageArr['childpage'] = $datadefaultpage['childpage'];
	}
	return 	$datadefaultpageArr;
}}
// -- EOC Force All eCashMeUp pages not to cache all browsers.14.10.2017
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require $filerootpath.'/api/chenosis.php';
require 'database.php';
// -- BOC -- Two factors authentications 26.03.2021

// -- Chenosis API process ochestration
if(!function_exists('createVerificationLogin'))
{
	function createVerificationLogin($data,$row)
	{
		try
		{
			$phone 		= $data['phone'];
			$access_token 	= '';
			$verificationId = '';
			$message  	    = '';
			$datareturn     = null;
			$json_obj       = null;
			

			// -- getsession('2fa_appname');
			// -- Chenosis API -- 1). GET access token
			$dataresponse = null;
			$client_id 	   = getsession('2fa_client_id');//'ANOpWzAvThjdNq9wHXX8NCBPANJwt7Fx';
			$client_secret = getsession('2fa_client_secret');//'ik6N5zRH9G8YuPPv';
			$data['client_id']     = $client_id ;
			$data['client_secret'] = $client_secret ;
			// -- API Feedback..
			$json_obj = getaccesstoken($data);
			if(isset($json_obj->access_token))
			{$access_token = $json_obj->access_token;}
			//////////////////////////////////////////////////////
			// -- Chenosis API -- 2). Create verification
			if(!empty($access_token))
			{
				$dataCreate 			   = null;
				$dataCreate['channel']     = "sms" ;
				$dataCreate['to'] = $data['phone'] ;
				$dataCreate['access_token'] = $access_token;
				$json_obj = createverification($dataCreate);
				if(isset($json_obj->data->verificationId))
				{$verificationId = $json_obj->data->verificationId;}
			}
			//////////////////////////////////////////////////////
			if(isset($json_obj->Error))
			{$message = $json_obj->Error;}

			$datareturn['phone'] = $phone;
			$datareturn['verificationId'] = $verificationId;
			$datareturn['access_token']   = $access_token;
			$datareturn['row']   = $row;
			$datareturn['error'] = $message;
			return $datareturn;
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- Chenosis API -- 3). Verify verification code.
if(!function_exists('verifylogin'))
{
	function verifylogin($data)
	{
		try
		{
			$valid 		= false;
			$json_obj   = null;
			$status 	= 'pending';
			$dataVerifycode = null;
			$message 		= '';

			// -- Chenosis API -- 3). Verify verification code.
			// -- Verify the 4-digit code.
			if(!empty($data['verificationId']))
			{
				$dataVerifycode['code']           = $data['code'] ;
				$dataVerifycode['access_token']   = $data['access_token'];
				$dataVerifycode['verificationId'] = $data['verificationId'];
				$json_obj  = verifycode($dataVerifycode);
				if(isset($json_obj->data->status))
				{$status = $json_obj->data->status;}
			}
			if($status == 'approved')
			{ $valid = true;}
			//////////////////////////////////////////////////////
			if(isset($json_obj->Error))
			{$message = $json_obj->Error;}
			$datareturn['status'] = $status;
			$datareturn['error'] = $message;
			$datareturn['valid'] = $valid;

			return 	$datareturn;
		}
		catch (Exception $e)
	    {
		  echo 'Caught exception: ',  $e->getMessage(), "\n";
	    }
	}
}
// EOC -- Two factors authentications 26.03.2021
// -- Get Audit Logs via ALIAS
if(!function_exists('GetAuditLogAlias'))
{
	function GetAuditLogAlias($alias)
	{
		try
		{
			$arrayLog   	  = array();
			$arrayLog[] 	  = 'Active';
			$arrayLog[] 	  =  $alias;
			$dataAuditLogUser = GetAuditActiveUser($arrayLog);
			return $dataAuditLogUser;
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- Get Audit Logs via USERID
if(!function_exists('GetAuditLogUserid'))
{
	function GetAuditLogUserid($userid)
	{
		try
		{
		  	$arrayLog   	  = array();
			$arrayLog[] 	  = 'Active';
			$arrayLog[] 	  =  $userid;
			$dataAuditLogUser = GetAuditActiveUser($arrayLog);
			return $dataAuditLogUser;
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- login_via_userid_or_alias
if(!function_exists('login_via_userid_or_alias'))
{
	function login_via_userid_or_alias($data)
	{
		// -- Database Connections
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// -- User - UserID & E-mail Duplicate Validation
		$query = "SELECT * FROM user WHERE {$data['searchField']} = ?";
		$q = $pdo->prepare($query);
		$q->execute(array($data['idnumber']));// -- Changed 16.09.2017 - MD5 Encryption. $passwordEncrypted
		$passwordEncrypted = $q->fetch(PDO::FETCH_ASSOC);
		$data['passwordEncrypted'] = $passwordEncrypted;
		$data['q'] = $q;
		return $data;
	}
}
////////////////////////////////////////////////////////////////////////////////////
// -- LoggedIn Successfully - 26.03.2021
////////////////////////////////////////////////////////////////////////////////////
if(!function_exists('loggedInSuccess'))
{
	function loggedInSuccess($row,$credential)
	{
		try
		{
			$userid		 	= $credential['userid'];
			$password	    = $credential['password'];
			$logo	 		= $credential['logo'];
			//$row = mysql_fetch_array($result);
			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.
			//$_SESSION['UserCode'] = $UserCode;
			//$_SESSION['IntUserCode'] = $IntUserCode;
			$_SESSION['email'] = $row['email'];
			$_SESSION['customerLoggedIn'] = true;

			// Title
			$_SESSION['Title'] = $row['Title'];
			// First name
			$_SESSION['FirstName'] = $row['FirstName'];
			// Last Name
			$_SESSION['LastName'] = $row['LastName'];
			//ID Number
			$_SESSION['CustomerId'] = $row['CustomerId'];
			// Phone numbers
			$_SESSION['Customerphone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];

			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];

			// Dob
			$_SESSION['Dob'] = $row['Dob'];

			// phone
			$_SESSION['Customerphone'] = $row['phone'];

			// User Code
			$_SESSION['UserCode'] = $row['UserCode'];

			//Internal User Code
			$_SESSION['IntUserCode'] = $row['IntUserCode'];

			//Bulk Client ID
			$_SESSION['bulksmsclientname'] = $row['bulksmsclientname'];

			//Bulk Client NAME
			$_SESSION['bulksmsclientid'] = $row['bulksmsclientid'];

			//Bulk cost Center Account
			$_SESSION['account'] = $row['account'];

			// -- DebiCheck user code.
			$_SESSION['debicheckusercode'] = $row['debicheckusercode'];

			// -- usercodePayments - 03.08.2021
			$_SESSION['usercodePayments'] = $row['usercodePayments'];

			// -- usercodestatements - 09.04.2022
			$_SESSION['statementusercode'] = $row['statementusercode'];
			
			if (!empty($row['nominated_account'])) {
			$_SESSION['nominated_account'] == $row['nominated_account'];
			}
			// -- Nominated Account - 25.05.2023
			

// -- =============================== Global Settings Values for Email Template ======================== -- //
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();

	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{

		// -- Company details from Form.
		$_SESSION['GlobalCompany']  = $data['name'];
		$_SESSION['Globalphone']    = $data['phone'];
		$_SESSION['Globalfax']      = $data['fax'];
		$_SESSION['Globalemail']    = $data['email'];
		$_SESSION['Globalwebsite']  = $data['website'];
		$_SESSION['Globallogo']     = $data['logo'];
		$_SESSION['GloballogoTemp'] = $logo;
		$_SESSION['Globalcurrency'] = $data['currency'];
		$_SESSION['Globallegalterms'] = $data['legaltext'];

		$_SESSION['adminemail']  =  $data['email'];
		$_SESSION['companyname'] =  $data['name'];

		$_SESSION['Globalstreet'] = $data['street'];
		$_SESSION['Globalsuburb'] = $data['suburb'];
		$_SESSION['Globalcity'] = $data['city'];
		$_SESSION['Globalstate'] = $data['province'];
		$_SESSION['Globalpostcode'] = $data['postcode'];
		$_SESSION['Globalregistrationnumber'] = $data['registrationnumber'];

		// ----- BOC -- 2017.06.04 ---------- //
		$_SESSION['terms'] = $data['terms'];
		$_SESSION['ncr'] = $data['ncr'];
		// ----- EOC -- 2017.06.04 ---------- //
	}

	// -- Creating Login Session Timeout - 30.09.2017.
		$_SESSION['loggedin_time'] = time();

	// -- Credit Rehabilitation - Consultant
	// -- Credit Rehabilitation - Manager
			if($_SESSION['role'] == 'crconsultant' || $_SESSION['role'] == 'crmanager')
			{
				$location = "creditrehabilitationhome?hash=".md5(date("h:i:sa"));
			}
	// -- Paycollection
			elseif($_SESSION['role'] == 'paycollection')
			{
						$location = "collectiondashboard?hash=".md5(date("h:i:sa"));
			}
	// -- Rental Applications
			elseif($_SESSION['role'] == 'Administrator')
			{
				$location = "dashboardrentals?hash=".md5(date("h:i:sa"));
			}
	// -- Bulk SMS
			elseif($_SESSION['role'] == 'bulksms' or $_SESSION['role'] == 'Super' or $_SESSION['role'] == 'Standard')
			{
						$location = "bulksmshome?hash=".md5(date("h:i:sa"));
			}
			//Support
			elseif($_SESSION['role'] == 'Support')
			{
						$location = "collectiondashboard?hash=".md5(date("h:i:sa"));
			}
			//DebiCheck
			elseif($_SESSION['role'] == 'DebiCheck')
			{
						$location = "collectiondashboard?hash=".md5(date("h:i:sa"));
			}
	// -- 3rdParty Access
			elseif($_SESSION['role'] == '3rdParty')
			{
						$location = "billCollection?hash=".md5(date("h:i:sa"));
			}
	// -- renter
			elseif($_SESSION['role'] == 'renter')
			{
					$location = "dashboardrentals?hash=".md5(date("h:i:sa"));
			}
			else
			{
						$location = "dashboard?hash=".md5(date("h:i:sa"));
			}
// -- BOC - New Default Page laucher using the table access. 31/01/2022
	$location 	= "index?hash=".md5(date("h:i:sa"));
	$role 		= $_SESSION['role'];
	$objectArr  = checkdefaultpagebyrole($pdo,$role);
	if(isset($objectArr['defaultpage'])) // -- New defaultpage...
	{
		 $default = $objectArr['defaultpage'];
		if(!empty($default))
		{
		  $location = $objectArr['childpage']."?hash=".md5(date("h:i:sa"));
		}
	}
// -- EOC - New Default Page laucher using the table access. 31/01/2022
	// -- Audit Log for Popi Act
									$today = date("Y-m-d H:i:s");
									$ipaddress = getUserIP();// -- visitor ip address
									$_SESSION['ipaddress'] = $ipaddress;
									$arrayLog   = array();
									$arrayLog[] = $_SESSION['username'];//- userid
									$arrayLog[] = $_SERVER['PHP_SELF'];//- page
									$arrayLog[] = $today;//- date time
									$arrayLog[] = $_SESSION['role'];//- role
									$arrayLog[] = $ipaddress;//- ipaddress
									$arrayLog[] = 'Active';
									$arrayLog[] = '';
									AddAuditLog($arrayLog);
					// -- Login Audit Trail
	// -- Creating Login Session Timeout - 30.09.2017.
	// -- =============================== 24.04.2017 - EOC ================================================= -- //
     echo "<script>window.open('".$location."','_self')</script>";
		}
	 catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
////////////////////////////////////////////////////////////////////////////////////
/*
			}
			catch(PDOException $e)
			{
				echo "Error:".$e->getMessage();
			}
*/
// -- Button Login Pressed.
if(!function_exists('btn_loginpressed'))
{
	function btn_loginpressed($valid, $fourdigits,$searchField,$searchField2)
	{
		try
		{
			if(isset($_POST['login']) or isset($_POST['resentsms']))
			{
				$userid = getpost('idnumber');
				$UserCode = '';
				$IntUserCode = '';

			if($valid) //Validated
			{
				 $pdo = Database::connect();
				 $tbl_name="user"; // Table name
				 $tbl_customer ="customer"; // Table name
				 $tbl_alias = "customer";
				 $userid = getpost('idnumber');
				 $useridTemp = $userid;
				 $password = getpost('password1');
				 // -- 01.05.2018 - Change.
				 // $check_user = "SELECT * FROM $tbl_name Inner Join $tbl_customer on $tbl_name.userid = $tbl_customer.customerid  WHERE userid='$userid'";//-- and password='$password'";
				if($searchField == 'userid'){$tbl_alias = $tbl_name; }//{$userid = '"'.$userid.'"'; $searchField = 'alias';}

			$check_user = "SELECT * FROM $tbl_name Inner Join $tbl_customer on $tbl_name.$searchField = $tbl_customer.$searchField2  WHERE $tbl_alias.$searchField = ?";
			//echo $check_user.' - '.$userid.' - '.$useridTemp.' - '.$searchField;
			//try
			//{
				$pdo = Database::connect();
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				 $q = $pdo->prepare($check_user);
				 $q->execute(array($userid));
				 $row = $q->fetch(PDO::FETCH_ASSOC);
				 $userid = $useridTemp;
				 Database::disconnect();
			/*}
			catch(PDOException $e)
			{
				echo "Error:".$e->getMessage();
			}*/
			// -- Mysql_num_row is counting table row
			$count = $q->rowCount();
			// -- 01.05.2018 - Change.
			$_SESSION['Customerphone'] = $row['phone'];
			if(isset($_SESSION['Globaltwofactors']))
			{
			   // -- Two Factors Authenticate - 26.03.2021
				$phone_v2 = $row['phone'];
				if(!empty($_SESSION['Globaltwofactors']))
				{
				if(empty($phone_v2)){ $phone_v2 = $row['phone2'];}
			  // -- Assumption that all number are saved as 082-replace 1st latter with 0 with 0027
				if(substr($phone_v2,1) != '+')
				{$phone_v2 = trim("+27".substr($phone_v2,1));}
				$dataAuthenticated['phone']  = $phone_v2;
				$dataAuthenticated['code']   = $fourdigits;
				$datacreateVerificationLogin = createVerificationLogin($dataAuthenticated,$row);
				// -- Chenosis API
				$_SESSION['verificationId'] = $datacreateVerificationLogin['verificationId'];
				$_SESSION['access_token']   = $datacreateVerificationLogin['access_token'];
				$_SESSION['error'] 			= $datacreateVerificationLogin['error'];
				$_SESSION['row'] 			= $datacreateVerificationLogin['row'];
				$_SESSION['Customerphone']  = $datacreateVerificationLogin['phone'];
				$valid = false; // -- Validated by VerifyLoginForm button
			}}
			if($valid)
			{
				if($count >= 1)
				{
					// -- Successfully loggedin
				 	$credential['userid']   = $userid;
					$credential['password'] = $password;
				 	$credential['logo']     = '';
					loggedInSuccess($row,$credential);
				}
			  } // -- Two Factors Authenticate 26.03.2021
			}
			}

		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
////////////////////////////////////////////////////////////////////////////////////
// -- Button Verify Pressed.
if(!function_exists('btn_verifypressed'))
{
	function btn_verifypressed($fourdigits,$fourdigitsError,$credential)
	{
		try
		{
			// -- Login we are done disable it & enable verification code.
			//echo 'VerifyLoginForm Pressed : '.$_POST['VerifyLoginForm'];
			if(isset($_SESSION['Globaltwofactors']))
			{
				// -- Two Factors Error Validation.
				//echo '<br> Code : '.$fourdigits;
				if (empty($fourdigits)) { $fourdigitsError = 'Please enter 6-digit code'; $valid = false;}
				else{
				$Globaltwofactors = $_SESSION['Globaltwofactors'];
				if(!empty($Globaltwofactors))
				{
					$fourdigits = getpost('fourdigits');
					// -- Chenosis API -- 3). Verify verification code.
					if(isset($_SESSION['verificationId']))
					{
						$dataVerifycode				      = null;
						$dataVerifycode['code']           = $fourdigits ;
						$dataVerifycode['access_token']   = $_SESSION['access_token'];
						$dataVerifycode['verificationId'] = $_SESSION['verificationId'];
						$obj = null;
						$obj['Error'] = '';
						$obj = verifylogin($dataVerifycode);
						//echo 'valid obj: '.$obj['valid'].' - '.$obj['status'];
						if(!$obj['valid'])
						{
						  $fourdigitsError = '6-digit code incorrect, please try again.'; $valid = false;
						  if(!empty($obj['Error']))
						  {
								$fourdigitsError = $fourdigitsError.'Error :'.$obj['Error'].'.Please contact Administrator';
						  }
						}
						else
						{
							$valid = true;
						}
						// -- If valid is true, then loggin the user.
						if($valid)
						{
							loggedInSuccess($_SESSION['row'],$credential);
						}
					}
					else{ $fourdigitsError = 'Please contact Administrator, to check you cellphone number'; $valid = false;}
			   }}
			}

			return $fourdigitsError;
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
////////////////////////////////////////////////////////////////////////////////////
$dbGlobal = 'ecashpdq_eloan';
$searchField = "userid";
$searchField2 = "CustomerId";
// -- Checked if LoggedIn, then direct to index Page..
if (isset($_SESSION['loggedin']))
{
	if($_SESSION['loggedin'] == true)
	{
		redirect("index");
	}
}
// -- EOC Checked if loggedin.

// -- Captcha
$modise = '';

// -- BOC Session Timeouts 30.09.2017
$message	= "";
// BC -- 26.03.2021
$fourdigits 	    = "";
$fourdigitsError = "";
$displaytwofactors 	= "display:none";
$displaylogin 	    = "display:block";

// EC -- 26.03.2021

if(isset($_GET["session_expired"]))
{
	$message = "Login Session is Expired. Please Login Again";
}
// -- EOC Session Timeouts 30.09.2017

if (!empty($_POST))
{

	 // Captcha declare
	 	$modise = getsession('captcha');
		$captcha = '';
		$captchaError = null;
		// --Captcha
		$captcha = getpost('captcha');
		// - BC - Two factors  - 26.03.2021
		$fourdigits = getpost('fourdigits');
		$displaylogin 	    	= getpost('displaylogin');
		$displaytwofactors 	    = getpost('displaytwofactors');
		// EC- Two factors  - 26.03.2021
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$password1Error = null;

		// Login Details
		$password1 = getpost('password1');
		$idnumber  = getpost('idnumber');

		// -- echo "<h3>ID - $idnumber </h3>";

		// To protect MySQL injection for Security purpose (Encryption 30.09.2017)
		$password1 = $password1;// -- stripslashes(
		$idnumber  = $idnumber;// -- stripslashes(
		$password1 = $password1;// -- mysql_real_escape_string(
		$idnumber  = $idnumber;// -- mysql_real_escape_string(

		// -- echo "<h3>ID 2 - $idnumber </h3>";

		// validate input - 		// Customer Details
		$valid = true;
		if (empty($password1)) { $password1Error = 'Please enter password'; $valid = false;}

		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}

// SA ID Number have to be 13 digits, so check the length
	//	if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}

// Is Numeric
	//	if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
	// -- Disable/Enable -- //

	// -- Enable and Disable a Captcha -- //
		// -- 06.05.2018 - Disable Captcha -- //
		if(isset($_SESSION['Globalcaptcha']))
		{
			$Globalcaptcha = $_SESSION['Globalcaptcha'];
			if(empty($Globalcaptcha))
			{

 // Captcha declare
	 	//$modise = $_SESSION['captcha'];
		//$captcha = '';
		$captchaError = null;
		// --Captcha
		//$captcha = $_POST['captcha'];

	  // Captcha Error Validation.
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}}
		}

	  // Captcha Error Validation.
		//if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		//if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}
		//$captcha

			// Captcha
			// 	refresh Image
			if (!isset($_POST['login'])) // If refresh button is pressed just to generate Image.
			{
				$valid = false;
			}
		if($valid)
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// ---------------- User via ID or ALIAS ---------------
			$logindata = null;
			$logindata['searchField'] = $searchField;
			$logindata['idnumber'] = $idnumber;
			$logindata = login_via_userid_or_alias($logindata);
			$passwordEncrypted = $logindata['passwordEncrypted'];
			$q = $logindata['q'];
		    // ---------------- TRY Authenticate via User via ALIAS ---------------
			if($q->rowCount() < 1)
			{ //User - UserID & E-mail Duplicate Validation
				$searchField = 	"alias";
				$searchField2 = "alias";
				$query = "SELECT * FROM user WHERE $searchField = ?"; // --  AND password = ?
				$q = $pdo->prepare($query);
				$q->execute(array($idnumber));// -- Changed 16.09.2017 - MD5 Encryption. $passwordEncrypted
				//  -- BOC encrypt password - 16.09.2017.
				$passwordEncrypted = $q->fetch(PDO::FETCH_ASSOC);
				//  -- EOC encrypt password - 16.09.2017.
			}
			if ($q->rowCount() < 1)
			{
				$password1Error = 'Username and password you entered do not match.'; $valid = false;
			}
			else
			{
				//  -- BOC encrypt password - 16.09.2017.
				if (!password_verify($password1, $passwordEncrypted['password']))
				{
					$password1Error = 'ID number and password you entered do not match.'; $valid = false;
					// -- 3 failed failed attempt
					if($passwordEncrypted['failedAttempt'] < 3)
					{
						// -- BOC Brut-force protection. count failedAttempt - 16.09.2017.
						$passwordEncrypted['failedAttempt'] = $passwordEncrypted['failedAttempt'] + 1;
						$sql = "UPDATE user SET failedAttempt = ? WHERE $searchField = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($passwordEncrypted['failedAttempt'],$idnumber)); // -- Changed 16.09.2017 - MD5 Encryption.
						Database::disconnect();
					}
					else
					{
						$password1Error = "We have temporarily locked your account after too many failed<br /> attempts to login.
						Please contact the administrator.";
						$valid = false;
					}
				}

				if($valid)
				{
					// -- Reset failedAttempt to zero.
					if($passwordEncrypted['failedAttempt'] < 3)
					{
						$passwordEncrypted['failedAttempt'] = 0;
						$sql = "UPDATE user SET failedAttempt = ? WHERE $searchField = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($passwordEncrypted['failedAttempt'],$idnumber)); // -- Changed 16.09.2017 - MD5 Encryption.
						Database::disconnect();
					}
					else
					{
						$password1Error = "We have temporarily locked your account after too many failed<br /> attempts to login.
						Please contact administration.";
						$valid = false;
					}
					// -- BOC Brut-force protection. count failedAttempt - 16.09.2017.
				}
				//  -- BOC encrypt password - 16.09.2017.

				// -- Get only logged in in one computer session at time.
				/*if($valid)
				{
					// -- Audit Log via - UserID..
					$dataAuditLogUser = GetAuditLogAlias($passwordEncrypted['userid']);
					// -- Only one user session is allowed.
					if(isset($dataAuditLogUser['audittrailid']))
					{
						// -- Check the current user status.
						$password1Error = "Only one user session is allowed<br />Another user is already logged in.
						Please contact administration.";
						$valid = false;
					}
					// -- if valid is still true try via ALIAS..
					$dataAuditLogUser = GetAuditLogAlias($passwordEncrypted['alias']);
					if($valid)
					{
						if(isset($dataAuditLogUser['audittrailid']))
						{
							// -- Check the current user status.
							$password1Error = "Only one user session is allowed<br />Another user is already logged in.
							Please contact administration.";
							$valid = false;
						}
					}
				}*/
			}
		}
// -- BOC
		// BC -- Enable two factor Login 26.03.2021 -- //
		//echo 'valid '.$valid;
		//echo 'Customerphone : '.$_SESSION['Customerphone'];
		if($valid)
		{
			/////////////////////////////////////////////////////////////////////////
			// -- Database Connections - Login Button Pressed
			btn_loginpressed($valid,$fourdigits,$searchField,$searchField2);
			/////////////////////////////////////////////////////////////////////////
			if(isset($_SESSION['Globaltwofactors']))
			{
					// -- Login we are done disable it & enable verification code.
					if(!empty($_SESSION['Globaltwofactors']))
					{
						$displaytwofactors 	= "display:block";
						$displaylogin 	    = "display:none";
					}
			}
		}
		// -- Two Factors : Enabled at GlobalSettings..
		if(isset($_SESSION['Globaltwofactors']))
		{ // -- 1). Verify Button Pressed...
			if(isset($_POST['VerifyLoginForm']) && !empty($_SESSION['Globaltwofactors']))
			{ 	$logo = '';
				$displaytwofactors 	= "display:block";
				$displaylogin 	    = "display:none";
				$credential['userid']   = $idnumber;
				$credential['password'] = $password1;
				$credential['logo']     = $logo;
				$fourdigitsError    = btn_verifypressed($fourdigits,$fourdigitsError,$credential);
			}
		 // -- 2). Resent SMS Button Pressed...
		 	if(isset($_POST['resentsms']) && !empty($_SESSION['Globaltwofactors']))
			{
				$logindata = null;
				$logindata['searchField'] = $searchField;
				$logindata['idnumber'] 	  = $idnumber;
				$logindata = login_via_userid_or_alias($logindata);
				$q = $logindata['q'];
				// ---------------- TRY Authenticate via User via ALIAS ---------------
				if($q->rowCount() < 1)
				{ //User - UserID & E-mail Duplicate Validation
					$searchField = 	"alias";
					$searchField2 = "alias";
				}
				$valid = true;// -- just for resending..
				//echo $valid.' - '.$fourdigits.' - '.$searchField.' - '.$searchField2;
				btn_loginpressed($valid,$fourdigits,$searchField,$searchField2);
			}
		}
		// EC -- Enable two factor Login 26.03.2021 -- //
	}
/*
    session_start();
    echo "<h3> PHP List All Modise-2 Session Variables</h3>";
    foreach ($_SESSION as $key=>$val)
    echo $key." ".$val."<br/>";
*/
// -- Generate Captcha Image
function ImageOLD($value)
{
	 $generateImage = '';
	 if (!empty($_POST))
	 {
	 if (isset($_POST['login']))
	   {$generateImage = 'No';}
	 }

	 if( $generateImage == '')
	 {
	$string = '';
	for ($i = 0; $i < 5; $i++)
	{
		$randomCharacter = rand(97, 122);
		$string .= chr($randomCharacter);
		//echo "<script type='text/javascript'>alert('".$string.$randomCharacter."')</script>";
	}

	$_SESSION['captcha'] = strtoupper($string); //store the captcha
	$_SESSION['value']   = strtoupper($value);
	$dir = 'fonts/';
	$image = imagecreatetruecolor(165, 50); //custom image size
	$font = "TNGMonitorsPlain.ttf";//"PlAGuEdEaTH.ttf"; // custom font style
	$color = imagecolorallocate($image, 113, 193, 217); // custom color
	$white = imagecolorallocate($image, 255, 255, 255); // custom background color
	imagefilledrectangle($image,0,0,399,99,$white);
	imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

	// Enable output buffering
	ob_start();
	imagepng($image);
	// Capture the output
	$imagedata = ob_get_contents();
	// Clear the output bufferx
	ob_end_clean();
	$_SESSION['image'] = $imagedata;
	}
	else
	{
	$imagedata = $_SESSION['image'];
	}

	return $imagedata;
}

 ?>
 <style>
 .message
 {
	color: #333;
	border: #FF0000 1px solid;
	background: #FFE7E7;
	padding: 5px 20px;
 }
 </style>
             <div id="content">
<div class="container background-white bottom-border">
                    <!--<div class="container">-->
                        <div class="row margin-vert-60">
                            <!-- Login Box -->
                            <div class="col-md-6 col-md-offset-3"">
								 <FORM class="login-page" METHOD="post" action=<?php echo "".$_SERVER['REQUEST_URI']."";?>>

									<?php if($message!="") { ?>
										<div class="message"><?php echo $message; ?></div>
										<br/>
									<?php } ?>
								<div style="<?php echo $displaylogin;?>">
								<div class="login-header margin-bottom-30">
								<h2 class="text-center"><strong>Log in</strong></h2>
								</div>

								<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <!--<h4>Username<span style='color:red'>*</span></h4>-->
									<div class = "input-group">
									<span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
										<input style="height:40px;z-index:0"  class="form-control"  id="idnumber" name="idnumber" type="text"  placeholder="Username" value="<?php echo !empty($idnumber)?$idnumber:'';?>">
									</div>
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>
								</div>
								</br>
								<!-- Password  -->
								<div class="control-group <?php echo !empty($password1Error)?'error':'';?>">
									<!-- <h4>Password<span style='color:red'>*</span></h4>-->
									<div class = "input-group">
									<span class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </span>
										<input style="height:40px;z-index:0"  class="form-control"  id="password1" name="password1" type="password"  placeholder="Password" value="<?php echo !empty($password1)?$password1:'';?>"  onchange="CreateUserID()">
									</div>
									<?php if (!empty($password1Error)): ?>
										<span class="help-inline"><?php echo $password1Error; ?></span>
									<?php endif; ?>
								</div>

								<?php if(isset($_SESSION['Globalcaptcha']))
										 {
											$Globalcaptcha = $_SESSION['Globalcaptcha'];
											if(empty($Globalcaptcha))
											{?>
								<!-- New Captcha -->
								<h3>Enter the verification code shown below</h3>
								<div id='captcha-wrap'>
									<!-- <img src="data:image/png;base64,".base64_encode(Image())" alt='' id='captcha' /> -->
									 <?php echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' />
											<img src='data:image/png;base64,".base64_encode(captchImage('refresh'))."' alt='' id='captcha' /></div>";
									  ?>
								</div>

								<div class='control-group <?php echo !empty($captchaError)?'error':'';?>'>
											<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value="<?php echo !empty($captcha)?$captcha:'';?>">

											<?php if (!empty($captchaError)): ?>
										<span class="help-inline"><?php echo $captchaError; ?></span>
									<?php endif; ?>

								</div>
								<!-- End New Captcha -->
								<?php }}?>
								</br>
								<div class="col">
								<a id="password-trigger" href="forgotpassword">Forgot Password?</a>
								</div>
								</br>
								</br>
								<button style="height:40px;z-index:0" class="btn btn btn-primary btn-block mb-4" value="login" name="login" type="submit">Log in</button>
                                
							<!--	<div class="row">
                                        <div class="col-md-6">
                                            <!-- <label class="checkbox">
                                                <input type="checkbox">Stay signed in</label>
                                        </div>
                                        <div class="col-md-6">
                                            
                                        </div>
                                    </div>-->
                                    <hr>
									
                                  <!--  <h4>Forgot your Password ?</h4>-->
                                      <!-- <p>
                                      <a href="#">Click here</a>to reset your password.</p> -->
										
								<!-- Two Factor Authentication -->
								</div>
								<?php include $filerootpath.'/verifylogin.php'; ?>
								<input style='height:30px;display:none' class='form-control margin-bottom-20' id='displaytwofactors' name='displaytwofactors' type='text' value="<?php echo $displaytwofactors;?>">
								<input style='height:30px;display:none' class='form-control margin-bottom-20' id='displaylogin' name='displaylogin' type='text' value="<?php echo $displaylogin;?>">
								</form>
                            </div>
                            <!-- End Login Box -->
                        </div>
                    <!--</div>-->
                </div>
		    </div>
