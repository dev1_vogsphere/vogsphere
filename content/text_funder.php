<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="funder"; // Table name 
 $fundername = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search for Funder</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='fundername' name='fundername' placeholder='Funder Name' class='form-control' type='text' value=$fundername>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							// $fundername = $_POST['fundername'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($fundername))
					    {
						$sql = "SELECT * FROM $tbl_name WHERE fundername = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($fundername));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<a class='btn btn-info' href=newfunder.php>Add New Funder</a> <br/> <p></p>";
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Funder Name</b></td>"; 
						echo "<td><b>Contact Name</b></td>";
						echo "<td><b>Phone</b></td>";
						echo "<td><b>Email</b></td>";
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
				
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['fundername']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['contactname']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['email']) . "</td>";  						
						echo "<td valign='top'><a class='btn btn-success' href=editfunder.php?funderid={$row['funderid']}>Edit</a></td><td>
						<a class='btn btn-danger' href=deletefunder.php?funderid={$row['funderid']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 
						// -- echo "<a class='btn btn-info' href=newfunder.php>Add New Funder</a>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->