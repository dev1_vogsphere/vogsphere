<?php
// * To change this template, choose Tools | Templates
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
	require 'database_ffms.php';
	$tbl_customer = "member";
	$tbl_loanapp = "member_policy";
	$dbname = "fin";
    $IntUserCodeB = getsession('UserCode');

	$customerid = null;
	$ApplicationId = null;
	
	$FILEIDDOC = ''; //ID Document
	$FILECONTRACT = ''; //Contract
	$FILEBANKSTATEMENT = '';
	$FILEPROOFEMP = '';
	$FILEFICA = '';
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$FILECREDITREP = '';
	$FILECONSENTFORM = '';
	$FILECREDITCONSENTFORM = '';
	$DEBITORDERCONSENTFORM = '';
	$POWEROFATTORNEY = '';
	$PROOFOFPAYMENT = '';
	$CREDITREPORTXDS = '';
	$CREDITREPORTTPN = '';
	$ACCOUNTSDETAILEDREPORT = '';
	$FILEOTHER 	= '';
	$FILEOTHER1 	= '';
	$FILEOTHER2 = '';
	$FILEOTHER3 	= '';
	$FILEOTHER4 	= '';
	$FILEOTHER5 	= '';
	$FILEOTHER6 	= '';
	$FILEOTHER7 	= '';
	// -- EOC 01.10.2017 - Upload Credit Reports.

	// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
	$FILEDEBITFORM  = '';

	
	$debitform = 'debitform';

	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

	$fieldname = '';
	// -- Documents to be uploaded.
	$ID = 'ID';
	$Contract = 'Contract';
	$BankStatement = 'BankStatement';
	$Employment = 'Employment';
	$residence = 'residence';
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$creditreport = "creditreport";
	$creditconsentfrom = "creditconsentfrom";
	$debitorderconsentfrom="debitorderconsentfrom";
	$powerofattorney="powerofattorney";
	$proofofpayment="proofofpayment";
	$creditreportxds="creditreportxds";
	$creditreporttpn="creditreporttpn";
	$accountsdetailedreport="accountsdetailedreport";
	
	$qareport="qareport";
	$bankstatement2="bankstatement2";
	$expenseslist="expenseslist";

	
	$consentform = "consentform";
	$other = "other";
	$other1= "other1";
	$other2 = "other2";
	$other3 = "other3";
	$other4 = "other4";
	$other5 = "other5";
	$other6 = "other6";
	$other7 = "other7";
	// -- EOC 01.10.2017 - Upload Credit Reports.
	$dataRole = null;
	$role = ""; 
	// -- Role : Customer
	
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	// -- EOC Encrypt 16.09.2017 - Parameter Data.

	// -- I'm coming from
	$comingfrom = $_SERVER['HTTP_REFERER'];
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$CustomerIdEncoded = $customerid;
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationIdEncoded = $ApplicationId;
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	$location = 'index';
	if ( null==$customerid ) 
	    {
		header("Location: ".$location);
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: ".$location);
		}
	 else {
		$pdo = db_connect::db_connection();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.Member_id = ? AND Member_Policy_id = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
// Read the role 
// if role = customer 	and loan status	!= approved 
// To allow the following documents to be uploaded.
// 1. Identity Document (ID),2. Loan POWER OF ATTORNEY {Not allowed},3. One month Bank Statement
// 4. CREDIT REPORT XDS, 5. Proof of residence.
		//$sql = "select * from user where userid = ?";
		//$q = $pdo->prepare($sql);
		//$q->execute(array($customerid));
		//$dataRole = $q->fetch(PDO::FETCH_ASSOC);	
		Database::disconnect();
	}

$valid_formats = array("jpg", "png", "gif", "zip", "bmp","pdf","PDF");
$max_file_size = 1024*10000; //10MB
$path = $filerootpath."/ffms/upload/".$IntUserCodeB."/"; // Upload directory
$count = 0;
if ( !empty($_POST)) 
{
// Stote Values For Later
			if($_POST['documents'] == $ID)
			{
			 $FILEIDDOC = $ID;
			 $fieldname = 'FILEIDDOC';
			}
			
			if($_POST['documents'] == $Contract)
			{
			 $FILECONTRACT = $Contract;
			 $fieldname = 'FILECONTRACT';
			}
			
			if($_POST['documents'] == $BankStatement)
			{
			 $FILEBANKSTATEMENT = $BankStatement;
			 $fieldname = 'FILEBANKSTATEMENT';
			}
			
			if($_POST['documents'] == $Employment)
			{
			 $FILEPROOFEMP = $Employment;
			 $fieldname = 'FILEPROOFEMP';
			}
			
			if($_POST['documents'] == $residence)
			{
			 $FILEFICA = $residence;
			 $fieldname = 'FILEFICA';			 
			}
			
		// -- BOC 01.10.2017 - Upload Credit Reports.
			if($_POST['documents'] == $consentform)
			{
			 $FILECREDITREP = $consentform;
			 $fieldname = 'FILECREDITREP';			 
			}
			if($_POST['documents'] == $creditreport)
			{
			 $FILECONSENTFORM  = $creditreport ;
			 $fieldname = 'FILECONSENTFORM';			 
			}
			if($_POST['documents'] == $creditconsentfrom)
			{
			 $FILECREDITCONSENTFORM  = $creditconsentfrom;
			 $fieldname = 'FILECREDITCONSENTFORM';			 
			}
			if($_POST['documents'] == $debitorderconsentfrom)
			{
			 $DEBITORDERCONSENTFORM  = $debitorderconsentfrom;
			 $fieldname = 'DEBITORDERCONSENTFORM';			 
			}
			if($_POST['documents'] == $powerofattorney)
			{
			 $POWEROFATTORNEY  = $powerofattorney;
			 $fieldname = 'POWEROFATTORNEY';			 
			}
			if($_POST['documents'] == $proofofpayment)
			{
			 $PROOFOFPAYMENT  = $proofofpayment;
			 $fieldname = 'PROOFOFPAYMENT';			 
			}
			if($_POST['documents'] == $creditreportxds)
			{
			 $CREDITREPORTXDS  = $creditreportxds;
			 $fieldname = 'CREDITREPORTXDS';			 
			}
			if($_POST['documents'] == $creditreporttpn)
			{
			 $CREDITREPORTTPN  = $creditreporttpn;
			 $fieldname = 'CREDITREPORTTPN';			 
			}
			if($_POST['documents'] == $accountsdetailedreport)
			{
			 $ACCOUNTSDETAILEDREPORT  = $accountsdetailedreport;
			 $fieldname = 'ACCOUNTSDETAILEDREPORT';			 
			}
			//Q.A REPORT
			if($_POST['documents'] == $qareport)
			{
			 $QAREPORT  = $qareport;
			 $fieldname = 'QAREPORT';			 
			}
			//BANK STATEMENT
			if($_POST['documents'] == $bankstatement2)
			{
			 $BANKSTATEMENT2  = $bankstatement2;
			 $fieldname = 'BANKSTATEMENT2';			 
			}
			//EXPENSES LIST
			if($_POST['documents'] == $expenseslist)
			{
			 $EXPENSESLIST  = $expenseslist;
			 $fieldname = 'EXPENSESLIST';			 
			}
			
			
			if($_POST['documents'] == $other)
			{
			 $FILEOTHER = $other;
			 $fieldname = 'FILEOTHER';			 
			}
			if($_POST['documents'] == $other1)
			{
			 $FILEOTHER1 = $other1;
			 $fieldname = 'FILEOTHER1';			 
			}
			if($_POST['documents'] == $other2)
			{
			 $FILEOTHER2 = $other2;
			 $fieldname = 'FILEOTHER2';			 
			}
			if($_POST['documents'] == $other3)
			{
			 $FILEOTHER3 = $other3;
			 $fieldname = 'FILEOTHER3';			 
			}
			if($_POST['documents'] == $other4)
			{
			 $FILEOTHER4 = $other4;
			 $fieldname = 'FILEOTHER4';			 
			}
			if($_POST['documents'] == $other5)
			{
			 $FILEOTHER5 = $other5;
			 $fieldname = 'FILEOTHER5';			 
			}
			if($_POST['documents'] == $other6)
			{
			 $FILEOTHER6 = $other6;
			 $fieldname = 'FILEOTHER6';			 
			}
			if($_POST['documents'] == $other7)
			{
			 $FILEOTHER7 = $other7;
			 $fieldname = 'FILEOTHER7';			 
			}
		// -- BOC 01.10.2017 - Upload Credit Reports.
		
		// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
			if($_POST['documents'] == $debitform)
			{
			 $FILEDEBITFORM = $debitform;
			 $fieldname = 'FILEDEBITFORM';			 
			}			

			if($_POST['documents'] == $other3)
			{
			 $FILEOTHER3 = $other3;
			 $fieldname = 'FILEOTHER3';			 
			}			

			if($_POST['documents'] == $other4)
			{
			 $FILEOTHER4 = $other4;
			 $fieldname = 'FILEOTHER4';			 
			}				    
		// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

}

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{	
$comingfrom = getpost('comingfrom');	
// Loop $_FILES to execute all files
	foreach ($_FILES['files']['name'] as $f => $name) 
	{     
	    if ($_FILES['files']['error'][$f] == 4) {
	        continue; // Skip file if any error found
	    }	       
	    if ($_FILES['files']['error'][$f] == 0) {	           
	        if ($_FILES['files']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
						// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;
			
			if($_POST['documents'] = $ID)
			{
			 $FILEIDDOC = $name;
			}
			
			if($_POST['documents'] == $Contract)
			{
			 $FILECONTRACT = $name;
			}
			
			if($_POST['documents'] == $BankStatement)
			{
			 $FILEBANKSTATEMENT = $name;
			}
			
			if($_POST['documents'] == $Employment)
			{
			 $FILEPROOFEMP = $name;
			}
			
			if($_POST['documents'] == $residence)
			{
			 $FILEFICA = $name;
			}
				
			// -- BOC 01.10.2017 - Upload Credit Reports.
			if($_POST['documents'] == $creditreport)
			{
				$FILECONSENTFORM = $name;
			}
			if($_POST['documents'] == $creditconsentfrom)
			{
				$FILECREDITCONSENTFORM = $name;
			}
			if($_POST['documents'] == $debitorderconsentfrom)
			{
				$DEBITORDERCONSENTFORM = $name;
			}
			if($_POST['documents'] == $powerofattorney)
			{
				$POWEROFATTORNEY = $name;
			}
			if($_POST['documents'] == $proofofpayment)
			{
				$PROOFOFPAYMENT = $name;
			}
			if($_POST['documents'] == $creditreportxds)
			{
				$CREDITREPORTXDS = $name;
			}
			if($_POST['documents'] == $creditreporttpn)
			{
				$CREDITREPORTTPN = $name;
			}
			if($_POST['documents'] == $accountsdetailedreport)
			{
				$ACCOUNTSDETAILEDREPORT = $name;
			}
			
			if($_POST['documents'] == $consentform)
			{
				$FILECREDITREP  = $name ;
			}
			
			if($_POST['documents'] == $other)
			{
				$FILEOTHER = $name;
			}
			if($_POST['documents'] == $other1)
			{
				$FILEOTHER1 = $name;
			}
			
			if($_POST['documents'] == $other2)
			{
				$FILEOTHER2 = $name;
			}
			// -- EOC 01.10.2017 - Upload Credit Reports.
				
			// -- BOC 24.03.2019 - Upload Debit Order Form, other4.
			if($_POST['documents'] == $debitform)
			{
				$FILEDEBITFORM = $name;
			}
			if($_POST['documents'] == $other3)
			{
				$FILEOTHER3 = $name;
			}
			if($_POST['documents'] == $other4)
			{
				$FILEOTHER4 = $name;
			}
			if($_POST['documents'] == $other5)
			{
				$FILEOTHER5 = $name;
			}
			if($_POST['documents'] == $other6)
			{
				$FILEOTHER6 = $name;
			}
			if($_POST['documents'] == $other7)
			{
				$FILEOTHER7 = $name;
			}
			// -- EOC 24.03.2019 - Upload Debit Order Form, other4.
	
			$sql = "UPDATE member l";
			$sql = $sql." SET $fieldname = ?";
			$sql = $sql." WHERE Member_id = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array('ffms/upload/'.$IntUserCodeB.'/'.$name,$customerid));
			
		// -- Update File Name Directory	
	            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }
				
				// -- BOC Encrypt the uploaded PDF with ID as password.
					$file_parts = pathinfo($name);
					$filenameEncryp = $path.$name;
					switch($file_parts['extension'])
					{
						case "pdf":
						{
							

						break;
						}
						case "PDF":
						{
							/* -- TCPDF library (search for installation path).
							require_once('tcpdf_include.php');
							require_once "FPDI/fpdi.php";
							$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF
							// -- Page Config
								
							// -- Page Config	
							$pdf->setSourceFile($filenameEncryp);
							$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);*/
							
						break;
						}
					}
				// -- EOC Encrypt the uploaded PDF file with ID as password.
	        }
	    }
	}
}
?>

<!--<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Multiple File Upload with PHP - Demo</title> -->
<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Required Documents Checklist</h1>
		<?php
		# error messages
		if (isset($message)) {
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		
		// Refresh Database Data
		$pdo = db_connect::db_connection();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.Member_id = ? AND Member_Policy_id = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		
			printf("<p class='status'>%d files added successfully!</p>\n", $count);
		}
		?>
				<!-- Multiple file upload html form-->
		<form action="" method="post" enctype="multipart/form-data">
		<p>Max file size 10Mb, Valid formats jpg, png, gif, pdf</p>
		<br />
		<!-- <input readonly type="checkbox" name="vehicle" value="ID" class="tick animate fadeInRight"> Identity Documents(ID)<br>
		<input readonly type="checkbox" name="vehicle" value="FICA" checked> CREDIT REPORT TPN<br>
		<input readonly type="checkbox" name="vehicle" value="BANK" checked>1 month Bank Statement<br> 
		-->		
		<ul class="plus animate fadeInRight">
            <li align="left"><b>Indicates Missing document</b></li>
		</ul>
		
		<ul class="tick animate fadeInRight">
            <li align="left"><b>Indicates Uploaded document</b></li>
		</ul>
				
	<?php 
	$FILEIDDOCCHECKED = '';
	$FILECONTRACTCHECKED = '';
	$FILEBANKSTATEMENTCHECKED = '';
	$FILEPROOFEMPCHECKED = '';
	$FILEFICACHECKED = '';
	$QAREPORTCHECKED = ''; 
	$BANKSTATEMENT2CHECKED = ''; 
	$EXPENSESLISTCHECKED = ''; 
	// -- BOC 01.10.2017 - Upload Credit Reports.
	$FILECREDITREPCHECKED = '';
	$FILECONSENTFORMCHECKED = '';
	$FILECREDITCONSENTFORMCHECKED = '';
	$DEBITORDERCONSENTFORMCHECKED = '';
	$POWEROFATTORNEYCHECKED = '';
	$PROOFOFPAYMENTCHECKED = '';
	$CREDITREPORTXDSCHECKED = '';
	$CREDITREPORTTPNCHECKED = '';
	$ACCOUNTSDETAILEDREPORTCHECKED = '';
	$FILEOTHERCHECKED = '';
	$FILEOTHER1CHECKED = '';
	$FILEOTHER2CHECKED = '';
	$FILEOTHER3CHECKED = '';
	$FILEOTHER4CHECKED = '';
	$FILEOTHER5CHECKED = '';
	$FILEOTHER6CHECKED = '';
	$FILEOTHER7CHECKED = '';
	// -- EOC 01.10.2017 - Upload Credit Reports.

	// -- BOC 01.10.2017 - Upload Debit Form.
	$FILEDEBITFORMCHECKED = '';
	// -- EOC 01.10.2017 - Upload Debit Form.

	if($FILEIDDOC 			!= ''){$FILEIDDOCCHECKED='checked';}
	if($FILECONTRACT 		!= ''){$FILECONTRACTCHECKED = 'checked';}
	if($FILEBANKSTATEMENT 	!= ''){$FILEBANKSTATEMENTCHECKED='checked';}
	if($FILEPROOFEMP 		!= ''){$FILEPROOFEMPCHECKED='checked';}
	if($FILEFICA 			!= ''){$FILEFICACHECKED='checked';}
	
	// -- BOC 01.10.2017 - Upload Credit Reports.
	if($FILECREDITREP 			!= ''){$FILECREDITREPCHECKED='checked';}

	if($FILECONSENTFORM 	!= ''){$FILECONSENTFORMCHECKED='checked';}
	if($FILECREDITCONSENTFORM 	!= ''){$FILECREDITCONSENTFORMCHECKED='checked';}
	if($DEBITORDERCONSENTFORM 	!= ''){$DEBITORDERCONSENTFORMCHECKED='checked';}
	if($POWEROFATTORNEY 	!= ''){$POWEROFATTORNEYCHECKED='checked';}
	if($PROOFOFPAYMENT 	!= ''){$PROOFOFPAYMENTCHECKED='checked';}
	if($CREDITREPORTXDS 	!= ''){$CREDITREPORTXDSCHECKED='checked';}
	if($CREDITREPORTTPN 	!= ''){$CREDITREPORTTPNCHECKED='checked';}
	if($ACCOUNTSDETAILEDREPORT 	!= ''){$ACCOUNTSDETAILEDREPORTCHECKED='checked';}
	
	if($QAREPORT 	!= ''){$QAREPORTCHECKED='checked';}
	if($BANKSTATEMENT2 	!= ''){$BANKSTATEMENT2CHECKED='checked';}
	if($EXPENSESLIST 	!= ''){$EXPENSESLISTCHECKED='checked';}
	
	if($FILEOTHER 			!= ''){$FILEOTHERCHECKED='checked';}
	if($FILEOTHER1 			!= ''){$FILEOTHER1CHECKED='checked';}
	if($FILEOTHER2 			!= ''){$FILEOTHER2CHECKED='checked';}
	if($FILEDEBITFORM   	!= ''){$FILEDEBITFORMCHECKED='checked';}
	if($FILEOTHER3 			!= ''){$FILEOTHER3CHECKED='checked';}
	if($FILEOTHER4 			!= ''){$FILEOTHER4CHECKED='checked';}
	if($FILEOTHER5 			!= ''){$FILEOTHER5CHECKED='checked';}
	if($FILEOTHER6 			!= ''){$FILEOTHER6CHECKED='checked';}
	if($FILEOTHER7 			!= ''){$FILEOTHER7CHECKED='checked';}

	// -- BOC 01.10.2017 - Upload Credit Reports.
	
	if($FILEIDDOC == '' and 
	$FILECONTRACT == '' and
	$FILEBANKSTATEMENT == '' and
	$FILEPROOFEMP == '' and
	$FILEFICA == '' and 
	$FILECREDITREP == '' and
	$FILECONSENTFORM == '' and 
	$FILECREDITCONSENTFORM == '' and
	$DEBITORDERCONSENTFORM == '' and
	$POWEROFATTORNEY == '' and
	$PROOFOFPAYMENT == '' and
	$CREDITREPORTXDS == '' and
	$CREDITREPORTTPN == '' and
	$ACCOUNTSDETAILEDREPORT == '' and
	$FILEOTHER == '' and 
	$FILEOTHER1 == '' and 
	$FILEOTHER2 == '' and 
	$FILEDEBITFORM == '' and
	$FILEOTHER3 == '' and
	$FILEOTHER4 == '' and
	$FILEOTHER5 == '' and
	$FILEOTHER6 == '' and
	$FILEOTHER7 == '' 
	){$FILEIDDOCCHECKED='checked';}
	
	// Role and Loan Status
	
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
	if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
	{ 
		if( empty($data['FILEIDDOC'])) // ID document
            {
			$FILEIDDOCCHECKED = '';
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED disabled>ID DOCUMENT</li></ul>";
			}
			else
			{
			$FILEIDDOCCHECKED = '';
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED disabled>ID DOCUMENT</li></ul>";
			}
			
			
	}
	elseif( empty($data['FILEIDDOC'])) // ID document
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED>ID DOCUMENT</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='ID' $FILEIDDOCCHECKED>ID DOCUMENT</li></ul>";
			}
	//EOC (11.10.2016)
	
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['FILECREDITCONSENTFORM'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='creditconsentfrom' $FILECREDITCONSENTFORMCHECKED disabled>CREDIT CONSENT FORM</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='creditconsentfrom' $FILECREDITCONSENTFORMCHECKED disabled>CREDIT CONSENT FORM</li></ul>";
			}
		  }
		  elseif( empty($data['FILECREDITCONSENTFORM'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='creditconsentfrom' $FILECREDITCONSENTFORMCHECKED >CREDIT CONSENT FORM</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='creditconsentfrom' $FILECREDITCONSENTFORMCHECKED >CREDIT CONSENT FORM</li></ul>";
			}
	// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['DEBITORDERCONSENTFORM'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='debitorderconsentfrom' $DEBITORDERCONSENTFORMCHECKED disabled>DEBIT ORDER CONSENT FORM</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='debitorderconsentfrom' $DEBITORDERCONSENTFORMCHECKED disabled>DEBIT ORDER CONSENT FORM</li></ul>";
			}
		  }
		  elseif( empty($data['DEBITORDERCONSENTFORM'])) // Credit Consent Form Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='debitorderconsentfrom' $DEBITORDERCONSENTFORMCHECKED >DEBIT ORDER CONSENT FORM</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='debitorderconsentfrom' $DEBITORDERCONSENTFORMCHECKED >DEBIT ORDER CONSENT FORM</li></ul>";
			}
	//EOC (12.10.2016)
		// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
		  {
			if( empty($data['POWEROFATTORNEY'])) // Contract Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='powerofattorney' $POWEROFATTORNEYCHECKED disabled>POWER OF ATTORNEY</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='powerofattorney' $POWEROFATTORNEYCHECKED disabled>POWER OF ATTORNEY</li></ul>";
			}
		  }
		  elseif( empty($data['POWEROFATTORNEY'])) // Contract Documents
            {
			  echo "<ul class='plus animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='powerofattorney' $POWEROFATTORNEYCHECKED >POWER OF ATTORNEY</li></ul>";
			}
			else
			{
			  echo "<ul class='tick animate fadeInRight'>";
			  echo "<li align='left'><input type='radio' name='documents' value='powerofattorney' $POWEROFATTORNEYCHECKED >POWER OF ATTORNEY</li></ul>";
			}
	//EOC (12.10.2016)
			
		// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{		
				if( empty($data['PROOFOFPAYMENT'])) //  Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='proofofpayment' $PROOFOFPAYMENTCHECKED disabled>PROOF OF PAYMENT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='proofofpayment' $PROOFOFPAYMENTCHECKED disabled>PROOF OF PAYMENT</li></ul>";
				}
			}
			elseif( empty($data['PROOFOFPAYMENT'])) //  Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='proofofpayment' $PROOFOFPAYMENTCHECKED>PROOF OF PAYMENT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='proofofpayment' $PROOFOFPAYMENTCHECKED>PROOF OF PAYMENT</li></ul>";
				}
//EOC (12.10.2016)
// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['CREDITREPORTXDS'])) // FILEPROOFEMP Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreportxds' $CREDITREPORTXDSCHECKED disabled>CREDIT REPORT XDS</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreportxds' $CREDITREPORTXDSCHECKED disabled>CREDIT REPORT XDS</li></ul>";
				}
			}
			elseif( empty($data['CREDITREPORTXDS'])) // FILEPROOFEMP Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreportxds' $CREDITREPORTXDSCHECKED>CREDIT REPORT XDS</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreportxds' $CREDITREPORTXDSCHECKED>CREDIT REPORT XDS</li></ul>";
				}
			//EOC (12.10.2016)	

// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{			
				if( empty($data['CREDITREPORTTPN'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreporttpn' $CREDITREPORTTPNCHECKED disabled>CREDIT REPORT TPN</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreporttpn' $CREDITREPORTTPNCHECKED disabled>CREDIT REPORT TPN</li></ul>";
				}
			}	
			elseif( empty($data['CREDITREPORTTPN'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreporttpn' $CREDITREPORTTPNCHECKED>CREDIT REPORT TPN</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='creditreporttpn' $CREDITREPORTTPNCHECKED>CREDIT REPORT TPN</li></ul>";
				}
				
//EOC (12.10.2016)	
// if Role = "Approved" or "Settled" No ID to be uploaded.(11.10.2016)
			if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{			
				if( empty($data['FILECONTRACT'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Contract' $FILECONTRACTCHECKED disabled>SERVICE AGREEMENT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Contract' $FILECONTRACTCHECKED disabled>SERVICE AGREEMENT</li></ul>";
				}
			}	
			elseif( empty($data['FILECONTRACT'])) // FILEFICA Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Contract' $FILECONTRACTCHECKED>SERVICE AGREEMENT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='Contract' $FILECONTRACTCHECKED>SERVICE AGREEMENT</li></ul>";
				}
		// -- Consent Form
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['ACCOUNTSDETAILEDREPORT'])) // FILECONSENTFORM Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='accountsdetailedreport' $ACCOUNTSDETAILEDREPORTCHECKED disabled>ACCOUNTS DETAILED REPORT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='accountsdetailedreport' $ACCOUNTSDETAILEDREPORTCHECKED disabled>ACCOUNTS DETAILED REPORT</li></ul>";
				}
			}
			elseif( empty($data['ACCOUNTSDETAILEDREPORT'])) // FILECONSENTFORM Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='accountsdetailedreport' $ACCOUNTSDETAILEDREPORTCHECKED>ACCOUNTS DETAILED REPORT</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='accountsdetailedreport' $ACCOUNTSDETAILEDREPORTCHECKED>ACCOUNTS DETAILED REPORT</li></ul>";
			}
			
		// -- Q.A REPORT
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['QAREPORT'])) // FILEOTHER Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='qareport' $QAREPORTCHECKED disabled>Q.A. REPORT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='qareport' $QAREPORTCHECKED disabled>Q.A. REPORT</li></ul>";
				}
			}
			elseif( empty($data['QAREPORT'])) // FILEOTHER Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='qareport' $QAREPORTCHECKED>Q.A. REPORT</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='qareport' $QAREPORTCHECKED>Q.A. REPORT</li></ul>";
			}
		// -- BANK STATEMENT
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['BANKSTATEMENT2'])) // FILEOTHER Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='bankstatement2' $BANKSTATEMENT2CHECKED disabled>BANK STATEMENT</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='bankstatement2' $BANKSTATEMENT2CHECKED disabled>BANK STATEMENT</li></ul>";
				}
			}
			elseif( empty($data['BANKSTATEMENT2'])) // FILEOTHER Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='bankstatement2' $BANKSTATEMENT2CHECKED>BANK STATEMENT</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='bankstatement2' $BANKSTATEMENT2CHECKED>BANK STATEMENT</li></ul>";
			}
		// -- EXPENSES LIST
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['EXPENSESLIST'])) // FILEOTHER Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='expenseslist' $EXPENSESLISTCHECKED disabled>EXPENSES LIST</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='expenseslist' $EXPENSESLISTCHECKED disabled>EXPENSES LIST</li></ul>";
				}
			}
			elseif( empty($data['EXPENSESLIST'])) // FILEOTHER Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='expenseslist' $EXPENSESLISTCHECKED>EXPENSES LIST</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='expenseslist' $EXPENSESLISTCHECKED>EXPENSES LIST</li></ul>";
			}
		
		// -- Other Document
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER1'])) // FILEOTHER Documents
				{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other1' $FILEOTHER1CHECKED disabled>OTHER 1</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other1' $FILEOTHER1CHECKED disabled>OTHER 1</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER1'])) // FILEOTHER Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other1' $FILEOTHER1CHECKED>OTHER 1</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other1' $FILEOTHER1CHECKED>OTHER 1</li></ul>";
			}
		
		// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER2'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other2' $FILEOTHER2CHECKED disabled>OTHER 2</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other2' $FILEOTHER2CHECKED disabled>OTHER 2</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER2'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other2' $FILEOTHER2CHECKED>OTHER 2</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other2' $FILEOTHER2CHECKED>OTHER 2</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER3'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other3' $FILEOTHER3CHECKED disabled>OTHER 3</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other3' $FILEOTHER3CHECKED disabled>OTHER 3</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER3'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other3' $FILEOTHER3CHECKED>OTHER 3</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other3' $FILEOTHER3CHECKED>OTHER 3</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Debit Form
// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER4'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other4' $FILEOTHER4CHECKED disabled>OTHER 4</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other4' $FILEOTHER4CHECKED disabled>OTHER 4</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER4'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other4' $FILEOTHER4CHECKED>OTHER 4</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other4' $FILEOTHER4CHECKED>OTHER 4</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER5'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other5' $FILEOTHER5CHECKED disabled>OTHER 5</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other5' $FILEOTHER5CHECKED disabled>OTHER 5</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER5'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other5' $FILEOTHER5CHECKED>OTHER 5</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other5' $FILEOTHER5CHECKED>OTHER 5</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER6'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other6' $FILEOTHER6CHECKED disabled>OTHER 6</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other6' $FILEOTHER6CHECKED disabled>OTHER 6</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER6'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other6' $FILEOTHER6CHECKED>OTHER 6</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other6' $FILEOTHER6CHECKED>OTHER 6</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	
		// -- Other Document 2
		if($dataRole['role'] == "customer" and ($data['ExecApproval'] == "APPR" or $data['ExecApproval'] == "SET" ))
			{	
				if( empty($data['FILEOTHER7'])) // FILEOTHER2 Documents
				{
				
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other7' $FILEOTHER7CHECKED disabled>OTHER 7</li></ul>";
				}
				else
				{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other7' $FILEOTHER7CHECKED disabled>OTHER 7</li></ul>";
				}
			}
			elseif( empty($data['FILEOTHER7'])) // FILEOTHER2 Documents
			{
				  echo "<ul class='plus animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other7' $FILEOTHER7CHECKED>OTHER 7</li></ul>";
			}
			else
			{
				  echo "<ul class='tick animate fadeInRight'>";
				  echo "<li align='left'><input type='radio' name='documents' value='other7' $FILEOTHER7CHECKED>OTHER 7</li></ul>";
			}
		// -- EOC 01.10.2017 - Upload Credit Reports. ONLY Administrator has access to uploading.	

  		
	?>			
		<br />
		<br />
					<input  type="hidden" name="comingfrom" id="comingfrom" value=<?php echo $comingfrom; ?>/><br> 

			<input type="file" name="files[]" multiple="multiple" accept="*">
			<table class ="table table-user-information">
				  <tr>
					<td>
						<div>
							<!-- <input type="submit" value="Upload"> -->
							<input type="submit" class="btn btn-primary" value="Upload">
						</div>
					</td>
				<td>
				<div>
				<?php if(empty($comingfrom)){$comingfrom = 'readcc.php';} echo '<a class="btn btn-primary" href="'.$comingfrom.'?customerid='.$CustomerIdEncoded.'&ApplicationId='.$ApplicationIdEncoded. '">Back</a>'; ?></div></td>
				</tr>
			</table>
		

		</form>
</div>
</div>

<!-- </body>
</html> -->