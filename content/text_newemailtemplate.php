<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
$tbl_user="smstemplate"; // Table name 
$templatetype = 'email';
//1. ---------------------  Placeholder ------------------- //
$sql = 'SELECT * FROM placeholder';
$dataPlaceholders 	= $pdo->query($sql);						   						 
$count 				= 0;
$placeholder		= '';
$smstemplatename	= '';
$subject = '';
$smstemplatecontent	= '';
$userid 			= '';

// -- Read SESSION tenantid.
 if(isset($_SESSION['userid']))
 {
	$tenantid = $_SESSION['userid'];
	
 }
 else
 {
	 if(isset($_SESSION['username']))
	 {
		 $tenantid = $_SESSION['username'];
	 }
 }

 $userid = $tenantid;
// -- Errors
$placeholderError		 =	'';
$smstemplatenameError	 =	'';
$smstemplatecontentError =	'';
$subjectError = '';
$valid = true;
$smstemplatecontent2 = '';

if (!empty($_POST)) 
{   $count = 0;

$placeholder		=	$_POST['placeholder'];
$smstemplatename	=	$_POST['smstemplatename'];
$smstemplatecontent	=	$_POST['smstemplatecontent'];
$subject	=	$_POST['subject'];

$_SESSION['Editor'] = $smstemplatecontent;
//echo 'SESSION = '.$_SESSION['Editor'];

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////	
if (empty($smstemplatename)) { $smstemplatenameError = $smstemplatenameError.'Please enter email template name'; $valid = false;}

if (empty($subject)) { $subjectError = $subjectError.'Please enter email template subject'; $valid = false;}

if (empty($smstemplatecontent)) { $smstemplatecontentError = $smstemplatecontentError.'Please enter email template content'; $valid = false;}

// -- Check Duplicates.
if($valid == true)
{$valid = CheckSmsTemplateExist($userid,$smstemplatename);}	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								smstemplate  VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if ($valid) 
	{	
			AddSMSTemplate($smstemplatename,$smstemplatecontent,$userid,$subject);
			Database::disconnect();
			$count = $count + 1;
	}
}
else	
{
	//$smstemplatecontent	 =	'smstemplatecontent';
	//$smstemplatecontent2 =  'smstemplatecontent2';
}
// -- BOC - Check if Check Sms Template Exist.
function CheckSmsTemplateExist($tenantid,$smstemplatename)
{
	Global $smstemplatenameError;	
	$lc_valid 	  = true;	
	$tbl 	      = 'smstemplate';
	
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			  
	// ---------------- Check Userid --------------- //
	$query = "SELECT * FROM $tbl WHERE tenantid = ? AND smstemplatename = ?";
	$q = $pdo->prepare($query);
	$q->execute(array($tenantid,$smstemplatename));
	if ($q->rowCount() >= 1)
	{$smstemplatenameError = 'SMS Template Name already exists.'; $lc_valid = false;}
	return $lc_valid;	
}
// -- Add SMS Template.
function AddSMSTemplate($smstemplatename,$smstemplatecontent,$userid,$subject)
{
	Global $templatetype;
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "INSERT INTO smstemplate(smstemplatename, smstemplatecontent,tenantid,templatetype,subject) VALUES (?,?,?,?,?)";
	$q = $pdo->prepare($sql);
	$q->execute(array($smstemplatename,$smstemplatecontent,$userid,$templatetype,$subject));
}
// -- EOC - 
?>
<!--
<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="index">Home</a></li>
			<li><a href="smstemplates">Email Template</a></li>
		</ol>
	</div>
</div> -->
<div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
	  <div class="row">
		<form  name="form1" action='' method='POST' class="signup-page">   
<!-- <div class="panel-heading"> -->
<!-- <h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New smstemplate  Details
                    </a>
                </h2> -->
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Email template  successfully added.</p>");
		}
?>
				
<!-- </div>-->	
<!-- 1. Email Template -->
		<div class="form-group <?php if (!empty($smstemplatenameError)){ echo "has-error";}?>">						
			<p><b>emailtemplatename</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='smstemplatename' id="smstemplatename" value="<?php echo !empty($smstemplatename)?$smstemplatename :'';?>"/>
			<?php if (!empty($smstemplatenameError)): ?>
			<small class="help-block"><?php echo $smstemplatenameError;?></small>
			<?php endif; ?>
			</p>
		</div>	
<!-- 2. Email Subject Template -->
		<div class="form-group <?php if (!empty($subjectError)){ echo "has-error";}?>">						
			<p><b>Subject</b><br />
			<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='subject' id="subject" value="<?php echo !empty($subject)?$subject :'';?>"/>
			<?php if (!empty($subjectError)): ?>
			<small class="help-block"><?php echo $subjectError;?></small>
			<?php endif; ?>
			</p>
		</div>	
		
			<p><b>Placeholders</b><br />
				<SELECT class="form-control" id="placeholder" name="placeholder" size="1">
					<?php
						$placeholderLoc = '';
						$placeholderSelect = $placeholder;
						foreach($dataPlaceholders as $row)
						{
							$placeholderLoc = $row['placeholder'];

							if($placeholderSelect == $placeholderSelect)
							{
								echo "<OPTION value='".$placeholderLoc."' selected>$placeholderLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value='".$placeholderLoc."'>$placeholderLoc</OPTION>";
							}
						}
												
						if(empty($dataPlaceholders))
						{
							echo "<OPTION value=0>No placeholders</OPTION>";
						}
						

					?>
					
					</SELECT>
			</p>
			<div class="form-group <?php if (!empty($smstemplatecontentError)){ echo "has-error";}?>">						
				<p>
					Click to add <input onclick='input()' type='button' value='Place holder' id='button'><br>
					<textarea class="form-control" rows="10" name="smstemplatecontent" id="smstemplatecontent" style="display:none"><?php echo $smstemplatecontent; ?></textarea>
					<?php if (!empty($smstemplatecontentError)): ?>
							<small class="help-block"><?php echo $smstemplatecontentError;?></small>
					<?php endif; ?>
				</p>
			</div>			
			<div class="container-fluid">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 nopadding">
								<textarea id="txtEditor" name="txtEditor"><?php echo $smstemplatecontent;?></textarea> 
								<!-- <textarea id="smstemplatecontent" name="smstemplatecontent" $smstemplatecontent2.$smstemplatename style="display:none"></textarea> -->	
							</div>
						</div>
					</div>
				</div>
			</div>  
			
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'emailtemplates';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			//echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				//<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>
  <button class='btn btn-primary' type='submit' onclick="SubmitEmailTemplate()">Save</button><!-- Generate OTP -->

  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>	
</div>
<script>
function input()
{
	var title = document.getElementById('placeholder').value;
	var area = document.forms.form1.smstemplatecontent.value;
	//alert(area);

	//var EmailWordFormat = document.forms.form1.txtEditor.value;
    document.forms.form1.smstemplatecontent.value = area + title;
	//document.forms.form1.txtEditor.value = "Modise" + EmailWordFormat + title;
	
	//alert(document.forms.form1.smstemplatecontent.value);
	UpdateEmailTemplate();
}    

function UpdateEmailTemplate()
{
// -- Email Template.
	var val1 = document.getElementById("smstemplatecontent").value;  
			//alert(val1);

	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = document.getElementById("smstemplatecontent").value;
		//alert(val1);
		jQuery("#txtEditor").Editor("setText", val1);
				
	});	
	
	jQuery(document).ready(function() 
	{

// -- Email Template.
		val1 = jQuery("#txtEditor").Editor("getText");
		
		document.getElementById("smstemplatecontent").value = val1;		
	});
} 

function SubmitEmailTemplate()
{
	var val1 = document.getElementById("smstemplatecontent").value;  

  jQuery(document).ready(function() 
	{
// -- Email Template.
		val1 = jQuery("#txtEditor").Editor("getText");
		document.getElementById("smstemplatecontent").value = val1;		
	});

// -- Email Template.	
	jQuery(document).ready(function() 
	{
// -- Email Template.
		val = document.getElementById("smstemplatecontent").value;
		val1 = jQuery("#txtEditor").Editor("getText");
		
		jQuery("#txtEditor").Editor("setText", val);
		
		//val = document.getElementById("smstemplatecontent2").value;
		val = jQuery("#txtEditor").Editor("getText");
	});	
} 

</script>