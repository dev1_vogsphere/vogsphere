<?php 
require 'database.php';
$branchcode = 0;
$branchcodeError = null;

$branchdescError = null;
$branchdesc = '';

$bankidError = null;
$BankName = '';

$count = 0;

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 
	Database::disconnect();	
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

if (!empty($_POST)) 
{   $count = 0;
	$branchdesc = $_POST['branchdesc'];
	$BankName = $_POST['BankName'];
	$branchcode = $_POST['branchcode'];
	
	$valid = true;
	
	if (empty($branchdesc)) { $branchdescError = 'Please enter Bank Branch Description.'; $valid = false;}

	if (empty($branchcode)) { $branchcodeError = 'Please enter Branch Code.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO bankbranch  (branchcode,bankid, branchdesc) VALUES (?,?,?)"; 	
			$q = $pdo->prepare($sql);
			$q->execute(array($branchcode,$BankName,$branchdesc));
			Database::disconnect();
			$count = $count + 1;
		}
}
?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
  
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Bank Branch Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Bank Branch successfully added.</p>");
		}
?>
				
</div>				
			<p><b>Branch Code</b><br />
			<input style="height:30px" type='text' name='branchcode' value="<?php echo !empty($branchcode)?$branchcode:'';?>"/>
			<?php if (!empty($branchcodeError)): ?>
			<span class="help-inline"><?php echo $branchcodeError;?></span>
			<?php endif; ?>
			</p> 
			<p><b>Bank Branch Description</b><br />
			<input style="height:30px" type='text' name='branchdesc' value="<?php echo !empty($branchdesc)?$branchdesc:'';?>"/>
			<?php if (!empty($branchdescError)): ?>
			<span class="help-inline"><?php echo $branchdescError;?></span>
			<?php endif; ?>
			</p> 
			<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'bankbranch';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
