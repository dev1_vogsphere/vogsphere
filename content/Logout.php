<?php  
$userid = '';
if(isset($_SESSION['username']))
{
	$userid = $_SESSION['username'];
}
require 'database.php';
session_start();//session is a way to store information (in variables) to be used across multiple pages. 
session_unset(); 
session_destroy();  
$url = "customerLogin";
if(isset($_GET["session_expired"])) 
{
	$url .= "?session_expired=" . $_GET["session_expired"];
}
// -- Audit Log. 
$arrayLog   	  = array();
$arrayLog[] 	  = 'Active';
$arrayLog[] 	  =  $userid;	
$dataAuditLogUser = GetAuditActiveUser($arrayLog);
// -- Update the Audit Log.
$arrayLogOut      = array();
if(isset($dataAuditLogUser['audittrailid']))
{
	$logoutdatetime = date("Y-m-d H:i:s");
	$audittrailid   = $dataAuditLogUser['audittrailid'];
	$arrayLogOut[] = 'Inactive';
	$arrayLogOut[] = $logoutdatetime;
	$arrayLogOut[] = $audittrailid;
	UpdateAuditLog($arrayLogOut);
}
echo "<script>alert('modise');</script>";
header("Location:$url");
?>  