<?php
require 'database.php';
// -- Database Declarations and config:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$tbl_user="client"; // Table name
$ID = '';
$count = 0;
$valid = true;
// -- Variables & Errors variables.
$Description 			= '';$DescriptionError 			= '';
$username 				= '';$usernameError 				= '';
$password 				= '';$passwordError 				= '';

$usercode 				= '';$usercodeError 				= '';
$debicheckusercode 		= '';$debicheckusercodeError 		= ''; 
$aggregate_limits_debits = '';$aggregate_limits_debitsError = '';  

$aggregate_limits_naedo  = '';$aggregate_limits_naedoError  = ''; 
$aggregate_limits_cards  = '';$aggregate_limits_cardsError  = '';  
$aggregate_limits_credits = '';$aggregate_limits_creditsError = ''; 

$item_limits_debits 	  = '';$item_limits_debitsError 	  = '';
$item_limits_naedo 		  = '';$item_limits_naedoError 		  = '';	
$item_limits_cards 		  = '';$item_limits_cardsError 		  = '';	 

$item_limits_credits 	  = '';$item_limits_creditsError 	  = '';
$urlqadebicheck 		  = '';$urlqadebicheckError 		  = '';
$urlproddebicheck 		  = '';$urlproddebicheckError 		  = '';		 
$urlqamercantile 		  = '';$urlqamercantileError 		  = '';

$urlprodmercantile 		  = '';$urlprodmercantileError 		  = '';	 
$abbreviatedshortname	  = '';$abbreviatedshortnameError	  = '';
$security 				  = '';$securityError 				  = '';

$shortname 				  = '';$shortnameError 				  = '';  
$Intusercode 			  = '';$IntusercodeError 			  = ''; 
$paymentusercode 		  = '';$paymentusercodeError 		  = '';

  if (isset($_GET['ID']) )
  {
	$ID = $_GET['ID'];
  }
  
  // -- Form Posting Data...
if ( !empty($_POST))
{
	$Description 			  = getpost('Description') 			 ;
	$username 				  = getpost('username') 			 ;
	$password 				  = getpost('password') 			 ;
	$usercode 				  = getpost('usercode') 			 ;
	$debicheckusercode 		  = getpost('debicheckusercode') 			 ;
	$aggregate_limits_debits  = getpost('aggregate_limits_debits') 			 ;
	$aggregate_limits_naedo   = getpost('aggregate_limits_naedo') 			 ;  
	$aggregate_limits_cards   = getpost('aggregate_limits_cards') 			 ;  
	$aggregate_limits_credits = getpost('aggregate_limits_credits') 			 ;
	$item_limits_debits 	  = getpost('item_limits_debits') 			 ; 	 
	$item_limits_naedo 		  = getpost('item_limits_naedo') 			 ; 		 
	$item_limits_cards 		  = getpost('item_limits_cards') 			 ; 		 
	$item_limits_credits 	  = getpost('item_limits_credits') 			 ; 	 
	$urlqadebicheck 		  = getpost('urlqadebicheck') 			 ; 		 
	$urlproddebicheck 		  = getpost('urlproddebicheck') 			 ; 		 
	$urlqamercantile 		  = getpost('urlqamercantile') 			 ; 		 
	$urlprodmercantile 		  = getpost('urlprodmercantile') 			 ; 		 
	
	$abbreviatedshortname	  = getpost('abbreviatedshortname') 			 ;	 
	$security 				  = getpost('security') 			 ; 				 
	$shortname 				  = getpost('shortname') 			 ; 				 
	$Intusercode 			  = getpost('Intusercode') 			 ; 			 
	$paymentusercode 		  = getpost('paymentusercode') 			 ;

// -- Error Validation
	$valid = true;
	if (empty($Description)) { $DescriptionError = 'Please enter description'; $valid = false;}
	//if (empty($username)) { $usernameError = 'Please enter username'; $valid = false;}
	//if (empty($password)) { $passwordError = 'Please enter password'; $valid = false;}

	//if (empty($usercode)) { $usercodeError = 'Please enter usercode'; $valid = false;}
	//if (empty($debicheckusercode)) { $debicheckusercodeError = 'Please enter debicheckusercode'; $valid = false;}

if(empty($aggregate_limits_debits)){$aggregate_limits_debits = '0.00';}
if(empty($aggregate_limits_naedo)){$aggregate_limits_naedo = '0.00';}
if(empty($aggregate_limits_cards)){$aggregate_limits_cards = '0.00';}
if(empty($aggregate_limits_credits)){$aggregate_limits_credits = '0.00';}

if(empty($item_limits_debits)){$item_limits_debits = '0.00';}
if(empty($item_limits_naedo)){$item_limits_naedo = '0.00';}
if(empty($item_limits_cards)){$item_limits_cards = '0.00';}
if(empty($item_limits_credits)){$item_limits_credits = '0.00';}

	/*
	if (empty($urlqadebicheck)) { $urlqadebicheckError = 'Please enter urlqadebicheck'; $valid = false;}
	if (empty($urlproddebicheck)) { $urlproddebicheckError = 'Please enter urlproddebicheck'; $valid = false;}
	if (empty($urlqamercantile)) { $urlqamercantileError = 'Please enter urlqamercantile'; $valid = false;}
	if (empty($urlprodmercantile)) { $urlprodmercantileError = 'Please enter urlprodmercantile'; $valid = false;}
	*/
	if (empty($abbreviatedshortname)) { $abbreviatedshortnameError = 'Please enter abbreviatedshortname'; $valid = false;}
	if(empty($security)){$security = '0.00';}
	if (empty($shortname)) { $shortnameError = 'Please enter shortname'; $valid = false;}
	if (empty($Intusercode)) { $IntusercodeError = 'Please enter Intusercode'; $valid = false;}
	//if (empty($paymentusercode)) { $paymentusercodeError = 'Please enter paymentusercode'; $valid = false;}

// -- Valid Create Entry in the table... -- //
// ---------------------------------------------------------------------
	if($valid)
	{

// -- Build Staging Data for restoration if request is rejected.
// -- Insert SQL statement
$sql = 
"INSERT INTO client
(Description,username,password,usercode,debicheckusercode,
aggregate_limits_debits,aggregate_limits_naedo,aggregate_limits_cards,aggregate_limits_credits,
item_limits_debits,item_limits_naedo,item_limits_cards,item_limits_credits,
urlqadebicheck,urlproddebicheck,urlqamercantile,urlprodmercantile,
abbreviatedshortname,security,shortname,Intusercode,paymentusercode)
VALUES
(?,?,?,?,?,
?,?,?,?,
?,?,?,?,
?,?,?,?,
?,?,?,?,?)";
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$q = $pdo->prepare($sql);
			$q->execute(array(
			$Description, 			 
			$username, 				 
			$password, 				 
			$usercode, 				 
			$debicheckusercode, 		 
			$aggregate_limits_debits, 
			$aggregate_limits_naedo,  
			$aggregate_limits_cards,  
			$aggregate_limits_credits,
			$item_limits_debits, 	 
			$item_limits_naedo, 		 
			$item_limits_cards, 		 
			$item_limits_credits, 	 
			$urlqadebicheck, 		 
			$urlproddebicheck, 		 
			$urlqamercantile, 		 
			$urlprodmercantile, 		 
			$abbreviatedshortname,	 
			$security, 				 
			$shortname, 				 
			$Intusercode, 			 
			$paymentusercode)); 		 
			Database::disconnect();
			$count = $count + 1;
	}
}  

?>
<div class="container background-white bottom-border">
<div class='row'>
		<form action='' method='POST' class="signup-page">
 <div class="table-responsive">
<table class="table table-user-information">
<!-- Customer Details -->
<!-- Title,FirstName,LastName -->

<tr>
	<!--<td><div><a class="btn btn-primary" href="custAuthenticate">Back</a></div></td> -->
	<td><div><button type="submit" class="btn btn-primary">Save</button></div></td>
								<td>
								<a class="btn btn-primary" href="clients">Back</a>
							</td>	

</tr>
<tr>
<td colspan="3">
<div class="panel-heading">

				<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New Client Details
                    </a>
                </h2>
</div>

<?php # error messages
		if (isset($message))
		{
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>Information updated successfully!</p>\n", $count);
		}
		// -- if there any error display here.
		else
		{
				// keep track validation errors - 		// Customer Details
//if(!empty($DescriptionError) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
//if(!empty($usernameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
//if(!empty($LastNameError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StreetError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($SuburbError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($CityError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($StateError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($PostCodeError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($DobError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($phoneError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}else
if(!empty($emailError ) ){printf("<div class=alert alert-warning'><strong>Warning!</strong><p class='alert alert-warning'>Failed to update has error(s) see below!</p></div>\n");}
		}
		?>

</td>
</tr>
<tr>
  <td>
	<div class="control-group <?php echo !empty($DescriptionError)?'error':'';?>">
		<label class="control-label">Description</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="Description" name="Description" type="text"  placeholder="Description" value="<?php echo !empty($Description)?$Description:'';?>">
			<?php if (!empty($DescriptionError)): ?>
			<span class="help-inline"><?php echo $DescriptionError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($usernameError)?'error':'';?>">
		<label class="control-label">Username</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="username" name="username" maxlength="4" type="text"  placeholder="username"
			value="<?php echo !empty($username)?$username:'';?>">
			<?php if (!empty($usernameError)): ?>
			<span class="help-inline"><?php echo $usernameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($passwordError)?'error':'';?>">
		<label class="control-label">password</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="password" name="password" type="password"  placeholder="password" value="<?php echo !empty($password)?$password:'';?>">
			<?php if (!empty($passwordError)): ?>
			<span class="help-inline"><?php echo $passwordError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

</tr>
<tr>
  <td>
	<div class="control-group <?php echo !empty($usercodeError)?'error':'';?>">
		<label class="control-label">usercode</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="usercode" name="usercode" type="text"  placeholder="usercode" maxlength="4" value="<?php echo !empty($usercode)?$usercode:'';?>">
			<?php if (!empty($usercodeError)): ?>
			<span class="help-inline"><?php echo $usercodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($debicheckusercodeError)?'error':'';?>">
		<label class="control-label">debicheckusercode</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="debicheckusercode" name="debicheckusercode" type="text"  placeholder="debicheckusercode"
			value="<?php echo !empty($debicheckusercode)?$debicheckusercode:'';?>">
			<?php if (!empty($debicheckusercodeError)): ?>
			<span class="help-inline"><?php echo $debicheckusercodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($aggregate_limits_debitsError)?'error':'';?>">
		<label class="control-label">aggregate_limits_debits</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_debits" name="aggregate_limits_debits" type="number" placeholder="0.00"  min="0.00" value="<?php echo !empty($aggregate_limits_debits)?$aggregate_limits_debits:'';?>">
			<?php if (!empty($aggregate_limits_debitsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_debitsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

</tr>
<tr>
  <td>
	<div class="control-group <?php echo !empty($aggregate_limits_naedoError)?'error':'';?>">
		<label class="control-label">aggregate_limits_naedo</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="aggregate_limits_naedo" name="aggregate_limits_naedo" type="number"  min="0.00" placeholder="0.00" value="<?php echo !empty($aggregate_limits_naedo)?$aggregate_limits_naedo:'';?>">
			<?php if (!empty($aggregate_limits_naedoError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_naedoError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($aggregate_limits_cardsError)?'error':'';?>">
		<label class="control-label">aggregate_limits_cards</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_cards" name="aggregate_limits_cards" type="number"  min="0.00" placeholder="0.00" value="<?php echo !empty($aggregate_limits_cards)?$aggregate_limits_cards:'';?>">
			<?php if (!empty($aggregate_limits_cardsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_cardsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($aggregate_limits_creditsError)?'error':'';?>">
		<label class="control-label">aggregate_limits_credits</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="aggregate_limits_credits" name="aggregate_limits_credits" type="number"  placeholder="0.00" min="0.00"
			value="<?php echo !empty($aggregate_limits_credits)?$aggregate_limits_credits:'';?>">
			<?php if (!empty($aggregate_limits_creditsError)): ?>
			<span class="help-inline"><?php echo $aggregate_limits_creditsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>
<tr>
  <td>
	<div class="control-group <?php echo !empty($item_limits_debitsError)?'error':'';?>">
		<label class="control-label">item_limits_debits</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="item_limits_debits" name="item_limits_debits" type="number" placeholder="0.00" min="0.00" value="<?php echo !empty($item_limits_debits)?$item_limits_debits:'';?>">
			<?php if (!empty($item_limits_debitsError)): ?>
			<span class="help-inline"><?php echo $item_limits_debitsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($item_limits_naedoError)?'error':'';?>">
		<label class="control-label">item_limits_naedo</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="item_limits_naedo" name="item_limits_naedo" type="number" placeholder="0.00"  min="0.00" value="<?php echo !empty($item_limits_naedo)?$item_limits_naedo:'';?>">
			<?php if (!empty($item_limits_naedoError)): ?>
			<span class="help-inline"><?php echo $item_limits_naedoError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($item_limits_cardsError)?'error':'';?>">
		<label class="control-label">item_limits_cards</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="item_limits_cards" name="item_limits_cards" type="number" placeholder="0.00"  min="0.00"
			value="<?php echo !empty($item_limits_cards)?$item_limits_cards:'';?>">
			<?php if (!empty($item_limits_cardsError)): ?>
			<span class="help-inline"><?php echo $item_limits_cardsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>

<tr>
  <td>
	<div class="control-group <?php echo !empty($item_limits_creditsError)?'error':'';?>">
		<label class="control-label">item_limits_credits</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="item_limits_credits" name="item_limits_credits" type="number" placeholder="0.00" min="0.00" value="<?php echo !empty($item_limits_credits)?$item_limits_credits:'';?>">
			<?php if (!empty($item_limits_creditsError)): ?>
			<span class="help-inline"><?php echo $item_limits_creditsError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($urlqadebicheckError)?'error':'';?>">
		<label class="control-label">urlqadebicheck</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="urlqadebicheck" name="urlqadebicheck" type="text"  placeholder="urlqadebicheck" value="<?php echo !empty($urlqadebicheck)?$urlqadebicheck:'';?>">
			<?php if (!empty($urlqadebicheckError)): ?>
			<span class="help-inline"><?php echo $urlqadebicheckError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($urlproddebicheckError)?'error':'';?>">
		<label class="control-label">urlproddebicheck</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="urlproddebicheck" name="urlproddebicheck" type="text" placeholder="urlproddebicheck"
			value="<?php echo !empty($urlproddebicheck)?$urlproddebicheck:'';?>">
			<?php if (!empty($urlproddebicheckError)): ?>
			<span class="help-inline"><?php echo $urlproddebicheckError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>
<tr>
  	<td>
	<div class="control-group <?php echo !empty($urlqamercantileError)?'error':'';?>">
		<label class="control-label">urlqamercantile</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="urlqamercantile" name="urlqamercantile" type="text"  placeholder="urlqamercantile" value="<?php echo !empty($urlqamercantile)?$urlqamercantile:'';?>">
			<?php if (!empty($urlqamercantileError)): ?>
			<span class="help-inline"><?php echo $urlqamercantileError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td>
	<div class="control-group <?php echo !empty($urlprodmercantileError)?'error':'';?>">
		<label class="control-label">urlprodmercantile</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="urlprodmercantile" name="urlprodmercantile" type="text"  placeholder="urlprodmercantile" value="<?php echo !empty($urlprodmercantile)?$urlprodmercantile:'';?>">
			<?php if (!empty($urlprodmercantileError)): ?>
			<span class="help-inline"><?php echo $urlprodmercantileError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($abbreviatedshortnameError)?'error':'';?>">
		<label class="control-label">abbreviatedshortname</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="abbreviatedshortname" name="abbreviatedshortname" type="text" placeholder="abbreviatedshortname"
			value="<?php echo !empty($abbreviatedshortname)?$abbreviatedshortname:'';?>">
			<?php if (!empty($abbreviatedshortnameError)): ?>
			<span class="help-inline"><?php echo $abbreviatedshortnameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($securityError)?'error':'';?>">
		<label class="control-label">security</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="security" name="security" type="number"  placeholder="0.00" value="<?php echo !empty($security)?$security:'';?>">
			<?php if (!empty($securityError)): ?>
			<span class="help-inline"><?php echo $securityError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>

	<td>
	<div class="control-group <?php echo !empty($shortnameError)?'error':'';?>">
		<label class="control-label">shortname</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="shortname" name="shortname" type="text" placeholder="shortname"
			value="<?php echo !empty($shortname)?$shortname:'';?>">
			<?php if (!empty($shortnameError)): ?>
			<span class="help-inline"><?php echo $shortnameError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	
	<td>
	<div class="control-group <?php echo !empty($IntusercodeError)?'error':'';?>">
		<label class="control-label">Intusercode</label>
		<div class="controls">
			<input style="height:30px" class="form-control margin-bottom-10" id="Intusercode" name="Intusercode" type="text" maxlength='4' placeholder="Intusercode" value="<?php echo !empty($Intusercode)?$Intusercode:'';?>">
			<?php if (!empty($IntusercodeError)): ?>
			<span class="help-inline"><?php echo $IntusercodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
</tr>
<tr>
	<td>
	<div class="control-group <?php echo !empty($paymentusercodeError)?'error':'';?>">
		<label class="control-label">paymentusercode</label>
		<div class="controls">
			<input class="form-control margin-bottom-10" id="paymentusercode" name="paymentusercode" type="text"  placeholder="paymentusercode" value="<?php echo !empty($paymentusercode)?$paymentusercode:'';?>">
			<?php if (!empty($paymentusercodeError)): ?>
			<span class="help-inline"><?php echo $paymentusercodeError;?></span>
			<?php endif; ?>
		</div>
	</div>
	</td>
	<td></td>
	<td></td>
</tr>
</table>
<table class="table table-user-information">
<!-- Buttons -->
						<tr>
							<!--<td><div>
								<a class="btn btn-primary" href="custAuthenticate">Back</a>
							</div> -->
							</td>
							<td>
							<div>
							  	<button type="submit" class="btn btn-primary">Save</button>
							</div>
							</td>
							<td>
								<a class="btn btn-primary" href="clients">Back</a>
							</td>	
						</tr>
						</table>
		</form>
	</div>
</div></div>