<?php
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($_SESSION['role'] != 'admin')
{
	//header("Location: customerLogin");
}

$FromDate= '';
$ToDate='';
$sessionrole='';

$To = '';
$From = '';

 $Allstatus = '';
 $ExecApproval    = '';

 $AllServiceMode = '';
 $ServiceMode    = '';

 $AllServiceType = '';
 $ServiceType    = '';

 $customerid = '';
 $customeridTemp = '';

// -- Generate Client ID from AccountNumber.
if(!function_exists('GenerateClientId'))
{
	function GenerateClientId($customerid)
	{
	   $AccountNumber = '';

	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);

		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9);
	   }
	   return $AccountNumber;
	}
}
if(isset($_GET['FromDate']))
{
	$FromDate=$_GET['FromDate'];
}

if(isset($_GET['ToDate']))
{
	$ToDate=$_GET['ToDate'];
}

if(isset($_SESSION['role']))
{
	$sessionrole=$_SESSION['role'];
}

// -- Filter Records.
if ( !empty($_POST))
{
	// -- Date Range.
	$From = getpost('From');
	$To   = getpost('To');
	// -- Status.
	$ExecApproval   = getpost('ExecApproval');
	// -- Account revered to be customerid.
	$customerid = getpost('customerid');
	if(!empty($customerid))
	{
		$customeridTemp = getpost('customerid');
		$customerid = GenerateClientId($customerid);
	}
}
else
{
	/*$sql = "SELECT * FROM loanapp as LA join customer as C on LA.CustomerId = C.CustomerId where ExecApproval NOT IN ('REH') order by DateAccpt desc ";
	$q = $pdo->prepare($sql);
	$q->execute();
	$data = $q->fetchAll();
	Database::disconnect();*/
}

// -- Reload the elements of the page.
	 $Approved = '';
	 $Pending = '';
	 $Cancelled = '';
	 $Rejected = '';
	 $All = '';
	 $Settled = '';
	 $Defaulted = '';
	 $REH = '';
	 $LEGAL = '';
	 $Fund  = '';

	if($ExecApproval == 'SET')
		{
		  $Settled = 'selected';
		}

	if($ExecApproval == 'APPR')
		{
		  $Approved = 'selected';
		}
	if($ExecApproval == 'PEN')
		{
		   $Pending = 'selected';
		}
	if($ExecApproval == 'CAN')
		{
			$Cancelled = 'selected';
		}
	if($ExecApproval == 'REJ')
		{
		   $Rejected = 'selected';
		}
	if($ExecApproval == 'All')
		{
		   $All = 'selected';
		}

// -- Defaulter - 20.06.2018.
	if($ExecApproval == 'DEF')
		{
		   $Defaulted = 'selected';
		}

// -- Legal & Rehab - 13.12.2018.
	if($ExecApproval == 'REH')
		{
		   $REH = 'selected';
		}

		if($ExecApproval == 'LEGAL')
		{
		   $LEGAL = 'selected';
		}
$ExecApproval = 'FUND';
	if($ExecApproval == '')
		{
		   $All = 'selected';
		}

	  if($To == "")
	   {
		$To = "9999-12-31";
	   }
// -- Apply the actual filter/search.
$tablename = 'loanapp';
$dbnameobj = 'ecashpdq_eloan';
$whereArray = null;
$queryFields[] = '*';

// -- status
if(!empty($ExecApproval))
{
	$whereArray[] = "ExecApproval = '{$ExecApproval}'";
}
else
{
	$whereArray[] = "ExecApproval NOT IN ('REH')";
}

// -- customerid
if(!empty($customerid))
{
	$whereArray[] = "CustomerId LIKE '%{$customerid}'";
	$customerid = $customeridTemp;

}
// from date
if(!empty($From))
{
	$whereArray[] = "DATE(DateAccpt) BETWEEN '{$From}' AND '{$To}'"; // -- Client Reference.
}
// -- dynamic search.
$dataQ = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);

$data = $dataQ->fetchAll(PDO::FETCH_ASSOC);
//print_r($data);
// -- if record were brought.
// -- build a list of customerid.
$customerid_list = '';
foreach($data as $row)
{
	if(empty($customerid_list))
	{	$customerid_list = "'".$row['CustomerId']."'";}
    else
	{	$customerid_list = $customerid_list.","."'".$row['CustomerId']."'";}
}
// -- dynamic search for customer data.
//print($customerid_list);
$dataCustomer = null;
$dataCustomerQ = null;

if(!empty($customerid_list))
{
	$tablename = 'customer';
	$whereArray = null;
	$queryFields = null;
	$queryFields[] = 'CustomerId';
	$queryFields[] = 'Title';
	$queryFields[] = 'FirstName';
	$queryFields[] = 'LastName';
	$whereArray[] = "CustomerId IN ({$customerid_list})";
	//print_r($whereArray);
	$dataCustomerQ = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
	$dataCustomer = $dataCustomerQ->fetchAll(PDO::FETCH_ASSOC);
}
//print_r($dataCustomer);

//print_r($dataCustomer);

// -- Get Loan Charged Fees
if(!function_exists('getloanchargedfees'))
{
	function getloanchargedfees($appid,$pdo)
	{
			$sql = "SELECT sum(amount) as amount FROM loanappcharges as LC join charge as C on LC.chargeid = C.chargeid  where applicationid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($appid));
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(isset($data['amount']))
			{
				return $data['amount'];
			}
			else
			{
				return 0.00;
			}
			Database::disconnect();
	}
}
// -- Generate AccountNumber.
if(!function_exists('GenerateAccountNumber'))
{
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';

	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);

		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9);
	   }
	   return $AccountNumber;
	}
}
// -- predicated_loan_values..
if(!function_exists('predicated_loan_values'))
{
	function predicated_loan_values($current_month,$repaymentAmount,$customerid,$ApplicationId,$monthlypayment,$issued_loan,$targeted_rev,$projected_rev)
	{
		$tar = ($monthlypayment + ($monthlypayment * 0.30));
		$total = payment_values($current_month,$repaymentAmount,$customerid,$ApplicationId);
		$arr = null;
		$arr[] = (float)$issued_loan + (float)$monthlypayment;
		$arr[] = $targeted_rev + (float)$tar;
		$arr[] = $projected_rev + (float)$monthlypayment;
		$arr[] = (float)$targeted_rev  - $issued_loan;
		$arr[] = (float)$total;		
		return $arr;
	}
}
if(!function_exists('print_values'))
{
	function print_values($current_month,$issued_loan,$targeted_rev,$projected_rev,$actual_rev,$profit)
	{
		$print = null;
		
	    $issued_loan = floatval($issued_loan);
        $print[] =  number_format($issued_loan,2,",",".");

	    $targeted_rev = floatval($targeted_rev);
        $print[]  =  number_format($targeted_rev,2,",",".");

	    $projected_rev = floatval($projected_rev);
        $print[] =  number_format($projected_rev,2,",",".");

	    $actual_rev = floatval($actual_rev);
        $print[] =  number_format($actual_rev,2,",",".");
		
		$profit = floatval($profit);
        $print[] =  number_format($profit,2,",",".");
		
		echo '<tr>
	        <td width="10%">'.$current_month.'</td>
            <td width="10%">'.$print[0].'</td>
            <td width="10%">'.$print[1].'</td>
            <td width="10%">'.$print[2].'</td>
			<td width="10%">'.$print[3].'</td>
            <td width="10%">'.$print[4].'</td>
          </tr>
          ';

		return $print;
	}
}

if(!function_exists('payment_values'))
{
	function payment_values($current_month,$repaymentAmount,$customerid,$ApplicationId)
	{
// ----------------------------------------------------------------------------------- //
// 					BEGIN - IMS Web Service URL & Read the Data From IMS 			   //
// ----------------------------------------------------------------------------------- //				 
// set feed URL
$WebServiceURL = "http://www.vogsphere.co.za/eloan/ws_Payments.php?repayment=".$repaymentAmount."&customerid=".$customerid."&ApplicationId=".$ApplicationId."";

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// pre tags to format nicely
//echo '<pre>';
//print_r($sxml);
//echo '</pre>';
$path  = '';
	$payment_month = 0.00;
// And if you want to fetch multiple Invoice IDs:
foreach($sxml->invoice as $invoice)
{
     if($current_month == substr($invoice->date,0,7))
	 {
			$payment_month = $payment_month + $invoice->amount;
	 }
}
return $payment_month;
}}

?>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>

<div class="container background-white bottom-border">

  <div class='row margin-vert-30'>
                          <!-- Login Box -->
  <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
  <form class='login-page' method='post' onsubmit="return fiterByDate();">
  <div class='login-header margin-bottom-30'>
    <h2 align="center">Loan Projection</h2>
    </div>
      <div class='row'>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
			<input style='height:30px;z-index:0' id="From" name='From' placeholder='Date From' class='form-control' type='Date' value="<?php echo $From;?>">
        </div>
        <div class='input-group margin-bottom-20'>
			<span class='input-group-addon'>
				<i class='fa fa-calendar'></i>
			</span>
        <input style='height:30px;z-index:0' id="To" name='To' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="<?php echo $To;?>">
        </div>
		<div class="control-group">
			<div class='input-group margin-bottom-20'>
			 <span class='input-group-addon'>
				<i class='fa-caret-down'></i>
			 </span>
			<SELECT class="form-control" id="ExecApproval" name="ExecApproval" size="1" style='z-index:0'>
				<!--<OPTION value=''     <?php echo $All;?>>All</OPTION>-->
				<OPTION value='FUND' <?php echo $Fund;?>>Funded</OPTION>
			</SELECT>
			</div>
		</div>

   <div class='row'>
        <div class='col-md-7'>
            <button class='btn btn-primary pull-right' type='submit' onclick="mySearch">Search</button>
        </div>
    </div>
 </div>
      </form>
    </div>
  </div>
</br>
</br>
<div class="table-responsive">
<table id="collections" class="table table-striped table-bordered" style="width:100%">

      <thead>
          <tr>
		      <th>Month</th>
		      <th>Issued Loans</th>
              <th>Target Revenue</th>
              <th>Projected Revenue</th>
              <th>Actual Revenue</th>
              <th>Profitability</th>
          </tr>
      </thead>
    <tfoot>
                  <tr>
                      <th colspan="3" style="text-align:right">Total:</th>
                      <th></th>
                  </tr>
      </tfoot>
      <?php
	    $statusNoOustanding = array("FUND");
	    $account          = '';
		$current_month    = '';
		$issued_loan      = 0.00;
		$targeted_rev     = 0.00;
		$projected_rev    = 0.00;
		$actual_rev       = 0.00;
		$profit           = 0.00;
	    foreach($data as $row)
        {	
			if(empty($current_month))
			{   
				$current_month    = substr($row["DateAccpt"],0,7);
	            $value_array = predicated_loan_values($current_month,$row["Repayment"],$row['CustomerId'],$row['ApplicationId'],$row["monthlypayment"],$issued_loan,$targeted_rev,$projected_rev);
				$issued_loan      = $value_array[0];
				$targeted_rev     = $value_array[1];
				$projected_rev    = $value_array[2];
				$profit           = $value_array[3];
		        $actual_rev       = $value_array[4];
			}
			elseif( $current_month == substr($row["DateAccpt"],0,7))
			{
			    $current_month    = substr($row["DateAccpt"],0,7);
	            $value_array = predicated_loan_values($current_month,$row["Repayment"],$row['CustomerId'],$row['ApplicationId'],$row["monthlypayment"],$issued_loan,$targeted_rev,$projected_rev);
				$issued_loan      = $value_array[0];
				$targeted_rev     = $value_array[1];
				$projected_rev    = $value_array[2];
				$profit           = $value_array[3];
		        $actual_rev       = $value_array[4];
			}
			else
			{
				// print out current month
		$print_array = null;
		$print_array = print_values($current_month,$issued_loan,$targeted_rev,$projected_rev,$actual_rev,$profit);

       // -- Clear fields
		$issued_loan      = 0.00;
		$targeted_rev     = 0.00;
		$projected_rev    = 0.00;
		$actual_rev       = 0.00;
		$profit           = 0.00;
	  // -- add new month values
	    $value_array = predicated_loan_values($current_month,$row["Repayment"],$row['CustomerId'],$row['ApplicationId'],$row["monthlypayment"],$issued_loan,$targeted_rev,$projected_rev);
	    $current_month    = substr($row["DateAccpt"],0,7);
		$issued_loan      = $value_array[0];
		$targeted_rev     = $value_array[1];
		$projected_rev    = $value_array[2];
		$profit           = $value_array[3];
		$actual_rev       = $value_array[4];

		  	}
        }
		// -- Last month print_out..
		$print_array = null;
		$print_array = print_values($current_month,$issued_loan,$targeted_rev,$projected_rev,$actual_rev,$profit);
      ?> 
  </table>
    </div>
    </br>
</div>

<script>

$.fn.dataTable.ext.search.push(

function (settings, data, dataIndex){
    var min = Date.parse( $('#min').val(), 10 );
    var max = Date.parse( $('#max').val(), 10 );
    var Action_Date=Date.parse(data[8]) || 0;

    if ( ( isNaN( min ) && isNaN( max ) ) ||
         ( isNaN( min ) && Action_Date <= max ) ||
         ( min <= Action_Date   && isNaN( max ) ) ||
         ( min <= Action_Date   && Action_Date <= max ) )
    {
      //  alert("True");
      //location.reload();
      $(document).ready();
        return true;

    }
    //alert("false");
    return false;

}
);

$(document).ready(function(){
var table =$('#collections').DataTable({
//"dom": "t"
lengthChange: true,
autoFill:true,
order: [[ 0, "desc" ]],
buttons: ['copy', 'excel','pdf','csv', 'print'],

"footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        total = api
            .column( 3)
            .data()
            .reduce( function (a, b) {
                //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotal = api
            .column( 3, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

            totalL = api
                .column( 5)
                .data()
                .reduce( function (a, b) {
                    //return (number_format((intVal(a) + intVal(b)),2'.', ''));
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotalL = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
      $( api.column( 3 ).footer() ).html(
      'Page Total  R'+pageTotal+'        '+'    Grand Total R'+ total
        );

      $( api.column( 5 ).footer() ).html(
        'Page Total  R'+pageTotalL+'        '+'    Grand Total R'+ totalL
          );

    }


});

table.buttons().container().appendTo('#collections_wrapper .col-sm-6:eq(0)')
//Event listner
$('#min,#max').change( function() {
  table.draw();
  });

$('#customerid').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );


$('#status').on('change', function(){
   table.search( this.value ).draw();
});

});

// #myInput is a <input type="text"> element

</script>
