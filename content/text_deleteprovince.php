<?php 
	require 'database.php';
	
	$provinceid = null;
	
	if ( !empty($_GET['provinceid'])) 
	{
		$provinceid = $_REQUEST['provinceid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$provinceid = $_POST['provinceid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM province WHERE provinceid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($provinceid));
			Database::disconnect();
			echo("<script>location.href='province.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="deleteprovince.php" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a Province
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="provinceid" value="<?php echo $provinceid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div>
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="province.php">No</a>
						</div>
					</form>
				</div>
	</div>			
</div> <!-- /container -->
