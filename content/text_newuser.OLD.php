<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//1. ---------------------  Role ------------------- //
  $sql = 'SELECT * FROM role ORDER BY role';
  $dataRoles = $pdo->query($sql);	
  
$tbl_user="user"; // Table name 
$passwordError = null;
$password = '';

$emailError = null;
$email = ''; 

$role = '';

$count = 0;
$userid = 0;

$Original_email = '';

if (!empty($_POST)) 
{   $count = 0;
	$password = $_POST['password'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];
	$role = $_POST['role'];
	
	$valid = true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);
		
		if (empty($userid)) { $useridError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($userid) != 13 ) { $useridError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($userid)) { $useridError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($userid,2,2) > 12) 
		{ 
			$useridError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{
		// Day from 1 - 30
			if (in_array(substr($userid,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($userid,4,2) > 30)
			   {$useridError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($userid,2,2),$months31)) 

			{
				// Day 
			   if (substr($userid,4,2) > 31)
			   {$useridError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($userid,2,2),$monthsFeb)) 
			{
				// Leap Year
				$yearValidate = parseInt(substr(0,2));
				$leapyear = ($yearValidate % 4 == 0);
				
				if($leapyear == true)
				{
					// Day 
			   if (substr($userid,4,2) > 29)
			   {$useridError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($userid,4,2) > 28)
					   {$useridError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//								USER ID VALIDATION														  //								
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if (empty($password)) { $passwordError = 'Please enter password Description.'; $valid = false;}
    if (empty($userid)) { $useridError = 'Please enter User ID.'; $valid = false;}	
	else
	{
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($userid));
			if ($q->rowCount() >= 1)
			{$useridError = 'User ID already in use.'; $valid = false;}
	}

		// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}	
	
	if (empty($email)) 
	{ $emailError = 'Please enter e-mail'; $valid = false;}
	else
	{
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}		
	 }
	 
	 
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "INSERT INTO  user (userid,password,email,role) VALUES (?,?,?,?)"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$password = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($userid,$password,$email,$role));// -- Changed 16.09.2017 - MD5 Encryption.
			Database::disconnect();
			$count = $count + 1;
		}
}

?>
<div class="container background-white bottom-border"> 
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">   
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         New User Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>User successfully added.</p>");
		}
?>
				
</div>				
			<p><b>User ID</b><br />
			<input style="height:30px" type='text' name='userid' value="<?php echo !empty($userid)?$userid:'';?>" />
			<?php if (!empty($useridError)): ?>
			<span class="help-inline"><?php echo $useridError;?></span>
			<?php endif; ?>
			</p>
			
			<p><b>Password</b><br />
			<input style="height:30px" type='password' name='password' value="<?php echo !empty($password)?$password:'';?>"/>
			<?php if (!empty($passwordError)): ?>
			<span class="help-inline"><?php echo $passwordError;?></span>
			<?php endif; ?>
			</p> 
			
			<p><b>E-mail</b><br />
			<input style="height:30px" type='text' name='email' value="<?php echo !empty($email)?$email:'';?>"/>
			<?php if (!empty($emailError)): ?>
			<span class="help-inline"><?php echo $emailError;?></span>
			<?php endif; ?>
			</p> 
			
			<p><b>Role</b><br />
				<div class="controls">
					<SELECT class="form-control" id="role" name="role" size="1">
					<?php
						$roleLoc = '';
						$roleSelect = $role;
						foreach($dataRoles as $row)
						{
							$roleLoc = $row['role'];

							if($roleLoc == $roleSelect)
							{
								echo "<OPTION value=$roleLoc selected>$roleLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$roleLoc>$roleLoc</OPTION>";
							}
						}
												
						if(empty($dataRoles))
						{
							echo "<OPTION value=0>No Roles</OPTION>";
						}
					?>
					</SELECT>
				</div>
			</p>	
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'users';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
		     </div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>