<?php
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

// -- Database Clients:
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM client';
$dataclient = $pdo->query($sql);

$datasrUserCodeArr = array();
foreach($dataclient as $row)
{
	$datasrUserCodeArr[] = $row['username']."|".$row['Description'];
}
// -- Database Client

// -- Debicheck UserCode -- //
$sql = 'SELECT * FROM client WHERE debicheckusercode <> username';
$dataclientDebicheck = $pdo->query($sql);
foreach($dataclientDebicheck as $row)
{
	$datasrUserCodeArr[] = $row['debicheckusercode']."|".$row['Description'];
}
// -- Debicheck UserCode -- //
$Allstatus = '';
$results 	 = null;
$userid 	 = '';
$UserCode 	 = '';
$IntUserCode = '';
$role 		 = '';
$srUserCode  = 'UFAL'; //--search by usercode
// -- Sessions -- //
// -- Logged User Details.
if(getsession('username')){$userid = getsession('username');}
// -- User Code.
if(getsession('UserCode')){$UserCode = getsession('UserCode');}
// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');}

// -- Role.
if(getsession('role')){$role = getsession('role');}

// -- Params -- //
$param = '';
if(isset($_GET['param']))
{$param = $_GET['param'];
$params = explode("|", $param);
if(empty($IntUserCode)){$IntUserCode = $params[0];}
if(empty($UserCode)){$UserCode = $params[1];}
if(empty($userid)){$userid = $params[2];}
if(empty($role)){$role = $params[3];}
}
$bid 		= '';
$reconArray = null;
$display = 'display:none';
$reconStats = '';
if($role == 'admin')
{
	$display = '';
}
// -- echo $IntUserCode.' - '.$UserCode.' - '.$userid.' - '.$role;

include $filerootpath.'/script_collectionrecon.php';
/*$month = '';
$month = date('Y-m');
$month = substr($month,0,7);
$tomonth = '';
$tomonth = date('9999-12');*/
$dataF = null;
$TotalPaid = null;

if(!empty($_POST['srUserCode']))
{
	$srUserCode = $_POST['srUserCode'];
}
if(empty($display))
{
	$IntUserCode = $srUserCode;
}

//foreach($datasrUserCodeArr as $row)
{
  $companyname = '';
  $lastMonth = '';
  $dataF 	   = null;
  $TotalPaid   = null;
  $TotalUNPaid = null;
  $TotalIncome = null;
  $TotalCharges = null;
  $AvailableBalance = null;
  $TotalRetainer = null;
  $balance = null;
  $GlobalBalance = 0;
  $bal = 0;
  $paid = 0;
  $unpaid = 0;
  $income = 0;
	$unsettled=0;
  $charges = 0;
  $retainer = 0;
  //$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
  $dataF['IntUserCode'] = $IntUserCode;//$rowData[0];

// -- Get Company Name.
foreach($datasrUserCodeArr as $row)
{
	$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
	  if($rowData[0] == $IntUserCode)
	  {
		  $companyname = $rowData[1];
	  }
  }

  $TotalPaid = get_all_paid($dataF);
  if(!empty($TotalPaid[0]['IntUserCode']))
  { $paid  = $TotalPaid[0]['Total'];
    //print("<br/>");print_r($TotalPaid);
  }

  $TotalUNPaid = get_all_unpaid($dataF);
  if(!empty($TotalUNPaid[0]['IntUserCode']))
  { $unpaid  = $TotalUNPaid[0]['Total'];
    //print("<br/>");print_r($TotalUNPaid);
	}

  $TotalIncome = get_all_income($dataF);
  if(!empty($TotalIncome[0]['IntUserCode']))
  { $income  = $TotalIncome[0]['Total'];
    //print("<br/>");print_r($TotalIncome);
  }

	$TotalIncome = get_all_unsettled($dataF);
  if(!empty($TotalIncome[0]['IntUserCode']))
  { $unsettled  = $TotalIncome[0]['Total'];
    //print("<br/>");print_r($TotalIncome);
  }

	$TotalCharges = get_all_charges($dataF);
  if(!empty($TotalCharges[0]))
  {
	//echo count($TotalCharges[0]);
	for($index=0;$index<count($TotalCharges[0]);$index++)
	{
		$chargescorrected = 0;
		$chargescorrected = str_replace( ',', '', $TotalCharges[0][$index]['charges']);
		//print($chargescorrected);
		//print("<br/>");
		$charges  = $charges + $chargescorrected;
		$charges = str_replace( ',', '', $charges );
	}
	//print("<br/>");print_r($TotalCharges);
  }
  // -- Retainer
  $dataF['month'] = '1970-01';
  $dataF['tomonth'] = '9999-12';
   $dataF['role'] = 'customer';
   $TotalRetainer = get_client_retainers($dataF);
  if(!empty($TotalRetainer[0]))
  { $retainer  = $TotalRetainer[0]['holdvalue'];
    //print("<br/>");print_r($TotalRetainer);
	$retainer = str_replace( ',', '', $retainer );
  }

  // -- Total Available Balance
  $bal = ($income - $unpaid) - ($paid) - ($charges) - $retainer ;
  $retainer = number_format($retainer ,2);
  $balance['IntUserCode'] = $dataF['IntUserCode'];
  $bal = number_format($bal ,2);
  $balance['Balance'] = $bal;
  $AvailableBalance[] = $balance;
  //print("<br/>");
  //print_r($AvailableBalance);
  $GlobalBalance = $bal;
}
/*
else
{

foreach($datasrUserCodeArr as $row)
{
$reconStats = $reconStats.'<ul class="nav navbar-nav">';
  $dataF 	   = null;
  $TotalPaid   = null;
  $TotalUNPaid = null;
  $TotalIncome = null;
  $TotalCharges = null;
  $AvailableBalance = null;
  $TotalRetainer = null;
  $balance = null;

  $bal = 0;
  $paid = 0;
  $unpaid = 0;
  $income = 0;
  $charges = 0;
  $retainer = 0;
  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
  $dataF['IntUserCode'] = $rowData[0];
$reconStats = $reconStats.'<li><h1>UserCode : <span class="label label-default"><strong>'.$dataF['IntUserCode'].'</strong></span></h1></li>';

  $TotalIncome = get_all_income($dataF);
  if(!empty($TotalIncome[0]['IntUserCode']))
  { $income  = $TotalIncome[0]['Total'];
    //print("<br/>");print_r($TotalIncome);
$reconStats = $reconStats.'<li><h1>Income : <span class="label label-default"><strong>'.$income.'</strong></span></h1></li>';
  }

  $TotalPaid = get_all_paid($dataF);
  if(!empty($TotalPaid[0]['IntUserCode']))
  { $paid  = $TotalPaid[0]['Total'];
    //print("<br/>");print_r($TotalPaid);
$reconStats = $reconStats.'<li><h1>Paid : <span class="label label-default"><strong>'.$paid.'</strong></span></h1></li>';

  }

  $TotalUNPaid = get_all_unpaid($dataF);
  if(!empty($TotalUNPaid[0]['IntUserCode']))
  { $unpaid  = $TotalUNPaid[0]['Total'];
    //print("<br/>");print_r($TotalUNPaid);
$reconStats = $reconStats.'<li><h1>Unpaid : <span class="label label-default"><strong>'.$unpaid.'</strong></span></h1></li>';

	}

   $TotalCharges = get_all_charges($dataF);
  if(!empty($TotalCharges[0]))
  { $charges  = $TotalCharges[0][0]['charges'];
  //print("<br/>");print_r($TotalCharges);
	$charges = str_replace( ',', '', $charges );
$reconStats = $reconStats.'<li><h1>Charges : <span class="label label-default"><strong>'.$charges.'</strong></span></h1></li>';

  }

  // -- Retainer
  $dataF['month'] = '1970-01';
  $dataF['tomonth'] = '9999-12';
   $dataF['role'] = 'customer';
   $TotalRetainer = get_client_retainers($dataF);
  if(!empty($TotalRetainer[0]))
  { $retainer  = $TotalRetainer[0]['holdvalue'];
    //print("<br/>");print_r($TotalRetainer);
	$retainer = str_replace( ',', '', $retainer );
$reconStats = $reconStats.'<li><h1>Retainer : <span class="label label-default"><strong>'.$retainer.'</strong></span></h1></li>';
  }

  // -- Total Available Balance
  $bal = ($income - $unpaid) - ($paid) - ($charges) - $retainer ;
  $retainer = number_format($retainer ,2);
  $balance['IntUserCode'] = $dataF['IntUserCode'];
  $bal = number_format($bal ,2);
  $balance['Balance'] = $bal;
  $AvailableBalance[] = $balance;
$reconStats = $reconStats.'<li><h1>Available Balance : <span class="label label-success"><strong>'.$bal.'</strong></span></h1></li>';

  //print("<br/>");
  //print_r($AvailableBalance);
$reconStats = $reconStats.'</ul>';
$reconStats = $reconStats.'<br/>';
}
}*/
?>
<!-- https://datatables.net/examples/advanced_init/row_grouping.html -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum%28%29.js"></script>
<style>
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>
<div class="container background-white bottom-border">
<!-- EOC -->
<h2 align="center">Company name : <?php echo $companyname; ?><name></h2>
</br>
<h3 align="center">Reconcilaition Report</h3>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
	<?php if(empty($display)){?>
	<li><h1>Income : <span class="label label-default"><strong><?php echo $income;?></strong></span></h1></li>
	<li><h1>Unpaid  : <span class="label label-primary"><strong>R<?php echo $unpaid;?></strong></span></h1></li>
	<li><h1>Paid  : <span class="label label-default"><strong>R<?php echo $paid;?></strong></span></h1></li>
	<li><h1>Charges  : <span class="label label-default"><strong>R<?php echo $charges;?></strong></span></h1></li>
	<li><h1>Security  : <span class="label label-default"><strong>R<?php echo $retainer;?></strong></span></h1></li>
	<?php }?>
	</br>
	</br>
	<li><h1>Available Balance  : <span class="label label-success"><strong>R<?php echo $balance['Balance'];?></strong></span></h1></li>
	<li><h1>Unsettled Income : <span class="label label-success"><strong>R<?php echo $unsettled;?></strong></span></h1></li>
    </ul>
  </div>
  	
</nav>
 <div class='row margin-vert-30'>
</br>
</br>

  <form method='post'>
<!-- Filter Buttons -->
</br>
</br>
</br>
<div class="portfolio-filter-container margin-top-20">
    <ul class="portfolio-filter">
        <li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            filter by:
        </li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            From:
        </li>
        <li>
		 <!-- <input style='height:30px;z-index:0' id="min" name='min' placeholder='Date From' class='form-control' type='Date' value="" /> -->
         <input  style='height:30px;z-index:0' id='month' name='month' placeholder='yyyy-mm-dd' class='form-control' type='Month' value="<?php echo $month;?>" />

		</li>
		<li style='height:30px;z-index:0' class="portfolio-filter-label label label-primary">
            To:
        </li>
        <li>
         <!-- <input style='height:30px;z-index:0' id="max" name='max' placeholder='Date To' class='form-control margin-bottom-20' type='Date' value="" /> -->
	     <input style='height:30px;z-index:0;<?php echo $display; ?>' id='tomonth' name='tomonth' placeholder='yyyy-mm-dd' class='form-control' type='Month' value="<?php echo $tomonth;?>" />
		</li>
		<li style='height:30px;z-index:0;<?php echo $display; ?>' class="portfolio-filter-label label label-primary">
            UserCode:
        </li>
		<li style='height:30px;z-index:0;<?php echo $display; ?>'>
		   <SELECT style="width:150px;height:30px;z-index:0;" class="form-control" id="srUserCode" name="srUserCode" size="1" style='z-index:0'>
				<!-- <OPTION value='' <?php echo $Allstatus;?>>All UserCodes</OPTION> -->
			    <?php
					$datasrUserCodeSelect = $srUserCode;
					foreach($datasrUserCodeArr as $row)
					{
					  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
					  if($rowData[0] == $datasrUserCodeSelect)
					  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'-'.$rowData[1].'</option>';}
					  else
					  {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'-'.$rowData[1].'</option>';}
					}
				?>
			</SELECT>
        </li>
		<?php if(empty($display)){?>
		<li style='height:30px;z-index:0;'>
		<?php if(empty($param)){?>
		   <button type="submit">Search</button>
		<?php }else{ ?>
			<a href="<?php if(empty($param)){}else{ echo "javascript:clickTab('recon');";}?>" class="btn btn-primary nextBtn btn-lg pull-right" id="search" type="button" >Search</a>
		<?php } ?>
		</li>
		<?php } ?>
     </ul>
</div></form>
  </div>
<div class="table-responsive">
<table id="example" class="display" style="font-size:10pt" >
        <thead>
            <tr>
                <th>UserCode</th>
                <th>Month</th>
                <th>Action Date</th>
                <th>Income</th>
                <th>Unpaids</th>
                <!-- <th>To Pay</th> Need split -->
                <th>Paid</th>
				<th>Charges</th><!-- Percentage -->
                <th>Security</th><!-- Retainer -->
                <th>DR/CR</th><!-- Retainer -->
				<th>Security(%)</th><!-- Percentage -->
                <th>Status</th>
				<th>Available Balance</th><!-- change#1 20.09.2021 Hide it and we shall uncomment and find solution for Available Balance-->
            </tr>
        </thead>
        <tbody>
		<?php if(!empty($results))
		{
			// -- Build Reconcilaition Array
			$reconArraytmp = null;
			$word = "Successful";
			$wordExclude = "pending,Submitted,Stopped Payment,Deactivated,Transaction Successful";
			$totalBalance = 0;
			$reconArraytmp['totalBalances'] = $totalBalance;
			$totalBalanceTxt = '';

array_multisort(
array_map(function($element) {
      return $element['IntUserCode'];
  }, $results), SORT_ASC,
array_map(function($element) {
      return $element['Month'];
  }, $results), SORT_ASC,array_map(function($element) {
      return $element['Status_Description'];
  }, $results), SORT_DESC, $results);
  //print_r($results);
  //print("<br/>");
  //print("<br/>");
// -- count Total Number of Rows - 1 = Last Index..
	$allRows = count($results) - 1;
	$securityPerMonth      = 0.00;
	$TotalPayOutPerMonth   = 0.00;
	$TotalChargesPerMonth  = 0.00;
	$TotalUnpaidPerMonth   = 0.00;
    $TotalIncomePerMonth   = 0.00;
    $previous_month_closing_balance = 0.00;
	$closingbalancePerMonth = 0.00;
	$Balance_bought_forward  = null;
	
// -- Formular 
//$Total = TotalPayOut(-81369.44) + TotalCharges(-986.54) + TotalUnpaid(-0) + TotalIncome(121840) + previous_month_closing_balance(0) = R39484.02

//array_multisort('Status_Description', SORT_ASC, 'Month', SORT_ASC, $data);
			$holdvalueDone  = '';
			$lastMonth 		= '';
			foreach($results as $key => $row)
			{
				/// - Closing Balance Previous last record -- ///
				// -- Apply the mathematics formular end of processing records.					
				if(empty($lastMonth))
				{
					$reconArraytmp['closingbalance'] = '';
				}
				// a).IF its the last record per each month.Mark as closing balance.
				//Mark as closing balance on the previous record]
				elseif($lastMonth != $row['Month'])
				{
					// check if LastMonth is inarray() to get the previous balance..
					if(!empty($Balance_bought_forward))
					{

						//echo 'Test IN';
						foreach($Balance_bought_forward as $keyBalance => $rowBalance)
						{
							if($lastMonth == $rowBalance['NextMonth'])
							{
								$previous_month_closing_balance = $rowBalance['currentBalance'];
								break;
							}
						}
				   }
					// -- Apply Mathematical Formular
				    $closingbalancePerMonth = ($TotalPayOutPerMonth * -1) + ($TotalChargesPerMonth * -1) + ($TotalUnpaidPerMonth * -1) + $TotalIncomePerMonth + $previous_month_closing_balance + ($securityPerMonth * -1);
					$closingbalancePerMonth = (float)number_format($closingbalancePerMonth,2,'.', '');
					$reconArray[$key - 1]['closingbalance'] = $closingbalancePerMonth;
					$reconArray[$key - 1]['BalanceBought_Forward'] = $previous_month_closing_balance;
					//$reconArraytmp['closingbalance'] = $TotalPayOutPerMonth;//$lastMonth.' = '.$row['Month'];//'';
					// -- Keep for Later Reference for Balance_bought_forward
					$arr['currentMonth']     = $lastMonth;
					$arr['security'] = $securityPerMonth;
					$arr['paid']           = $TotalPayOutPerMonth;					
					$arr['charges']        = $TotalChargesPerMonth;
					$arr['Unpaid']         = $TotalUnpaidPerMonth;
					$arr['Income']         = $TotalIncomePerMonth;					

					$arr['NextMonth']      = $row['Month'];
					$arr['currentBalance'] = $closingbalancePerMonth;
					$arr['BalanceBought_Forward'] = $previous_month_closing_balance;
					$Balance_bought_forward[] = $arr;
					
					// -- Initialise the variables
					$securityPerMonth				= 0.00;
					$TotalPayOutPerMonth   			= 0.00;
					$TotalChargesPerMonth 			= 0.00;
					$TotalUnpaidPerMonth   			= 0.00;
					$TotalIncomePerMonth   			= 0.00;
					$previous_month_closing_balance = 0.00;
	
				}
				// b).IF its the last record we mark the closing balance.
				//Mark as closing balance on the current record]				
				elseif($allRows == $key)
				{
					 if(!empty($Balance_bought_forward))
					 {
						foreach($Balance_bought_forward as $keyBalance => $rowBalance)
						{
							if($lastMonth == $rowBalance['NextMonth'])
							{
								$previous_month_closing_balance = $rowBalance['currentBalance'];
								break;
							}
						}}
				    $closingbalancePerMonth = ($TotalPayOutPerMonth * -1) + ($TotalChargesPerMonth * -1) + ($TotalUnpaidPerMonth * -1) + $TotalIncomePerMonth + $previous_month_closing_balance;
					$closingbalancePerMonth = (float)number_format($closingbalancePerMonth,2,".", '');
					$reconArraytmp['closingbalance'] = $closingbalancePerMonth;
					//$reconArray[$key]['closingbalance'] = $closingbalancePerMonth;
					//$reconArray[$key]['BalanceBought_Forward'] = $previous_month_closing_balance;
					// -- Keep for Later Reference for Balance_bought_forward
					$arr['currentMonth']   = $lastMonth;
					$arr['security'] = $securityPerMonth;					
					$arr['paid']           = $TotalPayOutPerMonth;					
					$arr['charges']        = $TotalChargesPerMonth;
					$arr['Unpaid']         = $TotalUnpaidPerMonth;
					$arr['Income']         = $TotalIncomePerMonth;					
					$arr['NextMonth']      = $row['Month'];
					$arr['currentBalance'] = $closingbalancePerMonth;
					$arr['BalanceBought_Forward'] = $previous_month_closing_balance;
					$Balance_bought_forward[] = $arr;					
				}
				// c).IF its none. Do not mark as closing balance.. 
				else
				{
				 $reconArraytmp['closingbalance'] = '';//$lastMonth.' = '.$row['Month'];//'';
				 $reconArraytmp['BalanceBought_Forward'] = '';
				}
				///////////////////////////////////////////////////////////////////////////////////
				$paid 	= '0.00';
				$unpaid = '0.00';
				$unpaidTxt = '';
				$chargesTxt = '';
				$onholdvalueTxt = '';
				$income = '0.00';
				$balance = '0.00';
				$reconArraytmp['Month'] = $row['Month'];
				$reconArraytmp['DateOnly'] = $row['DateOnly'];
				$reconArraytmp['TotalAmount'] = $row['TotalAmount'];
				$reconArraytmp['TotalPayOut'] = $row['TotalPayOut'];
				$TotalPayOutTxt = '';
				if($row['TotalPayOut'] != '0.00')
				{
					$TotalPayOutTxt = $reconArraytmp['TotalPayOut'];
				}
				$reconArraytmp['holdvalue'] = $row['holdvalue'];
				if($row['holdvalue'] != '0.00')
				{
					$onholdvalueTxt = $reconArraytmp['holdvalue'];
				}
				$reconArraytmp['percentage'] = $row['percentage'];
				$reconArraytmp['charges'] = $row['charges'];
				if($row['charges'] != '0.00')
				{
					$chargesTxt = $reconArraytmp['charges'];
				}
				$charges = $reconArraytmp['charges'];
				$payout = $reconArraytmp['TotalPayOut'];
							$totalBalanceTxt = '';
				$holdvalue = $reconArraytmp['holdvalue'];
// -- https://www.tutorialrepublic.com/faq/how-to-check-if-a-string-contains-a-specific-word-in-php.php
				// -- Minus it once..
				if($holdvalue !== '0.00' && empty($holdvalueDone))
				{
					$holdvalue = str_replace(",",'',$holdvalue);
					//echo $charges;
					$balance = $holdvalue * -1;
					$totalBalance = ($balance + $totalBalance);
				    $reconArraytmp['totalBalances'] = $totalBalance;
					$totalBalanceTxt = $reconArraytmp['totalBalances'];
					$holdvalueDone = 'X';
				}
				elseif($charges !== '0.00')
				{
					$charges = str_replace(",",'',$charges);
					//echo $charges;
					$balance = $charges * -1;
					$totalBalance = ($balance + $totalBalance);
				    $reconArraytmp['totalBalances'] = $totalBalance;
					$totalBalanceTxt = $reconArraytmp['totalBalances'];
				}

				elseif($payout !== '0.00')
				{
					$payout = str_replace(",",'',$payout);
					$balance = $payout * -1;
					$totalBalance = ($balance + $totalBalance);
				    $reconArraytmp['totalBalances'] = $totalBalance;
					$totalBalanceTxt = $reconArraytmp['totalBalances'];

				}
					elseif(strpos($row['Status_Description'], $word) !== false)
					{
						$paid 	 =  $row['TotalAmount'];
						$balance = $paid;
						$totalBalance = ($balance + $totalBalance);
						$reconArraytmp['totalBalances'] = $totalBalance;
						$totalBalanceTxt = $reconArraytmp['totalBalances'];
					}
					else
					{
						$wordExclude = "pending,Submitted,Stopped Payment,Deactivated,Transaction Successful";
						if(empty($row['Status_Description']))
						{
							//echo "Not found";
							$unpaid  =  $row['TotalAmount'];
							$balance = '0.00';// * -1;
							$totalBalanceTxt = '';
							$reconArraytmp['totalBalances'] = 0;
						}
						elseif(strpos($wordExclude,$row['Status_Description']) > 0)
						{
						   //echo "Found";
						   
						}
						else
						{
							//echo "Not found";
							$unpaid  =  $row['TotalAmount'];
							$balance = '0.00';// * -1;
							$totalBalanceTxt = '';
							$reconArraytmp['totalBalances'] = 0;						  
						}	
						
					}

				$reconArraytmp['unpaid']   = $unpaid;// -- unpaid
				$reconArraytmp['paid']     = $paid ;//-- Income
				$IncomeTxt = '';
				$reconArraytmp['income']    = $income; //07.09.2021
				if($row['TotalAmount'] != '0.00')
				{
					$IncomeTxt = $row['TotalAmount'];
					$reconArraytmp['income'] = $IncomeTxt;
				}
				$reconArraytmp['balance']  = number_format($balance, 2);
				// -- Reconcilaition of $reconArraytmp into $reconArray List
				$reconArray[] = $reconArraytmp;
				$bal = $reconArraytmp['totalBalances'];//number_format($reconArraytmp['totalBalances'],2);
				// ---------Closing Balance 21 September 2021 ------------- //
				$securityPerMonth      = $securityPerMonth  + $row['holdvalue'];
				$TotalPayOutPerMonth   = $TotalPayOutPerMonth  + $row['TotalPayOut'];
				$TotalChargesPerMonth  = $TotalChargesPerMonth + $charges;
				$TotalUnpaidPerMonth   = $TotalUnpaidPerMonth  + $reconArraytmp['unpaid'];
				$TotalIncomePerMonth   = $TotalIncomePerMonth  + $reconArraytmp['income'];
				$previous_month_closing_balance = 0.00;
			   // -------------------------------------------------------- //
				// -- Table Content -- //
				echo "<tr>";
				echo "<td>{$row['IntUserCode']}</td>";
				echo "<td>{$row['Month']}</td>";
				echo "<td>{$row['DateOnly']}</td>";
				echo "<td>{$IncomeTxt}</td>";
				echo "<td>{$reconArraytmp['unpaid']}</td>";
				//echo "<td>{$reconArraytmp['paid']}</td>";
				echo "<td>{$row['TotalPayOut']}</td>";
				echo "<td>{$row['charges']}</td>";
				echo "<td>{$row['holdvalue']}</td>";
				echo "<td>{$reconArraytmp['balance']}</td>";//--{$reconArraytmp['balance']}
				echo "<td></td>";
				echo "<td>{$row['Status_Description']}</td>";
				echo "<td></td>";//{$bal} change#2 20.09.2021 Hide it and we shall uncomment and find solution for Available Balance...
				echo "</tr>";
				$lastMonth = $reconArraytmp['Month'];
				//print_r($reconArraytmp);
				//print("<br/>");
				//print("<br/>");
			}
			//print_r($Balance_bought_forward);
			//print("<br/>");
		    //print("<br/>");
			//print_r($reconArray);
			
			
		}
		?>
		</tbody>
		<tfoot>
            <tr>
				<th>UserCode</th>
                <th>Month</th>
                <th>Action Date</th>
                <th>Income</th>
                <th>Unpaids</th>
                <!-- <th>To Pay</th> Need split -->
                <th>Paid</th>
				<th>Charges</th><!-- Percentage -->
                <th>Security</th><!-- Retainer -->
                <th>DR/CR</th><!-- Retainer -->
				<th>Security(%)</th><!-- Percentage -->
                <th>Status</th>
				<th>Available Balance</th> <!-- change#3 20.09.2021 Hide it and we shall uncomment and find solution for Available Balance -->
			</tr>
        </tfoot>
</table>
<!--<div class="table-responsive">
<!--<table id="summarybal" class="display" style="font-size:10pt" >
<thead><tr><th>Current Month</th><th>security</th><th>Payout</th><th>Charges</th><th>Unpaid</th><th>Income</th><th>BalanceBought_Forward</th><th>Current Balance</th><th>Next Month</th></tr></thead>
<tbody>-->
<?php
/*foreach($Balance_bought_forward as $key => $rowBal)
{
	echo "<tr>";
	echo "<td>{$rowBal['currentMonth']}</td><td>{$rowBal['security']}</td><td>{$rowBal['paid']}</td><td>{$rowBal['charges']}</td><td>{$rowBal['Unpaid']}</td><td>{$rowBal['Income']}</td><td>{$rowBal['BalanceBought_Forward']}</td><td>{$rowBal['currentBalance']}</td><td>{$rowBal['NextMonth']}</td>";
	echo "</tr>";
}*/
?>
<!-- </tbody>
</table></div>-->
</div>
<input type="text" style="display:none" id='available' name='available' value="<?php echo $GlobalBalance;?>"></input>
<input type="text" style="display:none" id='lastMonth' name='lastMonth' value="<?php echo $lastMonth;?>"></input>
</div>
<script>

$(document).ready(function()
{
	//alert('test1');

	// -- Call Script to get the Data.
	var results   = null;
	//results = call_script_collectionrecon();
	var today = new Date();
	var month = '';
	var mm = today.getMonth();
	var yyyy = today.getFullYear();
	var month = yyyy+"-"+mm;
	month = '1970-01';//document.getElementById('month').value;
    var groupColumn = 1;
    var table = $('#example').DataTable({
		 //"oSearch": {"sSearch": month},
        "columnDefs": [
            { "visible": false, "targets": groupColumn}
        ],

		//"bFilter": false, //-- disable search button
		"lengthMenu": [ [10, 26, 50, 100,-1], [10, 26, 50,100, "All"] ],
		"buttons" :
		[{
			extend: 'excelHtml5', footer: true,header:true,
			title : 'Debit Orders Reconcilaition History',
			exportOptions : {
			modifier :
			{
                    // DataTables core
                    order  : 'index', // 'current', 'applied',
                    //'index', 'original'
                    page   : 'current', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
            }
			}
		}
		,'pdf','csv', 'print'],
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 26,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group )
				{
					// -- Last Grouping Item Reconcilaition of Totals
					// -- Read from ReconArray.
					<!-- https://jonsuh.com/blog/convert-loop-through-json-php-javascript-arrays-objects/#:~:text=Convert%20JSON%20String%20to%20PHP,the%20objects%20as%20associative%20arrays. -->
					var JSONObject = <?php echo json_encode($reconArray); ?>;
					var totalPaid   = parseFloat('0.00');
					var totalUnPaid = parseFloat('0.00');
					var totalIncome = parseFloat('0.00');
					
					var total = parseFloat('0.00');
					var balance = parseFloat('0.00');
					var totalPayOut = parseFloat('0.00');
					var holdvalue   = parseFloat('0.00');
					var percentage  = '';
					var charges     = parseFloat('0.00');
					var totalBalance = parseFloat('0.00');
					var closingBalance = parseFloat('0.00');
					
					var doItOnce = 0;
					var array1 = [10000000000];// DO NOT REMOVE ITS HELPS TO PUSH INTO Array
					for(var key in JSONObject)
					{
						 if (JSONObject.hasOwnProperty(key))
						 {
						  if(JSONObject[key]["Month"] === group )
						  {
							  totalPaid = totalPaid + parseFloat(JSONObject[key]["paid"]);
							  totalUnPaid = totalUnPaid + parseFloat(JSONObject[key]["unpaid"]);
							  holdvalue = holdvalue + parseFloat(JSONObject[key]["holdvalue"]);
							  totalIncome = totalIncome + parseFloat(JSONObject[key]["income"]);//07.09.2021
							  string = JSONObject[key]["charges"];
							  string = string.replace(/,(?=\d{3})/g, '');
							  charges = charges + parseFloat(string);
							  if(JSONObject[key]["percentage"] !== '')
							  {
								  percentage = JSONObject[key]["percentage"];
							  }
							  totalPayOut = totalPayOut + parseFloat(JSONObject[key]["TotalPayOut"]);

							  if(JSONObject[key]["totalBalances"] !== '')
							  {
								  if(JSONObject[key]["totalBalances"] != '0')
								  {
									  string = JSONObject[key]["totalBalances"];
									  totalBalances = parseFloat(string);
									  array1.push(totalBalances);
								 //totalBalance = totalBalance + parseFloat(JSONObject[key]["totalBalances"]);
								  doItOnce = doItOnce + 1;}
							  }
							  // -- closingBalance
							  //alert(JSONObject[key]["closingbalance"]);
							  if(JSONObject[key]["closingbalance"] !== '')
							  {
								 closingBalance = JSONObject[key]["closingbalance"];
								 closingBalance = parseFloat(closingBalance);
								// alert(JSONObject[key]["closingbalance"]);
							  }
						  }
						 }
					}
					//total = totalPaid + totalUnPaid; 07.09.2021
					total = parseFloat(totalIncome).toFixed(2); //07.09.2021
					total =  parseFloat(total).toFixed(2);
					totalPaid = parseFloat(totalPaid).toFixed(2);
					totalUnPaid = parseFloat(totalUnPaid).toFixed(2);
					
					totalPayOut = parseFloat(totalPayOut).toFixed(2);
					charges = parseFloat(charges).toFixed(2);
 					holdvalue = parseFloat(holdvalue).toFixed(2);

					balance =  totalPaid  - totalPayOut;
					balance =  balance - charges;
					balance = parseFloat(balance).toFixed(2);
					//alert(closingBalance);

					//removeElementsWithValue(array, 0);

array1.sort(sortFloat);
					//array.reverse();
					//array1.shift();
/*for(i=0;i<array.length;i++)
{
	//alert(array[i]); '+balance+'
}*/
					if(array1 !== null)
					{
						/*if(document.getElementById("available").value == '0.00')
						{
							totalBalance = '0.00';
						}else*/{
						totalBalance = array1[0];}
					}
					if(document.getElementById('lastMonth').value === group)
					{
					   string = document.getElementById('available').value;
					   string = string.replace(/,(?=\d{3})/g, '');
					   totalBalance = string;
					}
					//'+balance+'
					var closingBalance = parseFloat(closingBalance).toFixed( 2 );

					totalBalance = parseFloat(totalBalance).toFixed(2);
                    $(rows).eq( i ).before(
                        '<tr class="group"><td>'+group+'</td><td></td><td>'+total+'</td><td>'+
						totalUnPaid+'</td><td>'+totalPayOut+'</td><td>'+charges+'</td><td>'+holdvalue+'</td><td></td><td>'+percentage+'</td><td></td><td>'+closingBalance+'</td></tr>'//</tr>'//change#4 
<!-- <th>UserCode</th><th>Month</th><th>Action Date</th><th>Income</th><th>Unpaids</th><th>To Pay</th><th>Paid</th><th>Charges</th><th>Security</th><th>DR/CR</th><th>Security(%)</th><th>Status</th><th>Available Balance</th>-->

					);
                    last = group;
                }
            } );
        },
		initComplete:function()
		{
			$.fn.dataTable.ext.search.push(
			function (settings, data, dataIndex){
				var min = Date.parse( $('#month').val(), 10 );
				var max = Date.parse( $('#tomonth').val(), 10 );
				var Action_Date=Date.parse(data[1]) || 0;
				if ( ( isNaN( min ) && isNaN( max ) ) ||
					 ( isNaN( min ) && Action_Date <= max ) ||
					 ( min <= Action_Date   && isNaN( max ) ) ||
					 ( min <= Action_Date   && Action_Date <= max ) )
				{
				  $(document).ready();
					return true;

				}
				return false;
			}
			);
		}
    } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );

table.buttons().container().appendTo('#example_wrapper .col-sm-6:eq(0)')
//Event listner
$('#month,#tomonth').change(function(){table.draw();});

/*$('#srUserCode').on( 'keyup', function () {
      table.search( this.value ).draw();
  } );

$('#srUserCode').on( 'change', function () {
      table.search( this.value ).draw();
  } );*/

/*$('#month').on('change', function(){
   table.search( this.value ).draw();
});

$( "#month").change();
*/

} );
function sortFloat(a,b) { return a - b; }

function removeElementsWithValue(arr, val) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === val) {
            arr.splice(i, 1);
        }
    }
    return arr;
}


function call_script_collectionrecon()
{
	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;


	var UserCode = <?php echo "'".$UserCode."'"; ?>;

	var role = <?php echo "'".$role."'"; ?>;

	var srUserCode = <?php echo "'".$srUserCode."'"; ?>;

	//alert(userid+' - '+IntUserCode+'-'+role);
var list =
{'month': document.getElementById('month').value,
'tomonth': document.getElementById('tomonth').value,
'userid' : userid,'role':role,
'IntUserCode': IntUserCode,
'UserCode' : UserCode,
'srUserCode':document.getElementById('srUserCode').value
};
					//alert(document.getElementById('Initials').value);
				var feedback = '';
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"script_collectionrecon.php",
				       data:{list:list},
					   //dataType : 'json',
						success: function(data)
						 {
							feedback = data;
							       // alert(data); // alerts first string

							//var results   = document.getElementById('results');
							//results.value = feedback;
							// -- Updating the table mappings
		/*					var message_info = document.getElementById('message_info');

PayNow = document.getElementById("PayNow");
Exit = document.getElementById("Exit");
backtoconfirm = document.getElementById("backtoconfirm");
addbeneficiary = document.getElementById("addbeneficiary");
							if(feedback.includes("success"))
							{
								message_info.innerHTML = 'Beneficiary added successfully.';
								message_info.className  = 'status';
								PayNow.style.visibility = '';
								Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								addbeneficiary.style.visibility = '';
								var res = feedback.split("|");
								document.getElementById('bid').value = res[1];
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								PayNow.style.visibility = 'hidden';
								Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";
								addbeneficiary.style.visibility = 'hidden';
							}
*/
						 }
					});
				});
				return feedback;
}
</script>
