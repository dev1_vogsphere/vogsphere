<?php 
	require 'database.php';
	
	$userid = null;
	
	if ( !empty($_GET['userid'])) 
	{
		$userid = $_REQUEST['userid'];
		// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
		$userid = base64_decode(urldecode($userid)); 
		// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$userid = $_POST['userid'];
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	 try {			
			$sql = "DELETE FROM user WHERE userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($userid));
			Database::disconnect();
			echo("<script>location.href='user.php';</script>");
		  } 
	 catch (PDOException $ex) 
		  {
			echo  $ex->getMessage();
		  }	
	} 
// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	else 
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "SELECT * FROM user WHERE userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($userid));		
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(empty($data))
			{
				header("Location: index");
			}
	}
	// -- EOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	
?>
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action="deleteuser.php" method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Delete a User
						    </a>
					    </h2>
						</div>						
				   </tr>
				   
	    			  <input type="hidden" name="userid" value="<?php echo $userid;?>"/>
					  <p class="alert alert-error">Are you sure you want to delete this item?</p>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Yes</button>
						  <a class="btn" href="users.php">No</a>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
