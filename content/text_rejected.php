<?php 
	require 'database.php';
	
	$customerid = null;
	$ApplicationId = null;
	$rejectionreasons='';
	$changedby=$_SESSION['username'];
	// -- BOC Encrypt 16.09.2017.
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$dbname = "eloan";
	// -- EOC Encrypt 16.09.2017.
    //get cancellations Reason
	/* -- Database Declarations and config:*/
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$sql = 'SELECT * FROM loanrejectionreasons';
	$rejectionreasons= $pdo->query($sql);
	$rejectionreasonsArr = array();
	foreach($rejectionreasons as $row)
	{
	$rejectionreasonsArr[] = $row['idrejection'].'|'.$row['rejectiondescriptions'];
	}
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.

	}
	
		if ( !empty($_GET['ApplicationId'])) {
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.		
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	if(isset($_POST['rejectionreason']))
	{
	$RejectionReason = $_POST['rejectionreason'];
	}

	if ( !empty($_GET['customerid'])) {
		$id = $_REQUEST['customerid'];
	}
	
	if ( !empty($_POST)) 
	{
		// keep track post values
		$customerid = $_POST['customerid'];
		$ApplicationId = $_POST['ApplicationId'];
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['rejectreason'] =$RejectionReason;
		$ExecApproval = 'REJ';
				
		$RejectionDate = date("Y-m-d");
		
		// Update status to cancelled

			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			
			$sql = "UPDATE loanapp";
			$sql = $sql." SET ExecApproval = ?, rejection_date= ?, changedby = ?, rejection_reason = ?";
			$sql = $sql." WHERE customerid = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ExecApproval,$RejectionDate,$changedby,$RejectionReason,$customerid,$ApplicationId));
			Database::disconnect();
		// -- Email send.
			SendEmailpage('content/text_emailLoanStatusRejected.php');			
			echo("<script>location.href = 'custAuthenticate.php';</script>");
		}
	   // -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
	  else 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid,$ApplicationId));		
			$data = $q->fetch(PDO::FETCH_ASSOC);
			
			// -- Get the user details.
			$sql =  "select * from user where userid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid));		
			$dataUser = $q->fetch(PDO::FETCH_ASSOC);
			$_SESSION['email'] = $dataUser['email'];
			$_SESSION['FirstName'] = $data['FirstName'];
			$_SESSION['LastName'] = $data['LastName'];
			$_SESSION['Title'] = $data['Title'];

			//print_r($dataUser);echo "<br/>";
			//print_r($data);
			if(empty($data))
			{
				header("Location: custAuthenticate");
			}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		}	
		
?>
<!-- <div class="container">-->
<div class='container background-white bottom-border'> <!-- class='container background-white'> -->
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
	    			<form class="login-page"  class="form-horizontal" action=<?php echo "".$_SERVER['REQUEST_URI']."";?> method="post">
					<tr>
						<div class="panel-heading">						
						<h2 class="panel-title">
							<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
							Reject a loan application
						    </a>
					    </h2>
						</div>						 
				   </tr>
			   
					  <input type="hidden" name="customerid" value="<?php echo $customerid;?>"/>
					  <input type="hidden" name="ApplicationId" value="<?php echo $ApplicationId;?>"/>
					<p class="alert alert-error">Please select rejection reason:</p>
					<!--<input type="text" class="form-control" name="cancellationreason" id="cancellationreason" required="required" maxlength="100"  value="<?php echo $CancellationReason;?>">-->
					<div class="form-group">
				 			<label for="form_account_type">Rejection Reason</label>
				 				<select class="form-control" id="rejectionreason" name="rejectionreason">
				 					<?php
				 					$rejectionreasons_Select = $rejectionreasons;
									foreach($rejectionreasonsArr as $key => $row )
										{
										     $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
						 					 if($rowData[0] == $rejectionreasons_Select)
						 					 {echo  '<option value="'.$rowData[1].'" selected>'.$rowData[1].'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData[1].'">'.$rowData[1].'</option>';}
										 }
				 					if(empty($rejectionreasonsArr))
				 					{echo  '<option value="0" selected>No Reason</option>';}

				 					?>
				 					</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
    
					  <div class="form-actions">
						  <button type="submit" class="btn btn-danger">Continue</button>
						  <a class="btn" href="custAuthenticate.php">Back</a>
						</div>
					</form>
				</div>
		</div>		
</div> <!-- /container -->
