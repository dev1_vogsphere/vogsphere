<?php 
require 'database.php';

$placeholderError = null;
$placeholder = '';
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$placeholder = $_POST['placeholder'];

	$valid = true;
	
	if (empty($placeholder)) { $placeholderError = 'Please enter placeholder name.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$sql = "select * from placeholder where placeholder = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($placeholder));
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(!empty($data))
			{
				$placeholderError = 'placeholder aleady exist!.'; $valid = false;
			}
			
			if ($valid) 
			{
				$sql = "INSERT INTO placeholder (placeholder) VALUES(?) "; 		
				$q = $pdo->prepare($sql);
				$placeholder = '['.$placeholder.']';
				$q->execute(array($placeholder));
				$count = $count + 1;
			}
			Database::disconnect();
		}
}
?>

   <div class='container background-white bottom-border'>		
	<div class='row margin-vert-30'>
		<form action='' method='POST' class="signup-page"> 
			<!-- <h2 class="panel-title">
								<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
									 New placeholder Details
								</a>
							</h2> -->
			<?php # error messages
					if (isset($message)) 
					{
						foreach ($message as $msg) 
						{
							printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>placeholder successfully added.</p>");
					}
			?>
			<div class="form-group <?php if (!empty($placeholderError)){ echo "has-error";}?>">
				<p><b>placeholder</b><br />
				<input style="height:30px"  class="form-control margin-bottom-20" type='text' name='placeholder'/>
				<?php if (!empty($placeholderError)): ?>
				<small class="help-block"><?php echo $placeholderError;?></small>
				<?php endif; ?>
				</p>	
			</div>			
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'placeholders';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			//echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
			//	<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>	

  <button class='btn btn-primary' type='submit'>Save</button><!-- Generate OTP -->
  <a class='btn btn-primary'  href=<?php echo $backLinkName?> >Back</a><!-- Generate OTP -->
			
		     </div></td>
		</tr>
		</table>	
		
		</form> </div></div>