<div class='container background-white bottom-border'>
	<div class='row margin-vert-90'>
		 <div class="col-md-12 text-left">
		 </br>

			 <h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'About eCashMeUp';}?></h2>
			 <p class='p1'>Welcome to eCashMeUp, where financial solutions meet cutting-edge technology. Founded in 2012 under Vogsphere (Pty) Ltd and registered as a separate entity in 2017. We are a dynamic FinTech company committed to revolutionizing the way businesses and individuals engage with financial services. As a trailblazer in the industry, we pride ourselves on our innovative solutions and commitment to driving financial inclusion. </p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'The markets we operate in';}?></h2>
			<p class='p1'>The eCashMeUp technology platform enables small to large enterprises in numerous vertical markets, including Telecommunications, Insurance, Education sector, Fitness Professionals, Retail and Industrial Union to improve and optimize cash flow and better engagement with customers.</p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'We place emphasis on the following';}?></h2>

			<p class='p1'><?php {echo "<ul class='p1'>";echo "<li>Innovate, with great power comes great responsibility</li>";echo "<li>Respect the privacy of others</li>";echo "<li>Think before solutioning</li>";echo "<li>Collaborate with others</li>";echo "<li>Integrity is all about the commandments</li>";echo "<li>Agility is all about delivering value</li>"; echo "</ul>";}?></p>

			<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'Our Vision';}?></h2>
				<p class='p1'>It is the vision of eCashMeUp to engineer robust financial solutions for the African Continent.</p>

				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'Our Mission Statement';}?></h2>
				<p class='p1'>To provide the financial industry with an innovative platform to solve the fundamental financial matters in the digital era</p>

				<h2 class="page-header" id="servicetitle" name="servicetitle" ><?php {echo 'Our Value Proposition';}?></h2>

				<p class="p1">Our client’s value proposition is highlighted as follows:></p>

				<p class='p1'><?php {echo "<ul class='p1'>";echo "<li>Better Collaboration with your Customers</li>";echo "<li>Automated Processes</li>";echo "<li>Continuous Delivery</li>";echo "<li>Self-service</li>";echo "<li>Improved Cash Flow Management</li>"; echo "</ul>";}?></p>


			</br>
			 <p class="p1">
			<button id="GetInTouch" type="button" class="btn btn-link" <font color="#FF8C00"><a class="p1" href='contactus'>Join us as we transform Africa</a></font></button>
			</p>
		 </div>
	 </div>
</div>
