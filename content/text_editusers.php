<?php 
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
//1. ---------------------  Role ------------------- //
$sql = 'SELECT * FROM role ORDER BY role';
$dataRoles = $pdo->query($sql);	
  
$tbl_user="user"; // Table name 
$passwordError = null;
$password = '';

$emailError = null;
$email = ''; 
$hashUrl = '';
$role = '';

$IntCode  = '';
$UserCode = '';			

$debicheckusercode = '';
$usercodePayments = '';
$count = 0;
$userid = 0;
$approver = '';
$username = getsession('username');

$Original_email = '';

// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$UserIdDecoded = "";
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.

if (isset($_GET['userid']) ) 
{ 
$userid = $_GET['userid']; 
// -- BOC DeEncrypt 16.09.2017 - Parameter Data.
$userid = base64_decode(urldecode($userid)); 
// -- EOC DeEncrypt 16.09.2017 - Parameter Data.
}

if (!empty($_POST)) 
{   $count = 0;
	//$password = $_POST['password'];
	$email = $_POST['email'];
	$userid = $_POST['userid'];
	$Original_email = $_POST['Original_email'];
	$debicheckusercode = $_POST['debicheckusercode'];
	$usercodePayments = $_POST['usercodePayments'];
	$role 			   = getpost('role');

	// -- Get approver checkbox
	if(isset($_POST['approver']))
	{$approver = 'X';}
	else
	{$approver = '';}
	
	$IntCode  = getpost('IntCode');
	$UserCode = getpost('UserCode');
	
	$valid = true;
	
	//if (empty($password)) { $passwordError = 'Please enter password Description.'; $valid = false;}
		// validate e-mail
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}	
	
	if (empty($email)) 
	{ $emailError = 'Please enter e-mail'; $valid = false;}
	else
	{
	//echo '<h2>modise'.$Original_email.'</h2>';
	// User - E-mail.
		if ($valid and ($Original_email != $email))
		{
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
		}
	 }
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE user SET email = ?,approver = ?,role = ? WHERE userid = ?"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$password = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($email,$approver,$role,$userid)); 
			// -- Update DebiCheck user Code in customer table.
			$changedby = getsession('username');;
			$changedon = date('Y-m-d');
			$changedat = date('h:m:s');
			$_SESSION['role'] = $role;
			//debicheckusercode
			$sql = "UPDATE customer SET changedby = ?,changedon = ?,changedat = ?,UserCode = ?,debicheckusercode  = ? ,IntUserCode = ?,usercodePayments = ? WHERE CustomerId = ?"; 		
			$q = $pdo->prepare($sql);
			//  -- BOC encrypt password - 16.09.2017.	
			$password = Database::encryptPassword($password);
			//  -- EOC encrypt password - 16.09.2017.	
			$q->execute(array($changedby,$changedon,$changedat,$UserCode,$debicheckusercode,$IntCode,$usercodePayments,$userid)); 
			
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from user where userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- customer
		$sql =  "select * from customer where CustomerId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$dataCustomer = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header("Location: custAuthenticate");
		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		$password 			= $data['password'];
		$email 				= $data['email'];
		$Original_email 	= $data['email'];
		$debicheckusercode  = $dataCustomer['debicheckusercode'];
		$approver = $data['approver'];
		$UserCode = $dataCustomer['UserCode'];
		$IntCode  = $dataCustomer['IntUserCode'];
	    $role 			    = $data['role'];
		//  -- BOC encrypt password - 16.09.2017.	
		$hashUrl = Database::encryptPassword($data['email'].$data['password']);
		//  -- EOC encrypt password - 16.09.2017.
		$usercodePayments = $dataCustomer['usercodePayments'];
}

?>
<div class="container background-white bottom-border">
<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page">    
<div class="panel-heading">
<h2 class="panel-title">
                    <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                         Change User Details
                    </a>
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>User successfully updated.</p>");
		}
?>
				
</div>				
			<p><b>User ID</b><br />
			<input style="height:30px" type='text' name='userid' value="<?php echo !empty($userid)?$userid:'';?>" readonly />
			</p>
			
			
			<p><b>E-mail</b><br />
			<input style="height:30px" type='text' name='email' value="<?php echo !empty($email)?$email:'';?>"/>
			<?php if (!empty($emailError)): ?>
			<span class="help-inline"><?php echo $emailError;?></span>
			<?php endif; ?>
			</p> 
			<?php if(getsession('role') == 'admin'){ ?>
			<p><b>DebiCheck UserCode</b><br />
			<input style="height:30px" type='text' id='debicheckusercode' name='debicheckusercode' value="<?php echo !empty($debicheckusercode)?$debicheckusercode:'';?>"/>
			</p>
			
			<p><b>User Code</b><br />
			<input style="height:30px" type='text' id='UserCode' name='UserCode' value="<?php echo !empty($UserCode)?$UserCode:'';?>"/>
			</p> 

			<p><b>Internal User Code</b><br />
			<input style="height:30px" type='text' id='IntCode' name='IntCode' value="<?php echo !empty($IntCode)?$IntCode:'';?>"/>
			</p> 
			<p><b>Payments usercode</b><br />
			  <input style="height:30px" type='text' id='usercodePayments' name='usercodePayments' value="<?php echo !empty($usercodePayments)?$usercodePayments:'';?>"/>
			</p>
			<p><b>Is an approver?</b><br />
				<input style="height:30px" type='checkbox' id='approver' name='approver' value="<?php echo !empty($approver)?$approver:'';?>" 
				<?php if(!empty($approver)){echo'checked';}?> />
			</p> 
			
			<p><b>Role</b><br />
				<div class="controls">
					<SELECT class="form-control" id="role" name="role" size="1">
					<?php
						$roleLoc = '';
						$roleSelect = trim($role);
						echo $roleSelect;
						foreach($dataRoles as $row)
						{
							$roleLoc = $row['role'];

							if($roleLoc == $roleSelect)
							{
								echo "<OPTION value=$roleLoc selected>$roleLoc</OPTION>";
							}
							else
							{
								echo "<OPTION value=$roleLoc>$roleLoc</OPTION>";
							}
						}
												
						if(empty($dataRoles))
						{
							echo "<OPTION value=0>No Roles</OPTION>";
						}
					?>
					</SELECT>
				</div>
			<?php } ?>	
			</p>
			<input style="height:30px" type='hidden' name='Original_email' value="<?php echo !empty($Original_email)?$Original_email:'';?>"   />
			</p>
			
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update User</button>
				<?php if($_SESSION['role'] == 'admin'){?>
				<a class="btn btn-primary" href="resetpassword?userid=<?php echo $userid.'&hash='.$hashUrl;?>">Reset Password</a>
				<a class="btn btn-primary" href="users">Back</a>
				<?php }else{?>
				 <a class="btn btn-primary" href="resetpassword?userid=<?php echo $userid.'&hash='.$hashUrl;?>">Reset Password</a>
				<?php }?>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div></div>
