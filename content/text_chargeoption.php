<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
 $tbl_name="user"; // Table name 
 $chargeoptionname = '';
  
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search Charge Options</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px' id='chargeoptionname' name='chargeoptionname' placeholder='Charge Option' class='form-control' type='text' value=$chargeoptionname>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>";
	}  							
?>  
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				<!-- <p>
					<a href="create.php" class="btn btn-success">Create</a>
				</p> -->
				  <div class="table-responsive">

				
		              <?php 
					   include 'database.php';
					   $pdo = Database::connect();
					   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $chargeoptionname = $_POST['chargeoptionname'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($chargeoptionname))
					    {
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM chargeoption WHERE chargeoptionname = ?";
						$q = $pdo->prepare($sql);
					    $q->execute(array($chargeoptionname));	
						$data = $q->fetchAll();  
						
					    }
						else
						{
							// -- All Records
						//$result = mysql_query("SELECT * FROM `accounttype`") or trigger_error(mysql_error()); 
						$sql = "SELECT * FROM chargeoption";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll();  
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////					  
						echo "<a class='btn btn-info' href=newchargeoption.php>Add New Charge Option</a><p></p>"; 
						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Charge Option ID</b></td>"; 
						echo "<td><b>Charge Option Name</b></td>"; 
						echo "<td><b>Charge Option Description</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['chargeoptionid']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['chargeoptionname']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['chargeoptiondesc']) . "</td>";  						
						echo "<td valign='top'><a class='btn btn-success' href=editchargeoption.php?chargeoptionid={$row['chargeoptionid']}>Edit</a></td>";
						
						// -- Do not Delete 1,2,3,4 option.
						if($row['chargeoptionid'] != 1 AND $row['chargeoptionid'] != 2 AND 
						   $row['chargeoptionid'] != 3 AND $row['chargeoptionid'] != 4 )
						{
						echo "<td>
						<a class='btn btn-danger' href=deletechargeoption.php?chargeoptionid={$row['chargeoptionid']}>Delete</a></td> "; 
						}
						else
						{
						  echo "<td>default option</td>";
						}
						
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->