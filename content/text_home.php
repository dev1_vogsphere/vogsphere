<script src="assets/js/3.3.6/bootstrap.min.js"></script>
<!-- <script src="assets/js/jquery-1.11.3.min.js"></script> -->

<?php

// -- Personal Details
$Title 		= '';
$FirstName  = '';
$LastName 	= '';
$refnumber  = '';
$customerid = '';
$customeridError = '';

// -- Contact Details.
$Lphone 		= '';

$phoneError			='';
$FirstNameError 	= '';
$LastNameError 		= '';
$tbl_lead 		= 'lead';
$valid = true;
$email = '';
$message = '';
$idnumberError = '';
$emailError = '';
$phoneError = '';

$comments = '';

$count = 0;

/*
company NAME
reg nr
contct name
phone
EMAIL
*/
?>
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<style>

html {
  font-size: 16px;
}

/*h3 {
  font-family: 'sharp grotesk semibold', sans-serif;
  font-size: 2.125rem;
  font-weight: 700;
  color: #444;
  letter-spacing: 1px;
  text-transform: uppercase;
  margin: 55px 0 35px;
}
width: 90%; */

.carousel-inner { margin: auto; }
.carousel-control 			 { width:  4%; }
.carousel-control.left,
.carousel-control.right {
  background-image:none;
}

.glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right {
  margin-top:-10px;
  margin-left: -10px;
  color: #444;
}

.carousel-inner {
  a {
    display:table-cell;
    height: 180px;
    width: 200px;
    vertical-align: middle;
  }
  img {
    max-height: 500px;
    margin: auto auto;
    max-width: 100%;
  }
}

@media (max-width: 767px) {
  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(100%, 0, 0);
      transform: translate3d(100%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-100%, 0, 0);
      transform: translate3d(-100%, 0, 0);
  }

}
@media (min-width: 767px) and (max-width: 992px ) {
  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(50%, 0, 0);
      transform: translate3d(50%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-50%, 0, 0);
      transform: translate3d(-50%, 0, 0);
  }
}
@media (min-width: 992px ) {

  .carousel-inner > .item.next,
  .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(16.7%, 0, 0);
      transform: translate3d(16.7%, 0, 0);
  }
  .carousel-inner > .item.prev,
  .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-16.7%, 0, 0);
      transform: translate3d(-16.7%, 0, 0);
  }

}
.iconImage {
    display: block;
    width: 76px;
    height: 76px;
    margin: 0 auto 4px auto;
    padding: 0;
    -webkit-transition: all.2s;
    -moz-transition: all.2s;
    -o-transition: all.2s;
    transition: all.2s;
    position: relative;
    top: 0;
	border-solid:1px;
    background-repeat: no-repeat!important;
}
</style>
            
			<div id="slideshow">
                <div class="container no-padding background-white bottom-border">
                    <div class="row">
                        <!-- Carousel Slideshow -->
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <!-- Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example" data-slide-to="1"></li>
                                <li data-target="#carousel-example" data-slide-to="2"></li>
								<li data-target="#carousel-example" data-slide-to="3"></li>
                            </ol>
                            <div class="clearfix"></div>
                            <!-- End Carousel Indicators -->
                            <!-- Carousel Images -->
                            <div class="carousel-inner">
							<div class="item active">
							<img src="assets/img/slideshow/msme_transformation.jpg">
							</div>
							<div class="item">
							<img src="assets/img/slideshow/insuranceonboarding.jpg">
							</div>
							<div class="item">
							<img src="assets/img/slideshow/ImaniRestAssured.jpg">
							</div>
							<div class="item">
							<img src="assets/img/slideshow/October23/Public_Private_Banner.jpg">
							</div>

							<div class="item">
							<img src="assets/img/slideshow/October23/DebiCheckVP1.jpg">
							</div>

							<div class="item">
							<img src="assets/img/slideshow/October23/loanbanner2.jpg">
							</div>
						
                        </div>
                        <!-- End Carousel Slideshow -->
                    </div>
                </div>
            </div>
                <div class="container background-grey bottom-border">
                  <br>
                  <!--<div align=center ><H1><strong>Efficient Way to Improve Your Business Cash Flow Management Processes<strong></H1>
                    <H3>Solution To Solve Fundamental Financial Matters In The Digital Era</H3>
                  </div>-->
                    <div class="row padding-vert-60">


                        <!-- Icons -->
                        <div class="col-md-4 text-center">

                            <!--<i class="fa-money fa-4x color-primary animate fadeIn"></i>
                             -->
							<a href="addleads?service=Electronic Signatures"><img class="iconImage rounded mx-auto d-block animate fadeIn" src="images/icon/ImaniRestAssured.png" /></a>
<h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00"> Imani Rest Assured </font></h3>
                            <p class="p1">All-in-one solution tailored for funeral parlours. From managing member policies to automated notifications, we've got you covered. Stay ahead with our comprehensive platform.</p>

							<p class="p1">
                           <!--<button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=Imani Rest Assured" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>
                           </p>
						</div>

                        <div class="col-md-4 text-center">
							<a href="addleads?service=DebiCheck"><img class="iconImage animate fadeIn" src="images/icon/debicheck.png" /></a>
                            <h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00">DebiCheck</font></h3>
                            <p class="p1">DebiCheck lets your customers easily confirm debit order details electronically, ensuring payments are collected without disputes. With up to 32 days of tracking, you’ll enjoy improved success rates and smoother cash flow.</p>
                            <p class="p1">
                           <!--<button id="DebiCheck" type="button" class="btn btn-link" data-popup-open="popup-1" onClick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=DebiCheck" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>
                           </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <!--<i class="fa fa-credit-card fa-4x color-primary animate fadeIn"></i>-->
							<a href="addleads?service=NAEDO"><img class="iconImage animate fadeIn" src="images/icon/naedo.png" /></a>
                            <h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00">NAEDO</font></h3>
                            <p class="p1">The NAEDO debit order service is a Non-Authenticated Early Debit Order that helps to track a customer’s account for up to 32 days and process your debit order when funds become available</p>
                            <p class="p1">
                           <!--<button id="NAEDO" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=NAEDO" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>
                           </p>

                        </div>
                        <!-- End Icons -->
                    </div>
					     <!-- <hr class="tall">-->
					             <div class="row padding-vert-5">
                        <!-- Icons -->

                        <!--  <div>Icons made by <a href="https://www.flaticon.com/authors/pause08"
                          title="Pause08">Pause08</a> from <a href="https://www.flaticon.com/"
                          title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
                          title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
                        </div>-->

                        <div class="col-md-4 text-center">
                            <!-- <i class="fa-cogs fa-4x color-primary animate fadeIn"></i>
                             -->
							<a href="addleads?service=Debit"><img class="iconImage animate fadeIn" src="images/icon/efticon.png"/></a>
							<h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00">EFT Debit Order</font></h3>
                            <p class="p1">Streamline your recurring payments with our EFT Debit Order service, offering flexible same-day or two-day collections. Plus, easily process bulk credit card payments through our trusted partner, so you can focus on growing your business!</p>
                            <p class="p1">
                           <!--<button id="debitorder" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=Debit" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>
                           </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <!-- <i class="fa-cloud-download fa-4x color-primary animate fadeIn"></i>
                             -->
							<a href="addleads?service=Cash-Pay"><img class="iconImage animate fadeIn" src="images/icon/ecashmeuppay.png" /></a><h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00">eCashMeUp Pay</font></h3>
                            <p class="p1">Cash-Pay is a reliable solution to pay your employees and suppliers Anytime, Anywhere,</p>
                            <p class="p1">
                           <!--<button id="CashPay" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=Cash-Pay" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>
                           </p>
                      </div>
                        <div class="col-md-4 text-center">
                            <!--<i class="fa-bar-chart fa-4x color-primary animate fadeIn"></i>
                             -->
							<a href="addleads?service=Account Verification Service (AVS)"><img class="iconImage animate fadeIn" src="images/icon/bankaccountverification.png" /></a><h3 class="padding-top-10 animate fadeIn"><font color="#FF8C00">Bank Account Verification</font></h3>
                            <p class="p1">Account verification service (AVS) is a simple and efficient way to verify your customer’s bank accounts</p>
                            <p class="p1">
                           <!--<button id="accountverification" type="button" class="btn btn-link" data-popup-open="popup-1" onclick="content(this)"><font color="#FF8C00">Subscribe</font></button>-->
						    <a href="addleads?service=Account Verification Service (AVS)" class="btn btn-link"><font color="#FF8C00">Learn more</font></a>

						   </p>

                        </div>


                        <!-- End Icons -->
                    </div>
					</br>
<!--Product Cards -->
<div class="container">
<h1 class="text-center"><strong>Effortless Payment Solutions for Every Business Size</strong></h1>
</br>
<h2 class="text-center"><strong>Choose Our Versatile Card Machines</strong></h2>
</br>
</br>
  <div class="row">
    <div class="col-md-4">
			<div class="card-deck">
				<div class="card">
					<img src="assets/img/slideshow/epos3.jpg"" class="card-img-top" alt="...">
					<div class="card-body">
						<h3 class="card-title text-center"><strong>ePOS 3</strong></h3>
							<ul class=" p1 card-text">
								<li>Swipe, Insert and Tap</li>
								<li>Waterproof, dustproof and drop-proof screen which has passed 1m drop test</li>
								<li>WiFi 3G and 4G modules are adopted to build powerful networking capability</li>
								<li>Campact and portable for accepting payments on the go</li>
								<li>600mAh high volume battery ensures 72 hours operation</li>
							</ul> 
						<!--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
						<a  href="https://ecashmeup.com/estorelive/product/epos-3" class="btn btn-link"><font color="#FF8C00"><strong>Buy Now</strong></font></a>
					</div>
				</div>
			</div>
	</div>
	 <div class="col-md-4">
	 <div class="card-deck">
				<div class="card">
					<img src="assets/img/slideshow/R1.jpg" class="card-img-top" alt="..."> <!--286x180-->
					<div class="card-body">
						<h3 class="card-title text-center"><strong>eR1</strong></h3>
							<ul class=" p1 card-text">
								<li>Swipe, Insert and Tap</li>
								<li>Smooth edges & thin profile</li>
								<li>Large battery capacity</li>
								<li>MSR/IC card</li>
								<li>NFC payments</li>
								<li>Campact and portable for accepting payments on the go</li>
								<li>600mAh high volume battery ensures 72 hours operation</li>
							</ul> 
						<!--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
						<a  href="https://ecashmeup.com/estorelive/product/er1-0" class="btn btn-link"><font color="#FF8C00"><strong>Buy Now</strong></font></a>
				</div>
			</div>
	</div>
	</div>
	 <div class="col-md-4">
	 <div class="card-deck">
				<div class="card">
					<img src="assets/img/slideshow/minipos.jpg" class="card-img-top" alt="...">
					<div class="card-body">
					<h3 class="card-title text-center"><strong>eMini</strong></h3>
							<ul class=" p1 card-text">
								<li>Swipe, Insert and Tap</li>
								<li>Professional scanning module is optional</li>
								<li>Campact and portable for accepting payments on the go</li>
								<li>NFC payments</li>
								<li>600mAh high volume battery ensures 72 hours operation</li>
								<li>Diverse payment options</li>
								<li>Large battery capacity</li>
							</ul> 
							<!--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
							<a  href="https://ecashmeup.com/estorelive/product/eposmini-8909" class="btn btn-link"><font color="#FF8C00"><strong>Buy Now</strong></font></a>
					</div>
				</div>
			</div>
	</div>
  
  </div>
</div>





<!-- </div>
		<div class="container background-grey bottom-border"> -->
                            <hr >
                            <!-- Testimonials -->
							<?php include 'testimonials.php'; ?>
</div>
                            <!-- End Testimonials - default full width -->
		<!-- Replaced with Customers Testimonial
		<div class="container background-grey bottom-border">
				<div class="row padding-vert-10">
				<div class="col-md-12 text-center"><h2>Integration Ecosystem</h2>
          <p> The eCashMeUp Platform is integrated to various platforms to provide value to our customers. eCashMeUp has invested heavily in financial technologies to solve fundamental financial matters in the digital era</p>
        </br>
        </div>
      </br>
      </br>
      </br>
<div class="col-md-14 col-md-offset-1">
<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
  <div class="carousel-inner">
    <div class="item active">
      <div class="col-md-2 col-sm-6 col-xs-12"><a href="#"><img src="assets/img/slideshow/NCR.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/Mercantile.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/TransUnion.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-1 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/cm.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/Vogsphere.jpg" class="img-responsive"></a></div>
    </div>
    <div class="item active">
      <div class="col-md-2 col-sm-4 col-xs-12"><a href="#"><img src="assets/img/slideshow/whatsapp.jpg" class="img-responsive"></a></div>
    </div>
  </div>

</div>
</div>

				</div>
		</div> -->

			<!-- BOC - PopUp for Trial -->
					 <div class="popup" data-popup="popup-1" style="overflow-y:auto;z-index: 2;">
						<div class="popup-inner">
										<div id="divContent" style="top-margin:100px;font-size: 10px;">
<!-- BOC Form to enable capuring of Data -->
<div class='container background-white bottom-border'>
<div class='row margin-vert-30'>
	<div class="col-xs-12 col-sm-12">
				<form class="form-horizontal" method="post" id="captcha-form" name="captcha-form"  role="form">
				<?php # error messages
					if (isset($message))
					{
						//foreach ($message as $msg)
						{
							//printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>Lead $message was successfully created.</p>");
					}
			   ?>
				<h4 class="page-header" id="servicetitle" name="servicetitle" >Company Information Details</h4>
				<div id="companyError" class="form-group">
						<label class="col-sm-2 control-label">Registration ID</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="customerid" name="customerid" type="text"  placeholder="registration number" value="<?php echo !empty($customerid)?$customerid:'';?>">
						</div>
						<div class="col-sm-4">
							<small id="customeridErrorText" class="help-block" style=""></small>
						</div>
				</div>

				<div id="FirstNameError" class="form-group">
						<label class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="FirstName" name="FirstName" type="text"  placeholder="Company name" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
						</div>
						<div class="col-sm-4">
							<small id="FirstNameErrorText" class="help-block" style=""></small>
						</div>
				</div>
				<div id="LastNameError" class="form-group">
						<label class="col-sm-2 control-label">Contact Name</label>
						<div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="LastName" name="LastName" type="text"  placeholder="contact person" value="<?php echo !empty($LastName)?$LastName:'';?>">
						</div>
						<div class="col-sm-4">
							<small id="LastNameErrorText" class="help-block" style=""></small>
						</div>
				</div>

			 <h4 class="page-header">Contact Details</h4>
				<div id="phoneError" class="form-group">
						<label class="col-sm-2 control-label">Contact number</label>
							<div class="col-sm-4">
								<input  class="form-control margin-bottom-10" id="Lphone" name="Lphone" type="text"  placeholder="phone number" value="<?php echo !empty($Lphone)?$Lphone:'';?>">
							</div>
							<div class="col-sm-4">
								<small id="phoneErrorText" class="help-block" style=""></small>
							</div>
				</div>
				<div id="emailError" class="form-group">
					<label class="col-sm-2 control-label">Email</label>
					 <div class="col-sm-4">
							<input class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
					 </div>
					 <div class="col-sm-4">
							<small id="emailErrorText" class="help-block" style=""></small>
					 </div>
				</div>
				<!--<h4 class="page-header"></h4>

					 <div class="clearfix"></div> -->

					<div class="form-group">
						<div class="col-sm-2">
							<button type="button" class="btn btn-primary btn-label-left" onclick="validate_save()">
							<span><i class="fa fa-clock-o"></i></span>
								Submit
							</button>
						</div>
					</div>
				</form>
			<!-- </div> 298 -->
		</div>
	</div>
</div>
<!-- EOC Capuring data... -->
										</div>
							<p class="p1"><a data-popup-close="popup-1" href="#">Close</a></p>
							<a class="popup-close" data-popup-close="popup-1" href="#">x</a>
						</div>
					 </div>
					</div>
			<!-- EOC - PopUp for Trial -->


                </div>
            </div>
		<input class="form-control margin-bottom-10" id="service" name="service" type="hidden"  placeholder="service">
<script>

/*function content(service)
{
		document.getElementById("service").value = service.id;
}
*/
function content(service)
{
	//alert(service.id);
	document.getElementById("service").value = service.id;
	document.getElementById("servicetitle").innerHTML = 'Company Information Details for '+service.id;
}

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		 /* var elem = document.getElementById("myBar");
		  var width = 10;
        elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';*/

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

function move() {
  var elem = document.getElementById("myBar");
  var width = 10;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
      elem.innerHTML = width * 1  + '%';
    }
  }
}

function htmlEntities(str)
{
	return String(str).replace('&amp;',/&/g).replace('&lt;',/</g).replace('&gt;',/>/g).replace('&quot;',/"/g);
}


 // -- Enter to capture comments   ---
 function validate_save()
 {
	 	 customerid  = document.getElementById('customerid').value;
	     FirstName	  = document.getElementById('FirstName').value;
	     LastName	  = document.getElementById('LastName').value;
	     Lphone		  = document.getElementById('Lphone').value;
	     email		  = document.getElementById('email').value;
		 service = document.getElementById('service').value;
		 valid = true;

		if(customerid != '' && FirstName != '' && Lphone != '' && email != '' && LastName != '')
		{

			if(phonenumber(Lphone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number';
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

    //alert("You have entered an invalid email address!")
			if(ValidateEmail(email) != true)
			{
				document.getElementById("emailError").setAttribute("class", "form-group has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email';
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group");
			 document.getElementById('emailErrorText').innerHTML = '';
			}

			if(customerid != '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = '';
			}

		    // -- Company name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			// -- Company contact name
			if(LastName != '')
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group");
			    document.getElementById('LastNameErrorText').innerHTML = '';
			}

if(valid)
{		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"addlead.php",
				       data:{customerid:customerid,FirstName:FirstName,LastName:LastName,Lphone:Lphone,email:email,service:service},
						success: function(data)
						 {
							jQuery("#divContent").html(data);
						 }
					});
				});
}
				//data1 = "<p class='status'> Information was successfully submited for approval.</p>";
				//document.getElementById("divContent").innerHTML = data1;

		}
		else
		{
			// -- CustomerId
			if(customerid == '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group has-error");
				document.getElementById('customeridErrorText').innerHTML = 'Please enter company registration';
			}
			else
			{
				document.getElementById("companyError").setAttribute("class", "form-group");
				document.getElementById('customeridErrorText').innerHTML = '';
		    }

		    // -- Company name
			if(FirstName == '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('FirstNameErrorText').innerHTML = 'Please enter company name';
			}
			else
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}

			// -- Company contact name
			if(LastName == '')
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group has-error");
			    document.getElementById('LastNameErrorText').innerHTML = 'Please enter contact person name';
			}
			else
			{
				document.getElementById("LastNameError").setAttribute("class", "form-group");
			    document.getElementById('LastNameErrorText').innerHTML = '';
			}

			// -- phone
			if(Lphone == '')
			{
				document.getElementById("phoneError").setAttribute("class", "form-group has-error");
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter phone';
			}
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

			// -- email
			if(email == '')
			{	document.getElementById("emailError").setAttribute("class", "form-group has-error");
			 	document.getElementById('emailErrorText').innerHTML = 'Please enter email';
			}
			else
			{
				document.getElementById("emailError").setAttribute("class", "form-group");
				document.getElementById('emailErrorText').innerHTML = '';
			}
		}
			  return false;
  }

function phonenumber(inputtxt)
{
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail)
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

</script>
