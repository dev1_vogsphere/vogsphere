<style>
#wrapper 
		{
			width: 50%;
			position: fixed;
			z-Index:1;
			top:43%;
		}
		#fixed_div 
		{
			/*margin-left: auto;
			margin-right: auto;*/
			position: relative;
			/*width: 100px;
			height: 30px;*/
			text-align: center;
			/*background: lightgreen;
			padding-right: 15px;
			padding-left: 15px;*/
		}
</style>
<?php 
require 'database.php';
$count       = 0;
$subscribe   = '';
$Nationality = '';
$role 		 = 'renter';
$captcha = '';
	if(!function_exists('uploadfile'))
	{
		function uploadfile($name)
		{
			try
			{
			// -- Upload directory
			$path = "rental/uploads/"; 
			$f    = $name;
			// -- Upload a file -------------------------------------------- //
			// -- SIZE
			// -- VALID FORMAT	
			// -- No error found! Move uploaded files 
				$name = date('d-m-Y-H-i-s').$name;
				$ext = pathinfo($name, PATHINFO_EXTENSION);
			// -- Encrypt The filename.
				$name = md5($name).'.'.$ext;
			// -- Update File Name Directory	
				if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
				{
					$count++; // Number of successfully uploaded files
				}				
				// -- BOC Encrypt the uploaded PDF with ID as password.
				$file_parts = pathinfo($name);
				$filenameEncryp = $path.$name;
				switch($file_parts['extension'])
				{
					case "pdf":
					{
						

					break;
					}
					case "PDF":
					{
						break;
					}
				}
			}					
			catch (Exception $e)
			{
				echo $e->getMessage();
			}
		}
	}
	
	if(!function_exists('create_sessions'))
	{
		function create_sessions($dataUser,$dataCustomer)
		{
			$_SESSION['Title'] 		= $dataCustomer['Title'];
			$_SESSION['initials']	= $dataCustomer['initials'];
			$_SESSION['FirstName']  = $dataCustomer['FirstName'];
			$_SESSION['LastName']   = $dataCustomer['LastName'];
			$_SESSION['password']   = $dataUser['password'];
			$_SESSION['idnumber']   = $dataUser['userid'];
			$_SESSION['phone'] 		= $dataCustomer['phone'];
			$_SESSION['email']		= $dataCustomer['email2'] ;
			$_SESSION['Street'] 	= $dataCustomer['Street'];
			$_SESSION['Suburb'] 	= $dataCustomer['Suburb'];
			$_SESSION['City'] 		= $dataCustomer['City'];
			$_SESSION['State'] 		= $dataCustomer['State'];
			$_SESSION['PostCode'] 	= $dataCustomer['PostCode'];
			$_SESSION['Dob'] 		= $dataCustomer['Dob'];
			//$_SESSION['phone'] 			= $dataCustomer['phone'];
			$_SESSION['Customerphone']  = $dataCustomer['phone'];
		}
	}			

if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}	
if(!function_exists('create_user'))
{
	function create_user($dataUser)
	{
		$id  = 0;
		try
		{
		$tbl_user = 'user';
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$password1 = Database::encryptPassword($dataUser['password']);
		$sqlData = sql_associative_array($dataUser);
		$sql = "INSERT INTO $tbl_user
		({$sqlData[0]})
		VALUES ({$sqlData[1]}?)";
		$q   = $pdo->prepare($sql);
		$q->execute($sqlData[2]);
		$id = $pdo->lastInsertId();	
		$pdo->commit();
	    }
		catch (PDOException $e)
		{
			//echo $e->getMessage();
		}
		return $id;	
	}
}	
if(!function_exists('get_customer'))
{
	function get_customer($customerid)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dataCustomer = null;
			$tbl_2  = 'customer';			

			$sql = "select * from $tbl_2 where CustomerId = ?"; 
			$q = $pdo->prepare($sql);
			$q->execute(array($customerid));
			$dataCustomer = $q->fetch(PDO::FETCH_ASSOC);			  
			
			if(!isset($dataCustomer['CustomerId']))
			{
			  $sql = "select * from $tbl_2 where alias = ?"; 
			  $q = $pdo->prepare($sql);
			  $q->execute(array($customerid));
			  $dataCustomer = $q->fetch(PDO::FETCH_ASSOC);			  				
			}
			
		return $dataCustomer;
	}
}
if(!function_exists('create_customer'))
{
	function create_customer($dataCustomer)
	{
		    $id  = 0;
			$tbl_customer = 'customer';
		    $pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			/*$sqlQuestionMarks[] = '(?,?,?,?,?,?,?,?,?,?,?,?,?,?)'; 
			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,Street,Suburb,City,State,PostCode,Dob,phone,email2,alias,initials)";
			$sql = $sql." VALUES ".implode(',', $sqlQuestionMarks);
			$q = $pdo->prepare($sql);
			$q->execute(array($dataCustomer));
			*/
			try
			{
			$sqlData = sql_associative_array($dataCustomer);
			$sql = "INSERT INTO $tbl_customer
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$id = $pdo->lastInsertId();	
			$pdo->commit();
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			return $id;	
	}		
}

if(!function_exists('create_rentalapplication'))
{
	function create_rentalapplication($data)
	{ 
		// -- Connect to the database.
		$id  = 0;
		$data['DateAccpt'] = date('Y-m-d');
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sqlData = sql_associative_array($data);
		
			try
			{
				$sql = "INSERT INTO rentalapp
				({$sqlData[0]})
				VALUES ({$sqlData[1]}?)";
				$q   = $pdo->prepare($sql);
				$q->execute($sqlData[2]);
				$id = $pdo->lastInsertId();	
				$pdo->commit();		
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			return $id;
	}
}
if(!function_exists('applicationFees'))
{
	function applicationFees($applicationFeeText,$values)
	{
		// -- Applicants Fees
		$rentalEFTInfo = $applicationFeeText[0];
		$rentalEFTInfo = str_replace('[amount]',$values['rentalapplicationfee'],$rentalEFTInfo);
		$rentalEFTInfo = str_replace('[email]',$values['rentalemail'],$rentalEFTInfo);
		
		$rentalDebitOrderInfo = $applicationFeeText[1];
		$rentalDebitOrderInfo = str_replace('[amount]',$values['rentalapplicationfee'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[email]',$values['rentalemail'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[debitorderdate]',$values['appfeedebitorderdate'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[AccountNumber]',$values['AccountNumber'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[AccountType]',$values['AccountType'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[Accountholder]',$values['AccountHolder'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[title]',$values['Title'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[name]',$values['FirstName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[surname]',$values['LastName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[ID]',$values['idnumber'],$rentalDebitOrderInfo);

		$rentalDebitOrderInfo = str_replace('[BankName]',$values['BankName'],$rentalDebitOrderInfo);
		$rentalDebitOrderInfo = str_replace('[BranchCode]',$values['BranchCode'],$rentalDebitOrderInfo);

		$applicationFeeText = null;
		$applicationFeeText[] = $rentalEFTInfo;
		$applicationFeeText[] = $rentalDebitOrderInfo;
		
		return $applicationFeeText;
	}
}
if(isset($_GET['subscribe']))
{
	$subscribe = $_GET['subscribe'];
	//$subcribtion = base64_decode(urldecode($subcribtion)); 
}

// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  //$data = $pdo->query($sql);						   						 
  $currency = "";
  
  $sql = 'SELECT * FROM bank';
  $dataBanks = $pdo->query($sql);
  
  $sql = 'SELECT * FROM bankbranch';
  $dataBankBranches = $pdo->query($sql);
  
  $sql = 'SELECT * FROM accounttype';
  $dataaccounttype = $pdo->query($sql);

  // -- BOC Loan Durations (30.06.2017)
  $Globalterms = "";
  $phone = "";
  $initials = '';
  $currency = getsession('currency');
  
//2. ---------------------- BOC - Provinces ------------------------------- //
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 
  $provinceArr = array();
	foreach($dataProvince as $row)
	{
		$provinceArr[] = $row['provinceid'].'|'.$row['provincename'];
	}

  $sql = 'SELECT * FROM rentalproperty';
  $datarentalproperty = $pdo->query($sql);						   						 
  $propertyArr = array();
	foreach($datarentalproperty as $row)
	{
		$propertyArr[] = $row['id'].'|'.$row['rentalpropertyname'].','.$row['rentalpropertytype'].','.$row['rentalpropertyprice'].','.$row['rentalpropertycity'];
	}	
	
$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypecode'].'|'.$row['accounttypedesc'];
}  	
	Database::disconnect();	
// --------------------- EOC 2017.04.15 ------------------------- //

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
$adminemail = "info@vogsphere.co.za";
$MessageErrorMail = "";

// -- Terms and Conditions. --- //
$agreeError = null;
$agree = null;	
	
// -- Terms and conditions. -- //

// -- Company Name:
$companyname = "";
if(isset($_SESSION['GlobalCompany']))
{
	$companyname = $_SESSION['GlobalCompany'];
}
else
{
	$companyname = "Vogsphere (PTY) LTD";
}

$password1 			= '';
$idnumber  			= '';
$tbl_user			= "user"; // Table name 
$tbl_customer 		= "customer"; // Table name
$tbl_loanapp 		= "loanapp"; // Table name
$db_eloan 			= "ecashpdq_eloan";
$captcha 			= '';
$modise 			= '';
$BankName  			= '';
$BranchCode 		= '';
$initialsError 	    = '';
$OtherSpecifyError  = '';
$OtherSpecify 		= '';
// -- Rental Arrays
$countArr = array();
$countArr[] = '0|0';
$countArr[] = '1|1';
$countArr[] = '2|2';
$countArr[] = '3|3';
$countArr[] = '4|4';
$countArr[] = '5|5';
$countArr[] = 'more|More';


$yesnoArr = array();
$yesnoArr[] = 'No|No';
$yesnoArr[] = 'Yes|Yes';


$leasedurarionArr = array();
$leasedurarionArr[] = '0|0';
$leasedurarionArr[] = '1|1';
$leasedurarionArr[] = '2|2';
$leasedurarionArr[] = '3|3';
$leasedurarionArr[] = '4|4';
$leasedurarionArr[] = '5|5';
$leasedurarionArr[] = '6|6';
$leasedurarionArr[] = '7|7';
$leasedurarionArr[] = '8|8';
$leasedurarionArr[] = '9|9';
$leasedurarionArr[] = '10|10';
$leasedurarionArr[] = '11|11';
$leasedurarionArr[] = '12|12';
$leasedurarionArr[] = 'more|More';

// 1. -- Questionaires
$leasedurarion 			= '';
$pets 					= '';
$evictions 	= '';
$vehicles 	= '';
$aboutvacancy = '';
$limitstopay  = '';
$emergencynamenumber = '';
$whyrenttome = '';
$felonies	= '';
$checkingaccount = '';

$aboutvacancyError = '';
$whyrenttomeError  = '';
$emergencynamenumberError  = '';
$petsError 			= '';
$desiredate = '';
$property   = '';

// 2. --  Personal Information Details
$nationality = '';
$gender 	 = '';

// 3. -- Payment Responsibility
$resppname      = '';
$respsurname    = '';
$respsurname    = '';
$respworkplace  = '';
$respoccupation = '';
$respworkaddr   = '';
$respworktel    = '';
$respworkemail  = '';
$respstreet     = '';
$respsuburb     = '';
$respcity       = '';
$respprovince   = '';
$resppostalcode = '';

$resppnameError      = '';
$respsurnameError    = '';
$respworkplaceError  = '';
$respoccupationError = '';
$respworkaddrError   = '';
$respworktelError    = '';
$respworkemailError  = '';
$respstreetError     = '';
$respsuburbError     = '';
$respstreetError	 = '';
$respcityError       = '';
$respprovinceError   = '';
$resppostalcodeError = '';

$DebitOrderChk = 'checked';
$EFTChk        = '';
$optpayments   = '';

$none = 'none';

$rentalapplicationfee = '150.00';
$rentalemail		  = 'info@intelliproperty,com'; 

$rentalEFTInfo
= 'Because you opted for payment option EFT, please email proof of application Fee of R[amount] payment to [email] in order to process your application.
Please use ID number as a reference. Please note Application Fee of R[amount] is non-refundable. see terms & conditions below for more details.';

$rentalDebitOrderInfo
 = 'Because you opted for payment option Debit Order, please authorize below:
I [title] [name] [surname] with ID : [ID], hereby authorize a debit order payment of R[amount] on [debitorderdate]
from Bank :[BankName] Branch:[BranchCode] , Account Number:[AccountNumber] , Account Type :[AccountType] , Account holder :[Accountholder] . 
Due to fraud your Bank will send a you a debicheck confirmation prio to the debit order for 
approval or rejection of the request. Please If you did not authorize this please reject the request.   
Please note Application Fee of R[amount] is non-refundable. Please ensure that there are enough funds in your account.
see terms & conditions below for more details.';   

$rentalEFTInfo = str_replace('[amount]',$rentalapplicationfee,$rentalEFTInfo);
$rentalEFTInfo = str_replace('[email]',$rentalemail,$rentalEFTInfo);

$rentalDebitOrderInfo = str_replace($rentalapplicationfee,'[amount]',$rentalDebitOrderInfo);
$rentalDebitOrderInfo = str_replace($rentalemail,'[email]',$rentalDebitOrderInfo);

$applicationFeeText = array();
$applicationFeeText[] = $rentalEFTInfo;
$applicationFeeText[] = $rentalDebitOrderInfo;

// 4 -- Current Apartment
$currapartmentname = '';
$currcaretaker     = '';
$currcontactnumber  = '';
$currmonthlyrent     = '';	
$currreasnforleaving = '';

$currapartmentnameError = '';
$currcaretakerError 	= '';
$currcontactnumberError = '';
$currmonthlyrentError = '';
$currreasnforleavingError = '';

// 5. -- Previous Apartment
$prevapartmentname = '';
$prevcaretaker     = '';
$prevcontactnumber  = '';
$prevmonthlyrent     = '';	
$prevreasnforleaving = '';


$prevapartmentnameError  = '';
$prevcontactnumberError  = '';
$prevcaretakerError      = '';
$prevmonthlyrentError     = '';
$prevreasnforleavingError = '';

// 6. -- Referrals.
$Friend = '';
$Online = '';
$Flier = '';
$Other  = '';
$Other  = '';
$FriendChk = 'checked';
$OnlineChk = '';
$FlierChk = '';
$OtherChk = '';

$friendfullname = '';
$friendfullnameError = '';

$friendcontactnumber = '';
$friendcontactnumberError = '';

$friendemail = '';
$friendemailError = '';

if(isset($_SESSION))
{
		$Globalterms = $_SESSION['intelliterms'];
}
$BankName = '';
$DobError = ''; 

// upload required dc.
$copyid 	 = '';
$payslip	 = '';
$copyidFile  = '';
$payslipFile = '';

$payslipError = '';
$copyidError  = '';
$applicationfeeTextValue = $rentalDebitOrderInfo;

// -- Check.
$agreeChk 		  = '';
$appfeesacceptChk = '';

$AccountType  = '';
$AccountNumber  ='';
$AccountHolder  = '';
$appfeedebitorderdate  = '';

////////////////////////////////////////////////////////////////////////
//								POST								  //	 
////////////////////////////////////////////////////////////////////////
 if (!empty($_POST)) 
 {
	$valid = true;
	$Dob   = getpost('Dob');
	$Age18 = Age18($Dob);
	$DobError = null;
	if(empty($Age18)){$valid= true;}else{$DobError = Age18($Dob); $valid = false;}
	
	$values = array();
	$values['rentalemail'] = $rentalemail ;
	$values['rentalapplicationfee'] = $rentalapplicationfee ;
	$applicationfeeTextValue = getpost('applicationfeeTextValue');

//echo $applicationfeeTextValue;
	$none = 'none';
	// -- 1. Questionaires 
	$leasedurarion 			= getpost('leasedurarion');
	$pets 					= getpost('pets');
	$evictions 				= getpost('evictions');
	$vehicles 				= getpost('vehicles');
	$aboutvacancy 			= getpost('aboutvacancy');
	$limitstopay  			= getpost('limitstopay');
	$emergencynamenumber	= getpost('emergencynamenumber');
	$whyrenttome 			= getpost('whyrenttome');
	$felonies				= getpost('felonies');
	$checkingaccount 		= getpost('checkingaccount');
	$desiredate 			= getpost('desiredate');
	$brokenlease 			= getpost('brokenlease');
	$smoke 					= getpost('smoke');
	$status 				= 'PEN';
	
	$property   			= getpost('property');
	$BankName 				= getpost('BankName');
	$BranchCode				= getpost('BranchCode');
	$AccountNumber          = getpost('AccountNumber');
	$AccountHolder			= getpost('AccountHolder');
	$AccountType			= getpost('AccountType');

	$values['BankName'] 	 = $BankName ;
	$values['BranchCode'] 	 = $BranchCode ;
	$values['AccountNumber'] = $AccountNumber ;
	$values['AccountHolder'] = $AccountHolder ;

	// -- Determine Account Description.
	foreach($Account_TypeArr as $row)
	{
	  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
	  if($rowData[0] == $AccountType)
	  { $values['AccountType'] = $rowData[1];}
	}
	
	if(!isset($values['AccountType'] ))
	{
		$values['AccountType']  = '';
	}

	$BankNameError 			= '';
	
	// -- clear:
	$aboutvacancyError        = '';
	$whyrenttomeError         = '';
	$emergencynamenumberError = '';
	$petsError                = '';

	if(empty($aboutvacancy))
	{$aboutvacancyError = 'required';$valid = false;}

	if(empty($whyrenttome))
	{$whyrenttomeError  = 'required';$valid = false;}

	if(empty($emergencynamenumber))
	{$emergencynamenumberError  = 'required';$valid = false;}

	if(empty($pets))
	{$petsError = 'required';$valid = false;}


// 3. -- Payment Responsibility
$resppname      = getpost('resppname');
$respsurname    = getpost('respsurname');
$respworkplace  = getpost('respworkplace');
$respoccupation = getpost('respoccupation');
$respworkaddr   = getpost('respworkaddr');
$respworktel    = getpost('respworktel');
$respworkemail  = getpost('respworkemail');
$respstreet     = getpost('respstreet');
$respsuburb     = getpost('respsuburb');
$respcity       = getpost('respcity');

$respprovince   = getpost('respprovince');
$resppostalcode = getpost('resppostalcode');

$resppnameError      = '';
$respsurnameError    = '';
$respworkplaceError  = '';
$respoccupationError = '';
$respworkaddrError   = '';
$respworktelError    = '';
$respworkemailError  = '';
$respstreetError     = '';
$respsuburbError     = '';
$respcityError       = '';
$respprovinceError   = '';
$resppostalcodeError = '';

if(empty($resppname)){$resppnameError      = 'required';$valid = false;}
if(empty($respsurname)){$respsurnameError      = 'required';$valid = false;}
if(empty($respworkplace)){$respworkplaceError  = 'required';$valid = false;}
if(empty($respoccupation)){$respoccupationError = 'required';$valid = false;}
if(empty($respworkaddr)){$respworkaddrError   = 'required';$valid = false;}
if(empty($respworktel)){$respworktelError    = 'required';$valid = false;}
if(empty($respworkemail)){$respworkemailError  = 'required';$valid = false;}
if(empty($respstreet)){$respstreetError     = 'required';$valid = false;}
if(empty($respsuburb)){$respsuburbError     = 'required';$valid = false;}
if(empty($respcity)){$respcityError       = 'required';$valid = false;}
if(empty($respprovince)){$respprovinceError   = 'required';$valid = false;}
if(empty($resppostalcode)){$resppostalcodeError = 'required';$valid = false;}

// -- Banking Details.
$DebitOrderChk = '';
$EFTChk        = '';
$optpayments   = '';
$appfeedebitorderdate = getpost('appfeedebitorderdate');
$optpayments    = getpost('optpayments');

if($optpayments == 'DebitOrder'){$DebitOrderChk = 'checked';}
if($optpayments == 'EFT'){$EFTChk = 'checked';}
$appfeedebitorderdateError = '';

if(!empty($DebitOrderChk))
{
	if(empty($appfeedebitorderdate)){$appfeedebitorderdateError = 'required';$valid = false;}
}
$values['appfeedebitorderdate'] = $appfeedebitorderdate;


// 4 -- Current Apartment
$currapartmentname 		= getpost('currapartmentname');
$currcaretaker     		= getpost('currcaretaker');
$currcontactnumber  	= getpost('currcontactnumber');
$currmonthlyrent     	= getpost('currmonthlyrent');	
$currreasnforleaving	= getpost('currreasnforleaving');

// -- Clear:
$currapartmentnameError = '';
$currcaretakerError 	= '';
$currcontactnumberError = '';
$currmonthlyrentError = '';
$currreasnforleavingError = '';

if(!empty($currapartmentname))
{
	if(empty($currcaretaker)){$currcaretakerError 	= 'required';$valid = false;}
	if(empty($currcontactnumber)){$currcontactnumberError = 'required';$valid = false;}
	if(empty($currmonthlyrent)){$currmonthlyrentError = 'required';$valid = false;}
	if(empty($currreasnforleaving)){$currreasnforleavingError = 'required';$valid = false;}
}

// 5. -- Previous Apartment
$prevapartmentname 		= getpost('prevapartmentname');
$prevcaretaker     		= getpost('prevcaretaker');
$prevcontactnumber  	= getpost('prevcontactnumber');
$prevmonthlyrent     	= getpost('prevmonthlyrent');	
$prevreasnforleaving 	= getpost('prevreasnforleaving');

// -- clear:
$prevapartmentnameError  = '';
$prevcontactnumberError  = '';
$prevcaretakerError      = '';
$prevmonthlyrentError     = '';
$prevreasnforleavingError = '';

if(!empty($prevapartmentname))
{
	if(empty($prevapartmentname)){$prevapartmentnameError     = 'required';$valid = false;}
	if(empty($prevcontactnumber)){$prevcontactnumberError     = 'required';$valid = false;}
	if(empty($prevcaretaker)){$prevcaretakerError      	      = 'required';$valid = false;}
	if(empty($prevmonthlyrent)){$prevmonthlyrentError         = 'required';$valid = false;}
	if(empty($prevreasnforleaving)){$prevreasnforleavingError = 'required';$valid = false;}
}
// -- Referrals
$friendfullname 	 = getpost('friendfullname');
$friendcontactnumber = getpost('friendcontactnumber');
$friendemail 		 = getpost('friendemail');
$OtherSpecify 		 = getpost('OtherSpecify');

// -- clear
$FriendChk = '';
$OnlineChk = '';
$FlierChk  = '';
$OtherChk = '';
$optradio = getpost('optradio');
$optpayments = getpost('optpayments');

$OtherSpecifyError = '';
$friendemailError = '';
$friendfullnameError = '';
$friendcontactnumberError = '';

if($optradio == 'Friend'){$FriendChk = 'checked';}
if($optradio == 'Online'){$OnlineChk = 'checked';}
if($optradio == 'Flier'){$FlierChk = 'checked';}
if($optradio == 'Other'){$OtherChk = 'checked';}

if(!empty($FriendChk))
{
	if(empty($friendfullname)){$friendfullnameError = 'required';$valid = false;}
	if(empty($friendcontactnumber)){$friendcontactnumberError = 'required';$valid = false;}
	if(empty($friendemail)){$friendemailError = 'required';$valid = false;}
}
if(!empty($OtherChk))
{
	if(empty($OtherSpecify)){$OtherSpecifyError = 'required';$valid = false;}
}
	$appfeesacceptError = '';
	$appfeesaccept = getpost('appfeesaccept');
	
	$appfeesacceptChk = '';
	if(empty($appfeesaccept)) 
	{
		$appfeesacceptError = 'required';$valid = false;
	}else{$appfeesacceptChk = 'checked';}
	
	 $appfeedebitorderdate = getpost('appfeedebitorderdate');
	 $appfeedebitorderdateError = '';
	 if($optpayments == 'DebitOrder')
	 {
		if(empty($appfeedebitorderdate))
		{
			$appfeedebitorderdateError = 'required';$valid = false;
		}
	  }
	  
// upload required dc.
$payslipError = '';
$copyidError  = '';

$copyid 	 = getpost('copyid');
$payslip	 = getpost('payslip');
$copyidFile  = getpost('copyidFile');
$payslipFile  = getpost('payslipFile');

if(!empty($copyid)){$copyidFile = $copyid;}
if(!empty($payslip)){$payslipFile = $payslip;}

if(empty($copyidFile)){ $copyidError  = 'required';}
if(empty($payslipFile )){ $payslipError  = 'required';}
	  
	// Captcha declaration
	$modise = getsession('captcha');
	$captchaError = null;
	$Nationality  = getpost('Nationality');
	// --Captcha
	$captcha = getsession('captcha');
	
    // Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$SuretyError = null;
		$firstpaymentError = null;
		$password1Error = null;
		$password2Error = null;

				
		// keep track validation errors - 		// Loan Details
		$MonthlyExpenditureError = null; 
		$ReqLoadValueError = null; 
		$DateAccptError = null; 
		$LoanDurationError = null; 
		$StaffIdError = null; 
		$InterestRateError = null; 
		$ExecApprovalError = null; 
		$MonthlyIncomeError = null; 

		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //
		// keep track post values
		// Customer Details
		$Title 			= $_POST['Title'];
		$idnumber 		= $_POST['idnumber'];
		$FirstName 		= $_POST['FirstName'];
		$LastName 		= $_POST['LastName'];
		$phone 			= $_POST['phone'];
		$email 			= $_POST['email'];
		$Street 		= $_POST['Street'];
		$Suburb 		= $_POST['Suburb'];
		$City 			= $_POST['City'];
		$State 			= $_POST['State'];
		$PostCode 		= $_POST['PostCode'];
		$Dob			= $_POST['Dob'];
		$StaffId 		= 1;
		$initials   	= $_POST['initials'];

		$values['Title'] = $Title ;
		$values['idnumber'] = $idnumber;
		$values['FirstName'] = $FirstName ;
		$values['LastName'] = $LastName ;

		// -- Terms and Conditions.
		$agreeError = '';
		$agree = "";
		$agreeChk = '';
		if(!empty($_POST['agree'])) 
        {
          		$agree = $_POST['agree'];
				$agreeChk = 'checked';
        }
		if (empty($agree)) { $agreeError = 'You must agree with the terms and conditions'; $valid = false;}
		// -- Terms and conditions.
		
		// Loan Details
		$DateAccpt  = date('Y-m-d');
		$ExecApproval  = "PEN";
		
		$TotalAssets = 0;
		$LoanValue = 0;
		
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);		
		// -------------------------------- Banking Details ------------------------------------------ //
		// validate input - 		// Customer Details
			
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		if (empty($initials)) { $initialsError = 'Please enter Initials'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($idnumber,2,2) > 12) 
		{ 
			$idnumberError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{
		// Day from 1 - 30
			if (in_array(substr($idnumber,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($idnumber,4,2) > 30)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($idnumber,2,2),$months31)) 

			{
				// Day 
			   if (substr($idnumber,4,2) > 31)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($idnumber,2,2),$monthsFeb)) 
			{
				// Leap Year parseInt
				$yearValidate = intval(substr($idnumber,0,2));
				$leapyear = ($yearValidate % 4 == 0);
				
				if($leapyear == true)
				{
					// Day 
			   if (substr($idnumber,4,2) > 29)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($idnumber,4,2) > 28)
					   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}
		
		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
		
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		// Phone number must be characters
		if (!is_Numeric($phone)) { $phoneError = 'Please enter valid phone numbers - digits only'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}
		
		// is Number
		if (!is_Numeric($PostCode)) { $PostCodeError = 'Please enter valid Postal Code - numbers only'; $valid = false;}

		// Is 4 numbers
		if (strlen($PostCode) > 4) { $PostCodeError = 'Please enter valid Postal Code - 4 numbers'; $valid = false;}
		
		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter/select Date of birth'; $valid = false;}
								
		// -- Enable and Disable a Captcha -- //
		// -- 06.05.2018 - Disable Captcha -- //
		if(isset($_SESSION['Globalcaptcha']))
		{
			$Globalcaptcha = $_SESSION['Globalcaptcha'];
			if(empty($Globalcaptcha))
			{
				// Captcha declaration
				$modise = $_SESSION['captcha'];
				$captchaError = null;
				// --Captcha
				$captcha = getsession('captcha');
				
			// Captcha Error Validation.
			if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
			if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}
			}
		}
		// -- Enable and Disable Captcha -- //
		// Captcha Error Validation.
		// if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		// if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}
		// -- Validate Password Only if not loggedin
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
			 // nothing
			}
			else
			{
			
			$password1 = $_POST['password1'];
			//$password2 = $_POST['password2'];
			
			if (empty($password1)) { $password1Error = 'Please enter Password'; $valid = false;}			
			}
			
		// 	refresh Image
			if (!isset($_POST['Apply'])) // If refresh button is pressed just to generate Image. 
			{
				
				$valid = false;
			}
////////////////////////////////////////////////////////////////////////
//						POST TO DATABASE					  		  //	 
////////////////////////////////////////////////////////////////////////		
		$applicationFeeText = applicationFees($applicationFeeText,$values);
		if( $optpayments == "DebitOrder")
		{
			$applicationfeeTextValue  = $applicationFeeText[1];
		}
		else
		{
			$applicationfeeTextValue  = $applicationFeeText[0];
		}

		// Insert e-Loan Application Data.
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// ---------------- User ---------------
			//User - UserID & E-mail Duplicate Validation
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));

			if ($q->rowCount() >= 1)
			{$idnumberError = 'ID number already exists.'; $valid = false;}
			
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
			
			if ($valid) 
			{
////////////////////////////////////////////////////////////////////////
//							POST ARRAYS DATA                          //
////////////////////////////////////////////////////////////////////////
		$data = array();
		$data['CustomerId'] = $idnumber;
		$data['leasedurarion'] = $leasedurarion;
		$data['pets'] = $pets;
		$data['evictions'] = $evictions;
		$data['vehicles'] = $vehicles;
		$data['brokenlease'] = $brokenlease;
		$data['aboutvacancy'] = $aboutvacancy;
		$data['checkingaccount'] = $checkingaccount;
		$data['felonies'] = $felonies;
		$data['limitstopay'] = $limitstopay;
		$data['emergencynamenumber'] = $emergencynamenumber;
		$data['smoke'] = $smoke;
		$data['whyrenttome'] = $whyrenttome;
		$data['property'] = $property;
		$data['desiredate'] = $desiredate;
		$data['Title'] = $Title;
		$data['initials'] = $initials;
		$data['FirstName'] = $FirstName;
		$data['LastName'] = $LastName;
		$data['Dob'] = $Dob;
		$data['nationality'] = $nationality;
		$data['gender'] = $gender;
		$data['phone'] = $phone;
		$data['email'] = $email;
		$data['Street'] = $Street;
		$data['Suburb'] = $Suburb;
		$data['City'] = $City;
		$data['province'] = $province;
		$data['PostCode'] = $PostCode;
		$data['resppname'] = $resppname;
		$data['respsurname'] = $respsurname;
		$data['respworkplace'] = $respworkplace;
		$data['respoccupation'] = $respoccupation;
		$data['respworkaddr'] = $respworkaddr;
		$data['respworktel'] = $respworktel;
		$data['respworkemail'] = $respworkemail;
		$data['respstreet'] = $respstreet;
		$data['respsuburb'] = $respsuburb;
		$data['respcity'] = $respcity;
		$data['respprovince'] = $respprovince;
		$data['resppostalcode'] = $resppostalcode;
		$data['AccountHolder'] = $AccountHolder;
		$data['BankName'] = $BankName;
		$data['AccountNumber'] = $AccountNumber;
		$data['AccountType'] = $AccountType;
		$data['BranchCode'] = $BranchCode;
		$data['optpayments'] = $optpayments;
		$data['applicationfeeTextValue'] = $applicationfeeTextValue;
		$data['appfeesaccept'] = $appfeesaccept;
		$data['appfeedebitorderdate'] = $appfeedebitorderdate;
		$data['copyidFile'] = $copyidFile;
		$data['payslipFile'] = $payslipFile;
		$data['currapartmentname'] = $currapartmentname;
		$data['currcontactnumber'] = $currcontactnumber;
		$data['currcaretaker'] = $currcaretaker;
		$data['currmonthlyrent'] = $currmonthlyrent;
		$data['currreasnforleaving'] = $currreasnforleaving;
		$data['optreferrals'] = $optradio;
		$data['OtherSpecify'] = $OtherSpecify;
		$data['friendfullname'] = $friendfullname;
		$data['friendcontactnumber'] = $friendcontactnumber;
		$data['friendemail'] = $friendemail;
		$data['agree'] = $agree;
		$data['UserCode'] = 'UFIMS';
		$data['IntUserCode'] = 'UFIMS';
		$data['ExecApproval'] = $status;
		$rentalapplicationid = create_rentalapplication($data);
////////////////////////////////////////////////////////////////////////				
	    $count = 0;
		 if(!empty($rentalapplicationid))
		 {	
				$dataUser = array();
				$dataUser['userid'] = $idnumber;
				$dataUser['password'] = $password1;
				$dataUser['email'] = $email;		
				$dataUser['role'] = $role;
				$dataUser['alias'] = $email;
				//create_user($dataUser);
			
				$dataCustomer   = array();
				$dataCustomer['CustomerId'] = $idnumber;
				$dataCustomer['Title'] = $Title;
				$dataCustomer['FirstName'] = $FirstName;
				$dataCustomer['LastName'] = $LastName;
				$dataCustomer['Street'] = $Street;
				$dataCustomer['Suburb'] = $Suburb;
				$dataCustomer['City'] = $City;
				$dataCustomer['State'] = $State;
				$dataCustomer['PostCode'] = $PostCode;
				$dataCustomer['Dob'] = $Dob;
				$dataCustomer['phone'] = $phone;
				$dataCustomer['email2'] = $email;
				$dataCustomer['alias'] = $email;
				$dataCustomer['initials'] = $initials;
				
				//create_customer($dataCustomer);
				//create_sessions($dataUser,$dataCustomer);
				$count = $count + 1;
				Database::disconnect();
				$clientname = $Title.' '.$FirstName.' '.$LastName ;
				$suretyname = $clientname;
				$name		= $suretyname;
			}		
		  }
		  else
		  {
			  $count = -1;
		  } 
		}
		else
		{
			  $count = -1;
		}
} // END of isEmpty('Post')
else
{
// -- Default Bankname - ABSA
  $BankName = "ABSA";
  $dataCustomer = array();
  
  $dataCustomer = get_customer(getsession('username'));
  $idnumber = $dataCustomer['CustomerId'];
  $Title    = $dataCustomer['Title'] ;
  $FirstName = $dataCustomer['FirstName'];
  $LastName = $dataCustomer['LastName'];
  $Street = $dataCustomer['Street'];
  $Suburb = $dataCustomer['Suburb'];
  $City = $dataCustomer['City'];
  $State = $dataCustomer['State'];
  $PostCode = $dataCustomer['PostCode'];
  $Dob = $dataCustomer['Dob'];
  $phone = $dataCustomer['phone'];
  $email = $dataCustomer['email2'];
  $email = $dataCustomer['alias'];
  $initials = $dataCustomer['initials'];
  
  $values = array();
  $values['appfeedebitorderdate'] = $appfeedebitorderdate;

		$values['Title'] = $Title ;
		$values['idnumber'] = $idnumber;
		$values['FirstName'] = $FirstName ;
		$values['LastName'] = $LastName ;
		$values['rentalemail'] = $rentalemail ;
		$values['rentalapplicationfee'] = $rentalapplicationfee ;

	$values['BankName'] 	 = $BankName ;
	$values['BranchCode'] 	 = $BranchCode ;
	$values['AccountNumber'] = $AccountNumber ;
	$values['AccountHolder'] = $AccountHolder ;

	$values['AccountType'] = $AccountType ;
  		$applicationFeeText = applicationFees($applicationFeeText,$values);
}

?>
<div class="container background-white bottom-border">
                    <div class="margin-vert-30">
                        <!-- Register Box id="captcha-form" name="captcha-form" -->
                        <div class="col-md-3">
						<div class="panel panel-default" style="border-color: #ddd;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">What is required?</h3>
                                </div>
                                <div class="panel-body">
									<ul class="posts-list tick animate fadeInRight animated">
									  <li>Email</i>
									  <li>A cellphone number</li>
									  <li>Copy SA ID number</li>
									  <li>Bank account details</li>
									  <li>Payslip</li>
									 </ul>		
                                </div>
                            </div>	
						<div class="panel panel-default" style="border-color: #ddd;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">How to apply?</h3>
                                </div>
                                <div class="panel-body">
                                    <ul class="posts-list margin-top-10">
                                        <li>
                                            <div class="recent-post">
                                                <a href="">
                                                    <img class="pull-left" src="rental/plus.png" alt="thumb1">
                                                </a>
                                                <a href="#" class="posts-list-title">Click to open</a>
                                                <br>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
										<li>
                                            <div class="recent-post">
                                                <a href="">
                                                    <img class="pull-left" src="rental/minus.png" alt="thumb1">
                                                </a>
                                                <a href="#" class="posts-list-title">Click to close</a>
                                                <br>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
										<li>
                                            <div class="recent-post">
                                                <a href="">
                                                    <img class="pull-left" src="rental/noerror.png" alt="thumb1">
                                                </a>
                                                <a href="#" class="posts-list-title">For example, complete or complete all required values in Questionaires</a>
                                                <br>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
										<li>
                                            <div class="recent-post">
                                                <a href="">
                                                    <img class="pull-left" src="rental/apply.png" alt="thumb1">
                                                </a>
												<br/>
                                                <a href="#" class="posts-list-title">Submits your rental application for verfication</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
										<li>
                                            <div class="recent-post">
                                                <a href="">
                                                    <img class="pull-left" src="rental/error.png" alt="thumb1">
                                                </a>
                                                <a href="#" class="posts-list-title">For example, you did not complete all required values in Questionaires</a>
                                                <br>
                                            </div>
                                        </li>
									</ul></div></div>
							</div>
                            <!--<div class="row margin-bottom-30">
													<h2 class="margin-bottom-10">About Us</h2>

                                <div class="col-md-3 animate fadeInLeft">
                                    <p>Commodo id natoque malesuada sollicitudin elit suscipit. Curae suspendisse mauris posuere accumsan massa posuere lacus convallis tellus interdum. Amet nullam fringilla nibh nulla convallis ut venenatis purus lobortis.</p>
                                    <p>Lorem Ipsum is simply dummy text of Lorem the printing and typesettings. Aliquam dictum nulla eu varius porta. Maecenas congue dui id posuere fermentum. Sed fringilla sem sed massa ullamcorper, vitae rutrum justo sodales.
                                        Cras sed iaculis enim. Sed aliquet viverra nisl a tristique. Curabitur vitae mauris sem.</p>
                                </div>-->
						<div class="col-md-8">
                              <form METHOD="post"  id="captcha-form" name="captcha-form" class="signup-page"  action=<?php echo "".$_SERVER['REQUEST_URI']."";?>> 
                                <div class="signup-header">
                                    <h2 class="text-center">Rental Application <?php echo date('Y'); ?></h2>
									<!-- <p>
									Please return this application to info@patniniproperties.co.za  
									Application Fee of  R150  and the property you are applying for _________________________________________________ Desire Move in Date__________________________________ and Copy of ID and Payslip of Person who will be Responsible for Payment
									</p> -->
									<p><i><b>Important Note to Applicants:</b> Please fill this application out in full. 
									Incomplete applications will be sent back to you to complete, 
									causing a delay in the process and decreasing your chances of renting from us.</i></p>	
									<p>Please complete all fields NOTE: <span style="color:red">*</span> = Required Field</p>
                                    <p>Already have an account? Click
                                        <a href="customerLogin"><span style="color:red">HERE</span></a> to login to your account.</p>
										<?php # error messages
									if (isset($message)) 	
									{
										foreach ($message as $msg) {
											printf("<p class='status'>%s</p></ br>\n", $msg);
										}
									}
									# success message
									if($count !=0)
									{
										# error message
										if($count == -1)
										{
											printf("<div class=alert alert-warning'>
												<p class='alert alert-warning'>Recitify Rental Application error(s) Below!</p></div>\n", $count);
										}else
									   # success message
										{printf("<p class='status'>Rental Application captured successfully!</p>\n".$MessageErrorMail, $count);}
									}									   
									?>
                                </div>
					<!-- BOC - Personal Information Details -->		
						<!-- Start - Accordion - Alternative -->
<div id="accordion" class="panel-group">						
					 <!-- <div class="tab-pane" id="sample-3b">	-->					
						<div class="panel <?php if(!empty($aboutvacancyError))
						{ echo 'panel-danger';}else if(!empty($limitstopayError)){ echo 'panel-danger';}
						else if(!empty($emergencynamenumberError)){ echo 'panel-danger';}
						else if(!empty($whyrenttomeError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayQuestionares" href="javascript:toggleQuestionares();" > <!-- href="#toggleQuestionares" data-parent="#accordion" data-toggle="collapse" >;" --> 
                                                Questionaires
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleQuestionares" style="display: <?php 
																		  /*if(!empty($aboutvacancyError)){ echo 'block';}
																		  else if(!empty($limitstopayError)){ echo 'block';}
																		  else if(!empty($emergencynamenumberError)){ echo 'block';}
																		  else if(!empty($whyrenttomeError)){ echo 'block';}																		  
																		  else{ echo 'none';}*/?>none">						
								 <p>Please answer all the questions truthfully</p>
							<div class="row">	
								<!-- Contact number -->
							 <div class="col-md-6">	
								<div class="control-group">
									<label class="control-label">How long will you live here?<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="leasedurarion" class="form-control" name="leasedurarion" size="1">
										<?php
										  $leasedurarionSelect = $leasedurarion;
										  foreach($leasedurarionArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] != '0')
											  {
												if($rowData[0] == $leasedurarionSelect)
												{echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
												else
												{echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
											 }
										  }
										?>
									</SELECT>
									</div>
								</div>
							 </div>	
							  <div class="col-md-6">
								<div class="control-group <?php echo !empty($petsError)?'error':'';?>">
									<label class="control-label">What pets do you have?</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="pets" type="text"  placeholder="for e.g. dog,cat" value="<?php echo !empty($pets )?$pets :'';?>">
									</div>
								</div>
							 </div>	
							 </div>	
							 <div class="row">	
								<!-- Contact number -->
							 <div class="col-md-6">	
								<div class="control-group>">
									<label class="control-label">How many evictions have been filled upon you?<span style="color:red">*</span></label>
									<div class="controls">
								    <SELECT  id="evictions" class="form-control" name="evictions" size="1">
										<?php
										  $evictionsSelect = $evictions;
										  foreach($countArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $evictionsSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div>
							 </div>
							 <div class="col-md-6">								 
								<div class="control-group">
									<label class="control-label">How many vehicles do you have?(Parking space)<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="vehicles" class="form-control" name="vehicles" size="1">
										<?php
										  $vehiclesSelect = $vehicles;
										  foreach($countArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $vehiclesSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div>
							 </div>	
							</div>	
							<div class="row">	
								<!-- Contact number -->
							 <div class="col-md-6">
								<div class="control-group">
									<label class="control-label">Have you ever broken a lease agreement?<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="brokenlease" class="form-control" name="brokenlease" size="1">
										<?php
										  $brokenleaseSelect = $brokenlease;
										  foreach($yesnoArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $brokenleaseSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div></div>
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($aboutvacancyError)?'error':'';?>">
									<label class="control-label">How did you find out about this vacancy?<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="aboutvacancy" name="aboutvacancy" type="text"  placeholder="for e.g. website : www.xx.com, Facebook, Friend: Name" value="<?php echo !empty($aboutvacancy)?$aboutvacancy:'';?>">
										<?php if (!empty($aboutvacancyError)): ?>
										<span class="help-inline"><?php echo $aboutvacancyError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>	
							</div>
							
							<div class="row">	
							 <div class="col-md-6">
								<div class="control-group">
									<label class="control-label">Do you have a checking account?<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="checkingaccount" class="form-control" name="checkingaccount" size="1">
										<?php
										  $checkingaccountSelect = $checkingaccount;
										  foreach($yesnoArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $checkingaccountSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div></div>
							<div class="col-md-6">	
								<div class="control-group">
									<label class="control-label"> How many felonies do you have?<span style="color:red">*</span></label>
									<div class="controls">
								    <SELECT  id="felonies" class="form-control" name="felonies" size="1">
										<?php
										  $feloniesSelect = $felonies;
										  foreach($countArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $feloniesSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									
									</div>
								</div></div>
							</div>
							
							<div class="row">	
							 <div class="col-md-6">
								<div class="control-group <?php echo !empty($limitstopayError)?'error':'';?>">
									<label class="control-label">What would limit your ability to pay rent?<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="limitstopay" type="text"  placeholder="for e.g. Nothing" value="<?php echo !empty($limitstopay)?$limitstopay:'';?>">
										<?php if (!empty($limitstopayError)): ?>
										<span class="help-inline"><?php echo $limitstopayError;?></span>
										<?php endif; ?>
									</div>
								</div></div>	
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($emergencynamenumberError)?'error':'';?>">
									<label class="control-label">Emergency contact name & number<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="emergencynamenumber" name="emergencynamenumber" type="text"  placeholder="for e.g. John Doe - 071 123 1234" value="<?php echo !empty($emergencynamenumber)?$emergencynamenumber:'';?>">
										<?php if (!empty($emergencynamenumberError)): ?>
										<span class="help-inline"><?php echo $emergencynamenumberError;?></span>
										<?php endif; ?>
									</div>
								</div></div>	
							</div>	
							<div class="row">	
							 <div class="col-md-6">	
								<div class="control-group">
									<label class="control-label">Do you smoke?<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="smoke" class="form-control" name="smoke" size="1">
										<?php
										  $smokeSelect = $smoke;
										  foreach($yesnoArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $smokeSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div></div>
								<div class="col-md-6">	
								<div class="control-group <?php echo !empty($whyrenttomeError)?'error':'';?>">
									<label class="control-label">Why should we rent to you?<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="whyrenttome" name="whyrenttome" type="text"  placeholder="for e.g. Excellent tenant" value="<?php echo !empty($whyrenttome)?$whyrenttome:'';?>">
										<?php if (!empty($whyrenttomeError)): ?>
										<span class="help-inline"><?php echo $whyrenttomeError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
							</div>
							
							<div class="row">	
							 <div class="col-md-6">
								<div class="control-group">
									<label class="control-label">Property you are applying for?<span style="color:red">*</span></label>
									<div class="controls">
									<SELECT  id="property" class="form-control" name="property" size="1">
										<?php
										  $propertySelect = $property;
										  foreach($propertyArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $propertySelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
										?>
									</SELECT>
									</div>
								</div></div> 	
							<!--  -->
							<div class="col-md-6">
								<div class="controls"><label class="control-label">Desire Move in Date?<span style="color:red">*</span></label>
									<input style="height:30px" class="form-control margin-bottom-10" name="desiredate" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($desiredate)?$desiredate:date('Y-m-d');?>">
								</div>
							</div>
								
						</div>	
					</div>	
			</div><!--</div>-->				
				<div class="tab-pane fade in" id="personal">							
								<div class="panel <?php
if(!empty($idnumberError)){ echo 'panel-danger';}else if(!empty($FirstNameError)){ echo 'panel-danger';} 
else if(!empty($LastNameError)){ echo 'panel-danger';} else if(!empty($DobError)){ echo 'panel-danger';}
else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle collapsed" id="displayRegister" href="javascript:toggleRegister();">
                                                Personal Information Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleRegister" 
							style="display:<?php /*if(!empty($idnumberError)){ echo ':block';}else if(!empty($FirstNameError)){ echo ':block';} 
else if(!empty($LastNameError)){ echo ':block';} else if(!empty($DobError)){ echo ':block';}else{ echo ':none';}*/?>none">						
					 
					 <div class="row">	
								<!-- Contact number -->
							 <div class="col-md-6">	
                                <label>Title</label>
								<SELECT  id="Title" class="form-control" name="Title" size="1" readonly >
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
								</SELECT>
								<br/>
					        </div>
							<div class="col-md-6">
							  <div class="control-group <?php echo !empty($initialsError)?'error':'';?>">
									<label>Initials<span style="color:red">*</span></label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" name="initials" type="text"  placeholder="initials" value="<?php echo !empty($initials)?$initials:'';?>">
										<?php if (!empty($initialsError)): ?>
										<span class="help-inline"><?php echo $initialsError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>	
								<!-- End Initials -->
					</div>			
                                <!-- <input class="form-control margin-bottom-20" type="text"> -->

						 <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">
								<!-- End ID number -->
								<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
									<label>Name<span style="color:red">*</span></label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="Name" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										<?php if (!empty($FirstNameError)): ?>
										<span class="help-inline"><?php echo $FirstNameError;?></span>
										<?php endif; ?>
									</div>
								</div>
								</div>
								<div class="col-md-6">
								<!-- Last Name -->
								<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
									 <label>Surname
										 <span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="Surname" value="<?php echo !empty($LastName)?$LastName:'';?>">
									<?php if (!empty($LastNameError)): ?>
										<span class="help-inline"><?php echo $LastNameError; ?></span>
									<?php endif; ?>	
									</div>
								</div></div>
						  </div>	
						  
						  <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">	
								<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <label >ID/Passport Number
											<span style="color:red">*</span>									
									 </label> 
									<div class="controls">
										<input readonly style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="number"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
							</div>	
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
								<label>Date of birth<span style="color:red">*</span></label>
									<div class="controls">
									<input  readonly style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date"  placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
									</div>
								</div></div>
							</div>
						  <div class="row">	
								<!-- Contact number -->
							 <div class="col-md-6">							
								<label>Nationality</label>
                                <SELECT readonly class="form-control" id="nationality" name="nationality" size="1">
									<option value="African">African</option>
									<option value="White">White</option>
									<option value="Indian">Indian</option>
									<option value="Coloured">Coloured</option>
									<option value="Other">Other</option>
								</select></div>
								<div class="col-md-6">	
								<label>Sex</label>
                                <SELECT  readonly class="form-control" id="gender" name="gender" size="1">
									<option value="Male">Male</option>
									<option value="Female">Female</option>
									<option value="other">Other</option>
								</select>
								</div>
							</div>
						</div> 
					</div>	
						<!-- EOC Personal Information Details -->			
						<!-- BOC Contact Details -->
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($phoneError)){ echo 'panel-danger';}else if(!empty($emailError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayContacts" href="javascript:toggleContacts();">
                                                Contact Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleContacts" style="display: <?php /*if(!empty($phoneError)){ echo 'block';}
																		  else if(!empty($emailError)){ echo 'block';}
																		  else{ echo 'none';}*/?>none">						
							 <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">
								<!-- Contact number -->
								<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number<span style="color:red">*</span></label>
									<div class="controls">
										<input readonly  style="height:30px" class="form-control margin-bottom-10" name="phone" type="number"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
								<div class="col-md-6">
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">E mail<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input readonly style="height:30px"  class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
								</div>
							 </div>	
							</div>	
						</div>	
					</div>	
						<!-- EOC Contact Details -->
						<!-- BOC Residential Address -->
					 <div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($StreetError)){ echo 'panel-danger';}else if(!empty($SuburbError)){ echo 'panel-danger';}
												else if(!empty($CityError)){ echo 'panel-danger';}else if(!empty($PostCodeError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayAddress" href="javascript:toggleAddress();">
                                                Residential Address Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleAddress" style="display:<?php /*if(!empty($StreetError)){ echo 'block';}else if(!empty($SuburbError)){ echo 'block';}
												else if(!empty($CityError)){ echo 'block';}else if(!empty($PostCodeError)){ echo 'block';}
												else{ echo 'none';}*/?>none">						
							 <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">
								<!-- Street -->
								<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
									<label>Street<span style="color:red">*</span></label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<span class="help-inline"><?php echo $StreetError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
										<label>Suburb<span style="color:red">*</span></label>
										<div class="controls">
							<input readonly style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<span class="help-inline"><?php echo $SuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								</div></div>
							 <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">								
								<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
									<label>City/Town/Township<span style="color:red">*</span></label>   
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<span class="help-inline"><?php echo $CityError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
								<div class="col-md-6">
								<label>Province</label>
                                <SELECT readonly class="form-control" id="State" name="State" size="1">
								<?php
										 $StateSelect = $State;
										  foreach($provinceArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $StateSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
								?>
								</SELECT>
								</div>
							</div>	
							 <div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">	</div>
								<div class="col-md-6">								
								<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
									<label class="control-label">Postal Code<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input readonly style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
										<?php if (!empty($PostCodeError)): ?>
										<span class="help-inline"><?php echo $PostCodeError;?></span>
										<?php endif; ?>
									</div>
								</div>
								</div></div>
							</div>
						</div>
					</div>
					<!-- BOC Payment Responsibility Details -->
					 <div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($resppnameError)){ echo 'panel-danger';}
												else if(!empty($respsurnameError)){ echo 'panel-danger';}
												else if(!empty($respworkplaceError)){ echo 'panel-danger';}
												else if(!empty($respworkaddrError)){ echo 'panel-danger';}
												else if(!empty($respworktelError)){ echo 'panel-danger';}
												else if(!empty($respworkemailError)){ echo 'panel-danger';}
												else if(!empty($respsuburbError)){ echo 'panel-danger';}
												else if(!empty($respcityError)){ echo 'panel-danger';}
												else if(!empty($respoccupationError)){ echo 'panel-danger';}
												else if(!empty($resppostalcodeError)){ echo 'panel-danger';}
												else if(!empty($respstreetError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayPayments" href="javascript:togglePayments();">
                                                Payment Responsibility Details
                                         </a>
                                    </h2>		
								</div>
							<div id="togglePayments" style="display:<?php /*if(!empty($resppnameError)){ echo 'block';}
																		  else if(!empty($respsurnameError)){ echo 'block';}
																		  else if(!empty($respworkplaceError)){ echo 'block';}
																		  else if(!empty($respworkaddrError)){ echo 'block';}
																		  else if(!empty($respworktelError)){ echo 'block';}
																		  else if(!empty($respworkemailError)){ echo 'block';}
																		  else if(!empty($respsuburbError)){ echo 'block';}
																		  else if(!empty($respcityError)){ echo 'block';}
																		  else if(!empty($respoccupationError)){ echo 'block';}
																		  else if(!empty($resppostalcodeError)){ echo 'block';}
																		  else if(!empty($respstreetError)){ echo 'block';}
																		  else{ echo 'none';}*/?>none">	
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">
								<!-- name								-->
								<div class="control-group <?php echo !empty($resppnameError)?'error':'';?>">
									<label>Name<span style="color:red">*</span></label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="resppname" type="text"  placeholder="Name" value="<?php echo !empty($resppname)?$resppname:'';?>">
										<?php if (!empty($resppnameError)): ?>
										<span class="help-inline"><?php echo $resppnameError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
							<div class="col-md-6">								
								<!-- SurName -->
								<div class="control-group <?php echo !empty($respsurnameError)?'error':'';?>">
									 <label>Surname
										 <span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="respsurname" type="text"  placeholder="SurName" value="<?php echo !empty($respsurname)?$respsurname:'';?>">
									<?php if (!empty($respsurnameError)): ?>
										<span class="help-inline"><?php echo $respsurnameError; ?></span>
									<?php endif; ?>	
									</div>
								</div>	
							</div></div>								
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">								
								<!-- name								-->
								<div class="control-group <?php echo !empty($respworkplaceError)?'error':'';?>">
									<label>Workplace<span style="color:red">*</span></label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="respworkplace" type="text"  placeholder="workplace" value="<?php echo !empty($respworkplace)?$respworkplace:'';?>">
										<?php if (!empty($respworkplaceError)): ?>
										<span class="help-inline"><?php echo $respworkplaceError;?></span>
										<?php endif; ?>
									</div>
								</div></div>	
								<div class="col-md-6">									
								<!-- SurName -->
								<div class="control-group <?php echo !empty($respoccupationError)?'error':'';?>">
									 <label>Occupation
										 <span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="respoccupation" type="text"  placeholder="occupation" value="<?php echo !empty($respoccupation)?$respoccupation:'';?>">
									<?php if (!empty($respoccupationError)): ?>
										<span class="help-inline"><?php echo $respoccupationError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
							</div></div>
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">								
								<div class="control-group <?php echo !empty($respworkaddrError)?'error':'';?>">
									 <label>Work Address
										 <span style="color:red">*</span>										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="respworkaddr" type="text"  placeholder="work address" value="<?php echo !empty($respworkaddr)?$respworkaddr:'';?>">
									<?php if (!empty($respworkaddrError)): ?>
										<span class="help-inline"><?php echo $respworkaddrError; ?></span>
									<?php endif; ?>	
									</div>
								</div></div>	
								<div class="col-md-6">	
								<div class="control-group <?php echo !empty($respworktelError)?'error':'';?>">
									<label class="control-label">Work Tel No:<span style="color:red">*</span></label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="respworktel" type="number"  placeholder="work tel no" value="<?php echo !empty($respworktel)?$respworktel:'';?>">
										<?php if (!empty($respworktelError)): ?>
										<span class="help-inline"><?php echo $respworktelError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div></div>
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">								
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($respworkemailError)?'error':'';?>">
									<label class="control-label">Work Email<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-10" id="respworkemail" name="respworkemail" type="email"  placeholder="Work E-mail" value="<?php echo !empty($respworkemail)?$respworkemail:'';?>">
										<?php if (!empty($respworkemailError)): ?>
										<span class="help-inline"><?php echo $respworkemailError;?></span>
										<?php endif; ?>
									</div>
								</div></div>
								<div class="col-md-6">									
								<!-- Street -->
								<div class="control-group <?php echo !empty($respstreetError)?'error':'';?>">
									<label>Street<span style="color:red">*</span></label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="respstreet" name="respstreet" type="text"  placeholder="Street" value="<?php echo !empty($respstreet)?$respstreet:'';?>">
										<?php if (!empty($respstreetError)): ?>
										<span class="help-inline"><?php echo $respstreetError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div></div>
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($respsuburbError)?'error':'';?>">
										<label>Suburb<span style="color:red">*</span></label>
										<div class="controls">
									    <input style="height:30px" class="form-control margin-bottom-10" id="respsuburb" name="respsuburb" type="text"  placeholder="Suburb" value="<?php echo !empty($respsuburb)?$respsuburb:'';?>">
											<?php if (!empty($respsuburbError)): ?>
											<span class="help-inline"><?php echo $respsuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								</div>
							 <div class="col-md-6">								
								<div class="control-group <?php echo !empty($respcityError)?'error':'';?>">
									<label>City/Town/Township<span style="color:red">*</span></label>   
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="respcity" name="respcity" type="text"  placeholder="City" value="<?php echo !empty($respcity)?$respcity:'';?>">
										<?php if (!empty($respcityError)): ?>
										<span class="help-inline"><?php echo $respcityError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							</div>
							<div class="row">	
								<div class="col-md-6">								
								<label>Province</label>
                                <SELECT class="form-control" id="respprovince" name="respprovince" size="1">
								 <?php
										 $respprovinceSelect = $respprovince;
										  foreach($provinceArr as $row)
										  {
											  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											  if($rowData[0] == $respprovinceSelect)
											  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
											  else
											  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										  }
								?>
								</SELECT></div>
								<div class="col-md-6">	
								<div class="control-group <?php echo !empty($resppostalcodeError)?'error':'';?>">
									<label class="control-label">Postal Code<span style="color:red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="resppostalcode" type="text"  placeholder="Postal Code" value="<?php echo !empty($resppostalcode)?$resppostalcode:'';?>">
										<?php if (!empty($resppostalcodeError)): ?>
										<span class="help-inline"><?php echo $resppostalcodeError;?></span>
										<?php endif; ?>
									</div>
								</div></div></div>
							</div>
						</div>
					</div>
					<!-- BOC Accordion Gompieno -->	
							<div class="tab-pane fade in" id="sample-3b">
							  <div class="panel <?php	if(!empty($AccountHolderError)){ echo 'panel-danger';}
														else if(!empty($BankNameError)){ echo 'panel-danger';}
														else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayBank" href="javascript:toggleBanking();">
                                                Banking Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleBank" style="display: <?php	/*if(!empty($AccountHolderError)){ echo 'block';}
														else if(!empty($BankNameError)){ echo 'block';}
														else{ echo 'none';}*/?>none">
							<div class="row">	
								<div class="col-md-6">																						
							<!-- Account Holder Name -->
							<label>Account Holder Name<span style="color:red">*</span></label>
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div></div>
							 <div class="col-md-6">		
							<!-- Bank Name -->
							<label>Bank Name</label>
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										  <SELECT class="form-control" id="BankName" name="BankName" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankid = '';
											$bankSelect = $BankName;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankid = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankid == $bankSelect)
												{
												echo "<OPTION value=$bankid selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankid>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div>	
								</div>
								</div></div></div>
							<div class="row">	
								<div class="col-md-6">																														
							<!-- Account Number  -->
							<label>Account Number<span style="color:red">*</span>
							</label>
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="number"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div></div>
							 <div class="col-md-6">	
							<!-- Account Type  -->
							<label>Account Type
							</label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1" >
											<?php
											  $accounttySelect = $AccountType;
											  foreach($Account_TypeArr as $row)
											  {
												  $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												  if($rowData[0] == $accounttySelect)
												  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
												  else
												  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
											  }

											if(empty($Account_TypeArr))
											{
												echo "<OPTION value=0>No Account Type</OPTION>";
											}
											?>
								</SELECT>

									</div>	
								</div>
								</div></div>
						<div class="row">	
						  <div class="col-md-6">	
							<label class="control-label">Branch Code
							</label>
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
									<div class="controls">
									 <SELECT class="form-control" id="BranchCode" name="BranchCode" size="1" >
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$branchcode = '';
											$bankid = '';
											$bankSelect = $BankName;
											$BranchSelect = $BranchCode;
											$branchdesc = '';
											foreach($dataBankBranches as $row)
											{
											  // -- Branches of a selected Bank
												$bankid = $row['bankid'];
												$branchdesc = $row['branchdesc'];
												$branchcode = $row['branchcode'];
													
												// -- Select the Selected Bank 
											  if($bankid == $bankSelect)
											   {
													if($BranchSelect == $BranchCode)
													{
													echo "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
													}
													else
													{
													 echo "<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
													}
											   }
											}
										
											if(empty($dataBankBranches))
											{
												echo "<OPTION value=0>No Bank Branches</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
									</div>
								</div></div>
								<div class="col-md-6">
									<label class="control-label">Payment Options</label>
									<!-- Radio BEGIN -->	
									<label class="radio-inline">
										<input type="radio" name="optpayments" id="optpayments" value="DebitOrder" onclick="applicationFeeText()" name="optpayments"  <?php echo $DebitOrderChk; ?>>Debit Order
									</label>
									<label class="radio-inline">
									  <input type="radio" name="optpayments" id="optpayments" value="EFT" onclick="applicationFeeText()" name="optpayments" <?php echo $EFTChk; ?>>EFT
									</label>
								</div>
								</div>
				
								</div>
							</div>
						</div>
					</div>
					<!-- Application Fee Payment -->
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($appfeedebitorderdateError)){ echo 'panel-danger';}else if(!empty($appfeesacceptError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayFees" href="javascript:toggleFees();">
                                                Application Fee Payment
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleFees" style="display: <?php /*if(!empty($appfeedebitorderdateError)){ echo 'block';}
																		  else if(!empty($appfeesacceptError)){ echo 'block';}
																		  else{ echo $none;}*/?>none">						
							 <div class="row">	
							   <div class="col-md-12">
								<div class="control-group">
								  <textarea id='applicationfeeTextValue' name='applicationfeeTextValue' class="form-control" style="height:250px;resize: none;" readonly><?php echo $applicationfeeTextValue;?></textarea>
								</div>
							   </div>	
							 </div>
							<div class="row">	
								<div class="col-md-6">
								  <div class="control-group <?php echo !empty($appfeesacceptError)?'error':'';?>">
									<label class="control-label">Accept Application Fee</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="appfeesaccept" name="appfeesaccept" type="checkbox" value="X" <?php echo $appfeesacceptChk; ?>>
										<?php if (!empty($appfeesacceptError)): ?>
										  <span class="help-inline"><?php echo $appfeesacceptError;?></span>
										<?php endif; ?>
									</div>
								  </div>
								</div>
								<div class="col-md-6">
								  <div class="control-group <?php echo !empty($appfeedebitorderdateError)?'error':'';?>">
									<label class="control-label">Application Fee Debit Date</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="appfeedebitorderdate" type="date" placeholder='yyyy-mm-dd' value="<?php echo !empty($appfeedebitorderdate)?$appfeedebitorderdate:'';?>">
										<?php if (!empty($appfeedebitorderdateError)): ?>
										  <span class="help-inline"><?php echo $appfeedebitorderdateError;?></span>
										<?php endif; ?>
									</div>
								  </div>								  
								</div>								
							</div>			
							</div>	
						</div>	
					</div>	
					<!-- Upload Required Documents -->
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($copyidError)){ echo 'panel-danger';}else if(!empty($payslipError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayUpload" href="javascript:toogleUpload();">
                                                Upload Required Documents
                                         </a>
                                    </h2>		
								</div>
							<div id="toogleUpload" style="display: <?php /*if(!empty($copyidError)){ echo 'block';}
																		  else if(!empty($payslipError)){ echo 'block';}
																		  else{ echo $none;}*/?>none">						
							 <p>Max file size 10Mb, Valid formats csv</p>
							 <div class="row">	
								<div class="col-md-6">																
									<label class="control-label">Copy of ID</label>
									<div class="controls">
									<input type="file" id="copyid" name="copyid" accept="*" >
								  	 <?php if (!empty($copyidError)): ?>
										<span class="help-inline" style='color:#a94442'><?php echo $copyidError;?></span>
									 <?php endif; ?>										
									</div>
								</div>
								<div class="col-md-6">																
									<br/>
									<div class="controls"><input type="text" name='copyidFile' id='copyidFile' value="<?php echo $copyidFile;?>" readonly /></div>
								</div>								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="controls">
									  <label class="control-label">Payslip of Person Responsible</label>  
									  <input type="file" id="payslip" name="payslip" accept="*">
								  	  <?php if (!empty($payslipError)): ?>
										<span class="help-inline" style='color:#a94442'><?php echo $payslipError;?></span>
									 <?php endif; ?>																	
									</div>
								</div>
								<div class="col-md-6">
									<div class="controls">
									<br/>
										<input type="text" name='payslipFile' id='payslipFile' value="<?php echo $payslipFile;?>" readonly />
									</div>
								</div>	
							</div>	
						</div>	
					</div>
				   </div>		
					<!-- BOC Contact Details -->
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($currcontactnumberError)){ echo 'panel-danger';}
						else if(!empty($currcaretakerError)){ echo 'panel-danger';}
						else if(!empty($currmonthlyrentError)){ echo 'panel-danger';}
						else if(!empty($currreasnforleavingError)){ echo 'panel-danger';}						
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayCurrentApartment" href="javascript:toggleCurrentApartment();">
                                                Current Apartment Details
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleCurrentApartment" style="display: <?php /*if(!empty($currcontactnumberError)){ echo 'block';}
																		      else if(!empty($currcaretakerError)){ echo 'block';}
																			  else if(!empty($currmonthlyrentError)){ echo 'block';}
																			  else if(!empty($currreasnforleavingError)){ echo 'block';}
																		      else{ echo $none;}*/?> none">						
							<div class="row">	
								<!-- Contact number -->
								<div class="col-md-6">																
								<!-- Contact number -->
								  <div class="control-group <?php echo !empty($currapartmentnameError)?'error':'';?>">
									<label class="control-label">Current Apartment Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="currapartmentname" type="text"  placeholder="current apartment name" value="<?php echo !empty($currapartmentname)?$currapartmentname:'';?>">
									</div>
								  </div>
								</div>
								<div class="col-md-6">	
								<!-- Contact number -->
								<div class="control-group <?php echo !empty($currcontactnumberError)?'error':'';?>">
									<label class="control-label">Contact number</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="currcontactnumber" type="number"  placeholder="Contact Number" value="<?php echo !empty($currcontactnumber)?$currcontactnumber:'';?>">
									</div>
								</div>
							</div></div>	
							<div class="row">	
								<div class="col-md-6">	
								<div class="control-group <?php echo !empty($currcaretakerError)?'error':'';?>">
									<label class="control-label">Caretaker Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="currcaretaker" type="text"  placeholder="Current Care Taker" value="<?php echo !empty($currcaretaker)?$currcaretaker:'';?>">
									</div>
							</div></div>
							<div class="col-md-6">
								<div class="control-group <?php echo !empty($currmonthlyrentError)?'error':'';?>">
									<label class="control-label">Monthly Rent(R)</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="currmonthlyrent" type="number" step="0.01"  placeholder="Monthly Rent" value="<?php echo !empty($currmonthlyrent)?$currmonthlyrent:'';?>">
									</div>
								</div>
							</div></div>	
							<div class="row">	
								<div class="col-md-6"></div>
								<div class="col-md-6">								
								<div class="control-group <?php echo !empty($currreasnforleavingError)?'error':'';?>">
									<label class="control-label">Reasons for Leaving</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="currreasnforleaving" type="text"  placeholder="Reasons for leaving" value="<?php echo !empty($currreasnforleaving)?$currreasnforleaving:'';?>">
									</div>
								</div>
							</div></div>									
							</div>	
						</div>	
					</div>
										<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($revcontactnumberError)){ echo 'panel-danger';}
						else if(!empty($prevcaretakerError)){ echo 'panel-danger';}
						else if(!empty($prevmonthlyrentError)){ echo 'panel-danger';}
						else if(!empty($prevreasnforleavingError)){ echo 'panel-danger';}						
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayPreviousApartments" href="javascript:togglePreviousApartments();">
                                                Previous Apartment Details
                                         </a>
                                    </h2>		
								</div>
							<div id="togglePreviousApartments" style="display: <?php /*if(!empty($revcontactnumberError)){ echo 'block';}
																		  else if(!empty($prevcaretakerError)){ echo 'block';}
																		  else if(!empty($prevmonthlyrentError)){ echo 'block';}
																		  else if(!empty($prevreasnforleavingError)){ echo 'block';}
																		  else{ echo $none;}*/?>none">						
								
							<div class="row">	
								<div class="col-md-6">									
								<div class="control-group <?php echo !empty($prevapartmentnameError)?'error':'';?>">
									<label class="control-label">Previous Apartment Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="prevapartmentname" type="text"  placeholder="Previous Apartment Name" value="<?php echo !empty($prevapartmentname)?$prevapartmentname:'';?>">
									</div>
								</div></div>
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($revcontactnumberError)?'error':'';?>">
									<label class="control-label">Contact number</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="revcontactnumber" type="text"  placeholder="contact number" value="<?php echo !empty($revcontactnumber)?$revcontactnumber:'';?>">
									</div>
								</div>
							</div></div>	
							<div class="row">	
								<div class="col-md-6">	
								<div class="control-group <?php echo !empty($prevcaretakerError)?'error':'';?>">
									<label class="control-label">Caretaker Name</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="prevcaretaker" type="text"  placeholder="Caretaker Name" value="<?php echo !empty($prevcaretaker)?$prevcaretaker:'';?>">
										<?php if (!empty($prevcaretakerError)): ?>
										<span class="help-inline"><?php echo $prevcaretakerError;?></span>
										<?php endif; ?>																		
									</div>
								</div></div>
								<div class="col-md-6">
								<div class="control-group <?php echo !empty($prevmonthlyrentError)?'error':'';?>">
									<label class="control-label">Monthly Rent(R)</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="prevmonthlyrent" type="text"  placeholder="Monthly Rent" value="<?php echo !empty($prevmonthlyrent)?$prevmonthlyrent:'';?>">
										<?php if (!empty($prevmonthlyrentError)): ?>
										<span class="help-inline"><?php echo $prevmonthlyrentError;?></span>
										<?php endif; ?>									
									</div>
								</div>
								</div></div>
							<div class="row">	
							<div class="col-md-6"></div>
								<div class="col-md-6">								
								<div class="control-group <?php echo !empty($prevreasnforleavingError)?'error':'';?>">
									<label class="control-label">Reasons for Leaving</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="prevreasnforleaving" type="text"  placeholder="Reasons for leaving" value="<?php echo !empty($prevreasnforleaving)?$prevreasnforleaving:'';?>">
										<?php if (!empty($prevreasnforleavingError)): ?>
										<span class="help-inline"><?php echo $prevreasnforleavingError;?></span>
										<?php endif; ?>
									</div>
								</div>
							</div></div>	
							</div>	
						</div>	
					</div>
					<div class="tab-pane fade in" id="sample-3C">						
						<div class="panel <?php if(!empty($OtherSpecifyError)){ echo 'panel-danger';}
						else if(!empty($friendfullnameError)){ echo 'panel-danger';}
						else if(!empty($friendcontactnumberError)){ echo 'panel-danger';}
						else if(!empty($friendemailError)){ echo 'panel-danger';}
												else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displayReferrals" href="javascript:toggleReferrals();">
                                                Referral
                                         </a>
                                    </h2>		
								</div>
							<div id="toggleReferrals" style="display: <?php /*if(!empty($OtherSpecifyError)){ echo 'block';}
																			else if(!empty($friendfullnameError)){ echo 'block';}
																			else if(!empty($friendcontactnumberError)){ echo 'block';}
																			else if(!empty($friendemailError)){ echo 'block';}
																		  else{ echo $none;}*/?>none">	
							<div class="row">	
								<div class="col-md-6">	
								<label class="control-label">How did you hear about us?</label>
								<!-- Radio BEGIN -->									
								<label class="radio-inline">
									<input type="radio" id="optradio" value="Friend" name="optradio" <?php echo $FriendChk; ?>>Friend
								</label>
								<label class="radio-inline">
								  <input type="radio" id="optradio" value="Online" name="optradio" <?php echo $OnlineChk; ?>>Online
								</label>
								<label class="radio-inline">
								  <input type="radio" id="optradio" value="Flier"  name="optradio" <?php echo $FlierChk; ?>>Flier
								</label>
								<label class="radio-inline">
								  <input type="radio" id="optradio" value="Other" name="optradio" <?php echo $OtherChk; ?>>Other
								</label>
								</div>
								<div class="col-md-6">	
								<div class="control-group <?php if(!empty($OtherChk)){echo !empty($OtherSpecifyError)?'error':'';}?>">
									<label class="control-label">If other please specify:</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="OtherSpecify" name="OtherSpecify" type="text"  value="<?php echo !empty($OtherSpecify)?$OtherSpecify:'';?>">
										<?php if(!empty($OtherChk)){if (!empty($OtherSpecifyError)): ?>
										<span class="help-inline"><?php echo $OtherSpecifyError;?></span>
										<?php endif;}?>
									</div>
								</div>	
							</div></div>
							<div class="row">	
								<div class="col-md-6">							
								<!-- friendfullname -->
								<div class="control-group <?php if(!empty($FriendChk)){echo !empty($friendfullnameError)?'error':'';}?>">
									 <label>Friend's Full Name:</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="friendfullname" type="text"  placeholder="friendfullname" value="<?php echo !empty($friendfullname)?$friendfullname:'';?>">
									<?php if (!empty($friendfullnameError)): ?>
										<span class="help-inline"><?php echo $friendfullnameError; ?></span>
									<?php endif; ?>	
									</div>
								</div></div>
							 <div class="col-md-6">								
								<!-- friendfullname -->
								<div class="control-group <?php echo !empty($friendcontactnumberError)?'error':'';?>">
									 <label>Friend's Contact Number:</label> 									
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="friendcontactnumber" type="text"  placeholder="Contact Number" value="<?php echo !empty($friendcontactnumber)?$friendcontactnumber:'';?>">
									<?php if (!empty($friendcontactnumberError)): ?>
										<span class="help-inline"><?php echo $friendcontactnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>	
							</div></div>
							<div class="row">	
								<div class="col-md-6"></div>
								<div class="col-md-6">								
								<!-- friendfull Email -->
								<div class="control-group <?php echo !empty($friendemailError)?'error':'';?>">
									 <label>Friend's Email Address: 
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="friendemail" type="text"  placeholder="friendemail" value="<?php echo !empty($friendemail)?$friendemail:'';?>">
									<?php if (!empty($friendemailError)): ?>
										<span class="help-inline"><?php echo $friendemailError; ?></span>
									<?php endif; ?>	
									</div>
								</div>	
							</div></div>								
								<!-- Radio END -->
							</div>	
						</div>	
</div>						
						<!-- EOC Residential Address -->									
<!---------------------- Login Details  Details --------------------------------------------------------->									
					<?php if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] != true){?>
					<div class="tab-pane fade in" id="sample-3b">						
						<div class="panel <?php if(!empty($password1Error)){ echo 'panel-danger';} 
												elseif(!empty($captchaError)){ echo 'panel-danger';} else{ echo 'panel-default';}?>">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle fa fa-plus-circle" id="displaylogindetails" href="javascript:togglelogindetails();">
                                                Login Details
                                         </a>
                                    </h2>		
								</div>
							<div id="togglelogindetails" style="display:<?php /*if(!empty($password1Error)){ echo 'block';}elseif(!empty($captchaError)){ echo 'block';}else { echo $none;}*/?>none">																				
								<!-- Start - Captcha -->
								<?php 
								$classname = "control-group error";
								$html = "";

								if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
                                       // echo "<a href='applicationForm.php' class='fa-gears'>Apply for e-loan</a>";
									}
									else
									{									
									echo '<div class="row"><div class="col-md-6">';	
									 echo" <div class='control-group'>
										<label class='control-label'>User ID</label>                                        
                                        <input  style='height:30px;z-index:0' readonly id='Userid' value='$idnumber' name='Userid' placeholder='UserID' class='form-control margin-bottom-20' type='text'>
									 </div></div>
									 <div class='col-md-6'>
									 <div class='control-group'>
										<label class='control-label'>User Name</label>                                        
                                        <input  style='height:30px;z-index:0' readonly id='username' value='$email' name='username' placeholder='UserName' class='form-control margin-bottom-20' type='text'>
									 </div></div></div>";
									 echo '<div class="row"><div class="col-md-6"></div><div class="col-md-6">';
									 echo "<label>Password<span style='color:red'>*</span></label>";
									 if(!empty($password1Error))
									 { 
									 echo "<div class='control-group error'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password'>
									 <span class='help-inline'>$password1Error</span></div>";
									 }
									 else
									 {
									  echo "<div class='control-group'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password' value=$password1></div>";
									 }	
									 echo "</div></div>";
// -- Disable/Enable Captcha 
								// -- 05.06.2018
									if(isset($_SESSION['Globalcaptcha']))
									{
										$Globalcaptcha = $_SESSION['Globalcaptcha'];

										if(empty($Globalcaptcha))
										{		 
										  // -- New Captcha					
										 echo '<div class="row"><div class="col-md-6">';										  
										 echo "<div class='panel-heading'>";
										 echo "<h3 class='panel-title'>
												<a class='accordion-toggle' href='#collapse-Two' data-parent='#accordion' data-toggle='collapse'>
													<b>Please enter the verification code shown. </b>                                           </a>
												</h3>
										  </div></div>";
										 echo '<div class="col-md-6">'; 
										 echo "<div id='captcha-wrap'>
												<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' /><img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>";
										 if(!empty($captchaError))
										 {
										 echo "<div class='control-group error'>
												<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
												<span class='help-inline'>$captchaError</span></div>";
										 }
										 else
										 {
										 $code = '';
										 echo "<div class='control-group'>
										 <input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
										</div>";
										 }
										 echo "</div></div>";
										}
									}
								}
								?>
								</div>
						</div>
					</div>	<?php }?>
				</div> <!-- Group -->	
					<!-- <div id="wrapper" name=""><div id="fixed_div" name="">-->
                              <hr>
                              <div class='row'>
								<div class="col-md-10">
                                    <div class='col-lg-9'>
									   <div class="form-group <?php echo !empty($agreeError)?'error':'';?>">
                                        <label class='checkbox'>
                                            <input type='checkbox' name="agree" id="agree" value="X" <?php echo $agreeChk; ?>>I read and accept the
                                            <a id="displayText" href="javascript:toggle();">Terms and Conditions</a>
                                        </label>
										<?php if(!empty($agreeError))
												{
													echo "<span class='text-danger'>$agreeError</span>";
												}
										?>
									   </div>	
                                    </div>
								</div>	
								<div class="col-md-2">									
                                    <div class='col-lg-4 text-right'>
                                        <button class='btn btn-primary' id="Apply" name="Apply"  type='submit'>Apply</button>
                                    </div>
								</div>	
                                </div>
								<!-- BOC - Testing -->
								<hr>
								<div id="toggleText" style="display: block;border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;"  >
									<h1><b>Terms and Conditions</b></h1>
									   <?php echo "<p>$Globalterms;</p>"; ?>
								</div>	
						<!--</div></div>-->		
						</div>
	
								<!-- EOC - Testing -->
                            </form> 
							<?php
							if(!empty($_POST))
							  // if($_SERVER['REQUEST_METHOD']=='POST')
							   {
								  //echo '<script type="text/javascript"> LastPaymentDate(); </script>';
							   } 
							?>
                         <!-- </div> Accordion Modise -->
                      </div>
			      </div>
			</div>		  
<?php  
// -- Database Connections  
// -- require 'database.php'; 
if(isset($_POST['Apply']))  
{  
	 $userid = $_POST['idnumber']; 
	 $password = $password1; 
	 $pdo = Database::connect();
	 $check_user = "SELECT * FROM $tbl_user Inner Join $tbl_customer on $tbl_user.userid = $tbl_customer.CustomerId  WHERE userid= ? and password= ?";
					
	 $q = $pdo->prepare($check_user);
	 $q->execute(array($userid,$password));
	 $row = $q->fetch(PDO::FETCH_ASSOC);
	 $count = $q->rowCount();

	if($count >= 1)
    {  
			//$row = mysql_fetch_array($result);
  			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.  
			
			// Title
			$_SESSION['Title'] = $row['Title'];	
			// First name
			$_SESSION['FirstName'] = $row['FirstName'];
			// Last Name
			$_SESSION['LastName'] = $row['LastName'];
			// Phone numbers
			$_SESSION['phone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];
			
			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];
			
			// Dob
			$_SESSION['Dob'] = $row['Dob'];
			
			// phone
			$_SESSION['phone'] = $row['phone'];
			
			$customerLoggedIn = true;
			$_SESSION['customerLoggedIn'] = $customerLoggedIn;		

     //echo "<script>window.open('index.php','_self')</script>";  

    }  
    /*else  
    {  
         // echo "<script>alert('Email or password is incorrect!".$userid.$password."')</script>";  
    }*/ 
} 
?>  				 
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 var expand 	= "fa fa-plus-circle";
 var collapse	= "fa fa-minus-circle";
 $(document).ready(function(){
  $(".accordion-toggle a").click(function(){
   alert("clicked");
  });
});
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 15.07.2017 - Banking Details ---  30

// -- Different Loan Types.
 function valueInterest(sel) 
 {
		var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
		var fromdate = sel.options[sel.selectedIndex].getAttribute('data-fromdate');
		var todate = sel.options[sel.selectedIndex].getAttribute('data-todate');
		var fromduration = sel.options[sel.selectedIndex].getAttribute('data-from');
		var toduration = sel.options[sel.selectedIndex].getAttribute('data-to');
		var fromamount = sel.options[sel.selectedIndex].getAttribute('data-fromamount');
		var toamount = sel.options[sel.selectedIndex].getAttribute('data-toamount');
		var qualifyfrom = sel.options[sel.selectedIndex].getAttribute('data-qualifyfrom');
		var qualifyto = sel.options[sel.selectedIndex].getAttribute('data-qualifyto');
		
//alert(interest);			
		document.getElementById('InterestRate').value = interest;
		document.getElementById('fromdate').value = fromdate;
		document.getElementById('todate').value = todate;
		document.getElementById('fromamount').value = fromamount;
		document.getElementById('toamount').value = toamount;
		document.getElementById('qualifyfrom').value = qualifyfrom;
		document.getElementById('qualifyto').value = qualifyto;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;

 }		
// -- EOC Different Loan Types.

// -- 30.06.2017 -- LoanDuration
 // -- Enter to capture comments   ---
 function defineloandutations(sel) 
 {
		
		var fromduration = document.getElementById('fromduration').value;
		var toduration = document.getElementById('toduration').value;

		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"DefineLoanDurations.php",
				       data:{fromduration:fromduration,toduration:toduration},
						success: function(data)
						 {	
						//alert(data);
							jQuery("#LoanDuration").html(data);
						 }
					});
				});				  
			  return false;
  }
// -- 30.06.2017 -- LoanDuration

// BOC -- Jquery for terms & conditions
 function toggle() 
 {
	var ele = document.getElementById("toggleText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "Terms and Conditions";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "Terms and Conditions";
		collapseOthers(ele.getAttribute('id'));

	}
}

function toggleBanking() 
 {
	var ele = document.getElementById("toggleBank");
	var text = document.getElementById("displayBank");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayBank" href="javascript:toggleBanking();">	Banking Details</a>';
		collapseOthers(ele.getAttribute('id'));

	}
} 

function toggleLoan() 
 {
	var ele = document.getElementById("toggleLoan");
	var text = document.getElementById("displayLoan");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayLoan" href="javascript:toggleLoan();">	Loan Application Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayLoan" href="javascript:toggleLoan();">	Loan Application Details</a>';
		collapseOthers(ele.getAttribute('id'));

	}
} 

function toggleRegister() 
 {
	var ele = document.getElementById("toggleRegister");
	var text = document.getElementById("displayRegister");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//$("#toggleRegister").slideDown("slow");
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+' " id="displayRegister" href="javascript:toggleRegister();">	Personal Information Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+' collapsed" id="displayRegister" href="javascript:toggleRegister();"> Personal Information Details</a>';
	    //$("#toggleRegister").slideDown("slow");
	   collapseOthers(ele.getAttribute('id'));
	}
} 

function toggleFees() 
 {
	var ele = document.getElementById("toggleFees");
	var text = document.getElementById("displayFees");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayFees" href="javascript:toggleFees();">	Application Fee Payment Details</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayFees" href="javascript:toggleFees();"> Application Fee Payment Details</a>';
	   collapseOthers(ele.getAttribute('id'));

	}
} 

function toogleUpload() 
 {
	var ele = document.getElementById("toogleUpload");
	var text = document.getElementById("displayUpload");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayUpload" href="javascript:toogleUpload();">	Upload Required Documents</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayUpload" href="javascript:toogleUpload();"> Upload Required Documents</a>';
		collapseOthers(ele.getAttribute('id'));

	}
} 

function toggleAddress() 
 {
	var ele = document.getElementById("toggleAddress");
	var text = document.getElementById("displayAddress");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
			//ele = "glyphicon glyphicon-chevron-down";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';

  	}
	else {
		ele.style.display = "block";
		//ele.class = "glyphicon glyphicon-chevron-up";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayAddress" href="javascript:toggleAddress();">	Residential Address</a>';
		collapseOthers(ele.getAttribute('id'));

	}
} 


function togglePayments() 
 {
	var ele = document.getElementById("togglePayments");
	var text = document.getElementById("displayPayments");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayPayments" href="javascript:togglePayments();">Payment Responsibility Details</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayPayments" href="javascript:togglePayments();">Payment Responsibility Details</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 

function toggleCurrentApartment() 
 {
	var ele = document.getElementById("toggleCurrentApartment");
	var text = document.getElementById("displayCurrentApartment");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayCurrentApartment" href="javascript:toggleCurrentApartment();">Current Apartment Details</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayCurrentApartment" href="javascript:toggleCurrentApartment();">Current Apartment Details</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 
function togglePreviousApartments() 
 {
	var ele = document.getElementById("togglePreviousApartments");
	var text = document.getElementById("displayPreviousApartments");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayPreviousApartments" href="javascript:togglePreviousApartments();">Previous Apartment Details</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayPreviousApartments" href="javascript:togglePreviousApartments();">Previous Apartment Details</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 


function toggleReferrals() 
{
	var ele = document.getElementById("toggleReferrals");
	var text = document.getElementById("displayReferrals");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayReferrals" href="javascript:toggleReferrals();">Referral</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayReferrals" href="javascript:toggleReferrals();">Referral</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 

function toggleQuestionares() 
{
	var ele = document.getElementById("toggleQuestionares");
	var text = document.getElementById("displayQuestionares");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayQuestionares" href="javascript:toggleQuestionares();">Questionares</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayQuestionares" href="javascript:toggleQuestionares();">Questionares</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 

var idValue = '';
function collapseOthers(id)
{
	// -- 
	var toggle	= ['toggleFees','toggleRegister','toggleAddress',
			,'toggleBank',
			,'toogleUpload',
			,'togglePayments',
			,'toggleCurrentApartment',
			,'togglePreviousApartments',
			,'toggleQuestionares',
			,'toggleContacts',
			,'toggleReferrals',
			,'togglelogindetails'];
	idValue = id;		
	toggle.forEach(myFunction);
}

function myFunction(item, index) 
{
	if(item !== idValue)
	{
		el = document.getElementById(item);
		if(el.style.display == 'block')
		{
		  el.style.display = "none";
		  el2 = 'display'+item.substring(6);
		  el3 = document.getElementById(el2);;
		  el3.className = "accordion-toggle "+expand;
		}
	}
}

function toggleContacts() 
 {
	var ele = document.getElementById("toggleContacts");
	var text = document.getElementById("displayContacts");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displayContacts" href="javascript:toggleContacts();">	Contact Details</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displayContacts" href="javascript:toggleContacts();"> Contact Details</a>';
		collapseOthers(ele.getAttribute('id'));
	}
} 

function togglelogindetails()
{
	var ele = document.getElementById("togglelogindetails");
	var text = document.getElementById("displaylogindetails");
	if(ele.style.display == "block") 
	{
    	ele.style.display = "none";
		text.outerHTML = '<a class="accordion-toggle '+expand+'" id="displaylogindetails" href="javascript:togglelogindetails();">	Login Details</a>';
  	}
	else 
	{
		ele.style.display = "block";
		text.outerHTML = '<a class="accordion-toggle '+collapse+'" id="displaylogindetails" href="javascript:togglelogindetails();"> Login Details</a>';
	    collapseOthers(ele.getAttribute('id'));
	}
}

function applicationFeeText()
{
	 Arr = <?php echo json_encode($applicationFeeText); ?>;
	 DebitOrder = document.getElementsByName('optpayments')[0].checked;
	 
	 if(DebitOrder)
	 {document.getElementById('applicationfeeTextValue').innerHTML = Arr[1];}
	 else
	 {document.getElementById('applicationfeeTextValue').innerHTML = Arr[0];}
 
}
// EOC -- Jquery. 
</script> 