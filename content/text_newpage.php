<?php 
require 'database.php';

$pageError = null;
$page = '';
$features = '';
$count = 0;

if (!empty($_POST)) 
{   $count = 0;
	$page = $_POST['page'];
	$features = $_POST['features'];
	
	$valid = true;
	
	if (empty($page)) { $pageError = 'Please enter page name.'; $valid = false;}
	
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$sql = "select * from page where page = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($page));
			$data = $q->fetch(PDO::FETCH_ASSOC);

			if(!empty($data))
			{
				$pageError = 'Page aleady exist!.'; $valid = false;
			}
			
			if ($valid) 
			{
				$sql = "INSERT INTO page (page,features) VALUES(?,?) "; 		
				$q = $pdo->prepare($sql);
				$q->execute(array($page,$features));
				$count = $count + 1;
			}
			Database::disconnect();
		}
}
?>
<div class="container background-white bottom-border">   
	<div class='row margin-vert-30'>
	   <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
		<div class="panel-heading">
			<h2 class="panel-title">
								<a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
									 New Page Details
								</a>
							</h2>
			<?php # error messages
					if (isset($message)) 
					{
						foreach ($message as $msg) 
						{
							printf("<p class='status'>%s</p></ br>\n", $msg);
						}
					}
					# success message
					if($count !=0)
					{
					printf("<p class='status'>page successfully added.</p>");
					}
			?>
							
			</div>				

			<p><b>Page</b><br />
			<input style="height:30px" type='text' name='page'/>
			<?php if (!empty($pageError)): ?>
			<span class="help-inline"><?php echo $pageError;?></span>
			<?php endif; ?>
			</p>		
			<p><b>Features (separated by , for e.g. translist,mdst summary</b><br />
			  <input style="height:30px" type='text' id='features' name='features'/>
		    </p>
		<table>
		<tr>
			<td><div>
			<?php
			$btnclass = 'Save';
			$btnback = 'Back';
			$btnSave = 'Save';
			$backLinkName = 'pages';
			
			if(isset($_SESSION["GlobalTheme"])) 
			{
				$btnSave = $_SESSION["buttonNew"];
				$btnback = $_SESSION["buttonBack"];
				$btnclass = $_SESSION["ActionbuttonClass"];							
			}
			
			echo "<button type='submit' class='".$btnclass."'>$btnSave </button>
				<a class='".$btnclass."' href=$backLinkName>$btnback </a>";
			?>		
		     </div></td>
		</tr>
		</table>	
		
		</form> 
	</div>
</div>
</div>