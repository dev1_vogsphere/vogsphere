<?php
//////////////////////////////////////////////////// SEARCH BOX ////////////////////////////////////////////
//																										  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////
include 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 
  Database::disconnect();
  
// --------------------------------- EOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

 $tbl_name="bankbranch"; // Table name 
 $bankid = '';
 $branchcode = '';
 
 echo "<div class='container background-white bottom-border'>";		
   
//<!-- Search Login Box -->
  if($_SESSION['role'] == 'admin')
	{
echo "<div class='row margin-vert-30'>
                            <!-- Login Box -->
                            <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
                                <form class='login-page' method='post' onsubmit=' return addRow()'>
                                    <div class='login-header margin-bottom-30'>
                                        <h2 class='text-center'>Search for Bank Branch</h2>
                                    </div>
									
                                    <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input style='height:30px;z-index:0' id='branchcode' name='branchcode' placeholder='Branch Code' class='form-control' type='text' value=$branchcode>
                                    </div>	
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <button class='btn btn-primary pull-right' type='submit'>Search</button>
                                        </div>
                                    </div>";
	}  							
?>  
<label>Bank Name</label>
									<div class="controls">
										<div class="controls">
										<SELECT class="form-control" id="bankid" name="bankid" size="1" onChange="valueselect(this);">
									<!-------------------- BOC 2017.04.15 - Bank Details & Branch Code --->	
											<?php
											$bankidTemp = '';
											$bankSelect = $bankid;
											$bankdesc = '';
											foreach($dataBanks as $row)
											{
												$bankidTemp = $row['bankid'];
												$bankdesc = $row['bankname'];
												// Select the Selected Role 
												if($bankidTemp == $bankSelect)
												{
												echo "<OPTION value=$bankidTemp selected>$bankdesc</OPTION>";
												}
												else
												{
												 echo "<OPTION value=$bankidTemp>$bankdesc</OPTION>";
												}
											}
										
											if(empty($dataBanks))
											{
												echo "<OPTION value=0>No Banks</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - Bank Details & Branch Code --->												
										  </SELECT>
										</div></div>
 </form>
                            </div>										
            </div> <!-- Search Login Box -->
		<!---------------------------------------------- Start Data ----------------------------------------------->
		<div>
				  <div class="table-responsive">

				
		              <?php 
		
						
					if($_SESSION['role'] != 'customer')
					{	 
						if ( !empty($_POST)) 
						   {
							 $bankid = $_POST['bankid'];
							 $branchcode = $_POST['branchcode'];
						   }
					}					   
					   $sql = "";
					   $q = "";
									   
/////////////////////////////////////////////////////////////////////////////////////////////
//                         		Role : admin     										   //
/////////////////////////////////////////////////////////////////////////////////////////////						 
					if($_SESSION['role'] == 'admin')
					{
					  if(!empty($bankid))
					    {
						if($branchcode != '')
						{
							$sql = "SELECT * FROM $tbl_name WHERE bankid = ? AND branchcode = ?";
							$q = $pdo->prepare($sql);
							$q->execute(array($bankid,$branchcode));	
							$data = $q->fetchAll();  
						}
						else
						{
							$sql = "SELECT * FROM $tbl_name WHERE bankid = ?";
							$q = $pdo->prepare($sql);
							$q->execute(array($bankid));	
							$data = $q->fetchAll();  
						}
						
					    }
						else
						{
							// -- All Records
						$sql = "SELECT * FROM $tbl_name";
						$q = $pdo->prepare($sql);
					    $q->execute();	
						$data = $q->fetchAll(); 
						}												
					}
//////////////////////////////////////////////////// END SEARCH BOX ////////////////////////////////////////////
//																										  	  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						echo "<a class='btn btn-info' href=newbankbranch.php>Add New Bank Branch</a><p></p>"; 

						echo "<table class='table table-striped table-bordered'>"; 
						echo "<tr>"; 
						echo "<td><b>Branch Code</b></td>"; 
						echo "<td><b>Branch Name</b></td>"; 
						echo "<td><b>Bank ID</b></td>"; 
						echo "<th colspan='2'>Action</th>"; 
						echo "</tr>"; 
						  
						foreach($data as $row)
						{ 
						//foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
						echo "<tr>";  
						echo "<td valign='top'>" . nl2br( $row['branchcode']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['branchdesc']) . "</td>";  
						echo "<td valign='top'>" . nl2br( $row['bankid']) . "</td>";  
						echo "<td valign='top'><a class='btn btn-success' href=editbankbranch.php?branchcode={$row['branchcode']}>Edit</a></td><td>
						<a class='btn btn-danger' href=deletebankbranch.php?branchcode={$row['branchcode']}>Delete</a></td> "; 
						echo "</tr>"; 
						} 
						echo "</table>"; 

	 				  /*foreach ($data as $row) 
					  {
					  }*/
					   Database::disconnect();
					  ?>
			
				</div>
    	</div>
		</div>
   		<!---------------------------------------------- End Data ----------------------------------------------->