<?php
		//error_reporting( E_ALL );
		ini_set('display_errors', 0);
		
	require 'database.php';
	$tbl_customer = "customer";
	$tbl_loanapp = "loanapp";
	$tbl_lead = "lead";
	$dbname = "ecashpdq_eloan";
	$filecount = 0;
	$customerid = null;
	$ApplicationId = null;
			
	$FILELEADS = '';

	$fieldname = '';
	$filenameEncryp = '';
	
	// -- Documents to be uploaded.
	$leads = 'leads';

	// -- EOC 01.10.2017 - Upload Credit Reports.
	$dataRole = null;
	$role = ""; 
	// -- Role : Customer
	$name = '';
	// -- BOC Encrypt 16.09.2017 - Parameter Data.
	$CustomerIdEncoded = "";
	$ApplicationIdEncoded = "";
	$message = null;
	// -- EOC Encrypt 16.09.2017 - Parameter Data.
	
	$location = 'index';
	
	$UserCode = null; 
	$IntUserCode = null;

	$today = date('Y-m-d');

	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}
	
	// -- if not logged in go back.
	if ( null==$userid ) 
	    {
			if (!headers_sent()) 
			{
				header("Location: ".$location);
			}
		}
$valid_formats = array("csv");
$max_file_size = 1024*10000; //10MB
$path = "Files/Collections/Instructions/".$userid."_".$today."_"; // Upload directory 
$count = 0;
if ( !empty($_POST)) 
{
	 $message = null;
	 print_r($message);
}

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{		
	$filecount = count($_FILES['files']['name']);
	if($filecount == 1)
	{
	foreach ($_FILES['files']['name'] as $f => $name) 
	{     
	    if ($_FILES['files']['error'][$f] == 4) {
	        continue; // Skip file if any error found
	    }	       
	    if ($_FILES['files']['error'][$f] == 0) {	           
	        if ($_FILES['files']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				continue; // Skip invalid file formats
			}
			// -- Check Maximu file one can upload
	        else{ // No error found! Move uploaded files 
						// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;
			
		 // -- Update File Name Directory	
	            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }
				
				// -- BOC Encrypt the uploaded PDF with ID as password.
					$file_parts = pathinfo($name);
					$filenameEncryp = $path.$name;
					switch($file_parts['extension'])
					{
						case "pdf":
						{
							

						break;
						}
						case "PDF":
						{
							
						break;
						}
					}
				// -- EOC Encrypt the uploaded PDF file with ID as password.
	        }
	    }
	}
}
else
{
	$message[] = "Only one file can be uploaded at a time.";
}	
}

/**
    * @param string $filePath
    * @param int $checkLines
    * @return string
    */
if(!function_exists('getCsvDelimiter'))	
{	
   function getCsvDelimiter($filePath)
   {
	  $checkLines = 1;
      $delimiters =[',', ';', '\t'];

      $default =',';

       $fileObject = new \SplFileObject($filePath);
       $results = [];
       $counter = 0;
       while ($fileObject->valid() && $counter <= $checkLines) {
           $line = $fileObject->fgets();
           foreach ($delimiters as $delimiter) {
               $fields = explode($delimiter, $line);
               $totalFields = count($fields);
               if ($totalFields > 1) {
                   if (!empty($results[$delimiter])) {
                       $results[$delimiter] += $totalFields;
                   } else {
                       $results[$delimiter] = $totalFields;
                   }
               }
           }
           $counter++;
       }
       if (!empty($results)) {
           $results = array_keys($results, max($results));

           return $results[0];
       }
return $default;
}
}

//If you need a function which converts a string array into a utf8 encoded string array then this function might be useful for you:
if(!function_exists('utf8_string_array_encode'))	
{	

function utf8_string_array_encode($array){
    $func = function(&$value,&$key){
        if(is_string($value)){
            $value = utf8_encode($value);
        }
        if(is_string($key)){
            $key = utf8_encode($key);
        }
        if(is_array($value)){
            utf8_string_array_encode($value);
        }
    };
    array_walk($array,$func);
    return $array;
}
}
?>

<!--<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Multiple File Upload with PHP - Demo</title> -->
<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Upload Batch DEBICHECKS via csv File</h1>
		<?php
		# error messages
		if (isset($message)) {
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>%d files added successfully!</p>\n", $count);
		}
		?>
				<!-- Multiple file upload html form-->
		<form action="" method="post" enctype="multipart/form-data">		
		<p>Download a Template DEBICHECK file <a href='Files/Collections/Instructions/Template-Batch Upload Debi Check.xlsx' target='_blank' style="color:red"><strong>here</strong></a> and Instructions <a href='Files/Collections/Instructions/Documentation_BatchInstructions.docx' target='_blank' style="color:red"><strong>here</strong></a></p>
		<p>Max file size 10Mb, Valid formats csv</p>
		<p style="color:red"><b>The functionality is underconstruction.coming soon!</b></p>
		<br />
				
			<input type="file" name="files[]" multiple="multiple" accept=".csv" style="display:none">
			<table class ="table table-user-information">
				  <tr>
					<td>
						<div>
							<!-- <input type="submit" value="Upload"> -->
							<input type="submit" class="btn btn-primary" value="Upload" style="display:none">
						</div>
					</td>
				<td>
				<div>
				<?php echo '<a class="btn btn-primary" href="'.$location.'">Back</a>'; ?></div></td>
				</tr>
			</table>		
			<?php if(isset($_POST))
			{
				
				if(!empty($filenameEncryp))
				{ ?>
				<div class="row">
				  <div class="table-responsive">
					<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
					  <tr>
					  <td>Feedback</td>
					  <td>User Code 			</td>
					  <td>Reference 1 	</td>
					  <td>Reference 2 	</td>
					  <td>ID Type 		</td>
					  <td>ID No 		</td>
					  <td>Initials 			</td>
					  <td>Account Holder 		</td>
					  <td>Account Type 		</td>
					  <td>Account Number 		</td>
					  <td>Branch Code 		</td>
					  <td>Number of Payments 		</td>
					  <td>Service Type 		</td>					
					  <td>Service Mode 		</td>
					  <td>Frequency 			</td>
					  <td>Action Date 		</td>
					  <td>Bank Reference 		</td>
					  <td>Amount 				</td>
					  </tr>					  
			<?php
					// ----- call ------ 
					$arrayInstruction = null;
					$csvdata = null;
					$count = 0;
					$x=0;
					$z = 0;
					$message = '';
					$anySuccess = ''; 
					$separetor = getCsvDelimiter($filenameEncryp);//";";
					$notification = array();
					$notificationsList = array();
					
					$csvdata = csv_in_array( $filenameEncryp, $separetor, "\"", true ); 
					//print_r($csvdata);
					$count = count($csvdata); 
					for($x=0; $x<$count; $x++) 
						{ 
							$keydataJson = array();
							$keydata 	 = $csvdata[$x];
							//$keydata = utf8_string_array_encode($keydata);
							//print($keydata['Reference 1']);
							//Debug Encoding - echo json_encode($keydata)."\n";
							// -- UTF-8
							$keydata = json_encode($keydata,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
							if(empty($keydata))
							{
								// -- ANSI
								$keydata = $csvdata[$x];
							}
							else
							{
								$keydataJson = json_decode($keydata,true);
							}
							
							$arrayInstruction[1] = $UserCode;//$keydata['Client_Id']; 										
							$TotalColmn = 17;
							$index = 0;
							// -- Is to avoid unicharacters e.g. \ff
							
							if(!empty($keydataJson))
							{
							$keydata = array();
							foreach($keydataJson as $item => $v) 
							{ 
							//print_r($v);
							//print('<br/>');
							// -- Trim trailing & leading spaces.
								$v = trim($v);
								if($index == 0){$keydata['Reference 1'] = $v;}
								if($index == 1){$keydata['Reference 2'] = $v;}
								if($index == 2){$keydata['ID Type'] = $v;}
								if($index == 3){$keydata['ID Number'] = $v;}

								if($index == 4){$keydata['Contact Number'] = $v;}
								if($index == 5){$keydata['Initials'] = $v;}
								if($index == 6){$keydata['Account Holder'] = $v;}
								if($index == 7){$keydata['Account Type'] = $v;}
								
								if($index == 8){$keydata['Account Number'] = $v;}
								if($index == 9){$keydata['Bank Name'] = $v;}
								if($index == 10){$keydata['Number of Payments'] = $v;}
								if($index == 11){$keydata['Service Mode'] = $v;}								
								if($index == 12){$keydata['Service Type'] = $v;}

								if($index == 13){$keydata['Frequency'] = $v;}
								if($index == 14){$keydata['Action Date'] = str_replace('/','-',$v); }
								if($index == 15){$keydata['Bank Reference'] = $v;}
								if($index == 16){$keydata['Amount'] = $v;}
								if($index == 17){$keydata['Notify'] = $v;}
								if($index == 18){$keydata['Entry Class'] = $v;}
								$index = $index  + 1;
							}
							}
							// -- DEBICHECK 							
							// -- UTF-8
							//print_r($keydata);

							//echo $keydata['Reference 1'];
							if(isset($keydata['Reference 1'])){$arrayInstruction[2] = $keydata['Reference 1'];}else{$arrayInstruction[2] = '';}
							// -- UTF-8 DOM
							//echo $arrayInstruction[2];
							if(empty($arrayInstruction[2])){if(isset($keydata['\ufeffReference 1'])){$arrayInstruction[2] = $keydata['\ufeffReference 1'];}else{$arrayInstruction[2] = '';}}
							if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';}							
							//if(isset($keydata['Reference 2'])){$arrayInstruction[3] = $keydata['Reference 2'];}else{$arrayInstruction[3] = '';} 								
							if(isset($keydata['ID Type'])){$arrayInstruction[4] = $keydata['ID Type'];}else{$arrayInstruction[4] = '';} 									
							if(isset($keydata['ID Number'])){$arrayInstruction[5] = $keydata['ID Number'];}else{$arrayInstruction[5] = '';} 									
							if(isset($keydata['Initials'])){$arrayInstruction[6] = $keydata['Initials'];}else{$arrayInstruction[6] = '';} 										
							if(isset($keydata['Account Holder'])){$arrayInstruction[7] = $keydata['Account Holder'];}else{$arrayInstruction[7] = '';} 									
							if(isset($keydata['Account Type'])){$arrayInstruction[8] = Getaccounttypebyname($keydata['Account Type']);}else{$arrayInstruction[8] = '';} 									
							if(isset($keydata['Account Number'])){$arrayInstruction[9] = $keydata['Account Number'];}else{$arrayInstruction[9] = '';} 	
// -- User bank name on the spreadsheet to get the branchcode.							
							if(isset($keydata['Bank Name'])){$arrayInstruction[10] = Getbrachcodefromname($keydata['Bank Name']);}else{$arrayInstruction[10] = '';}//$keydata['Bank Name']; 									
							if(isset($keydata['Number of Payments'])){$arrayInstruction[11] = $keydata['Number of Payments'];}else{$arrayInstruction[11] = '';} 									
							if(isset($keydata['Service Type'])){$arrayInstruction[12] = $keydata['Service Type'];}else{$arrayInstruction[12] = '';} 									
							if(isset($keydata['Service Mode'])){$arrayInstruction[13] = $keydata['Service Mode'];}else{$arrayInstruction[13] = '';} 									
							if(isset($keydata['Frequency'])){$arrayInstruction[14] = $keydata['Frequency'];}else{$arrayInstruction[14] = '';} 										
							if(isset($keydata['Action Date'])){$arrayInstruction[15] = $keydata['Action Date'];}else{$arrayInstruction[15] = '';} 									
							if(isset($keydata['Bank Reference'])){$arrayInstruction[16] = $keydata['Bank Reference'];}else{$arrayInstruction[16] = '';} 								
							if(isset($keydata['Amount'])){$arrayInstruction[17] = $keydata['Amount'];}else{$arrayInstruction[17] = '';} 										
							$arrayInstruction[18] = '1';//$keydata['Status_Code']; 									
							$arrayInstruction[19] = 'pending';//$keydata['Status_Description']; 							
							$arrayInstruction[20] = '';//$keydata['mandateid']; 										
							$arrayInstruction[21] = '';//$keydata['mandateitem']; 
							$arrayInstruction[22] = $IntUserCode;									
							$arrayInstruction[23] = $UserCode; 	
							if(isset($keydata['Entry Class'])){$arrayInstruction[24] = Getentryclassid($keydata['Entry Class']);}else{$arrayInstruction[24] = 0;} 										
							if(isset($keydata['Contact Number'])){$arrayInstruction[25] = $keydata['Contact Number'];}else{$arrayInstruction[25] = '';} 										
							if(isset($keydata['Notify'])){$arrayInstruction[26] = $keydata['Notify'];}else{$arrayInstruction[26] = '';} 										
							$arrayInstruction[27] = $userid; // -- created by 	

							$message = auto_create_billinstruction($arrayInstruction); 
							if($message == 'success' )
							{ 
								$anySuccess  = $message ;
							    // -- Build Queue Message to notify client about their debit orders.
								if($arrayInstruction[26] == 'Yes')
								{
									$smstemplatename = 'debitorder_notification';
									$applicationid = 0;		
									$Action_Date = $arrayInstruction[15];			
									$Reference_1 = $arrayInstruction[2];
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{	
										$notification['phone'] 		 = $arrayInstruction[25];
										$notification['content'] 	 = $contents;
										$notification['reference_1'] = $arrayInstruction[2];	
										$notificationsList[] = $notification;	// add into list
									}
								}
							}
							
							echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";//<td>".$arrayInstruction[18]."</td><td>".$arrayInstruction[19]."</td><td>".$arrayInstruction[20]."</td><td>".$arrayInstruction[21]."</td><td>".$arrayInstruction[22]."</td><td>".$arrayInstruction[23]."</td><tr>";		
						}
						// -- one sms/email with notification..
						if($anySuccess == 'success')
							{
								$smstemplatename = 'batch_debitorder_uploaded';
								$applicationid = 0;		
								$Action_Date = $arrayInstruction[15];			
								$Reference_1 = $arrayInstruction[2];					
								// -- Sent to the uploader.
								auto_email_collection($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								
								// -- Sent to info@ecashmeup.com
								auto_email_collection('8707135963082',$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								
								// -- notify clients who signed for notifications(queue)
								if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
							}
					echo "</table>
				</div>
			</div>";				
				}
			}
				//echo nl2br(file_get_contents($filenameEncryp)); // get the contents, and echo it out.
			?>
		</form>
</div>
</div>

<!-- </body>
</html> -->