<?php 
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = 'SELECT * FROM accounttype';
$dataaccounttype = $pdo->query($sql);

$sql = 'SELECT * FROM bankbranch';
$databankbranch = $pdo->query($sql);


$Account_TypeArr = array();
foreach($dataaccounttype as $row)
{
	$Account_TypeArr[] = $row['accounttypeid'].'|'.$row['accounttypedesc'];
}

$Branch_CodeArr = array();
foreach($databankbranch as $row)
{
	$Branch_CodeArr[] = $row['branchcode'].'|'.$row['bankid'];
}

$NotifyArr[] = 'None';					 
$NotifyArr[] = 'SMS';
$NotifyArr[] = 'EMAIL';

$userid = '';
$UserCode = '';
$IntUserCode = '';

// -- Sessions
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}

	// -- User Code.
	if(getsession('UserCode'))
	{
		$UserCode = getsession('UserCode');
	}

	// -- Int User Code.
	if(getsession('IntUserCode'))
	{
		$IntUserCode = getsession('IntUserCode');
	}

$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	
	if(empty($IntUserCode))
	{
		$IntUserCode = $params[0];
	}
	
	if(empty($UserCode))
	{
		$UserCode = $params[1];
	}
	
	if(empty($userid))
	{
		$userid = $params[2];
	}
}
$bid = '';
// -- echo $IntUserCode." - ".$UserCode." - ".$userid ;
?>
<style>
.box
{
  width:800px;
  margin:0 auto;
}
.active_tab1
{
  background-color:#fff:
  color:#333;
  font-weight: 600;
}
.inactive_tab1
{
  background-color:#f5f5f5:
  color:#333;
  cursor: not-allowed;
}
.has-error
{
  border-color:#cc0000:
  background-color:#ffff99:
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.inactiveLink {
   pointer-events: none;
   cursor: default;
}

</style>
<div class="container background-white bottom-border">
     <br />	
	 <br />
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step col-xs-4">
            <a id="step1" href="#step-a" type="button" class="btn btn-primary btn-circle inactiveLink">1</a>
            <p><strong>Beneficiary Details</strong></p>
        </div>
        <div class="stepwizard-step col-xs-4">
            <a href="#step-b" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">2</a>
            <p><strong>Review</strong></p>
        </div>
        <div class="stepwizard-step col-xs-4">
            <a href="#step-c" type="button" class="btn btn-default btn-circle inactiveLink" disabled="disabled">3</a>
            <p><strong>Complete</strong></p>
        </div> 
    </div>
</div>

 <form id="contact-form" method="POST" action="#" style="display:block" role="form">
    <div class="row setup-content" id="step-a">
         <div class="controls">
           <div class="container-fluid" style="border:1px solid #ccc ">
             </br>
             <div class="row">
			   <div class="control-group">
				<div class="col-md-3" style="display:block">
                     <div class="form-group">
                         <label for="form_Initials"><strong>Initials:</strong><span style="color:red">*</span></label>
                         <input required id="Initials" type="text" name="Initials" class="form-control" placeholder="Enter Benefiary Initials"  maxlength="3" data-error="Initials is required." value = "">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>	
				 <div class="col-md-3" style="display:block">
                     <div class="form-group">
                         <label for="form_Initials"><strong>UserCode:</strong></label>
                         <input required id="benefiaryusercode" type="text" name="benefiaryusercode" class="form-control" placeholder="Enter Benefiary UserCode"  maxlength="4" value = "">
                     </div>
                 </div>	
                 <div class="col-md-6" style="display:none">
                     <div class="form-group">
                         <input id="User_Code" type="hidden" name="User_Code" class="form-control" value="<?php $_SESSION['UserCode']; ?>" placeholder="" required="required" data-error="Bank Reference is required.">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>				 
             </div>
			 </div>
            <!-- </br>-->
             <div class="row">
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Holder"><strong>Account Holder Name:</strong><span style="color:red">*</span></label>
                         <input id="Account_Holder" type="text" name="Account_Holder" class="form-control" placeholder="Enter Benefiary Name" required="required" maxlength="30" data-error="Account Holder is required." value = "" />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Type:"><strong>Bank Account Type:</strong><span style="color:red">*</span></label>
                         <select class="form-control" id="Account_Type" name="Account_Type">
						  <?php 
						  	$Account_TypeSelect = $Account_Type;
							$rowData = null;
						  foreach($Account_TypeArr as $row)
						  {
							  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							  if($rowData[0] == $Account_TypeSelect)
							  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							  else
							  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
						  }?>
                        </select>
                        <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				 </div>
		<div class="row">		 
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Account_Number"><strong>Bank Account Number:</strong><span style="color:red">*</span></label>
                         <input id="Account_Number" type="number" name="Account_Number" class="form-control" placeholder="Enter Benefiary Account" required="required" data-error="Account Number is required." pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==13) return false;" value = "">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Branch_Code"><strong>Bank Name:</strong><span style="color:red">*</span></label>
                         <!--<input id="Branch_Code" type="text" name="Branch_Code" class="form-control" placeholder="" required="required" data-error="Branch Code is required.">-->
                         <select class="form-control" id="Branch_Code" name="Branch_Code">
						 <?php 
						  	$Branch_CodeSelect = $Branch_Code;
							$rowData = null;
							foreach($Branch_CodeArr as $row)
							{
								  $rowData = explode(utf8_encode('|'), $row);  //for UTF-8
								  if($rowData[0] == $Branch_CodeSelect)
								  {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
								  else
								  {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							}?>
                        </select>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div></div>
		<div class="row">		 
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Reference_1"><strong>My Reference:</strong><span style="color:red">*</span></label>
                         <input id="Reference_1" type="text" name="Reference_1" class="form-control" placeholder="Enter Your Reference" required="optional" data-error="Reference 1 is required." maxlength="9" value ="" />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>

				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Bank_Reference"><strong>Benefiary Reference:</strong><span style="color:red">*</span></label>
                         <input id="Bank_Reference" type="text" name="Bank_Reference" class="form-control" placeholder="Enter Benefiary Reference" required="required" data-error="Bank Reference is required." maxlength="9"  value = "" />
                         <div class="help-block with-errors"></div>
                     </div>
                 </div></div>
		<div class="row">		 
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="Notify:"><strong>Notice of payment:</strong></label>
                         <select class="form-control" id="Notify" name="Notify" onchange="toogleEmailNumber()" >
  						  <?php
						  	$NotifySelect = $Notify;

						  foreach($NotifyArr as $row)
						  {
							  if($row == $NotifySelect)
							  {echo  '<option value="'.$row.'" selected>'.$row.'</option>';}
							  else
							  {echo  '<option value="'.$row.'">'.$row.'</option>';}
						  }
						  ?>                         
                        </select>
                        <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
				  <div class="col-md-3">
                     <div class="form-group">
                         <label for="contact_number"><strong>Contact Number/Email</strong></label>
                         <input id="contact_number" type="number" data-error="Contact Number is required." required="required" placeholder="Enter Mobile Number/Email" name="contact_number" class="form-control" value = "" 
						 pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==10) return false;"/>
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>				 
             </div>
           </div>
		</div>	
           </br>
		   <div >
		    <button class="btn btn-primary nextBtn btn-lg pull-right"  type="button" >Next</button>
		  </div>
		</div>  

			<div class="row setup-content" id="step-b">	
				<?php include $filerootpath.'/confirm_paysinglebenefiary.php'; ?>
				<br/>				
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Confirm</button>		   
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" >Back</button>
		   </div>
		   <div class="row setup-content" id="step-c">	
		   <div class="controls">
			<div class="container-fluid" style="border:1px solid #ccc ">
			<div class="row">
			<div class="control-group">
			  <div class="col-md-6">
			    <div class="messages">
				  <p id="message_info" name="message_info" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
			  </div>
			</div>
			</div>
           <div class="row">
			<div class="control-group">
			  <div class="col-md-6">
				  <button class="btn btn-primary" type="button" id="addbeneficiary" onclick="toStep1()"><strong>Add another Beneficiary</strong></button>								  
			  </div>
			</div>
			</div>
			<br/>
			</div></div>	
			 <br/>			
			 <a href="<?php if(empty($param)){ echo "paysinglebenefiary";}else{ echo "javascript:clickTab('paybeneficiary');";}?>" class="btn btn-primary nextBtn btn-lg pull-right" id="PayNow" type="button" >PayNow</a>		   
			 <button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
			 <a href="index" class="btn btn-primary  btn-lg pull-right" id="Exit" type="button" >Exit</a>
			 <button class="btn btn-primary btn-lg pull-right"  id="backtoconfirm"  onclick="toStep1()" type="button" >Edit</button>			 
		   </div>
		   <input style="display:none" type="text" id="bid" name="bid" value="<?php echo $bid; ?>">
     </form>
</div>
<script>
/*
https://bbbootstrap.com/snippets/multi-step-form-wizard-30467045
https://bootsnipp.com/snippets/yaZa
*/
function valueselect(sel) 
 {
		var Service_Mode = sel.options[sel.selectedIndex].value;      
		var selectedService_Type = document.getElementById('Service_Type').value;
		document.getElementById('Service_Mode').value = Service_Mode;
		//alert(Service_Mode);
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectServiceTypes.php",
				       data:{Service_Mode:Service_Mode,Service_Type:selectedService_Type},
						success: function(data)
						 {	
						// alert(data);
							var book = data.split('|');;//JSON.parse(data);
							
							jQuery("#Service_Type").html(book[0]);
							jQuery("#entry_class").html(book[1]);
						 }
					});
				});				  
			  return false;
  }
// -- 
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allpreviousBtn = $('.previousBtn');
			
		
		allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
 allpreviousBtn.click(function(){
	 var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
			isValid = true;

	 if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
	 
 });
 
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
			curInputsNum = curStep.find("input[type='number'],input[type='url']"),
			curInputsDate = curStep.find("input[type='date'],input[type='url']"),
			curInputsEmail = curStep.find("input[type='email'],input[type='url']"),
            isValid = true;

			if(curStepBtn == 'step-b')
			{
				call_script_add_beneficiary();
			}
			// alert('Test0 - '+isValid);

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
		//alert('Test1 - '+isValid);

	   // -- Validate contact number		
		validate_contactnumber();
		
		// -- Required number.
		for(var i=0; i<curInputsNum.length; i++){
            if (!curInputsNum[i].validity.valid){
                isValid = false;
                $(curInputsNum[i]).closest(".form-group").addClass("has-error");
            }
        }
		
			   // -- Validate contact Email Pattern		
		validate_contactnumber();
		
		// -- Required Email.
		for(var i=0; i<curInputsEmail.length; i++){
            if (!curInputsEmail[i].validity.valid){
                isValid = false;
                $(curInputsEmail[i]).closest(".form-group").addClass("has-error");
            }
        }
		
		
		//alert('curInputsDate = '+curInputsDate.length);
		// alert('Test2 - '+isValid);

		// -- Required Date.
		for(var i=0; i<curInputsDate.length; i++){
			//alert(curInputsDate[i].validity.valid);
            if (!curInputsDate[i].validity.valid){
                isValid = false;
				curInputsDate[i].required = true;
				 curInputsDate[i].focus(); 
                $(curInputsDate[i]).closest(".form-group").addClass("has-error");
            }
        }
		add_confirm_benefiary();
		//alert('Test1 - '+isValid);
		document.getElementById('message_info').innerHTML = '';		
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
		
		
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
	
	// -- Next Step Validated
	navListItems.click( function(e) 
	{	
  var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

		$(".form-group").removeClass("has-error");
		//alert(curStepBtn);
        for(var i=0; i<curInputs.length; i++){
			
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
		
		                isValid = false;

		//alert('Test2 - '+nextStepWizard.attr("id"));
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
		
		return false; 
	} );
});

 function toStep1()
 {
	  document.getElementById("step1").click();
 }
function validate_contactnumber()
{
		//alert('Notify :'+document.getElementById('Notify').value);
		if(document.getElementById('Notify').value === 'SMS')
		{
			document.getElementById('contact_number').required = true;//setAttribute('required','required');
		}
		else if(document.getElementById('Notify').value === 'EMAIL')
		{
			document.getElementById('contact_number').required = true;//setAttribute('required','required');
		}
		else
		{
			document.getElementById('contact_number').required = false;
		}
		//alert('Value :'+document.getElementById('contact_number').value);
}

function add_confirm_benefiary()
{	
	// -- confirmation.
	document.getElementById('beneficiaryLbl').innerHTML = document.getElementById('Account_Holder').value+'-'+document.getElementById('Account_Number').value;							
	document.getElementById('InitialsLbl').innerHTML = document.getElementById('Initials').value;						
	document.getElementById('Account_HolderLbl').innerHTML = document.getElementById('Account_Holder').value;			
	document.getElementById('Account_NumberLbl').innerHTML = document.getElementById('Account_Number').value;			
	var Account_Type = document.getElementById('Account_Type').value;	
	var Account_TypeDesc = '';
	var branchcode = document.getElementById('Branch_Code').value;
	var bankname = '';
	Branch_CodeArr  = <?php echo json_encode($Branch_CodeArr);?>;
	Arr = <?php echo json_encode($Account_TypeArr);?>;
	for (var key in Arr)
	{ 
	   row = new Array();
	   row = Arr[key].split('|');
	   if(Account_Type == row[0])
	   {
		   Account_TypeDesc = row[1];
	   }
	}
	
	for (var key in Branch_CodeArr)
	{ 
	   row = new Array();
	   row = Branch_CodeArr[key].split('|');
	   if(branchcode == row[0])
	   {
		   bankname = row[1];
	   }
	}
	document.getElementById('Account_TypeLbl').innerHTML = Account_TypeDesc;			
	document.getElementById('Branch_CodeLbl').innerHTML = bankname;			
	document.getElementById('Reference_1Lbl').innerHTML = document.getElementById('Reference_1').value;
	document.getElementById('Bank_ReferenceLbl').innerHTML = document.getElementById('Bank_Reference').value;
	document.getElementById('NotifyLbl').innerHTML = document.getElementById('Notify').value;			
	document.getElementById('contact_numberLbl').innerHTML = document.getElementById('contact_number').value;	
	document.getElementById('benefiaryusercodeLbl').innerHTML = document.getElementById('benefiaryusercode').value;			
}	

function toogleEmailNumber()
{
  contact_number = document.getElementById("contact_number");
  if(document.getElementById('Notify').value === 'SMS')
  {
	 contact_number.outerHTML = 
	 '<input id="contact_number" type="number" data-error="Contact Number is required." required="required" placeholder="Enter Mobile Number" name="contact_number" class="form-control" value = "" pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==10) return false;"/>';
  }
  else if(document.getElementById('Notify').value === 'EMAIL')
  {
	 contact_number.outerHTML = 
	 '<input id="contact_number" type="email" data-error="Contact Email is required." placeholder="Enter Email" name="contact_number" class="form-control"   value = "" />';	  
  }
  else
  {
	 contact_number.outerHTML = 
	 '<input id="contact_number" type="text" name="contact_number" class="form-control" value = ""  />';	  	  
  }
}

function call_script_add_beneficiary()
{
	var userid = <?php echo "'".$userid."'"; ?>;

	var IntUserCode = <?php echo "'".$IntUserCode."'"; ?>;
	

	var UserCode = <?php echo "'".$UserCode."'"; ?>;
	
	var contact_number = '';
	var contact_email  = '';
	
	var benefiaryusercode = document.getElementById('benefiaryusercode').value;
	if(document.getElementById('Notify').value === 'SMS')
	{
		contact_number = document.getElementById('contact_number').value;
	}
	else if(document.getElementById('Notify').value === 'EMAIL')
	{
		contact_email = document.getElementById('contact_number').value;
	}
	//alert(userid+' - '+IntUserCode);
			var list =					
					{'Initials': document.getElementById('Initials').value,
'Account_Holder': document.getElementById('Account_Holder').value,
'Account_Type': document.getElementById('Account_Type').value,
'Account_Number': document.getElementById('Account_Number').value,
'Bank_Reference': document.getElementById('Bank_Reference').value,
'Client_Reference_1': document.getElementById('Reference_1').value,
'Notify': document.getElementById('Notify').value,
'benefiarycontactnumber': contact_number,
'benefiaryemail': contact_email,
'createdby' : userid,
'IntUserCode': IntUserCode,
'UserCode' : UserCode,
'Branch_Code':document.getElementById('Branch_Code').value,
'benefiaryusercode':benefiaryusercode};
					//alert(document.getElementById('Initials').value);
				var feedback = '';
		jQuery(document).ready(function()
		{
		jQuery.ajax({  type: "POST",
					   url:"script_addbenefiary.php",
				       data:{list:list},
						success: function(data)
						 {
							feedback = data;
							//alert(data);
							// -- Updating the table mappings
							var message_info = document.getElementById('message_info');								
							
PayNow = document.getElementById("PayNow");
Exit = document.getElementById("Exit");
backtoconfirm = document.getElementById("backtoconfirm");
addbeneficiary = document.getElementById("addbeneficiary"); 
							if(feedback.includes("success"))
							{
								message_info.innerHTML = 'Beneficiary added successfully.';
								message_info.className  = 'status';	
								PayNow.style.visibility = '';
								Exit.style.visibility = '';
								backtoconfirm.style = "display:none";
								addbeneficiary.style.visibility = '';
								var res = feedback.split("|");
								document.getElementById('bid').value = res[1];
							}
							else
							{
								message_info.innerHTML = feedback;
								message_info.className  = 'alert alert-error';
								PayNow.style.visibility = 'hidden';	
								Exit.style.visibility = 'hidden';
								backtoconfirm.style = "display:block";								
								addbeneficiary.style.visibility = 'hidden';								
							}

						 }
					});
				});
				
}
</script>