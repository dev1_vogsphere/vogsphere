<?php 
require 'database.php';
$accounttypedescError = null;
$accounttypedesc = '';
$count = 0;
$accounttypeid = 0;

if (isset($_GET['accounttypeid']) ) 
{ 
$accounttypeid = (int) $_GET['accounttypeid']; 
}

if (!empty($_POST)) 
{   $count = 0;
	$accounttypedesc = $_POST['accounttypedesc'];
	$valid = true;
	
	if (empty($accounttypedesc)) { $accounttypedescError = 'Please enter Account Type Description.'; $valid = false;}
		
		if ($valid) 
		{	
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE accounttype SET accounttypedesc = ? WHERE accounttypeid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($accounttypedesc,$accounttypeid));
			Database::disconnect();
			$count = $count + 1;
		}
}
else
{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from accounttype where accounttypeid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($accounttypeid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$accounttypedesc = $data['accounttypedesc'];
}

?>
<div class="container background-white bottom-border">   
	<div class='margin-vert-30'>
        <div class='col-md-6 col-md-offset-3 col-sm-offset-3'>
		<form action='' method='POST' class="signup-page"> 
		<div class="panel-heading">
				<h2 class="text-center">
                         Change Account Type
                </h2>
<?php # error messages
		if (isset($message)) 
		{
			foreach ($message as $msg) 
			{
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
		printf("<p class='status'>Account Type successfully updated.</p>");
		}
?>
				
</div>				

			<p><b>Account Type</b><br />
			<input style="height:30px" type='text' name='accounttypedesc' value="<?php echo !empty($accounttypedesc)?$accounttypedesc:'';?>"/>
			<?php if (!empty($accounttypedescError)): ?>
			<span class="help-inline"><?php echo $accounttypedescError;?></span>
			<?php endif; ?>
			</p> 
		<table>
		<tr>
			<td><div>
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn-primary" href="accounttype">Back</a>
				</div></td>
		</tr>
		</table>	
		</form> 
	</div>
</div>
</div>
