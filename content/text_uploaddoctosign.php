<?php
	$path = "esignature/";
	//require $path.'xpdl.php';
	require "signature.php";

	$message = null;	
	$location = 'index';
	// -- Read SESSION userid.
	$userid = null;
	// -- Logged User Details.
	if(getsession('username'))
	{
		$userid = getsession('username');
	}
	// -- if not logged in go back.
	if ( null==$userid ) 
	{
		if (!headers_sent()) 
		{
			header("Location: ".$location);
		}
	}
$valid_formats = array("pdf");
$max_file_size = 1024*10000; //10MB
$path = $path."files/"; // Upload directory

$count = 0;
if ( !empty($_POST)) 
{
	 $message = null;
		print_r($message);
}

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{		
// Loop $_FILES to execute all files
	$filecount = count($_FILES['files']['name']);
	if($filecount == 1)
	{
	foreach ($_FILES['files']['name'] as $f => $name) 
	{     
	    if ($_FILES['files']['error'][$f] == 4) {
	        continue; // Skip file if any error found
	    }	       
	    if ($_FILES['files']['error'][$f] == 0) {	           
	        if ($_FILES['files']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
						// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			
			$ext = pathinfo($name, PATHINFO_EXTENSION);

			// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;
			
			/*if($_POST['documents'] = $leads)
			{
			 $FILELEADS = $name;
			}*/
			
		// -- Update File Name Directory	
	            if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
				{
	            	$count++; // Number of successfully uploaded files
	            }				
		// -- BOC Encrypt the uploaded PDF with ID as password.
					$file_parts = pathinfo($name);
					$filenameEncryp = $path.$name;
					switch($file_parts['extension'])
					{
						case "pdf":
						{
							

						break;
						}
						case "PDF":
						{
							/* -- TCPDF library (search for installation path).
							require_once('tcpdf_include.php');
							require_once "FPDI/fpdi.php";
							$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF
							// -- Page Config
								
							// -- Page Config	
							$pdf->setSourceFile($filenameEncryp);
							$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);*/
							
						break;
						}
					}
	        }
	    }
	}
}
else
{
	$message[] = "Only one file can be uploaded at a time.";
}	
}
?>

<!--<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Multiple File Upload with PHP - Demo</title> -->
<div class="container background-white bottom-border">	
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] 
{
	cursor:pointer;
	width:100%;
	border:none;
	background:#006dcc;
	background-image:linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-moz-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	background-image:-webkit-linear-gradient(bottom, #006dcc 0%, ##006dcc 52%);
	color:#FFF;
	font-weight: normal;
	margin: 0px;
	padding: 4px 12px; 
	border-radius:0px;
}
input[type="submit"]:hover 
{
	background-image:linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-moz-linear-gradient(bottom, #04c 0%, #04c 52%);
	background-image:-webkit-linear-gradient(bottom, #04c 0%, #04c 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>

<!--</head>
<body> -->
	<div class="wrap">
	 <h1>Upload PDF Document for e-Signature</h1>
		<?php
		# error messages
		if (isset($message)) {
			foreach ($message as $msg) {
				printf("<p class='status'>%s</p></ br>\n", $msg);
			}
		}
		# success message
		if($count !=0)
		{
			printf("<p class='status'>%d files added successfully!</p>\n", $count);
		}
		?>
				<!-- Multiple file upload html form-->
		<form action="" method="post" enctype="multipart/form-data">
		<p>Max file size 10Mb, Valid formats pdf</p>
		<br />
		<br />
		<br />
			<input type="file" name="files[]" multiple="multiple" accept=".pdf">
			<table class ="table table-user-information">
				  <tr>
					<td>
						<div>
							<input type="submit" class="btn btn-primary" value="Upload">
						</div>
					</td>
				<td>
				<div>
				<?php echo '<a class="btn btn-primary" href="'.$location.'">Back</a>'; ?></div></td>
				</tr>
			</table>		
			<?php if(isset($_POST))
			{				
				if(!empty($filenameEncryp))
				{ 
				// -- Upload document for e-Signature.
				$dataObj['userid'] = $userid;
				$dataObj['url']    = './'.$filenameEncryp;
				$dataInsert[] = $filenameEncryp;
				$dataInsert[] = 'Document';
				$dataInsert[] = $dataObj['userid'];
				$dataInsert[] = ''; //-- e-signature file attached to a document)
				$dataInsert[] = 'pdf';
				$dataInsert[] = date('Y-m-d');
				$dataInsert[] = date('h:m:s');
				$dataInsert[] = $dataObj['userid'];
				$dataInsert[] = 'NEED TO SIGN';
				$dataInsert[] = $dataObj['url'];
				db_insert_signed_doc($dataInsert);
				echo review_uploaded_doc($dataObj);
				}
			}
			?>
		</form>
</div>
</div>

<!-- </body>
</html> -->