<?php
// -- BOC 2017.06.24
// -- To avoid : Fatal error: Cannot redeclare class.
// -- EOC 2017.06.24
$pdo = null;
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Account/Reference must be the 9 digits derived from the ID number.
// -- Reading from the ID backward.  
if (!function_exists('GenerateAccountNumber')) 
{
function GenerateAccountNumber($customerid)
{
   $AccountNumber = '';
   
   if(!empty($customerid))
   {
     $AccountNumber = strrev($customerid);
	 
	 // -- 9 digits.
	 $AccountNumber = substr($AccountNumber,0,9); 
   }
   return $AccountNumber;
}
}

if (!function_exists('createcollections')) 
{
	function createcollections($arrayCollections)
	{
	try 
	  {
		Global $pdo;  
		$sql = "INSERT INTO collection
		(Client_ID,
		Client_Reference_1,
		Client_Reference_2,
		Client_ID_Type,
		Client_ID_No,
		Initials,
		Account_Holder,
		Account_Type,
		Account_Number,
		Branch_Code,
		No_Payments,
		Service_Type,
		Service_Mode,
		Frequency,
		Action_Date,
		Bank_Reference,
		Amount,
		Status_Code,
		Status_Description,
		mandateid,
		mandateitem)
		VALUES
		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$q = $pdo->prepare($sql);
		$q->execute(array($arrayCollections[1],
		$arrayCollections[2],
		$arrayCollections[3],
		$arrayCollections[4],
		$arrayCollections[5],
		$arrayCollections[6],
		$arrayCollections[7],
		$arrayCollections[8],
		$arrayCollections[9],
		$arrayCollections[10],
		$arrayCollections[11],
		$arrayCollections[12],
		$arrayCollections[13],
		$arrayCollections[14],
		$arrayCollections[15],
		$arrayCollections[16],
		$arrayCollections[17],
		$arrayCollections[18],
		$arrayCollections[19],
		$arrayCollections[20],
		$arrayCollections[21]));
		Database::disconnect();
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}	
}

if (!function_exists('updatecollections')) 
{
	function updatecollections($arrayCollections)
	{
	 try 
	  {
		Global $pdo;
		$sql = "UPDATE collection
		SET
		Client_ID = ?,
		Client_Reference_1 = ?,
		Client_Reference_2 = ?,
		Client_ID_Type = ?,
		Client_ID_No = ?,
		Initials = ?,
		Account_Holder = ?,
		Account_Type = ?,
		Account_Number = ?,
		Branch_Code = ?,
		No_Payments = ?,
		Service_Type = ?,
		Service_Mode = ?,
		Frequency = ?,
		Action_Date = ?,
		Bank_Reference = ?,
		Amount = ?,
		Status_Code = ?,
		Status_Description = ?,
		mandateid = ?,
		mandateitem = ?
		WHERE collectionid = ?;";
		$q = $pdo->prepare($sql);
		
		$q->execute(array(
		$arrayCollections[2],
		$arrayCollections[3],
		$arrayCollections[4],
		$arrayCollections[5],
		$arrayCollections[6],
		$arrayCollections[7],
		$arrayCollections[8],
		$arrayCollections[9],
		$arrayCollections[10],
		$arrayCollections[11],
		$arrayCollections[12],
		$arrayCollections[13],
		$arrayCollections[14],
		$arrayCollections[15],
		$arrayCollections[16],
		$arrayCollections[17],
		$arrayCollections[18],
		$arrayCollections[19],
		$arrayCollections[20],
		$arrayCollections[21],
		$arrayCollections[22],
		$arrayCollections[1]));
		Database::disconnect();
		} 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('deletecollections')) 
{
	function deletecollections($collectionid)
	{
	  try 
	  {	
	  	Global $pdo;
		$sql = "DELETE FROM collection
		WHERE collectionid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($collectionid));
		Database::disconnect();
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readcollections')) 
{
	function readcollections()
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description FROM Collection ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute();		
		$data = $q->fetchAll();
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readcollection')) 
{
	function readcollection($collectionid)
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description FROM Collection WHERE collectionid = ? ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute(array($collectionid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readPaymentschedule')) 
{
	function readPaymentschedule($customerid,$applicationid,$item)
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description,mandateid,mandateitem FROM Collection WHERE Client_ID = ? AND mandateid = ? AND mandateitem = ? ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$applicationid,$item));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('GenerateAccountNumber')) 
{
	// -- Account/Reference must be the 9 digits derived from the ID number.
	// -- Reading from the ID backward.  
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';
	   
	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);
		 
		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9); 
	   }
	   return $AccountNumber;
	}
}

if (!function_exists('GetPaymentSchedule')) 
{
	function GetPaymentSchedule($ApplicationId)
	{
	  try
	  {	
		Global $pdo;
		$tbl_name ="paymentschedule"; 
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_name where ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($ApplicationId));		
		$data = $q->fetchAll();
		
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
?>