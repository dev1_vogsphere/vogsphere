<?php
date_default_timezone_set('Africa/Johannesburg');
//print('test');
// * To change this template, choose Tools | Templates
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
//============================================================+
// 	SEND AN eInvoice to client(s) -
//============================================================+
// ------------ Start Send an Email ----------------//
if(!function_exists('SendEmail_effms'))
{

function SendEmail_effms($att)
{


		require_once('class/class.phpmailer.php');
		try
		{
					// -- Email Server Hosting -- //
						$Globalmailhost = $_SESSION['Globalmailhost'];
						$Globalport = $_SESSION['Globalport'];
						$Globalmailusername = $_SESSION['Globalmailusername'];
						$Globalmailpassword = $_SESSION['Globalmailpassword'];
						$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
						$Globaldev = $_SESSION['Globaldev'];
						$Globalprod = $_SESSION['Globalprod'];
						$adminemail = $_SESSION['Globalemail'];
						$companyname = $_SESSION['GlobalCompany'];
						// -- Email Server Hosting -- //

					/*	Production - Live System */
						if($Globalprod == 'p')
						{
							$mail=new PHPMailer();
							$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
							$mail->SetFrom($adminemail,$companyname);
							$mail->AddReplyTo($adminemail,$companyname);
						}
					/*	Development - Testing System */
						else
						{
								$mail=new PHPMailer();
								$mail->IsSMTP();
								$mail->SMTPDebug=1;
								$mail->SMTPAuth=true;
								$mail->SMTPSecure= $GlobalSMTPSecure;//"tls";//
								$mail->Host=$Globalmailhost;//"smtp.vogsphere.co.za";//
								$mail->Port= $Globalport;//587;//
								$mail->Username=$Globalmailusername;//"vogspesw";//
								$mail->Password=$Globalmailpassword;//"fCAnzmz9";//
						}
								$dateForm = date('F')."  ".date('Y');

								$mail->SetFrom($emailComp,$companyname);

								$mail->AddStringAttachment($att, 'Policy Schedule.pdf');

								//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
								$mail->Subject=  $companyname." Policy Schedule";//
								// Enable output buffering
								$_SESSION['invDateForm'] = $dateForm;

								ob_start();
								include 'content/text_emailInvoice.php';//execute the file as php
								$body = ob_get_clean();
								$fullname = $FirstName.' '.$LastName;
								$email = $_SESSION['invEmail'];

								// print $body;
								$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
								$mail->AddAddress($member_Email, $fullname);
								$mail->AddBCC($emailComp, $companyname);


								if($mail->Send())
								{
						$successMessage = "An e-mail has been sent to ".$member_Email;
						// -- Somehow the code below stops the buffer and returns called jQuery  code page, on the eStatement.php.
						// -- Mystery code. This code is mysterous do not understand at this point.25.06.2017
						//echo "<script>alert('".$successMessage."')</script>";
							include 'success.php';//execute the file as php

								}
								else
								{
								  // echo 'mail not sent';
								$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";

								}
								
			}
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}
}}
//-------------------------------------------------------------------
//							GET_premium
//-------------------------------------------------------------------
if(!function_exists('GET_premium'))
{
function GET_premium($data)
{
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM premium where PackageOption_id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($data['PackageOption_id']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
	//print_r($principalData);
	return $principalData;
}}

//-------------------------------------------------------------------
//							GET_MEMBER_PACKAGE_POLICY
//-------------------------------------------------------------------
if(!function_exists('GET_MEMBER_PACKAGE_POLICY'))
{
function GET_MEMBER_PACKAGE_POLICY($data)
{
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM policy as p join packageoption as po on p.PackageOption_id = po.PackageOption_id where p.Policy_id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($data['Policy_id']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
	//print_r($principalData);
	return $principalData;
}}
//-------------------------------------------------------------------
//							get_debicheck_data
//-------------------------------------------------------------------
if(!function_exists('get_debicheck_data'))
{
function get_debicheck_data($data)
{
	$principalData = null;
	//if(isset($data['Member_Policy_id']))
	{
	$pdo_collection = Database::connectDB();			
	$pdo_collection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM mandates where reference = ?";
	$q = $pdo_collection->prepare($sql);
	$q->execute(array($data['Member_Policy_id']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
	//print_r($data['Member_Policy_id']);
	}
	return $principalData;
}}
//-------------------------------------------------------------------
//							GET_PRINCIPAL member ADRESS DATA
//-------------------------------------------------------------------
if(!function_exists('get_main_member_address'))
{
function get_main_member_address($data)
{
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM address where Address_id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($data['Address_id']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
	return $principalData;
}}

//-------------------------------------------------------------------
//							GET_PRINCIPAL member DATA
//-------------------------------------------------------------------
if(!function_exists('get_main_member_data'))
{
function get_main_member_data($data)
{
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM member as m join member_policy as mp on m.Member_id = mp.Member_id where m.Member_id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($data['customerid']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
	return $principalData;
}}

if(!function_exists('get_principal_data'))
{
function get_principal_data($data)
{
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM member where Member_id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($data['customerid']));
	$principalData = $q->fetch(PDO::FETCH_ASSOC);
		
	// -- Principal member...
	$tablePrincipalData = '';
	if(empty($principalData))
	{
		$tablePrincipalData = '<tr><td></td><td></td><td></td></tr>';   
	}
	else
	{
		$tablePrincipalData = '<tr><td>'.$principalData['First_name'].' '.$principalData['Surname'].'</td><td>'.$principalData['Member_id'].'</td><td>'.$data['premium'].'</td></tr>';   
	}
	$tablePrincipalMember = 
	'<table class="table table-striped">
	<tbody>
	<tr><th colspan="3">PRINCIPAL MEMBER</th></tr>
	<br>
	<tr class="table-dark">
	<th>Surname and Name</th>	
	<th>ID No</th>	
	<th>Premium</th>					
	</tr>'.  
	 $tablePrincipalData
	.'</tbody>
	</table>';
  return $tablePrincipalMember;
}}
//-------------------------------------------------------------------
//							GET_SPOUSE member DATA
//-------------------------------------------------------------------

if(!function_exists('get_spouse_data'))
{
function get_spouse_data($principalData)
{
	// -- Spouse member...
		$today = date('Y-m-d');
		$pdo_ffms = db_connect::db_connection();			
		$sql = "SELECT * FROM spouse where spouse_id = ?";
		$q = $pdo_ffms->prepare($sql);
		$q->execute(array($principalData['Spouse_id']));
		$SpouseData = $q->fetch(PDO::FETCH_ASSOC);		
		
		$tableSpouseData = '';
		$tableSpouseMember = '';
		if(!empty($SpouseData['spouse_id']))
		{
			$tableSpouseData = '<tr><td>'.$SpouseData['spousefirst_name'].$SpouseData['spousesurname'].'</td><td>'.$SpouseData['spouse_id'].'</td></tr>';   
		
		$tableSpouseMember = 
		'<table class="table table-striped" width="100%" id="spouse" name="spouse">
		<tbody>
		<br>
		<br>
		<tr><th colspan="3">SPOUSE</th></tr>
		<br>
		<tr class="table-dark">
		<th>Surname and Name</th>	
		<th>ID No</th>							
		</tr>'.$tableSpouseData.'</tbody>
		</table>';	
		}
  return $tableSpouseMember;
}}
//-------------------------------------------------------------------
//							GET_Dependent DATA
//-------------------------------------------------------------------
if(!function_exists('get_dependent_3_data'))
{
function get_dependent_3_data($data)  
{
// -- Dependent Members...
	    $today = date('Y-m-d');
		$pdo_ffms = db_connect::db_connection();			
		$sql = "SELECT * FROM extendend_member where Main_member_id = ".$data['customerid']." AND Ext_memberType_id = '3'";
		$q = $pdo_ffms->prepare($sql);
		$ChildrenData = $pdo_ffms->query($sql);	
		
		$tableChildrenData = '';
		$tableChildren = '';

		foreach($ChildrenData as $row)
		{
			$tableChildrenData = $tableChildrenData .'<tr><td>'.$row['Ext_member_name'].' '.$row['Ext_member_surname'].'</td><td>'.$row['Ext_member_id'].'</td><td>'.$row['Remarks'].'</td></tr>';   	
		}
		
		if(!empty($tableChildrenData))
		{
			$tableChildren = '<table class="table table-striped" width="100%" id="Children" name="Children">
							<tbody>
							<br>
							<br>
							<tr><th colspan="3">DEPENDANT</th></tr>
							<br>
							<tr>
								<th>Surname and Name</th>	
								<th>ID No</th>		
								<th>Benefits</th>								
							</tr>'.$tableChildrenData.'    
							</tbody>
						</table>';
		}				
		return $tableChildren;
}}
//-------------------------------------------------------------------
//							GET_Beneficiary DATA
//-------------------------------------------------------------------
if(!function_exists('get_beneficiary_2_data'))
{
function get_beneficiary_2_data($data)
{

	// -- Beneficiary member...
	    $today = date('Y-m-d');
		$pdo_ffms = db_connect::db_connection();			
		$sql = "SELECT * FROM extendend_member where Main_member_id = ".$data['customerid']." AND Ext_memberType_id = '2'";
		$q = $pdo_ffms->prepare($sql);
		$BeneficiariesData = $pdo_ffms->query($sql);	

		$tableBeneficiariesData = '';
		$tableBeneficiaries = '';
		
		foreach($BeneficiariesData as $row)
		{
			$tableBeneficiariesData = $tableBeneficiariesData .'<tr><td>'.$row['Ext_member_name'].' '.$row['Ext_member_surname'].'</td><td>'.$row['Ext_member_id'].'</td><td>'.$row['Percentage'].'</td></tr>';   	
		}
		
		if(!empty($tableBeneficiariesData))
		{	
			$tableBeneficiaries = '<table class="table table-striped" width="100%" id="benefiary" name="benefiary">
							<tbody>
							<br>
							<br>
							<tr><th colspan="2">NOMINATED BENEFIACIARY</th></tr>
							<br>
							<tr class="table-dark">
								<th>Surname and Name</th>	
								<th>ID No</th>	
								<th>Percentage</th>
							</tr>'.$tableBeneficiariesData.'    
							</tbody>
						</table>';
		}				
		return $tableBeneficiaries;
}}
//-------------------------------------------------------------------
//							GET_extended family member DATA
//-------------------------------------------------------------------
if(!function_exists('get_extended_members_4_data'))
{
function get_extended_members_4_data($data)
{
	// -- Extended member...
	    $today = date('Y-m-d');
		$pdo_ffms = db_connect::db_connection();			
		$sql = "SELECT * FROM extendend_member where Main_member_id = ".$data['customerid']." AND Ext_memberType_id = '4'";
		$q = $pdo_ffms->prepare($sql);
		$ExtendedFamilyData = $pdo_ffms->query($sql);	
		
		$tableExtendedFamily = '';
		$tableExtendedFamilyMembers = '';
		

			foreach($ExtendedFamilyData as $row)
			{
				$tableExtendedFamily = $tableExtendedFamily .'<tr><td>'.$row['Ext_member_name'].' '.$row['Ext_member_surname'].'</td><td>'.$row['Ext_member_id'].'</td><td>'.$row['Remarks'].'</td></tr>';   	
			}
			
		if(!empty($tableExtendedFamily))
		{
		 $tableExtendedFamilyMembers = '<table class="table table-striped" width="100%" id="Extended" name="Extended">
							<tbody>
							<br>
							<br>
							<tr><th colspan="5">EXTENDED FAMILY</th></tr>
							<br>
							<tr class="table-dark">
								<th>Surname and Name</th>	
								<th>ID No</th>								
								<th>Benefits</th>									
							</tr>'.$tableExtendedFamily.'    
							</tbody>
						</table>';
		}	
		return $tableExtendedFamilyMembers;
}}

//-------------------------------------------------------------------
//							eInvoice
//-------------------------------------------------------------------
$tableDetails  = '';
//-------------------------------------------------------------------
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
$filerootpath = filerootpath();
require $filerootpath.'/database.php';
require  $filerootpath.'/database_ffms.php';
//-------------------------------------------------------------------
//           				DECLARATIONS			    			-
//-------------------------------------------------------------------	
	$customerid = null;
	$ApplicationId = null;
	$email = '';
	$dataUser = null;
	$credit = 0.00; // -- Total Amount paid
	$newcharges = 0.00; // -- Interest amount
	$installment = 0.00;
	$street = '';//$dataComp['street'];
	$suburb = '';//$dataComp['suburb'];
	$city = '';//$dataComp['city'];
	$State = '';//$dataComp['province'];
	$PostCode = '';//$dataComp['postcode'];
	$registrationnumber = '';//$dataComp['registrationnumber'];

	$Company  = '';
	$phone    = '';
	$fax      = '';
	$emailComp    = '';
	$website  = '';
	$logo     = '';
	$logoTemp = '';
	$currency = '';
	$legalterms = '';
	$principalData = null;		
//-------------------------------------------------------------------
//           				INPUT PARAMETERS		    			-
//-------------------------------------------------------------------
// -- Check if variables are already set $a is set
	if (!isset($emailFlag)) {$emailFlag = '';}
	if (!isset($tenantid)) {$tenantid = '';}
	if (!isset($customerid)) {$customerid = '';}
// ================================ send an e-mail ==================
if ( !empty($_GET['email']))
	{
		$emailFlag = $_REQUEST['email'];
	}
	else
	{
	// -- Send Multiple Emails
	 if(isset($_POST['email']))
	 {
	    $emailFlag = $_POST['email'];
	 }
	}
// ================================ End Send an e-mail ===============

// ================================ BOC - tenantid =========================
//$tenantid = 'UFRF';
if ( !empty($_GET['tenantid']))
	{
		$tenantid = $_REQUEST['tenantid'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['tenantid']))
	 {
	    $tenantid = $_POST['tenantid'];
	 }
	}
	//echo $tenantid;
// ================================ EOC - tenantid =========================
//$customerid = '84080355450866'; //'620103554586';//
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		/*if(isurlencode($customerid))
		{
			$customerid = base64_decode(urldecode($customerid));
		}*/
	}
	else
	{
		if(isset($_POST['customerid']))
		  {
		  	$customerid = $_POST['customerid'];
			/*if(isurlencode($customerid))
			{
				$customerid = base64_decode(urldecode($customerid));
			}*/
		  }
	}
//-------------------------------------------------------------------
//           				Declarations				    		-
//-------------------------------------------------------------------
	$dataMember = null;
	$dataMemberPolicy = null;
	$SpouseData = null;
	$ChildrenData = null;
	$tablePrincipalMember = '';
	
	if ( null==$customerid )
	    {
			header("Location: custAuthenticate.php");
		}
	 else {
//-------------------------------------------------------------------
//           				Principal member DATA		    		-
//-------------------------------------------------------------------
		$input['customerid']       = $customerid;
		$main_member_data          = get_main_member_data($input);
		$dataPackage 		       = GET_MEMBER_PACKAGE_POLICY($main_member_data);
		$input['Member_Policy_id'] = $main_member_data['Member_Policy_id'];
		$addressMember             = get_main_member_address($main_member_data);
		$input['member_province']  = $addressMember ['Line_2'];
		$input['member_debitdate'] = get_debicheck_data($input)['collection_date_text'];
		$input['Benefits']          = 'R'.number_format($dataPackage['Benefits'],2);
		$input['Benefits_Descriptions'] =$dataPackage['Benefits_Descriptions'];
		$input['PackageOption_id']  = $dataPackage['PackageOption_id'];
		$dataPremium 		        = GET_premium($input);
		$input['premium']	        = 'R'.number_format($dataPremium['Premium_amount'],2); 
		$tablePrincipalMember       = get_principal_data($input);
		$member_name     = ucfirst(strtolower($main_member_data['First_name'])).' '.ucfirst(strtolower($main_member_data['Surname']));
		$member_province = $input['member_province'];
		$member_ContactNo = $addressMember['Cellphone'];
		$member_Email = $addressMember['Member_email'];
		$member_Joined_Date = strtotime($main_member_data['createdon']);
		$member_Joined_Date_converted=date('Y-m-d',$member_Joined_Date);
		$member_policy= $input['Member_Policy_id'];
		
		
	
//-------------------------------------------------------------------
//           				Spouse member DATA		    			-
//-------------------------------------------------------------------			
		//--$input['Spouse_id'] = $customerid;
		$tableSpouseMember  = get_spouse_data($main_member_data);
//-------------------------------------------------------------------
//           				Children DATA(Extended_TypeId = '3')    -
//-------------------------------------------------------------------		
		$tableChildren = get_dependent_3_data($input);
//-------------------------------------------------------------------
//           				Extended Family (Extended_TypeId = '4') -
//-------------------------------------------------------------------		
		$tableExtendedFamilyMembers = get_extended_members_4_data($input);
//-------------------------------------------------------------------
//           				Beneficiaries (Extended_TypeId = '2') -
//-------------------------------------------------------------------
		$tableBeneficiaries = get_beneficiary_2_data($input);
	}
//-------------------------------------------------------------------
//           				COMPANY DATA-tenant							-
//-------------------------------------------------------------------
	$pdo_ffms = db_connect::db_connection();			
	$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Select Address & Company Details.
	$sql = "select * from Seq_Generator_fin where Client_Id = ?";
	$q = $pdo_ffms->prepare($sql);
	$q->execute(array($tenantid));
	$dataComp = $q->fetch(PDO::FETCH_ASSOC);
	db_connect::disconnect();

	// -- Update Address & Company details.
	if(!empty($dataComp['Client_Id']))
	{
		// -- Company details from Form.
		$logo     = $dataComp['logo'];
		$arr = explode("/", $logo);
		$logo = $arr[1];

		$Company  = $dataComp['Org_Name'];
		$phone    = $dataComp['ffms_phone'];
		$fax      = '';//$dataComp['fax'];
		$emailComp    = $dataComp['ffms_email'];
		$website  = '';//$dataComp['website'];
		$logoTemp = $logo;
		$currency = '';//$dataComp['currency'];
		$legalterms = '';//$dataComp['legaltext'];
		$input['Org_Name'] = $Company;
		
		$street = '';//$dataComp['street'];
		$suburb = '';//$dataComp['suburb'];
		$city = '';//$dataComp['city'];
		$State = '';//$dataComp['province'];
		$PostCode = '';//$dataComp['postcode'];
		$registrationnumber = '';//$dataComp['registrationnumber'];

		$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';
	}
	
//-------------------------------------------------------------------
//           				COMPANY DATA-ecash							-
//-------------------------------------------------------------------
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$dataCompCash = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();

	// -- Update Address & Company details.
	if($dataCompCash['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		/*$Company  = $dataComp['name'];
		$phone    = $dataComp['phone'];
		$fax      = $dataComp['fax'];
		$emailComp    = $dataComp['email'];
		$website  = $dataComp['website'];
		$logo     = $dataComp['logo'];
		$logoTemp = $logo;
		$currency = $dataComp['currency'];
		$legalterms = $dataComp['legaltext'];*/
	}
//echo '$logo='.$logo;
$currency = 'R';
//-------------------------------------------------------------------
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
// Logo are update on page tcpdf_autoconfig.php (Under root directory)
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once "FPDI/fpdi.php";

$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$companyname = $Company;//"Vogsphere Pty Ltd";
$companyStreet = $street;//"43 Mulder Street";
$companySuburb = $suburb;//"The reeds";
$companyPostCode = $PostCode;//"0157";
$companyCity = $city;//"Centurion";
//$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';
$companyphone =$fax;

$address = $companyname.','.$companyStreet.','.$companySuburb.','.$companyPostCode.','.$companyCity.','.$companyphone.','.$emailComp;
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($companyname);
$pdf->SetTitle($companyname);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject("$companyname - Policy Schedule");
$pdf->SetKeywords('Invoice, Policy Schedule');
//PDF_HEADER_TITLE = 'Modise';
//\t\t\t\t\t
$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

$pdf_title = $gtab.$companyname.' - Policy Schedule';

$variables = array(
    '{im_title}' => ucfirst(strtolower($main_member_data['Title'])),
    '{im_surname}' => ucfirst(strtolower($main_member_data['Surname'])),
	'{company_name}'=> $companyname,
	'{benefits}'=>$input['Benefits'],
	'{benefits_descriptions}'=>$input['Benefits_Descriptions'],
	
	);

/*define ('PDF_HEADER_STRING2',$tab.$companyname."\n"
							 .$tab.$companyStreet."\n"
							 .$tab.$companySuburb."\n"
							 .$tab.$companyPostCode."\n"
							 .$tab.$companyCity."\n"
							 .$tab.$companyphone."\n"
							 .$tab.$emailComp);*/

define ('PDF_HEADER_LOGO_CUSTOM', $logo);
define ('PDF_HEADER_LOGO_WIDTH_CUSTOM', 90);

// set default header data PDF_HEADER_STRING // -- array(0,64,255) rgb color:
//$pdf->SetHeaderData(PDF_HEADER_LOGO_CUSTOM, PDF_HEADER_LOGO_WIDTH_CUSTOM, $pdf_title, PDF_HEADER_STRING2, array(0,0,0), array(0,0,0));
$pdf->SetHeaderData(PDF_HEADER_LOGO_CUSTOM, PDF_HEADER_LOGO_WIDTH_CUSTOM, $pdf_title, array(0,0,0), array(0,0,0));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('times', '', 12, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


// --------------- Company Details -------------------------------------------------
$tableCompanyDetails = '';

// -- Loan Insurance or Protection Plan.
//$ProtectionPlan = $currency.''.number_format(GetProtectionPlan(),2);
$today = date('Y-m-d');
$tableDetailsHeader = '<p>'.strtr($dataComp['header'], $variables).'</p>';		
//echo strtr($dataComp['header'], $variables);
// Display replaced string
$tableDetailsHeader = str_replace("{member_policyid}", $input['Member_Policy_id'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{member_province}", $input['member_province'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{Org_Name}", $input['Org_Name'], $tableDetailsHeader);

$tableDetailsHeader = str_replace("{member_premium}", $input['premium'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{member_debitdate}", $input['member_debitdate'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{member_benefits}", $input['Benefits'], $tableDetailsHeader);

$tableDetailsHeader = str_replace("{ffms_phone}", $dataComp['ffms_phone'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{ffms_ems}", $dataComp['ffms_ems'], $tableDetailsHeader);
$tableDetailsHeader = str_replace("{ffms_email}", $dataComp['ffms_email'], $tableDetailsHeader);


$tableDetailsFooter = '<p>'.$dataComp['footer'].'</p>';	
$tableDetailsFooter = str_replace("{ffms_phone}", $dataComp['ffms_phone'], $tableDetailsFooter);
$tableDetailsFooter = str_replace("{ffms_ems}", $dataComp['ffms_ems'], $tableDetailsFooter);
$tableDetailsFooter = str_replace("{ffms_email}", $dataComp['ffms_email'], $tableDetailsFooter);
$tableDetailsFooter = str_replace("{member_benefits}", $input['Benefits'], $tableDetailsFooter);
$tableDetailsFooter = str_replace("{benefits_descriptions}", $input['Benefits_Descriptions'], $tableDetailsFooter);

// ---------------------------------------------------------------------------------
$credit = $currency.number_format($credit,2);
$newcharges = number_format($newcharges,2);
if (empty($contents))
{	
$html = <<<EOD
<style>
</style>
<br/>
<br/>
<table>
   <!--<tr><th bgcolor="#337ab7" color="White" width="40%">Client</th><th bgcolor="#337ab7" color="White" width="60%" align="right">Statement Summary</th></tr>-->
   <tr><td align="left">Date:</td><td width="40%" align="left">$today</td></tr>
   <tr><td align="left">Joined Date:</td><td width="40%" left="right">$member_Joined_Date_converted</td></tr>
   <tr><td align="left">Name:</td><td width="40%" align="left">$member_name</td></tr>
   <tr><td>ID no:</td><td width="40%" align="left">$customerid</td></tr>
   <tr><td>Policy no:</td><td width="40%" align="left">$member_policy</td></tr>
   <tr><td>Contact no</td><td width="40%" align="left">$member_ContactNo</td></tr>
   <tr><td>Email</td><td width="%40" align="left">$member_Email</td></tr>
   <tr><td></td><td></td><td></td></tr>
</table>
<style>
	th{ background-color:#337ab7;color:White;}
</style>
	$tableDetailsHeader
	$tablePrincipalMember
	$tableSpouseMember 
	$tableChildren
	$tableExtendedFamilyMembers
	$tableBeneficiaries
	$tableDetailsFooter 
EOD;
}
else
{
	$html = $contents;
}

// =========================================== Store Values for e-mail eStatement =======
	/*					$space = '';
						$_SESSION['invTitle'] 		  = $Title;
						$_SESSION['invFirstName']     = $FirstName;
						$_SESSION['invLastName']      = $LastName;
						$_SESSION['invidnumber']      = $idnumber;
						$_SESSION['invAccountNumber'] = $ApplicationId;
						$_SESSION['invInvoiceNumber'] = $ApplicationId;
						$_SESSION['invDueDate'] 	  = $PaymentDueDate;
						$_SESSION['invAmountDue'] 	  = $tot_outstanding;
						$_SESSION['invCurrency'] 	  = $currency;

						$_SESSION['GlobalCompany']  = '';//$dataComp['name'];
						$_SESSION['Globalphone']    = '';//$dataComp['phone'];
						$_SESSION['Globalfax']      = '';//$dataComp['fax'];
						$_SESSION['Globalwebsite']  = '';//$dataComp['website'];
						$_SESSION['Globallogo']     = '';//$dataComp['logo'];
						$_SESSION['GloballogoTemp'] = '';//$dataComp['logo'];
						$_SESSION['Globalcurrency'] = '';//$dataComp['currency'];
						$_SESSION['Globallegalterms'] = '';//$dataComp['legaltext'];
						$_SESSION['Globalstreet'] = '';//$dataComp['street'];
						$_SESSION['Globalsuburb'] = '';//$dataComp['suburb'];
						$_SESSION['Globalcity'] = '';//$dataComp['city'];
						$_SESSION['Globalstate'] = '';//$dataComp['province'];
						$_SESSION['Globalpostcode'] = '';//$dataComp['postcode'];
						$_SESSION['Globalregistrationnumber'] = '';//$dataComp['registrationnumber'];
*/
						// -- Email Server Hosting
						$_SESSION['Globalmailhost']  	= $dataCompCash['mailhost'];
						$_SESSION['Globalport'] 		= $dataCompCash['port'];
						$_SESSION['Globalmailusername'] = $dataCompCash['username'];
						$_SESSION['Globalmailpassword'] = $dataCompCash['password'];
						$_SESSION['GlobalSMTPSecure']	= $dataCompCash['SMTPSecure'];
						$_SESSION['Globaldev'] 		    = $dataCompCash['dev'];
						$_SESSION['Globalprod']         = $dataCompCash['prod'];
						$_SESSION['Globalemail']        = $dataComp['ffms_email'];//$dataComp['email'];
						$_SESSION['GlobalCompany']      = $Company;//$dataComp['name'];	
						$_SESSION['invEmail'] 		    = $addressMember['Member_email'];
						$_SESSION['Globallogo']         = $dataComp['logo'];
						$_SESSION['Globalphone']        = $dataComp['ffms_phone'];
						
					// -- EOC Master Session Values --- //
					
// =========================================== ECO Store Values ==========================

// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //

// Set some content to print
// $tot_outstanding = number_format($tot_outstanding,2);
// $credit = number_format($credit,2);
// $newcharges = number_format($newcharges,2);

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";
$eStatement = 'eStatement_'.$ApplicationId;
// Close and output PDF document
// This method has several options, check the source code documentation for more information.

$fileName = $eStatement.'.pdf';
$_SESSION['invFileName'] = $fileName;

//$emailFlag = 'N';
if(empty($emailFlag))
{
$pdf->Output($fileName, 'I');
//echo $html;
}
else // Send an e-mail
{
 $fileatt = $pdf->Output($fileName, 'S');
 SendEmail_effms($fileatt);
}

/*
// -- To avoid : Redeclaring trim.
//if (!function_exists('trim_input'))
//{
	// -- Trim Input
	function trim_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);

	  return $data;
	}
//}

// -- BOC T.M Modise, 18.06.2017
// -- Get the loan protection plan ---- //
// -- Which is the insurance amount fee multiply with the loan duration. -- //
function GetProtectionPlan()
{
$protectionplan = 0.00;
$fee = 0.00;

// -- Database Connections.
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

global $LoanDuration;
global $customerid;
global $ApplicationId;

$charge = 4;

$sql =  "select * from loanappcharges where ApplicationId = ? AND chargeid = ?";
$q = $pdo->prepare($sql);
$q->execute(array($ApplicationId,$charge));
$dataChargeAdded = $q->fetchAll(PDO::FETCH_ASSOC);

// -- if the Insurance Charges has been added.
if(!empty($dataChargeAdded))
{
// -- Get the insurance charge amount.
	$sql =  "select * from charge where chargeid = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($charge));
	$dataCharge = $q->fetch	(PDO::FETCH_ASSOC);

	$fee = $dataCharge['amount'];
// -- Amount * Duration = Protection Plan.
	if($fee > 0)
	{
	  if($LoanDuration > 0)
	  {
	    $protectionplan = $fee * $LoanDuration;
	  }
	}
}

return $protectionplan;
}

// -- Account/Reference must be the 9 digits derived from the ID number.
// -- Reading from the ID backward.
function GenerateAccountNumber($customerid)
{
   $AccountNumber = '';

   if(!empty($customerid))
   {
     $AccountNumber = strrev($customerid);

	 // -- 9 digits.
	 $AccountNumber = substr($AccountNumber,0,9);
   }
   return $AccountNumber;
}
// -- EOC T.M Modise, 18.06.2017

// -- BOC Check if parameters are Encoded & Decoded. 17.09.2017.
function isurlencode($param)
{
	//if ( urlencode(base64_encode(base64_decode(urldecode($param)))) === $param)
	if (preg_match("@^[a-zA-Z0-9%+-_]*$@", $param)) //{ // variable is urlencoded. }
	{
		return true;
	}
	else
	{
		return false;
	}
}
// -- EOC Check if parameters are Encoded & Decoded. 17.09.2017.
*/
//============================================================+
// END OF FILE
//============================================================+
?>
