<?php 
// Begin of -- Debit Order Schedule.
$tbl_name ="paymentschedule"; 
$HTML = "";
$scheduleddate = null;

$ReferenceNumber = null;
$AccountHolder	 = null;
$BranchCode	     = null;
$AccountNumber	 = null;
$Servicetype	 = null;
$ServiceMode	 = null;
$AccountType	 = null;
$displayData     = null;
$ApplicationId = null;
$CustomerId = null;

// -- 1.ReferenceNumber.
if(isset($_POST['ReferenceNumber']))
{
	$ReferenceNumber = $_POST['ReferenceNumber'];
}

// -- 2.AccountHolder.
if(isset($_POST['AccountHolder']))
{
	$AccountHolder = $_POST['AccountHolder'];
}
	
// -- 3.BranchCode.
if(isset($_POST['BranchCode']))
{
	$BranchCode = $_POST['BranchCode'];
}
	
// -- 4.AccountNumber.
if(isset($_POST['AccountNumber']))
{
	$AccountNumber = $_POST['AccountNumber'];
}
	
// -- 5.Servicetype.
if(isset($_POST['Servicetype']))
{
	$Servicetype = $_POST['Servicetype'];
}
	
// -- 6.ServiceMode.
if(isset($_POST['ServiceMode']))
{
	$ServiceMode = $_POST['ServiceMode'];
}
	
// -- 7.AccountType.
if(isset($_POST['AccountType']))
{
	$AccountType = $_POST['AccountType'];
}

// -- 8.CustomerId.
if(isset($_POST['CustomerId']))
{
	$CustomerId = $_POST['CustomerId'];
	$CustomerId = base64_decode(urldecode($CustomerId)); 
}

// -- 9.ApplicationId.
if(isset($_POST['ApplicationId']))
{
	$ApplicationId = $_POST['ApplicationId'];
	$ApplicationId = base64_decode(urldecode($ApplicationId)); 
	echo '$ApplicationId ='.$ApplicationId;
}

// -- Different file names.
$base = '';
$checkDB = 'database.php';
$checkDB2 = 'database_invoice2';
$DoNotDB = '';
$DoNotDB2 = '';
$fileNames = '';

// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
if (!empty($_POST)) 
{   

$included_files = get_included_files();
//require_once 'database_invoice2.php';

foreach ($included_files as $filename) 
{

    if($filename == $checkDB)
	{
	  $DoNotDB = 'yes';
	}
	
	if($filename == $checkDB2)
	{
	 $DoNotDB2 = 'yes';
	}
	
	$fileNames = $fileNames.$filename."\n"; 
}

if(empty($DoNotDB))
{require 'database.php';}		
$HTML = $HTML.'<div style="overflow-y:auto;" style="z-index:10000;">';
$HTML = $HTML.'<table class="table table-striped table-bordered" style="white-space: nowrap;">';

$HTML = $HTML.'<thead>';
$HTML = $HTML.'<tr>
				   <th>Reference</th>
				   <th>Account Holder</th>
				   <th>Branch Code</th>
				   <th>Account Number</th>
				   <th>Service Type</th>
				   <th>Service Mode</th>
				   <th>Amount</th>
				   <th>Action Date</th>
				   <th>Status Code</th>
				   <th>Description</th> -->
				</tr></thead><tbody>';

$displayData = GetPaymentSchedule($ApplicationId);

foreach ($displayData as $row) 							
{
 $scheduleddate = $row['scheduleddate'];
 $amountdue 	= $row['amountdue'];	
 
 $HTML = $HTML."
				<tr>
					<td>$ReferenceNumber</td>
					<td>$AccountHolder</td>
					<td>$BranchCode</td>
					<td>$AccountNumber</td>
					<td>$Servicetype</td>
					<td>$ServiceMode</td>
					<td>$Displayinterestdue</td>
					<td>$scheduleddate</td>
					<td></td>
					<td>Pending</td>
				</tr>";
}
$HTML = $HTML.'</tbody></table></div>';
}
echo $HTML;

// End of -- Debit Order Schedule.

?>