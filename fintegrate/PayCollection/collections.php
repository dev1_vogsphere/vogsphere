<?php
//$connect = mysqli_connect("localhost","root","root","PayCollections");
$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
//$query ="SELECT  FROM Collection ORDER BY First_Action_Date DESC";
$query ="SELECT Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description FROM ecashpdq_paycollections.collection ORDER BY Action_Date DESC";
$result = mysqli_query($connect,$query);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Collection</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
</head>

<body>
  <br/><br />
  <br/><br />
  <div class="container">

  <div class="table-responsive">
  <table id="collections" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Reference</th>
                <th>Account Holder</th>
                <th>Branch Code</th>
                <th>Account Number</th>
                <th>Service Type</th>
                <th>Service Mode</th>
                <th>Amount</th>
                <th>Action Date</th>
                <th>Status Code</th>
                <th>Description</th>
            </tr>
        </thead>

        <?php
          while($row = mysqli_fetch_array($result))
          {
            echo '
            <tr>
              <td>'.$row["Client_Reference_1"].'</td>
              <td>'.$row["Account_Holder"].'</td>
              <td>'.$row["Branch_Code"].'</td>
              <td>'.$row["Account_Number"].'</td>
              <td>'.$row["Service_Type"].'</td>
              <td>'.$row["Service_Mode"].'</td>
              <td>'.$row["Amount"].'</td>
              <td>'.$row["Action_Date"].'</td>
              <td>'.$row["Status_Code"].'</td>
              <td>'.$row["Status_Description"].'</td>

            </tr>
            ';
          }
        ?>

    </table>
      </div>
</div>
</body>

</html>

<script>
$(document).ready(function(){
$('#collections').DataTable();

});
</script>
