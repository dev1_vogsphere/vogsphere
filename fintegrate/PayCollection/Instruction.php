<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Collection</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <link href="https://.bootstrapcdn.com//3.3.6/css/.min.css" rel="stylesheet"/>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <style>
    .box
    {
      width:800px;
      margin:0 auto;
    }
    .active_tab1
    {
      background-color:#fff:
      color:#333;
      font-weight: 600;
    }
    .inactive_tab1
    {
      background-color:#f5f5f5:
      color:#333;
      cursor: not-allowed;
    }
    .has-error
    {
      border-color:#cc0000:
      background-color:#ffff99:
    }
    </style>
</head>

<body>
  <br/><br />
  <br/><br />
  <div class="container box"
       <br />
       <form id="contact-form" method="POST" onsubmit="return test();" role="form">
           <div class="messages"></div>
           <div class="controls">

             <!--Contract Details-->
             <div class="container-fluid" style="border:1px solid #ccc ">
               </br>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Reference_1">Reference 1: *</label>
                           <input id="Reference_1" type="text" name="Reference_1" class="form-control" placeholder="" required="optional" data-error="Reference 1 is required." maxlength="9">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Reference_2">Client Reference 2: *</label>
                           <input id="Reference_2" type="text" name="Reference_2" class="form-control" placeholder="" required="required" data-error="Reference 2 is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
               </div>
             </div>
             </br>
             <!--Client Indetification-->
             <div class="container-fluid" style="border:1px solid #ccc ">
               </br>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Client_ID_Type">Client ID Type: *</label>
                           <input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Client_ID_No">Client Identification Number: *</label>
                           <input id="Client_ID_No" type="text" name="Client_ID_No" class="form-control" placeholder="" required="required" data-error="Client ID number is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
               </div>
             </div>
            </br>
             <!--Bank Account Details-->
             <div class="container-fluid" style="border:1px solid #ccc ">
               </br>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Initials">Initials: *</label>
                           <input id="Initials" type="text" name="Initials" class="form-control" placeholder="" required="required" data-error="Initials is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Account_Holder">Account_Holder *</label>
                           <input id="Account_Holder" type="text" name="Account_Holder" class="form-control" placeholder="" required="required" data-error="Account Holder is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Account_Type:">Account Type:*</label>
                           <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Account_Number">Account Number:*</label>
                           <input id="Account_Number" type="text" name="Account_Number" class="form-control" placeholder="" required="required" data-error="Account Number is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Branch_Code">Branch Code:*</label>
                           <input id="Branch_Code" type="text" name="Branch_Code" class="form-control" placeholder="" required="required" data-error="Branch Code is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
               </div>
             </div>
             </br>
             <!--Collection Generation Details-->
             <div class="container-fluid" style="border:1px solid #ccc ">
               </br>
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_No_Payments">Number of Payments:*</label>
                           <input id="No_Payments" type="text" name="No_Payments" class="form-control" placeholder="" required="required" data-error="Number of Payments is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Frequency">Frequency:*</label>
                           <input id="Frequency" type="text" name="Frequency" class="form-control" placeholder="" required="required" data-error="Frequency is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Amount">Amount *</label>
                           <input id="Amount" type="text" name="Amount" class="form-control" placeholder="" required="required" data-error="Amount is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Service_Type">Service Type:*</label>
                           <input id="Service_Type" type="text" name="Service_Type" class="form-control" placeholder="" required="required" data-error="Service Type is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Action_Date">Action Date:*</label>
                           <input id="Action_Date" type="text" name="Action_Date" class="form-control" placeholder="" required="required" data-error="Action_Date is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Service_Mode">Service Mode:*</label>
                           <input id="Service_Mode" type="text" name="Service_Mode" class="form-control" placeholder="" required="required" data-error="Service Mode is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="form_Bank_Reference">Bank Reference*</label>
                           <input id="Bank_Reference" type="text" name="Bank_Reference" class="form-control" placeholder="" required="required" data-error="Bank Reference is required.">
                           <div class="help-block with-errors"></div>
                       </div>
                   </div>
               </div>
             </div>

              <!--Generate Paymentbuttons-->
               <div class="row">
               </br></br>
                   <div class="col-md-6">
                         <input type="submit" class="btn btn-success btn-send" value="Generate Payments">
                   </div>
               </div>

           </div>
       </form>

  </div>

</body>

</html>

<script type="text/javascript">
function test(){
  var Reference_1= document.getElementById("Reference_1").value;
  var Reference_2= document.getElementById("Reference_2").value;
  var Client_ID_Type=document.getElementById("Client_ID_Type").value;
  var Client_ID_No= document.getElementById("Client_ID_No").value;
  var Initials=document.getElementById("Initials").value;
  var Account_Holder= document.getElementById("Account_Holder").value;
  var Account_Type=document.getElementById("Account_Type").value;
  var Account_Number= document.getElementById("Account_Number").value;
  var Branch_Code= document.getElementById("Branch_Code").value;
  var No_Payments=document.getElementById("No_Payments").value;
  var Service_Type=document.getElementById("Service_Type").value;
  var Service_Mode=document.getElementById("Service_Mode").value;
  var Frequency=document.getElementById("Frequency").value;
  var Action_Date=document.getElementById("Action_Date").value;
  var Bank_Reference=document.getElementById("Bank_Reference").value;
  var Amount=document.getElementById("Amount").value;


  alert("test");
  $.ajax({
    url:"action_page.php",
    data:{
    Reference_1:Reference_1,
    Reference_2:Reference_2,
    Client_ID_Type:Client_ID_Type,
    Client_ID_No:Client_ID_No,
    Initials:Initials,
    Account_Holder:Account_Holder,
    Account_Type:Account_Type,
    Account_Number:Account_Number,
    Branch_Code:Branch_Code,
    No_Payments:No_Payments,
    Service_Type:Service_Type,
    Service_Mode:Service_Mode,
    Frequency:Frequency,
    Action_Date:Action_Date,
    Bank_Reference:Bank_Reference,
    Amount:Amount
},
    success:function(result)
  {
     alert(result);
  }
})
}
</script>
