<?php
// -- BOC 2017.06.24
// -- To avoid : Fatal error: Cannot redeclare class.
// -- EOC 2017.06.24
if (!class_exists('Database')) 
{
class Database 
{
	private static $dbName = 'ecashpdq_paycollections' ; 
	private static $dbHost = 'localhost' ;	
	private static $dbUsername = 'ecashpdq_root';
	private static $dbUserPassword = 'nsa{VeIsl)+h';

	
	private static $cont  = null;
	public static $conn;
	
	public function __construct() 
	{
		exit('Init function is not allowed');
	}
	
	/*	public static function connectDB()
		{
			self::$conn = mysql_connect("localhost", self::$dbUsername, self::$dbUserPassword) or die(mysql_error());
			return self::$conn;
		}
	*/
	public static function connect()
	{
	   // One connection through whole application
       if ( null == self::$cont )
       {      
        try 
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);  
        }
        catch(PDOException $e) 
        {
          die($e->getMessage());  
        }
       } 
       return self::$cont;
	}
	
	public static function disconnect()
	{
		self::$cont = null;
	}
	//  -- BOC encrypt password - 16.09.2017.	
	public static function encryptPassword($password)
	{
			/**
			 * In this case, we want to increase the default cost for BCRYPT to 12.
			 * Note that we also switched to BCRYPT, which will always be 60 characters.
			 */
			$options = ['cost' => 12,];
			return password_hash($password, PASSWORD_BCRYPT, $options);
	}
	// 	-- EOC encrypt password - 16.09.2017.
}

// -- Hash Every Page.
function redirect($url) 
{
    ob_start();
	$hash="?guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}

// -- Hash to Page with parameters.
function redirectParam($url) 
{
    ob_start();
	$hash="&guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}
// -- Contains Characters
// returns true if $needle is a substring of $haystack
function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}

// -- Redirect to Login Page with not logged in.

// -- Generate Captcha Image
function captchImage($value)
{	
	 $generateImage = '';
	 if (!empty($_POST)) 
	 {
	 if (isset($_POST['login']))
	   {$generateImage = 'No';}
   
     if (isset($_POST['Apply']))
		{$generateImage = 'No';}
	}

	 
	 if( $generateImage == '')
	 {
	$string = '';
	for ($i = 0; $i < 5; $i++) {
		$string .= chr(rand(97, 122));
	}

	$_SESSION['captcha'] = $string; //store the captcha
	$_SESSION['value'] = $value;
	$dir = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]).'/fonts/';//"REQUEST_URI-PHP_SELF-SERVER_NAME
	$image = imagecreatetruecolor(165, 50); //custom image size
	$font = "PlAGuEdEaTH.ttf"; // custom font style
	$color = imagecolorallocate($image, 113, 193, 217); // custom color
	$white = imagecolorallocate($image, 255, 255, 255); // custom background color
	imagefilledrectangle($image,0,0,399,99,$white);
	imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);
//echo $dir.$font;
	// Enable output buffering
	ob_start();
	imagepng($image);
	// Capture the output
	$imagedata = ob_get_contents();
	// Clear the output bufferx
	ob_end_clean();
	$_SESSION['image'] = $imagedata;
	}
	else
	{
	$imagedata = $_SESSION['image'];
	}
	$random = chr(rand(97, 122));
	
	//echo "<h2>Generate Image - $generateImage - $random</h2>";

	return $imagedata;
}

// -- Disbursement Fee,Insurance Fee.
function FeeContract($AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if (empty($dataDisbursementFee))
		{ 
		// -- Return the Fee.
		  		  return 0.00;		
		}
		else
		{
			return $dataDisbursementFee['amount'];
		}
}
// -- Percentage Fee like VAT 15%
function FeePercentageBasedContract($AppID,$chargeid, $Item,$amountdue)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 
		$percentage = 0.00;
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if (empty($dataDisbursementFee))
		{ 
		// -- Return the Fee.
		  		  return 0.00;		
		}
		else
		{
			$percentage = $dataDisbursementFee['percentage'];
			// -- convert to percentage amount to calculate with amount due
			if($amountdue > 0)
			{
				if($percentage > 0)
				{
					$percentage = $percentage / 100;
					$amountdue = ($amountdue * $percentage);
				}
				else
				{
					$amountdue = 0.00;
				}
			}
			
			return $amountdue;
		}
}

// BOC --- Get MutipleCharge ID - Fees; 01.05.2018.
// -- Ge the Fees IDs.
function GetContractFeesID($loanDate,$ChargeType,$chargeoptionid)
{
   // ----- Global - Payment Allocated Charges, description ----------------- //
	$indexInside = 0;
	$dataCharges = '';
	$ArrayCharges = null;

	// -- Database Connections.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	// --------------------- Charges ------------------- //
	$sql = 'SELECT * FROM charge WHERE chargetypeid = ? AND fromdate <= ?  AND todate >= ? AND chargeoptionid = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($ChargeType,$loanDate ,$loanDate,$chargeoptionid));
	$dataCharges = $q->fetchAll(PDO::FETCH_ASSOC);

	//echo 'charge type:'.$ChargeType.';Loan Date:'.$loanDate;
	if(empty($dataCharges))
	{
		return $ArrayCharges;
	}
	else
	{
	
		foreach($dataCharges as $rowCharge)
		{
			  $ArrayCharges [$indexInside] = $rowCharge['chargeid'];
			  $indexInside = $indexInside + 1;
		}	
				return $ArrayCharges;
	}	
}

// -- Get Fee Fixed Amount.
function GetFeeFixedAmount($chargetypeid,$applicationid,$item,$scheduleddate,$chargeoptionid)
{
	$chargeidArray = GetContractFeesID($scheduleddate,$chargetypeid,$chargeoptionid);
	$IndexStart = 0;
	$fees = 0.00;
	if(empty($chargeidArray))
	{
		//echo 'empty';
	}
	else
	{	
		// -- Go through all row per chargeID.
		$ArrayLength = count($chargeidArray);
		$chargeidArrayLength = count($chargeidArray);
		$chargeidArrayGlobal = $chargeidArray;
		// -- Get all charges & amount per chargeID.
		for($j=$IndexStart;$IndexStart<$ArrayLength;$IndexStart++)
		{
				$chargeid = $chargeidArray[$IndexStart];
				$fees = $fees + FeeContract($applicationid,$chargeid,$item);
		}
	}
	
	return $fees;
}
// EOC --- Get MutipleCharge ID - Fees; 01.05.2018.


}
?>