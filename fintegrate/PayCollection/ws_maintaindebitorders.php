<?php
/* require the user as the parameter 
PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json

GET ALL the Debit Orders
04.11.2017
Parameter : status =
APPR - which means Approval Loans
SET  - Which means Settled Loans
CAN  - which means Cancelled Loans
REJ  - Which means Rejected Loans

format = 
xml  - Which means xml format.
json - Which means json format. 
*/
require 'database.php';
require 'functions.php';

$found 	= false;
$UseApplicationID = false;
$xml 	= "";
$ReferenceNumber = "";
$ApplicationId = null;
$operationId = null;
$format = "";
$arrayDebitOrderSchedule = null;

// -- Query with ReferenceNumber
if ( !empty($_GET['ReferenceNumber'])) 
{
		$ReferenceNumber = $_GET['ReferenceNumber'];
		$found = true;
}

// -- Query with ApplicationId.
if ( !empty($_GET['ApplicationId'])) 
{
		$ApplicationId = $_GET['ApplicationId'];
		$UseApplicationID = true;
}

// -- action with operationId.
if ( !empty($_GET['operationId'])) 
{
		$operationId = $_GET['operationId'];
}

// -- Maintain Debit Orders.
if ( !empty($_GET['arrayDebitOrderSchedule'])) 
{
	$str = $_GET["arrayDebitOrderSchedule"];
	// -- Remove the first and last character.
	$str = substr($str, 1);    // -- Remove First
	$str = substr($str, 0, -1);  // Reference Last.
	$jsonStr  = json_decode(($str),true);
	$arrayDebitOrderSchedule = $jsonStr;

//print_r($arrayDebitOrderSchedule);

	//echo $str;
}
//$strAA = 'a:13:{i:0;a:6:{i:1;s:9:"280703581";i:2;s:9:"280703581";i:3;s:9:"280703581";i:4;s:9:"280703581";i:5;s:9:"280703581";i:6;s:9:"280703581";}i:1;a:6:{i:1;s:15:"ThabangPhaleng";i:2;s:15:"ThabangPhaleng";i:3;s:15:"ThabangPhaleng";i:4;s:15:"ThabangPhaleng";i:5;s:15:"ThabangPhaleng";i:6;s:15:"ThabangPhaleng";}i:2;a:6:{i:1;s:6:"250655";i:2;s:6:"250655";i:3;s:6:"250655";i:4;s:6:"250655";i:5;s:6:"250655";i:6;s:6:"250655";}i:3;a:6:{i:1;s:11:"62538150335";i:2;s:11:"62538150335";i:3;s:11:"62538150335";i:4;s:11:"62538150335";i:5;s:11:"62538150335";i:6;s:11:"62538150335";}i:4;a:6:{i:1;s:11:"DebitOrder";i:2;s:11:"DebitOrder";i:3;s:11:"DebitOrder";i:4;s:11:"DebitOrder";i:5;s:11:"DebitOrder";i:6;s:11:"DebitOrder";}i:5;a:6:{i:1;s:7:"TWODAY";i:2;s:7:"TWODAY";i:3;s:7:"TWODAY";i:4;s:7:"TWODAY";i:5;s:7:"TWODAY";i:6;s:7:"TWODAY";}i:6;a:6:{i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:0:"";i:5;s:0:"";i:6;s:0:"";}i:7;a:6:{i:1;s:10:"2017-02-28";i:2;s:10:"2017-03-28";i:3;s:10:"2017-04-28";i:4;s:10:"2017-05-28";i:5;s:10:"2017-06-28";i:6;s:10:"2017-07-28";}i:8;a:6:{i:1;s:1:"1";i:2;s:1:"1";i:3;s:1:"1";i:4;s:1:"1";i:5;s:1:"1";i:6;s:1:"1";}i:9;a:6:{i:1;s:7:"pending";i:2;s:7:"pending";i:3;s:7:"pending";i:4;s:7:"pending";i:5;s:7:"pending";i:6;s:7:"pending";}i:10;a:6:{i:1;s:2:"83";i:2;s:2:"83";i:3;s:2:"83";i:4;s:2:"83";i:5;s:2:"83";i:6;s:2:"83";}i:11;a:6:{i:1;s:1:"0";i:2;s:1:"1";i:3;s:1:"2";i:4;s:1:"3";i:5;s:1:"4";i:6;s:1:"5";}i:12;a:6:{i:1;s:3:"Add";i:2;s:3:"Add";i:3;s:3:"Add";i:4;s:3:"Add";i:5;s:3:"Add";i:6;s:3:"Add";}}';

//$arrayDebitOrderSchedule = unserialize($strAA);

/* soak in the passed variable or set our own */			
if(!empty($_GET['format']))		
{
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default			
}
else
{
	$format = 'xml';
}

try
 {
	// -- BOC 31.10.2017 --------- //
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //
		//echo sizeof($arrayDebitOrderSchedule);

if(!empty($arrayDebitOrderSchedule))
{
	//echo sizeof($arrayDebitOrderSchedule);
	
	// -- Each Debit Order Schedule.
	for ($j=1;$j<=sizeof($arrayDebitOrderSchedule);$j++)
	{
		$oneDimensional = null;
		$oneDimensional[1] = 'UFEC';//1001;  								 // -- CustomerID
		$oneDimensional[2] = $arrayDebitOrderSchedule[$j][0];        // -- Client_Reference_1
		$oneDimensional[3] = $arrayDebitOrderSchedule[$j][0];		 // -- Client_Reference_2 01.03.2020
		$oneDimensional[4] = 'South African Identification Number';  // -- Client_ID_Type	  01.03.2020
		$oneDimensional[5] = $arrayDebitOrderSchedule[$j][13];  // -- Client_ID_No
		$oneDimensional[6] = $arrayDebitOrderSchedule[$j][16];	     // -- Initials
		$oneDimensional[7] = $arrayDebitOrderSchedule[$j][1];  		 // -- Account_Holder
		$oneDimensional[8] = $arrayDebitOrderSchedule[$j][18];		 // -- Account_Type
		$oneDimensional[9]  = (int)$arrayDebitOrderSchedule[$j][3];  // -- Account_Number
		$oneDimensional[10] = (int)$arrayDebitOrderSchedule[$j][2];  // -- Branch_Code.
		$oneDimensional[11] = (int)$arrayDebitOrderSchedule[$j][11] + 1;		 // -- No_Payments.
		$oneDimensional[12] = $arrayDebitOrderSchedule[$j][4];  	 // -- Service_Type.
		$oneDimensional[13] = $arrayDebitOrderSchedule[$j][5];  	 // -- Service_Mode.
		$oneDimensional[14] = $arrayDebitOrderSchedule[$j][17];		 // -- Frequency.
		$oneDimensional[15] = $arrayDebitOrderSchedule[$j][7];  	 // -- Action_Date.
		$oneDimensional[16] = $arrayDebitOrderSchedule[$j][15];      // -- Bank_Reference.
		$oneDimensional[17] = $arrayDebitOrderSchedule[$j][6];  	 // -- Amount.
		$oneDimensional[18] = (int)$arrayDebitOrderSchedule[$j][8];  // -- Status_Code
		$oneDimensional[19] = $arrayDebitOrderSchedule[$j][9]; 		 // -- Status_Description
	  	$oneDimensional[20] = $arrayDebitOrderSchedule[$j][10]; 	 // -- ApplicationId
		$oneDimensional[21] = $arrayDebitOrderSchedule[$j][11]; 	 // -- Item

	  //$oneDimensional[10] = ;
	
	//$oneDimensional[11] = ;
		
		// --  Check Operation Id: Change.
		if($arrayDebitOrderSchedule[$j][12] == "Update")
		{
			// -- CollectionID
			$oneDimensional[0] = (int)$arrayDebitOrderSchedule[$j][14]; // -- CollectionID
			updatecollections($oneDimensional);
		}
		// --  Check Operation Id: Add.
		else if($arrayDebitOrderSchedule[$j][12] == "Add")
		{
			createcollections($oneDimensional);
		}
	}
}	
		// -- Get Debit Orders.
		 if(!empty($found) and !empty($UseApplicationID))
		 {
			$sql =  "SELECT * FROM collection Where Client_Reference_1 = ? AND mandateid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ReferenceNumber,$ApplicationId));
		 }
		 else if($found)
		 {			 
			$sql =  "SELECT * FROM collection Where Client_Reference_1 = ? AND Status_Description = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ReferenceNumber,'pending'));
		 }	
		 else if($UseApplicationID)
		 {
			$sql =  "SELECT * FROM collection Where mandateid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($ApplicationId));
		 }
		else
		{
			$sql =  "SELECT * FROM collection";
			$q = $pdo->prepare($sql);
			$q->execute();
		}	
			   $datadebitorderschedule = $q->fetchAll(PDO::FETCH_ASSOC); 
			   $xml = '<debitorderschedules>';

			   foreach($datadebitorderschedule as $rowdebitorderschedule)
			   {					  
				// For each payment schedule related to the loan application.
					$xml = $xml.'<debitorderschedule>';
					$xml = $xml.'<collectionid>'		.$rowdebitorderschedule['collectionid'].		'</collectionid>'		 ;
					$xml = $xml.'<Client_ID>'			.$rowdebitorderschedule['Client_ID'].			'</Client_ID>'		;
					$xml = $xml.'<Client_Reference_1>'  .$rowdebitorderschedule['Client_Reference_1'].	'</Client_Reference_1>';
					$xml = $xml.'<Client_Reference_2>'  .$rowdebitorderschedule['Client_Reference_2'].	'</Client_Reference_2>';
					$xml = $xml.'<Client_ID_Type>'      .$rowdebitorderschedule['Client_ID_Type'].		'</Client_ID_Type>'    ;
					$xml = $xml.'<Client_ID_No>'        .$rowdebitorderschedule['Client_ID_No'].		'</Client_ID_No>'      ;
					$xml = $xml.'<Initials>'            .$rowdebitorderschedule['Initials'].			'</Initials>'          ;
					$xml = $xml.'<Account_Holder>'      .$rowdebitorderschedule['Account_Holder'].		'</Account_Holder>'    ;
					$xml = $xml.'<Account_Type>'        .$rowdebitorderschedule['Account_Type'].		'</Account_Type>'      ;
					$xml = $xml.'<Account_Number>'      .$rowdebitorderschedule['Account_Number'].		'</Account_Number>'    ;
					$xml = $xml.'<Branch_Code>'         .$rowdebitorderschedule['Branch_Code'].		'</Branch_Code>'       ;
					$xml = $xml.'<No_Payments>'         .$rowdebitorderschedule['No_Payments'].		'</No_Payments>'       ;
					$xml = $xml.'<Service_Type>'        .$rowdebitorderschedule['Service_Type'].		'</Service_Type>'      ;
					$xml = $xml.'<Service_Mode>'        .$rowdebitorderschedule['Service_Mode'].		'</Service_Mode>'      ;
					$xml = $xml.'<Frequency>'           .$rowdebitorderschedule['Frequency'].			'</Frequency>'         ;
					$xml = $xml.'<Action_Date>'         .$rowdebitorderschedule['Action_Date'].		'</Action_Date>'       ;
					$xml = $xml.'<Bank_Reference>'      .$rowdebitorderschedule['Bank_Reference'].		'</Bank_Reference>'    ;
					$xml = $xml.'<Amount>'              .$rowdebitorderschedule['Amount'].				'</Amount>'            ;
					$xml = $xml.'<Status_Code>'         .$rowdebitorderschedule['Status_Code'].		'</Status_Code>'       ;
					$xml = $xml.'<Status_Description>'  .$rowdebitorderschedule['Status_Description'].	'</Status_Description>';
					$xml = $xml.'<mandateid>'           .$rowdebitorderschedule['mandateid'].			'</mandateid>'         ;
					$xml = $xml.'<mandateitem>'         .$rowdebitorderschedule['mandateitem'].			'</mandateitem>'       ;					  
					$xml = $xml. '</debitorderschedule>';
			   }
			   
			   // -- Default : xml & json values.
			   if($datadebitorderschedule == null)
			   {
					 $xml = $xml.'<collectionid>'		.'</collectionid>'		 ;
					$xml = $xml.'<Client_ID>'			.'</Client_ID>'		;
					$xml = $xml.'<Client_Reference_1>'  .'</Client_Reference_1>';
					$xml = $xml.'<Client_Reference_2>'  .'</Client_Reference_2>';
					$xml = $xml.'<Client_ID_Type>'      .'</Client_ID_Type>'    ;
					$xml = $xml.'<Client_ID_No>'        .'</Client_ID_No>'      ;
					$xml = $xml.'<Initials>'            .'</Initials>'          ;
					$xml = $xml.'<Account_Holder>'      .'</Account_Holder>'    ;
					$xml = $xml.'<Account_Type>'        .'</Account_Type>'      ;
					$xml = $xml.'<Account_Number>'      .'</Account_Number>'    ;
					$xml = $xml.'<Branch_Code>'         .'</Branch_Code>'       ;
					$xml = $xml.'<No_Payments>'         .'</No_Payments>'       ;
					$xml = $xml.'<Service_Type>'        .'</Service_Type>'      ;
					$xml = $xml.'<Service_Mode>'        .'</Service_Mode>'      ;
					$xml = $xml.'<Frequency>'           .'</Frequency>'         ;
					$xml = $xml.'<Action_Date>'         .'</Action_Date>'       ;
					$xml = $xml.'<Bank_Reference>'      .'</Bank_Reference>'    ;
					$xml = $xml.'<Amount>'              .'</Amount>'            ;
					$xml = $xml.'<Status_Code>'         .'</Status_Code>'       ;
					$xml = $xml.'<Status_Description>'  .'</Status_Description>';
					$xml = $xml.'<mandateid>'           .'</mandateid>'         ;
					$xml = $xml.'<mandateitem>'         .'</mandateitem>'       ;
			   }
		   
				$xml = $xml. '</debitorderschedules>';
				
			// -- Determine the format.
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database::disconnect();	
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
?>
