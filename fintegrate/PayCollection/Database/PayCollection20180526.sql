-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: PayCollections
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Client`
--

DROP TABLE IF EXISTS `Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Client` (
  `Client_ID` int(11) NOT NULL DEFAULT '1001',
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Client_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Client`
--

LOCK TABLES `Client` WRITE;
/*!40000 ALTER TABLE `Client` DISABLE KEYS */;
INSERT INTO `Client` VALUES (1001,'ECASHMEUP Pty Ltd'),(1002,'Exbee Holdings Pty Ltd'),(1003,'MahwayiBusiness Pty Ltd'),(1004,'Vogsphere Pty Ltd');
/*!40000 ALTER TABLE `Client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Collection`
--

DROP TABLE IF EXISTS `Collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Collection` (
  `collectionid` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) DEFAULT NULL,
  `Client_Reference_1` varchar(45) DEFAULT NULL,
  `Client_Reference_2` varchar(45) DEFAULT NULL,
  `Client_ID_Type` varchar(20) DEFAULT NULL,
  `Client_ID_No` bigint(13) DEFAULT NULL,
  `Initials` varchar(2) DEFAULT NULL,
  `Account_Holder` varchar(50) DEFAULT NULL,
  `Account_Type` varchar(20) DEFAULT NULL,
  `Account_Number` bigint(20) DEFAULT NULL,
  `Branch_Code` int(20) DEFAULT NULL,
  `No_Payments` int(20) DEFAULT NULL,
  `Service_Type` varchar(20) DEFAULT NULL,
  `Service_Mode` varchar(20) DEFAULT NULL,
  `Frequency` varchar(20) DEFAULT NULL,
  `Action_Date` date DEFAULT NULL,
  `Bank_Reference` varchar(20) DEFAULT NULL,
  `Amount` decimal(11,2) DEFAULT NULL,
  `Status_Code` int(5) DEFAULT NULL,
  `mandateid` varchar(45) DEFAULT NULL,
  `mandateitem` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`collectionid`)
)  ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Collection`
--

LOCK TABLES `Collection` WRITE;
/*!40000 ALTER TABLE `Collection` DISABLE KEYS */;
INSERT INTO `Collection` VALUES (1001,'80394510',NULL,' South African Ident',7,'SB','SB Mavunda','Cheque',1564086028,470010,1,'Debit Order','TWO DAY','Weekly','2018-05-11','ECASHMEUP',940.63,2,'Insufficient Funds'),(1001,'280061042',NULL,' South African Ident',6,'B','B THERON','Cheque',9256225691,632005,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',940.63,0,'Successful'),(1001,'80521620',NULL,' South African Ident',8,'H','H Shondlani','Savings Account',1318072377,470010,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',710.00,0,'Successful'),(1001,'680167562',NULL,' South African Ident',9,'M','M Mahwayi','Cheque',62558299907,250655,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',1200.00,0,'Successful'),(1002,'CH53',NULL,' South African Ident',8,'C',' C Morris','Cheque',62281329485,250655,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',7000.00,0,'Successful'),(1001,'280703581',NULL,' South African Ident',9,'T','T Phaleng','Cheque',1146024673,198765,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',1530.00,30,'No Authority to Debit'),(1001,'680170010',NULL,' South African Ident',9,'M','M Boets','Cheque',302231811,632005,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',200.00,0,'Successful '),(1001,'480975503',NULL,' South African Ident',8,'OO','OO Malebye','Cheque',10037121152,51001,1,'Debit Order','TWO DAY','Monthly','2018-04-25','ECASHMEUP',3305.35,3,'Contravention to Payers Authority '),(1001,'880833052',NULL,' South African Ident',8,'TH','TH Molema','Cheque',52844196,51001,1,'Debit Order','TWO DAY','Monthly','2018-04-28','ECASHMEUP',1267.00,0,'Successful '),(1003,'Flat212',NULL,' South African Ident',8,'LG','LG Mopako','Savings Account',1503750815,470010,1,'Debit Order','TWO DAY','Monthly','2018-04-30','ECASHMEUP',5632.94,2,'Insufficient Funds'),(1001,'480761021',NULL,' South African Ident',7,'D',' D Wagner','Cheque',300075715,51001,1,'Debit Order','TWO DAY','Monthly','2018-04-30','ECASHMEUP',1050.63,2,'Insufficient Funds'),(1004,'ThandiweC',NULL,' South African Ident',8,'S','S Eiman','Cheque ',62624961133,250655,1,'Debit Order','TWO DAY','Monthly','2018-05-02','ECASHMEUP',58.31,0,'Successful '),(1001,'180192640',NULL,' South African Ident',8,'P','P Chakela','Cheque ',62575560076,250655,1,'Debit Order','TWO DAY','Monthly','2018-05-02','ECASHMEUP',1309.67,0,'Successful ');
/*!40000 ALTER TABLE `Collection` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-26 22:49:50
