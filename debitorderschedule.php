<?php
require 'conf.inc.php';
// Begin of -- Debit Order Schedule.
$tbl_name ="paymentschedule";
$HTML = "";
$scheduleddate = null;

$ReferenceNumber = null;
$AccountHolder	 = null;
$BranchCode	     = null;
$AccountNumber	 = null;
$Servicetype	 = null;
$ServiceMode	 = null;
$AccountType	 = null;
$displayData     = null;
$ApplicationId   = null;
$item 			 = null;
$CustomerId = null;
$arrayDebitOrderSchedule = null;
$indexInside = 0;
$collectionid = null;
$ApprovedLoan = true;

// -- client id type.
$clientidtype = null;
$initials 	  = null;
$accounttype  = null;
$companyname  = 'ECASHMEUP'; // -- Bank Reference.
$frequency    = null;
$initials 		  = null;
$paymentfrequency = null;

// -- 1.ReferenceNumber.
if(isset($_POST['ReferenceNumber']))
{
	$ReferenceNumber = $_POST['ReferenceNumber'];
}

// -- 2.AccountHolder.
if(isset($_POST['AccountHolder']))
{
	$AccountHolder = $_POST['AccountHolder'];
}

// -- 3.BranchCode.
if(isset($_POST['BranchCode']))
{
	$BranchCode = $_POST['BranchCode'];
}

// -- 4.AccountNumber.
if(isset($_POST['AccountNumber']))
{
	$AccountNumber = $_POST['AccountNumber'];
}

// -- 5.Servicetype.
if(isset($_POST['Servicetype']))
{
	$Servicetype = $_POST['Servicetype'];
}

// -- 6.ServiceMode.
if(isset($_POST['ServiceMode']))
{
	$ServiceMode = $_POST['ServiceMode'];
}

// -- 7.AccountType.
if(isset($_POST['AccountType']))
{
	$AccountType = $_POST['AccountType'];
}

// -- 8.CustomerId.
if(isset($_POST['customerid']))
{
	$CustomerId = $_POST['customerid'];
	$CustomerId = base64_decode(urldecode($CustomerId));
}

// -- 9.ApplicationId.
if(isset($_POST['ApplicationId']))
{
	$ApplicationId = $_POST['ApplicationId'];
	$ApplicationId = base64_decode(urldecode($ApplicationId));
}

// -- 10. Initials.
if(isset($_POST['initials']))
{
	$initials = $_POST['initials'];
}

// -- 11. paymentfrequency.
if(isset($_POST['frequency']))
{
	$paymentfrequency = $_POST['frequency'];
}

// -- Different file names.
$base = '';
$checkDB = 'database.php';
$checkDB2 = 'database_invoice2';
$DoNotDB = '';
$DoNotDB2 = '';
$fileNames = '';

// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
if (!empty($_POST))
{

$included_files = get_included_files();
//require_once 'database_invoice2.php';

foreach ($included_files as $filename)
{

    if($filename == $checkDB)
	{
	  $DoNotDB = 'yes';
	}

	if($filename == $checkDB2)
	{
	 $DoNotDB2 = 'yes';
	}

	$fileNames = $fileNames.$filename."\n";
}

if(empty($DoNotDB))
{require 'database.php';}
$HTML = $HTML.'<div style="overflow-y:auto;" style="z-index:10000;">';
$HTML = $HTML.'<table class="table table-striped table-bordered" style="white-space: nowrap;">';

$HTML = $HTML.'<thead>';
$HTML = $HTML.'<tr>
				   <th>Reference</th>
				   <th>Account Holder</th>
				   <th>Branch Code</th>
				   <th>Account Number</th>
				   <th>Service Type</th>
				   <th>Service Mode</th>
				   <th>Amount</th>
				   <th>Action Date</th>
				   <th>Status Code</th>
				   <th>Description</th>
				</tr></thead><tbody>';

$displayData = GetPaymentSchedule($ApplicationId);

// -- Web Service for all Paymentschedule.
$WebServiceURL = "http://localhost/ws_paymentschedules?ApplicationId=".$ApplicationId;

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// -- Web Service for all Debit Or//ders.

$WebServiceURL2 = "http://localhost/fintegrate/PayCollection/ws_maintaindebitorders.php?ReferenceNumber=".$ReferenceNumber;
//print_r($WebServiceURL2);
// read feed into SimpleXML object
$sxml2 = simplexml_load_file($WebServiceURL2);
//print_r($sxml2);
// -- Action the debit order.
$sxmlAction = null;

// -- Two action : operationAdd, OperationChange.
$operationAdd = true;
$operationChange = null;
$debitorderscheduleStatus = '1';
$debitorderscheduleDesc = 'pending';

// -- if you want to fetch multiple payment schedule IDs:
foreach($sxml->paymentschedule as $paymentschedule)
{
	
	$debitorderscheduleStatus = '1';
	$debitorderscheduleDesc = 'pending';
	$operationAdd = true;
	$operationChange = null;

	$applicationId  = (string)$paymentschedule->applicationId;
	$scheduleddate  = (string)$paymentschedule->scheduleddate;
	$amountdue 		= (string)$paymentschedule->monthlypayment;
	$DisplayAmount = $amountdue;
	if(empty($amountdue))
	{$amountdue = '0.00';}
	
	$item = (string)$paymentschedule->item;

	//echo (string)$paymentschedule->status;
// -- ONLY if the loan is approved.

if((string)$paymentschedule->status == 'APPR' | (string)$paymentschedule->status == 'FUND' )
{
	if($paymentschedule->item >= 0)
	{
// -- Iterate through each Payment Schedule item.
$indexInside = $indexInside + 1;

// -- Debit Orders Schedule.
		if(!empty($sxml2))
		{
			// And if you want to fetch multiple order scheduled IDs:
			foreach($sxml2->debitorderschedule as $debitorderschedule)
			{
			//	echo 'Item ID:'.$debitorderschedule->mandateitem.' = '.$paymentschedule->item;
			//	echo $debitorderschedule->mandateid.' = '.$paymentschedule->applicationId;

				if((string)$debitorderschedule->mandateid == (string)$paymentschedule->applicationId)
				{
					//echo 'Status_Code Inside:'.$debitorderschedule->Status_Code;
					if((string)$debitorderschedule->mandateitem == (string)$paymentschedule->item)
					{
						if($debitorderschedule->Status_Code != 0) // Not Successful
						{
							$operationChange = true;
							$collectionid = $debitorderschedule->collectionid;
						}
					}
					$operationAdd = null;
					$debitorderscheduleStatus = (string)$debitorderschedule->Status_Code;
					$debitorderscheduleDesc   = (string)$debitorderschedule->Status_Description;
				}
			}
		}

// -- Action:Operation.
		if(!empty($operationChange) or !empty($operationAdd))
		{
			// -- Update Debit Order Schedule Item.
				$arrayDebitOrderSchedule[$indexInside][0] = $ReferenceNumber;
				$arrayDebitOrderSchedule[$indexInside][1] = $AccountHolder;
				$arrayDebitOrderSchedule[$indexInside][2] = $BranchCode;
				$arrayDebitOrderSchedule[$indexInside][3] = $AccountNumber;
				$arrayDebitOrderSchedule[$indexInside][4] = $Servicetype;
				$arrayDebitOrderSchedule[$indexInside][5] = $ServiceMode;
				$arrayDebitOrderSchedule[$indexInside][6] = $amountdue;
				$arrayDebitOrderSchedule[$indexInside][7] = $scheduleddate;
				$arrayDebitOrderSchedule[$indexInside][8] = $debitorderscheduleStatus;
				$arrayDebitOrderSchedule[$indexInside][9] = $debitorderscheduleDesc;

			// - Mandating Fields, Debit Orders.
				$arrayDebitOrderSchedule[$indexInside][10] = $applicationId;
				$arrayDebitOrderSchedule[$indexInside][11] = $item;

			// -- Update Debit Order Schedule.
				if($operationChange)
				{
				// -- Operation Change
					$arrayDebitOrderSchedule[$indexInside][12] = 'Update';
				// -- Collection ID to update.
					$arrayDebitOrderSchedule[$indexInside][14] = $collectionid;

				}
			// -- Create Debit Order Schedule.
				else if($operationAdd)
				{
			// -- Add Debit Order Schedule Item.
			// -- Operation Change
					$arrayDebitOrderSchedule[$indexInside][12] = 'Add';
				}
				$arrayDebitOrderSchedule[$indexInside][13] = $CustomerId;
				//echo $CustomerId;
			// -- Bank reference number
				$arrayDebitOrderSchedule[$indexInside][15] = $companyname;

			// -- Initials.
				$arrayDebitOrderSchedule[$indexInside][16] = $initials;

			// -- Frequency.
				$arrayDebitOrderSchedule[$indexInside][17] = $paymentfrequency;

			// -- AccountType.
				$arrayDebitOrderSchedule[$indexInside][18] = $AccountType;
		}
	}
	// -- Approved Loan.
	$ApprovedLoan = true;

  }
  // -- Only approved loans.
  else
  {
	  // -- Not Approved Loan.
		$ApprovedLoan = false;
  }
 
}
//array_walk_recursive($arrayDebitOrderSchedule,'formatSerialize');
//$str=serialize($arrayDebitOrderSchedule);
//$str = json_encode($arrayDebitOrderSchedule);
//echo $str;
/*	$strArray = unserialize($str);
	array_walk_recursive($strArray,'formatSerializeRev');
	echo var_dump($strArray);
*/
//echo 'length : '.strlen($str);
//$str = preg_replace('/\s+/', '', $str);

// -- Web Service for all Debit Orders.
//$WebServiceURL2 = "http://localhost/fintegrate/PayCollection/ws_maintaindebitorders.php?ReferenceNumber=".$ReferenceNumber.'&arrayDebitOrderSchedule='.'"'.$str.'"';
//echo $ApprovedLoan;

if($ApprovedLoan)
{
	// -- Debit Orders with an array.
	$str = json_encode($arrayDebitOrderSchedule);
	// -- Web Service for all Debit Orders.
	$WebServiceURL2 = "http://localhost/fintegrate/PayCollection/ws_maintaindebitorders.php?ReferenceNumber=".$ReferenceNumber.'&arrayDebitOrderSchedule='.'"'.$str.'"';
	
}
else
{
// -- Debit Orders Retrieve.
	// -- Web Service for all Debit Orders.
$WebServiceURL2 =  "http://localhost/fintegrate/PayCollection/ws_maintaindebitorders.php?ApplicationId=".$applicationId;
	  echo 'Only approved loan can be scheduled for debit orders.';
}
//echo $WebServiceURL2;

// read feed into SimpleXML object
$sxml2 = simplexml_load_file($WebServiceURL2);

// -- Read the latest Debit Order(s).
// And if you want to fetch multiple order scheduled IDs:
foreach($sxml2->debitorderschedule as $debitorderschedule)
{
$debitorderschedule->mandateid;
$paymentschedule->applicationId;
$debitorderschedule->mandateitem;

$ReferenceNumber			= $debitorderschedule->Client_Reference_1;
$AccountHolder				= $debitorderschedule->Account_Holder;
$BranchCode					= $debitorderschedule->Branch_Code;
$AccountNumber				= $debitorderschedule->Account_Number;
$Servicetype				= $debitorderschedule->Service_Type;
$ServiceMode				= $debitorderschedule->Service_Mode;
$amountdue					= $debitorderschedule->Amount;
$scheduleddate				= $debitorderschedule->Action_Date;
$debitorderscheduleStatus	= $debitorderschedule->Status_Code;
$debitorderscheduleDesc		= $debitorderschedule->Status_Description;

		$HTML = $HTML."
					<tr>
						<td>$ReferenceNumber</td>
						<td>$AccountHolder</td>
						<td>$BranchCode</td>
						<td>$AccountNumber</td>
						<td>$Servicetype</td>
						<td>$ServiceMode</td>
						<td>$DisplayAmount</td>
						<td>$scheduleddate</td>
						<td>$debitorderscheduleStatus</td>
						<td>$debitorderscheduleDesc</td>
					</tr>";

}
$HTML = $HTML.'</tbody></table></div>';
}
echo $HTML;

// End of -- Debit Order Schedule.

?>
