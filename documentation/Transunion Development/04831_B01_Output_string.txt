<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
   <soap:Body>
      <BusinessSearchResponse xmlns="https://secure.transunion.co.za/TUBureau">
         <BusinessSearchResult>
            <RawData/>
            <ResponseStatus>Success</ResponseStatus>
            <ProcessingStartDate>2023-01-26T01:33:50.0407185+02:00</ProcessingStartDate>
            <ProcessingTimeSecs>2.203114</ProcessingTimeSecs>
            <UniqueRefGuid>72b9c047-2e83-4864-9f6f-9131f28194d1</UniqueRefGuid>
            <SearchResponse>
               <SearchResponse>
                  <ITNumber>622717478</ITNumber>
                  <Name>IDENTITY DEVELOPMENT FUND MANAGERS (PTY) LTD</Name>
                  <NameType>P</NameType>
                  <BusinessName>IDF CAPITAL (PTY) LTD</BusinessName>
                  <PhysicalAddress>2ND FLOOR</PhysicalAddress>
                  <Suburb>ILLOVO</Suburb>
                  <Town>JOHANNESBURG</Town>
                  <Country>ZA</Country>
                  <PostCode>2196</PostCode>
                  <PostalAddress>POSTNET 138</PostalAddress>
                  <PostalSuburb>SAXONWOLD</PostalSuburb>
                  <PostalTown>JOHANNESBURG</PostalTown>
                  <PostalCountry>ZA</PostalCountry>
                  <PostalPostCode>2132</PostalPostCode>
                  <PhoneNo>27117727900</PhoneNo>
                  <FaxNo>27117727910</FaxNo>
                  <RegNo>200701583007</RegNo>
                  <RegStatus>In Business</RegStatus>
                  <RegStatusCode>03</RegStatusCode>
                  <TradingNumber/>
               </SearchResponse>
            </SearchResponse>
         </BusinessSearchResult>
      </BusinessSearchResponse>
   </soap:Body>
</soap:Envelope>