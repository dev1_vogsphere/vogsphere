0120092222EFT REPORTS   4106ACBJERGI012D0012DATAOUT20090222LIVE01324106                                                             

 VET NO      : 0001                   D C V / U S E R / H E A D E R / T R A I L E R   R E P O R T                                   
                                                                                                                                    
 USER                                                                                          CONTACT NAME & TEL NO                
 ----                                                                                          ---------------------                
       MERCANTILE  EBUREAU                  0236                                                     CALL CENTRE                    
       PO BOX 1231                                                                                                                  
       HILLCREST                                                                                     011-3020765                    
       3650                                                                                                                         
                                                                                                                                    
 TAPE DETAILS                                                                                  ORIGINATING BANK                     
 ------------                                                                                  ----------------                     
       TAPE NUMBER           : MED22349                                                              MERCANTILE BANK LIMITED        
       CREATION DATE         : 09/02/22                                                              SANDTON                        
       DATE SUBMITTED TO ACB : 09/02/22                                                              45=09=05                       
                                                                                                                                    
 ---------------------------------------------------------------------------------------------------------------------------------- 
 !        F I E L D         !             H E A D E R   R E C O R D      ERROR !             T R A I L E R   R E C O R D    ERROR ! 
 !  D E S C R I P T I O N   ! CONTENTS      REASON IF INVALID             CODE ! CONTENTS      REASON IF INVALID             CODE ! 
 ---------------------------------------------------------------------------------------------------------------------------------- 
 ! RECORD IDENTIFIER        ! 04                                               ! 92                                               ! 
 !                          !                                                  !                                                  ! 
 ! USER CODE                ! 0236                                             ! 0236                                             ! 
 !                          !                                                  !                                                  ! 
 ! CREATION DATE            ! 090222                                           ! N/A                                              ! 
 !                          !                                                  !                                                  ! 
 ! PURGE DATE               ! 090222                                           ! N/A                                              ! 
 !                          !                                                  !                                                  ! 
 ! FIRST ACTION DATE        ! 110222                                           ! 110222                                           ! 
 !                          !                                                  !                                                  ! 
 ! LAST ACTION DATE         ! 120222                                           ! 120222                                           ! 
 !                          !                                                  !                                                  ! 
 ! FIRST SEQUENCE NUMBER    ! 018239                                           ! 018239                                           ! 
 !                          !                                                  !                                                  ! 
 ! LAST SEQUENCE NUMBER     ! N/A                                              ! 018792                                           ! 
 !                          !                                                  !                                                  ! 
 ! USER GENERATION NUMBER   ! 0037                                             ! N/A                                              
 !                          !                                                  !                                                  ! 
 ! SERVICE TYPE             ! TWO DAY                                          ! N/A                                              ! 
 !                          !                                                  !                                                  ! 
 ! NUMBER OF DEBIT RECORDS  ! N/A                                              ! 000553                                           ! 
 !                          !                                                  !                                                  ! 
 ! NUMBER OF CREDIT RECORDS ! N/A                                              ! 000001                                           ! 
 !                          !                                                  !                                                  ! 
 ! NUMBER OF CONTRA RECORDS ! N/A                                              ! 000001                                           ! 
 !                          !                                                  !                                                  ! 
 ! TOTAL DEBIT VALUE        ! N/A                                              ! 000006580750                                     ! 
 !                          !                                                  !                                                  ! 
 ! TOTAL CREDIT VALUE       ! N/A                                              ! 000006580750                                     ! 
 !                          !                                                  !                                                  ! 
 ! HASH TOTAL OF HOMING     ! N/A                                              ! 508676931953                                     ! 
 ! ACCOUNT NUMBERS          !                                                  !                                                  ! 
 ---------------------------------------------------------------------------------------------------------------------------------- 
                                                                                                                                    
  TOTAL NUMBER OF  1.RECEIVED:        553      TOTAL VALUE OF   1.RECEIVED:         65 807,50   *** TAPE ACCEPTED ***               
   TRANSACTIONS    2.ACCEPTED:        553      TRANSACTIONS     2.ACCEPTED:         65 807,50                                       
                 3.HOMED BACK:          0                     3.HOMED BACK:              0,00   **** DATASET ACCEPTED ****          
                   4.REJECTED:          0                       4.REJECTED:              0,00                                       
BSV     EFR023    15H01                   B A N K S E R V   (P T Y)   L T D                         2009/12/15      PAGE:-    3     
                                                                                                                                    
CENTRE : JOHANNESBURG                             REG. NO. 1993/07766/06                                                            
                                                                                                                                    
MAGTAPE SERVICE                                                                                       PROCESSING DATE :- 09/02/22   
                                                                                                                                    
 WORKUNIT NO : EJGI009D  FILENAME : EJGI009D                                                                     USER :- 0236       
                                     INPUT VET REPORT - TRANSACTION AND CONTRA RECORDS                                              
                                                                                                                                    
<------------------  M A J O R   R E C O R D   D E T A I L S  ------------------>-<---------  E R R O R   D E T A I L S  ---------->
TRANSACTION TYPE   SEQ.  HOMING   HOMING ACC.      AMOUNT  USER        HOMING ACC.! FLD FIELD       DESCRIPTION OF    ACTION    ERR 
                   NO.   BRANCH   NUMBER                   REFERENCE   NAME       ! NO. CONTENTS    ERROR                       CODE
------------------------------------------------------------------------------------------------------------------------------------
52-DIRECT DEBIT   18792 45=09=05 01000000047     65 807,50 POCIT       POCIT      !                                                 
  -CONTRA                                                  CONTRA57FE             !                                                 
                                                           F79FD09E75             !                                                 
         ACTION DATE:- 09/02/22                                                   !                                                 
....................................................................................END OF TRANSACTION SET..........................
                                                                                  !                                                 
                                                                                                                                    
    TOTAL VALID TRANSACTIONS        :                553                                         *** TAPE ACCEPTED ***              
    TOTAL INVALID TRANSACTIONS      :                  0                                                                            
    TOTAL 'HOMED BACK' TRANSACTIONS :                  0                                         **** DATASET ACCEPTED ****         
