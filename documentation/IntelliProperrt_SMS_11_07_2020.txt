Intelli Property Management - Internal Uset Code
---------------------------

Page
----
text_smstemplates.php			    - Done
text_editsmstemplate.php			- Done
text_newsmstemplate.php				- Done

text_uploadbulksmscustomers.php		- Done
excel file ~ convert to .csv
instructions
text_sendbulksms.php				- Done

SelectSubGroup.php					- Done			
script_AddNewCustomer.php			- Done
SelectBulkSMSClientCustomers.php    - Done

Database
--------
subgroup
ALTER TABLE `ecashpdq_eloan`.`subgroup` 
ADD COLUMN `UserCode` VARCHAR(45) NULL AFTER `parentgroup`,
ADD COLUMN `IntUserCode` VARCHAR(45) NULL AFTER `UserCode`;

smstemplate
ALTER TABLE `ecashpdq_eloan`.`smstemplate` 
ADD COLUMN `UserCode` VARCHAR(45) NULL AFTER `departmentid`,
ADD COLUMN `IntUserCode` VARCHAR(45) NULL AFTER `UserCode`;

