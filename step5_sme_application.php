<div class="row setup-content" id="step-5">
		 <div class="controls">
		 <div class="container-fluid" style="border:1px solid #ccc">

			 <?php
	 		$ApplicationId = '';
	 		$customerid = '';
	 		$hash="&guid=".md5(date("h:i:sa"));
			$ApplicationIdEncoded = '';
	 		?>
	 <div class="col-md-12">
			    <div class="messages">
				  <p id="message_infoUploadfile" name="message_infoUploadfile" style="margin: 8px 0;font-size: 12px;"></p>
			    </div>
    </div>

	</br>
	
     <div class="row">
		 <table class = "table table-hover">
			    <thead>
			       <tr style ="background-color: #f0f0f0">
					  <th>Upload</th>
			          <!-- <th>View Document</th>-->
			          <th>Type</th>
			       </tr>
			    </thead>
			 <!----------------- Read AppLoans document names ----------------------------->
			  <?php
			 	 // -- ID Document
			 	 $iddocument = '';
			 	 // -- Contract Agreement
			 	 $contract = '';
			 	 // -- Bank statement
			 	 $bankstatement = '';
			 	 // -- Proof of income
			 	 $proofofincome = '';
			 	 // -- Fica
			 	 $fica = '';
			 	 $other = '';
			     $other1 = '';
			 	 $other2 = '';
			 	 $other3 = '';
				 $other4 = '';
				 $other5 = '';
				 $other6 = '';
				 $other7 = '';
				 $other8 = '';
				 $other9 = '';
				 $other10 = '';
				 $other11 = '';
				 $other12 = '';
				 $other13 = '';
				 $other14 = '';
				 $other15 = '';
				 $other16 = '';
			 	// -- EOC 24.03.2019 - Upload Debit Order Form, other4.

			 	$NoFIle = "";

				$count = 1;
			 	if($count >= 1)
			     {

			    if(empty($iddocument)){$iddocument = $NoFIle;}
			    if(empty($contract)){ $contract = $NoFIle;}
			    if(empty($bankstatement)){$bankstatement = $NoFIle;}
			    if(empty($proofofincome)){$proofofincome = $NoFIle;}
			    if(empty($fica)){ $fica = $NoFIle;}
			    if(empty($creditreport)){  $creditreport = $NoFIle;}
	
			     $ViewDocument = 'View Document';
			 	$classCSS = "class='btn btn-warning'";
				$ext = ".pdf";
			 	$iddocumentHTML = '';
			 	if($iddocument == '')
			 	{
			 		$iddocumentHTML = "<tr>
					  <td><input type='file' name='files1' id='files1' multiple='multiple' accept=$ext ></td>
			          <!-- <td>a</td> -->
			          <td>Certified ID copies</td>
			 		</tr>";
			 	}
			 	else
			 	{
			 		$iddocumentHTML = "<tr>
					  <td><input type='file' name='files1'  multiple='multiple' accept=$ext ></td>					
			          <!-- <td><a $classCSS href='uploads/$iddocument' target='_blank'>$ViewDocument</a></td> -->
			          <td>Certified ID copies</td>
			 		</tr>";
			 	}
			 	$contractHTML = '';
			 	if($contract == '')
			 	{
			 		$contractHTML = "<tr>
					  <td><input type='file' name='files2' id='files2' multiple='multiple' accept=$ext ></td>					
			          <!-- <td><a href='uploads/$contract' target='_blank'>$contract</a></td> -->
			          <td>Affidavit from directors/ members that they are aware of the contents of the application form</td>
			       </tr>
			       ";
			 	}
			 	else
			 	{
			 		$contractHTML = "
			       <tr>
					  <td><input type='file' name='files2' id='files2' multiple='multiple' accept=$ext ></td>					
			          <!-- <td><a $classCSS href='uploads/$contract' target='_blank'>$ViewDocument</a></td> -->
			          <td>Affidavit from directors/ members that they are aware of the contents of the application form</td>
			       </tr>";
			 	}

			 	$bankstatementHTML = '';
			 	if($bankstatement == '')
			 	{
			 		$bankstatementHTML = "<tr>
					  <td><input type='file' name='files3' id='files3' multiple='multiple' accept=$ext ></td>										
			          <!-- <td><a href='uploads/$bankstatement' target='_blank'>$bankstatement</a></td> -->
			          <td>Curriculum Vitae(s) of all the Directors</td>
			       </tr>";
			 	}
			 	else
			 	{
			 		$bankstatementHTML = "<tr>
					  <td><input type='file' name='files3' id='files3' multiple='multiple' accept=$ext ></td>															
			          <!-- <td><a $classCSS href='uploads/$bankstatement' target='_blank'>$ViewDocument</a></td> -->
			          <td>Curriculum Vitae(s) of all the Directors</td>
			       </tr>";
			 	}

			 	$proofofincomeHTML = '';
			 	if($proofofincome == '')
			 	{
			 		$proofofincomeHTML = "
			 	  <tr>
					  <td><input type='file' name='files4' id='files4' multiple='multiple' accept=$ext ></td>																			  
			          <!-- <td><a href='uploads/$proofofincome' target='_blank'>$proofofincome</a></td> -->
			          <td>CIPC Registration Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$proofofincomeHTML = "
			 	  <tr>
					  <td><input type='file' name='files4' id='files4' multiple='multiple' accept=$ext ></td>																		  
				  
			          <!-- <td><a $classCSS href='uploads/$proofofincome' target='_blank'>$ViewDocument</a></td> -->

			          <td>CIPC Registration Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	$ficaHTML = '';
			 	if($fica == '')
			 	{
			 		$ficaHTML = "

			 	  <tr>
					  <td><input type='file' name='files5' id='files5' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$fica' target='_blank'>$fica</a></td> -->
			          <td>Tax Clearance Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$ficaHTML = "
			 	  <tr>
					  <td><input type='file' name='files5' id='files5' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$fica' target='_blank'>$ViewDocument</a></td> -->
			          <td>Tax Clearance Certificate</td>
			       </tr>
			 	  ";
			 	}
				$otherHTML = '';
			 	if($other == '')
			 	{
			 		$otherHTML = "

			 	  <tr>
					  <td><input type='file' name='files6' id='files6' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other' target='_blank'>$other</a></td> -->
			          <td>Letter of Good Standing with Compensation Fund</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$otherHTML = "
			 	  <tr>
					  <td><input type='file' name='files6' id='files6' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other' target='_blank'>$ViewDocument</a></td> -->
			          <td>Letter of Good Standing with Compensation Fund</td>
			       </tr>
			 	  ";
			 	}
				$other1HTML = '';
			 	if($other1 == '')
			 	{
			 		$other1HTML = "

			 	  <tr>
					  <td><input type='file' name='files7' id='files7' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other1' target='_blank'>$other1</a></td> -->
			          <td>Unemployment Insurance Fund Compliance Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other1HTML = "
			 	  <tr>
					  <td><input type='file' name='files7' id='files7' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other1' target='_blank'>$ViewDocument</a></td> -->
			          <td>Unemployment Insurance Fund Compliance Certificate</td>
			       </tr>
			 	  ";
			 	}
				$other2HTML = '';
			 	if($other2 == '')
			 	{
			 		$other2HTML = "

			 	  <tr>
					  <td><input type='file' name='files8' id='files8' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other2' target='_blank'>$other2</a></td> -->
			          <td>Valid PAYE Registration Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other2HTML = "
			 	  <tr>
					  <td><input type='file' name='files8' id='files8' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other2' target='_blank'>$ViewDocument</a></td> -->
			          <td>Valid PAYE Registration Certificate</td>
			       </tr>
			 	  ";
			 	}
				$other3HTML = '';
			 	if($other3 == '')
			 	{
			 		$other3HTML = "

			 	  <tr>
					  <td><input type='file' name='files9' id='files9' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other3' target='_blank'>$other3</a></td> -->
			          <td>Valid VAT Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other3HTML = "
			 	  <tr>
					  <td><input type='file' name='files9' id='files9' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other3' target='_blank'>$ViewDocument</a></td> -->
			          <td>Valid VAT Certificate</td>
			       </tr>
			 	  ";
			 	}
				$other4HTML = '';
			 	if($other4 == '')
			 	{
			 		$other4HTML = "

			 	  <tr>
					  <td><input type='file' name='files10' id='files10' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other4' target='_blank'>$other4</a></td> -->
			          <td>Personal Bank Statements for the past 12 months</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other4HTML = "
			 	  <tr>
					  <td><input type='file' name='files10' id='files10' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other4' target='_blank'>$ViewDocument</a></td> -->
			          <td>Personal Bank Statements for the past 12 months</td>
			       </tr>
			 	  ";
			 	}
				$other5HTML = '';
			 	if($other5 == '')
			 	{
			 		$other5HTML = "

			 	  <tr>
					  <td><input type='file' name='files11' id='files11' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other5' target='_blank'>$other5</a></td> -->
			          <td>Official Business Bank Confirmation Letter</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other5HTML = "
			 	  <tr>
					  <td><input type='file' name='files11' id='files11' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other5' target='_blank'>$ViewDocument</a></td> -->
			          <td>Official Business Bank Confirmation Letter</td>
			       </tr>
			 	  ";
			 	}
				$other6HTML = '';
			 	if($other6 == '')
			 	{
			 		$other6HTML = "

			 	  <tr>
					  <td><input type='file' name='files12' id='files12' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other6' target='_blank'>$other6</a></td> -->
			          <td>Business Bank Statements for the past 12 months</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other6HTML = "
			 	  <tr>
					  <td><input type='file' name='files12' id='files12' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other6' target='_blank'>$ViewDocument</a></td> -->
			          <td>Business Bank Statements for the past 12 months</td>
			       </tr>
			 	  ";
			 	}
				$other7HTML = '';
			 	if($other7 == '')
			 	{
			 		$other7HTML = "

			 	  <tr>
					  <td><input type='file' name='files13' id='files13' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other7' target='_blank'>$other7</a></td> -->
			          <td>Attached Personal Credit Report</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other7HTML = "
			 	  <tr>
					  <td><input type='file' name='files13' id='files13' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other7' target='_blank'>$ViewDocument</a></td> -->
			          <td>Attached Personal Credit Report</td>
			       </tr>
			 	  ";
			 	}
				$other8HTML = '';
			 	if($other8 == '')
			 	{
			 		$other8HTML = "

			 	  <tr>
					  <td><input type='file' name='files14' id='files14' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other8' target='_blank'>$other8</a></td> -->
			          <td>Attached Business Credit Report</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other8HTML = "
			 	  <tr>
					  <td><input type='file' name='files14' id='files14' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other8' target='_blank'>$ViewDocument</a></td> -->
			          <td>Attached Business Credit Report</td>
			       </tr>
			 	  ";
			 	}

				$other9HTML = '';
			 	if($other9 == '')
			 	{
			 		$other9HTML = "

			 	  <tr>
					  <td><input type='file' name='files15' id='files15' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other9' target='_blank'>$other9</a></td> -->
			          <td>BBBEEE Affidavit or Certificate</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other9HTML = "
			 	  <tr>
					  <td><input type='file' name='files15' id='files15' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other9' target='_blank'>$ViewDocument</a></td> -->
			          <td>BBBEEE Affidavit or Certificate</td>
			       </tr>
			 	  ";
			 	}
				
				$other10HTML = '';
			 	if($other10 == '')
			 	{
			 		$other10HTML = "

			 	  <tr>
					  <td><input type='file' name='files16' id='files16' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other10' target='_blank'>$other10</a></td> -->
			          <td>Tribal Authority letter - Permission to use land</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other10HTML = "
			 	  <tr>
					  <td><input type='file' name='files16' id='files16' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other10' target='_blank'>$ViewDocument</a></td> -->
			          <td>Tribal Authority letter - Permission to use land</td>
			       </tr>
			 	  ";
			 	}
				
				$other11HTML = '';
			 	if($other11 == '')
			 	{
			 		$other11HTML = "

			 	  <tr>
					  <td><input type='file' name='files17' id='files17' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other11' target='_blank'>$other11</a></td> -->
			          <td>A valid signed offtake agreement</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 		$other11HTML = "
			 	  <tr>
					  <td><input type='file' name='files17' id='files17' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other11' target='_blank'>$ViewDocument</a></td> -->
			          <td>A valid signed offtake agreement</td>
			       </tr>
			 	  ";
			 	}
				
				$other12HTML = '';
			 	if($other12 == '')
			 	{
			 		$other12HTML = "

			 	  <tr>
					  <td><input type='file' name='files18' id='files18' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other12' target='_blank'>$other12</a></td> -->
			          <td>Expressed interest in establishing a charcoal focused business</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 	  $other12HTML = "
			 	  <tr>
					  <td><input type='file' name='files18' id='files18' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other12' target='_blank'>$ViewDocument</a></td> -->
			          <td>Expressed interest in establishing a charcoal focused business</td>
			       </tr>
			 	  ";
			 	}
				
				$other13HTML = '';
			 	if($other13 == '')
			 	{
			 		$other13HTML = "

			 	  <tr>
					  <td><input type='file' name='files19' id='files19' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other13' target='_blank'>$other13</a></td> -->
			          <td>Approved Business Plan</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 	  $other13HTML = "
			 	  <tr>
					  <td><input type='file' name='files19' id='files19' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other13' target='_blank'>$ViewDocument</a></td> -->
			          <td>Approved Business Plan</td>
			       </tr>
			 	  ";
			 	}
				
				$other14HTML = '';
			 	if($other14 == '')
			 	{
			 		$other14HTML = "

			 	  <tr>
					  <td><input type='file' name='files20' id='files20' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other14' target='_blank'>$other14</a></td> -->
			          <td>Amount of biomass produced within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 	  $other14HTML = "
			 	  <tr>
					  <td><input type='file' name='files20' id='files20' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other14' target='_blank'>$ViewDocument</a></td> -->
			          <td>Amount of biomass produced within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
				
				$other15HTML = '';
			 	if($other15 == '')
			 	{
			 		$other15HTML = "

			 	  <tr>
					  <td><input type='file' name='files21' id='files21' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other15' target='_blank'>$other15</a></td> -->
			          <td>Amount of charcoal produced within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 	  $other15HTML = "
			 	  <tr>
					  <td><input type='file' name='files21' id='files21' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other15' target='_blank'>$ViewDocument</a></td> -->
			          <td>Amount of charcoal produced within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
				
				$other16HTML = '';
			 	if($other16 == '')
			 	{
			 		$other16HTML = "

			 	  <tr>
					  <td><input type='file' name='files22' id='files22' multiple='multiple' accept=$ext ></td> 
			          <!-- <td><a href='uploads/$other16' target='_blank'>$other16</a></td> -->
			          <td>Amount of revenue generated within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
			 	else
			 	{
			 	  $other16HTML = "
			 	  <tr>
					  <td><input type='file' name='files22' id='files22' multiple='multiple' accept=$ext ></td>				  
			          <!-- <td><a $classCSS href='uploads/$other16' target='_blank'>$ViewDocument</a></td> -->
			          <td>Amount of revenue generated within the last twelve (12) months</td>
			       </tr>
			 	  ";
			 	}
			

				
			    echo "<tbody>"
			       .$iddocumentHTML.$contractHTML.$bankstatementHTML.$proofofincomeHTML.$ficaHTML.$otherHTML.$other1HTML.$other2HTML.$other3HTML.$other4HTML.$other5HTML.$other6HTML.$other7HTML.$other8HTML.$other9HTML.$other10HTML.$other11HTML.$other12HTML.$other13HTML.$other14HTML.$other15HTML.$other16HTML."
			    </tbody>";
			    } ?>
			 <!------------------ End AppLoans documents ----------------------------------->
			 </table>

		 </div>
		 </div>
</div>
</br>

<!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Save</button>-->
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
