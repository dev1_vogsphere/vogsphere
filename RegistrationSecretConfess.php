
<?php 
/* -------------------------------------------------------------------------------------- 
					Database Operations Insert, Update, and Delete.
------------------------------------------------------------------------------------------
Joomla

*/
$doc = JFactory::getDocument();
//require '/templates//database.php';
require 'templates/beez3/database.php';

// == Database Tables 
$tbl_user = 'tab_user';

// -- Declare all the form input values with its error parameters.
		// -- Store Error Messages
		$usernameError = null;
		$titleError = null;
		$firstnameError = null; 
		$surnameError = null;
		$phoneError = null;
		$emailError = null;
		$dobError = null;
		$passwordError = null;
		$genderError = null;
		$agreeError = null;

		// -- Data(10)
		$username = null;
		$title = null;
		$firstname = null; 
		$surname = null;
		$phone = null;
		$email = null;
		$dob = null;
		$password = null;
		$gender = null;
		$agree = null;
		$country = null;
	    $province = null;

// -- Register Button Clicked	
 if (!empty($_POST)) 
	{
		// keep track post values
		// User Details
		$username = $_POST['username'];
		$title = $_POST['title'];
		$firstname = $_POST['firstname'];
		$surname = $_POST['surname'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$dob = $_POST['dob'];
		$password = $_POST['password'];
		$gender = "";
   		$agree = "";
  		$country = $_POST['country'];
	    $province = $_POST['province'];
   
		if(!empty($_POST['gender'])) 
		{
			$gender = $_POST['gender'];
		}
   
		if(!empty($_POST['agree'])) 
        {
          		$agree = $_POST['agree'];
        }
		
		// User Details
		// validate input 		
		$valid = true;
		if (empty($username)) { $usernameError = 'Please enter username'; $valid = false;}
		if (empty($title)) { $titleError = 'Please enter Title'; $valid = false;}
		if (empty($firstname)) { $firstnameError = 'Please enter firstname'; $valid = false;}
		if (empty($surname)) { $surnameError = 'Please enter surname'; $valid = false;}
		if (empty($phone)) { $phoneError = 'Please enter phone'; $valid = false;}
		if (empty($email)) { $emailError = 'Please enter email'; $valid = false;}
		if (empty($dob)) { $dobError = 'Please enter date of birth'; $valid = false;}
		if (empty($password)) { $passwordError = 'Please enter password'; $valid = false;}
		if (empty($gender)) { $genderError = 'Please choose a gender'; $valid = false;}
   		if (empty($agree)) { $agreeError = 'You must agree with the terms and conditions'; $valid = false;}

		// Insert User Details Data.
		if ($valid) 
		{
          	$signedupdate  = date('Y-m-d');
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// ---------------- User ---------------
			//User - UserName & E-mail Duplicate Validation
			$query = "SELECT * FROM $tbl_user WHERE username = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($username));
			
			if ($q->rowCount() >= 1)
			{$usernameError = 'username already exists.'; $valid = false;}
			
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
			
			// -- if all valid insert user details.
			if ($valid) 
			{
				$sql = "INSERT INTO $tbl_user(username,title,firstname,surname,gender,email,phonenumber,dob,password,disclaimer,signedupdate,country,province) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
				$q = $pdo->prepare($sql);
				$q->execute(array($username,$title,$firstname,$surname,$gender,$email,
                  $phone,$dob,$password,$agree,$signedupdate,$country,$province ));
				
				echo"<script>window.open('index.php/login-welcome','_self')</script>";  
			}
		
			Database::disconnect();
		}
		
	}	
?>

	<div class="w3-row-padding">
       <div class="w3-col m12">
		 	<div class="w3-card-2 w3-round w3-white">
			    <form action="home-secretly-confess" method="post">
				<fieldset>  
                            <div class="form-group">  
                                <input class="form-control <?php echo !empty($usernameError)?'error':'';?>" placeholder="Username(Fake Name)" id="username" name="username" type="text" value="<?php echo !empty($username)?$username:'';?>" autofocus>  
								<?php if(!empty($usernameError))
 										{
   											echo "<span class='help-inline'>$usernameError</span>";
 										}
								?>
								
                            </div>  
  
							<div class="form-group"> 	
								<label>Title</label>
								<SELECT  id="title" class="form-control" name="title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
								</SELECT>
							</div>
							
							<div class="form-group">  
                                <input class="form-control <?php echo !empty($firstnameError)?'error':'';?>" placeholder="Name" id="firstname" name="firstname" type="text" value="<?php echo !empty($firstname)?$firstname:'';?>" autofocus> 

                              <?php if(!empty($firstnameError))
 										{
   											echo "<span class='help-inline'>$firstnameError</span>";
 										}
								?>
                          </div>		
                              							<div class="form-group">  

								<input class="form-control <?php echo !empty($surnameError)?'error':'';?>" placeholder="Surname" id="surname" name="surname" value="<?php echo !empty($surname)?$surname:'';?>" type="text" autofocus> 
                              
                              <?php if(!empty($surnameError))
 										{
   											echo "<span class='help-inline'>$surnameError</span>";
 										}
								?>
								
                            </div>  
  
                            <div class="form-group">  
                                <input class="form-control <?php echo !empty($emailError)?'error':'';?>" placeholder="E-mail" id="email" name="email" value="<?php echo !empty($email)?$email:'';?>" type="email" autofocus>  
							 <?php if(!empty($emailError))
 										{
   											echo "<span class='help-inline'>$emailError</span>";
 										}
								?>	
                            </div>  
                          
							<div class="form-group">  
								<input class="form-control <?php echo !empty($phoneError)?'error':'';?>" placeholder="Phone number" id="phone" name="phone" value="<?php echo !empty($phone)?$phone:'';?>" type="text" autofocus>  
                              	<?php if(!empty($phoneError))
 										{
   											echo "<span class='help-inline'>$phoneError</span>";
 										}
								?>	
                              
							</div>
						
                          <div class="form-group">  
                                <input class="form-control <?php echo !empty($passwordError)?'error':'';?>" placeholder="Password" id="password" name="password" type="password" value="<?php echo !empty($password)?$password:'';?>"> 
                               <?php if(!empty($passwordError))
 										{
   											echo "<span class='help-inline'>$passwordError</span>";
 										}
								?>
                              	
                            </div>  
<!--------------------------- Country ---------------------------->   
<!-- Country dropdown list by ahandy --!>
<div class="form-group"> 	
<label>Country</label>
<SELECT  id="country" class="form-control" name="country">
    <option value="Afghanistan">Afghanistan</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antartica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo">Congo, the Democratic Republic of the</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
    <option value="Croatia">Croatia (Hrvatska)</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="East Timor">East Timor</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="France Metropolitan">France, Metropolitan</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-Bissau">Guinea-Bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
    <option value="Holy See">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran">Iran (Islamic Republic of)</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
    <option value="Korea">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macau">Macau</option>
    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia">Micronesia, Federated States of</option>
    <option value="Moldova">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russia">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
    <option value="Saint LUCIA">Saint LUCIA</option>
    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option> 
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia (Slovak Republic)</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa" selected>South Africa</option>
    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
    <option value="Span">Spain</option>
    <option value="SriLanka">Sri Lanka</option>
    <option value="St. Helena">St. Helena</option>
    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syria">Syrian Arab Republic</option>
    <option value="Taiwan">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Vietnam">Viet Nam</option>
    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Yugoslavia">Yugoslavia</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
</select>
</div>
<!--------------------------- End Country ------------------------> 
<!--------------------------- Province ------------------------>  
                          
<div class="form-group"> 	
<label>Country</label>
<SELECT  id="province" class="form-control" name="province">
    <option value="Eastern Cape">Eastern Cape</option>
    <option value="Gauteng">Gauteng</option>
    <option value="Free State">Free State</option>
    <option value="Kwazulu Natal">Kwazulu Natal</option>
    <option value="Mpumalanga">Mpumalanga</option>
    <option value="Northern Cape">Northern Cape</option>
    <option value="North West">North West</option>
    <option value="Limpopo">Limpopo</option>
    <option value="Western Cape">Western Cape</option>
	<option value="Other">Other</option>
</SELECT> 
</div>           
<!--------------------------- End Province ------------------------>  
                          
							<div style="margin-top:10px" class="form-group">
								<div class="col-sm-20 controls">  
								<label>Date of birth</label><input class="form-control <?php echo !empty($dobError)?'error':'';?>" placeholder="yyyy-mm-dd" id="dob" name="dob" type="Date" value="<?php echo !empty($dob)?$dob:'';?>"autofocus>  
								
                                <?php if(!empty($dobError))
 										{
   											echo "<span class='help-inline'>$dobError</span>";
 										}
								?>
							</div>
							</div>
                          
				<div style="margin-top:10px" class="form-group <?php echo !empty($genderError)?'error':'';?>">
				<label>Gender</label>
				<div class="col-sm-20 controls">
                  <input type="radio" name="gender" value="male" <?php if ($gender == 'male') echo 'checked'; ?>/> Male
                  <input type="radio" name="gender" value="female" <?php if ($gender == 'female') echo 'checked'; ?>/> Female
                  <input type="radio" name="gender" value="other" <?php if ($gender == 'other') echo 'checked'; ?>/> Other
				</div>

                  <?php if(!empty($genderError))
 						{
   						  echo "<span class='help-inline'>$genderError</span>";
 						}
					?>
              	</div>
           
 <!-- start Modise T&C -->   
                          <div class="form-group">
        <label>Terms of use</label>
        <div class="col-xs-20">
            <div style="border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;">
                <p>Lorem ipsum dolor sit amet, veniam numquam has te. No suas nonumes recusabo mea, est ut graeci definitiones. His ne melius vituperata scriptorem, cum paulo copiosae conclusionemque at. Facer inermis ius in, ad brute nominati referrentur vis. Dicat erant sit ex. Phaedrum imperdiet scribentur vix no, ad latine similique forensibus vel.</p>
                <p>Dolore populo vivendum vis eu, mei quaestio liberavisse ex. Electram necessitatibus ut vel, quo at probatus oportere, molestie conclusionemque pri cu. Brute augue tincidunt vim id, ne munere fierent rationibus mei. Ut pro volutpat praesent qualisque, an iisque scripta intellegebat eam.</p>
                <p>Mea ea nonumy labores lobortis, duo quaestio antiopam inimicus et. Ea natum solet iisque quo, prodesset mnesarchum ne vim. Sonet detraxit temporibus no has. Omnium blandit in vim, mea at omnium oblique.</p>
                <p>Eum ea quidam oportere imperdiet, facer oportere vituperatoribus eu vix, mea ei iisque legendos hendrerit. Blandit comprehensam eu his, ad eros veniam ridens eum. Id odio lobortis elaboraret pro. Vix te fabulas partiendo.</p>
                <p>Natum oportere et qui, vis graeco tincidunt instructior an, autem elitr noster per et. Mea eu mundi qualisque. Quo nemore nusquam vituperata et, mea ut abhorreant deseruisse, cu nostrud postulant dissentias qui. Postea tincidunt vel eu.</p>
                <p>Ad eos alia inermis nominavi, eum nibh docendi definitionem no. Ius eu stet mucius nonumes, no mea facilis philosophia necessitatibus. Te eam vidit iisque legendos, vero meliore deserunt ius ea. An qui inimicus inciderint.</p>
            </div>
        </div>
    </div>

    <div class="form-group <?php echo !empty($agreeError)?'error':'';?>">
        <!--<div class="col-xs-20 col-xs-offset-3">-->
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="agree" value="agree" <?php if ($agree == 'agree') echo 'checked'; ?> /><a href="index.php/termsofuse" target="_blank">I Agree with the terms and conditions</a>
                </label>
              <?php if(!empty($agreeError))
 						{
                      echo "</br><span class='help-inline'>$agreeError</span>";
 						}
			  ?>
            </div>
        <!--</div>-->
    </div>
<!-- Modise T&C -->              
                            <button class="btn btn-lg btn-success btn-block" id="register" name="register"  type='submit'>Register</button>
                            <!-- <input class="btn btn-lg btn-success btn-block" type="submit" value="register" name="register" > -->  
                        </fieldset>  
				</form> 		
	   </div>
	</div>
</div>


<html>  
<head lang="en">  
    <meta charset="UTF-8">  
    <link type="text/css" rel="stylesheet" href="bootstrap-3.2.0-dist\css\bootstrap.css">  
    <title>Registration</title>  
</head>  
<style>  
    .login-panel {  
        margin-top: 150px;  
  
</style>  
<body>  
  
<div class="container"><!-- container class is used to centered  the body of the browser with some decent width-->  
    <div class="row"><!-- row class is used for grid system in Bootstrap-->  
        <div class="col-md-4 col-md-offset-4"><!--col-md-4 is used to create the no of colums in the grid also use for medimum and large devices-->  
            <div class="login-panel panel panel-success">  
                <div class="panel-heading">  
                    <h3 class="panel-title">Registration</h3>  
                </div>  
                <div class="panel-body">  
                    <form role="form" method="post" action="registration.php">  
                        <fieldset>  
                            <div class="form-group">  
                                <input class="form-control" placeholder="Username" name="name" type="text" autofocus>  
                            </div>  
  
                            <div class="form-group">  
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>  
                            </div>  
                            <div class="form-group">  
                                <input class="form-control" placeholder="Password" name="pass" type="password" value="">  
                            </div>  
  
  
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="register" name="register" >  
  
                        </fieldset>  
                    </form>  
                    <center><b>Already registered ?</b> <br></b><a href="login.php">Login here</a></center><!--for centered text-->  
                </div>  
            </div>  
        </div>  
    </div>  
</div>  
  
</body>  
  
</html>  
  
<?php  
  
//include("database/db_conection.php");//make connection here  
if(isset($_POST['register']))  
{  
    $user_name=$_POST['name'];//here getting result from the post array after submitting the form.  
    $user_pass=$_POST['pass'];//same  
    $user_email=$_POST['email'];//same  
  
  
    if($user_name=='')  
    {  
        //javascript use for input checking  
        echo"<script>alert('Please enter the name')</script>";  
exit();//this use if first is not work then other will not show  
    }  
  
    if($user_pass=='')  
    {  
        echo"<script>alert('Please enter the password')</script>";  
exit();  
    }  
  
    if($user_email=='')  
    {  
        echo"<script>alert('Please enter the email')</script>";  
    exit();  
    }  
//here query check weather if user already registered so can't register again.  
    $check_email_query="select * from users WHERE user_email='$user_email'";  
    //$run_query=mysqli_query($dbcon,$check_email_query);  
  
    if(mysqli_num_rows($run_query)>0)  
    {  
echo "<script>alert('Email $user_email is already exist in our database, Please try another one!')</script>";  
exit();  
    }  
//insert the user into the database.  
   // $insert_user="insert into users (user_name,user_pass,user_email) VALUE ('$user_name','$user_pass','$user_email')";  
    //if(mysqli_query($dbcon,$insert_user))  
    {  
        echo"<script>window.open('welcome.php','_self')</script>";  
    }  
}  
  
?>  