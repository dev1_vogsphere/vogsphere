<?php
require 'database.php';
 $paysingle = ''; 
if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}
if(!function_exists('create_benefiary'))
{
	function create_benefiary($data)
	{
		$id  = 0;
		try
		{
			$tbl_user = 'benefiary';
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sqlData = sql_associative_array($data);
			$sql = "INSERT INTO $tbl_user
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$id = $pdo->lastInsertId();	
			$pdo->commit();
	    }
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $id;	
	}
}
if(!function_exists('get_benefiary'))
{
	function get_benefiary($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'benefiary';			
			$sql = "select * from $tbl_2 where 
			Account_Number  = ? and Account_Type  = ? and Branch_Code = ? and IntUserCode = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['Account_Number'],$dataFilter['Account_Type'],
							  $dataFilter['Branch_Code'],$dataFilter['IntUserCode']));
			$data = $q->fetch(PDO::FETCH_ASSOC);			  
			
		return $data;
	}
}	

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$message = '';
$list = getpost('list');
$list['createdon'] = date('Y-m-d');
$list['createdat'] = date('h:m:s');
$dataBenefiaryOb = null;

// -- Check Benefiary
$dataBenefiaryObj = get_benefiary($list);

if(!isset($dataBenefiaryObj['benefiaryid']))
{
	// -- Create Benefiary	
   $benefiaryid = create_benefiary($list);	
	// -- Benefiary does exist.
	$message = 'success|'.$benefiaryid;					
}
else
{
	// -- Benefiary does exist.
	$message = 'Beneficiary record already exist with those details.';				
}
 echo $message; 
//print_r($dataBenefiaryObj['benefiaryid']); 
 //print_r($list);
?>