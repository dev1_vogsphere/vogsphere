<div class="row setup-content" id="step-3">

	<div class="col-xs-12">
		<div class="col-md-12">
			    <div class="messages">
				  <p id="message_extended" name="message_extended" style="margin: 8px 0;font-size: 12px;"></p>
				</div>
		<div class="card">
	  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	    Dependant Detials
	  </h3>
	  <div class="card-body">
	    <div id="dependent1" class="table-editable">
	      <span class="table-add float-right mb-3 mr-2"
	        ><a href="#!" class="text-success"
	          ><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a
	      ></span>
	      <table id="dependent" name="dependent" class="table table-bordered table-responsive-md table-striped text-center">
	        <thead>
	          <tr>
	            <th class="text-center">First Name</th>
	            <th class="text-center">Middle Name</th>
	            <th class="text-center">Surname</th>
	            <th class="text-center">Identity Number</th>
	            <th class="text-center">Relationship</th>
				<th class="text-center">Benefits</th>

	          </tr>
			  <?php echo $tableChildren;?>
	        </thead>
	        <tbody>
	        </tbody>
	      </table>
	    </div>
	  </div>
 <!--Extended family-->

 <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	 Extended Family Details
 </h3>
 <div class="card-body">
	 <div id="extendend1" class="table-editable">
		 <span class="table2-add float-right mb-3 mr-2"
			 ><a href="#!" class="text-success"
				 ><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a
		 ></span>
		 <table id="extendend" name="extendend" class="table table-bordered table-responsive-md table-striped text-center">
			 <thead>
				 <tr>
					 <th class="text-center">First Name</th>
					 <th class="text-center">Middle Name</th>
					 <th class="text-center">Surname</th>
					 <th class="text-center">Identity Number</th>
					 <th class="text-center">Relationship</th>
					 <th class="text-center">Benefits</th>

				 </tr>
				 <?php echo $tableExtendedFamilyMembers;?>
			 </thead>
			 <tbody>
			 </tbody>
		 </table>
	 </div>
 </div>

 <!--Nominated benefiaciary-->
 <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
	Nominated Benefiaciary
 </h3>
 <div class="card-body">
	 <div id="benefiaciary1" class="table-editable">
		 <span class="table3-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
		 <table id="benefiaciary" name="benefiaciary" class="table table-bordered table-responsive-md table-striped text-center">
			 <thead>
			    <tr>
					 <th class="text-center">First Name</th>
					 <th class="text-center">Middle Name</th>
					 <th class="text-center">Surname</th>
					 <th class="text-center">Identity Number</th>
					 <th class="text-center">Relationship</th>
					 <th class="text-center">Percentage</th>
				</tr>
				 <?php echo $tableBeneficiaries;?>				 
			 </thead>
			 <tbody>
				<!--	 <tr>
					<td class="pt-3-half" contenteditable="true"></td>
	           		<td class="pt-3-half" contenteditable="true"></td>
	            	<td class="pt-3-half" contenteditable="true"></td>
	            	<td class="pt-3-half" contenteditable="true"></td>
	            	<td class="pt-3-half" contenteditable="true"></td>
					 <td class="pt-3-half" contenteditable="true"></td>
					 <td>
						 <span class="table-remove"
							 ><button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
								 Remove
							 </button></span
						 >
					 </td>
				 </tr>-->

			 </tbody>
		 </table>
	 </div>
 </div>

	</div>	<!--CLose card-->

		</br>
    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
		<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>
