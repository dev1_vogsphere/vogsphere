<?php
if(!function_exists('phonemasks'))
{
	function phonemasks($data)
	{
		$result = '';
		if(  preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', $data,  $matches ) )
		{
			//$result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
			 $result = '***-***-'.$matches[3];
		}
		return $result;
}}
$phoneMasked = '';
if(isset($_SESSION['Customerphone']))
{
	$phoneMasked = $_SESSION['Customerphone'];
	$phoneMasked = phonemasks($phoneMasked);
}
?>
<div class="row margin-vert-30" style="<?php echo $displaytwofactors;?>" >
	<h2 class="text-center">Authenticate Your Account</h2>
	<br>
	<h4>
	 Protecting your account is our top priority. Please confirm your account by entering the 6-digit authorization code sent to [<?php echo $phoneMasked; ?>].
 </h4>
 	<br>
<div class="col-md-8 col-md-offset-2 col-sm-offset-2">
			<div class="login-header margin-bottom-30">
				<div class='control-group <?php echo !empty($fourdigitsError)?'error':'';?>'>
					<input style='height:30px' class='form-control margin-bottom-20' id='fourdigits' name='fourdigits' type='number' pattern="/^-?\d+\.?\d*$/" onkeypress="if(this.value.length==6) return false;" placeholder='6-digits code' value="<?php echo !empty($fourdigits)?$fourdigits:'';?>">
					<?php if (!empty($fourdigitsError)): ?>
					<span class="help-inline"><?php echo $fourdigitsError; ?></span>
					<?php endif; ?>
					<div class="row">
						<div class="col-md-6">
								<!--<button class="btn btn-primary pull-right" value="resentsms" name="resentsms" type="submit">Resent via SMS</button>-->
							</div>
						 <div class="col-md-6">
							 <button class="btn btn-primary pull-right" value="VerifyLoginForm" name="VerifyLoginForm" type="submit">Submit</button>
						 </div>
						 <br>
						  <br>
								<h4>
								 It may take a minute to receive your code. Haven't receive it? <button class="bootstrap-link" value="resentsms" name="resentsms" type="submit">Resend</button>
								<!-- <button class="btn btn-primary pull-right" value="resentsms" name="resentsms" type="submit">Resend</button>-->
							 </h4>
					</div>

				</div>
			</div>
	</div>


<!--	<div class="col-md-6">
		<button class="btn btn-primary pull-right" value="resentsms" name="resentsms" type="submit">Resent via SMS</button>
	</div>-->


</div>
