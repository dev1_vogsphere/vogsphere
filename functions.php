<?php
// -- BOC 2017.06.24
// -- To avoid : Fatal error: Cannot redeclare class.
// -- EOC 2017.06.24
$pdo = null;
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Read User.
if (!function_exists('readuser')) 
{
	function readuser($userid)
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT * FROM user WHERE userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($userid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- Account/Reference must be the 9 digits derived from the ID number.
// -- Reading from the ID backward.  
if (!function_exists('GenerateAccountNumber')) 
{
function GenerateAccountNumber($customerid)
{
   $AccountNumber = '';
   
   if(!empty($customerid))
   {
     $AccountNumber = strrev($customerid);
	 
	 // -- 9 digits.
	 $AccountNumber = substr($AccountNumber,0,9); 
   }
   return $AccountNumber;
}
}

if (!function_exists('createcollections')) 
{
	function createcollections($arrayCollections)
	{
	try 
	  {
		Global $pdo;  
		$sql = "INSERT INTO collection
		(Client_ID,
		Client_Reference_1,
		Client_Reference_2,
		Client_ID_Type,
		Client_ID_No,
		Initials,
		Account_Holder,
		Account_Type,
		Account_Number,
		Branch_Code,
		No_Payments,
		Service_Type,
		Service_Mode,
		Frequency,
		Action_Date,
		Bank_Reference,
		Amount,
		Status_Code,
		Status_Description,
		mandateid,
		mandateitem)
		VALUES
		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$q = $pdo->prepare($sql);
		$q->execute(array($arrayCollections[1],
		$arrayCollections[2],
		$arrayCollections[3],
		$arrayCollections[4],
		$arrayCollections[5],
		$arrayCollections[6],
		$arrayCollections[7],
		$arrayCollections[8],
		$arrayCollections[9],
		$arrayCollections[10],
		$arrayCollections[11],
		$arrayCollections[12],
		$arrayCollections[13],
		$arrayCollections[14],
		$arrayCollections[15],
		$arrayCollections[16],
		$arrayCollections[17],
		$arrayCollections[18],
		$arrayCollections[19],
		$arrayCollections[20],
		$arrayCollections[21]));
		Database::disconnect();
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}	
}

if (!function_exists('updatecollections')) 
{
	function updatecollections($arrayCollections)
	{
	 try 
	  {
		Global $pdo;
		$sql = "UPDATE collection
		SET
		Client_ID = ?,
		Client_Reference_1 = ?,
		Client_Reference_2 = ?,
		Client_ID_Type = ?,
		Client_ID_No = ?,
		Initials = ?,
		Account_Holder = ?,
		Account_Type = ?,
		Account_Number = ?,
		Branch_Code = ?,
		No_Payments = ?,
		Service_Type = ?,
		Service_Mode = ?,
		Frequency = ?,
		Action_Date = ?,
		Bank_Reference = ?,
		Amount = ?,
		Status_Code = ?,
		Status_Description = ?,
		mandateid = ?,
		mandateitem = ?
		WHERE collectionid = ?;";
		$q = $pdo->prepare($sql);
		
		$q->execute(array(
		$arrayCollections[2],
		$arrayCollections[3],
		$arrayCollections[4],
		$arrayCollections[5],
		$arrayCollections[6],
		$arrayCollections[7],
		$arrayCollections[8],
		$arrayCollections[9],
		$arrayCollections[10],
		$arrayCollections[11],
		$arrayCollections[12],
		$arrayCollections[13],
		$arrayCollections[14],
		$arrayCollections[15],
		$arrayCollections[16],
		$arrayCollections[17],
		$arrayCollections[18],
		$arrayCollections[19],
		$arrayCollections[20],
		$arrayCollections[21],
		$arrayCollections[22],
		$arrayCollections[1]));
		Database::disconnect();
		} 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('deletecollections')) 
{
	function deletecollections($collectionid)
	{
	  try 
	  {	
	  	Global $pdo;
		$sql = "DELETE FROM collection
		WHERE collectionid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($collectionid));
		Database::disconnect();
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readcollections')) 
{
	function readcollections()
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description FROM Collection ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute();		
		$data = $q->fetchAll();
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readcollection')) 
{
	function readcollection($collectionid)
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description FROM Collection WHERE collectionid = ? ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute(array($collectionid));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('readPaymentschedule')) 
{
	function readPaymentschedule($customerid,$applicationid,$item)
	{
	 try
	 {
		Global $pdo;
		$sql = "SELECT collectionid,Client_ID,Client_Reference_1,Account_Holder,Branch_Code,Account_Number,Service_Type,Service_Mode,Amount,Action_Date,Status_Code,Status_Description,mandateid,mandateitem FROM Collection WHERE Client_ID = ? AND mandateid = ? AND mandateitem = ? ORDER BY Action_Date DESC";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$applicationid,$item));		
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if (!function_exists('GenerateAccountNumber')) 
{
	// -- Account/Reference must be the 9 digits derived from the ID number.
	// -- Reading from the ID backward.  
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';
	   
	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);
		 
		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9); 
	   }
	   return $AccountNumber;
	}
}

if (!function_exists('GetPaymentSchedule')) 
{
	function GetPaymentSchedule($ApplicationId)
	{
	  try
	  {	
		Global $pdo;
		$tbl_name ="paymentschedule"; 
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_name where ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($ApplicationId));		
		$data = $q->fetchAll();
		
		Database::disconnect();
		return $data;
	  } 
	  catch (Exception $e) 
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}


if (!function_exists('redirect')) 
{

// -- Hash Every Page.
function redirect($url) 
{
    ob_start();
	$hash="?guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}
}

// -- Hash to Page with parameters.
if (!function_exists('redirectParam')) 
{
	function redirectParam($url) 
	{
		ob_start();
		$hash="&guid=".md5(date("h:i:sa"));
		header('Location: '.$url.$hash);
		ob_end_flush();
		die();
	}
}
// -- Redirect to Login Page with not logged in.
if (!function_exists('contains')) 
{
	// -- Contains Characters
	// returns true if $needle is a substring of $haystack
	function contains($needle, $haystack)
	{
		return strpos($haystack, $needle) !== false;
	}
}

// -- html decode
if ( !function_exists('htmlspecialchars_decodePayCollexions') )
{
    function htmlspecialchars_decodePayCollexions($text)
    {
        return strtr($text, array_flip(get_html_translation_table(HTML_SPECIALCHARS)));
    }
}

// -- Check Security against Data Injection.
if(!function_exists('htmli'))
{
	function htmli($data)
	{
		switch($_COOKIE["security_level"])
		{
			 //security_level Low
			case "0" : 
				$data = no_check($data);            
				break;
			//security_level medium
			case "1" :
				$data = xss_check_1($data);
				break;
			//security_level high
			case "2" :            
				$data = xss_check_3($data);            
				break;
			default : 
				$data = no_check($data);            
				break;   
		}       
		return $data;
	}
}

if(!function_exists('xss_check_3'))
{
	function xss_check_3($data, $encoding = "UTF-8")
	{
		// htmlspecialchars - converts special characters to HTML entities    
		// '&' (ampersand) becomes '&amp;' 
		// '"' (double quote) becomes '&quot;' when ENT_NOQUOTES is not set
		// "'" (single quote) becomes '&#039;' (or &apos;) only when ENT_QUOTES is set
		// '<' (less than) becomes '&lt;'
		// '>' (greater than) becomes '&gt;'  
		return htmlspecialchars($data, ENT_QUOTES, $encoding);//ENT_QUOTES
	}
}
// -- Send an Email
if(!function_exists('SendEmail'))
{
	// ------------ Start Send an Email ---------------- //
	function SendEmail($emailContent,$emailAdd,$title)
	{
			require_once('class/class.phpmailer.php');

			Global $adminemail;
			Global $companyname;
			

			try
			{
				$email = '';		
	// -- Email Server Hosting -- //
		$Globalmailhost = $_SESSION['Globalmailhost'];
		$Globalport = $_SESSION['Globalport'];
		$Globalmailusername = $_SESSION['Globalmailusername'];
		$Globalmailpassword = $_SESSION['Globalmailpassword'];
		$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
		$Globaldev = $_SESSION['Globaldev'];
		$Globalprod = $_SESSION['Globalprod'];		
		
	// -- Email Server Hosting -- //	
	/*	Production - Live System */
		if($Globalprod == 'p')
		{
			$mail=new PHPMailer();
			$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
			$mail->SetFrom($adminemail,$companyname);
			$mail->AddReplyTo($adminemail,$companyname);
		}
	/*	Development - Testing System */	
		else
		{
				$mail=new PHPMailer();
				$mail->IsSMTP();
				$mail->SMTPDebug=1;
				$mail->SMTPAuth=true;
				$mail->SMTPSecure=$GlobalSMTPSecure;
				$mail->Host=$Globalmailhost;
				$mail->Port= $Globalport;
				$mail->Username=$Globalmailusername;
				$mail->Password=$Globalmailpassword;
		}	
				$mail->SetFrom($adminemail,$companyname);
			
						// -- The Code
						$mail->Subject =  $title;
						//ob_start();
						//include 'text_emailForgottenPassword.php';
						$body = $emailContent;//ob_get_clean();					
						$fullname = $email;//$FirstName.' '.$LastName;
						$email = $emailAdd;//$_SESSION['email'];
						$mail->MsgHTML($body );
						$mail->AddAddress($email, $fullname);
						$mail->AddBCC($adminemail, $companyname);

						if($mail->Send()) 
						{
						//$successMessage = "Your password recovery key has been sent to your e-mail address.";
						$successMessage = "INFO-Email was sent to the successful.";
						}
						else 
						{
						  // echo 'mail not sent';
						$successMessage = "ERROR-Your user name is valid but your e-mail address seems to be invalid!";
						}
				}		
				catch(phpmailerException $e)
				{
					$successMessage = $e->errorMessage();
				}
				catch(Exception $e)
				{
					$successMessage = $e->getMessage();
				}	
	}
	
}

// -- Add Email/SMS History.
if(!function_exists('AddCommunicatioHistory'))
{
	function AddCommunicatioHistory($smstemplatecontent,$customerid,$refnumber,$userid,$status,$email,$phone)
	{
		Global $templatetype;
		$historydate = date("Y-m-d");
		$historytime = date("h:i:sa");
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO sentcomm_history
	(sentcomm_historytype,sentcomm_historydate,sentcomm_historytime,message,customerid,refnumber,email,phone,status,userid) VALUES (?,?,?,?,?,?,?,?,?,?)";
		$q = $pdo->prepare($sql);
		$q->execute(array($templatetype,$historydate,$historytime,$smstemplatecontent,$customerid,$refnumber,$email,$phone,$status,$userid));
	}
}

// -- Add Email/SMS History.
if(!function_exists('AddCommunicatioHistoryGlobal'))
{
	function AddCommunicatioHistoryGlobal($smstemplatecontent,$customerid,$refnumber,$userid,$status,$email,$phone,$templatetype)
	{
		$historydate = date("Y-m-d");
		$historytime = date("h:i:sa");
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO sentcomm_history
	(sentcomm_historytype,sentcomm_historydate,sentcomm_historytime,message,customerid,refnumber,email,phone,status,userid) VALUES (?,?,?,?,?,?,?,?,?,?)";
		$q = $pdo->prepare($sql);
		$q->execute(array($templatetype,$historydate,$historytime,$smstemplatecontent,$customerid,$refnumber,$email,$phone,$status,$userid));
	}
}
?>