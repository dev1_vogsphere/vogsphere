<?php 
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Table name - PaymentSchedule.  
$tbl_name ="paymentschedule"; 
$ApplicationId = 0;

// -- PaymentSchedule
$item = 0;
$scheduleddate = null;
$days = 0;
$paidby = null;
$disbursement = 0.00;

$disbursementdate = null;
$amountdue = null;
$balance = null;
$interestdue = null;
$fees = null;
$penalty = null;
$Duration = 0;
$loanedInterestAmt = 0.00;
$customername = '';
$customerid = '';
$repayment = 0.00;
$interest = 0;
$chargeid = 0;

$HTML = "";
$display = '';
 
// -- Duration.
if(isset($_POST['Duration']))
{
	$Duration = $_POST['Duration'];
}

// -- ApplicationId.
if(isset($_POST['ApplicationId']))
{
	$ApplicationId = $_POST['ApplicationId'];
}

// -- First Payment Date.
if(isset($_POST['FirstPaymentDate']))
{
	$scheduleddate = $_POST['FirstPaymentDate'];
}

// -- Disbursement Amount.
if(isset($_POST['ReqLoadValue']))
{
	$disbursement = $_POST['ReqLoadValue'];
}

// -- Instalment Amount.
if(isset($_POST['monthlypayment']))
{
	$amountdue = $_POST['monthlypayment'];
}

// -- Disbursement Date.
if(isset($_POST['DateAccpt']))
{
   $disbursementdate = $_POST['DateAccpt'];
}

// -- customerid.
if(isset($_POST['customerid']))
{
   $customerid = $_POST['customerid'];
}

// -- customername.
if(isset($_POST['customername']))
{
   $customername = $_POST['customername'];
}

// Loan amount with Interest.
if(isset($_POST['repayment']))
{
   $repayment = $_POST['repayment'];
}

// Display - Payment Schedules
if(isset($_POST['display']))
{
   $display = $_POST['display'];
}


// -- Interest Rate.
if(isset($_POST['interest']))
{
   $interest = $_POST['interest'];
}

if (!empty($_POST)) 
{   

		$valid = true;
	 
		//$data = $q->fetch(PDO::FETCH_ASSOC);
		//$ApplicationId = $data['ApplicationId'];

	 	//if ($q->rowCount() >= 1)
		//{$valid = false;}
		// -- Display = 'X' display ONLY, display = space(Display and Create Payment Schedule)
		if(empty($display ))
		{
			// -- Create a Disbursement Entry.
			CreatePaymentSchedules(-1,$ApplicationId,$disbursementdate,$disbursement,'0.00',$repayment);
			
					//echo "valid = ".$valid;

			if ($valid) 
			{	
				for ($i = 0; $i < $Duration; $i++)
				{
					CreatePaymentSchedules($i,$ApplicationId,$scheduleddate,$disbursement,$amountdue,$repayment);
				}
			}
		}
	// -- Display ONLY, must go search for an ApplicationId & customer Details.
		else
		{
		
		// -- $ApplicationId - Get the Payment ID.
				$sql = 'SELECT * FROM loanapp as L WHERE ApplicationId = ?';
				$q = $pdo->prepare($sql);
				$q->execute(array($ApplicationId));	
				$dataLoanApp = $q->fetch(PDO::FETCH_ASSOC); 

				// -- Get Customer Data name.
				$sql = 'SELECT * FROM customer WHERE CustomerId = ?';
				$q = $pdo->prepare($sql);
				$q->execute(array($dataLoanApp['CustomerId']));
				$dataCustomer = $q->fetch(PDO::FETCH_ASSOC); 
				
				$FirstName = $dataCustomer['FirstName'];
				$LastName = $dataCustomer['LastName'];
				$Title = $dataCustomer['Title'];
				
				$customerid = $dataLoanApp['CustomerId'];
				$customername = $Title.'.'.$FirstName.' '.$LastName;
				$interest = $dataLoanApp['InterestRate'];
				$Duration = $dataLoanApp['LoanDuration'];
				$disbursement = $dataLoanApp['ReqLoadValue'];
				$repayment = $dataLoanApp['Repayment'];
				$disbursementdate = $dataLoanApp['DateAccpt'];				
		}
					// -- PaymentSchedule Clients Details.
					//Client name: , Interest Rate : , Loan Duration : , Report Run Date : , Loan Requested Amount, Loan Repayment Amount with Interest:, Date Approved:, Customer ID:, Status: 
					// -- PaymentSchedule - fields.
						$item = 0;
						$repoRunDate = DATE('Y-m-d');
						$days = 0;
						$Totaldays = 0;
						$paidby = '';
						$amountdue = 0;
						$balance = 0;
					// -- Display on Screen.	
						$Displaybalance = 0;
						$DisplayTotalDue = 0;
						$DisplayTotalOutstanding = 0;
						$Displayamountpaid = 0;
						$Displayfees = 0;
						$Displayinterestdue = 0;
						$Displaypenalty = 0;
							
						$interestdue = 0;
						$fees = 0;
						$penalty = 0;
						$valid = true;
						$amountpaid = '0.00';
						$interest = number_format($interest,2);
						
						$TotalOutstanding = '0.00';
						$TotalPaid = '0.00';
					    $TotalDue = '0.00';
						
						// -- Totals.
						$Totaldisbursement = 0;
						$Totalamountdue = 0;
						$Totalbalance = 0;
						$Totalinterestdue = 0;
						$Totalfees = 0;
						$Totalpenalty = 0;
						$Totalamountpaid = 0;
						$TotalsOutstanding = 0;
						$TotalsPaid = 0;
						$TotalsDue = 0.00;
						
						// -- TotalsAmtPaid -- //
						$TotalsAmtPaid = GetPayments($customerid,$ApplicationId,$repayment);
						
						$pdo = Database::connect();
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$sql =  "select * from $tbl_name where ApplicationId = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($ApplicationId));		
						$data = $q->fetchAll();

					$HTML = '	<link href="assets/css/bootstrap.min.css" rel="stylesheet">

					<link href="media/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="resources/demo.css">
	<style type="text/css" class="init">
	div.dataTables_wrapper {margin-bottom: 3em;}
	</style>		
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
	</script>
	<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="resources/demo.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
	$("table.display").DataTable();
} );
	</script>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
		        <link rel="stylesheet" href="assets/css/font-awesome.css" rel="stylesheet">

	
';
					$RowCount  = 0;
					$entries = 10;
					$LastRow = $q->rowCount();
					
						
						
						$HTML = $HTML.'<div style="overflow-y:auto;" style="z-index:10000;">';
								//<div class="row" style="overflow-y:auto;font-size:10px;">
$HTML = $HTML.'
						<table class="table table-striped table-bordered" style="white-space: nowrap;">
						<tr><td><b>ID:</b> '.$customerid.'<b> Name:</b> '.$customername.'</td>
						
						<td><b>Interest Rate :</b> '.$interest.'%<b>  Duration :</b> '.$Duration.' <b>Application ID :</b>'.$ApplicationId.'</td>
						</tr>
						<tr><td><b>Report Date :</b> '.$repoRunDate.'<b>  Requested Amount:</b> '.$disbursement.'</td>
						
						<td><b>Repayment Amount:</b> '.$repayment.'<b>  Date Approved:</b> '.$disbursementdate.'</td>
						</tr>
						</table>';
						
						foreach ($data as $row) 	
						{
						  $RowCount = $RowCount + 1;
						// -- Print Head and Foot. Every 10 record
						  if (($RowCount % 40) == 0) 
						  {
						 // -- Close Table 
						// -- Totals --
						$TotalsOutstanding = number_format($TotalsOutstanding,2);
						$TotalsDue = number_format($TotalsDue,2);
						$TotalsPaid = number_format($TotalsPaid,2);
						//$Totalbalance = number_format($Totalbalance,2);
						$Totalinterestdue = number_format($Totalinterestdue,2);
						$Totalfees = number_format($Totalfees,2);
						$Totalpenalty = number_format($Totalpenalty,2);
						$Totalamountpaid = number_format($Totalamountpaid,2);
						$Totalamountdue  = number_format($Totalamountdue,2);
						$Totalbalance = number_format($balance,2);
						
						//$TotalsDue = number_format($TotalsDue,2);
						$HTML = $HTML."<tr><td></td><td><b><u>Total</u></b></td><td><b><u>$Totaldays</u></b></td>
						<td></td><td><b><u>$Totaldisbursement</u></b></td><td><b><u>$Totalamountdue</u></b></td>
						<td><b><u>$Totalbalance</u></b></td><td><b><u>$Totalinterestdue</u></b></td><td><b><u>$Totalfees</u></b></td>
						
						<td><b><u>$Totalpenalty</u></b></td>
						<td><b><u>$TotalsDue</u></b></td><td><b><u>$TotalsPaid</u></b></td><td><b><u>$TotalsOutstanding</u></b></td></tr>";
							$HTML = $HTML.'</tbody></table>';
							
						 // -- Open a new one	
						  $HTML = $HTML.'
						  <table id="" class="display" cellspacing="0" style="white-space: nowrap;">';

						$HTML = $HTML.'<thead>
						<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
						<th colspan="3">Total Cost of Loan</th></tr> -->';
						$HTML = $HTML.'<tr><th>Item</th><th>Date</th><th>Days</th><th>Paid by</th><th>Disbursement</th>
						<th>Amount Due</th><th>Amount Balance</th><th>Interest Due</th><th>Fees</th><th>Penalties</th>
						<th>Total Due</th><th>Total Paid</th><th>Total Outstanding</th></tr></thead>';

						/*$HTML = $HTML.'<tfoot>
						<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
						<th colspan="3">Total Cost of Loan</th></tr> -->';
						$HTML = $HTML.'<tr><th>Item</th><th>Date</th><th>Days</th><th>Paid by</th><th>Disbursement</th>
						<th>Amount Due</th><th>Amount Balance</th><th>Interest Due</th><th>Fees</th><th>Penalties</th>
						<th>Total Due</th><th>Total Paid</th><th>Total Outstanding</th></tr></tfoot><tbody>';

						  */
						  
						  }
						// -- First Time Print Head and Foot.
						  if ($RowCount == 1) 
						  {
						  						 // -- Open a new one	
						  $HTML = $HTML.'
						  <table id="" class="display" cellspacing="0" style="white-space: nowrap;">';

						$HTML = $HTML.'<thead>
						<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
						<th colspan="3">Total Cost of Loan</th></tr> -->';
						$HTML = $HTML.'<tr><th>Item</th><th>Date</th><th>Days</th><th>Paid by</th><th>Disbursement</th>
						<th>Amount Due</th><th>Amount Balance</th><th>Interest Due</th><th>Fees</th><th>Penalties</th>
						<th>Total Due</th><th>Total Paid</th><th>Total Outstanding</th></tr></thead>';

						/*$HTML = $HTML.'<tfoot>
						<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
						<th colspan="3">Total Cost of Loan</th></tr> -->';
						$HTML = $HTML.'<tr><th>Item</th><th>Date</th><th>Days</th><th>Paid by</th><th>Disbursement</th>
						<th>Amount Due</th><th>Amount Balance</th><th>Interest Due</th><th>Fees</th><th>Penalties</th>
						<th>Total Due</th><th>Total Paid</th><th>Total Outstanding</th></tr></tfoot><tbody>';
							*/
						  }
						
						$item = $row['item']; $scheduleddate = $row['scheduleddate'];$days = $row['days'];
						$paidby = $row['paidby'];$disbursement = $row['disbursement'];$amountdue = $row['amountdue'];
						//$balance = $row['balance'];
						
						$interestdue = $row['interestdue'];$fees = $row['fees'];$penalty = $row['penalty'];
						$amountpaid = $row['amountpaid'];
						// -- if its Disbursement Entry.
						if($item < 0)
						{
						   $TotalOutstanding = $fees;
						   $TotalsOutstanding = $TotalOutstanding + $TotalsOutstanding;
						   $Totaldisbursement = $disbursement;
						   
						   $disbursement = number_format($disbursement,2);
						   $fees = number_format($fees,2);
						   $TotalsOutstanding = number_format($TotalsOutstanding,2);
						   $Totaldisbursement = number_format($Totaldisbursement,2);
						   // -- Fee : Insurance Fee.
						   $chargeid = 1; // -- Will Change once Implemented the UI.
						   $fees = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
						   $fees = number_format($fees,2);

						   // -- $interestdue + 
						   // -- Blank fields not part of disbursement.
						   $amountdue = '';
						   $balance = '';
						   $penalty = '';
						   $amountpaid = 0.00;
						   $TotalDue = '';
						   
						   // -- Adjust the Fees Depending on the paid amount.
						   if ($fees <= $TotalsAmtPaid && $fees != 0)
							{
							   $amountpaid = $fees; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							   $fees  = 0.00; // -- This has been paid.
							}
							elseif($TotalsAmtPaid >0)
							{
							   $amountpaid = $TotalsAmtPaid; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							   $fees  = $fees - $TotalsAmtPaid; // -- This has been paid.
							}
							
						   $days = '';
						   $interestdue = '';
						   
						    $TotalOutstanding  = $fees;
							$TotalsOutstanding = $TotalOutstanding;
						   
						   	//$DisplayTotalDue	 = number_format($TotalDue,2);
							$DisplayTotalOutstanding = number_format($TotalOutstanding,2);
							$Displayamountpaid = number_format($amountpaid,2);
							
							// -- Total Fees.
							$Totalfees =  $fees;
							$Displayfees = number_format($fees,2);
						}
						// -- For all payment schedule.
						else
						{
						  // -- PaymentDaysOverDue
							$days = PaymentDaysOverDue($scheduleddate);
							$Totaldays = $Totaldays + $days;
							
							// -- Fee : Insurance Fee.
							$chargeid = 4; // -- Will Change once Implemented the UI.
							$fees = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
							// -- Interest Due : Bounced Fee.
							$chargeid = 3; // -- Will Change once Implemented the UI.
							$interestdue = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
							// -- Penalties : Over on Instalment.
							$chargeid = 2; // -- Will Change once Implemented the UI.
							$penalty = OverDuePenalty($days,$ApplicationId,$chargeid, $item);
							
						   // -- Populate the Payment Schedule.	
							$TotalDue =  $amountdue + $fees +  $penalty + $interestdue;
							
						   // -- Total Amount Due	
							$Totalamountdue = $Totalamountdue + $amountdue;
							
							// -- 
							$TotalsDue = $TotalDue + $TotalsDue;

							$balance = $balance - $amountdue;
							
							// -- Round to a precision
							//if($balance < 0)
							//{
							  //$balance = 0.00;
							//}
							
							// -- Amount Paid.
							$amountpaid = 0;
							
							if ($TotalDue <= $TotalsAmtPaid && $TotalDue != 0)
							{
							   $amountpaid = $TotalDue; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							}
							elseif($TotalsAmtPaid >0)
							{
							   $amountpaid = $TotalsAmtPaid; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							}
							
							$TotalOutstanding  = ($TotalDue -  $amountpaid);
							$TotalsOutstanding = $TotalsOutstanding + $TotalOutstanding;
							
							// -- Total Fees.
							$Totalfees =  $Totalfees + $fees;
							$Totalpenalty = $Totalpenalty + $penalty;
							$Totalinterestdue = $Totalinterestdue + $interestdue;
							
							//$balance = number_format($balance,2);
							//$TotalsOutstanding = number_format($TotalsOutstanding,2);
							//$TotalOutstanding = number_format($TotalOutstanding,2);
							//$TotalsDue = number_format($TotalsDue,2);
							//$TotalDue = number_format($TotalDue,2);

						    // -- Blank fields not part of paymentschedule.
							$disbursement = '';
							$Displaybalance = number_format($balance,2);
							$DisplayTotalDue	 = number_format($TotalDue,2);
							$DisplayTotalOutstanding = number_format($TotalOutstanding,2);
							$Displayamountpaid = number_format($amountpaid,2);
							$Displayfees = number_format($fees,2);
							$Displayinterestdue = number_format($interestdue,2);
							$Displaypenalty = number_format($penalty,2);
						}
						$HTML = $HTML."
										<tr><td>$item</td>
										<td>$scheduleddate</td>
										<td>$days</td>
										<td>$paidby</td>
										<td>$disbursement</td>
										<td>$amountdue</td><td>$Displaybalance</td><td>$Displayinterestdue</td><td>$Displayfees</td>
										<td>$Displaypenalty</td>
										<td>$DisplayTotalDue</td><td>$Displayamountpaid</td><td>$DisplayTotalOutstanding</td></tr>";
						// -- Total Repayment Amount with Interest Rate.
							if($item < 0)
							{$balance = $repayment;}
							
						// -- Last Row - Please close.
						if($LastRow == $RowCount)
						{
						// -- Totals --
						$TotalsOutstanding = number_format($TotalsOutstanding,2);
						$TotalsDue = number_format($TotalsDue,2);
						$TotalsPaid = number_format($TotalsPaid,2);
						//$Totalbalance = number_format($Totalbalance,2);
						$Totalinterestdue = number_format($Totalinterestdue,2);
						$Totalfees = number_format($Totalfees,2);
						$Totalpenalty = number_format($Totalpenalty,2);
						$Totalamountpaid = number_format($Totalamountpaid,2);
						$Totalamountdue  = number_format($Totalamountdue,2);
						$Totalbalance = number_format($balance,2);
						
					//$TotalsDue = number_format($TotalsDue,2);
					$HTML = $HTML."<tr><td></td><td><b><u>Total</u></b></td><td><b><u>$Totaldays</u></b></td>
					<td></td><td><b><u>$Totaldisbursement</u></b></td><td><b><u>$Totalamountdue</u></b></td>
					<td><b><u>$Totalbalance</u></b></td><td><b><u>$Totalinterestdue</u></b></td><td><b><u>$Totalfees</u></b></td>
					
					<td><b><u>$Totalpenalty</u></b></td>
					<td><b><u>$TotalsDue</u></b></td><td><b><u>$TotalsPaid</u></b></td><td><b><u>$TotalsOutstanding</u></b></td></tr>";
						$HTML = $HTML.'</tbody></table>';
						
						}
							
						}
						
					$HTML = $HTML.'</div>';
						
					echo $HTML;
					
			//echo 'Error creating Payment Schedule.';
		
}

// -- Create - PaymentSchedules.
function CreatePaymentSchedules($i,$ApplicationId,$scheduleddate,$disbursement,$amountdue,$repayment)
{
// -- PaymentSchedule - fields.
	$item = '0.00';
	$days = '0.00';
	$paidby = '';
	$balance = '0.00';
	$interestdue = '0.00';
	$fees = '0.00';
	$penalty = '0.00';
	$tbl_name ="paymentschedule"; 
	$amountpaid = '0.00';
	$TotalOutstanding = '0.00';
	$TotalPaid = '0.00';
	$chargeid = 0;
	
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// if its 
	
	// -- Get the Next Payment Next Schedule Due Date
			   if($i >= 0)
			   {
			     $scheduleddate = PaymentNextScheduleDue($scheduleddate);
				 $disbursement =  0.00;
				 
				 // -- PaymentDaysOverDue
				$days = PaymentDaysOverDue($scheduleddate);
				 
				// -- Fee : Insurance Fee.
				$chargeid = 4; // -- Will Change once Implemented the UI.
				$fees = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Interest Due : Bounced Fee.
				$chargeid = 3; // -- Will Change once Implemented the UI.
				$interestdue = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Penalties : Over on Instalment.
				$chargeid = 2; // -- Will Change once Implemented the UI.
				$penalty = OverDuePenalty($days,$ApplicationId,$chargeid, $i);

			   }
			   // -- Disbursement Entry. Determine the Interest Due le Fees
			   else
			   {
				// -- Interest Due Loan Repayment Amount - Loaned Amount.
				//$interestdue = $repayment - $disbursement;
				
				// -- If Immediate Tranfer was requested.
				$chargeid = 1; // -- Will Change once Implemented the UI.
				$fees = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Determine if the Disbursement Immediate Disbursement Fee is Charged.
				
				// -- Blank fields that wont be filled.				
			   }
				if(PaymentScheduleExit($ApplicationId,$i) < 1 )
				{
					$item = $i;
					
					$sql = "INSERT INTO $tbl_name
					(ApplicationId,item,scheduleddate,days,paidby,disbursement,amountdue,balance,interestdue,fees,penalty,amountpaid)
					VALUES
					(?,?,?,?,?,?,?,?,?,?,?,?)";
				
					$q = $pdo->prepare($sql);
					$q->execute(array($ApplicationId,$item,$scheduleddate,$days,$paidby,$disbursement,$amountdue,$balance,$interestdue,$fees,$penalty,$amountpaid));
	
					Database::disconnect();
				}
} 

// -- Get the Payment Schedule.
function PaymentScheduleExit($AppID, $Item)
{
	
		$tbl_name ="paymentschedule"; 

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_name where ApplicationId = ? AND item = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$Item));
		
		if ($q->rowCount() >= 1)
		{ 
		  return 1;
		}
		else
		{
		  return 0;		
		}
}
// ----------------------------------------------------------------------------------------- //
//								Penalties AND Fees, Interest								 // 
// ----------------------------------------------------------------------------------------- //
// -- Disbursement Fee,Insurance Fee.
function Fee($AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if ($q->rowCount() >= 1)
		{ 
		// -- Return the Fee.
		  return $dataDisbursementFee['amount'];
		}
		else
		{
		  return 0;		
		}
}

// -- OverDue Penalties.
function OverDuePenalty($OverDays,$AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 
		$minDays = 0;
		$maxDays = 0;
		$Penalty = '0.00';
		$graceperiod = 0.00;
		$arreas = 1;
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		
		if ($q->rowCount() >= 1)
		{ 
		
			// -- OverDue days must be in range.
			$minDays = $dataDisbursementFee['min'];
			$maxDays = $dataDisbursementFee['max'];
			$Penalty =  $dataDisbursementFee['amount'];
			$graceperiod = $dataDisbursementFee['graceperiod'];
			
			// -- OverDue days must be in between range.
			if($OverDays >= $minDays AND $OverDays > 0)
			{
			// -- Get the start days to calculate arreas.
				$OverDays = $OverDays - $minDays;
			
			// -- Maximum - days.
				if($OverDays >= $maxDays)
				{
				  $OverDays = $maxDays;
				}
				
			// -- if there is grace period.
			   if($graceperiod > 0 AND $graceperiod <= $OverDays)
			   { $OverDays = $OverDays - $graceperiod; }
			   elseif($graceperiod > $OverDays)
			   { $OverDays = 0;} 
			   
			// -- ensure we do not catch exception multiple by 0.   
			   if($OverDays > 0)
			   {
				$Penalty = $Penalty * $OverDays;
			   }
			   else
			   {
			     $Penalty = 0.00; 
			   }
			}
			else
			{
				$Penalty = 0.00;
			}
			
			// -- Return the Fee.
			  return $Penalty;
		}
		else
		{
		  return '0.00';		
		}
}

// ----------------------------------------------------------------------------------------- //
//							End-Penalties AND Fees, Interest								 // 
// ----------------------------------------------------------------------------------------- //
function PaymentDaysOverDue($PaymentDate)
{
	// -- PaymentScheduleDate.
	$day = substr($PaymentDate,-2);
	$month = substr($PaymentDate,5,2);
	$year = substr($PaymentDate,0,4);
	$monthsDue = 0;
	
	// -- Get the current day, month, year.
	$cyear = date('Y');
	$cmonth = date('m');
	$cday = date('d');
	
	$PaymentDueDate = '';
	$daysInMonth = 0;
	
	// -- Days Over Due
	$daysOverDue = 0;
	
	// -- Get the days in a month.
	$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year );
	
	// -- Overdue days.
	if($cyear == $year)
	{
	  // -- Over Days 
	  
	  // -- OverDue Months.
	  if($month == $cmonth)
	  {
			if($cday > $day )
			{
			  $daysOverDue = $cday - $day; 
			}
	  }
	  elseif($cmonth > $month)
	  {
		// -- How many months due?
		$monthsDue = $month - $cmonth;
		
		$daysOverDue = $cday; 

		// -- Go through each month & get its days.
		for($i=1;$i<=$monthsDue;$i++)
		{
			$cmonth = $cmonth + $i;
			// -- Get the days in a month.
			$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $cmonth , $year );
			$daysOverDue = $daysOverDue + $daysInMonth;
		}
		
	  }
	  
	}
	
	return $daysOverDue;
}


// -- Get the Next Payment Schedule Due Date.
function PaymentNextScheduleDue($FirstPaymentDate)
{
	$day = substr($FirstPaymentDate,-2);
	$month = substr($FirstPaymentDate,5,2) + 1;
	$year = substr($FirstPaymentDate,0,4);
	$PaymentDueDate = '';
	$daysInMonth = 0;
	global $days;
	
	// -- Get the current day, month, year.
	$cyear = date('Y');
	$cmonth = date('m');
	$cday = date('d');

	// -- if the Next Month is greater than 12, we going to the next year. 
	if($month > 12)
	{
	  $year = $year + 1;
	  $month = 1;
	}
	
	// -- Get the days in a month.
	$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year );
	
	// -- Ensure the days dont get confused.
	if($day > $daysInMonth)
	{
	  $day = $daysInMonth;
	}
	
	// -- if the month is less than 10 add 0.
	if($month < 10)
	{
	   $month = "0".$month;
	}

	$PaymentDueDate = $year.'-'.$month.'-'.$day;
	global $scheduleddate;
	$scheduleddate = $PaymentDueDate;
	
	return $PaymentDueDate;
}
// ------------------------------------------------- //
// -- 				Get the Payments
// -- 				20.05.2017 .
// -- IMS Data System
// ------------------------------------------------ //
function GetPayments($customerid,$ApplicationId,$repaymentAmount)
{
//require 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
$tot_outstanding = 0;
$tot_paid = 0; 

// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
			   // -- Get Payment Invoices from common Tab Data.
				$sql =  "select * from common where customer_identification = ?";
				$q = $pdo->prepare($sql);
				$q->execute(array($customerid));
				$dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
					 
			    $data = null;
					 
			   // $dataCommon brings all the Invoices related to a client.
			   foreach($dataCommon as $rowCommon)
			   {
					 $invoice_id = $rowCommon['id'];
					 
 					 // -- Get Payment Details Data.
					  $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($invoice_id ,$ApplicationId));
					  $data = $q->fetchAll(PDO::FETCH_ASSOC);
					  
				// For each Invoice Get the Payments related to the Loan Application Id			 
					 foreach ($data as $row) 
					  {
					    $paymentid = $row['id'];
					    $iddocument = $row['FILE'];
					    $path=str_replace(" ", '%20', $iddocument);
					  //echo '<td>'.number_format($row['amount'],2). '</td>';
						$tot_paid = $tot_paid + $row['amount']; 
					   }
					   		  
				}	// End all Customer Invoices
				// -- Get the curreny 
			    // -- $currency = $_SESSION['currency'];
			
			  if($data != null)
				{
					     $tot_outstanding = $repaymentAmount - $tot_paid;
					    /*echo '<table class="table">';
						echo "<tr class='bg-danger'><td><b>Total Repayment Amount</b></td><td><b>$currency".number_format($repaymentAmount,2)."<b></td></tr>";
					    echo "<tr class='bg-success'><td><b>Total Paid Amount</b></td><td><b>$currency".number_format($tot_paid,2)."</b></td></tr>";
						echo "<tr  class='bg-info'><td><b>Total Outstanding Amount</b></td><td><b>$currency".number_format($tot_outstanding,2)."</b></td></tr>";
						echo '</table>';*/
				}
							   Database::disconnect();
			  return $tot_paid;
			  
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //	
}


?>