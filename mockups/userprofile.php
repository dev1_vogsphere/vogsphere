
<link rel="stylesheet" href="../css/bootstrap-5.0.2-dist/css/bootstrap.min.css">

<div class="container rounded bg-white mt-5 mb-5">
    <div class="row">
        <div class="col-md-3 border-right">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
              <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
              <span class="font-weight-bold">Mulunghisi Mahwayi</span>
              <span class="text-black-50">mahwaymp@gmail.com</span>
              <span> </span></div>
        </div>
        <div class="col-md-5 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">My Profile</h4>
                </div>
                <div class="row mt-2">
                    <div class="col-md-6"><label class="labels">First Name</label><input type="text" class="form-control" placeholder="" value="" readonly></div>
                    <div class="col-md-6"><label class="labels">Last Name</label><input type="text" class="form-control" value="" placeholder="" readonly></div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6"><label class="labels">Contact Number</label><input type="text" class="form-control" placeholder="" value=""></div>
                    <div class="col-md-6"><label class="labels">Email Address</label><input type="text" class="form-control" placeholder="" value=""></div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6"><label class="labels">Street</label><input type="text" class="form-control" placeholder="" value=""></div>
                    <div class="col-md-6"><label class="labels">Suburb</label><input type="text" class="form-control" placeholder="" value=""></div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6"><label class="labels">Province</label><input type="text" class="form-control" placeholder="" value=""></div>
                    <div class="col-md-6"><label class="labels">City</label><input type="text" class="form-control" placeholder="" value=""></div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6"><label class="labels">Country</label><input type="text" class="form-control" placeholder="" value=""></div>
                    <div class="col-md-6"><label class="labels">State/Region</label><input type="text" class="form-control" value="" placeholder=""></div>
                </div>
                <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Save Profile</button></div>

        </div>

    </div>
</div>
</div>
</div>
