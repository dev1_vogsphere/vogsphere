<div class="row setup-content" id="step-2">
	<div class="col-xs-12">
		<div class="col-md-12">
			 <div class="container-fluid" style="border:1px solid #ccc ">
				 <fieldset class="border" >
					 <legend class ="text-left">Full names of Owner, Partner, Member of Trustee of the Business</legend>
				</fieldset>
		     <div class="row">
								
							 <!--Title-->
							 <div class="col-md-3">
							 	 <div class="form-group">
							 			 <label for="form_Title:">Title<span style="color:red">*</span></label>
							 					 <select class="form-control" id="im_title" name="im_title">
							 						 <?php
							 							 $im_titleSelect = $im_title;
							 						 $rowData = null;
							 						 foreach($im_titleArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $im_titleSelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							 					 </select>
							 											 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 		 <div class="help-block with-errors"></div>
							 	 </div>
							 </div>
						

							 <!--Full Names-->
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Full_Names">First Name<span style="color:red">*</span></label>
                         <input id="im_first_name" type="text" name="im_first_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="first_name is required." value = "<?php echo $im_first_name;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
							
								<div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Account_Holder">Surname<span style="color:red">*</span></label>
												 <input id="im_surname" type="text" name="im_surname" class="form-control" placeholder="" required="required"  maxlength="30" data-error="im_surname is required." value = "<?php echo $im_surname;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
							


							<div class="col-md-3">
								 <div class="form-group">
										 <label for="form_IdentificationNumber">Identification Number<span style="color:red">*</span></label>
										 <input onkeyup="id_to_dob()" id="im_member_id" type="text" name="im_member_id" class="form-control" placeholder="" required="required" maxlength="30" data-error="member_id is required." value = "<?php echo $im_member_id;?>">
										 <div class="help-block with-errors"></div>
								 </div>
							</div>

							<div class="col-md-3">
							 	 <div class="form-group">
							 			 <label for="form_Title:">Company type<span style="color:red">*</span></label>
							 					 <select class="form-control" id="im_ownership_type" name="im_ownership_type">
							 						 <?php
							 							 $im_ownershiptypeSelect = $im_ownership_type;
							 						 $rowData = null;
							 						 foreach($im_companyArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $im_ownershiptypeSelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							 					 </select>
							 											 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 		 <div class="help-block with-errors"></div>
							 	 </div>
							 </div>
						
							<div class="col-md-3">
								<div class="form-group">
								<label for="form_Shareholding">Shareholding(%)<span style="color:red">*</span></label>
								<input id="im_Shareholding" type="text" name="im_Shareholding" class="form-control" required="required" maxlength="30" placeholder="" value = "<?php echo $im_Shareholding;?>">
								<div class="help-block with-errors"></div>
								</div>
							</div>
						

				</div>	
         
			</br>

           </div>
           </br>
		   
	<div class="container-fluid" style="border:1px solid #ccc ">
		<div class="row">
		   <div class="card-body">
	    <div id="directors1" class="table-editable">
	
	      <span class="table-add float-right mb-3 mr-2">
	      <a class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="false" onclick="childrenRow()">Other(s)</i></a>

	      </span>
	</br>
	      <table id="directors" name="directors" class="table table-bordered table-responsive-sm table-striped text-center">
	        <thead>
	          <tr>
	            <th class="text-center">Title</th>
	            <th class="text-center">First Name</th>
	            <th class="text-center">Surname</th>
	            <th class="text-center">Identity Number</th>
				<th class="text-center">Shareholding(%)</th>

	          </tr>
			  <?php echo $tableChildren;?>
	        </thead>
	        <tbody>
	        </tbody>
	      </table>
	    </div>
	  </div>
		   
	</div>
	</div>
	</br>
		   

					<div class="container-fluid" style="border:1px solid #ccc ">
					<fieldset class="border" >
					<legend class ="text-left">Who is key contact person in your business?</legend>
					</fieldset>
						</br>
						 <div class="row">
						 
						 <!--Key Title-->
							 <div class="col-md-3">
							 	 <div class="form-group">
							 			 <label for="form_key_Title:">Title<span style="color:red">*</span></label>
							 					 <select class="form-control" id="im_key_title" name="im_key_title">
							 						 <?php
							 						  $im_key_titleSelect = $im_key_title;
							 						 $rowData = null;
							 						 foreach($im_titleArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $im_key_titleSelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							 					 </select>
							 											 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 		 <div class="help-block with-errors"></div>
							 	 </div>
							 </div>
							 			 <!--Full Names-->
								 <div class="col-md-3">
									 <div class="form-group">
										 <label for="form_key_Full_Names">First Name<span style="color:red">*</span></label>
										 <input id="im_key_first_name" type="text" name="im_key_first_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="first_name is required." value = "<?php echo $im_key_first_name;?>">
										 <div class="help-block with-errors"></div>
									 </div>
								 </div>
								
				<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_key_surname">Surname<span style="color:red">*</span></label>
								 <input id="im_key_surname" type="text" name="im_key_surname" class="form-control" placeholder="" required="required"  maxlength="30" data-error="im_key_surname is required." value = "<?php echo $im_key_surname;?>">
								 <div class="help-block with-errors"></div>
						 </div>
				</div>
								
				<div class="col-md-3">
					 <div class="form-group">
							 <label for="form_IdentificationNumber">Identification Number<span style="color:red">*</span></label>
							 <input onkeyup="id_to_dob()" id="im_key_member_id" type="text" name="im_key_member_id" class="form-control" placeholder="" required="required" maxlength="30" data-error="member_id is required." value = "<?php echo $im_key_member_id;?>">
							 <div class="help-block with-errors"></div>
					 </div>
				</div>


				 <div class="col-md-3">
					 <div class="form-group">
							 <label for="form_Contact_Number">Cellphone Number<span style="color:red">*</span></label>
							 <input id="im_key_cellphone" type="number" name="im_key_cellphone" class="form-control" placeholder="" required="required" maxlength="10" data-error="im_cellphone is required." value = "<?php echo $im_cellphone;?>">
							 <div class="help-block with-errors"></div>
					 </div>
				</div>


				<div class="col-md-3">
	 			 <div class="form-group">
					<label for="form_Contact_Number">Email Address<span style="color:red">*</span></label>
					<input id="im_key_email" type="text" name="im_key_email" class="form-control" placeholder="" required="required"   maxlength="30" data-error="email is required." value = "<?php echo $im_email;?>">
					<div class="help-block with-errors"></div>
	 			 </div>
				</div>
			

		</div>

		

			 </div>
			 </br>

		
			 </br>

        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>
