<?php

/* 1 */
class __TwigTemplate_5b441a318dcb9547456125094c50fa27 extends Twig_Template
{
    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, (isset($context['lang']) ? $context['lang'] : null), "html");
        echo "\" xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Invoice</title>




<style type=\"text/css\">
/* Custom CSS code */
table {border-spacing:0; border-collapse: collapse; }
ul {list-style-type: none; padding-left:0;}
body, input, textarea { font-family:helvetica,sans-serif; font-size:8pt; }
body { color:#464648; margin:2cm 1.5cm; }
h2 { color:black; font-size:16pt; font-weight:bold; line-height:1.2em; border-bottom:1px solid #0099FF; margin-center:220px }
h3 { color:black; font-size:13pt; font-weight:bold; margin-bottom: 0em}
label {color:black;}




table th.right,
table td.right { text-align:right; }




.customer-data { padding:1em 0; }
.customer-data table { width:100%; }
.customer-data table td { width:50%; }
.customer-data td span { display:block; margin:0 0 5pt; padding-bottom:2pt; border-bottom:1px solid #DCDCDC; }
.customer-data td span.left { margin-right:1em; }
.customer-data label { display:block; font-weight:bold; font-size:8pt; }
.payment-data { padding:1em 0; }
.payment-data table { width:100%; }
.payment-data th,
.payment-data td { line-height:1em; padding:5pt 8pt 5pt; border:1px solid #DCDCDC; }
.payment-data thead th { background:#FAFAFA; }
.payment-data th { font-weight:bold; white-space:nowrap; }
.payment-data .bottomleft { border-color:white; border-top:inherit; border-right:inherit; }
.payment-data span.tax { display:block; white-space:nowrap; }
.terms, .notes { padding:9pt 0 0; font-size:7pt; line-height:9pt; }
.company-data { text-align: right; }
.section { margin-bottom: 1em; }
.logo { text-align: left; }
</style>




<style type=\"text/css\">
/* CSS code for printing */
@media print {
body { margin:auto; }
.section { page-break-inside:avoid; }
div#sfWebDebug { display:none; }
}
</style>
</head>
<body>




<div class=\"section\">
<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
<tr>
<td>
";
        // line 70
        if ($this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_logo", array(), "any", false)) {
            // line 71
            echo "<span class=\"logo\" align=\"left\">
<img src=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_logo", array(), "any", false), "html");
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_name", array(), "any", false), "html");
            echo "\" />
</span>
";
        }
        // line 75
        echo "</td>




<td>
<span class=\"company-data\">
<ul>
<li>Company: ";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_name", array(), "any", false), "html");
        echo "</li>
<li>Address: ";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_address", array(), "any", false), "html");
        echo "</li>
<li>Phone: ";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_phone", array(), "any", false), "html");
        echo "</li>
<li>Registration No: ";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_fax", array(), "any", false), "html");
        echo "</li>
<li>Email: ";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_email", array(), "any", false), "html");
        echo "</li>
<li>Web: ";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['settings']) ? $context['settings'] : null), "company_url", array(), "any", false), "html");
        echo "</li>
</ul>
</span>
</td>
</tr>
</table>
</div>




<div class=\"h2\">
<h2 style=\"text-align:center;\">Invoice for ";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "customer_name", array(), "any", false), "html");
        echo " #";
        echo twig_escape_filter($this->env, (isset($context['invoice']) ? $context['invoice'] : null), "html");
        echo "</h2>
</div>




<div class=\"section\">
<h3>Client info</h3>




<div class=\"customer-data\">
<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">
<tr>
<td>
<span class=\"left\">
<label>Customer:</label>
";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "customer_name", array(), "any", false), "html");
        echo "
</span>
</td>
<td>
<span class=\"right\">
<label>Customer identification:</label>
";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "customer_identification", array(), "any", false), "html");
        echo "
</span>
</td>
</tr>
<tr>
<td>
<span class=\"left\">
<label>Contact person:</label>
";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "contact_person", array(), "any", false), "html");
        echo "
</span>
</td>
<td>
<span class=\"right\">
<label>Email:</label>
";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "customer_email", array(), "any", false), "html");
        echo "
</span>
</td>
</tr>
<tr>
<td>
<span class=\"left\">
<label>Invoicing address:</label>
";
        // line 146
        echo simple_format_text(twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "invoicing_address", array(), "any", false), "html"));
        echo "
</span>
</td>
<td>
<span class=\"right\">
<label>Shipping address:</label>
";
        // line 152
        echo simple_format_text(twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "shipping_address", array(), "any", false), "html"));
        echo "
</span>
</td>
</tr>
</table>
</div>
</div>




<div class=\"section\">
<h3>Payment details</h3>




<div class=\"payment-data\">
<table style=\"background-color:#E0F0FF;\">
<thead>
<tr>
<th bgcolor=\"#0099FF\" style=\"color:white;\">Description</th>
<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Unit Cost</th>
<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Qty</th>
<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Taxes</th>
";
        // line 178
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            // line 179
            echo "<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Discount</th>
";
        }
        // line 181
        echo "<th bgcolor=\"#0099FF\" style=\"color:white;\"class=\"right\">Price</th>
</tr>
</thead>
<tbody>
";
        // line 185
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "Items", array(), "any", false));
        foreach ($context['_seq'] as $context['_key'] => $context['item']) {
            // line 186
            echo "<tr>
<td>
";
            // line 188
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['item']) ? $context['item'] : null), "description", array(), "any", false), "html");
            echo "
</td>
<td class=\"right\">";
            // line 190
            echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['item']) ? $context['item'] : null), "unitary_cost", array(), "any", false)), "html");
            echo "</td>
<td class=\"right\">";
            // line 191
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context['item']) ? $context['item'] : null), "quantity", array(), "any", false), "html");
            echo "</td>
<td class=\"right\">
";
            // line 193
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context['item']) ? $context['item'] : null), "Taxes", array(), "any", false));
            foreach ($context['_seq'] as $context['_key'] => $context['tax']) {
                // line 194
                echo "<span class=\"tax\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context['tax']) ? $context['tax'] : null), "name", array(), "any", false), "html");
                echo "</span>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tax'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 196
            echo "</td>
";
            // line 197
            if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
                // line 198
                echo "<td class=\"right\">";
                echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['item']) ? $context['item'] : null), "discount_amount", array(), "any", false)), "html");
                echo "</td>
";
            }
            // line 200
            echo "<td class=\"right\">";
            echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['item']) ? $context['item'] : null), "gross_amount", array(), "any", false)), "html");
            echo "</td>
</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 203
        echo "</tbody>
<tfoot>
<tr>
<td class=\"bottomleft\" colspan=\"";
        // line 206
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            echo "4";
        } else {
            echo "3";
        }
        echo "\"></td>
<th class=\"right\" style=\"color:black;\">Base</th>
<td class=\"right\">";
        // line 208
        echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "base_amount", array(), "any", false)), "html");
        echo "</td>
</tr>
";
        // line 210
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            // line 211
            echo "<tr>
<td class=\"bottomleft\" colspan=\"";
            // line 212
            if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
                echo "4";
            } else {
                echo "3";
            }
            echo "\"></td>
<th class=\"right\" style=\"color:black;\">Discount</th>
<td class=\"td_global_discount right\">";
            // line 214
            echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)), "html");
            echo "</td>
</tr>
";
        }
        // line 217
        echo "<tr>
<td class=\"bottomleft\" colspan=\"";
        // line 218
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            echo "4";
        } else {
            echo "3";
        }
        echo "\"></td>
<th class=\"right\" style=\"color:black;\">Subtotal</th>
<td class=\"td_subtotal right\">";
        // line 220
        echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "net_amount", array(), "any", false)), "html");
        echo "</td>
</tr>
<tr>
<td class=\"bottomleft\" colspan=\"";
        // line 223
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            echo "4";
        } else {
            echo "3";
        }
        echo "\"></td>
<th class=\"right\" style=\"color:black;\">Taxes</th>
<td class=\"td_total_taxes right\">";
        // line 225
        echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "tax_amount", array(), "any", false)), "html");
        echo "</td>
</tr>
<tr class=\"strong\">
<td class=\"bottomleft\" colspan=\"";
        // line 228
        if ($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "discount_amount", array(), "any", false)) {
            echo "4";
        } else {
            echo "3";
        }
        echo "\"></td>
<th class=\"right\" style=\"color:black;\">Total</th>
<td class=\"td_total right\">";
        // line 230
        echo twig_escape_filter($this->env, common_twig_extension_format_currency($this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "gross_amount", array(), "any", false)), "html");
        echo "</td>
</tr>
</tfoot>
</table>
</div>
</div>

<div class=\"section\">
 <h3>Notes</h3>
 <div class=\"terms\">
   ";
        // line 240
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "notes", array(), "any", false), "html");
        echo "
 </div>
</div>

<div class=\"section\">
<h3>Terms & conditions</h3>
<div class=\"terms\">
";
        // line 247
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context['invoice']) ? $context['invoice'] : null), "terms", array(), "any", false), "html");
        echo "
</div>
</div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "1";
    }

    public function isTraitable()
    {
        return false;
    }
}
