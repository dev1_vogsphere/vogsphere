<?php
// auto-generated by sfDefineEnvironmentConfigHandler
// date: 2014/06/15 01:05:11
sfConfig::add(array(
  'mod_printtemplates_enabled' => true,
  'mod_printtemplates_view_class' => 'sfPHP',
  'mod_printtemplates_urls_include' => array (
),
  'mod_printtemplates_urls_variables' => array (
  'editRow' => 'printTemplates/edit',
  'showRow' => 'printTemplates/edit',
  'save' => '@templates?action=save',
  'deleteAction' => '@templates?action=delete',
),
));
