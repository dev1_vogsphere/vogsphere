<style>
.font-bold {
  font-weight: bold;
}
</style>
<div class="controls">
<div class="container-fluid" style="border:1px solid #ccc ">
<div class="row">
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Initials</label>
				<label name="InitialsLbl" id="InitialsLbl"></label>
			</div>
		</div>	
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Customer Reference</label>
				<label name="Reference_1Lbl" id="Reference_1Lbl"></label>
			</div>
		</div>	
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Contract Reference:</label>
				<label name="Reference_2Lbl" id="Reference_2Lbl"></label>
			</div>
		</div>
		<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Company Branch</label>
			<label name="companybranchLbl" id="companybranchLbl"></label>
		</div>
		</div>
			
</div>	
<div class="row">	
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Identification Type</label>
				<label name="Client_ID_TypeLbl" id="Client_ID_TypeLbl"></label>
			</div>
		</div>

		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Identification Number</label>
				<label name="Client_ID_NoLbl" id="Client_ID_NoLbl"></label>
			</div>
		</div>
	<div class="col-md-3">	
        <div class="form-group">
			<label class="font-bold">Name on Card</label>
			<label name="Account_HolderLbl" id="Account_HolderLbl"></label>
		</div>
	</div>		
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Card Number</label>
			<label name="Account_NumberLbl" id="Account_NumberLbl"></label>
		</div>
	</div>			
</div>	
<div class="row">		
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Notify</label>
			<label name="NotifyLbl" id="NotifyLbl"></label>
		</div>
	</div>
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Contact Number</label>
			<label name="contact_numberLbl" id="contact_numberLbl"></label>
		</div>
	</div>	
</div>
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">No of Instalments</label>
			<label name="No_PaymentsLbl" id="No_PaymentsLbl"></label>
		</div>
	</div>
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Amount</label>
			<label name="AmountLbl" id="AmountLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Frequency</label>
			<label name="FrequencyLbl" id="FrequencyLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Action Date</label>
			<label name="Action_DateLbl" id="Action_DateLbl"></label>
		</div>
	</div>
</div>	


</div>		
</div>
</div>