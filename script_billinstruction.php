<?php
date_default_timezone_set('Africa/Johannesburg');
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}
if(!function_exists('bill_single_instruction'))
{
	function bill_single_instruction($data)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		////////////////////////////////////////////////////////////////////////
		$message = '';
		$id  = 0;
		$ret = '';
		try
		{
			$tbl = 'collection';
			$sqlData = sql_associative_array($data);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$id = $pdo->lastInsertId();	
			$pdo->commit();
	    }
		catch (PDOException $e)
		{
			$message = $e->getMessage();
		}
		$ret = $id."|".$message;
		return $ret;	
	}
}
if(!function_exists('get_bill_single_instruction'))
{
	function get_bill_single_instruction($dataFilter)
	{
		// -- check with Client_Reference_1, Account_Number,Action_Date,IntUserCode.
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'collection';			
		$sql = "select * from $tbl where 
		Action_Date  = ? and Client_Reference_1 = ? and IntUserCode = ? and 
		Account_Number = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['Action_Date'],
						  $dataFilter['Client_Reference_1'],$dataFilter['IntUserCode'],
						  $dataFilter['Account_Number']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
////////////////////////////////////////////////////////////////////////
$message = ''; 
$list = getpost('list');
$list['createdon'] = date('Y-m-d');
$list['createdat'] = date("H:i:s");
$list['Status_Code'] = '1'; 									
$list['Status_Description'] = 'pending';	
////////////////////////////////////////////////////////////////////////

$databill_single_instructionObj = null;
// -- Check bill_single_instruction details
$databill_single_instructionObj = get_bill_single_instruction($list);

// -- Create Single Bill Instruction.
$retCollection = bill_single_instruction($list);
$rowData = explode(utf8_encode('|'), $retCollection);  // -- for UTF-8
$collectionid = $rowData[0];

if(!empty($collectionid))
{
	// -- Payment was made.
	$message = 'success|'.$collectionid;	
	if($list['Notify'] == 'Yes')
	{
		$smstemplatename = 'single_debitorder_uploaded';
		$applicationid = 0;		
		$contents = auto_template_content($userid,$smstemplatename,$applicationid,$list['Bank_Reference'],$list['Action_Date']);
		if(!empty($contents) && !empty($list['Contact_No']))
		{	
			$notification['phone'] 		 	= $list['Contact_No'];
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= $list['Bank_Reference'];	
			$notification['usercode'] 	 	= $list['UserCode'];
			$notification['createdby'] 	 	= $list['createdby'];
			$notification['IntUserCode'] 	= $list['IntUserCode'];
			$notification['amount'] 		= $list['Amount'];
			$notification['Account_Holder'] = $list['Account_Holder'];										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList); }
		}
	}	
}
else
{
	$message = 'ERROR|'.$rowData[1];
}

/*if(isset($databill_single_instructionObj['collectionid']))
{
	// -- Single Bill Instruction warning message.
	$message =  $message.'|Warning: Debit Order already exist for on '.
	$databill_single_instructionObj['Action_Date'].' from bank account number '.
	$databill_single_instructionObj['Account_Number'].' with client reference '.
	$databill_single_instructionObj['Client_Reference_1'];			
}*/
echo $message;
/*

	   // -- Logged User Details.
		if(getsession('username'))
		{
			$userid = getsession('username');
		}
	   		$Service_Mode = $_POST['Service_Mode'];
			$Service_Type = $_POST['Service_Type'];
			$entry_class  = $_POST['entry_class'];	

							//print_r($_POST);
							$arrayInstruction[1] = $UserCode;//$_POST['Client_Id']; 										
							// -- UTF-8
							if(isset($_POST['Reference_1'])){$arrayInstruction[2] = $_POST['Reference_1'];}else{$arrayInstruction[2] = '';}
							// -- UTF-8 DOM
							if(empty($arrayInstruction[2])){if(isset($_POST['\ufeffReference 1'])){$arrayInstruction[2] = $_POST['\ufeffReference 1'];}else{$arrayInstruction[2] = '';}}
							if(isset($_POST['Reference_2'])){$arrayInstruction[3] = $_POST['Reference_2'];}else{$arrayInstruction[3] = '';}							
							//if(isset($_POST['Reference 2'])){$arrayInstruction[3] = $_POST['Reference 2'];}else{$arrayInstruction[3] = '';} 								
							if(isset($_POST['Client_ID_Type'])){$arrayInstruction[4] = $_POST['Client_ID_Type'];}else{$arrayInstruction[4] = '';} 									
							if(isset($_POST['Client_ID_No'])){$arrayInstruction[5] = $_POST['Client_ID_No'];}else{$arrayInstruction[5] = '';} 									
							if(isset($_POST['Initials'])){$arrayInstruction[6] = $_POST['Initials'];}else{$arrayInstruction[6] = '';} 										
							if(isset($_POST['Account_Holder'])){$arrayInstruction[7] = $_POST['Account_Holder'];}else{$arrayInstruction[7] = '';} 									
							if(isset($_POST['Account_Type'])){$arrayInstruction[8] = $_POST['Account_Type'];}else{$arrayInstruction[8] = '';} 									
							if(isset($_POST['Account_Number'])){$arrayInstruction[9] = $_POST['Account_Number'];}else{$arrayInstruction[9] = '';} 	
// -- User bank name on the spreadsheet to get the branchcode.							
							if(isset($_POST['Branch_Code'])){$arrayInstruction[10] = $_POST['Branch_Code'];}else{$arrayInstruction[10] = '';}//$_POST['Bank Name']; 									
							if(isset($_POST['No_Payments'])){$arrayInstruction[11] = $_POST['No_Payments'];}else{$arrayInstruction[11] = '';} 									
							if(isset($_POST['Service_Type'])){$arrayInstruction[12] = $_POST['Service_Type'];}else{$arrayInstruction[12] = '';} 									
							if(isset($_POST['Service_Mode'])){$arrayInstruction[13] = $_POST['Service_Mode'];}else{$arrayInstruction[13] = '';} 									
							if(isset($_POST['Frequency'])){$arrayInstruction[14] = $_POST['Frequency'];}else{$arrayInstruction[14] = '';} 										
							if(isset($_POST['Action_Date'])){$arrayInstruction[15] = $_POST['Action_Date'];}else{$arrayInstruction[15] = '';} 									
							if(isset($_POST['Bank_Reference'])){$arrayInstruction[16] = $_POST['Bank_Reference'];}else{$arrayInstruction[16] = '';} 								
							if(isset($_POST['Amount'])){$arrayInstruction[17] = $_POST['Amount'];}else{$arrayInstruction[17] = '0.00';} 										
							$arrayInstruction[18] = '1';//$_POST['Status_Code']; 									
							$arrayInstruction[19] = 'pending';//$_POST['Status_Description']; 							
							$arrayInstruction[20] = '';//$_POST['mandateid']; 										
							$arrayInstruction[21] = '';//$_POST['mandateitem']; 
							$arrayInstruction[22] = $IntUserCode;									
							$arrayInstruction[23] = $UserCode; 
							
							
							if(isset($_POST['entry_class'])){$arrayInstruction[24] = $_POST['entry_class'];}else{$arrayInstruction[24] = 0;} 										
							if(isset($_POST['contact_number'])){$arrayInstruction[25] = $_POST['contact_number'];}else{$arrayInstruction[25] = '';} 										
							if(isset($_POST['Notify'])){$arrayInstruction[26] = $_POST['Notify'];}else{$arrayInstruction[26] = '';} 
							$arrayInstruction[27] = $userid;

$Reference_1   = $arrayInstruction[2];
$Reference_2   = $arrayInstruction[3];
$Client_ID_No  =$arrayInstruction[5];
$Initials      =$arrayInstruction[6];
$Account_Holder=$arrayInstruction[7];
$Account_Type  =$arrayInstruction[8];
$Account_Number=$arrayInstruction[9];
$Branch_Code   =$arrayInstruction[10];
$No_Payments   =$arrayInstruction[11];
$Amount        =$arrayInstruction[17];
$Frequency     =$arrayInstruction[14];
$Action_Date   =$arrayInstruction[15];
$Service_Mode  =$arrayInstruction[13];
$Service_Type  =$arrayInstruction[12];
$entry_class   =$arrayInstruction[24];

$Bank_Reference=$arrayInstruction[16];
$contact_number = $arrayInstruction[25];
$Client_ID_Type = $arrayInstruction[4];
$Notify = $arrayInstruction[26];

											$entry_classSelect = $entry_class;

	   // -- Default Mode
	  $sql = "SELECT * FROM entry_class where service_mode = '".$Service_Mode."'";	  
	  $dataentry_class = $pdo->query($sql);

							$message = auto_create_billinstruction($arrayInstruction); 
							//echo $message.' '.$arrayInstruction[26].'<br/>';
							if($message == 'success')
							{
								$smstemplatename = 'single_debitorder_uploaded';
								$applicationid = 0;								

								// -- Sent to info@ecashmeup.com
								auto_email_collection('8707135963082',$smstemplatename,$applicationid,$Reference_1,$Action_Date);
								if($arrayInstruction[26] == 'Yes')
								{
									$smstemplatename = 'debitorder_notification';
									$applicationid = 0;		
									$Action_Date = $arrayInstruction[15];			
									$Reference_1 = $arrayInstruction[2];
									$contents = auto_template_content($userid,$smstemplatename,$applicationid,$Reference_1,$Action_Date);
									if(!empty($contents) && !empty($arrayInstruction[25]))
									{	
										$notification['phone'] 		 	= $arrayInstruction[25];
										$notification['content'] 	 	= $contents;
										$notification['reference_1'] 	= $arrayInstruction[2];	
										$notification['usercode'] 	 	= $UserCode;
										$notification['createdby'] 	 	= $userid;
										$notification['IntUserCode'] 	= $arrayInstruction[22];
										$notification['amount'] 		= $arrayInstruction[17];
										$notification['Account_Holder'] = $arrayInstruction[7];										
										// -- 
										$notificationsList[] = $notification;	// add into list
										// -- notify clients who signed for notifications(queue)
										if(!empty($notificationsList)){auto_sms_queue($notificationsList); }
										
									}
								}
							}								
							
			
   */
?>