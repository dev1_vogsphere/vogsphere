<div class="row setup-content" id="step-2">
	<div class="col-xs-12">
		<div class="col-md-12">
			 <div class="container-fluid" style="border:1px solid #ccc ">
				 <fieldset class="border" >
					 <legend class ="text-left">Main Member Details</legend>
				</fieldset>
				
				<!--Member Migration--2024/04/14-->
				<div class="row">
						<div class="col-md-3">
								<div class="form-group">
									<label for="form_migration:">Member Migration<span style="color:red">*</span></label>
									<label class="radio-inline"><input type="radio" name="im_Migration" onclick="policystatus(1)" value="0" checked>No</label>
									<label class="radio-inline"><input type="radio"  name="im_Migration" value="1" onclick="policystatus(0)">Yes</label>
									
									<div class="help-block with-errors"></div>
								</div>	
							</div>	
							</br>
				</div>
         <div class="row">
		 
							
								

							 <!--Title-->
							 <div class="col-md-3">
							 	 <div class="form-group">
							 			 <label for="form_Title:">Title<span style="color:red">*</span></label>
							 					 <select class="form-control" id="im_title" name="im_title">
							 						 <?php
							 							 $im_titleSelect = $im_title;
							 						 $rowData = null;
							 						 foreach($im_titleArr as $row)
							 						 {
							 							 $rowData = explode(utf8_encode('|'), $row);  //for UTF-8

							 							 if($rowData[0] == $im_titleSelect)
							 							 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
							 							 else
							 							 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
							 						 }?>
							 					 </select>
							 											 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 		 <div class="help-block with-errors"></div>
							 	 </div>
							 </div>
							 <!--Initials-->

							 <div class="col-md-3">
							 		<div class="form-group">
							 				<label for="form_Full_Names">Initials<span style="color:red">*</span></label>
							 				<input id="im_initials" type="text" name="im_initials" class="form-control" placeholder="" required="required" maxlength="30" data-error="initialsis required." value = "<?php echo $im_initials;?>">
							 				<div class="help-block with-errors"></div>
							 		</div>
							 </div>

							 <!--Full Names-->
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Full_Names">First Name<span style="color:red">*</span></label>
                         <input id="im_first_name" type="text" name="im_first_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="first_name is required." value = "<?php echo $im_first_name;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
								 <div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Middle_Name">Middle Name</label>
												 <input id="im_Middle_name" type="text" name="im_Middle_name" class="form-control" placeholder="" maxlength="30" value = "<?php echo $im_Middle_name;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
								<div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Account_Holder">Surname<span style="color:red">*</span></label>
												 <input id="im_surname" type="text" name="im_surname" class="form-control" placeholder="" required="required"  maxlength="30" data-error="im_surname is required." value = "<?php echo $im_surname;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
									 <label for="form_Gender">Gender</label>
									 <label class="radio-inline"><input type="radio" name="im_gender" value="2" checked />Female</label>
									 <label class="radio-inline"><input type="radio" name="im_gender" value="1"/>Male</label>
									 <div class="help-block with-errors"></div>
								    </div>
							    </div>
							 <div class="col-md-3">
		 						<div class="form-group">
		 							<label for="form_Client_ID_Type">Identification Type</label>
		 									<select class="form-control" id="Client_ID_Type" name="Client_ID_Type">
		 										 <?php
		 										 $Client_ID_TypeSelect = $Client_ID_Type;
		 										 foreach($Client_ID_TypeArr as $row)
		 										 {
		 											 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
		 											 if($rowData[0] == $Client_ID_TypeSelect)
		 											 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
		 											 else
		 											 {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
		 										 }
		 										 ?>
		 										</select>
		 												<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
		 										<div class="help-block with-errors"></div>
		 							</div>
		 					</div>

							<div class="col-md-3">
								 <div class="form-group">
										 <label for="form_IdentificationNumber">Identification Number<span style="color:red">*</span></label>
										 <input onkeyup="id_to_dob()" id="im_member_id" type="text" name="im_member_id" class="form-control" placeholder="" required="required" maxlength="30" data-error="member_id is required." value = "<?php echo $im_member_id;?>">
										 <div class="help-block with-errors"></div>
								 </div>
							</div>

								 <div class="col-md-3">
									 <div class="form-group">

										<label for="form_Date_of_Birth:">Date of Birth</label>
										<input id="im_Date_of_birth" type="date"  name="im_Date_of_birth" class="form-control" placeholder="aaa" data-error="Date_of_birth is required." value = "<?php echo $im_Date_of_birth;?>">
																<div class="help-block with-errors">
										<div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
											<span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
										</div>
										</div>
									</div>
								 </div>

								<div class="col-md-3">
								 <div class="form-group">
									 <label for="form_Client_ID_Type">Marital Status</label>
											 <select class="form-control" id="im_maritalStatus" name="im_maritalStatus" onChange="change_marital(this);">
													<?php
													$im_maritalStatusSelect = $im_maritalStatus;
		
													foreach($MaritalList as $key => $rowData )
										{
						 					 if($rowData->MaritalStatus_id == $im_maritalStatusSelect)
						 					 {echo  '<option value="'.$rowData->MaritalStatus_id.'" selected>'.$rowData->MaritalStatus_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->MaritalStatus_id.'">'.$rowData->MaritalStatus_name.'</option>';}
										 }
										 if(empty($MaritalList ))
										 {echo  '<option value="0" selected>No MaritalStatus</option>';}
													?>
												 </select>
														 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
												 <div class="help-block with-errors"></div>
									 </div>
							 </div>

							 <div class="col-md-3">
								<div class="form-group">
									<label for="form_Client_ID_Type">Type of Marriage</label>
											<select class="form-control" id="im_marriage" name="im_marriage">
												 <?php
												 $im_marriageSelect = $im_marriage;
												 foreach($MarriageList as $key => $rowData )
										{
						 					 if($rowData->marriage_id == $im_marriageSelect)
						 					 {echo  '<option value="'.$rowData->marriage_id.'" selected>'.$rowData->marriage_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->marriage_id.'">'.$rowData->marriage_name.'</option>';}
										 }
										 if(empty($MarriageList ))
										 {echo  '<option value="0" selected>No marriage</option>';}
												 ?>
												</select>
														<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
												<div class="help-block with-errors"></div>
									</div>
							</div>
							
							<!--Member Migration--2024/04/14-->
							<div class="row" id="MemberStatus" style="display:none">
							
							<div class="col-md-3">
								<div class="form-group">
								<label for="form_Policy_Number">Policy Number<span style="color:red">*</span></label>
								<input id="im_policy_number_existing" type="text" name="im_policy_number_existing" class="form-control" placeholder="" value = "<?php echo $im_policy_number_old;?>">
								<div class="help-block with-errors"></div>
								</div>
							</div>
							</div>

				</div>
			</br>

				<div class="row">
             </div>

           </div>
           </br>

					<div class="container-fluid" style="border:1px solid #ccc ">
					<fieldset class="border" >
					<legend class ="text-left">Main Member Contact Details</legend>
					</fieldset>
						</br>
						 <div class="row">

						 <div class="col-md-3">
							 <div class="form-group">
									 <label for="form_Contact_Number">Cellphone Number<span style="color:red">*</span></label>
									 <input id="im_cellphone" type="number" name="im_cellphone" class="form-control" placeholder="" required="required" maxlength="10" data-error="im_cellphone is required." value = "<?php echo $im_cellphone;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>

						<div class="col-md-3">
			 			 <div class="form-group">
			 					 <label for="form_Contact_Number">Work Number</label>
			 					 <input id="im_work_tel" type="tel" name="im_work_tel" class="form-control" placeholder="" maxlength="30" data-error="work_tel is required." value = "<?php echo $im_work_tel;?>">
			 					 <div class="help-block with-errors"></div>
			 			 </div>
			 		</div>

					<div class="col-md-3">
		 			 <div class="form-group">
		 					 <label for="form_Contact_Number">Home Number</label>
		 					 <input id="im_home_tel" type="tel" name="im_home_tel" class="form-control" placeholder="" maxlength="30" data-error="im_home_tel is required." value = "<?php echo $im_home_tel;?>">
		 					 <div class="help-block with-errors"></div>
		 			 </div>
		 		</div>

				<div class="col-md-3">
	 			 <div class="form-group">

	 					 <label for="form_Contact_Number">Email Address</label>
	 					 <input id="im_email" type="email" name="im_email" class="form-control" placeholder="" maxlength="30" data-error="email is required." value = "<?php echo $im_email;?>">

	 					 <div class="help-block with-errors"></div>
	 			 </div>
	 		</div>

			
			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Physical Address</label>

							<input id="im_line_5" type="text" name="im_line_5" class="form-control" placeholder="" maxlength="100" value = "<?php echo $im_line_5;?>">

							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Suburb</label>
								<input id="im_line_6" type="text" name="im_line_6" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_6;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Town</label>
							<input id="im_line_7" type="text" name="im_line_7" class="form-control" placeholder=""  maxlength="30"  value = "<?php echo $im_line_7;?>">
							<div class="help-block with-errors"></div>
		 					
					</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
						<label for="form_Province">Province</label>
						 <input id="im_line_8" type="text" name="im_line_8" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_8;?>">
						 <div class="help-block with-errors"></div>
					
				</div>
			</div>
			
			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Postal Address</label>

							<input id="im_line_1" type="text" name="im_line_1" class="form-control" placeholder=""  maxlength="100" data-error="P.O.Box is required." value = "<?php echo $im_line_1;?>">

							<div class="help-block with-errors"></div>
					</div>
			</div>
			
			<div class="col-md-3">
			<div class="form-group">
			<label for="form_No_Payments">Suburb</label>
			<input id="im_line_2" type="text" name="im_line_2" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_2;?>">
			<div class="help-block with-errors"></div>				
			</div>
			</div>
			
			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Town</label>
								<input id="im_line_3" type="text" name="im_line_3" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_3;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>
			
			<div class="col-md-3">
					<div class="form-group">
						<label for="form_Province2">Province</label>
						 <select class="form-control" id="im_line_4" name="im_line_4">
									<?php
									$provinceid = '';
									$provinceSelect = $im_line_4;
									$Provine_name = '';
									foreach($provinceArr as $row)
									{
									$provinceid = $row['Province_id'];
									$Provine_name = $row['Provine_name'];
									// Select the Selected Role 
									if($Provine_name  == $provinceSelect)
									{
									echo "<OPTION value=$provinceid selected>$Provine_name</OPTION>";
									}
									else
									{
									echo "<OPTION value=$provinceid>$Provine_name</OPTION>";
									}
									}

									if(empty($Provine_name))
									{
									echo "<OPTION value=0>No Province Selected</OPTION>";
									}
									?>			
						</select>
						<div class="help-block with-errors"></div>
					</div>
			</div>
			

		<!--	<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Postal Code</label>
							<input id="im_line_4" type="text" name="im_line_4" class="form-control" placeholder="" maxlength="30"  value = "">
							<div class="help-block with-errors"></div>
					</div>
			</div>-->



		</div>

		<div class="row">
				 </div>

			 </div>
			 </br>

			 <!--Spouse Details-->
			<div class="container-fluid" style="border:1px solid #ccc ">
			<fieldset class="border" >
			<legend class ="text-left">Source of Income</legend>
			</fieldset>
				</br>
				 <div class="row">

					 <div class="col-md-3">
						 <div class="form-group">
								 <label for="form_Title:">Occupation</label>
										 <select class="form-control" id="im_occupation_id_1" name="im_occupation_id_1">
											 <?php
												 $im_occupation_id_1Select = $im_occupation_id_1;
										foreach($im_occupationList as $key => $rowData )
										{
						 					 if($rowData->Occupation_id == $im_occupation_id_1Select)
						 					 {echo  '<option value="'.$rowData->Occupation_id.'" selected>'.$rowData->Occupation_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->Occupation_id.'">'.$rowData->Occupation_name.'</option>';}
										 }
										 if(empty($im_occupationList ))
										 {echo  '<option value="0" selected>No Occupation</option>';}

											 ?>
										 </select>
																 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>

					 <div class="col-md-3">
						 <div class="form-group">
								 <label for="form_Title:">Source of Income</label>
										 <select class="form-control" id="source_of_income_2" name="source_of_income_2">
											 <?php
										$source_of_income_2Select = $source_of_income_2;
										foreach($source_of_incomeList as $key => $rowData )
										{
						 					 if($rowData->Source_of_income_id == $source_of_income_2Select)
						 					 {echo  '<option value="'.$rowData->Source_of_income_id.'" selected>'.$rowData->Source_of_income_name.'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData->Source_of_income_id.'">'.$rowData->Source_of_income_name.'</option>';}
										 }
										 if(empty($source_of_incomeList ))
										 {echo  '<option value="0" selected>No Source of income</option>';}

											?>
										 </select>
																 <!-- <input id="Account_Type" type="text" name="Account_Type" class="form-control" placeholder="" required="required" data-error="Account Type is required.">-->
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Income provider name</label>
									<input id="Income_provider_name" type="text" name="Income_provider_name" class="form-control" placeholder="" maxlength="30" value = "<?php echo $Income_provider_name;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">

									<label for="form_Full_Names">Income provider contact no</label>
									<input id="Income_provider_Contact_No" type="tel" name="Income_provider_Contact_No" class="form-control" placeholder=""  maxlength="30" data-error="Income_provider_Contact_No is required." value = "<?php echo $Income_provider_Contact_No;?>">

									<div class="help-block with-errors"></div>
							</div>
					 </div>

					 <div class="col-md-3">
							<div class="form-group">

									<label for="form_Full_Names">Income provider email address</label>
									<input id="Income_provider_Email" type="text" name="Income_provider_Email" class="form-control" placeholder=""  maxlength="30" data-error="Income provider Email is required." value = "<?php echo $Income_provider_Email;?>">

									<div class="help-block with-errors"></div>
							</div>
					 </div>

				</div>

		<div class="row"></div>

			 </div>
			 </br>

			 <!--Spouse Details-->
			<div class="container-fluid" style="border:1px solid #ccc ">
				
				<fieldset class="border" >
					<legend class ="text-left">Spouse Details</legend>
				</fieldset>
				</br>


				 <div class="row">

				 <div class="col-md-2">
						 <div class="form-group">
								 <label for="form_Title:">
                                    Title
                                 </label>
								 <select class="form-control" id="im_spousetitle" name="im_spousetitle">
								    <?php
								    $im_spousetitleSelect = $im_spousetitle;
									$rowData = null;
											 foreach($im_titleArr as $row)
											 {
												 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												 if($rowData[0] == $im_spousetitleSelect)
												 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
												 else
												 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
											 }?>
								</select>
							 <div class="help-block with-errors"></div>
						 </div>
					 </div>

					 <!--Initials-->
					 <div class="col-md-3">
							<div class="form-group">
									<label for="form_Full_Names">Initials</label>
									<input id="im_spouseinitials2" type="text" name="im_spouseinitials2" class="form-control" placeholder=""  maxlength="30" data-error="im_spouseinitials is required." value = "<?php echo $im_spouseinitials2;?>">
									<div class="help-block with-errors"></div>
							</div>
					 </div>	 
					<!--Full Names-->
	 				<div class="col-md-3">
					 <div class="form-group">
								<label for="form_Full_Names1">First Name</label>
								<input id="im_spousefirst_name" type="text" name="im_spousefirst_name" class="form-control" placeholder=""  maxlength="30" data-error="im_spousefirst_name is required." value = "<?php echo $im_spousefirst_name;?>">
								<div class="help-block with-errors"></div>
					</div>
					</div>

					
					<!--Middle Name-->
					 <div class="col-md-3">
					 	<div class="form-group">
								<label for="form_Middle_Name3">Middle Name</label>
								<input id="im_spouseMiddle_name" type="text" name="im_spouseMiddle_name" class="form-control" placeholder="" maxlength="30" value = "<?php echo $im_spouseMiddle_name;?>">
								<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="col-md-3">
								 <div class="form-group">
										 <label for="form_Account_Holder">Surname</label>
										 <input id="im_spousesurname" type="text" name="im_spousesurname" class="form-control" placeholder=""  maxlength="30" data-error="im_spousesurname is required." value = "<?php echo $im_spousesurname;?>">
										 <div class="help-block with-errors"></div>
								 </div>
						 </div>
					

						 <div class="col-md-3">
							<div class="form-group">
								<label for="form_Client_ID_Type">Identification Type</label>
										<select class="form-control" id="Client_ID_Type" name="Client_ID_Type">
											 <?php
											 $Client_ID_TypeSelect = $Client_ID_Type;
											 foreach($Client_ID_TypeArr as $row)
											 {
												 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
												 if($rowData[0] == $Client_ID_TypeSelect)
												 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
												 else
												 {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
											 }
											 ?>
											</select>
													<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
											<div class="help-block with-errors"></div>
								</div>
						</div>

						<div class="col-md-3">
							 <div class="form-group">
									 <label for="form_IdentificationNumber">Identification Number</label>
									 <input id="im_spouse_id" type="text" name="im_spouse_id" class="form-control" placeholder="" maxlength="30" data-error="spouse_id is required." value = "<?php echo $im_spouse_id;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>
				</div>
			</div>
	</br>
        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>
