<?php
require 'database.php';
$paysingle = ''; 
if(!function_exists('get_paybenefiary'))
{
	function get_paybenefiary($dataFilter)
	{
		// -- check with benefiaryid,Client_Reference_1, Account_Number,Action_Date,IntUserCode.
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'payments';			
		$sql = "select * from $tbl where 
		benefiaryid  = ? and Action_Date  = ? and Client_Reference_1 = ? and IntUserCode = ? and 
		Account_Number = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['benefiaryid'],$dataFilter['Action_Date'],
						  $dataFilter['Client_Reference_1'],$dataFilter['IntUserCode'],
						  $dataFilter['Account_Number']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
$message = '';
$list = getpost('list');
$userid = $list['createdby'];
$list['createdon'] = date('Y-m-d');
$list['createdat'] = date('h:m:s');
$list['Status_Code'] = '1'; 									
$list['Status_Description'] = 'pending';	
		
$dataPayBenefiaryObj = null;
// -- Check Pay benefiary details
$dataPayBenefiaryObj = get_paybenefiary($list);

if(isset($dataPayBenefiaryObj['benefiaryid']))
{
	// -- Payment warning message.
	$message =  "Warning:Payment to Beneficiary already exist for ".
	$dataPayBenefiaryObj['Action_Date']." to bank account number ".
	$dataPayBenefiaryObj['Account_Number']." with client reference ".
	$dataPayBenefiaryObj['Client_Reference_1']."\nDo you want to continue?";			
}

 echo $message; 
?>