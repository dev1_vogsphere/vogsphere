<?php
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$frequency_code  = '';	
$collection_day_code = '';
$collection_day = '';
$id_ = '';

$arrayHTML = array();

if(isset($_POST['frequency'])){$frequency_code 	= $_POST['frequency'];}
if(isset($_POST['collection_day'])){$collection_day_code = $_POST['collection_day'];}
if(isset($_POST['id_'])){$id_ = $_POST['id_'];}
if(empty($id_)){$id_ = 'collection_day';}

$html = '';

if($frequency_code != 'WEEK' and $frequency_code != 'ADHO')
{
$html = '<input style="height:30px" id="'.$id_.'" name="'.$id_.'" placeholder="yyyy-mm-dd" class="form-control" type="date" />'; //value = "'.<?php echo $collection_day; 
}
else
{
$html = "<SELECT class='form-control' id='".$id_."' name='".$id_."' size='1'>";

// -- Select Collection Days.
$sql = 'SELECT * FROM debtor_collection_codes WHERE frequency_code = ?';
$q = $pdo->prepare($sql);
$q->execute(array($frequency_code));	
$dataservice_collection_days = $q->fetchAll();		

$frequency_codeSelect = $frequency_code;
$collection_day_codeSelect = $collection_day;
$collection_day_Des 		   = '';
foreach($dataservice_collection_days as $row)
{
  // -- collection_days of a selected frequency_code
	$frequency_code      = $row['frequency_code'];
	$collection_day_Des 	 = $row['collection_desc'];
	$collection_day_code = $row['collection_day_code'];
		
	// -- Select the Selected frequency_code 
  if($frequency_code == $frequency_codeSelect)
   {
		if($collection_day_code == $collection_day_codeSelect)
		{
		$html = $html.  '<OPTION value="'.$collection_day_code.'" selected>'.$collection_day_code.'-'.$collection_day_Des.'</OPTION>';
		}
		else
		{
		 $html = $html.  '<OPTION value="'.$collection_day_code.'">'.$collection_day_code.'-'.$collection_day_Des.'</OPTION>';
		}
   }
}

if(empty($dataservice_collection_days))
{
	$html = $html.  "<OPTION value=0>No collection day codes</OPTION>";
}

$html=$html."</SELECT>";
}
//$arrayHTML['frequency'] = $html;

//print_r($arrayHTML);										
	//$json = json_encode($arrayHTML, JSON_PRETTY_PRINT); 

 echo $html;		
?>