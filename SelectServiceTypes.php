<?php
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$Service_Mode  = '';	
$Service_Type = '';

$arrayHTML = array();

if(isset($_POST['Service_Mode'])){$Service_Mode 	= $_POST['Service_Mode'];}
if(isset($_POST['Service_Type'])){$Service_Type = $_POST['Service_Type'];}

$html = "<SELECT class='form-control' id='Service_Type' name='Service_Type' size='1'>";

// -- Select SERVICE TYPE.
$sql = 'SELECT * FROM service_type WHERE service_mode = ?';
$q = $pdo->prepare($sql);
$q->execute(array($Service_Mode));	
$dataservice_type = $q->fetchAll();		

// -- Select ENTRY CLASS.
$sql = 'SELECT * FROM entry_class WHERE service_mode = ?';
$q = $pdo->prepare($sql);
$q->execute(array($Service_Mode));	
$dataentry_class = $q->fetchAll();		

$Service_Type = '';
$Service_ModeSelect = $Service_Mode;
$Service_TypeSelect = $Service_Type;
$service_type_desc = '';
foreach($dataservice_type as $row)
{
  // -- Service_Type of a selected Service_Mode
	$Service_Mode 	   = $row['service_mode'];
	$service_type_desc = $row['service_type_desc'];
	$Service_Type 	   = $row['idservice_type'];
		
	// -- Select the Selected Service_Mode 
  if($Service_Mode == $Service_ModeSelect)
   {
		if($Service_TypeSelect == $Service_Type)
		{
		$html = $html.  '<OPTION value="'.$service_type_desc.'" selected>'.$service_type_desc.'</OPTION>';
		}
		else
		{
		 $html = $html.  '<OPTION value="'.$service_type_desc.'">'.$service_type_desc.'</OPTION>';
		}
   }
}

if(empty($dataservice_type))
{
	$html = $html.  "<OPTION value=0>No service types</OPTION>";
}

	$html=$html."</SELECT>";
$arrayHTML['Service_Type'] = $html;

$html2 = "<SELECT class='form-control' id='entry_class' name='entry_class' size='1'>";
$entry_class = '';
$Service_ModeSelect = $Service_Mode;
$entry_classSelect = $entry_class;
$Entry_Class_Des = '';
foreach($dataentry_class as $row)
{
  // -- Service_Type of a selected Service_Mode
	$Service_Mode 	   = $row['service_mode'];
	$Entry_Class_Des = $row['Entry_Class_Des'];
	$entry_class 	   = $row['identry_class'];
		
	// -- Select the Selected Service_Mode 
  if($Service_Mode == $Service_ModeSelect)
   {
		if($entry_classSelect == $entry_class)
		{
		$html2 = $html2.  "<OPTION value=$entry_class selected>$Entry_Class_Des</OPTION>";
		}
		else
		{
		 $html2 = $html2.  "<OPTION value=$entry_class>$Entry_Class_Des</OPTION>";
		}
   }
}

if(empty($dataentry_class))
{
	$html2 = $html2.  "<OPTION value=0>No tracking</OPTION>";
}
$html2=$html2."</SELECT>";
$arrayHTML['entry_class'] = $html2;	

//print_r($arrayHTML);										
	$json = json_encode($arrayHTML, JSON_PRETTY_PRINT); 

 echo $html.'|'.$html2;		
?>