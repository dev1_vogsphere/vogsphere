<?php
date_default_timezone_set('Africa/Johannesburg');
session_start();
ob_start();
ini_set('opcache.enable', '0');
ini_set('opcache.revalidate_freq', '0');
// -- phpinfo();

// -- BOC Force All eCashMeUp pages not to cache on all browsers.14.10.2017
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// -- EOC Force All eCashMeUp pages not to cache all browsers.14.10.2017
// -- EOC -- 2017.06.24
// -- Global Settings to update the Website Dynamic information ---- //
// -- Address, Company name updated on the website.
// -- Company details
$Company  = "";
$Address  = "";
$street = null;
$suburb = null;
$city = null;
$State = null;
$PostCode = null;
$phone    = "";
$fax      = "";
$email    = "";
$adminemail  = "";
$website  = "";
$logo     = "";
$currency = "";
$legalterms    = "";
$registrationnumber = "";
$terms = "";
$ncr = "";
$disclaimer = "";

$accountholdername = "";
$accounttype = "";
$accountnumber = "";
$branchcode = "";
$bankname = "";
$province = "";

// ----------- BOC Themes $_SESSION --------- 22.10.2017 //
if(!isset($_SESSION["GlobalTheme"]))
{
	$_SESSION["GlobalTheme"] = 'default';
	$_SESSION["buttonNew"] = 'Save';
	$_SESSION["buttonUpdate"] = 'Update';
	$_SESSION["buttonBack"] = 'Back';
	$_SESSION["ActionbuttonClass"] = "btn btn-primary";
	$_SESSION["ContentButtonClass"] = "btn btn-info";
}
// ----------- EOC Themes $_SESSION --------- 22.10.2017 //

// ************************************************************************* //
// -------------- Read the Global Settings from the DataBase. -------------- //
// ************************************************************************* //
// -- ONLY Load DataBase Connection if it was not loaded file names by
// -- checking file name database.php
$base = '';
$checkDB = 'database.php';
$checkDB2 = 'database_invoice2';
$DoNotDB = '';
$DoNotDB2 = '';
$fileNames = '';

// -- Avoid Reload : Check if database.php and database_invoice2.php were loaded
// -- before.
$included_files = get_included_files();

foreach ($included_files as $filename)
{

    if($filename == $checkDB)
	{
	  $DoNotDB = 'yes';
	}

	if($filename == $checkDB2)
	{
	 $DoNotDB2 = 'yes';
	}

	$fileNames = $fileNames.$filename."\n";
}

if(empty($DoNotDB))
{require 'database.php';}

// -- BOC Hashing, 15.10.2017.
// -- Redirect to Hash Page to ensure every page is hashed
// -- to create avoid caching on page.
/*$needle = "";
if (isset($_GET['guid']))
{
}
else
{
	$needle = "";
	$customerLogin = 'customerLogin';

	// -- if its $_POST
	if(!empty($_POST))
	{
		if(contains($customerLogin, $_SERVER['REQUEST_URI']))
		{
			$needle = $customerLogin;
		}
	}

	$paramPage = "?";
	if(contains($paramPage, $_SERVER['REQUEST_URI']))
	{
		$needle = $paramPage;
	}

	switch($needle)
	{
		case $customerLogin:
		{
			// -- Do not redirect to another page here.
			break;
		}
		// -- Pages That already has parameters.
		case $paramPage:
		{
			redirectParam($_SERVER['REQUEST_URI']);
			break;
		}
		default:
		{
			redirect($_SERVER['REQUEST_URI']);
			break;
		}
	}
}*/
//echo "<h2>".$_SERVER['REQUEST_URI']."</h2>";
// -- EOC Hashing, 15.10.2017.
// -- BOC Hashing, 15.10.2017.
// -- Redirect to Hash Page to ensure every page is hashed
// -- to create avoid caching on page.
$customerLogin = 'customerLogin';
$needle = "";

if (isset($_GET['guid']))
{
		// -- non-login Pages: applicationForm,applicationFunder,customerLogin.
		if(
		   contains('KgomanocashloansapplicationForm', $_SERVER['REQUEST_URI']) or
		   contains('avovisionLanding', $_SERVER['REQUEST_URI']) or 
		   contains('avovisionRegistration', $_SERVER['REQUEST_URI']) or 
		   contains('avovisoncustomerLogin', $_SERVER['REQUEST_URI']) or
		   contains('avovisioncustomerLogin', $_SERVER['REQUEST_URI']) or
		   contains('applicationForm', $_SERVER['REQUEST_URI']) or
		   contains('applicationFunder', $_SERVER['REQUEST_URI']) or
		   contains('customerLogin', $_SERVER['REQUEST_URI']) or
		   contains('index', $_SERVER['REQUEST_URI']) or
		   contains('forgotpassword', $_SERVER['REQUEST_URI']) or
		   contains('resetpassword', $_SERVER['REQUEST_URI']) or
		 //contains('upload', $_SERVER['REQUEST_URI']) or [(13/May/2022, issue with upload pages)]
		   contains('loancalculator', $_SERVER['REQUEST_URI']) or
		   contains('debitorder',$_SERVER['REQUEST_URI']) or
		   contains('cashpay',$_SERVER['REQUEST_URI']) or
		   contains('accountverification',$_SERVER['REQUEST_URI']) or
		   contains('debicheck',$_SERVER['REQUEST_URI']) or
		   contains('naedo',$_SERVER['REQUEST_URI']) or
		   contains('contactus',$_SERVER['REQUEST_URI']) or
		   contains('aboutus',$_SERVER['REQUEST_URI']) or
		   contains('addleads',$_SERVER['REQUEST_URI']) or
		   contains('rentalapplication',$_SERVER['REQUEST_URI']) or
		   contains('legaldocuments',$_SERVER['REQUEST_URI']) or	// -- 30.06.2021
		   contains('/?', $_SERVER['REQUEST_URI']))
			{
					// -- do Nothing.
			}
		 // -- Login Page : Page loaded after user has logged in.Except for Index Page.
			else
			{
				//-- BOC Check if logged in, if not logged In.
							 if(isset($_SESSION['role']))
							 {
								 // -- Do not redirect.

							 }
							 else
							 {

								// -- its okay, navigate to login back.
								redirect($customerLogin);
							 }
				// -- EOC Check if logged in, if not logged In.
			}
}
else
{
	$needle = "";

	// -- if its $_POST
	if(!empty($_POST))
	{
		if(contains($customerLogin, $_SERVER['REQUEST_URI']))
		{
			$needle = $customerLogin;
		}
	}

	$paramPage = "?";
	if(contains($paramPage, $_SERVER['REQUEST_URI']))
	{
		$needle = $paramPage;
	}

	switch($needle)
	{
		case $customerLogin:
		{
			// -- Do not redirect to another page here.
			break;
		}
		// -- Pages That already has parameters.
		case $paramPage:
		{
			redirectParam($_SERVER['REQUEST_URI']);
			break;
		}
		default:
		{
			redirect($_SERVER['REQUEST_URI']);
			break;
		}
	}
}
// -- EOC Hashing, 15.10.2017.
/* ONLY for Development Box.
if(empty($DoNotDB2))
{require $checkDB2;}
*/

	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();

	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $data['name'];
		$phone    = $data['phone'];
		$fax      = $data['fax'];
		// -- BOC Encrption 16.09.2017
		$_SESSION['phone'] = $phone;
		$_SESSION['fax']   = $fax;
		$website  = $data['website'];
		$_SESSION['website'] = $website;
		// -- EOC Encrption 16.09.2017
		$email    = $data['email'];
		$_SESSION['adminemail'] =  $email;// -- Encrption 16.09.2017
		$adminemail = $_SESSION['adminemail'];
		$_SESSION['companyname'] =  $Company; // -- Encrption 16.09.2017
		$logo     = $data['logo'];
		$logoTemp = $logo;
		$_SESSION['logo'] =  $logo;// -- Encrption 16.09.2017

		$currency = $data['currency'];
		$legalterms = $data['legaltext'];

		$street = $data['street'];
		$suburb = $data['suburb'];
		$city = $data['city'];
		$State = $data['province'];

		// -- Read Province Name -- //
		$sql = 'SELECT * FROM province WHERE provinceid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($State));
		$dataProvince = $q->fetch(PDO::FETCH_ASSOC);
		$State = $dataProvince['provincename'];
		$province = $State;
		$PostCode = $data['postcode'];
		$registrationnumber = $data['registrationnumber'];


		$terms = $data['terms'];
		$ncr = $data['ncr'];

		// --- Banking Details ------ //
		$disclaimer = $data['disclaimer'];
		$bankname = $data['bankname'];
		$accountholdername = $data['accountholdername'];
		$accountnumber = $data['accountnumber'];
		$branchcode  = $data['branchcode'];
		$accounttype = $data['accounttype'];

	// -- BOC Global Session Values --- //
		$_SESSION['MasterCompany']  = $data['name'];
		$_SESSION['GlobalCompany']  = $data['name'];
		$_SESSION['Globalphone']    = $data['phone'];
		$_SESSION['Globalfax']      = $data['fax'];
		$_SESSION['Globalemail']    = $data['email'];
		$_SESSION['Globalwebsite']  = $data['website'];
		$_SESSION['Masterlogo']     = $logo;
		$_SESSION['GloballogoTemp'] = $data['logo'];
		$_SESSION['Globalcurrency'] = $data['currency'];
		$_SESSION['Globallegalterms'] = $data['legaltext'];
		$_SESSION['Globalterms'] = $data['terms'];
		$_SESSION['Globalstreet'] = $data['street'];
		$_SESSION['Globalsuburb'] = $data['suburb'];
		$_SESSION['Globalcity'] = $data['city'];
		$_SESSION['Globalstate'] = $data['province'];
		$_SESSION['Globalpostcode'] = $data['postcode'];
		$_SESSION['Globalregistrationnumber'] = $data['registrationnumber'];
		$_SESSION['intelliterms'] = $data['intelliterms'];
		$_SESSION['intelliterms'] = str_replace('[date]',"<b>".date('Y-m-d')."</b>",$_SESSION['intelliterms']);

		$_SESSION['intelliterms'] = str_replace('[BR]',"<br/>",$_SESSION['intelliterms']);
		$_SESSION['intelliterms'] = str_replace('[LI]',"<li>",$_SESSION['intelliterms']);
		$_SESSION['intelliterms'] = str_replace('[ELI]',"</li>",$_SESSION['intelliterms']);

		// -- Email Server Hosting
		$_SESSION['Globalmailhost']  = $data['mailhost'];
		$_SESSION['Globalport'] = $data['port'];
		$_SESSION['Globalmailusername'] = $data['username'];
		$_SESSION['Globalmailpassword'] = $data['password'];
		$_SESSION['GlobalSMTPSecure'] = $data['SMTPSecure'];
		$_SESSION['Globaldev'] = $data['dev'];
		$_SESSION['Globalprod'] = $data['prod'];
		$_SESSION['Globalcaptcha'] = $data['captcha'];
		$_SESSION['Globalerrorlog'] = $data['errorlog'];
		$_SESSION['Globaltwofactors'] = $data['twofactors']; // Enable two factors 26.03.2021

		$_SESSION['2fa_client_id'] 	   = $data['2fa_client_id']; // Enable two factors 19.05.2021
		$_SESSION['2fa_client_secret'] = $data['2fa_client_secret']; // Enable two factors 19.05.2021
		$_SESSION['2fa_appname'] 	   = $data['2fa_appname']; // Enable two factors 19.05.2021

	// -- EOC Master Session Values --- //
	}

// -- Switch them off when done debugging ErrorLog --//
if(isset($_SESSION['Globalerrorlog']))
{
	if(!empty($_SESSION['Globalerrorlog']))
	{
		error_reporting( E_ALL );
		ini_set('display_errors', 1);
	}
}
// -- Switch them off when done debugging ErrorLog --//

// -- To avoid : Redeclaring trim.
if (!function_exists('trim_input'))
{
	// -- Trim Input
	function trim_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);

	  return $data;
	}
}

if (!function_exists('ReplacePeriodWithSpace'))
{
// -- Replace the Period "." with new line.<br />
function ReplacePeriodWithSpace($content)
{
  $content = str_replace(".",".<br/>",$content);
  return $content;
}
}

/* -- Check Login Session Timeout - 30.09.2017.*/
if(isset($_SESSION["username"]))
{

	if(!isLoginSessionExpired())
	{
	//	header("Location:index");
	} else
	{
	 /*-- Session Timeout 06.01.2019 -- */
		/*echo
			"<div class='modal fade' id='logout_popup' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-body'>
				<div style='width:100%;height:100%;margin: 0px; padding:0px'>
					<div style='width:25%;margin: 0px; padding:0px;float:left;'>
						<i class='fa fa-warning' style='font-size: 140px;color:#da4f49'></i>
					</div>
					<div style='width:70%;margin: 0px; padding:0px;float:right;padding-top: 10px;padding-left: 3%;'>
						<h4>Your session is about to expire!</h4>
						<p style='font-size: 15px;'>You will be logged out in <span id='timer' style='display: inline;font-size: 30px;font-style: bold'>10</span> seconds.</p>
						<p style='font-size: 15px;'>Do you want to stay signed in?</p>
					</div>
				</div>
			</div>
			<div class='clearfix'></div>
			<div style='margin-left: 30%;margin-bottom: 20px;margin-top: 20px;'>
				<a href='javascript:;' onclick='resetTimer()' class='btn btn-primary' aria-hidden='true'>Yes, Keep me signed in</a>
				<a href='<?php echo 'Logout.php';?>' class='btn btn-danger' aria-hidden='true'>No, Sign me out</a>
			</div>
		</div>
	</div>
</div>";	*/
				/*-- Session Timeout 06.01.2019 -- */
		header("Location:Logout?session_expired=1");
	}
}
function isLoginSessionExpired()
{
	$login_session_duration = 3600; // -- 30 minutes (60sec x 30) Session Expires.
	$current_time = time();
	$loggedin_time = getsession('loggedin_time');
	$username = getsession("username");
	if(empty($loggedin_time))
	{
		$loggedin_time = (int) 0;
	}

	//if(isset($_SESSION['loggedin_time']) and isset($_SESSION["username"]))
	if(!empty($username))
	{
		if(((time() - $loggedin_time ) > $login_session_duration))
		{
			unset($_SESSION['loggedin_time']);
			session_start();//session is a way to store information (in variables) to be used across multiple pages.
			session_unset();
			session_destroy();
			//echo '<a href="www.google.co.za">Link</a>';

			// -- Audit Log.
			$arrayLog   	  = array();
			$arrayLog[] 	  = 'Active';
			$arrayLog[] 	  =  $username;
			$dataAuditLogUser = GetAuditActiveUser($arrayLog);
			// -- Update the Audit Log.
			$arrayLogOut      = array();
			if(isset($dataAuditLogUser['audittrailid']))
			{
				$logoutdatetime = date("Y-m-d H:i:s");
				$audittrailid   = $dataAuditLogUser['audittrailid'];
				$arrayLogOut[] = 'Inactive';
				$arrayLogOut[] = $logoutdatetime;
				$arrayLogOut[] = $audittrailid;
				UpdateAuditLog($arrayLogOut);
			}
			return true;
		}
	}
	return false;
}
/*function isLoginSessionExpired()
{
	$login_session_duration = 1800; // -- 30 minutes (60sec x 30) Session Expires.
	$current_time = time();
	if(isset($_SESSION['loggedin_time']) and isset($_SESSION["username"]))
	{
		if(((time() - $_SESSION['loggedin_time']) > $login_session_duration))
		{
			unset($_SESSION['loggedin_time']);
			return true;
		}
	}
	return false;
}*/

/* -- Debugging
   session_start();
    echo "<h3> PHP List All Session Variables</h3>";
    foreach ($_SESSION as $key=>$val)
    echo $key." ".$val."<br/>";

*/
// -- Check Login Session Timeout - 30.09.2017.

// ************************************************************************* //
// -------------- EOC Read the Master Settings from the DataBase. -------------- //
// ************************************************************************* //
// -- EOC -- 2017.06.24
?>
<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
	<!-- Title -->
	<title><?php echo $Company; ?></title>
   <!-- Meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
	<!-- Favicon -->
	<link href="assets/img/Icon_vogs.ico" rel="shortcut icon">

	<!-- Bootstrap Core CSS- Bootstrap 3 -->
	<link rel="stylesheet" href="assets/css/bootstrap.css" rel="stylesheet">
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/1.11.1/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	
	
	<!-- Bootstrap Core CSS- Bootstrap 5 2023-09-16 
	<link rel="stylesheet" href="assets/bootstrap5/bootstrap-5.0.2-dist/css/bootstrap.css" rel="stylesheet">
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/1.11.1/jquery.min.js"></script>
    <script src="assets/bootstrap5/bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
	<link href="assets/bootstrap5/bootstrap-5.0.2-dist/css/bootstrap.min.css" rel="stylesheet">-->

	<!-- Testing -->
	<link href="media/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="resources/demo.css">
	<!-- Testing -->

	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
	</script>
        <!-- Template CSS -->
	<link rel="stylesheet" href="assets/css/animate.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/nexus.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/custom.css" rel="stylesheet">

<!-- Dynamic DataTable CSS
<link href="media/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="resources/demo.css">
	<style type="text/css" class="init">

	div.dataTables_wrapper {
		margin-bottom: 3em;
	}

	</style>
--->
<!-- Dynamic DataTable JS
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
	</script>
	<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="resources/demo.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">

$(document).ready(function() {
	$('table.display').DataTable();
} );

	</script>
	 ---->
<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 3em; margin: 10px 0}
p{ margin: 8px 0}
/*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}*/
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background:white; /*#e7edee; 30.06.2017*/
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] {
	cursor:pointer;
	width:100%;
	border:none;
	background:#991D57;
	background-image:linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-moz-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-webkit-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	color:#FFF;
	font-weight: bold;
	margin: 20px 0;
	padding: 10px;
	border-radius:5px;
}
input[type="submit"]:hover {
	background-image:linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-moz-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-webkit-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}

h2{ font-size: 31.5;}

h3{ font-size: 20px;}

</style>
<script>
  //webshim.setOptions('basePath', '/js-webshim/minified/shims/');
  //request the features you need:
 // webshim.polyfill('es5 mediaelement forms');
  $(function(){
    // use all implemented API-features on DOM-ready
  });
  // -- disable right click on my web page?
  //document.addEventListener('contextmenu', event => event.preventDefault());

</script>
        <!-- Google Fonts-->
        <link href="assets/css/Roboto.css" rel="stylesheet" type="text/css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120665770-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120665770-1');
</script>

		<!--
<link href="media/css/dataTables.bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
		<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
		<link rel="stylesheet" type="text/css" href="resources/demo.css">
		<style type="text/css" class="init">
			div.dataTables_wrapper {
			margin-bottom: 3em;
			}
		</style>

		 <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"> </script>

		<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
		</script>
		<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js">
		</script>
		<script type="text/javascript" language="javascript" src="resources/demo.js">
		</script>
		<script type="text/javascript" language="javascript" class="init">
			$(document).ready(function()
			{
				$('table.display').DataTable();
			} );
		</script>
		-->

	<!------------------------------ Editor -------------------------------
	<!-- Loan Calculator --->
		<link href="assets/css/LoanCalculator.css" type="text/css" rel="stylesheet"/>
		<script src="assets/js/LoanCalculator.js"></script>

		<script src="assets/js/2.1.4/jquery.min.js"></script>
		<script src="assets/js/3.3.5/bootstrap.min.js"></script>
		<script src="assets/js/editor.js"></script>
		<script>
			$(document).ready(function()
			{
				$("#txtEditor").Editor();
				$("#txtEditorAdmin").Editor();
				$("#txtEditorCustomer").Editor();
			});
		</script>
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
		<link href="assets/css/editor.css" type="text/css" rel="stylesheet"/>

<script>
/*
window.onbeforeunload = function()
{
    //Ajax request to update the database
	endSession();
}*/
function endSession()
{
	username = <?php if(isset($_SESSION['username'])){echo '"'.$_SESSION['username'].'"';}else{ echo '"'.'"';} ?>;
$.ajax({
     url: 'Logout.php',
	 data: {username:username},
     success: function (result)
	 {}
    });
}
function goodbye(e) {
        if(!e) e = window.event;
        //e.cancelBubble is supported by IE - this will kill the bubbling process.
        e.cancelBubble = true;
        e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog

        //e.stopPropagation works in Firefox.
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
    }
// -- Standard
window.onbeforeunload = function (e)
{
	  return null;
    e = e || window.event;
        e.preventDefault();
    // For IE and Firefox prior to version 4
    if (e) {
        //e.returnValue = 'Any string';
    }
else { e.returnValue = 'Any string chome';}
//endSession();
    // For Safari


//remove||re-assign onbeforeunload on hover
    $('a[href^="javascript:"]')
      .hover(
             function(){window.onbeforeunload=null;},
             function(){window.onbeforeunload=$(window).data('beforeunload');}
            );
 return null;
//exitComfirm();
  };
    //return 'Any string';
//};


//window.onbeforeunload = exitComfirm();//goodbye(this);
function exitComfirm()
  {

//remove||re-assign onbeforeunload on hover
    $('a[href^="javascript:"]')
      .hover(
             function(){window.onbeforeunload=null;},
             function(){window.onbeforeunload=$(window).data('beforeunload');}
            );


    if(confirm('Are you sure you want to leave the current page?'))
	{
      return true;
    }
	else
	{
      if(window.event) {
        window.event.returnValue = false;
      } else {
        e.preventDefault();
      }
      return false;
    }
  }
function exitAlert()
  {
    /*if(confirm('Are you sure you want to leave the current page?'))
	{
	  endSession();
      return true;
    }
	else
	{
      if(window.event) {
        window.event.returnValue = false;
      } else {
        e.preventDefault();
      }
      return false;
    }*/
//		  endSession();

	return "string";
  }
</script>
	<!------------------------------ EOC Editor --------------------------->
    </head>


<body>
        <div id="body-bg">
            <!-- Phone/Email -->
			<?php if($menu_item != 1000){  ?>
            <div id="pre-header" class="background-gray-lighter">
                <div class="container no-padding">
                    <div class="hidden-xs">
                        <div class="col-sm-6 padding-vert-5">
						<?php
								//session_start();
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
									{
                           echo "<h4><strong>Customer Care:&nbsp;".$_SESSION['phone']."</strong></h4>";
									}
								  else
									{
                           echo "<h4><strong>Customer Care:&nbsp;".$_SESSION['Globalphone']."</strong></h4>";
									}
						?>
                        </div>
                        <div class="col-sm-6 text-right padding-vert-5">
						<?php
								//session_start();
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
									{

										// User Code & Customer id
										$CustomerIdEncoded =  urlencode(base64_encode(getsession('username')));
										$UserCodeEncoded =  urlencode(base64_encode(getsession('UserCode')));
										//--<strong>Welcome ".$_SESSION['Title']." ".$_SESSION['FirstName']." ".$_SESSION['LastName']."</strong>
										echo "<a id='userprofile' name='userprofile' href='userprofile?customerid=".$CustomerIdEncoded."&usercode=".$UserCodeEncoded."' class='fa fa-lg fa-3x fa-user'></a>";
										//echo "<strong>&nbsp;&nbsp;Email:</strong>&nbsp;".$_SESSION['email']."";
									} else
									{
										echo "<h4><strong>Email:&nbsp;".$_SESSION['Globalemail']."</strong></h4>";
								    }
									?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Phone/Email -->
            <!-- Header style="height:190px"  -->
			<div id="header">
               <!-- <div style="border:solid" class="container"> -->
                   <!--<div>	<div  class="row"> -->
                    <div class="container">
                        <!-- Logo -->
						<div class="row">
							<div  class="logo">
								<a  href="index.php" title="">
								<!-- "assets/img/logo-theone.png" -->
			<img  src="images/<?php if(contains('avo', $_SERVER['REQUEST_URI']))
									{echo 'logo_avo.jpg';}
								 else if(contains('KgomanocashloansapplicationForm', $_SERVER['REQUEST_URI']))
									{echo 'kgomanologo.jpg';}
								    else if(contains('rentalapplication', $_SERVER['REQUEST_URI']))
									{echo 'intelli.jpg';}
									else
									{echo $logo;}?>" alt="Logo" width="577">
									<!-- <img src="assets/img/logo.png" alt="Logo" /> -->
								</a>
							</div>
						</div>
                        <!-- End Logo -->
                   </div>
               <!-- </div> -->
            </div>
			<?php }  ?>
            <!-- End Header -->
            <!-- Top Menu -->
			<?php if($menu_item != 1000){  ?>
				<?php include_once('menu.php');?>
			<?php }  ?>
            <!-- End Top Menu -->
            <!-- === END HEADER === -->
            <!-- === BEGIN CONTENT === -->
            <!-- <div id="content" class="bottom-border-shadow"> -->
                <?php include_once($page_content);?>
            <!--</div>-->
            <!-- === END CONTENT === -->
            <!-- === BEGIN FOOTER === -->
			<?php if($menu_item != 1000){  ?>
            <div id="base">
                <div class="container bottom-border padding-vert-30">
                    <div><!-- <div class="row"> -->
                        <!-- Disclaimer  -->
                        <div class="col-md-4">
                            <h4 class="class margin-bottom-10"><font color="#FFFFFF">Disclaimer</font></h4>
                            <p><?php echo ReplacePeriodWithSpace(trim_input($disclaimer));?>
                        </div>
                        <!-- End Disclaimer -->
                        <!-- Contact Details -->
                        <div class="col-md-3 margin-bottom-20">
						<!-- Dynamic Global Settings 24.07.2017 -->
                            <h4 class="margin-bottom-10"><font color="#FFFFFF">Contact Details</font></h4>
                            <p>
                                <span class="fa-phone">Telephone:</span><?php echo $phone; ?>
                                <br>
                                <span class="fa-envelope">Email:</span>
                                <a <?php echo "href=mailto:".$adminemail.""; ?> style="color:white"><?php echo $adminemail; ?></a>
                                <br>
                                <span class="fa-link">Website:</span>
                                <a <?php echo "href=".$website.""; ?> style="color:white"><?php echo $website; ?></a>
                            </p>
							<!-- Dynamic Global Settings 24.07.2017 -->
                            <p><?php echo $street; ?>
                                <br><?php echo $city; ?>
                                <br><?php echo $province; ?>
                                <br><?php echo $PostCode; ?></p>
                        </div>
                        <!-- End Contact Details -->
                        <!-- Sample Menu -->
                        <div class="col-md-2 margin-bottom-20">
		                            <h4 class="margin-bottom-10"><font color="#FFFFFF">Company</font></h4>
		                            <ul class="menu">
		                               <!-- <li>
		                                    <a class="fa fa-home fa-fw" href="aboutus">About Us</a>
		                                </li>-->
																		<?php
																			echo "<li><a class='fa-info' href='aboutus'>About Us</a></li>";
																		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
																		{
																			echo "<li><a class='fa-university href='applicationFormAnother'>Apply for Loan</a></li>";
																			echo "<li><a  class='fa-sign-out' id='Login' name='Login' href='Logout'>Logout</a></li>";
																		} else
																		{
																			echo "<li><a class='fa-university' href='applicationForm'>Apply for Loan</a></li>";
																			echo "<li><a href='customerLogin' class='fa-sign-in'>Login</a></li>";
																	    }
																	?>
									                                <!-- <li>
																	<a href="customerLogin.php" class="fa-gears">e-loan login</a>
									                                </li> -->
									                 </ul>
									        <!-- <div class="clearfix"></div>-->
                        </div>
                        <!-- End Sample Menu -->
												<div class="col-md-3">
														<h4 class="class margin-bottom-10"><font color="#FFFFFF">Legal Documents</font></h4>
																<a  href="legaldocuments" style="color:white">Privacy Policy</a></p>
																<a  href="legaldocuments" style="color:white">Terms and Conditions</a></p>

												</div>
                    </div>
                </div>
            </div>
            <!-- Footer -->


            <div id="footer" class="background-grey">
                <div class="container">
                    <div><!-- <div class="row"> -->
                        <!-- Footer Menu -->
                        <div id="footermenu" class="col-md-8">
                            <ul class="list-unstyled list-inline">
                                <li>
                                    <a href="index" target="_blank">Home <?php echo 'modise = '.$menu_item;?> </a>
                                </li>
									<?php
									if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
								    {
										echo "<li><a href='applicationFormAnother' >Apply for Loan</a></li>";
										echo "<li><a id='Login' name='Login' href='Logout' class='fa-gears'>Logout</a></li>";
									} else
									{
										echo "<li><a href='customerLogin' class='fa-gears' >Login</a></li>";
										echo "<li><a href='applicationForm' >Apply for Loan</a></li>";
								    }
									?>
							</ul>
                        </div>
                        <!-- End Footer Menu -->
                        <!-- Copyright -->
                        <div id="copyright" class="col-md-4">
                            <p class="text-center"><?php echo "Copyright "." &copy;"." ".date('Y').' '.$Company;?></p>
                        </div>
                        <!-- End Copyright -->
                    </div>
                </div>

            </div>
			<?php }?>
            <!-- End Footer
            <script type="text/javascript" src="assets/js/slimbox2.js" charset="utf-8"></script>
			-->
			<script  type="text/javascript" src="assets/js/scripts.js"></script>
			<script  type="text/javascript" src="assets/js/scripts_habitat.js"></script>
			<!-- Mobile Menu - Slicknav
            <script type="text/javascript" src="assets/js/jquery.slicknav.js" type="text/javascript"></script>-->

<!-- Trying -->
            <!-- Animate on Scroll
            <script type="text/javascript" src="assets/js/jquery.visible.js" charset="utf-8"></script>-->
            <!-- Sticky Div
            <script type="text/javascript" src="assets/js/jquery.sticky.js" charset="utf-8"></script>-->
            <!-- Slimbox2
            <script type="text/javascript" src="assets/js/slimbox2.js" charset="utf-8"></script>
            <!-- Modernizrf
            <script src="assets/js/modernizr.custom.js" type="text/javascript"></script>
-->
<!-- EOC Trying -->
<!-- Isotope - Portfolio Sorting
            <script type="text/javascript" src="assets/js/jquery.isotope.js" type="text/javascript"></script>-->
            <!-- Mobile Menu - Slicknav -->
            <script type="text/javascript" src="assets/js/jquery.slicknav.js" type="text/javascript"></script>
            <!-- Animate on Scroll-->
            <script type="text/javascript" src="assets/js/jquery.visible.js" charset="utf-8"></script>
            <!-- Sticky Div -->
            <script type="text/javascript" src="assets/js/jquery.sticky.js" charset="utf-8"></script>
            <!-- Slimbox2-->
            <script type="text/javascript" src="assets/js/slimbox2.js" charset="utf-8"></script>
            <!-- Modernizr -->
            <script src="assets/js/modernizr.custom.js" type="text/javascript"></script>
            <!-- End JS -->
            <!-- End JS -->
		</div>
    </body>
</html>
<!-- === END FOOTER === -->
