<?php
date_default_timezone_set('Africa/Johannesburg');

// * To change this template, choose Tools | Templates
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database_ffms.php';
require  $filerootpath.'/database.php';
// -- email confirmation 23/5/2023....
if(!function_exists('email_confirmation'))
{
	function email_confirmation($dataConfirmation)
	{
		 $emailFlag     = 'yes';
		 $tenantid      = $dataConfirmation['IntUserCode'];
		 $customerid    = $dataConfirmation['Member_id'];
		 require  $dataConfirmation['filerootpath'].'/effms_confirmation.php';
	}
}
// -- Update seq_generator table...06/05/2023
if(!function_exists('update_seq_generator_fin'))
{
	function update_seq_generator_fin($dataFilter)
	{
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$message = '';
		try
		{
			$sql = "UPDATE Seq_Generator_fin
			SET Next_Seq_Number = ?, Previous_Seq_Number = ? WHERE Client_Id = ?;";
			$q = $pdo->prepare($sql);
			$q->execute(array(
			$dataFilter['Next_Seq_Number'],	$dataFilter['Previous_Seq_Number'],$dataFilter['Client_Id']));
		}
		catch (Exception $e)
	    {
			$message = 'Caught exception: '.  $e->getMessage(). "\n";
	    }
	  return $message;
	}
}
// -- Get seq_generator table...06/05/2023
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_seq_generator_fin'))
{
	function get_seq_generator_fin($IntUserCode)
	{
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'Seq_Generator_fin';			
		$sql = "SELECT * FROM $tbl where Client_Id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		if(empty($data))
		{
		// nothing
		}
		return $data;
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_last_memberpolicyid'))
{
	function get_last_memberpolicyid($IntUserCode)
	{
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'member_policy';			
		$sql = "SELECT Member_Policy_id FROM $tbl ORDER BY Member_Policy_id DESC LIMIT 1";
		$q= $pdo->query($sql);
		$data = $q->fetch();
		
		//$q = $pdo->prepare($sql);
		//$q->execute();
		//$data = $q->fetch(PDO::FETCH_ASSOC);
		// -- get sequence number 06/05/2023
		$data_seq = get_seq_generator_fin($IntUserCode);
		$Org_Name=$data_seq['Org_Name'];
		$newPolicy = 0;
		//$newPolicy = (int)$data_seq['Next_Seq_Number'];
		$newPolicy= str_pad($data_seq['Next_Seq_Number'],4,"0",STR_PAD_LEFT);
		
		//Migrate policy number check 2024/04/14
		$policy_number=getpost('policy_number_existing');
		error_log("Before".$policy_number);
			//Generate a policy number
			if(!empty($policy_number))		
			{
					error_log("Inside".$policy_number);
				if(!empty($policy_number)){
					$data['Member_Policy_id']=$policy_number;

				}else{
				//$data_seq['Previous_Seq_Number'] = $newPolicy;
				$data_seq['Previous_Seq_Number'] = str_pad($newPolicy,4,"0",STR_PAD_LEFT);
				$data['Member_Policy_id'] = date('Y').$data_seq['prefix'].$data_seq['Next_Seq_Number'] ;
				$newPolicy = $newPolicy + 1;
				//$data_seq['Next_Seq_Number'] = $newPolicy;
				$data_seq['Next_Seq_Number'] = str_pad($newPolicy,4,"0",STR_PAD_LEFT);
				// call update..
				update_seq_generator_fin($data_seq);
				}
			}
			else
			{
				error_log("Outside".$policy_number);
				// -- Build one
				//$array = str_split($data['Member_Policy_id'],6);
				// -- update sequence
				//$data_seq['Previous_Seq_Number'] = $newPolicy;
				$data_seq['Previous_Seq_Number']=str_pad($newPolicy,4,"0",STR_PAD_LEFT);
				$data['Member_Policy_id'] = date('YM').$data_seq['prefix'].$newPolicy;
				$newPolicy = $newPolicy + 1;
				$data_seq['Next_Seq_Number'] = str_pad($newPolicy,4,"0",STR_PAD_LEFT);
				// call update..
				update_seq_generator_fin($data_seq);

			}
			
		
		return $data;	
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('bill_single_instruction'))
{
	function bill_single_instruction($data)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		////////////////////////////////////////////////////////////////////////
		$message = '';
		$id  = 0;
		$ret = '';
		try
		{
			$tbl = 'collection';
			$sqlData = sql_associative_array($data);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$id = $pdo->lastInsertId();	
			$pdo->commit();
	    }
		catch (PDOException $e)
		{
			$message = $e->getMessage();
		}
		$ret = $id."|".$message;
		return $ret;	
	}
}
////////////////////////////////////////////////////////////////////////
// -- Move Uploaded File -- //
if(!function_exists('moveuploadedfile'))
{
	function moveuploadedfile($name,$tmp_name )
	{
			$today = date('Y-m-d H-i-s');
			$userid = getpost('userid');
			$path = "ffms/upload/".$userid."_".$today."_"; // Upload directory
			$count = 0;
		// No error found! Move uploaded files
		// Filename add datetime - extension.
			$name = date('d-m-Y-H-i-s').$name;
			$ext = 'pdf';//pathinfo($name, PATHINFO_EXTENSION);
		// -- Encrypt The filename.
			$name = md5($name).'.'.$ext;

		// -- Update File Name Directory
	            if(move_uploaded_file($tmp_name , $path.$name))
				{
	            	$count++; // Number of successfully uploaded files
	            }
			$newfilename = $path.$name;
			return 	$newfilename ;
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_bankref'))
{
	function get_ffms_bankref($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = Database::connectDB();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'client';			
		$sql = "select * from $tbl where username = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['IntUserCode']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_package_option'))
{
	function get_ffms_package_option($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'policy';			
		$sql = "select * from $tbl where Policy_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['Policy_id']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_premium_amnt'))
{
	function get_ffms_premium_amnt($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'premium';			
		$sql = "select * from $tbl where PackageOption_id = ? and Years = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['PackageOption_id'],1));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_spouse'))
{
	function get_ffms_spouse($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'spouse';			
		$sql = "select * from $tbl where spouse_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['spouse_id']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}

if(!function_exists('sql_upd_associative_array'))
{
	function sql_upd_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode('= ?,',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}
if(!function_exists('create_ffms_application'))
{
	function create_ffms_application($member,$spouse,$address,$source_of_income,
	$banking_details,$member_policy,$extended_members)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		////////////////////////////////////////////////////////////////////////
		$message = 'member';
		$Member_id  = 0;
		$ret = '';
		try
		{
// 2 -- Spouse
$data_member_Obj = null;
$data_member_Obj = get_ffms_spouse($spouse);
if(!isset($data_member_Obj['spouse_id']))
{
	if(!empty($data_member_Obj['spouse_id']))
	{
			$tbl = 'spouse';
			$sqlData = sql_associative_array($spouse);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$spouse_id = $pdo->lastInsertId();	
	 }
}
else
{
	//$member['spouse_id'] = $data_member_Obj['spouse_id'];	
}			
$message = 'spouse';
// 3 - address
			$tbl = 'address';
			$sqlData = sql_associative_array($address);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$Address_id = $pdo->lastInsertId();	
			//$pdo->commit();	
			$member['Address_id'] = $Address_id;	
$message = 'address';			
$dataFilter['IntUserCode'] = getpost('IntUserCode');
// 1 -- Member			
////////////////////////////////////////////////////////////////////////
// -- Upload the documents..
// -- Upload the uploaded files..
$filenameEncryp1 = "";
$filenameEncryp2 = "";
$filenameEncryp3 = "";
$filenameEncryp4 = "";
$filenameEncryp5 = "";
//print_r($_FILES);
if(isset($_FILES['File']['tmp_name']))
{
$tmp_name1 		= $_FILES['File']['tmp_name'];
$name1     		= $_FILES['File']['name'];
$filenameEncryp1 = moveuploadedfile($tmp_name1,$tmp_name1);
$message = $filenameEncryp1;
}

if(isset($_FILES['File2']['tmp_name']))
{
$tmp_name2 = $_FILES['File2']['tmp_name'];
$name2     = $_FILES['File2']['name'];
$filenameEncryp2 = moveuploadedfile($tmp_name2,$tmp_name2);
}

if(isset($_FILES['File3']['tmp_name']))
{
$tmp_name3 = $_FILES['File3']['tmp_name'];
$name3     = $_FILES['File3']['name'];
$filenameEncryp3 = moveuploadedfile($tmp_name3,$tmp_name3);
}

if(isset($_FILES['File4']['tmp_name']))
{
$tmp_name4 = $_FILES['File4']['tmp_name'];
$name4     = $_FILES['File4']['name'];
$filenameEncryp4 = moveuploadedfile($tmp_name4,$tmp_name4);
}

if(isset($_FILES['File5']['tmp_name']))
{
$tmp_name5 = $_FILES['File5']['tmp_name'];
$name5     = $_FILES['File5']['name'];
$filenameEncryp5 = moveuploadedfile($tmp_name5,$tmp_name5);
}
////////////////////////////////////////////////////////////////////////
$member['FILEIDDOC'] 		 = $filenameEncryp1;  
$member['FILECONTRACT'] 	 = $filenameEncryp2; 
$member['FILEBANKSTATEMENT'] = $filenameEncryp3; 
$member['FILEPROOFEMP'] 	 = $filenameEncryp4; 
$member['FILEFICA'] 		 = $filenameEncryp5;
////////////////////////////////////////////////////////////////////////
//print_r($member);
			$tbl = 'member';
			$sqlData = sql_associative_array($member);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//$Member_id = $pdo->lastInsertId();	
			//$pdo->commit();
			//$member_income['Member_id'] = $Member_id;
			$message = 'member';	
// 4-source of income
			$tbl = 'member_income';
			unset($source_of_income['Member_income_id']);			
			$sqlData = sql_associative_array($source_of_income);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//$Member_income_id = $pdo->lastInsertId();	
			//$pdo->commit();
			//$banking_details['Main_member_id'] = $Member_id;
			$message = 'member_income';
// 5-- Banking Details	...
		if(!empty($banking_details['Debit_date'])){
			$tbl = 'member_bank_account';
			unset($banking_details['Member_bank_account_id']);			
			$sqlData = sql_associative_array($banking_details);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//$Member_bank_account_id = $pdo->lastInsertId();	
			//$pdo->commit();
		}
			$message = 'member_bank_account';

// 6. -- Create Dependent/Extentend member/Benefiaciary Details ...
if(!empty($extended_members)){
			$size = count($extended_members);
			if($size > 0){
			for($i = 0;$i<$size;$i++)
			{
				$tbl = 'extendend_member';
				if(!empty($extended_members[$i]['Ext_member_id']))
				{
				str_replace('<span style="font-size: 12px;">','',$extended_members[$i]['Ext_member_id']);
				str_replace('</span>','',$extended_members[$i]['Ext_member_id']);
				$sqlData = sql_associative_array($extended_members[$i]);
				$sql = "INSERT INTO $tbl
				({$sqlData[0]})
				VALUES ({$sqlData[1]}?)";
				//print($sqlData[0]);
				//print($sqlData[1]);
				//print_r($sqlData[2]);
				$q   = $pdo->prepare($sql);
				$q->execute($sqlData[2]);
				}
		}}}
// 7 -- member policy..
			$member_policy['Creation_date'] = date('Y-m-d');
			$tbl = 'member_policy';
			//unset($member_policy['Member_Policy_id']);	
			$sqlData = sql_associative_array($member_policy);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			
// -- if arrived here then its success
			$Member_id = $member['Member_id'];		

	    }
		catch (PDOException $e)
		{
			$message = $message.$e->getMessage();
		}
		$ret = $Member_id."|".$message;
		return $ret;	
	}
}
//-- update ffms_application. 28/05/2023
if(!function_exists('update_ffms_application'))
{
	function update_ffms_application($member,$spouse,$address,$source_of_income,
	$banking_details,$member_policy,$extended_members)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		////////////////////////////////////////////////////////////////////////
		$message = 'member';
		$Member_id  = 0;
		$ret = '';
		try
		{
// 2 -- Spouse
$data_member_Obj = null;
$data_member_Obj = get_ffms_spouse($spouse);
if(!isset($data_member_Obj['spouse_id']))
{
	if(!empty($data_member_Obj['spouse_id']))
	{
			$tbl = 'spouse';
			$sqlData = sql_upd_associative_array($spouse);
			$sqlData[2][] = $data_member_Obj['spouse_id'];
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE spouse_id = ?";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//$spouse_id = $pdo->lastInsertId();	
	 }
}
else
{
	//$member['spouse_id'] = $data_member_Obj['spouse_id'];	
}			
			$message = 'spouse';
// 3 - address
			$tbl = 'address';
			$sqlData = sql_upd_associative_array($address);
			$sqlData[2][] = $member['Address_id'];
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE Address_id = ?";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			//$Address_id = $pdo->lastInsertId();	
			//$member['Address_id'] = $Address_id;	
			
			$message = 'address';			
			$dataFilter['IntUserCode'] = getpost('IntUserCode');
// 1 -- Member			
////////////////////////////////////////////////////////////////////////
// -- Upload the documents..
// -- Upload the uploaded files..
$filenameEncryp1 = "";
$filenameEncryp2 = "";
$filenameEncryp3 = "";
$filenameEncryp4 = "";
$filenameEncryp5 = "";
//print_r($_FILES);
if(isset($_FILES['File']['tmp_name']))
{
$tmp_name1 		= $_FILES['File']['tmp_name'];
$name1     		= $_FILES['File']['name'];
$filenameEncryp1 = moveuploadedfile($tmp_name1,$tmp_name1);
$message = $filenameEncryp1;
}

if(isset($_FILES['File2']['tmp_name']))
{
$tmp_name2 = $_FILES['File2']['tmp_name'];
$name2     = $_FILES['File2']['name'];
$filenameEncryp2 = moveuploadedfile($tmp_name2,$tmp_name2);
}

if(isset($_FILES['File3']['tmp_name']))
{
$tmp_name3 = $_FILES['File3']['tmp_name'];
$name3     = $_FILES['File3']['name'];
$filenameEncryp3 = moveuploadedfile($tmp_name3,$tmp_name3);
}

if(isset($_FILES['File4']['tmp_name']))
{
$tmp_name4 = $_FILES['File4']['tmp_name'];
$name4     = $_FILES['File4']['name'];
$filenameEncryp4 = moveuploadedfile($tmp_name4,$tmp_name4);
}

if(isset($_FILES['File5']['tmp_name']))
{
$tmp_name5 = $_FILES['File5']['tmp_name'];
$name5     = $_FILES['File5']['name'];
$filenameEncryp5 = moveuploadedfile($tmp_name5,$tmp_name5);
}
////////////////////////////////////////////////////////////////////////
if(empty($filenameEncryp1)){$member['FILEIDDOC'] 		 = $filenameEncryp1;}  
if(empty($filenameEncryp2)){$member['FILECONTRACT'] 	 = $filenameEncryp2;} 
if(empty($filenameEncryp3)){$member['FILEBANKSTATEMENT'] = $filenameEncryp3;} 
if(empty($filenameEncryp4)){$member['FILEPROOFEMP'] 	 = $filenameEncryp4;} 
if(empty($filenameEncryp5)){$member['FILEFICA'] 		 = $filenameEncryp5;}
////////////////////////////////////////////////////////////////////////
//print_r($member);
			$tbl = 'member';
			$sqlData = sql_upd_associative_array($member);
			$sqlData[2][] = $member['Member_id'];
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE Member_id = ?";
			$q   = $pdo->prepare($sql);
			print_r($sqlData[2]);
			$q->execute($sqlData[2]);	
			
			$message = 'member';	
// 4-source of income
			$tbl = 'member_income';
			$Member_income_id = $source_of_income['Member_income_id'];
			unset($source_of_income['Member_income_id']);
			$sqlData = sql_upd_associative_array($source_of_income);
			$sqlData[2][] = $Member_income_id;
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE Member_income_id = ?";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$message = 'member_income';
// 5-- Banking Details	...
			$tbl = 'member_bank_account';
			$Member_bank_account_id = $banking_details['Member_bank_account_id'];
			unset($banking_details['Member_bank_account_id']);
			/*unset($banking_details['Main_member_id']);
			unset($banking_details['Account_number']);
			unset($banking_details['Account_type']);
			unset($banking_details['Bank']);		
			unset($banking_details['Branch_name']);	
			unset($banking_details['Branch_code']);	
			unset($banking_details['Personal_number']);
			unset($banking_details['Debit_date']);	
			unset($banking_details['Commence_date']);
			unset($banking_details['Notify']);		
			unset($banking_details['payment_method']);
			unset($banking_details['IntUserCode']);*/  
			$sqlData = sql_upd_associative_array($banking_details);
			$sqlData[2][] = $Member_bank_account_id;
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE Member_bank_account_id = ?";
			$q   = $pdo->prepare($sql);			
			$q->execute($sqlData[2]);
			$message = 'member_bank_account';
// 6. -- Create Dependent/Extentend member/Benefiaciary Details ...
if(!empty($extended_members)){
			$size = count($extended_members);
			if($size > 0){
			for($i = 0;$i<$size;$i++)
			{
				$tbl = 'extendend_member';
				$sqlData = sql_associative_array($extended_members[$i]);
				$sql = "INSERT INTO $tbl
				({$sqlData[0]})
				VALUES ({$sqlData[1]}?)";
				$q   = $pdo->prepare($sql);
				$q->execute($sqlData[2]);
		}}}
// 7 -- member policy..
			$member_policy['Creation_date'] = date('Y-m-d');
			$tbl = 'member_policy';
			$Member_Policy_id  = $member_policy['Member_Policy_id'];
			unset($member_policy['Member_Policy_id']);			
			$sqlData = sql_upd_associative_array($member_policy);
			$sqlData[2][] = $Member_Policy_id;
			$sql = "UPDATE $tbl SET {$sqlData[0]} = ? WHERE Member_Policy_id = ?";			
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			
// -- if arrived here then its success
			$Member_id = $member['Member_id'];		

	    }
		catch (PDOException $e)
		{
			$message = $message.$e->getMessage();
		}
		$ret = $Member_id."|".$message;
		return $ret;	
	}
}

if(!function_exists('get_ffms_member'))
{
	function get_ffms_member($dataFilter)
	{
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'member';			
		$sql = "select * from $tbl where Member_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['Member_id']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
if(!function_exists('get_ffms_bankbranch'))
{
	function get_ffms_bankbranch($bank)
	{
		$branchcode = "";
	    $pdo = Database::connect();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'bankbranch';			
		$sql = "select * from $tbl where bankid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($bank));
		$data = $q->fetch(PDO::FETCH_ASSOC);
			if(isset($data['branchcode']))
			{
				$branchcode = $data['branchcode'];
			}
		return $branchcode;		
	}
}

////////////////////////////////////////////////////////////////////////
$list = getpost('list'); 
$message = ''; 
$bank = getpost('Bank');
$branchcode = get_ffms_bankbranch($bank);
//var member_gender = document.getElementsByName('im_gender');
$update  = getpost('update');

// 1 -- Member	
$member =
array(
'Member_id' 		    =>getpost('Member_id'),		
'Title' 			 	=>getpost('Title'),	
'First_name' 			=>getpost('First_name'),	
'Middle_name' 			=>getpost('Middle_name'),	
'Initials' 	 			=>getpost('Initials'),	
'Surname' 				=>getpost('Surname'),	
'Gender_id' 			=>getpost('Gender_id'),	
'Date_of_birth'			=>getpost('Date_of_birth'),	 	
'MaritualStatus_id'		=>getpost('MaritualStatus_id'),			
'Marriage_id'			=>getpost('Marriage_id'),	
'Spouse_id'				=>getpost('Spouse_id'),	
'Address_id' 			=>getpost('Address_id'),
'Life_State_id' 		=>getpost('Life_State_id'),		
'Language_id' 			=>getpost('Language_id'),
'FILEIDDOC'				=>getpost('FILEIDDOC'), 		 
'FILECONTRACT'			=>getpost('FILECONTRACT'),	 
'FILEBANKSTATEMENT'		=>getpost('FILEBANKSTATEMENT'),
'FILEPROOFEMP'			=>getpost('FILEPROOFEMP'), 	 
'FILEFICA'				=>getpost('FILEFICA'),         
'IntUserCode' 			=>getpost('IntUserCode'));
// 2 -- Spouse
$spouse =
array(
'spouse_id' 			=>getpost('spouse_id'),			 
'spousetitle'			=>getpost('spousetitle'),
'spousefirst_name' 		=>getpost('spousefirst_name'),
'spousemiddle_name' 	=>getpost('spousemiddle_name'),	
'spouseinitials' 		=>getpost('spouseinitials'),
'spousesurname' 		=>getpost('spousesurname'),
'spousegender' 			=>getpost('spousegender'),
'spousedate_of_birth' 	=>getpost('spousedate_of_birth'),
'spouseaddress' 		=>getpost('spouseaddress'),
'spouselife_State' 		=>getpost('spouselife_State'),
'spouselanguage_id' 	=>getpost('spouselanguage_id'),
'IntUserCode' 			=>getpost('IntUserCode'));

// 3 - address
$address =
array(
'Suburb_id'				=>getpost('Suburb_id'),	  
'Line_1'				=>getpost('Line_1'),	
'Line_2'				=>getpost('Line_2'),	
'Line_3'				=>getpost('Line_3'),
'Line_4'				=>getpost('Line_4'),
'Line_5'				=>getpost('Line_5'),
'Line_6'				=>getpost('Line_6'),
'Line_7'				=>getpost('Line_7'),
'Line_8'				=>getpost('Line_8'),		
'Member_email'			=>getpost('Member_email'),	
'Fax'					=>getpost('Fax'),
'Cellphone'				=>getpost('Cellphone'),	
'Work_tel'				=>getpost('Work_tel'),	
'Home_tel'				=>getpost('Home_tel'),	
'Comm_preference'		=>getpost('Comm_preference'),
'IntUserCode' 			=>getpost('IntUserCode'));
$UserCode2=getpost('IntUserCode');
// 4-source of income
//$source_of_income = getpost('source_of_income');
$source_of_income =
array(
'Member_income_id'		=>getpost('Member_income_id'),
'Source_of_income_id'	=>getpost('Source_of_income_id'),						
'Occupation_id'			=>getpost('Occupation_id'),			
'Member_id'				=>getpost('Member_id'),		
'Income_provider_name'	=>getpost('Income_provider_name'),					        
'Telelphone_nr'			=>getpost('Telelphone_nr'),			        
'Income_provider_email_address'					=>getpost('Income_provider_email_address'),
'IntUserCode' 			=>getpost('IntUserCode'));
// 5-- Banking Details	...
$acc = getpost('Account_number');
//$accHolder = ;
$accType = getpost('Account_type') ;
$banking_details =
array(
'Member_bank_account_id' =>getpost('Member_bank_account_id'),
'Main_member_id' 	=>getpost('Main_member_id'),
'Account_holder' 	=>getpost('Account_holder'),
'Account_number' 	=>$acc,
'Account_type' 	 	=>$accType,
'Bank'	 			=>$bank,
'Branch_name' 	 	=>$branchcode,
'Branch_code' 		=>$branchcode,
'Personal_number' 	=>getpost('Personal_number'),	
'Debit_date' 		=>getpost('Debit_date'),
'Commence_date'		=>getpost('Commence_date'),
'Notify' 			=>getpost('Notify'),
'payment_method'    =>getpost('paymentmethod'),
'notify_method'    =>getpost('Methodofcommunication_name'),
'IntUserCode' 		=>getpost('IntUserCode'));

$Policy_id = getpost('Policy_id');

// 7 -- member_policy
$Member_Policy_id = null;
if($update == 'display:block')
{
	$Member_Policy_id['Member_Policy_id'] = getpost('Member_Policy_id');
}
else
{
	$Member_Policy_id = get_last_memberpolicyid(getpost('IntUserCode'));
}
$member_policy = 
array(
'Member_Policy_id' => $Member_Policy_id['Member_Policy_id'],
'Member_id' =>getpost('Member_id'),
'Policy_id'  =>getpost('Policy_id'), 
'Active' 	 =>getpost('Active'),
'Remarks' 	 =>getpost('Remarks'),
'IntUserCode' 			=>getpost('IntUserCode'));
/*$list['createdon'] = date('Y-m-d');
$list['createdat'] = date("H:i:s");
$list['Status_Code'] = '1'; 									
$list['Status_Description'] = 'pending';*/	
////////////////////////////////////////////////////////////////////////
// Get total number of rows to be inserted..
$rows = getpost('rows');
$extended_members = null;
// 8. -- Dependent Details ...
// 9. -- Extended Dependent Details ...
// 10.-- Benefiaciary Details ...
for($i = 0;$i < $rows;$i++)
{
$extended_members[] = 
array(
'Ext_member_name'	=>getpost('Ext_member_name'.$i),
'middlename'		=>getpost('middlename'.$i),
'Ext_member_surname'=>getpost('Ext_member_surname'.$i),
'Ext_member_id'		=>getpost('Ext_member_id'.$i),
'Relationship'		=>getpost('Relationship'.$i),
'Percentage'		=>getpost('Percentage'.$i),
'Remarks'			=>getpost('Remarks'.$i),
'Active'			=>getpost('Active'.$i),
'Ext_memberType_id' =>getpost('Ext_memberType_id'.$i),
'Life_state_id'		=>getpost('Life_state_id'.$i),
'Main_member_id'	=>getpost('Main_member_id'.$i),
'IntUserCode' 		=>getpost('IntUserCode'.$i));
}

$dataFilter 			 = null;
$dataFilter['Policy_id'] = $Policy_id;
$data = null;
$data = get_ffms_package_option($dataFilter);

$package_option_id = $data['PackageOption_id'];
$dataFilter 			 = null;
$dataFilter['PackageOption_id'] = $package_option_id;
$data = null;
$data = get_ffms_premium_amnt($dataFilter);
$premiun_amnt = $data['Premium_amount'];

// -- Bank_Reference
$dataFilter 	 = null;
$data 			 = null;
$dataFilter['IntUserCode'] = getpost('IntUserCode');
$data 			 = get_ffms_bankref($dataFilter);
$bank_reference  = $data['abbreviatedshortname'];

//Member reposible for the account

//Debtor Identification
if(empty(getpost('debtor_identification'))) //2023/07/09
{
$debtor_identification=getpost('Member_id');
}
else
{
	$debtor_identification=getpost('debtor_identification');
}
//Debtor Document type*
if(empty(getpost('debtor_id_type'))) //2023/07/09
{
$debtor_id_type='I';
//$debtor_id_type==getpost('Client_ID_Type');---to be updated read from table
}
else
{
$debtor_id_type='I';
//$debtor_id_type=getpost('debtor_id_type');
}
//Debtor Telephone Contact
if(empty(getpost('debtor_contact_number'))) //2023/07/09
{
$debtor_contact_number=getpost('Cellphone');
}
else
{
$debtor_contact_number=getpost('debtor_contact_number');
}
//Debtor Email Contact
if(empty(getpost('debtor_email'))) //2023/07/09
{
$debtor_email=getpost('Email');
}
else
{
	$debtor_email=getpost('debtor_email');
}

// -- Schedule Debit Order..
$debitorder = 
array(		
'Initials'			=>getpost('Initials'),
'Account_Holder'	=>getpost('Account_holder'),
'Account_Type'		=>$accType,
'Account_Number'	=>$acc,
'Branch_Code'		=>$branchcode,
'Client_Reference_1'=>$Member_Policy_id['Member_Policy_id'],//getpost('Personal_number'),
'Client_Reference_2'=>$Member_Policy_id['Member_Policy_id'],
//'Bank_Reference'	=>getpost('Bank_Reference'),
'Notify'			=>getpost('Notify'),
'Amount'		    =>$premiun_amnt,
'Action_Date'		=>getpost('Debit_date'),
'createdon'			=>date('Y-m-d'),
//'Contact_No'		=>getpost('Cellphone'), 
'Contact_No'		=>$debtor_contact_number,
'No_Payments'		=>99,
'Service_Mode'		=>'EFT',//getpost('Service_Mode'),
'Service_Type'		=>'SAMEDAY',
'Frequency'			=>'Monthly',
'entry_class'		 =>'0',
'Status_Description' => 'pending',
'Status_Code' 		 => 1,
'Bank_Reference'     => $bank_reference,
//'Client_ID_Type'	=>getpost('Client_ID_Type'),
'Client_ID_Type'	=>$debtor_id_type,
//'Client_ID_No'		->(if(empty(getpost('debtor_identification'))){'Client_ID_No'		=>getpost('Member_id')}else{'Client_ID_No'		=>getpost('debtor_identification')}),//2023/07/09
//'Client_ID_No'		=>getpost('Member_id'),
'Client_ID_No'		=> $debtor_identification,
'Client_Id'		    =>getpost('IntUserCode'),
'createdby' 		=>getpost('createdby'),
'IntUserCode'		=>getpost('IntUserCode'),
'UserCode' 			=>getpost('UserCode'),
'debitorder_date'   =>getpost('Debit_date'));
///////////////////////////////////////////////////////////////////////////////////
// -- Schedule Debit Check Mandate..13/08/2022
$status_code = '01';
$status_desc = 'pending'; 
///////////////////////////////////////////////////////////////////////////////////
$DebtaccType = '';
if($accType == '1'){$DebtaccType = 'CURRENT';}
if($accType == '2'){$DebtaccType = 'SAVINGS';}
if($accType == '3'){$DebtaccType = 'CURRENT';}
///////////////////////////////////////////////////////////////////////////////////
$debitcheckmandate   = null;
$arrayInstruction[1] = $Member_Policy_id['Member_Policy_id']; // Reference
$arrayInstruction[2] = $Member_Policy_id['Member_Policy_id']; // Reference
$arrayInstruction[3] = 'Y';    							      // tracking indicator
$arrayInstruction[4] = '0230';	 							  // debtor_auth_code
$auth_type 			 = 'Real Time';                           // auth_type first 9  characters.
$arrayInstruction[5] = $auth_type; 							  // auth type
$arrayInstruction[6] = 'RCUR';							      // instalment_occurence');
$arrayInstruction[7] = 'MNTH';						          // Frequency');
$arrayInstruction[8] = getpost('Debit_date');				  // mandate_initiation_date');
$arrayInstruction[9] = getpost('Debit_date');		          // first_collection_date');
$arrayInstruction[10] = 'ZAR';								  // collection_amount_currency');
$arrayInstruction[11] = $premiun_amnt;					      //'collection_amount');
$arrayInstruction[12] = 'ZAR';				                  //'maximum_collection_currency');
$arrayInstruction[13] = $premiun_amnt;				          //$maximum_collection_amount 			= getpost('maximum_collection_amount');
$arrayInstruction[14] = '0021';			      				  //$entry_class Insurance Premium						= getpost('entry_class');
$arrayInstruction[15] = getpost('Account_holder');			  //$debtor_account_name 				= getpost('debtor_account_name');
//$arrayInstruction[16] = getpost('Member_id');			      //$debtor_identification 				= getpost('debtor_identification');
$arrayInstruction[16] = $debtor_identification; //2023/07/09
//if(empty(getpost('debtor_identification'))){$arrayInstruction[16]=getpost('Member_id')}else{$arrayInstruction[16]=getpost('debtor_identification');//2023/07/09
$arrayInstruction[17] = $acc;								  //$debtor_account_number 				= getpost('debtor_account_number');
$arrayInstruction[18] = $DebtaccType;						  //$debtor_account_type 				= getpost('debtor_account_type');
$arrayInstruction[19] = $branchcode;					      //$debtor_branch_number 				= getpost('debtor_branch_number');
//$arrayInstruction[20] = getpost('Cellphone');				  //$debtor_contact_number 				= getpost('debtor_contact_number');
$arrayInstruction[20] = $debtor_contact_number;				  //$debtor_contact_number 				= getpost('debtor_contact_number');
//$arrayInstruction[21] = getpost('Email');					  //$debtor_email 						= getpost('debtor_email');
$arrayInstruction[21] = $debtor_email;						  //$debtor_email 						= getpost('debtor_email');
$col_date 			  = DateTime::createFromFormat("Y-m-d", getpost('Debit_date'));
					 
 
if(!empty($col_date)){$db_collection_day    = $col_date->format("d");}else{$db_collection_day="0000-01-31";}
  
								
 
$arrayInstruction[22] = $db_collection_day;
$arrayInstruction[23] = 'Y';								  //$date_adjustment_rule_indicator 	= getpost('date_adjustment_rule_indicator');
$arrayInstruction[24] = 'A';							      //$adjustment_category 				= getpost('adjustment_category');
$arrayInstruction[25] = '0.00';								  //$adjustment_rate 					= getpost('adjustment_rate');
$arrayInstruction[26] = 'ZAR';								  //$adjustment_amount_currency 		= getpost('adjustment_amount_currency');
$arrayInstruction[27] = '0.00';								  //$adjustment_amount 					= getpost('adjustment_amount');
$arrayInstruction[28] = 'ZAR';								  //$first_collection_amount_currency   = getpost('first_collection_amount_currency'); 
$arrayInstruction[29] = $premiun_amnt; 			  			  //= getpost('first_collection_amount');
$arrayInstruction[30] = 'USAGE BASED';							  //$debit_value_type 					= getpost('debit_value_type');
$arrayInstruction[31] = ' ';								  //$cancellation_reason 				= getpost('cancellation_reason');
$arrayInstruction[31] = ' ';		
$arrayInstruction[32] = ' ';								  //$amended 							= getpost('amended');
$arrayInstruction[33] = $status_code;						  //getpost('status_code');
$arrayInstruction[34] = $status_desc;						  //getpost('status_desc');
$arrayInstruction[35] = $debtor_id_type;			          //$document_type 						= getpost('Client_ID_Type');
$arrayInstruction[36] = '';									  //$amendment_reason_md16 				= getpost('amendment_reason_md16');
$arrayInstruction[36] = ' ';
$arrayInstruction[37] = ' ';								  //$amendment_reason_text 				= getpost('amendment_reason_text');
$arrayInstruction[37] = ' ';
$arrayInstruction[38] = 'N';								  //$tracking_cancellation_indicator 	= getpost('tracking_cancellation_indicator');		
$arrayInstruction[39] = getpost('UserCode');
$arrayInstruction[40] = getpost('createdby');
$arrayInstruction[41] = date('Y-m-d');
$arrayInstruction[42] = getpost('Debit_date') ;
$arrayInstruction[43] = getpost('IntUserCode');
$arrayInstruction[44] = getsession('nominated_account');       //nominated account
$arrayInstruction[45] = 'N';       //RMS Mandate set to Y
$arrayInstruction[46] = '07';      //Collection indicator Tracking  
//////////////////////////////////////////////////////////////////////////////////////
//print_r($debitorder);

$databill_single_instructionObj = null;
// -- Check Member details
$memberObj = get_ffms_member($member);
$retMember = null;

if($update == 'display:block')
{
// -- Update ffms application.
$retMember 		= update_ffms_application($member,$spouse,$address,$source_of_income,$banking_details,$member_policy,
$extended_members);
}
else
{
// -- Create ffms application.
$retMember 		= create_ffms_application($member,$spouse,$address,$source_of_income,$banking_details,$member_policy,
$extended_members);
}
$rowData 			= explode(utf8_encode('|'), $retMember);  // -- for UTF-8
$memberid 			= $rowData[0];

if(!empty($memberid))
{
// -- Create Single Bill Instruction.
if(getpost('paymentmethodname') == 'Debit Order EFT')
{
	$retCollection = bill_single_instruction($debitorder);
	$rowData = explode(utf8_encode('|'), $retCollection);  // -- for UTF-8
	$collectionid = $rowData[0];

// -- Payment was made -- //
	$message = 'success|'.$memberid.' '.$collectionid.'pay:'.getpost('paymentmethodname').'|'.$Member_Policy_id['Member_Policy_id'];	
	
	//SMS as Comms
	 if(getpost('Methodofcommunication_name') == 'SMS')
	 {
		$data_sms = get_seq_generator_fin(getpost('IntUserCode'));
		$contents ='Dear '.strtoupper(getpost('Initials')).' '.ucfirst(strtolower(getpost('Surname'))).','.$data_sms['eft_message'].' '.$Member_Policy_id['Member_Policy_id'].'. '.'Please call'.' '.$data_sms['ffms_phone'].' '.'for enquires';
		error_log(print_r('SMS',TRUE));
		/*$smstemplatename = 'single_debitorder_uploaded';*/
		$applicationid = 0;		
		//send SMS										
		if(!empty($contents))
		{	
			error_log(print_r('Sending SMS',TRUE));
			$notification['phone'] 		 	= getpost('Cellphone');
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= getpost('UserCode');	
			$notification['usercode'] 	 	= getpost('UserCode');
			$notification['createdby'] 	 	= getpost('createdby');
			$notification['IntUserCode'] 	= getpost('IntUserCode');
			$notification['amount'] 		= getpost('Amount');
			$notification['Account_Holder'] = getpost('Account_holder');										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
		}
  
		}
		else{
			//Do nothing - other commas method to be added
		}
}
error_log(print_r(getpost('Notify'),TRUE));
	if(getpost('Notify') == 'Yes')
	{
	 error_log(print_r('Notify Yes',TRUE));
	 
	 //SMS as Comms
	 if(getpost('Methodofcommunication_name') == 'SMS')
	 {
		$data_sms = get_seq_generator_fin(getpost('IntUserCode'));
		$contents ='Dear '.strtoupper(getpost('Initials')).' '.ucfirst(strtolower(getpost('Surname'))).','.$data_sms['sms_message'].' '.$Member_Policy_id['Member_Policy_id'].'. '.'Please call'.' '.$data_sms['ffms_phone'].' '.'for enquires';
		error_log(print_r('SMS',TRUE));
		/*$smstemplatename = 'single_debitorder_uploaded';*/
		$applicationid = 0;		
		//auto_template_content($userid,$smstemplatename,$applicationid,$list['Bank_Reference'],$list['Action_Date']);
		/*
		if(getpost('IntUserCode') == 'UFRF')
		{
			$contents ='DEAR '.getpost('Initials').' '.getpost('Surname').', THANK YOU FOR CHOOSING RFUNERALS. YOUR MEMBERSHIP NUMBER '.$Member_Policy_id['Member_Policy_id'].' IS ACTIVE. PLEASE CALL 0649477681 FOR ENQUIRIES';
		}
		if(getpost('IntUserCode') == 'UFAC')
		{
		    $contents ='DEAR '.getpost('Initials').' '.getpost('Surname').', THANK YOU FOR CHOOSING AVIRI CREDIT. YOUR MEMBERSHIP NUMBER '.$Member_Policy_id['Member_Policy_id'].' IS ACTIVE. PLEASE CALL 0878204506 FOR ENQUIRIES';	
		}
		if(getpost('IntUserCode') == 'UFLF')
		{
			$contents ='DEAR '.getpost('Initials').' '.getpost('Surname').', THANK YOU FOR CHOOSING LEHASA FUNERAL DIRECTORS. YOUR MEMBERSHIP NUMBER '.$Member_Policy_id['Member_Policy_id'].' IS ACTIVE. PLEASE CALL 0878204506 FOR ENQUIRIES';
		}
		if(getpost('IntUserCode') == 'UFNB')
		{
			$contents ='DEAR '.getpost('Initials').' '.getpost('Surname').', THANK YOU FOR CHOOSING NKUNAS BURIAL SOCIETY. YOUR MEMBERSHIP NUMBER '.$Member_Policy_id['Member_Policy_id'].' IS ACTIVE. PLEASE CALL 0727211417 FOR ENQUIRIES';
		}
		if(getpost('IntUserCode') == 'UEUF')
		{
			$contents ='DEAR '.getpost('Initials').' '.getpost('Surname').', THANK YOU FOR CHOOSING UNIQUE FUNERALS. YOUR ACTIVE MEMBERSHIP NUMBER IS'.$Member_Policy_id['Member_Policy_id'].'. PLEASE CALL 0104426748 FOR ENQUIRIES';
		}*/
	 }
			//send SMS										
	if(!empty($contents))
	{	
		error_log(print_r('Sending SMS',TRUE));
		$notification['phone'] 		 	= getpost('Cellphone');
		$notification['content'] 	 	= $contents;
		$notification['reference_1'] 	= getpost('UserCode');	
		$notification['usercode'] 	 	= getpost('UserCode');
		$notification['createdby'] 	 	= getpost('createdby');
		$notification['IntUserCode'] 	= getpost('IntUserCode');
		$notification['amount'] 		= getpost('Amount');
		$notification['Account_Holder'] = getpost('Account_holder');										
		// -- 
		$notificationsList[] = $notification;	// add into list
		// -- notify clients who signed for notifications(queue)
		if(!empty($notificationsList)){auto_sms_queue($notificationsList); }
										  
	}
	
}
/////////////////////////////////////////////////////////////////////////////
if( getpost('paymentmethodname') == 'Debi Check')//MULU
{
	error_log(print_r($arrayInstruction,TRUE));
	
	$message         = auto_create_debicheck_mandate($arrayInstruction); 
	$Action_Date     = getpost('Debit_date');
	$reference       = $Member_Policy_id['Member_Policy_id'];
	$collectionid    = '';
	//$smstemplatename = 'single_debitorder_uploaded';
	//$applicationid   = 0;								
	// -- Sent to info@ecashmeup.com -- //
	//auto_email_collection('8707135963082',$smstemplatename,$applicationid,$reference,$Action_Date);
	error_log(print_r('before '.$message, TRUE)); 
	if($message != 'success')
	{
		$message = 'ERROR|'.$message;
		error_log(print_r('Inside '.$message, TRUE)); 
	}
	else
	{
		
		 $dataConfirmation['IntUserCode']  = getpost('IntUserCode');
		 $dataConfirmation['Member_id']    = $memberid;
		 $dataConfirmation['filerootpath'] = $filerootpath;
		 //-auto email pdf
		 //email_confirmation($dataConfirmation);
		 	// -- Debi check created -- //
			error_log(print_r('else'.$message, TRUE)); 
		 $message = 'success|'.$memberid.' '.$collectionid.'pay:'.getpost('paymentmethodname').'|'.$Member_Policy_id['Member_Policy_id'];	
	}
}
if( getpost('paymentmethodname') == 'Cash')//20240116
{
	////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		////////////////////////////////////////////////////////////////////////
	    //Insert into bank account table a record
		$tbl = 'member_bank_account';
		unset($banking_details['Member_bank_account_id']);			
		$sqlData = sql_associative_array($banking_details);
		$sql = "INSERT INTO $tbl
		({$sqlData[0]})
		VALUES ({$sqlData[1]}?)";
		$q   = $pdo->prepare($sql);
		$q->execute($sqlData[2]);
		$message = 'member_bank_account';
		$Action_Date     = getpost('Debit_date');
		$reference       = $Member_Policy_id['Member_Policy_id'];
		$collectionid    = '';
		error_log(print_r('Cash Customer',TRUE));
	 //SMS as Comms
		if(getpost('Methodofcommunication_name') == 'SMS')
		{
			$data_sms = get_seq_generator_fin(getpost('IntUserCode'));
			$contents ='Dear '.strtoupper(getpost('Initials')).' '.ucfirst(strtolower(getpost('Surname'))).','.$data_sms['cash_message'].' '.$Member_Policy_id['Member_Policy_id'].'. '.'Please call'.' '.$data_sms['ffms_phone'].' '.'for enquires';
			error_log(print_r('SMS',TRUE));
			/*$smstemplatename = 'single_debitorder_uploaded';*/
			$applicationid = 0;		
			//send SMS										
			if(!empty($contents))
			{	
			error_log(print_r('Sending SMS',TRUE));
			$notification['phone'] 		 	= getpost('Cellphone');
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= getpost('UserCode');	
			$notification['usercode'] 	 	= getpost('UserCode');
			$notification['createdby'] 	 	= getpost('createdby');
			$notification['IntUserCode'] 	= getpost('IntUserCode');
			$notification['amount'] 		= getpost('Amount');
			$notification['Account_Holder'] = getpost('Account_holder');										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList))
			{
				auto_sms_queue($notificationsList); 
			}
		}

		}

		error_log(print_r('before '.$message, TRUE)); 
		if($message != 'success')
		{
		$message = 'ERROR|'.$message;
		error_log(print_r('Inside '.$message, TRUE)); 
		}
		else
		{

		$dataConfirmation['IntUserCode']  = getpost('IntUserCode');
		$dataConfirmation['Member_id']    = $memberid;
		$dataConfirmation['filerootpath'] = $filerootpath;
		//-auto email pdf
		//email_confirmation($dataConfirmation);
		// -- Debi check created -- //
		error_log(print_r('else'.$message, TRUE)); 
		$message = 'success|'.$memberid.' '.$collectionid.'pay:'.getpost('paymentmethodname').'|'.$Member_Policy_id['Member_Policy_id'];	
		}
		error_log(print_r('Cash Customer Done',TRUE));
}
/////////////////////////////////////////////////////////////////////////////
if( getpost('paymentmethodname') == 'PayAt')//20240116
{
	
	 //SMS as Comms
	 if(getpost('Methodofcommunication_name') == 'SMS')
	 {
		$data_sms = get_seq_generator_fin(getpost('IntUserCode'));
		$contents ='Dear '.strtoupper(getpost('Initials')).' '.ucfirst(strtolower(getpost('Surname'))).','.$data_sms['payAt_message'].$Member_Policy_id['Member_Policy_id'].'. '.'Please call'.' '.$data_sms['ffms_phone'].' '.'for enquires';
		error_log(print_r('SMS',TRUE));
		/*$smstemplatename = 'single_debitorder_uploaded';*/
		$applicationid = 0;		
		//send SMS										
		if(!empty($contents))
		{	
			error_log(print_r('Sending SMS',TRUE));
			$notification['phone'] 		 	= getpost('Cellphone');
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= getpost('UserCode');	
			$notification['usercode'] 	 	= getpost('UserCode');
			$notification['createdby'] 	 	= getpost('createdby');
			$notification['IntUserCode'] 	= getpost('IntUserCode');
			$notification['amount'] 		= getpost('Amount');
			$notification['Account_Holder'] = getpost('Account_holder');										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
		}
  
		}
		else{
			//Do nothing - other commas method to be added
		}
}
/////////////////////////////////////////////////////////////////////////////	

/////////////////////////////////////////////////////////////////////////////
if( getpost('paymentmethodname') == 'EasyPay')//20240116
{
	
	 //SMS as Comms
	 if(getpost('Methodofcommunication_name') == 'SMS')
	 {
		$data_sms = get_seq_generator_fin(getpost('IntUserCode'));
		$contents ='Dear '.strtoupper(getpost('Initials')).' '.ucfirst(strtolower(getpost('Surname'))).','.$data_sms['easyPay_message'].' '.$Member_Policy_id['Member_Policy_id'].'. '.'Please call'.' '.$data_sms['ffms_phone'].' '.'for enquires';
		error_log(print_r('SMS',TRUE));
		/*$smstemplatename = 'single_debitorder_uploaded';*/
		$applicationid = 0;		
		//send SMS										
		if(!empty($contents))
		{	
			error_log(print_r('Sending SMS',TRUE));
			$notification['phone'] 		 	= getpost('Cellphone');
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= getpost('UserCode');	
			$notification['usercode'] 	 	= getpost('UserCode');
			$notification['createdby'] 	 	= getpost('createdby');
			$notification['IntUserCode'] 	= getpost('IntUserCode');
			$notification['amount'] 		= getpost('Amount');
			$notification['Account_Holder'] = getpost('Account_holder');										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList);}
		}
  
		}
		else{
			//Do nothing - other commas method to be added
		}
}
/////////////////////////////////////////////////////////////////////////////	
}
else
{
	$message = 'ERROR|'.$rowData[1];
}

/*if(isset($databill_single_instructionObj['collectionid']))
{
	// -- Single Bill Instruction warning message.
	$message =  $message.'|Warning: Debit Order already exist for on '.
	$databill_single_instructionObj['Action_Date'].' from bank account number '.
	$databill_single_instructionObj['Account_Number'].' with client reference '.
	$databill_single_instructionObj['Client_Reference_1'];			
}*/
echo $message;

?>