<div class="row setup-content" id="step-1">
	<div class="controls">
		<div class="container-fluid" style="border:1px solid #ccc ">
             </br>
			 <div class="panel-body">


				<div class="row">

				<p>Dear applicant. Welcome and thank you for taking the time to complete your application. This is crucial step towards capacitating your business</p>
				</br>
				<p> Please complete this application in full with all the required information, including the required supporting documents. Where the information requested is not applicable, please write “Not Applicable” or “N/A”. Incomplete applications will not be accepted. All applications declined by Avocado Vision or withdrawn by the applicant, will not be kept on our records. Please do not submit master/original documents. ​</p>
				</br>
				<p><strong>NOTE: THIS FORM IS FREE AND NO FEES ARE PAYABLE TO AVOCADO VISION OR ANY OTHER PERSONS FOR THE COMPLETION OR ASSISTANCE WITH THIS APPLICATION FORM.</strong></p>
				</br>
				<p><strong>Please prepare the following documents:</strong></p>
				<div class="col-sm-6">
				<ul>
					<li>Certified ID copies of all Directors</li>
					<li>Affidavit from directors or members that they are aware of the contents of the application form</li>
					<li>Curriculum Vitae(s) of all the Directors of the Company ​</li>
					<li>CIPC Registration Certificate </li>
					<li>Tax Clearance Certificate</li>
					<li>Letter of Good Standing with Compensation Fund</li>
					<li>Unemployment Insurance Fund Compliance Certificate</li>
					<li>Valid PAYE Registration Certificate</li>
					<li>Valid VAT Certificate</li>
					<li>Personal Bank Statements for the past 12 months </li>				
					<li>Official Business Bank Confirmation Letter</li>
				</ul>
				
				</div>
				<div class="col-sm-6">
				<ul>
					<li>Business Bank Statements for the past 12 months ​</li>
					<li>Attached Personal Credit Report</li>
					<li>Attached Business Credit Report </li>
					<li>BBBEEE affidavit/certificate</li>
					<li>Tribal Authority letter - Permission to use land</li>
					<li>A valid signed offtake agreement </li>
					<li>Expressed interest in establishing a charcoal focused business</li>
					<li>Approved Business Plan</li>
					<li>Amount of biomass produced within the last twelve (12) months</li>
					<li>Amount of charcoal produced within the last twelve (12) months</li>
					<li>Amount of revenue generated within the last twelve (12) months </li>
				</ul>
				</div>
				
				</div>


           </div>
     </div>
 </div>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<!-- <button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>-->
<a style="<?php echo $backHTML;?>" class="btn btn-primary previousBtn btn-lg pull-right" href="finfindmember">Back</a>

</div>
<br/>
