<?php
date_default_timezone_set('Africa/Johannesburg');
require 'database.php';

if (!function_exists('updatecollections2'))
{
	function updatecollections2($arrayCollections)
	{
	 try
	  {
		//Global $pdo;
		$message = '';
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "UPDATE collection
		SET
		Account_Holder = ?,
		Account_Type = ?,
		Account_Number = ?,
		Branch_Code = ?,
		No_Payments = ?,
		Action_Date = ?,
		Amount = ?,
		Status_Code = ?,
		Status_Description = ?,
		changedby = ?,
		changedon = ?,
		changedat = ?
		WHERE collectionid = ?;";
		$q = $pdo->prepare($sql);
		
		$arrayCollections[12] = date("H:i:s");
		
		$q->execute(array(
		$arrayCollections[1],
		$arrayCollections[2],
		$arrayCollections[3],
		$arrayCollections[4],
		$arrayCollections[5],
		$arrayCollections[6],
		$arrayCollections[7],
		$arrayCollections[8],
		$arrayCollections[9],
		$arrayCollections[10],
		$arrayCollections[11],
		$arrayCollections[12],
		$arrayCollections[0]));
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		$message = 'Caught exception: '.  $e->getMessage(). "\n";
	  }
	}
}

$collectionid 			 = getpost('collectionid'); 	
$Account_Holder          = getpost('Account_Holder');  
$Account_Type	         = getpost('Account_Type');	
$Account_Number          = getpost('Account_Number');  
$Branch_Code		     = getpost('Branch_Code');		
$No_Payments		     = getpost('No_Payments');		
$Action_Date		     = getpost('Action_Date');		
$Amount		             = getpost('Amount');			

$changedby  = getpost('changedby');
$changedon  = getpost('changedon');
$changedat  = getpost('changedat');

$Status_Code 			 = getpost('Status_Code');
$Status_Description 	 = getpost('Status_Description');


// -- Update Collection.
$message = '';
$arrayCollections[] = $collectionid;
$arrayCollections[] = $Account_Holder;
$arrayCollections[] = $Account_Type;
$arrayCollections[] = $Account_Number;
$arrayCollections[] = $Branch_Code;
$arrayCollections[] = $No_Payments;
$arrayCollections[] = $Action_Date;
$arrayCollections[] = $Amount;

$arrayCollections[] = $Status_Code;
$arrayCollections[] = $Status_Description;

$arrayCollections[] = $changedby;
$arrayCollections[] = $changedon;
$arrayCollections[] = $changedat;



// -- Function.
$message = updatecollections2($arrayCollections);
if(empty($message))
{
	 $message = 'updated successfully';
}
echo $message;
	
?>