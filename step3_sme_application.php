<div class="row setup-content" id="step-3">
	<div class="col-xs-12">
		<div class="col-md-12">
			 <div class="container-fluid" style="border:1px solid #ccc ">
				 <fieldset class="border" >
					 <legend class ="text-left">Business Information</legend>
				</fieldset>
				<div class="row">
								
		

							 <!--Full Names-->
                 <div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Full_Names">Business name<span style="color:red">*</span></label>
                         <input id="im_business_name" type="text" name="im_business_name" class="form-control" placeholder="" required="required" maxlength="30" data-error="first_name is required." value = "<?php echo $im_business_name;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>
							
								<div class="col-md-3">
										 <div class="form-group">
												 <label for="form_Account_Holder">CIPC reg number<span style="color:red">*</span></label>
												 <input id="im_business_cipc_no" type="text" name="im_business_cipc_no" class="form-control" placeholder="" required="required"  maxlength="30" data-error="im_surname is required." value = "<?php echo $im_business_cipc_no;?>">
												 <div class="help-block with-errors"></div>
										 </div>
								</div>
							


							<div class="col-md-3">
								 <div class="form-group">
										 <label for="form_IdentificationNumber">Type of business<span style="color:red">*</span></label>
										 <input onkeyup="id_to_dob()" id="im_business_type" type="text" name="im_business_type" class="form-control" placeholder="" required="required" maxlength="30" data-error="member_id is required." value = "<?php echo $im_business_type;?>">
										 <div class="help-block with-errors"></div>
								 </div>
							</div>

						
							
							<div class="col-md-3">
								<div class="form-group">
								<label for="form_ownership_type">Type of products offered<span style="color:red">*</span></label>
								<input id="im_products_offered" type="text" name="im_products_offered" class="form-control" placeholder="" value = "<?php echo $im_products_offered;?>">
								<div class="help-block with-errors"></div>
								</div>
							</div>
							
						
							<div class="col-md-3">
								<div class="form-group">
								<label for="form_Shareholding">Business location<span style="color:red">*</span></label>
								<input id="im_business_location" type="text" name="im_business_location" class="form-control" placeholder="" value = "<?php echo $im_business_location;?>">
								<div class="help-block with-errors"></div>
								</div>
							</div>
						

				</div>
			</br>

           </div>
           </br>

<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Business Address</legend>
</fieldset>
</br>
 <div class="row">
 <div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Physical Address</label>

							<input id="im_line_1" type="text" name="im_line_1" class="form-control" placeholder="" maxlength="100" value = "<?php echo $im_line_1;?>">

							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Suburb</label>
								<input id="im_line_2" type="text" name="im_line_2" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_2;?>">
							<div class="help-block with-errors"></div>
					</div>
			</div>

			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Town</label>
							<input id="im_line_3" type="text" name="im_line_3" class="form-control" placeholder=""  maxlength="30"  value = "<?php echo $im_line_3;?>">
							<div class="help-block with-errors"></div>
		 					
					</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
						<label for="form_Province">Province</label>
						 <input id="im_line_4" type="text" name="im_line_4" class="form-control" placeholder=""  maxlength="100"  value = "<?php echo $im_line_4;?>">
						 <div class="help-block with-errors"></div>
					
				</div>
			</div>
			
			<div class="col-md-3">
					<div class="form-group">
							<label for="form_No_Payments">Postal code</label>

							<input id="im_line_5" type="text" name="im_line_5" class="form-control" placeholder=""  maxlength="100" data-error="P.O.Box is required." value = "<?php echo $im_line_5;?>">

							<div class="help-block with-errors"></div>
					</div>
			</div>
</div>



</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Stage of business</legend>
</fieldset>
</br>
 <div class="row">
 <div class="col-md-3">
			<div class="form-group">
			<label class="checkbox-inline"><input type="radio" id="im_stage" name="im_stage" value="Start-up (less than a year)" /> Start-up (less than a year)</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<label class="checkbox-inline"><input type="radio" id="im_stage"  name="im_stage" value="Existing (1-3 years in existence)" /> Existing (1-3 years in existence)</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<label class="checkbox-inline"><input type="radio" id="im_stage" name="im_stage" value="Existing (more than 3 years in existence)" /> Existing (more than 3 years in existence)</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>
</div>



</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Business Banking Details</legend>
</fieldset>
</br>
		<div class="row">

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account holder</label>
								 <input id="Account_holder" type="text" name="Account_holder" class="form-control" placeholder=""  maxlength="30" data-error="Account_holder  is required." value = "<?php echo $Account_holder;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
				 		<div class="form-group">
				 			<label for="form_account_type">Account Type</label>
				 				<select class="form-control" id="Account_type" name="Account_type">
				 					<?php
				 					$Account_typeSelect = $Account_type;
				 					foreach($Account_TypeArr as $key => $row)
				 					{
										$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
				 						if($rowData[0]  == $Account_typeSelect)
				 						{echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
				 						else
				 						{echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
				 					}
				 					if(empty($Account_TypeArr ))
				 					{echo  '<option value="0" selected>No Account_types</option>';}

				 					?>
				 					</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
				  </div>

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account Number</label>
								 <input id="Account_number" type="text" name="Account_number" class="form-control" placeholder=""  maxlength="30" data-error="Account_number is required." value = "<?php echo $Account_number;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
						 	 <div class="form-group">
						 		 <label for="form_broker_name">Bank</label>
						 			 <select class="form-control" id="Bank" name="Bank">
						 				 <?php
						 				 $BankSelect = $Bank;
										foreach($bankArr as $key => $row )
										{
										      $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
						 					 if($rowData[0] == $BankSelect)
						 					 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										 }
										 if(empty($bankArr ))
										 {echo  '<option value="0" selected>No Banks</option>';}

						 				 ?>
						 				 </select>
						 						<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
						 				 <div class="help-block with-errors"></div>
						 		 </div>
						 </div>
					 </div>



</div>
</br>
<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Brush Cutter</label>
		 <input id="im_Brush_Cutter" type="number" name="im_Brush_Cutter" class="form-control" placeholder=""  maxlength="6" data-error="Brush Cutter is required." value = "<?php echo $Brush_Cutter;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>

</br>
</br>
</br>
<div class="container mt-4">
    <h2 class="text-center">Technical Training (Certification)</h2>
    <button class="btn btn-success mb-2" onclick="addRow()">Add Training</button>
    <table class="table table-bordered" id="trainingTable">
        <thead>
        <tr>
            <th>Training</th>
            <th>Rate</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Explanatory Notes</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" class="form-control" value="Chainsaw" id="training1"></td>
            <td><input type="number" class="form-control" id="rate1" value="4500" min="0" onchange="updateTotal(1)"></td>
            <td><input type="number" class="form-control" id="quantity1" value="10" min="0" onchange="updateTotal(1)"></td>
            <td id="total1">R 45 000,00</td>
            <td><textarea class="form-control">5 DAYS COURSE, TO UP MY PRODUCTION I NEED AT LEAST 10...</textarea></td>
            <td><button class="btn btn-danger" onclick="removeRow(this)">Remove</button></td>
        </tr>
        <tr>
            <td><input type="text" class="form-control" value="First Aid" id="training2"></td>
            <td><input type="number" class="form-control" id="rate2" value="850" min="0" onchange="updateTotal(2)"></td>
            <td><input type="number" class="form-control" id="quantity2" value="1" min="0" onchange="updateTotal(2)"></td>
            <td id="total2">R 850,00</td>
            <td><textarea class="form-control"></textarea></td>
            <td><button class="btn btn-danger" onclick="removeRow(this)">Remove</button></td>
        </tr>
        <tr>
            <td><input type="text" class="form-control" value="Health and Safety" id="training3"></td>
            <td><input type="number" class="form-control" id="rate3" value="950" min="0" onchange="updateTotal(3)"></td>
            <td><input type="number" class="form-control" id="quantity3" value="1" min="0" onchange="updateTotal(3)"></td>
            <td id="total3">R 950,00</td>
            <td><textarea class="form-control"></textarea></td>
            <td><button class="btn btn-danger" onclick="removeRow(this)">Remove</button></td>
        </tr>
        <tr>
            <td><input type="text" class="form-control" value="Herbicide" id="training4"></td>
            <td><input type="number" class="form-control" id="rate4" value="2500" min="0" onchange="updateTotal(4)"></td>
            <td><input type="number" class="form-control" id="quantity4" value="0" min="0" onchange="updateTotal(4)"></td>
            <td id="total4">R 0,00</td>
            <td><textarea class="form-control"></textarea></td>
            <td><button class="btn btn-danger" onclick="removeRow(this)">Remove</button></td>
        </tr>
        <tr>
            <td><input type="text" class="form-control" value="Firefighting" id="training5"></td>
            <td><input type="number" class="form-control" id="rate5" value="1000" min="0" onchange="updateTotal(5)"></td>
            <td><input type="number" class="form-control" id="quantity5" value="1" min="0" onchange="updateTotal(5)"></td>
            <td id="total5">R 1 000,00</td>
            <td><textarea class="form-control"></textarea></td>
            <td><button class="btn btn-danger" onclick="removeRow(this)">Remove</button></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="3">Total</th>
            <th id="grandTotal">R 47 800,00</th>
            <th colspan="2"></th>
        </tr>
        </tfoot>
    </table>
</div>

</br>



<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Business Annual Turnover on the date of signing this application for credit</legend>
</fieldset>
</br>
 <div class="row">
 
	<div class="col-md-2">
		<div class="form-group">
		<label class="checkbox-inline"><input type="radio" id="im_turnover" name="im_turnover" value="R0-R100,000" />R0-R100,000</label>
		<div class="help-block with-errors"></div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
		<!--<label for="form_Gender">Gender</label>-->
		<label class="checkbox-inline"><input type="radio" id="im_turnover" name="im_turnover" value="R100,001-R200,000" />R100,001-R200,000</label>
		<div class="help-block with-errors"></div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
		<!--<label for="form_Gender">Gender</label>-->
		<label class="checkbox-inline"><input type="radio" id="im_turnover" name="im_turnover" value="R200,001-R300,000" />R200,001-R300,000</label>
		<div class="help-block with-errors"></div>
		</div>
	</div>
		<div class="col-md-3">
		<div class="form-group">
		<!--<label for="form_Gender">Gender</label>-->
		<label class="checkbox-inline"><input type="radio" id="im_turnover" name="im_turnover" value="R300,001-R500,000" />R300,001-R500,000</label>
		<div class="help-block with-errors"></div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
		<!--<label for="form_Gender">Gender</label>-->
		<label class="checkbox-inline"><input type="radio" id="im_turnover" name="im_turnover" value="Exceeds R500,001" />Exceeds R500,001</label>
		<div class="help-block with-errors"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		 <div class="form-group">
				 <label for="form_IdentificationNumber">Applicant's annual firewood sales on the date:</label>
				 <input id="im_annual_sales" type="text" name="im_annual_sales" class="form-control" placeholder=""  maxlength="30" data-error="Annual sales is required." value = "<?php echo $im_annual_sales;?>">
				 <div class="help-block with-errors"></div>
		 </div>
	</div>
		<div class="col-md-3">
		 <div class="form-group">
				 <label for="form_IdentificationNumber">The Applicant's Asset Value on the date of signing this agreement:</label>
				 <input id="im_assets_value" type="text" name="im_assets_value" class="form-control" placeholder=""  maxlength="30" data-error="Assets value  is required." value = "<?php echo $im_assets_value;?>">
				 <div class="help-block with-errors"></div>
		 </div>
	</div>
</div>



</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Please provide overall values of the date of this application. Only complete the ones applicable</legend>
</fieldset>
</br>
<div class="row">


<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Kilns">Kilns</label>
		 <input id="im_Kilns" type="number" name="im_Kilns" class="form-control" placeholder=""  maxlength="6" data-error="Kilns  is required." value = "<?php echo $Kilns;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>

<!--<div class="col-md-3">
  <div class="form-group">
	<label>Kilns</label>
	<select class="form-control" id="im_Kilns" name="im_Kilns" >
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div> -->

<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Spades">Spades</label>
		 <input id="im_Spades" type="number" name="im_Spades" class="form-control" placeholder=""  maxlength="6" data-error="Spades  is required." value = "<?php echo $Spades;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>

<!--<div class="col-md-3">
  <div class="form-group">
	<label>Spades</label>
	<select class="form-control" id="im_Spades" name="im_Spades">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->

<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Chainsaws">Chainsaws</label>
		 <input id="im_Chainsaws" type="number" name="im_Chainsaws" class="form-control" placeholder=""  maxlength="6" data-error="Chainsaws  is required." value = "<?php echo $Chainsaws;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>


<!--<div class="col-md-3">
  <div class="form-group">
	<label>Chainsaws</label>
	<select class="form-control" id="im_Chainsaws" name="im_Chainsaws">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->

<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Wheelbarrows">Wheelbarrows</label>
		 <input id="im_Wheelbarrows" type="number" name="im_Wheelbarrows" class="form-control" placeholder=""  maxlength="6" data-error=" Wheelbarrows is required." value = "<?php echo $Wheelbarrows;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>


<!--<div class="col-md-3">
  <div class="form-group">
	<label>Wheelbarrows</label>
	<select class="form-control" id="im_Wheelbarrows" name="im_Wheelbarrows">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->

<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Axes</label>
		 <input id="im_Axes" type="number" name="im_Axes" class="form-control" placeholder=""  maxlength="6" data-error=" Axes is required." value = "<?php echo $Axes;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>



<!--<div class="col-md-3">
  <div class="form-group">
	<label>Axes</label>
	<select class="form-control" id="im_Axes" name="im_Axes">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->

<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Fire Extinguishers</label>
		 <input id="im_Extinguishers" type="number" name="im_Extinguishers" class="form-control" placeholder=""  maxlength="6" data-error=" Fire Extinguishers is required." value = "<?php echo $Extinguishers;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>


<!--<div class="col-md-3">
  <div class="form-group">
	<label>Fire Extinguishers</label>
	<select class="form-control" id="im_Extinguishers" name="im_Extinguishers">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->



<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Lasher Bows</label>
		 <input id="im_Lasher_Bows" type="number" name="im_Lasher_Bows" class="form-control" placeholder=""  maxlength="6" data-error=" Lasher Bows is required." value = "<?php echo $Lasher_Bows;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>




<!--<div class="col-md-3">
  <div class="form-group">
	<label>Lasher Bows</label>
	<select class="form-control" id="im_Lasher_Bows" name="im_Lasher_Bows">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->


<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Scale</label>
		 <input id="im_Scale" type="number" name="im_Scale" class="form-control" placeholder=""  maxlength="6" data-error="Scale is required." value = "<?php echo $Scale;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>



<!--<div class="col-md-3">
  <div class="form-group">
	<label>Scale</label>
	<select class="form-control" id="im_Scale" name="im_Scale">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->


<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Shovels</label>
		 <input id="im_Shovels" type="number" name="im_Shovels" class="form-control" placeholder=""  maxlength="6" data-error="Shovels is required." value = "<?php echo $Shovels;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>


<!--<div class="col-md-3">
  <div class="form-group">
	<label>Shovels</label>
	<select class="form-control" id="im_Shovels" name="im_Shovels">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->


<div class="col-md-3">
 <div class="form-group">
		 <label for="form_Axes">Brush Cutter</label>
		 <input id="im_Brush_Cutter" type="number" name="im_Brush_Cutter" class="form-control" placeholder=""  maxlength="6" data-error="Brush Cutter is required." value = "<?php echo $Brush_Cutter;?>">
		 <div class="help-block with-errors"></div>
 </div>
</div>



<!--<div class="col-md-3">
  <div class="form-group">
	<label>Brush Cutter</label>
	<select class="form-control" id="im_Brush_Cutter" name="im_Brush_Cutter">
	<option>0</option>
	<option>1</option>
	<option>2</option>
	<option>3</option>
	</select> 
  </div>
</div>-->


<div class="col-md-6">
  <div class="form-group">
<div data-mdb-input-init class="form-outline">
  <textarea class="form-control" id="OtherEquipments" name="OtherEquipments" rows="1"></textarea>
  <label class="form-label" for="textAreaExample">Other:add with corresponding values</label>
</div>
 </div>
</div>
</div>
</br>
<div class="row">
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Is your company registered for compensation fund?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_compensation_fund"  name="im_compensation_fund" value="No" >No</label>
	<label class="radio-inline"><input type="radio" id="im_compensation_fund" name="im_compensation_fund" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>	

<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Is your company registered for unemployment insurance fund?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_uif" name="im_uif"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_uif" name="im_uif" value="Yes">Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>	
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Is the company registered for VAT?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_vat_no" name="im_vat_no"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_vat_no"  name="im_vat_no" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Is the company registered for PAYE?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_paye" name="im_paye"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_paye" name="im_paye" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Is the company registered for Income tax?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_income_tax" name="im_income_tax"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_income_tax" name="im_income_tax" value="Yes" >Yes</label>
	</br>
	<input id="im_income_tax_number" type="text" name="im_income_tax_number" class="form-control" placeholder="If yes, enter the income tax number " value = "">
	
	<div class="help-block with-errors"></div>
</div>	
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Do you have an Accounting Officer?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_accounting_officer" name="im_accounting_officer"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_accounting_officer" name="im_accounting_officer" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>

<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Do you have a business plan drafted and approved by all Directors of the company?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_businessplan" name="im_businessplan"  value="No" >No</label>
	<label class="radio-inline"><input type="radio" id="im_businessplan" name="im_businessplan" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Do you have management accounts - Schedule of profile and loss within the last 12 months of operation?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_management_accounts" name="im_management_accounts" value="No" >No</label>
	<label class="radio-inline"><input type="radio" id="im_management_accounts" name="im_management_accounts" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Have you prepared cash flow projections for your business i.e. for at least 3 years?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_cash_flow" name="im_cash_flow"  value="No" >No</label>
	<label class="radio-inline"><input type="radio" id="im_cash_flow" name="im_cash_flow" value="Yes" >Yes</label>
	
	<div class="help-block with-errors"></div>
</div>	
</div>

<div class="col-md-6">
<div class="form-group">
	<label for="form_migration:">Have you received funding from other institutions?<span style="color:red">*</span></label>
	<label class="radio-inline"><input type="radio" id="im_funding_prev" name="im_funding_prev"  value="No" >No</label>
	<label class="radio-inline"><input type="radio"  id="im_funding_prev" name="im_funding_prev" value="Yes" >Yes</label>
	</br>
	<input id="im_funding_details" type="text" name="im_funding_details" class="form-control" placeholder="If yes, please provide details" value = "">
	
	<div class="help-block with-errors"></div>
</div>	
</div>


</div>

</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Purpose of Financing</legend>
</fieldset>
</br>
 <div class="row">
 	<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="checkbox" id="im_startup_cost" name="im_startup_cost" value="Start-up costs" />Start-up costs</label>
			<input type="text" id="im_startup_cost_amount" name="im_startup_cost_amount" class="form-control" placeholder="R" value = "">
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="checkbox" id="im_asset_acquisition" name="im_asset_acquisition" value="Asset acquisition" />Asset acquisition</label>
			<input type="text" id="im_asset_acquisition_amount" name="im_asset_acquisition_amount" class="form-control" placeholder="R" value = "">
			<div class="help-block with-errors"></div>
			</div>
		</div>
</div>
 <div class="row">
 	<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="checkbox" id="im_working_capital" name="im_working_capital" value="Working capital" />Working capital</label>
			<input type="text" id="im_working_capital_amount" name="im_working_capital_amount" class="form-control" placeholder="R" value = "">
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="checkbox" id="im_funding" name="im_funding" value="Total funding required" />Total funding required</label>
			<input id="im_funding_amount" type="text" name="im_funding_amount" class="form-control" placeholder="R" value = "">
			<div class="help-block with-errors"></div>
			</div>
		</div>
</div>



</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Owners' contribution: Have you invested in the business as the owner?</legend>
</fieldset>
</br>
 <div class="row">
		<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="radio" id="im_invested" name="im_invested" value="Yes" />Yes</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="radio" id="im_invested" name="im_invested" value="No" />No</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			<input id="im_invested_free_text" type="text" name="im_invested_free_text" class="form-control" placeholder="If yes,specify the contribution" value = "">
			<div class="help-block with-errors"></div>
			</div>
		</div>

	</div>

</div>
</br>
<div class="container-fluid" style="border:1px solid #ccc ">
<fieldset class="border" >
<legend class ="text-left">Consent</legend>
</fieldset>
 <div class="row">
	<div class="col-md-12">
		<div class="form-group">
		<label>I declare that the information furnished in this application including the supporting documentation is true and correct in every respect, and indemnify Avo Vision of any responsibility for the information submitted.</label>
		</br>
 	<div class="help-block with-errors"></div>
			</div>
		</div>
 </div>
   
     <div class="row">
		<div class="col-md-6">
			<div class="form-group">
			<!--<label for="form_Gender">Gender</label>-->
			<label class="checkbox-inline"><input type="checkbox" id="im_consent" name="im_consent" value="X" /> Agree to Terms and Privacy Policy</label>
			<div class="help-block with-errors"></div>
			</div>
		</div>

	</div>

</div>

		 </br>

			 </br>

        <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
				<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
		</div>
	</div>
</div>

