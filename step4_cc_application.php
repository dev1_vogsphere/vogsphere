<div class="row setup-content" id="step-4">

	<div class="container-fluid" style="border:1px solid #ccc ">
		</br>
		 <div class="panel-body">
				
				<div class="row">
					
					<label>Is the main member responsible for the account payments?</label>			
					<div class="form-check-inline">
						<label class="form-check-label">
						<input type="radio" class="form-check-input" name="optradio" onclick="debtor(1)" checked>Yes
						</label>
					</div>	
					
					<div class="form-check-inline">
						<label class="form-check-label" data-radio="radio_1">
						<input type="radio" class="form-check-input" name="optradio" onclick="debtor(0)" >No
						</label>
					</div>
						
			  </div>
					
				
				</br>
				
				<div class="row" id="Debtor" style="display:none">
				
					<div class="col-md-3 ">
						<div class="form-group">
							<label for="form_Client_ID_Type">Debtor Document type<span style="color:red">*</label>
									<select class="form-control" id="debtor_id_type" name="debtor_id_type">
										 <?php
										 $Client_ID_TypeSelect = $Client_ID_Type;
										 foreach($Client_ID_TypeArr as $row)
										 {
											 $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
											 if($rowData[0] == $Client_ID_TypeSelect)
											 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[0].'</option>';}
											 else
											 {echo  '<option value="'.$rowData[0].'">'.$rowData[0].'</option>';}
										 }
										 ?>
										</select>
												<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
										<div class="help-block with-errors"></div>
							</div>
					</div>

					<div class="col-md-3">
                     <div class="form-group">
                         <label for="form_Client_ID_No">Debtor Identification<span style="color:red">*</span></label>
                         <input id="debtor_identification" type="text" name="debtor_identification" class="form-control" placeholder="" value = "<?php echo $debtor_identification;?>">
                         <div class="help-block with-errors"></div>
                     </div>
					</div>
					
						 <div class="col-md-3">
                     <div class="form-group">
                         <label for="debtor_contact_number">Debtor Telephone Contact <span style="color:red">*</label>
                         <input id="debtor_contact_number" type="text" name="debtor_contact_number" class="form-control"  value = "<?php echo $debtor_contact_number;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>	
				 <div class="col-md-3">
                     <div class="form-group">
                         <label for="debtor_email">Debtor Email Contact<span style="color:red">*</label>
                         <input id="debtor_email" type="text" name="debtor_email" class="form-control" value = "<?php echo $debtor_email;?>">
                         <div class="help-block with-errors"></div>
                     </div>
                 </div>	
					
				</div>
				
				<div class="row">

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account holder<span style="color:red">*</span></label>
								 <input id="Account_holder" type="text" name="Account_holder" class="form-control" placeholder="" required="required" maxlength="30" data-error="Account_holder  is required." value = "<?php echo $Account_holder;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
				 		<div class="form-group">
				 			<label for="form_account_type">Account Type</label>
				 				<select class="form-control" id="Account_type" name="Account_type">
				 					<?php
				 					$Account_typeSelect = $Account_type;
				 					foreach($Account_TypeArr as $key => $row)
				 					{
										$rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
				 						if($rowData[0]  == $Account_typeSelect)
				 						{echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
				 						else
				 						{echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
				 					}
				 					if(empty($Account_TypeArr ))
				 					{echo  '<option value="0" selected>No Account_types</option>';}

				 					?>
				 					</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
				  </div>

					<div class="col-md-3">
						 <div class="form-group">
								 <label for="form_IdentificationNumber">Account Number<span style="color:red">*</span></label>
								 <input id="Account_number" type="text" name="Account_number" class="form-control" placeholder="" required="required" maxlength="30" data-error="Account_number is required." value = "<?php echo $Account_number;?>">
								 <div class="help-block with-errors"></div>
						 </div>
					</div>

					<div class="col-md-3">
						 	 <div class="form-group">
						 		 <label for="form_broker_name">Bank</label>
						 			 <select class="form-control" id="Bank" name="Bank">
						 				 <?php
						 				 $BankSelect = $Bank;
										foreach($bankArr as $key => $row )
										{
										      $rowData = explode(utf8_encode('|'), $row);  // -- for UTF-8
						 					 if($rowData[0] == $BankSelect)
						 					 {echo  '<option value="'.$rowData[0].'" selected>'.$rowData[1].'</option>';}
						 					 else
						 					 {echo  '<option value="'.$rowData[0].'">'.$rowData[1].'</option>';}
										 }
										 if(empty($bankArr ))
										 {echo  '<option value="0" selected>No Banks</option>';}

						 				 ?>
						 				 </select>
						 						<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
						 				 <div class="help-block with-errors"></div>
						 		 </div>
						 </div>
					 </div>

			<div class="row">

						<div class="col-md-3">
							 <div class="form-group">
									 <label for="form_IdentificationNumber">Reference no</label>
									 <input id="Personal_number" type="text" name="Personal_number" class="form-control" placeholder="" maxlength="30" data-error="Reference no. is required." value = "<?php echo $Personal_number;?>">
									 <div class="help-block with-errors"></div>
							 </div>
						</div>

						<div class="col-md-3">
							<div class="form-group">

							 <label for="form_Date_of_Birth:">Commencement date<span style="color:red">*</span></label>
							 <input id="Commence_date" min="<?php if ($backHTML != 'display:block'){echo date('Y-m-d');} ?>" type="date" required="required" name="Commence_date" class="form-control" placeholder="aaa" data-error="Commence_date is required." value = "<?php echo $Commence_date;?>">
						 </div>
						</div>

						<div class="col-md-4">
							<div class="form-group">

							 <label for="form_Date_of_Birth:">Please debit my account on<span style="color:red">*</span></label>
							 <input id="Debit_date" name="Debit_date" min="<?php echo date('Y-m-d'); ?>" type="date"  required="required" name="Debit_date" class="form-control" placeholder="aaa" data-error="Debit_date is required." value = "<?php echo $Debit_date;?>"
							 onChange="applyrules(this)">
													 <div class="help-block with-errors">
							 <div id="rulealert" name="rulealert"><!-- 18.09.2021 -->
								 <span class="closebtn" onclick="this.parentElement.style.display='none';"></span>
							 </div>
							 </div>
						 </div>
						</div>

				</div>

		<div class="row">
			<div class="col-md-3">
									<div class="form-group">
											<label for="Notify:">Notify</label>
											<select class="form-control" id="Notify" name="Notify">
						 <?php
						 $NotifySelect = $Notify;

					 foreach($NotifyArr as $row)
					 {
						 if($row == $NotifySelect)
						 {echo  '<option value="'.$row.'" selected>'.$row.'</option>';}
						 else
						 {echo  '<option value="'.$row.'">'.$row.'</option>';}
					 }
					 ?>
					</select>
							<div class="help-block with-errors"></div>
						</div>
					</div>
				
			<div class="col-md-3">
				<div class="form-group">
						<label for="paymentmethod:">Payment Method</label>
						 <select class="form-control" name="paymentmethod_name" id="paymentmethod_name">
						 <?php
						 $paymentmethodSelect = $PaymentMethodList;
						
						 foreach($PaymentMethodList  as $key => $rowData)
						 {
							 if($rowData->Method_of_contribution_id == $paymentmethodSelect)
							 {echo  '<option value="'.$rowData->Method_of_contribution_id.'" selected>'.$rowData->Method_of_contribution_name.'</option>';}
							 
							 else
							 {
								echo  '<option value="'.$rowData->Method_of_contribution_id.'">'.$rowData->Method_of_contribution_name.'</option>';
								//echo '<option value="'.$rowData->Method_of_contribution_id.'"selected>'.$rowData->Method_of_contribution_id.'">'.$rowData->Method_of_contribution_name.'</option>';
								
							 }

						 }
						 if(empty($PaymentMethodList))
						 {
							 echo  '<option value="0" selected>No Payment Method</option>';
						 }
					     ?>
						</select>
						<div class="help-block with-errors"></div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
						<label for="paymentmethod:">Method of communication</label>
						 <select class="form-control" name="Methodofcommunication_name" id="Methodofcommunication_name">
						 <?php
						 $MethodofcommunicationSelect = $CommsMethodList;
						
						 foreach($CommsMethodList  as $key => $rowData)
						 {
							 if($rowData->idcomms_method == $MethodofcommunicationSelect)
							 {echo  '<option value="'.$rowData->idcomms_method.'" selected>'.$rowData->comms_method_name.'</option>';}
							 
							 else
							 {
								echo  '<option value="'.$rowData->idcomms_method.'">'.$rowData->comms_method_name.'</option>';
								//echo '<option value="'.$rowData->Method_of_contribution_id.'"selected>'.$rowData->Method_of_contribution_id.'">'.$rowData->Method_of_contribution_name.'</option>';
							 }

						 }
						 if(empty($CommsMethodList))
						 {
							 echo  '<option value="0" selected>No Payment Method</option>';
						 }
					     ?>
						</select>
						<div class="help-block with-errors"></div>
				</div>
			</div>
		




			</div>
	</div>
</div>
</br>
</br>

<!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Save</button>-->
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>
</div>
