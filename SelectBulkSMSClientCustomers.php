<?php
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$group    = '';
if(isset($_POST['group']))
{
	$group    = $_POST['group'];
}
$subgroup    = $_POST['ext'];
$IntUserCode = $_POST['IntUserCode'];

$ext = '';

$html = "<SELECT class='form-control' id='customer' multiple name='customer[]' size='1'>";
$dataCustomers = null;
if($subgroup != '0')
{	
	if(empty($subgroup))
	{
		if(empty($group))
	    {
		// -- Select item. GR1
		$sql = "SELECT * FROM customer WHERE bulksmsclientid = ? AND subgroupext NOT IN ('')";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));
		$dataCustomers1 = $q->fetchAll();		
		
		// -- Select item. GR2
		$sql = "SELECT * FROM customer WHERE bulksmsclientid = ? AND subgroupext2 NOT IN ('')";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));
		$dataCustomers = $q->fetchAll();
		$dataCustomers = array_merge($dataCustomers1,$dataCustomers);
	   }
		elseif($group == 'Group1')	
	   {
		   // -- Select item.
		$sql = "SELECT * FROM customer WHERE subgroupext NOT IN ('') AND bulksmsclientid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($IntUserCode));	
		$dataCustomers = $q->fetchAll();
	   }
       elseif($group == 'Group2')
	   {
		   // -- Select item.
			$sql = "SELECT * FROM customer WHERE subgroupext2 NOT IN ('') AND bulksmsclientid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($IntUserCode));	
			$dataCustomers = $q->fetchAll();
	   }	
	}
	else
	{
		
		// -- Select item.
		$sql = 'SELECT * FROM customer WHERE subgroupext = ? AND bulksmsclientid = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($subgroup,$IntUserCode));	
		$dataCustomers = $q->fetchAll();
		if(empty($dataCustomers))
		{
			// -- Select item.
			$sql = 'SELECT * FROM customer WHERE subgroupext2 = ? AND bulksmsclientid = ?';
			$q = $pdo->prepare($sql);
			$q->execute(array($subgroup,$IntUserCode));	
			$dataCustomers = $q->fetchAll();
		}   
	}
  }	
// -- Add the All Payment Schedule Items.
	$html = print_r($dataCustomers)."<OPTION value='' selected>All</OPTION>";
	
// -- dataSubGroups
	foreach($dataCustomers as $row)
	{
		// -- Branches of a selected Bank
		$refnumber = $row['Title'].' '.$row['FirstName'].' '.$row['LastName'];
		$phone     = $row['phone2'];
		$CustomerId     = $row['CustomerId'];
													
		// -- Select the Selected Bank ($CustomerId)
		$html = $html."<OPTION value=$phone>$refnumber - $phone</OPTION>";
	}
										
		if(empty($dataCustomers))
		{
			$html = $html."<OPTION value=0>No Customers</OPTION>";
		}
	$html=$html."</SELECT>";
	
 echo $html;		

?>