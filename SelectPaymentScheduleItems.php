<?php
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$applicationid = $_POST['applicationid'];
$scheduledate = '';

$html = "<SELECT class='form-control' id='item' multiple name='item[]' size='1'>";

// -- Select item.
$sql = 'SELECT * FROM paymentschedule WHERE applicationid = ?';
$q = $pdo->prepare($sql);
$q->execute(array($applicationid));	
$dataPaymentSchedules = $q->fetchAll();		

// -- Add the All Payment Schedule Items.
	$html = "<OPTION value='' selected>All</OPTION>";
	
// -- Payment Schedules
	foreach($dataPaymentSchedules as $row)
	{
		// -- Branches of a selected Bank
		$scheduledate = $row['scheduleddate'];
		$item = $row['item'];
													
		// -- Select the Selected Bank 
		$html = $html."<OPTION value=$item>$item - $scheduledate</OPTION>";
	}
										
		if(empty($dataPaymentSchedules))
		{
			$html = $html."<OPTION value=0>No Payment Schedule</OPTION>";
		}
	$html=$html."</SELECT>";
	
 echo $html;		
?>