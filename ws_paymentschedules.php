<?php
/* require the user as the parameter 
PHP ODATA service Reference:
https://davidwalsh.name/web-service-php-mysql-xml-json

GET ALL the PaymentSchedules
04.11.2017
Parameter : status =
APPR - which means Approval Loans
SET  - Which means Settled Loans
CAN  - which means Cancelled Loans
REJ  - Which means Rejected Loans

format = 
xml  - Which means xml format.
json - Which means json format. 
*/

// -- Total Income
$tot_outstanding = 0.00;
$tot_paid = 0.00;
$tot_repayment = 0.00;
require 'database.php';

$found 	= false;
$UseApplicationID = false;
$xml 	= "";
$status = "";
$ApplicationId = null;
$format = "";

// -- $RemainingTerm
if ( !empty($_GET['status'])) 
{
		$status = $_GET['status'];
		$found = true;
}

// -- Query with ApplicationId.
if ( !empty($_GET['ApplicationId'])) 
{
		$ApplicationId = $_GET['ApplicationId'];
		$UseApplicationID = true;
}

/* soak in the passed variable or set our own */			
if(!empty($_GET['format']))		
{
	$format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; //xml is the default			
}
else
{
	$format = 'xml';
}

try
 {
	// -- BOC 31.10.2017 --------- //
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
		// -- Get Payment Invoices from common Tab Data.
		if($UseApplicationID)
		 {
			 $sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId where A.ApplicationId = ?";
			 $q = $pdo->prepare($sql);
			 $q->execute(array($ApplicationId));
		 }
		 else if($found)
		 {			 
			$sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId Where ExecApproval = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($status));
		 }	
		else
		{
			$sql =  "SELECT * FROM paymentschedule as A join loanapp as B on A.ApplicationId = B.ApplicationId";
			$q = $pdo->prepare($sql);
			$q->execute();
		}	
			   $dataPaymentSchedules = $q->fetchAll(PDO::FETCH_ASSOC); 
			   $xml = '<paymentschedules>';

			   foreach($dataPaymentSchedules as $rowPaymentSchedule)
			   {			
				// -- eloan Database --------- //
				 $pdo = Database::connect();
				 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		 
				// For each payment schedule related to the loan application.
					  $xml = $xml.'<paymentschedule>';
					  $xml = $xml.'<id>'.$rowPaymentSchedule['CustomerId'].'</id>';
					  $xml = $xml.'<applicationId>'.$rowPaymentSchedule['ApplicationId'].'</applicationId>';
					  $xml = $xml.'<item>'.$rowPaymentSchedule['item']. '</item>';
					  $xml = $xml.'<scheduleddate>'.$rowPaymentSchedule['scheduleddate']. '</scheduleddate>';								
					  $xml = $xml.'<status>'.$rowPaymentSchedule['ExecApproval']. '</status>';								
					  $xml = $xml. '<monthlypayment>'.$rowPaymentSchedule['monthlypayment']. '</monthlypayment>';
					  
					  // -- Get the charges
						$sql =  "SELECT * FROM loanappcharges as A join charge as B on A.chargeid = B.chargeid WHERE applicationId = ? AND item = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($rowPaymentSchedule['ApplicationId'],			 	$rowPaymentSchedule['item']));
						$datacharges = null;
						$datacharges = $q->fetchAll(PDO::FETCH_ASSOC); 
						if(!empty($datacharges))
						{
						  $xml = $xml.'<charges>';
						  foreach($datacharges as $rowCharge)
						  {
							$xml = $xml.'<charge>';	  
							$xml = $xml.'<chargeid>'.$rowCharge['chargeid'].'</chargeid>';
							$xml = $xml.'<chargename>'.$rowCharge['chargename'].'</chargename>';
							$xml = $xml.'<chargeamount>'.$rowCharge['amount'].'</chargeamount>';
							$xml = $xml.'</charge>';	
						  }
						  $xml = $xml.'</charges>';	
					    }
						
		// -- eloan Disconnect Database --------- //			   
		  Database::disconnect();	
		  
		// -- Paycollection Database --------- //
		 $pdo = Database::connectDB();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);					
		// -- Get the Payments
						$sql =  "SELECT * FROM collection 
						WHERE mandateid = ? AND mandateitem = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($rowPaymentSchedule['ApplicationId'],			 	$rowPaymentSchedule['item']));
						$datapays = null;
						$datapays = $q->fetchAll(PDO::FETCH_ASSOC); 
						if(!empty($datapays))
						{
						  $xml = $xml.'<payments>';
						  foreach($datapays as $rowPay)
						  {
							$xml = $xml.'<payment>';	  
							$xml = $xml.'<Action_Date>'.$rowPay['Action_Date'].'</Action_Date>';
							$xml = $xml.'<Amount>'.$rowPay['Amount'].'</Amount>';
							$xml = $xml.'<Status_Description>'.$rowPay['Status_Description'].'</Status_Description>';
							$xml = $xml.'</payment>';	
						  }
						  $xml = $xml.'</payments>';	
					    }						
					  $xml = $xml. '</paymentschedule>';
			   }
			   
			   // -- Default : xml & json values.
			   if($dataPaymentSchedules == null)
			   {
					  $xml = $xml.'<paymentschedule>';
					  $xml = $xml.'<applicationId>0</applicationId>';
					  $xml = $xml.'<item>0</item>';
					  $xml = $xml.'<scheduleddate>0000-00-00</scheduleddate>';								
					  $xml = $xml.'<status></status>';								
					  $xml = $xml. '<monthlypayment>0.00</monthlypayment>';
					  $xml = $xml. '</paymentschedule>';
			   }
		// -- Paycollection Disconnect Database --------- //			   
		  Database::disconnect(); 
				$xml = $xml. '</paymentschedules>';
				
			// -- Determine the format.
			// -- Display JSON format.
			if($format == 'json')
			{
			   header('Content-type: application/json');	
			   $xmlString = simplexml_load_string($xml);
			   $json = json_encode($xmlString);
			    echo $json;
			   //$array = json_decode($json,TRUE);
			}
			// -- Display XML format. 
			// -- Default.
			else
			{
				header('Content-type: text/xml');
			    echo $xml;
			}	
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //				 
					   Database::disconnect();	
			}
			catch(Exception $e) 
			{
			  echo 'Message: ' .$e->getMessage();
			}
	
?>
