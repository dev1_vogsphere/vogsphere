<?php
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';

if(!function_exists('delete_beneficiary'))
{
	function delete_beneficiary($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		////////////////////////////////////////////////////////////////////////

		$message = '';
		try
		{
			$sql = "DELETE FROM benefiary	
			WHERE benefiaryid = ?;";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['benefiaryid']));
			Database::disconnect();
		}
	  catch (Exception $e)
	  {
		$message = 'Caught exception: '.  $e->getMessage(). "\n";
	  }
	  return $message;
   }
}
////////////////////////////////////////////////////////////////////////
$list = getpost('list');

////////////////////////////////////////////////////////////////////////
$message = delete_beneficiary($list);
if(empty($message))
{
	 $message = 'removed successfully.';
}
echo $message;
?>