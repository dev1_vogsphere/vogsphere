<?php
require 'database.php';

if (!function_exists('updatecollections2'))
{
	function updatecollections2($id,$arrayCollections)
	{
	 try
	  {
		//Global $pdo;
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "UPDATE mandates
		SET
		tracking_indicator					= ?,  	
		Frequency                  			= ?,
		mandate_initiation_date	  			= ?,
		first_collection_date      			= ?,
		collection_amount					= ?,
		maximum_collection_amount  			= ?,
		debtor_account_name 	      		= ?,
		debtor_identification 	  			= ?,
		document_type	          			= ?,
		debtor_account_number  	  			= ?,
		debtor_account_type	      			= ?,
		debtor_branch_number  	  			= ?,
		debtor_contact_number	  			= ?,
		debtor_email               			= ?,
		collection_date            			= ?,
		date_adjustment_rule_indicator		= ?,
		adjustment_category        			= ?,
		adjustment_rate            			= ?,
		adjustment_amount          			= ?,
		first_collection_amount 			 = ?,
		debit_value_type           			= ?,
		amendment_reason_md16 = ?,
		amendment_reason_text = ?,
		collection_date_text  = ?,
		changedby = ?,
		changedon = ?,
		changedat = ?
		WHERE reference = ?";
		$q = $pdo->prepare($sql);
		$arrayCollections['collection_date_text'] = $arrayCollections['collection_date'];
		
		  if(!empty($arrayCollections['collection_date']) && ($arrayCollections['Frequency'] != 'WEEK' and $arrayCollections['Frequency'] != 'ADHO'))
			{
				$col_date=date_create($arrayCollections['collection_date']);
					//	echo $col_date;

				$col_date = DateTime::createFromFormat("Y-m-d", $arrayCollections['collection_date']);
				$db_collection_day = $col_date->format("d");
				$arrayCollections['collection_date'] = $db_collection_day;
			}
		//print_r($arrayCollections );	
		$arrayCollections['changedon'] = date('Y-m-d');
		$arrayCollections['changedat'] = date('h:m:s');;

		$q->execute(array(
		$arrayCollections['tracking_indicator'],				
		$arrayCollections['Frequency'],                  		
		$arrayCollections['mandate_initiation_date'],	  		
		$arrayCollections['first_collection_date'], 
		$arrayCollections['collection_amount'], 	
		$arrayCollections['maximum_collection_amount'],  		
		$arrayCollections['debtor_account_name'], 	      	
		$arrayCollections['debtor_identification'], 	  		
		$arrayCollections['document_type'],	          		
		$arrayCollections['debtor_account_number'],  	  		
		$arrayCollections['debtor_account_type'],	      		
		$arrayCollections['debtor_branch_number'],  	  		
		$arrayCollections['debtor_contact_number'],	  		
		$arrayCollections['debtor_email'],               		
		$arrayCollections['collection_date'],            		
		$arrayCollections['date_adjustment_rule_indicator'],	
		$arrayCollections['adjustment_category'],        		
		$arrayCollections['adjustment_rate'],            		
		$arrayCollections['adjustment_amount'],          		
		$arrayCollections['first_collection_amount'], 		
		$arrayCollections['debit_value_type'],    
		$arrayCollections['amendment_reason_md16'],           		
		$arrayCollections['amendment_reason_text'], 
		$arrayCollections['collection_date_text'],	
		$arrayCollections['changedby'],
		$arrayCollections['changedon'],
		$arrayCollections['changedat'],
		$id));
		
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

$reference 			 = getpost('id_'); 		
$list		         = getpost('list');			

// -- Update Collection.
$message = '';

// -- Function.
$message = updatecollections2($reference,$list);
if(empty($message))
{
	 $message = 'Reference : '.$reference.' was updated successfully.';
}

echo $message;
//print_r($list );	
?>