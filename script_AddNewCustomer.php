<?php // -- BOC Add Customer, 2018.01.03.
require 'database.php';
/**
    * @param string $filePath
    * @param int $checkLines
    * @return string
    */
if(!function_exists('getCsvDelimiter'))	
{	
   function getCsvDelimiter($filePath)
   {
	  $checkLines = 1;
      $delimiters =[',', ';', '\t'];

      $default =',';

       $fileObject = new \SplFileObject($filePath);
       $results = [];
       $counter = 0;
       while ($fileObject->valid() && $counter <= $checkLines) {
           $line = $fileObject->fgets();
           foreach ($delimiters as $delimiter) {
               $fields = explode($delimiter, $line);
               $totalFields = count($fields);
               if ($totalFields > 1) {
                   if (!empty($results[$delimiter])) {
                       $results[$delimiter] += $totalFields;
                   } else {
                       $results[$delimiter] = $totalFields;
                   }
               }
           }
           $counter++;
       }
       if (!empty($results)) {
           $results = array_keys($results, max($results));

           return $results[0];
       }
return $default;
}
}
if(!function_exists('db_insert_subgroup'))
{
	function db_insert_subgroup($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// -- subgroup
		 $sqlQuestionMarks[] = '(?,?,?,?,?,?,?)';
			try
			{
				$sql = "INSERT INTO subgroup
						(
						ext,
						popularname,
						groupname,
						softsort,
						parentgroup,
						UserCode,
						IntUserCode
						)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();		
			}					
			catch (PDOException $e)
			{
			}
			return $id;
	}
}

$valid_formats = array("csv");
$max_file_size = 1024*10000; //10MB
$path = "bulksmscontacts/"; // Upload directory
$count = 0;
$bulksmsclientid = '';
$UserCode		 = '';

if(isset($_POST['bulksmsclientid'])){ $bulksmsclientid = $_POST['bulksmsclientid'];}
if(isset($_POST['UserCode'])){ $UserCode = $_POST['UserCode'];}

	$filecount = count($_FILES['files']['name']);
	if($filecount == 1)
		{
		foreach ($_FILES['files']['name'] as $f => $name) 
		{     
			//print_r($_FILES['files']['error']);

			if ($_FILES['files']['error'][$f] == 4) {
				continue; // Skip file if any error found
			}	       
			if ($_FILES['files']['error'][$f] == 0) 
			{	           
				if ($_FILES['files']['size'][$f] > $max_file_size) {
					$message[] = "$name is too large!.";
					continue; // Skip large files
				}
				elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
					$message[] = "$name is not a valid format";
					continue; // Skip invalid file formats
				}
				else{ // No error found! Move uploaded files 
							// Filename add datetime - extension.
				$name = date('d-m-Y-H-i-s').$name;
				
				$ext = pathinfo($name, PATHINFO_EXTENSION);

				// -- Encrypt The filename.
				$name = md5($name).'.'.$ext;
				
				/*if($_POST['documents'] = $leads)
				{
				 $FILELEADS = $name;
				}	*/		
			// -- Update File Name Directory	

					if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) 
					{
						$count++; // Number of successfully uploaded files
					}

					// -- BOC Encrypt the uploaded PDF with ID as password.
						$file_parts = pathinfo($name);
						$filenameEncryp = $path.$name;
						switch($file_parts['extension'])
						{
							case "pdf":
							{
								break;
							}
							case "PDF":
							{
								break;
							}
						}
					// -- EOC Encrypt the uploaded PDF file with ID as password.
				}
			}
			else
			{
				$message[] = codeToMessage($filecount);
				//print_r($message);			
			}
		}
	}
	else
	{
		$message[] = "Only one file can be uploaded at a time.";
		//unset $_FILES['files'];
		//print_r($_FILES['files']);
	}	
	process_file($filenameEncryp,$bulksmsclientid,$UserCode);
	
	function process_file($filenameEncryp,$bulksmsclientid,$UserCode)
	{
			$dbname 	  = "ecashpdq_eloan";

		if(!empty($filenameEncryp))
				{ 
				echo "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;'>
					  <tr>
					  <td>Feedback</td>					  
					  <td>CustomerId</td>					  
					  <td>Title</td>					  
					  <td>firstname</td>
					  <td>surname</td>	
					  <td>province</td>
					  <td>Phone2</td>
					  <td>Phone</td>					  
					  <td>createdon</td>  
					  <td>status</td>
					  <td>occupation</td>
					  <td>group1</td>
					  <td>bulksmsclient</td>					  
					  <td>group2</td>
					  </tr>";					  
			
					// ----- call ------ 
					$csvdata = null;
					$count = 0;
					$x=0;
					$z = 0;
					$message = '';
					$separetor = getCsvDelimiter($filenameEncryp);//";";
					$csvdata = csv_in_array( $filenameEncryp,$separetor, "\"", true ); 
					$count = count($csvdata); 
					$blank = "''";
					$singleQuote = "";
					
					// -- Group 1 -- Array Fetch.// -- Group 2 -- Array Fetch.
					// -- Get the search dynamic.
							    $arrayGr1and2 = null;
								$tablename   = 'subgroup';
								$dbnameobj   = $dbname;
								$queryFields = null;
								$whereArray  = null;
								$whereArray[]  = "IntUserCode = '{$bulksmsclientid}'";								
								$queryFields[] = '*';								  
								
								$subgroupObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
								$countGr1and2 = 0;
								foreach($subgroupObj as $row)
								{
									$arrayGr1and2[$countGr1and2][1] = $singleQuote.$row['ext'].$singleQuote;
									$arrayGr1and2[$countGr1and2][2] = $singleQuote.$row['groupname'].$singleQuote;									
									$countGr1and2 = $countGr1and2 + 1;
								}
					$subgroupFound = 0;
					for($x=0; $x<$count; $x++) 
						{ 
						    $subgroupFound = 0;
							$keydataJson = array();							
							$keydata = $csvdata[$x];
							// -- UTF-8
							$keydata = json_encode($keydata,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
							if(empty($keydata))
							{
								// -- ANSI
								$keydata = $csvdata[$x];
							}
							else
							{
								$keydataJson = json_decode($keydata,true);
							}
							$index = 0;
							if(!empty($keydataJson))
							{
								//print_r($keydataJson);
								$keydata = array();
								foreach($keydataJson as $item => $v) 
								{ 
								//print_r($v);
								//print('<br/>');							
								// -- Trim trailing & leading spaces.
									$v = trim($v);
									if($index == 0){$keydata['id'] = $v;}
									if($index == 1){$keydata['raw_cellnr'] = $v;}
									if($index == 2){$keydata['27_format'] = $v;}
									if($index == 3){$keydata['0_format'] = $v;}
									
									if($index == 4){$keydata['title'] = $v;}

									if($index == 5){$keydata['firstname'] = $v;}
									if($index == 6){$keydata['surname'] = $v;}
									if($index == 7){$keydata['designation'] = $v;}
									if($index == 8){$keydata['group1'] = $v;}
									
									if($index == 9){$keydata['group2'] = $v;}
									if($index == 10){$keydata['dateadded'] = $v;}
									$index = $index  + 1;
								}
							}
							//print_r($keydata);
							if(empty($keydata)){continue;}
							if(isset($keydata['id'])){}else{$keydata['id'] = '';}
							if(isset($keydata['title'])){}else{$keydata['title'] = '';}
							if(isset($keydata['firstname'])){}else{$keydata['firstname'] = '';}
							if(isset($keydata['surname'])){}else{$keydata['surname'] = '';}
							if(isset($keydata['0_format'])){}else{$keydata['0_format'] = '';}
							if(isset($keydata['27_format'])){}else{$keydata['27_format'] = '';}
							if(isset($keydata['dateadded'])){}else{$keydata['dateadded'] = '';}
							if(isset($keydata['status'])){}else{$keydata['status'] = '';}
							if(isset($keydata['designation'])){}else{$keydata['designation'] = '';}
							if(isset($keydata['group1'])){}else{$keydata['group1'] = '';}
							if(isset($keydata['group2'])){}else{$keydata['group2'] = '';}
							
							$array[1] = $singleQuote.$keydata['id'].$singleQuote;	
							if($keydata['title'] == '' or $keydata['title'] == 'NULL'){$array[2] = 'Mx';}else{$array[2] = $singleQuote.$keydata['title'].$singleQuote;}	
							if($keydata['firstname'] == '' or $keydata['firstname'] == 'NULL'){$array[3] = 'no firstname';}else{$array[3] = $singleQuote.$keydata['firstname'].$singleQuote;}							
							if($keydata['surname'] == '' or $keydata['surname'] == 'NULL'){$array[4] = 'no surname';}else{$array[4] = $singleQuote.$keydata['surname'].$singleQuote;}							
							$array[5] = $singleQuote.'MP'.$singleQuote;
							$array[7] = $singleQuote.$keydata['0_format'].$singleQuote;
							$array[6] = $singleQuote.$keydata['27_format'].$singleQuote;
							if($keydata['dateadded'] == '' or $keydata['dateadded'] == 'NULL'){$array[8] = '';}else{$array[8] = $singleQuote.convertDatetoMYSQL($keydata['dateadded']).$singleQuote;}							
							if($keydata['status'] == '' or $keydata['status'] == 'NULL'){$array[9] = '';}else{$array[9] = $singleQuote.$keydata['status'].$singleQuote;}							
							if($keydata['designation'] == '' or $keydata['designation'] == 'NULL'){$array[10] = 'no designation';}else{$array[10] = $singleQuote.$keydata['designation'].$singleQuote;}							
							$array[11] = $singleQuote.$keydata['group1'].$singleQuote;
							if($keydata['group1'] == '' or $keydata['group1'] == 'NULL'){$array[11] = '';}
							else
							  {
								$IndexStart = 0;
								$grp1 = $keydata['group1'];
								$array[11] = $grp1;
								for($j=$IndexStart;$IndexStart<$countGr1and2;$IndexStart++)
								{
										$grpSelected = $arrayGr1and2[$IndexStart][2];
										if($grpSelected == $grp1){$array[11] = $arrayGr1and2[$IndexStart][1]; $subgroupFound = 1; break;}
								}
								if(empty($subgroupFound))
								{
								  // -- if not found, then insert a group.
								  $newContact = array();
								  $newContact[] = $grp1;
								  $newContact[] = $grp1;
								  $newContact[] = $grp1;
								  $newContact[] = '0';
								  $newContact[] = 'Group1';
								  $newContact[] = $UserCode;
								  $newContact[] = $bulksmsclientid;
								  db_insert_subgroup($newContact);
								}
								$subgroupFound = 0;
						      }
							$array[12] = $singleQuote.$bulksmsclientid.$singleQuote;								  
							if($keydata['group2'] == '' or $keydata['group2'] == 'NULL'){$array[13] = $singleQuote.''.$singleQuote;}
							else
							  {
								$IndexStart = 0;
								$grp2 = $keydata['group2'];
								$array[13] = $grp2;
								for($j=$IndexStart;$IndexStart<$countGr1and2;$IndexStart++)
								{
										$grpSelected = $arrayGr1and2[$IndexStart][2];
										if($grpSelected == $grp2){$array[13] = $arrayGr1and2[$IndexStart][1]; $subgroupFound = 1; break;}
								}
								if(empty($subgroupFound))
								{
								  // -- if not found, then insert a group.
								  $newContact = array();
								  $newContact[] = $grp2;
								  $newContact[] = $grp2;
								  $newContact[] = $grp2;
								  $newContact[] = '0';
								  $newContact[] = 'Group2';
								  $newContact[] = $UserCode;
								  $newContact[] = $bulksmsclientid;
								  db_insert_subgroup($newContact);
								}	
								$subgroupFound = 0;								
						      }	
							  $array[14] = '';//-Alias

							$message = '';
							$id = $array[1];
							if(empty(CheckCustomerIDExist($id)))
							{
								$message = AddNewCustomer($array); 
							}
							else
							{
								$message = 'CustomerId already exist.';
							}
							if(empty($message)){$message = 'success';}
							echo"<tr>".
								 "<td>".$message."</td>".							
								 "<td>".$array[1]."</td>".
								 "<td>".$array[2]."</td>".								 
								 "<td>".$array[3]."</td>".
							     "<td>".$array[4]."</td>".
								 "<td>".$array[5]."</td>".
								 "<td>".$array[7]."</td>".
								 "<td>".$array[6]."</td>".
								 "<td>".$array[8]."</td>".
								 "<td>".$array[9]."</td>".
								 "<td>".$array[10]."</td>".	
								 "<td>".$array[11]."</td>".	
								 "<td>".$array[12]."</td>".		
								 "<td>".$array[13]."</td>".		
								 "<tr>";		
						}
					echo "</table>";				
				}
			}
?>