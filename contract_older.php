<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
//------------------------------------------------------------------- 
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
 require 'database.php';
	$customerid = null;
	$ApplicationId = null;
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
	}
	
	if ( null==$customerid ) 
	    {
		header("Location: custAuthenticate.php");
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: custAuthenticate.php");
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from Customer, LoanApp where Customer.CustomerId = ? AND ApplicationId = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;

	}
$currency = 'R';
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
			// Id number
			$idnumber = $data['CustomerId'];

			// Title
			$Title = $data['Title'];	
			// First name
			$FirstName = $data['FirstName'];
			// Last Name
			$LastName = $data['LastName'];
			
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			
			// Phone numbers
			$phone = $data['phone'];
			// Email
			$email = "tumelomodise4@yahoo.com";//$data['email'];
			// Street
			$Street = $data['Street'];

			// Suburb
			$Suburb = $data['Suburb'];

			// City
			$City = $data['City'];
			// State
			$State = $data['State'];
			
			// PostCode
			$PostCode = $data['PostCode'];
			
			// Dob
			$Dob = $data['Dob'];
			
			// phone
			$phone = $data['phone'];

//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
 $loandate = $data['DateAccpt'];// "01-08-2016";
 $loanamount = $currency.''.$data['ReqLoadValue'];// "R2000";
 $loanpaymentamount = $currency.''.$data['Repayment']; //"R2400";
 $loanpaymentInterest = "20%";
 $FirstPaymentDate = $data['FirstPaymentDate'];
 $LastPaymentDate = $data['lastPaymentDate'];
 $LoanDuration = $data['LoanDuration']; // Changed .01.09.2016

//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
 $companyname = "Vogsphere Pty Ltd";
 $companyStreet = "43 Mulder Street";
 $companySuburb = "The reeds";
 $companyPostCode = "0157";
 $companyCity = "Centurion";
 $companyphone = '0793401754/0731238839';
 
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Vogsphere (Pty) Ltd');
$pdf->SetTitle('Vogsphere (Pty) Ltd');
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject('Vogsphere (Pty) Ltd - Loan Agreement');
$pdf->SetKeywords('Loan, Contract, Agreement, Money');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

/* <h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
 */
 
// Set some content to print
$html = <<<EOD

<p>This Client Loan Agreement has been drafted on the <b>$loandate</b> by and between <b>$companyname</b>, the Loaner and <b>$clientname</b> the Client.
The Client is a <b>$companyname</b> customer and has requested for a loan for personal reasons which will be granted if he/she agrees to the below mentioned rules and regulations:</p>

<ul>
<li>Vogsphere Pty Ltd will grant loan to <b>$clientname</b> for an amount of <b>$loanamount</b> at <b>$loanpaymentInterest</b> of the loan amount which he/she needs to repay in within <b>$LoanDuration month(s).</b></li>
<li>	In case the Client fails to pay the installment by due date, this will result as a breach of this agreement and legal action will be taken against the client.</li>
<li>	The client must repay the principal debt together with interest calculated in accordance with the quotation on the dates stated in the quotation.</li>
<li>	Repayment method used to be decided by <b>$companyname</b>, the Loaner and the client must make sure repayments are received on the dates on which they are due under this agreement. </li>
<li>	Client must make repayments in South African rand.</li>
</ul>
<br/>
<p>The rules and regulations have been stated in accordance to the laws of the state of South Africa. 
In witness to the agreement to the terms above, the parties or authorized agents hereby affix their signature</p>  
<p>Client: <b>$clientname</b> </p>
<p>Signed at __________________________ on this ______________________ </p>
<p>day of _____________________________ </p>
<p>Signature: ____________________________________________________ </p>
<p>Date: _________________________________ </p>
<p></p>

<p>Name of Provider: <b>$companyname</b>


<p>Signature: _____________________________ </p>


<p>Date: _________________________________ </p>


<p>Commissioner of Oaths</p> 


<p>Justice of Peace: ________________________ </p>


<p>Date: _______________________________ </p>


<p>Witness: _____________________________ </p>


<p>Name: _______________________________ </p>


<p>Date: ________________________________ </p>

<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
}
</style>
<p align="center"><b>LOAN - AGREEMENT STATEMENT</b></p>
<table>
   <tr><th colspan="2" bgcolor="Black" color="White">COMPANY PHYSICAL ADDRESS</th></tr>
   <tr><td>Company Name:</td><td><b>$companyname</b></td></tr>
   <tr><td>Physical address: </td><td><b>$companyStreet</b></td></tr>
   <tr><td></td><td><b>$companySuburb</b></td></tr>
   <tr><td></td><td><b>$companyCity</b></td></tr>
   <tr><td></td><td><b>$companyPostCode</b></td></tr>
   <tr><td></td><td>Contact Number: <b>$companyphone</b></td></tr>
</table>
<table>
   <tr><th colspan="2" bgcolor="Black" color="White">CLIENT PHYSICAL ADDRESS</th></tr>
   <tr><td>Company Client:</td><td><b>$clientname</b></td></tr>
   <tr><td>Physical address:</td><td><b>$Street</b></td></tr>
   <tr><td></td><td><b>$Suburb</b></td></tr>
   <tr><td></td><td><b>$City</b></td></tr>
   <tr><td></td><td><b>$PostCode</b></td></tr>
   <tr><td></td><td>Contact Number: <b>$phone</b></td></tr>
   <tr><td>Company Client ID: </td><td><b>$idnumber</b></td></tr>
</table>
<br>
<br>
<br>
<table>
   <tr><th colspan="2" bgcolor="Black" color="White">TOTAL COST AND INTEREST</th></tr>
   <tr><td>Amount Loaned</td><td>$loanamount</td></tr>
   <tr><td>Percentage of loan amount (monthly)</td><td>$loanpaymentInterest</td></tr>
   <tr><td>Total amount repayment</td><td>$loanpaymentamount</td></tr>
   <tr><th colspan="2" bgcolor="Black" color="White">REPAYMENT ARRANGEMENTS</th><th></th></tr>
   <tr><td>Frequency of payment</td><td>Monthly</td></tr>
   <tr><td>Method of payment</td><td>EFT</td></tr>
   <tr><td>First payment date</td><td>$FirstPaymentDate</td></tr>
   <tr><td>Last payment date</td><td>$LastPaymentDate</td></tr>
</table>
<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p> <!-- Space -->
<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p> <!-- Space -->
<p></p><p></p><p></p><p></p><p></p> <!-- Space -->

<p align="center"><b>SURETY AGREEMENT</b></p>

<p>I <b>$suretyname</b> residing at ________________________________ hereby </p>
 
<p>Agrees to act as surety for the debt of <b>$name</b>, residing at </p>
<p>________________________________</p>

<p><b>Financial Qualifications</b></p> 
<p>I the Surety hold the following assets: </p>
<p>________________________________________________________________________</p> 
<p>The Client and the Surety are bound to <b>$companyname</b>, residing at </p>

<p>________________________________________________________________________</p> 

<p>(Obligee), for <b>$loanpaymentamount</b>. Client and Surety are and shall remain jointly and severally liable for this amount.</p> 

<p><b>Acknowledgment of Indebtedness </b></p> 
<p>This indebtedness arises from a written contract entered into by the Client and Obligee that was executed on <b>$loandate</b>, a copy of which is attached and incorporated by reference.</p> 

<p><b>Liability of Surety  </b></p> 
<p>Surety's liability shall terminate upon Surety's written notice of such termination to Obligee. Surety, in such case, shall remain liable to Obligee for any obligation that may have arisen prior to the receipt of such notice by Obligee. </p>

<p><b>Scope of the Loan</b></p>  
<p>This Surety Loan is solely for the debt noted in the underlying contract dated <b>$loandate</b> for the amount of <b>$loanpaymentamount.</b></p>

<p><b>Notice</b></p>  
<p>Obligee must first give notice of demand for payment to the Client. Upon Client’s failure to pay, Obligee may make demand upon Surety. </p>

<p><b>Actions on the Loan </b></p>  
<p>Any action or proceeding in connection with this Loan shall be in the state of South Africa. Any costs or solicitor fees in connection with such action shall be borne by the Client.</p>

<p>The Loan shall be binding upon Surety, Surety's successors, assigns, and legal representatives. 
IN WITNESS WHEREOF, The Client and Surety have executed this Loan at ______________ on this ___ day of _________________________________</p>

<p>Client: <b>$name</b></p>

<p>Signature: _____________________________ </p>

<p>Date: _________________________________ </p>


<p>Surety: <b>$name</b></p>


<p>Signature: _____________________________ </p>


<p>Date: _________________________________ </p>


<p>Justice of Peace: ________________________ </p>


<p>Date: _________________________________</p>

EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('contract_agreement.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
