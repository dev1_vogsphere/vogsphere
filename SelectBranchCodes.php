<?php
require 'database.php';
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$bankid = $_POST['bankid'];
$branchcode = $_POST['branchcode'];

$html = "<SELECT class='form-control' id='BranchCode' name='BranchCode' size='1'>
";

// -- Select Branchcode.
$sql = 'SELECT * FROM bankbranch WHERE bankid = ?';
$q = $pdo->prepare($sql);
$q->execute(array($bankid));	
$dataBankBranches = $q->fetchAll();		

// --
	$BranchSelect = $branchcode;
	$branchdesc = '';
	foreach($dataBankBranches as $row)
	{
		// -- Branches of a selected Bank
		$branchdesc = $row['branchdesc'];
		$branchcode = $row['branchcode'];
													
		// -- Select the Selected Bank 
			if($BranchSelect == $branchcode)
			{
				$html = $html. "<OPTION value=$branchcode selected>$branchcode - $branchdesc</OPTION>";
			}
			else
			{
				$html = $html."<OPTION value=$branchcode>$branchcode - $branchdesc</OPTION>";
			}
	}
										
		if(empty($dataBankBranches))
		{
			$html = $html."<OPTION value=0>No Bank Branches</OPTION>";
		}
	$html=$html."</SELECT>";
	
 echo $html;		
?>