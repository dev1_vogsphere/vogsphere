<?php
//-------------------------------------------------------------------
//						Contract 
//------------------------------------------------------------------- 
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
require 'database.php';
$customerid = null;
$ApplicationId = null;

$tbl_customer = "customer";
$tbl_loanapp = "loanapp";
$dbname = "eloan";

// -- 07.05.2018 -- //
// --------- Add New Charges --------------	///
$initiationfee 	  	  = 0.00;
$VATinitiationfee	  = 0.00;
$Totinitiationfee 	  = 0.00;
$monthlyfee 		  = 0.00;
$insurancefee 		  = 0.00;
$vat 				  = 0.00;

// -- 07.05.2018 -- //
// ---------------------------------------- ///	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$customerid = base64_decode(urldecode($customerid)); 
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Encrypt 16.09.2017 - Parameter Data.
		$ApplicationId = base64_decode(urldecode($ApplicationId));
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
	}
	
	if ( null==$customerid ) 
	    {
		header("Location: custAuthenticate.php");
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: custAuthenticate.php");
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from $tbl_customer, $tbl_loanapp where $tbl_customer.CustomerId = ? AND ApplicationId = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
	
		
		 $_SESSION['ApplicationId'] = $ApplicationId;
		 $_SESSION['customerid'] = $customerid;
		
		 $sql = 'SELECT * FROM paymentfrequency WHERE paymentfrequencyid = ?';
		 $q = $pdo->prepare($sql);	
		 $q->execute(array($data['paymentfrequency']));
		 $dataPaymentFrequency = $q->fetch(PDO::FETCH_ASSOC);						   						 
		 $data['paymentfrequency'] = $dataPaymentFrequency['paymentfrequencydesc'];
	Database::disconnect();		
}
//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
// ----- 10.02.2017	
$ncr = "";
$emailComp = "";

	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	$q->execute();
	$dataComp = $q->fetch(PDO::FETCH_ASSOC);
	
	// -- Update Address & Company details.
	if($dataComp['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $dataComp['name'];
		$phone    = $dataComp['phone'];
		$fax      = $dataComp['fax'];
		$email    = $dataComp['email'];
		$emailComp = $dataComp['email'];
		$website  = $dataComp['website']; 
		$logo     = $dataComp['logo'];
		$logoTemp = $logo;
		$currency = $dataComp['currency']; 
		$legalterms = $dataComp['legaltext'];

		$street = $dataComp['street'];
		$suburb = $dataComp['suburb'];
		$city = $dataComp['city'];
		$State = $dataComp['province'];
		$PostCode = $dataComp['postcode'];	
		$registrationnumber = $dataComp['registrationnumber'];
		
		$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $phone.'/'.$fax;
		$ncr =  $dataComp['ncr'];
		$vat = $dataComp['vat'];
	} // ----- 10.02.2017
//////////////// New Functiions 27/11/2022///////////////////////////////////////////

// -----
	/* 09.05.2018 - Commented out Old Fee Calculations.
// -- Monthly Service - Fee 
    $chargeid = 3;
	$sql = "SELECT * FROM charge WHERE chargeid = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($chargeid));	
	$datamonthlyfee = $q->fetch(PDO::FETCH_ASSOC);
	$monthlyfee = $datamonthlyfee['amount'];
	
// -- Monthly Insurance Fee
    $chargeid = 4;
	$sql = "SELECT * FROM charge WHERE chargeid = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($chargeid));	
	$datainsurancefee = $q->fetch(PDO::FETCH_ASSOC);
	$insurancefee = $datainsurancefee['amount'];
	*/
	
	// -- Read e-mail address
	// -- User - UserID & E-mail Duplicate Validation
	$query = "SELECT * FROM user WHERE userid = ?";
	$q = $pdo->prepare($query);
	$q->execute(array($customerid));
	$dataUser = $q->fetch(PDO::FETCH_ASSOC);

// -- Payment Method.
	$sql = "SELECT * FROM paymentmethod WHERE paymentmethodid = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array( $data['paymentmethod']));	
	$dataPaymentMethod = $q->fetch(PDO::FETCH_ASSOC);
	$paymentMethodDesc = $dataPaymentMethod['paymentmethoddesc'];
	
	Database::disconnect();
//-------------------------------------------------------------------// 
//           				CUSTOMER DATA							-//
//-------------------------------------------------------------------//
			// Id number
			$idnumber = $data['CustomerId'];

			// Title
			$Title = $data['Title'];	
			// First name
			$FirstName = $data['FirstName'];
			// Last Name
			$LastName = $data['LastName'];
			
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			
			// Phone numbers
			$phone = $data['phone'];
			// Email
			$email = "tumelomodise4@yahoo.com";//$data['email'];
			// Street
			$Street = $data['Street'];

			// Suburb
			$Suburb = $data['Suburb'];

			// City
			$City = $data['City'];
			// State
			$State = $data['State'];
			
			// PostCode
			$PostCode = $data['PostCode'];
			
			// Dob
			$Dob = $data['Dob'];
			
			// phone
			$phone = $data['phone'];

//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
 $loandate = $data['DateAccpt'];// "01-08-2016";
 $loanamount = $currency.''.$data['ReqLoadValue'];// "R2000";
 $loanpaymentamount = $currency.''.$data['Repayment']; //"R2400";
 $loanpaymentInterest = $data['InterestRate'];// -- 30.01.2017 - Added Interest Rate.
 $loanpaymentInterest = number_format($loanpaymentInterest).'%';
 $percentage = $loanpaymentInterest;		  // -- 30.01.2017 - Added Interest Rate.
 $Instalment = $currency.''.$data['monthlypayment'];	// -- 30.01.2017 - Added Instalment.
 $FirstPaymentDate = $data['FirstPaymentDate'];
 $LastPaymentDate = $data['lastPaymentDate'];
 $FILEIDDOC = $data['FILEIDDOC'];
 $paymentmethod = $data['paymentmethod'];
 $c_DebitOrder = "DTO";
 $paymentfrequency = $data['paymentfrequency'];
 $duration = $data['LoanDuration'];
 // ------------------------------------------------------------------
 //							CONTRACT OPTIONAL DATA					 -
 // ------------------------------------------------------------------
$annexureB = "AND ANNEXURE 'B'";
$annexureC = "AND ANNEXURE 'C'";

$option2 =  "<p><b>OPTION 2 to CLAUSE 3</b><br/>
I elect to repay the loan and the interest thereof by means of debit order. I consent to the debit order on an appropriate form attached hereto as <b>annexure ‘C’</b> to this agreement.</p>";

$clause = 
"<p><b>CLAUSE TO BE USED AS AND WHEN APPLICABLE</b>
I elect _______________________with ID number_________________as my surety to this agreement. The surety and debtor will be jointly and severally liable for the principle debt and all terms and conditions of this agreement. A surety agreement is attached to this agreement and thus incorporated as part of this agreement.</p>";
	
// -- Only show Option Clause 2 to clause 3.
if($paymentmethod != $c_DebitOrder)
{
	$option2 = "<p></p><p></p><p></p><p></p><p></p><!-- Space -->";
	$annexureC = "";
}

// Logo are update on page tcpdf_autoconfig.php (Under root directory)
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once "FPDI/fpdi.php";
$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $dataComp['phone'].'/'.$fax;//'0793401754/0731238839';

$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

$pdf_title = $gtab.$companyname.' - eStatement';
define ('PDF_HEADER_STRING2', $tab.$companyStreet."\n"
							 .$tab.$companySuburb."\n"
							 .$tab.$companyPostCode."\n"
							 .$tab.$companyCity."\n"
							 .$tab.$companyphone."\n"
							 .$tab.$emailComp."\n"
							 .$tab.$registrationnumber."\n"
							 .$tab.$ncr);
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);
							 
// set document information $tab.$companyname."\n"
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($companyname);
$pdf->SetTitle($companyname);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject("$companyname - Loan Agreement");
$pdf->SetKeywords('Loan, Contract, Agreement, Money');
// -- tcpdf_autoconfig.php where pdfPDF_HEADER are defined.
// -- set default header data
// -- Logo
define ('PDF_HEADER_LOGO2',$logo);
define ('PDF_HEADER_LOGO_WIDTH2',97);

$pdf_title = $gtab.$companyname;
$pdf->SetHeaderData(PDF_HEADER_LOGO2, PDF_HEADER_LOGO_WIDTH2, $pdf_title, PDF_HEADER_STRING2, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// -- BOC Watermark -- //
		$xxx = 30; 
$yyy = 120;
$op = 100; 
$outdir = true;
$name = "5985efa9aee18";
// -- EOC Watermark -- //
// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

//$pdf->SetFont('Arial','B',50);
    

/* <h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
 */

///= 27-11-2022
// -- Dynamic LoanApplicationCharges..
$calculated_loanAppCharge = array();
$temp = array();
//////////////////////////////////////////////////////////////////////////////
// -- [1]. Initiation fee, charged upfront.
$chargename = 'Initiation Fee';
$data_charge_master = GetloanAppChargeByName($data,$chargename);
if(!empty($data_charge_master['chargeid']))
{
	$temp       = Calculate_LoanAppCharge($data_charge_master,$data,$chargename);
	//echo $chargename.'<br/>';
	if(!empty($temp))
	{$calculated_loanAppCharge = array_merge($temp,$calculated_loanAppCharge);}
}
// -- [2]. Monthly Service Fee.
$chargename = 'Monthly Service Fee';
$data_charge_master = GetloanAppChargeByName($data,$chargename);
//print_r($data);
//print_r($data_charge_master);
if(!empty($data_charge_master['chargeid']))
{
	$temp       = Calculate_LoanAppCharge($data_charge_master,$data,$chargename);
	//print('Tumelo'.$data_charge_master['chargeid']."<br/>");

	//echo $chargename.'<br/>';
	if(!empty($temp))
	{$calculated_loanAppCharge = array_merge($temp,$calculated_loanAppCharge);}
}
// -- 15/Jan/2023
// -- [3]. Insurance Fee
$chargename = 'Insurance Fee';
$data_charge_master = GetloanAppChargeByName($data,$chargename);
<<<<<<< HEAD
//print_r($data_charge_master);
if(!empty($data_charge_master))//05/06/2023 (is not mandatory to calculate)
=======
if(!empty($data_charge_master['chargeid']))//05/06/2023 (is not mandatory to calculate)
>>>>>>> 76cc6e420914a5689f35552556b2749d6164d814
{
   $temp       = Calculate_LoanAppCharge($data_charge_master,$data,$chargename);
  //echo $chargename.'<br/>';
	if(!empty($temp))
	{$calculated_loanAppCharge = array_merge($temp,$calculated_loanAppCharge);}
}
#print($temp);
//print_r($calculated_loanAppCharge);
//////////////////////////////////////////////////////////////////////////////
#print_r($calculated_loanAppCharge);
$monthlyfee 	  = 0.00;
$initiationfee    = 0.00;
$insurancefee     = 0.00;
$Monthlyfee       = 0.00;
$VATMonthlyfee    = 0.00;
$totalservicefee  = 0.00;
$creditMultiple	  = 0.00;

$monthlyfee = number_format($monthlyfee,2, '.', '');
$initiationfee = number_format($initiationfee,2, '.', '');
$insurancefee = number_format($insurancefee,2, '.', '');
$monthlyfee = number_format($monthlyfee,2, '.', '');
$VATMonthlyfee = number_format($VATMonthlyfee,2, '.', '');
$totalservicefee = number_format($totalservicefee,2, '.', '');
$creditMultiple  = number_format($creditMultiple,2, '.', '');

foreach($calculated_loanAppCharge as $row)
{
// -- 07.05.2018 -- //
		// -- BOC - Implement new Pricing -- //
		// -- Company Vat.			 
		// -- [1]. Initiation fee, charged upfront.
		if($row[1] == 'Initiation Fee')
		{
			//$chargetypeid = 5;	 // -- Fees based on fixed amount.
			//$chargeoptionid = 1; // -- initiation fee option.
			//$item = 0; // -- Ensure Initiation Fee is on item 0.
			//$initiationfee = GetFeeFixedAmount($chargetypeid,$ApplicationId,$item,$loandate,$chargeoptionid);
			$initiationfee = number_format($row[2],2, '.', '');// -- PHP number format without comma

			// ------ a). VAT on Initiation fee.
			// -- Extract 15% VAT backward (0,1304347826086957) Magic number..:-)
			$magicVATExtract = 0.1304347826086957;
			if($vat > 0){ $VATinitiationfee = ($magicVATExtract) * $initiationfee; }else{$VATinitiationfee = 0.00;}
			$VATinitiationfee = number_format($VATinitiationfee,2, '.', '');// -- PHP number format without comma

			// ------ b). Total Initiation fee.
			$Totinitiationfee =  $initiationfee; //$VATinitiationfee;// +12/12/2022
			$Totinitiationfee = number_format($Totinitiationfee,2, '.', '');// -- PHP number format without comma
			// -- for display purpose
			$initiationfee = $initiationfee - $VATinitiationfee;
			$initiationfee = number_format($initiationfee,2, '.', '');
		}
		// -- [2]. Monthly Service Fee.
		if($row[1] == 'Monthly Service Fee')
		{
		//	$chargetypeid = 5;	 // -- Fees based on fixed amount.
		//	$chargeoptionid = 2; // -- monthly service fee option.
		//	$item = 0; // -- Ensure Initiation Fee is on item 0.
		//	$monthlyfee = GetFeeFixedAmount($chargetypeid,$ApplicationId,$item,$loandate,$chargeoptionid);
			$monthlyfee = number_format($row[2],2, '.', '');// -- PHP number format without comma

			// ------ a). VAT on Service Fee.
			// -- Calculate VAT.
			// -- Extract 15% VAT backward (0,1304347826086957) Magic number..:-)
			$magicVATExtract = 0.1304347826086957;			
			if($vat > 0){ $VATMonthlyfee = ($magicVATExtract) * $monthlyfee; }else{$VATMonthlyfee = 0.00;}
			$VATMonthlyfee = number_format($VATMonthlyfee,2, '.', '');// -- PHP number format without comma

			// ------ b).Total Service Fee for period.
			//$totalservicefee = $VATMonthlyfee + $monthlyfee; //- 12/12/2022
			//$totalservicefee =  $duration * ($VATMonthlyfee + $monthlyfee); //- 12/12/2022
			$totalservicefee =  $duration * $monthlyfee; //- 12/12/2022			
			$totalservicefee = number_format($totalservicefee,2, '.', '');// -- PHP number format without comma
			// -- for display purpose
			$monthlyfee = $monthlyfee - $VATMonthlyfee;
			$monthlyfee = number_format($monthlyfee,2, '.', '');
		}
		// -- [3]. Insurance Fees
		if($row[1] == 'Insurance Fee')
		{
			//$chargetypeid = 5;	 // -- Fees based on fixed amount.
			//$chargeoptionid = 4; // -- Insurance Fee option.
			//$item = 0; // -- Ensure Initiation Fee is on item 0.
			//$insurancefee = GetFeeFixedAmount($chargetypeid,$ApplicationId,$item,$loandate,$chargeoptionid);
		//--$insurancefee 	 = $duration * $insurancefee; 
			$insurancefee = number_format($row[2],2, '.', '');// -- PHP number format without comma
		}
		// -- EOC - Implement new Pricing -- //		
// -- 07.05.2018 -- //  
}
#print("Monthlyfee:".$monthlyfee);
//17/01/2023 (Should the Total Cost of credit balance with statement of account)
// at the moment is missing with 9.13(of VAT on service fee)
// -- Total of all instalments, including interest
//$totalInstalment = $monthlyfee + $insurancefee + $data['monthlypayment']+$VATMonthlyfee;//--12/12/2022
$totalInstalment = $monthlyfee + $insurancefee + $data['monthlypayment'] + $VATMonthlyfee;
// -- 19/01/2023 -- Added $VATMonthlyfee above to balance equation Mulu to confirm
$totalInstalment = number_format($totalInstalment,2, '.', '');// -- PHP number format without comma

$totalInterestAmount = $data['Repayment'] -  $data['ReqLoadValue'];
$totalInterestAmount = number_format($totalInterestAmount,2,'.', '');// -- PHP number format without comma
$totalCostOfCredit = ($totalInstalment * $duration) + $Totinitiationfee;
// -- 30.01.2019 - Divide totalCostOfCredit / Duration to get Instalment
$totalInstalment = $totalCostOfCredit / $duration;
$totalInstalment = number_format($totalInstalment,2, '.', '');// -- PHP number format without comma

// -- 30.01.2019 - Divide totalCostOfCredit / Duration to get Instalment
$totalCostOfCredit = number_format($totalCostOfCredit,2, '.', ''); // -- PHP number format without comma
$creditMultiple = $totalCostOfCredit / $data['ReqLoadValue'];
$creditMultiple = number_format($creditMultiple,2, '.', '');// -- PHP number format without comma
$insurancefee 	 = $duration * $insurancefee; // -- Lates Insurance.
$insurancefee = number_format($insurancefee,2, '.', '');

// -- set Image.
// -- $pdf->Image('5985efa9aee18.png', 0, 0, 330, 20, '', '', '', false, 0, '', false, false, 0);

// -- Watermark
$watermark = "CONFIDENTIAL";
$watermarkImage = 'watermark2.png';

// -- watermark RGB
$waterHPos = 20;
$waterLPos = 230;
$waterSize = 230;

// -- Set some content to print
$html = '
<div style="position:absolute;z-index:100;">
<p align="center">NCR Registration Number: '.$ncr.'
<h3><b>Pre-Agreement Statement and Quotation</b></h3>
In terms of section 92 of the National Credit Act 34 of 2005
</p>

<table>
   <tr><th colspan="2"  color="Black" bgcolor="White"><b>CREDIT PROVIDER</b></th>
   </tr>
   <tr><td>Full Registered Name</td><td>'.$companyname.'</td></tr>
   <tr><td>NCR Registration No.</td><td>'.$ncr.'</td></tr>
   <tr><td>Company Registration No.</td><td>'.$registrationnumber.'</td></tr>
   <tr><td>Physical Address</td><td>'.$companyStreet.','.$companySuburb.','.$companyCity.','.$companyPostCode.'</td></tr>
   <tr><td>Contact Number</td><td>'.$companyphone.'</td></tr>
   </table>
   <br/>
   <br/>   
   <table>
   <tr><th colspan="2" color="Black" bgcolor="White"><b>CONSUMER</b></th>
   </tr>
   <tr><td>Full Name</td><td>'.$clientname.'</td></tr>
   <tr><td>Identity Number</td><td>'.$customerid.'</td></tr>
   <tr><td>Physical Address</td><td>'.$Street.','.$Suburb.','.$City.','.$State.','.$PostCode.'</td></tr>
   <tr><td>Contact Number</td><td>'.$phone.'</td></tr>
   <tr><td>Email Address</td><td>'.$dataUser['email'].'</td></tr>
   <tr><td>Account Number</td><td>'.GenerateAccountNumber($customerid)
.'</td></tr>
   </table>
<p>
<b>Date 	: </b>'.$loandate.'
</p>
<table>
   <tr><th><b>Credit advanced</b></th><th>'.$loanamount.'</th></tr>
   <tr>
     <td>Instalment, including interest, fees & credit insurance, excluding optional insurance
    </td>
	<td>'.$currency.''.$totalInstalment.'</td>
   </tr>
   <tr><td>Number of Instalments payable</td><td>'.$duration.'</td></tr>
   <tr><td>Instalments payable (monthly, weekly, other)</td><td>'.$paymentfrequency.'</td></tr>
   
    <tr><td><b>Total of all instalments, including interest, fees & credit insurance, excluding optional insurance.</b></td><td>'.$currency.''.$totalInstalment.'</td></tr>
	<tr><td></td><td></td></tr>
   <tr><td>Initiation fee, charged upfront</td><td>'.$currency .$initiationfee.'</td></tr>
   <tr><td>VAT on Initiation fee</td><td>'.$currency .$VATinitiationfee.'</td></tr>
   <tr><td><b>Total Initiation fee</b></td><td>'.$currency .$Totinitiationfee.'</td></tr>
   <tr><td></td><td></td></tr>
   <tr><td>Monthly Service Fee, included in instalment</td><td>'.$currency.' '.$monthlyfee.'</td></tr>
   <tr><td>VAT on Service Fee </td><td>'.$currency.' '.$VATMonthlyfee.'</td></tr>
   <tr><td><b>Total Service Fee for period of the loan</b></td><td>'.$currency.' '.$totalservicefee.'</td></tr>
   <tr><td></td><td></td></tr>
   <tr><td>Annual Interest Rate</td><td>'.$loanpaymentInterest.'</td></tr>
   <tr><td><b>Total Interest charges for period of loan</b></td><td>'.$currency.' '.$totalInterestAmount.'</td></tr>
</table>';
   // -- Page 1.
//$pdf->SetTextColor($waterR,$waterG,$waterB);
//$pdf->SetFont('dejavusans', 'B', 50, '', false);
//$pdf->RotatedText(20, 230, $watermark, 45);
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->setAlpha(1,"Normal",1,false);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
$pdf->AddPage();
	
//$pdf->AddPage();

$html = '<table>
<tr><td></td><td></td></tr>
   <tr><td>Monthly Credit Life insurance charge, included in instalment</td><td>'.$currency.' '.$insurancefee.'</td></tr>
   <tr><td><b>Total cost of credit</b></td><td>'.$currency.' '.$totalCostOfCredit.' </td></tr>
   <tr><td></td><td></td></tr>
   <tr><td>Credit Cost Multiple*
(*the ratio of the total cost of credit to the advanced principal debt, that is, the total cost of credit divided by the advanced principal debt expressed as a number to two decimal places)
</td><td>'.$creditMultiple.'</td></tr>
</table>

<p align="center"><b>PAYMENT ARRANGEMENTS</b>
</p>
<table>
   <tr><th>Method of loan repayment</th><th>'.$paymentMethodDesc.'</th></tr>
   <tr><td>Instalment amount</td><td>'.$currency.' '.$totalInstalment.'</td></tr>
   <tr><td>Number of instalments</td><td>'.$duration.'</td></tr>
   <tr><td>First instalment date</td><td>'.$FirstPaymentDate.'</td></tr>
   <tr><td>Last instalment date</td><td>'.$LastPaymentDate.'</td></tr>
</table>  

<p align="center"><b>ADDITIONAL INFORMATION</b>
</p>
<p>
This Pre-Agreement Statement and Quotation will be valid for a period of 5 (five) business days.
</p>
<p>
This Pre-Agreement Statement and Quotation, together with the Loan Terms, will form your Loan Agreement.
</p>
<p>
The approval of your application is at the sole discretion of '.$companyname.', applying its business models and rules in accordance with the National Credit Act 34 of 2005, as amended.
</p>
<p>If we enter into the Loan Agreement with you, we will send you monthly statements by way of email to the email address that you provided during your application.
</p> 
';

// -- Page 2
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
$pdf->AddPage();

//$pdf->AddPage();


$html = '
 <!----------- Spaces ----------------->
<p align="center"><h3><b>Loan Terms of Small Credit Agreement</b></h3>
    In terms of section 93(2) and form 20.2 of the National Credit Act 34 of 2005
</p>

	<table>
   <tr><th colspan="2" color="Black" bgcolor="White"><b>CREDIT PROVIDER</b></th>
   </tr>
   <tr><td>Full Registered Name</td><td>'.$companyname.'</td></tr>
   <tr><td>NCR Registration No.</td><td>'.$ncr.'</td></tr>
   <tr><td>Company Registration No.</td><td>'.$registrationnumber.'</td></tr>
   <tr><td>Physical Address</td><td>'.$companyStreet.','.$companySuburb.','.$companyCity.','.$companyPostCode.'</td></tr>
   <tr><td>Contact Number</td><td>'.$companyphone.'</td></tr>
   </table>
   <br/> 
   <br/>
   <table>
   <tr><th colspan="2" color="Black" bgcolor="White"><b>CONSUMER</b></th>
   </tr>
   <tr><td>Full Name</td><td>'.$clientname.'</td></tr>
   <tr><td>Identity Number</td><td>'.$customerid.'</td></tr>
   <tr><td>Physical Address</td><td>'.$Street.','.$Suburb.','.$City.','.$State.','.$PostCode.'</td></tr>
   <tr><td>Contact Number</td><td>'.$phone.'</td></tr>
   </table>
<p>
These loan terms, together with the Pre-Agreement Statement and Quotation and Debit Order Authorisation, form the Loan Agreement between you and '.$companyname.'.
</p>
<br/>
<p>
<b>You</b> refers to the applicant or person that enters into the Loan Agreement with '.$companyname.', whichever applies in the circumstances. 
<b>We</b> or <b>us</b> or <b>our</b> refers to '.$companyname.' 
<b>The Act</b> refers to the National Credit Act 34 of 2005.
</p>

<style>
ol {
  list-style-type:upper-alpha;
  position:relative;
}

.customListBox > ol { 
    counter-increment: lists;
    counter-reset: items;
    list-style-type: none;
}

.customListBox > ol > li::before {
    content: counters(lists, "", decimal) "." counters(items, "", decimal) " ";
    counter-increment: items;
    position:absolute;
    left: 0;
}</style>

<div class="customListBox">
<ol>
 <li><b>INTRODUCTION</b>
	 <ol>
	    <li>'.$companyname.' is a registered credit provider that provides unsecured short term loans.</li>
		<li>1.2.	'.$companyname.' may, in our discretion, and applying our internal business models and risk rules, together with the Act, decide to grant a loan to an applicant that qualify in terms of the Act and our internal risk rules.</li>

		<li>1.3.	All loan agreements with '.$companyname .' are being entered into by electronic means.</li>
		
	<li>1.4.	By applying for a loan, you give us the right to:
		<ol class="foo">
		<li>
		1.4.1.	perform credit checks, and affordability assessment and other relevant checks that may apply in terms of our internal business rules or legislation; 
		<li>1.4.2.	verify with third parties that the information provided by you is correct; 
		<li>1.4.3.	disclose your payment history to relevant third parties as we may be authorised to do by law; and 
		<li>1.4.4.	disclose your personal information in terms of our privacy policy.
		</li>
		</ol>
	</li>
	<li>1.5.	You may only apply for a loan with us to use the loan for personal, and not business purposes.</li>
	 </ol>
 </li>

 <ol>
  </div>

  ';
	// -- Page 3
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
$pdf->AddPage();

//$pdf->AddPage();
	
$html = '
<style>
ol {
  list-style-type:upper-alpha;
  position:relative;
}

.customListBox > ol { 
    counter-increment: lists;
    counter-reset: items;
    list-style-type: none;
}

.customListBox > ol > li::before {
    content: counters(lists, "", decimal) "." counters(items, "", decimal) " ";
    counter-increment: items;
    position:absolute;
    left: 0;
}</style>

<div class="customListBox">
<ol>
  <li><b>2. APPLYING FOR A LOAN AND ACCEPTING THE TERMS OF THE LOAN AGREEMENT</b>
		<li>2.1.	You can apply for a loan with us, by completing the application form available from our website.</li>
		<li>2.2.	You must provide true and correct details at all times.</li>
		<li>2.3.	You may not apply for a loan with us if you are under (or intend to apply for) debt review, administration, sequestration, or a restructuring order, or if you are over indebted.</li>
		<li>2.4.	After you have completed the application and agreed to be bound by the terms of the Loan Agreement, we will only make payment of the Loan Amount (as indicated on the Pre-Agreement Statement and Quotation), once we have verified that the information you provided to us is correct, have performed the necessary credit checks and determined that based on the information you can afford the loan.
		</li>
		
		<li>2.5.	You must consider the information provided in the Pre-Agreement Statement and Quotation carefully. It explains the following:
			<ol>
				<li>2.5.1.	Loan amount: this is the amount credit that we will pay into your bank account subject to clause 2.4 above;</li>
				<li>2.5.2.	Initiation fee: this is a once-off amount that we may charge in terms of the Act for initiation the loan agreement;</li>
				<li>2.5.3.	Monthly service fee: this is a monthly charge that we charge in terms of the Act for administering the loan agreement;</li>
				<li>2.5.4.	Interest rate: the interest rate is fixed for the term of the Loan Agreement and is calculated on the daily balance and will be accrued daily. If you are in arrears, we may charge interest on the arrears at the maximum rate allowed by the Act. We will charge interest from the date that the money is paid into your bank account; 
				</li>
			</ol>
		</li>

		<li>2.6.	You can accept the terms of the Loan Agreement electronically by ticking the "I accept" button.</li>
		<li>2.7.	We will send you a copy of the Loan Agreement electronically to the email address provided during application.</li>
		<li>2.8.	During the application process, you will be provided with a PIN number electronically. You must keep this safe and confidential. We will be entitled to assume that any use of your secret PIN is authorised use by yourself. You must notify us immediately if you become aware that you have lost your PIN or that any unauthorised person may have access to it. We will not be responsible for any unauthorised use of your PIN.</li>
		<li>2.9.	If you are married in Community of Property, or in terms of the common or customary law, you confirm that you have obtained your spouses written consent to enter into the loan agreement.</li>
  </ol>
  
<ol>
  <li><b>3.INSURANCE </b>
		<ol>
			<li>3.1.	Your Loan Agreement is subject to you maintaining credit life insurance which shall not exceed the total of your outstanding obligations to us for the duration of the Loan Agreement. </li>
			<li>3.2.	You have the right to waive our proposed insurance policy and procure similar insurance elsewhere, subject to you naming us as the loss payee under that insurance policy in terms of section 106 of the Act.</li> 
			<li>3.3.	Should you choose to exercise the rights set out in paragraph 3.2 above, we may require you to provide us with written directions in a prescribed form naming us as the loss payee under the policy or instructing us to continue to bill you for the amount of the premiums for the alternative policy. </li>
			<li>3.4.	The insurance policy will pay out a maximum toward your outstanding loan amount</li>
			<li>in the event of death, dread disease and permanent disability and will pay limited monthly instalments in the event of retrenchment and temporary disability.</li>
			<li>3.5.	The monthly premiums for such insurance policy is set out in the Pre-Agreement Statement and Quotation. </li>
		</ol>
	</li>	
</ol>
</div>
';
// -- Page 4
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
//$pdf->AddPage();

//$pdf->AddPage();

$html = '
<style>
ol {
  list-style-type:upper-alpha;
  position:relative;
}

.customListBox > ol { 
    counter-increment: lists;
    counter-reset: items;
    list-style-type: none;
}

.customListBox > ol > li::before {
    content: counters(lists, "", decimal) "." counters(items, "", decimal) " ";
    counter-increment: items;
    position:absolute;
    left: 0;
}</style>

<div class="customListBox">
<ol>  
  <li><b>4. ELECTRONIC COMMUNICATIONS</b>
	<ol>
	  <li>4.1.	We will send all loan agreements, statements or other correspondence via email to the email address that you provided to us during application.</li>
	  <li>4.2.	All correspondence sent will be deemed to have been received by you within 36 (thirty six) hours after our log files indicate it to be sent.</li>
	 <li>4.3.	You must immediately inform us of any change to your email address.</li>
	</ol>
  </li>
  <li><b>5.PAYING YOUR LOAN</b>
	  <ol>
	  <li>5.1.	We only provide short term loans.
	  <li>5.2.	Once we have paid the loan amount into your bank account, you are responsible to pay the full outstanding loan on the payment date as indicated in the Pre-Agreement Statement and Quotation, attached hereto.
	 <li>5.3.	If you are in arrears, we will apportion payment as follows:
		<ol><li>5.3.1.	Interest;</li>
		<li>5.3.2.	Service fees;</li>
		<li>5.3.3.	Initiation fee; and</li>
		<li>5.3.4.	Loan amount.</li></ol></li>
		<li>5.4.	You have agreed to receive statements electronically.</li>
		<li>5.5.	If you do not receive a statement, it does not mean that you can withhold the payment due to us.</li>
	  </ol>
  </li> 
  <li><b>6.DEFAULT AND TERMINATION</b>
	<ol>
		<li>6.1.	If you do not pay the full outstanding amount due in terms of the Loan Agreement on time, the full outstanding balance will immediately become due and payable.</li>
		<li>6.2.	If you are in default, including, but not limited to, not paying in terms of the Loan Agreement, providing incorrect or misleading information to us, or committing any other breach of the Loan Agreement, we will inform you in writing that you are in default and suggest that you refer the Loan Agreement to a debt counsellor, alternative dispute resolution agent, consumer court or ombud with jurisdiction, to resolve any dispute or agree on a plan to bring the payments up to date.</li>
		<li>6.3.	If you have been in default for at least 20 (twenty) business days and at least 10 (ten) business days have passed since we sent you notice and you have not responded to the notice, or have responded by rejecting our proposals, we may cancel the Loan Agreement or approach a court for an order to enforce the Loan Agreement. The 20 (twenty) day and 10 (ten) day periods may run simultaneously.</li>
		<li>6.4.	All costs permissible in terms of the Act, including collection costs and default administration charges that we incur for the recovery of any amount due as a result of a breach of this Loan Agreement, shall be payable by you on lawful demand thereof.
		<ol><li>6.4.1.'.$companyname.' may charge the maximum default administration and collections costs as allowed by the Magistrates Court Act No. 32 of 1944, the Supreme Court Act No. 59 of 1959, the Attorneys Act of 1979, or the Debt Collectors Act No. 114 of 1998, whichever applies.</li>
		<li>6.4.2.	If '.$companyname.' appoints an attorney to institute action against you and/or collect any amounts due you agree that you will be liable for costs on an attorney and own client scale.</li>
		</li>
		</ol>
		 <li>6.5.	We may provide an adverse report to the credit bureaus as permitted by the Act on 20 (twenty) days notice to you.
 </li>
	</ol>
  </li>
</ol></div>'; 
		// -- Page 5..
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);		
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
//$pdf->AddPage();
	
$html = '
<style>
ol {
  list-style-type:upper-alpha;
  position:relative;
}

.customListBox > ol { 
    counter-increment: lists;
    counter-reset: items;
    list-style-type: none;
}

.customListBox > ol > li::before {
    content: counters(lists, "", decimal) "." counters(items, "", decimal) " ";
    counter-increment: items;
    position:absolute;
    left: 0;
}</style>

<div class="customListBox">
<ol> 
 <li><b>7.EARLY TERMINATION</b>
	<ol>
		<li>7.1.	In terms of sections 122 and 125 of the Act, you may terminate the Loan Agreement at any time by paying your full outstanding balance to us. This will include all outstanding interest, fees and the loan amount. You do not need to give us notice if you want to terminate early.
		</li>
	</ol>
 </li> 
 <li><b>8.RIGHT TO APPLY TO A DEBT COUNSELLOR</b>
	<ol>
		<li>8.1.	You have the right to apply to a Debt Counsellor to be declared over-indebted in terms of section 86 of the Act before we start any enforcement proceedings against you if you are in default under the Loan Agreement.</li>
		<li>8.2.	The Debt Counsellor will require you to complete a prescribed form and to pay a fee prescribed by the Act. If your application is accepted, the Debt Counsellor will assess your application and will either reject or accept your application.
		</li>
	</ol>
 </li> 
  <li><b>9.STATEMENTS</b>
	<ol>
		<li>9.1.	The parties agree that you will receive a monthly statement of account, unless agreed otherwise, through electronic channels unless only possible to deliver via normal mail.</li> 
		<li>9.2.'.$companyname.'will, at your request, provide you with a statement setting out all charges levied, all payments made and the balance outstanding.</li>
	</ol>
  </li> 
  <li><b>10.COMPLAINTS AND THE NCR</b>
		<ol>
		<li>10.1.	The details of the National Credit Regulator:
		<li>Telephone: 	(011) 554-2600 or 0860 627 627</li>
		<li>Fax: 		(011) 484-6122</li>
		<li>Email: 		info@NCR.org.za</li>
		<li>Website:		www.ncr.org.za </li>
		<li>
		10.2.	The details of the Credit Ombudsman:</li>
		<li>Telephone: 	0861 662 837</li>
		<li>Fax:		086 674 7414</li>
		<li>Email: 		ombud@creditombud.org.za
		<li>Website:		http://www.creditombud.org.za/ 
		</li>
	   </ol>
  </li>
  
  <li><b>11.CERTIFICATE</b>
		<ol>
			<li>11.1.	A certificate signed by any one of our managers (who need not prove that appointment), will be prima facie (on the face of it) proof of the outstanding balance.</li>
		</ol>
  </li> 
  
  <li><b>12.DOMICILIUM</b>
		<ol>
		<li>
		12.1.	Your residential address (or such other address you provided for this purpose), as stated in the Loan Agreement, is the address at which you will accept service of legal documents and notices (your chosen domicilium citandi et executandi).
		<li>12.2.	If your address changes, you must immediately inform us of the new address in writing.</li>
		
		<li>12.3.	Serviceor delivery  of legal documents, including delivery of a section 129 letter,  will take place at your residential address by '.$Street.','.$Suburb.','.$City.','.$State.','.$PostCode.' and registered mail : '.$dataUser['email'].'</li>
		</ol>
  </li> 
  
</ol></div>
';
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
//$pdf->AddPage();
	
$html = '
<style>
ol {
  list-style-type:upper-alpha;
  position:relative;
}

.customListBox > ol { 
    counter-increment: lists;
    counter-reset: items;
    list-style-type: none;
}

.customListBox > ol > li::before {
    content: counters(lists, "", decimal) "." counters(items, "", decimal) " ";
    counter-increment: items;
    position:absolute;
    left: 0;
}</style>

<div class="customListBox">
<ol>
<li><b>13.CESSION</b>
	<ol>
		<li>13.1 You agree that we may, at any time and without notice, cede (transfer) and/or  assign any or all of our rights in and/or claims against you or our obligations to a third party,notwithstanding that a cession to more than one person may result in a splitting of claims against you. By accepting the terms of this Loan Agreement, you expressly accept the splitting of claims.</li>
	 </ol>
   </li>
   
	  <li><b>14.WHOLE AGREEMENT</b>
		<ol>
			<li>14.1 The Loan Agreement forms the whole agreement between us and there are no additional unspoken or implied terms and conditions, unless we amend the Loan Agreement as allowed for in terms of the Act, in which case the amended agreement will apply.</li>
		</ol>
	  </li> 
	  
	  <li><b>15	MARKETING RIGHTS AND OTHER ACKNOWLEDGMENTS:</b>
	  <ol>
		<li>15.1.	You have the right to be excluded from:
		<ol>
			<li>15.1.1.	telemarketing campaigns that may be conducted by or on behalf of us;</li>
			<li>15.1.2.	marketing or customer list that may be sold or distributed other than as required by the Act; or</li>
			<li>15.1.3.	any mass distribution of email or SMS messages by or on behalf of us.</li>
		</ol>
	  </li>

	 <li>15.2.	By accepting the terms of the Loan Agreement, you acknowledge and confirm that:
		<ol>
			<li>15.2.1.	entering into the Loan Agreement with us will not make you over indebted;</li>
			<li>15.2.2.	you understand the risks, costs, and your obligations in terms of the Loan Agreement;</li>
			<li>15.2.3.	you have provided us with true and correct information, and that you understand that we rely on the information provided to us.
		</ol>
	</li>
</ol>
 </div>';
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45); 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);
//$pdf->AddPage();

// BOC -- Add Water Mark, 04.08.2017.
$x = 100;
$y = 100;
$angle = 45;
// EOC -- Add Water Mark, 04.08.2017.

// Print text using writeHTMLCell()

//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";

// -- Get the root directory
$directory = "uploads/";
$filename = $directory.$FILEIDDOC;

// -- Check if the file exist.
// -- $pages = $pdf->setSourceFile( 'contractExample.pdf' );

//$filename = "uploads/23-10-2016-12-15-50DOC001.PDF";
try
{
// -- Add the new page the last 
// -- Display an ID document.
if ( file_exists($directory.$FILEIDDOC) )
{

// -- Check if file name is not empty,
if(!empty($FILEIDDOC))
{

$pdf->AddPage();

// -- Get the last page.  
  $pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($filename);			
		
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		//$pdf->AddPage();
		// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
// -- Watermark 
		$pdf->SetFont('dejavusans', 'B', 50, '', true);
		$pdf->SetTextColor(192,192,192);
		$pdf->RotatedText(20, 230, $watermark, 45);
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 0);
	}
	
	}// -- End File name check.
}
else
{
// File does not exist.
  // $html = $html  + '<h2>File does not exist</h2>'; 
}
}
catch(Exception $e) 
{}

//$pdf->AddPage();

// ----------------------- annexure ‘C’ ------------------------------
//																	 -
// -------------------------------------------------------------------
$debitorderform = "uploads/debitorderform.pdf";
$sw_bookmark = 0;
$pages = "";
// -- Show Debit Order Only if Debit Order is choosen.
if($paymentmethod == $c_DebitOrder)
{
if ( file_exists($debitorderform) )
{
// -- Get the last page.  
  //$pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($debitorderform);
  //$pages = 2;
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		$pdf->AddPage();
	// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
		
		$pdf->SetFont('dejavusans', 'B', 50, '', true);
		$pdf->SetTextColor(192,192,192);
		$pdf->RotatedText(20, 230, $watermark, 45);
		
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 18);
		
	}
}
else
{
// File does not exist.
  // $html = $html  + '<h2>File does not exist</h2>'; 
}
$pdf->AddPage();

// ----------------------- annexure ‘C2’ ------------------------------
//																	 -
// -------------------------------------------------------------------
$debitorderform = "uploads/debitorderform2.pdf";
$sw_bookmark = 0;
$pages = "";

if ( file_exists($debitorderform) )
{
// -- Get the last page.  
  $pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($debitorderform);
  
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		//$pdf->AddPage();
		// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
		
		$pdf->SetFont('dejavusans', 'B', 50, '', true);
		$pdf->SetTextColor(192,192,192);
		$pdf->RotatedText(20, 230, $watermark, 45);
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 18);
		
	}
}
else
{
// File does not exist.
  $html = $html  + "<h2>File does not exist $debitorderform</h2>"; 
}
} // Debit Order Form

// Close and output PDF document
// ============ SEND DONWLOAD AND UPLOAD CONTARCT ===================== //
$fileName = 'contract.pdf';

$fileatt = $pdf->Output($fileName, 'S');
//$attachment = chunk_split($fileatt);
//SendEmail($fileatt);
// ======================================== //
// This method has several options, check the source code documentation for more information.
$pdf->Output($fileName, 'I'); 
//$pdf = PlaceWatermark($pdf,$fileName, "This is a lazy, but still simple test\n This should stand on a new line!", 30, 120, 100,TRUE);
$text = "confidential";
$xxx = 30; 
$yyy = 120;
$op = 100; 
$outdir = true;
$file = $fileName;
//	$pdf->$PDFVersion = '1.4';
$pdf->setPdfVersion('1.4');

//============================================================+
// 	SEND A CONTRACT ONCE PROCESSED FILE
//============================================================+
// ------------ Start Send an Email ----------------//
if(!function_exists('SendEmail'))
{
	function SendEmail($att)
	{
			require_once('class/class.phpmailer.php');

			try
			{
						$mail=new PHPMailer();
						$mail->IsSMTP();
						$mail->SMTPDebug=1;
						$mail->SMTPAuth=true;
						$mail->SMTPSecure="tls";
						$mail->Host="smtp.vogsphere.co.za";
						$mail->Port= 587;//465;
						$mail->Username="vogspesw";
						$mail->Password="fCAnzmz9";
						$mail->SetFrom("info@vogsphere.co.za","e-Loan - Vogsphere (PTY) LTD");
						// -- The Code
						//$mail->AddAttachment($att);
						$mail->AddStringAttachment($att, 'filename.pdf');
						
						//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
						$mail->Subject=  "Your Password has been Recovered";
						//$mail->Body= $email_text;//"This is the HTML message body <b>in bold!</b>";
						// Enable output buffering
						
			ob_start();
			include 'content\text_emailForgottenPassword.php';//execute the file as php
			$body = ob_get_clean();			
						
			$fullname = $FirstName.' '.$LastName;

			$email = 'tumelomodise4@gmail.com';//$_SESSION['email'];
			
						// print $body;
						$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
						$mail->AddAddress($email, $fullname);

						if($mail->Send()) 
						{
						$successMessage = "Your e-Loan application is successfully registered. An e-mail is sent your Inbox!";
						}
						else 
						{
						  // echo 'mail not sent';
						$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";
						
						}
				}		
				catch(phpmailerException $e)
				{
					$successMessage = $e->errorMessage();
				}
				catch(Exception $e)
				{
					$successMessage = $e->getMessage();
				}	
	}
}
//============================================================+
// END OF FILE
//============================================================+
// -- Account/Reference must be the 9 digits derived from the ID number.
// -- Reading from the ID backward.  
if(!function_exists('GenerateAccountNumber'))
{
	function GenerateAccountNumber($customerid)
	{
	   $AccountNumber = '';
	   
	   if(!empty($customerid))
	   {
		 $AccountNumber = strrev($customerid);
		 
		 // -- 9 digits.
		 $AccountNumber = substr($AccountNumber,0,9); 
	   }
	   return $AccountNumber;
	}
}
?>
