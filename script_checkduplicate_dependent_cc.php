<?php
require 'database_ffms.php';
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_ffms_dependent_member'))
{
	function get_ffms_dependent_member($dataFilter)
	{
		////////////////////////////////////////////////////////////////////////
		// -- Database Declarations and config:
	    $pdo = db_connect::db_connection();			
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$data = null;
		$tbl  = 'extendend_member';			
		$sql = "select * from $tbl where Ext_member_id = ? AND Ext_memberType_id = ? AND Main_member_id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['Ext_member_id'],$dataFilter['Ext_memberType_id'],$dataFilter['Main_member_id']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}
$message = '';
$list = getpost('list');

$data_member_Obj = null;
// -- Check member details
$data_member_Obj = get_ffms_dependent_member($list);

if(isset($data_member_Obj['Ext_member_id']))
{
	// -- Member warning message.
	$message =  $message.'ERROR: Dependent/Extended/Benefiary Member ('.$data_member_Obj['Ext_member_id'].') already exist on the system for Main Member '.$data_member_Obj['Main_member_id'];
	
	/*$data_member_Obj['Action_Date'].' from bank account number '.
	$databill_single_instructionObj['Account_Number'].' with client reference '.
	$databill_single_instructionObj['Client_Reference_1'];*/			
}

 echo $message; 
?>