<?php
// -- 18.09.2021 -- //
date_default_timezone_set('Africa/Johannesburg');
////////////////////////////////////////////////////////////////////////
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{ return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);}
}
if(!function_exists('calculateBankHolidays'))
{
/*
*    Function to calculate which days are South African bank holidays for a given year.
*    Referenced: David Scourfield, 07 August 2006(https://www.php.net/manual/en/ref.calendar.php) for British
*    Created by Vogsphere, 17 September 2021
***  Please Note : The Public Holidays Act (Act No 36 of 1994 [PDF])
***  Determines whenever any public holiday falls on a Sunday,
**** the Monday following on it shall be a public holiday.
*    USAGE:
*
*    array calculateBankHolidays(int $yr)
*
*    ARGUMENTS
*
*    $yr = 4 digit numeric representation of the year (eg 1997).
*
*    RETURN VALUE
*
*    Returns an array of strings where each string is a date of a bank holiday in the format "yyyy-mm-dd".
*
*/

function calculateBankHolidays($yr) {

    $bankHols = Array();

    // -- #1 1 January: New Year’s Day
    switch ( date("w", strtotime("$yr-01-01 12:00:00")) ) {
        case 0:// sunday
            $bankHols[] = "$yr-01-02";
            break;
        default:
            $bankHols[] = "$yr-01-01";
    }
	// #2 21 March: Human Rights Day
	 switch ( date("w", strtotime("$yr-01-01 12:00:00")) ) {
        case 0:// sunday
            $bankHols[] = "$yr-03-22";
            break;
        default://weekday
            $bankHols[] = "$yr-03-21";
    }
	// #3 15 April: Good Friday *
    $bankHols[] = date("Y-m-d", strtotime( "+".(easter_days($yr) - 2)." days", strtotime("$yr-03-21 12:00:00") ));

    // #4 Easter Monday:
    $bankHols[] = date("Y-m-d", strtotime( "+".(easter_days($yr) + 1)." days", strtotime("$yr-03-21 12:00:00") ));

	// #5 27 April: Freedom Day
	// echo 'Freedom Day:'.date("w", strtotime("$yr-04-27 12:00:00"));
     switch ( date("w", strtotime("$yr-04-27 12:00:00")) ) {
        case 0:// sunday
            $bankHols[] = "$yr-04-28";
            break;
        default://weekday
            $bankHols[] = "$yr-04-27";
    }
    // #6  1 May : Worker's Day
        switch (date("w", strtotime("$yr-05-01 12:00:00"))) {
            case 0:// sunday
                $bankHols[] = "$yr-05-02";
                break;
			default://weekday
                $bankHols[] = "$yr-05-01";
                break;
        }
    // #7  16 June: Youth Day
        switch (date("w", strtotime("$yr-06-16 12:00:00"))) {
            case 0:// sunday
                $bankHols[] = "$yr-06-17";
                break;
			default://weekday
                $bankHols[] = "$yr-06-16";
                break;
        }
	// #8  9 August: National Women’s Day
        switch (date("w", strtotime("$yr-08-09 12:00:00"))) {
            case 0:// sunday
                $bankHols[] = "$yr-08-10";
                break;
			default://weekday
                $bankHols[] = "$yr-08-09";
                break;
        }
	// #9 24 September: Heritage Day
		switch (date("w", strtotime("$yr-09-24 12:00:00"))) {
            case 0:// sunday
                $bankHols[] = "$yr-09-25";
                break;
			default://weekday
                $bankHols[] = "$yr-09-24";
                break;
        }
	// #10 16 December: Day of Reconciliation
	   switch (date("w", strtotime("$yr-08-09 12:00:00"))) {
            case 0:// sunday
                $bankHols[] = "$yr-12-17";
                break;
			default://weekday
                $bankHols[] = "$yr-12-16";
                break;
        }
	// #11 25 December: Christmas Day
    // #12 26 December: Day of Goodwill
    switch ( date("w", strtotime("$yr-12-25 12:00:00")) ) {
        case 5://friday
            $bankHols[] = "$yr-12-25";
            //$bankHols[] = "$yr-12-28";
            break;
        case 6://saturday
            $bankHols[] = "$yr-12-27";
            //$bankHols[] = "$yr-12-28";
            break;
        case 0:// -- sunday
            $bankHols[] = "$yr-12-26";
            //$bankHols[] = "$yr-12-27";
            break;
        default://weekday
            $bankHols[] = "$yr-12-25";
            $bankHols[] = "$yr-12-26";
    }

    // Millenium eve
    if ($yr == 1999) {
        $bankHols[] = "1999-12-31";
    }

    return $bankHols;

}}
// -- rule_two_day Service
if(!function_exists('rule_two_day_service'))
{
	function rule_two_day_service($action_date)
	{
		// -- Get Bank Holidays
		// -- Holidays for the year
		$year = substr($action_date,0,4);
		$bankHols = calculateBankHolidays($year);

		// -- stop time.
		$stoptime = date("h:i",strtotime("11:00"));
		// -- SysTime(st)
		$st = date("H:i");
		// -- SysDate(sd)
		$sd = date('Y-m-d');
		// -- Input parameter ~ Action date(ad)
		$sd_plus_3 = date('Y-m-d', strtotime($sd. ' + 3 days'));
			/*if($st > $stoptime){$action_date = date('Y-m-d', strtotime($action_date. ' + 1 days'));}*/

	// -- 10/01/2021 avoid confusing message..
		$cutoffmsg = '';
		$actiondateBeforeCutoff = $action_date;
		
		// --  1). Action Date (AD) less System Date (SD) = SD + 3, then make Action Date = SD + 3
		// -- SysDate + 3 days
		// TWO DAY SERVICE ACTION DATE SHOULD NOT BE LESS THAN SYSTEM DATE + 2 DAYS
		if($action_date < date('Y-m-d', strtotime($sd_plus_3)))
		{
			//$action_date= "Test1 |".$action_date; //- Enable for debugging
			$action_date  = $sd_plus_3;
		}
	
		// --  2). Action Date (AD) = System Date (SD) then Action Date = SD + 3
     if($action_date == date('Y-m-d', strtotime($sd_plus_3)))
		{
					//$action_date= "Proposed Test2 |".$action_date; // - Enable for debugging
					// -- If SysTime: before Stop Time. 11:00
					if($st <= $stoptime){/* do nothing */}
					// -- If SysTime: after the Stop Time.
					// -- Make AD + 4 days
					if($st > $stoptime){
						$sd_plus_4 = date('Y-m-d', strtotime($sd. ' + 4 days'));
						$actiondate=	$sd_plus_4;
						// -- 10/01/2021
						$cutoffmsg = '.<b>Action Date :</b>'.$actiondateBeforeCutoff.' bumped to next processing date <b> Action Date:</b>';
					}

					// -- if NOT a weekend and NOT a bank holiday - Pick this Action date,else use system generated Action Date.
				if(!CheckIfWeekend($action_date))
			  {
				//$action_date= "Proposed Test2 do nothing |".$action_date; // -- do nothing
								if(!in_array($action_date, $bankHols))
								{
								/*$action_date= "Test2 do nothing |".$action_date; do nothing*/
								//$action_date = 'Proposed |'.$action_date;
													// 10/01/2021
					$cutoffmsg = $cutoffmsg.next_actiondate( false,$action_date,$bankHols ).' due to cutoff time '.$st.' > '.$stoptime;
					$action_date = $cutoffmsg.' |'. next_actiondate( false,$action_date,$bankHols );

								}
								else
								{
									$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );									
								}
				}

				else
				{
					$cutoffmsg = 'Proposed ';
					$action_date = $cutoffmsg.' |'. next_actiondate( false,$action_date,$bankHols );
				}


		}
		// --  3). Action Date (AD) greater System Date (SD) = SD + 3.
		else
		{
			//$action_date= "Proposed Test3 |".$action_date;//- Enable for debugging
		    // -- if NOT a weekend and NOT a bank holiday - Pick this Action date,else use system generated Action Date.
				if(!CheckIfWeekend($action_date))
			  {
				/*$action_date= "Test2 do nothing |".$action_date; do nothing*/
								if(!in_array($action_date, $bankHols))
								{
								/*$action_date= "Test2 do nothing |".$action_date; do nothing*/
								}
								else
								{
									$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );
								}
				}

				else
				{
              if(Weekendday($action_date)=="saturday")
							{
								$action_date = 'Proposed |'.date('Y-m-d', strtotime(	$action_date. ' + 4 days'));
							}
							if(Weekendday($action_date)=="sunday")
							{
								$action_date = 'Proposed |'.date('Y-m-d', strtotime(	$action_date. ' + 3 days'));
							}
					//$action_date = 'Proposed |'.date('Y-m-d', strtotime(	$action_date. ' + 4 days'));
					//$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );
				}

		}

		return $action_date;
	}
}
// -- Add Email/SMS History.
if(!function_exists('CheckIfWeekend'))
{
	function CheckIfWeekend($dt)
	{
		$results = false;
		date_default_timezone_set('Africa/Johannesburg');

        $dt1 = strtotime($dt);
        $dt2 = date("l", $dt1);
        $dt3 = strtolower($dt2);
    if(($dt3 == "saturday" )|| ($dt3 == "sunday"))
		{
            $results = true;//echo $dt3.' is weekend'."\n";
        }
    else
		{
            $results = false;//echo $dt3.' is not weekend'."\n";
        }
		return $results;
	}
}
if(!function_exists('Weekendday'))
{
	function Weekendday($dt)
	{
		$results = false;
		date_default_timezone_set('Africa/Johannesburg');

        $dt1 = strtotime($dt);
        $dt2 = date("l", $dt1);
        $dt3 = strtolower($dt2);
    if(($dt3 == "saturday" )|| ($dt3 == "sunday"))
		{
            $results = $dt3;//echo $dt3.' is weekend'."\n";
        }
    else
		{
            $results = $dt3;//echo $dt3.' is not weekend'."\n";
        }
		return $results;
	}
}
// -- Recursively pick the Next Available Action date
if(!function_exists('next_actiondate'))
{
	function next_actiondate( $available,$pre_action_date,$bankHols )
	{
		// -- #1 Base Case, if the proposed date is available pick it.
		if($available)
		{
			return $pre_action_date;
		}
		// -- #2 Recursion
		if(!CheckIfWeekend($pre_action_date) && !in_array($pre_action_date, $bankHols))
		{
			$available = true;
		}
		else
		{
			$available = false;
			$pre_action_date = date('Y-m-d', strtotime($pre_action_date. ' + 1 days'));
		}
		// -- Results
		$results = next_actiondate( $available,$pre_action_date,$bankHols );
		return $results;
	}
}
////////////////////////////////////////////////////////////////////////
// -- rule_sameday Service -- 06.11.2021
if(!function_exists('rule_sameday_service'))
{
	function rule_sameday_service($action_date)
	{
		// -- Get Bank Holidays
		// -- Holidays for the year
		$year = substr($action_date,0,4);
		$bankHols = calculateBankHolidays($year);

		// -- stop time.
		$stoptime = date("h:i",strtotime("12:00"));

		if(Weekendday($action_date)=="saturday")
		{
				$stoptime = date("h:i",strtotime("09:00"));
		}
		// -- SysTime(st)
		$st = date("H:i");
		// -- SysDate(sd)
		$sd = date('Y-m-d');
		// -- Input parameter ~ Action date(ad)
		//$sd_plus_3 = date('Y-m-d', strtotime($sd. ' + 3 days'));
		$sd_plus_1 = date('Y-m-d', strtotime($sd. ' + 1 days'));
// -- SAME DAY SERVICE Rules -- //
// -- Rule #1 : If action_date eq system_date, then check the schedule time.
		if($action_date == date('Y-m-d'))
		{
// -- Rule #2 : If system_time > 12:00 them suggest the next day then
			if($st > $stoptime)
			{
				$action_date=	$sd_plus_1;
// -- Rule #3 :check if next day is not a weekend then
				if(!CheckIfWeekend($action_date))
			    {
// -- Rule #4 :check if next day is not a holiday then
					if(in_array($action_date, $bankHols))
					{
						$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );
					}else{$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );}
				}
				else
				{
						if(Weekendday($action_date)=="saturday")
						{
							// -- Rule #4 :check if next day is not a holiday then
							if(in_array($action_date, $bankHols))
							{
								$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );
							}
						}
						if(Weekendday($action_date)=="sunday")
						{
							$action_date = 'Proposed |'.date('Y-m-d', strtotime(	$action_date. ' + 1 days'));
						}
				}
		 }}
		 else
		 {
			 // -- Rule #3 :check if next day is not a weekend then
				if(!CheckIfWeekend($action_date))
			    {
// -- Rule #4 :check if next day is not a holiday then
					if(in_array($action_date, $bankHols))
					{
						$action_date = 'Proposed |'. next_actiondate( false,$action_date,$bankHols );
					}else{$action_date = next_actiondate( false,$action_date,$bankHols );} //'Proposed |'. 
				}
				else
				{
						if(Weekendday($action_date)=="saturday")
						{
							// -- Rule #4 :check if next day is not a holiday then
							if(in_array($action_date, $bankHols))
							{
								$action_date = next_actiondate( false,$action_date,$bankHols );//'Proposed |'. 
							}
						}
						if(Weekendday($action_date)=="sunday")
						{
							$action_date = date('Y-m-d', strtotime(	$action_date. ' + 1 days')); //'Proposed |'.
						}
				}
		 }
		return $action_date;
	}
}
////////////////////////////////////////////////////////////////////////
// -- POST VALUES validations & assignment.
// -- Check if Charges Exist.
if(!function_exists('getpost'))
{
	function getpost($name)
	{
		$value = '';
		if(isset($_POST[$name]))
		{
			$value = $_POST[$name];
		}

		return $value;
	}
}
////////////////////////////////////////////////////////////////////////
$message = '';
$list = getpost('list');
////////////////////////////////////////////////////////////////////////
// Called from a javascript
if(!empty($list))
{
	// -- Type :  TWO DAY (13.11.2021)
	$message = $list['Action_Date'];
	if(isset($list['Type']) && isset($list['Action_Date']))
	{	
			//$message = $message.'Test1';

	   if(!empty($list['Action_Date']))
	   {
			if($list['Type'] == 'TWO DAY')
			{
			  $message = rule_two_day_service($list['Action_Date']);
			}
			// -- Type : SAMEDAY (13.11.2021)
			if($list['Type'] == 'SAMEDAY')
			{
			   $message = rule_sameday_service($list['Action_Date']);
			}
			//$message = $message.'Test2';
	   }
	}
    echo $message;
}
?>
