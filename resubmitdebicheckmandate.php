<?php
require 'database.php';

if (!function_exists('resubmitdebicheck'))
{
	function resubmitdebicheck($id,$arrayCollections)
	{
	 try
	  {
		//Global $pdo;
		//print($id);
		$id = $arrayCollections['idmandate'];
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql		 = 'SELECT * FROM mandates where idmandate = ?';
		$q		 	 = $pdo->prepare($sql);
		$q->execute(array($id));
		$dataMandate = $q->fetch(PDO::FETCH_ASSOC);

		$sql = "UPDATE mandates
		SET
		reference               = ?,
		status_code				= ?,
		status_desc				= ?,
		changedby				= ?,
		changedon 				= ?,
		changedat 				= ?
		WHERE idmandate 		= ?";
		$q = $pdo->prepare($sql);

		//print_r($arrayCollections );
		$arrayCollections['reference']   = 'R_'.$dataMandate['reference'];		
		$arrayCollections['changedon']   = date('Y-m-d');
		$arrayCollections['changedat']   = date('h:m:s');;
		$arrayCollections['status_code'] = '999999';
		$arrayCollections['status_desc'] = 'Resubmitted';		
		
		$q->execute(array(
		$arrayCollections['reference'],		
		$arrayCollections['status_code'],
		$arrayCollections['status_desc'],		
		$arrayCollections['changedby'],
		$arrayCollections['changedon'],
		$arrayCollections['changedat'],
		$id));
		Database::disconnect();

		// -- Get mandate.
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql		 = 'SELECT * FROM mandates where idmandate = ?';
		$q		 	 = $pdo->prepare($sql);
		$q->execute(array($id));
		$dataMandate = $q->fetch(PDO::FETCH_ASSOC);
		//print_r($dataMandate);
		$dataMandate['reference']   = $dataMandate['contract_reference'];		
		$message = create_debicheck1($dataMandate);
		
		if($message == 'success')
		{
			 $message = 'Debi Check Reference : '.$dataMandate['reference'].' was resubmit successfully.';
		}

		Database::disconnect();

		return $message;
		
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
if (!function_exists('create_debicheck1'))
{	
	function create_debicheck1($dataMandate)
	{
	 try
	  {
		/*Global $pdo;*/
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		//////////////////////////////////// Object ////////////////////////////////////		
		$arrayInstruction[1]  = $dataMandate['reference']; 			  // Reference
		$arrayInstruction[2]  = $dataMandate['contract_reference'];    // Reference
		$arrayInstruction[3]  = $dataMandate['tracking_indicator'];    // tracking indicator
		$arrayInstruction[4]  = $dataMandate['debtor_auth_code'];	  // debtor_auth_code
		$auth_type 			  = $dataMandate['auth_type'];      		  // auth_type first 9  characters.
		$arrayInstruction[5]  = $auth_type; 							  // auth type
		$arrayInstruction[6]  = $dataMandate['instalment_occurence'];  // instalment_occurence');
		$arrayInstruction[7]  = $dataMandate['frequency'];  			  // Frequency
		$arrayInstruction[8]  = $dataMandate['mandate_initiation_date']; // mandate_initiation_date');
		$arrayInstruction[9]  = $dataMandate['first_collection_date']; // first_collection_date');
		$arrayInstruction[10] = $dataMandate['collection_amount_currency']; // collection_amount_currency');
		$arrayInstruction[11] = $dataMandate['collection_amount'];	        // collection_amount');
		$arrayInstruction[12] = $dataMandate['maximum_collection_currency'];// maximum_collection_currency');
		$arrayInstruction[13] = $dataMandate['maximum_collection_amount'];  // maximum_collection_amount 			= getpost('maximum_collection_amount');
		$arrayInstruction[14] = $dataMandate['entry_class'];  		  // entry_class Insurance Premium						= getpost('entry_class');
		$arrayInstruction[15] = $dataMandate['debtor_account_name'];  // debtor_account_name 				= getpost('debtor_account_name');
		$arrayInstruction[16] = $dataMandate['debtor_identification'];// debtor_identification 				= getpost('debtor_identification');
		$arrayInstruction[17] = $dataMandate['debtor_account_number'];// debtor_account_number 				= getpost('debtor_account_number');
		$arrayInstruction[18] = $dataMandate['debtor_account_type'];  // debtor_account_type 				= getpost('debtor_account_type');
		$arrayInstruction[19] = $dataMandate['debtor_branch_number']; // debtor_branch_number 				= getpost('debtor_branch_number');
		$arrayInstruction[20] = $dataMandate['debtor_contact_number'];// debtor_contact_number 				= getpost('debtor_contact_number');
		$arrayInstruction[21] = $dataMandate['debtor_email'];		  // debtor_email 						= getpost('debtor_email');
		$arrayInstruction[22] = $dataMandate['collection_date'];      // 
		$arrayInstruction[23] = $dataMandate['date_adjustment_rule_indicator'];   // date_adjustment_rule_indicator 	= getpost('date_adjustment_rule_indicator');
		$arrayInstruction[24] = $dataMandate['adjustment_category'];              // adjustment_category 				= getpost('adjustment_category');
		$arrayInstruction[25] = $dataMandate['adjustment_rate'];                  // adjustment_rate 					= getpost('adjustment_rate');
		$arrayInstruction[26] = $dataMandate['adjustment_amount_currency'];       // adjustment_amount_currency 		= getpost('adjustment_amount_currency');
		$arrayInstruction[27] = $dataMandate['adjustment_amount'];                // adjustment_amount 					= getpost('adjustment_amount');
		$arrayInstruction[28] = $dataMandate['first_collection_amount_currency']; // first_collection_amount_currency   = getpost('first_collection_amount_currency'); 
		$arrayInstruction[29] = $dataMandate['first_collection_amount']; 		  // getpost('first_collection_amount');
		$arrayInstruction[30] = $dataMandate['debit_value_type']; 				  // debit_value_type 					= getpost('debit_value_type');
		$arrayInstruction[31] = $dataMandate['cancellation_reason'];   			  // cancellation_reason 				= getpost('cancellation_reason');
		$arrayInstruction[31] = ' ';		
		$arrayInstruction[32] = ' ';								  // amended 							= getpost('amended');
		$arrayInstruction[33] = '00';//$dataMandate['status_code'];		  // status_code;						  //getpost('status_code');
		$arrayInstruction[34] = 'pending';//$dataMandate['status_desc'];		  // status_desc;						  //getpost('status_desc');
		$arrayInstruction[35] = $dataMandate['document_type'];        // document_type 						= getpost('Client_ID_Type');
		$arrayInstruction[36] = '';									  // amendment_reason_md16 				= getpost('amendment_reason_md16');
		$arrayInstruction[36] = ' ';
		$arrayInstruction[37] = ' ';								  //$amendment_reason_text 				= getpost('amendment_reason_text');
		$arrayInstruction[37] = ' ';
		$arrayInstruction[38] = $dataMandate['tracking_cancellation_indicator']; // tracking_cancellation_indicator 	= getpost('tracking_cancellation_indicator');		
		$arrayInstruction[39] = $dataMandate['usercode'];
		$arrayInstruction[40] = $dataMandate['createdby'];
		$arrayInstruction[41] = date('Y-m-d');
		$arrayInstruction[42] = $dataMandate['collection_date_text'];
		$arrayInstruction[43] = $dataMandate['IntUserCode'];
		$arrayInstruction[44] = getsession('nominated_account');       //nominated account
		$arrayInstruction[45] = 'Y';       //RMS Mandate set to Y
		//////////////////////////////////////////////////////////////////////////////////////
		$message              = auto_create_debicheck_mandate($arrayInstruction); 
		return $message; 
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
  }
}
// -- Resubmit Debicheck Mandate.
$id 			     = getpost('id_'); 			
$list		         = getpost('list');			
$message 			 = '';

// -- Function.
$message = resubmitdebicheck($id,$list);

echo $message;
//print_r($list );	
?>