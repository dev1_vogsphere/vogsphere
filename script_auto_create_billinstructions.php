<?php
/*
Rules :
- Get the debit order with below conditions
- Frequency = 'Monthly'
- Action_Date < {today's date}
- Status_Code in ('00') in a list current '00'
- Group by client_reference_1
*/
$title='script_auto_create_billinstruction';
// -- Declarations
require 'database.php';
require  'script_rule_two_day_service.php'; // -- 18.09.2021 Rule #1 Two Day Service..
$message = '';
$arrayInstruction = null;
$collection_data = null;
// -- Dynamically API for data collection.
$tablename = 'collection';
$dbnameobj = 'ecashpdq_paycollections';
$queryFields = null;
$whereArray = null;
$statusList = "('00','50','02','09','99')"; // -- Transaction Success, Insufficient Funds , Submitted ,Held for Representment
$today = date('Y-m-d');
// -- Payment Run Count
if(!function_exists('paymentRunCount'))
{
	function paymentRunCount($accountnumber)
	{
			$paymentRunCount = 0;
			$dbnameobj 	 = 'ecashpdq_paycollections';
			$queryFields = null;
			$whereArray  = null;
			$whereArray[]  = "Client_Reference_1 = '{$accountnumber}'";  // -- Action Date.
			//$whereArray[]  = "Action_Date = '{$actiondate}'"; 			 // -- Client Reference.
			$queryFields[] = '*';
			$collectionObj = search_dynamic($dbnameobj,'collection',$whereArray,$queryFields);
			if($collectionObj->rowCount() > 0)
			{
				$paymentRunCount = $collectionObj->rowCount();
			}

			return $paymentRunCount;
	}
}

if(!function_exists('NextActionDate'))
{
	// -- Next Action Date.
function NextActionDate($Actiondate)
{
	$day = substr($Actiondate,-2);
	//$month = substr($Actiondate,5,2) + 1;
	//$year = substr($Actiondate,0,4);
	$NextActiondate = '';
	$daysInMonth = 0;

	// -- Get the current day, month, year.
	$cyear = date('Y');
	$cmonth = date('m');
	$cday = date('d');

	$month  = $cmonth;
	$year   = $cyear;

	// -- if the Next Month is greater than 12, we going to the next year.
	if($month > 12)
	{
	  $year = $year + 1;
	  $month = 1;
	}

	// -- Get the days in a month.
	$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year );

	// -- Ensure the days dont get confused.
	if($day > $daysInMonth)
	{
	  $day = $daysInMonth;
	}

	// -- if the month is less than 10 add 0.
	if($month < 10)
	{
	   //$month = "0".$month;
	}

	$NextActiondate = $year.'-'.$month.'-'.$day;
	return $NextActiondate;
}
}
?>
<div class="row">
  <div class="table-responsive">
	<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
	  <tr>
					  <td>Feedback</td>
					  <td>Client_Id</td>
					  <td>Client_Reference_1</td>
					  <td>Client_Reference_2</td>
					  <td>Client_ID_Type 		</td>
					  <td>Client_ID_No 		</td>
					  <td>Initials 			</td>
					  <td>Account_Holder 		</td>
					  <td>Account_Type 		</td>
					  <td>Account_Number 		</td>
					  <td>Branch_Code 		</td>
					  <td>No_Payments 		</td>
					  <td>Service_Type 		</td>
					  <td>Service_Mode 		</td>
					  <td>Frequency 			</td>
					  <td>Action_Date 		</td>
					  <td>Bank_Reference 		</td>
					  <td>Amount 				</td>
		</tr>
				<?php
				$arrayCollections = null;
				// -- from first of the previous month till today.
				// --  first day of last month
				$datestring=date('Y-m-d').' first day of last month';
				$dt = date_create($datestring);
				$startdate = $dt->format('Y-m-01'); //2011-02
				//$startdate = '2019-11-01';
				//echo $startdate.' - End date : '.$today;
				$whereArray = null;
				$whereArray[] = "Frequency = 'Monthly'";

				$whereArray[] = "Action_Date > '{$startdate}'"; // -- Start Action_Date records.
				$whereArray[] = "Action_Date < '{$today}'"; // -- Action_Date.

				$whereArray[] = "Status_Code IN {$statusList} order by Action_Date desc"; // -- Action Date. //group by Client_Reference_1
				//$queryFields[] = 'Client_Id,Client_Reference_2,Client_ID_Type,Initials,Account_Holder,Account_Type,Account_Number,Branch_Code,No_Payments,Service_Type,Service_Mode,Frequency,MAX(Action_Date) as Action_Date,Bank_Reference,Amount';
				$queryFields[] = '*';
				$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
				//print_r($collectionObj);
				if(empty($collectionObj))
				{
					$message = 'no record found to update';
					echo"<tr><td>".$message."</td><td></tr>";
				}
				else
				{
					foreach($collectionObj as $row)
					{	$valid = true;
						$message = '';
						if(isset($row['Client_Id']		   )){$arrayInstruction[1] =  $row['Client_Id']		;}else{$arrayInstruction[1] = '';}
						if(isset($row['Client_Reference_1'])){$arrayInstruction[2] =  $row['Client_Reference_1'];}else{$arrayInstruction[2] = '';}
						if(isset($row['Client_Reference_2'])){$arrayInstruction[3] =  $row['Client_Reference_2'];}else{$arrayInstruction[3] = '';}
						if(isset($row['Client_ID_Type']    )){$arrayInstruction[4] =  $row['Client_ID_Type']    ;}else{$arrayInstruction[4] = '';}
						if(isset($row['Client_ID_No']      )){$arrayInstruction[5] =  $row['Client_ID_No']      ;}else{$arrayInstruction[5] = '';}
						if(isset($row['Initials']		  )) {$arrayInstruction[6] =  $row['Initials']		  ;  }else{$arrayInstruction[6] = '';}
						if(isset($row['Account_Holder']    )){$arrayInstruction[7] =  $row['Account_Holder']    ;}else{$arrayInstruction[7] = '';}
						if(isset($row['Account_Type']      )){$arrayInstruction[8] =  $row['Account_Type']      ;}else{$arrayInstruction[8] = '';}
						if(isset($row['Account_Number']    )){$arrayInstruction[9] =  $row['Account_Number']    ;}else{$arrayInstruction[9] = '';}
						if(isset($row['Branch_Code']       )){$arrayInstruction[10] = $row['Branch_Code']       ;}else{$arrayInstruction[10] ='';}
						if(isset($row['No_Payments']       )){$arrayInstruction[11] = $row['No_Payments'];/*'99';*/}else{$arrayInstruction[11] ='';}
						if(isset($row['Service_Type']      )){$arrayInstruction[12] = $row['Service_Type']      ;}else{$arrayInstruction[12] ='';}
						if(isset($row['Service_Mode']      )){$arrayInstruction[13] = $row['Service_Mode']      ;}else{$arrayInstruction[13] ='';}
						if(isset($row['Frequency']         )){$arrayInstruction[14] = $row['Frequency']         ;}else{$arrayInstruction[14] ='';}
						if(isset($row['Action_Date']       )){$arrayInstruction[15] = NextActionDate($row['debitorder_date']);/*NextActionDate($row['Action_Date']);*/}else{$arrayInstruction[15] ='';}
						if(isset($row['Bank_Reference']    )){$arrayInstruction[16] = $row['Bank_Reference']    ;}else{$arrayInstruction[16] ='';}
						if(isset($row['Amount']            )){$arrayInstruction[17] = $row['Amount']            ;}else{$arrayInstruction[17] ='';}
						$arrayInstruction[18] = '1';//$keydata['Status_Code'];
						$arrayInstruction[19] = 'pending';//$keydata['Status_Description'];
						$arrayInstruction[20] = '';//$keydata['mandateid'];
						$arrayInstruction[21] = '';//$keydata['mandateitem'];
						if(isset($row['IntUserCode']       )){$arrayInstruction[22] = $row['IntUserCode']       ;}else{$arrayInstruction[22] ='';}
						if(isset($row['UserCode']          )){$arrayInstruction[23] = $row['UserCode']          ;}else{$arrayInstruction[23] ='';}

						if(isset($row['entry_class'])){$arrayInstruction[24] = $row['entry_class'];}else{$arrayInstruction[24] = 0;}
						if(isset($row['Contact_No'])){$arrayInstruction[25] = $row['Contact_No'];}else{$arrayInstruction[25] = '';}
						if(isset($row['Notify'])){$arrayInstruction[26] = $row['Notify'];}else{$arrayInstruction[26] = '';}
						// -- Debit Order Date - 03/12/2021
						$arrayInstruction[30] = $row['debitorder_date'];

						// -- System user - 9901019999999
						$arrayInstruction[27] = '9901019999999';

						// -- Rule #1 if payment is zero or empty (it means its an infinite run until otherwise instructed.), else check total count of run against
						// -- number of payment runs
						if(empty($row['No_Payments']) or $row['No_Payments'] == '0')
						{
							// -- initialise to zero
							$arrayInstruction[11] = '00';
						}
						else
						{
							$payruncount = paymentRunCount($row['Client_Reference_1']);
							if( $payruncount >= $row['No_Payments'] )
							{
								$message = 'Reached limit of total number of payments :'.$row['No_Payments'].' by '.$payruncount;
								$valid = false;
							}
						}
						$ruleTwoDayMsg = '';
						if(!empty($arrayInstruction[15]))
						{
						// -- Rule #1 - 18.09.2021 = Validate Rule Two Day Services -- //
							if($arrayInstruction[12] == 'TWO DAY' or $arrayInstruction[12] == 'SAMEDAY' or $arrayInstruction[12] == 'TWODAY' or $arrayInstruction[12] == 'SAME DAY') // -- Service Day
							{
								// -- Action Date Validation.
								$result = rule_two_day_service($arrayInstruction[15]);
								$results = explode("|", $result);
								if(isset($results[1]))// -- There is a proposed Action Date
								{
									$ruleTwoDayMsg = '.<b>Action Date :</b>'.$arrayInstruction[15].' is a weekend or holiday. system auto generated <b>Action Date:</b>'.$results[1];
									$arrayInstruction[15] = $results[1];
								}
							}
						///////////////////////////////////////////////////////////////////////////////
						}
						else{$message = 'no actiondate'; $valid = false;}
						// -- if its valid process the creating of the batch instruction.
						if ($valid)
						{
							$message = auto_create_billinstruction($arrayInstruction);
						}
						if(empty($message)){$message = 'success'.$ruleTwoDayMsg;}
						echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";
					}
				}
				//echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";
				?>
	<table>
   </div>
</div>
