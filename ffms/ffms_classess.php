<?php 
class memberBLL
{
    public $Member_id = "";
    public $Title = "";
    public $First_name = "";
    public $Middle_name = "";
    public $Initials = "";
    public $Surname = "";
    public $Gender_id = "";
    public $Date_of_birth = "";
    public $MaritualStatus_id = "";
    public $Marriage_id = "";
    public $Spouse_id = "";
    public $Address_id = "";
    public $Life_State_id = "";
    public $Language_id = "";
    public $FILEIDDOC = "";
    public $FILECONTRACT = "";
    public $FILEBANKSTATEMENT = "";
    public $FILEPROOFEMP = "";
    public $FILEFICA = "";
    public $FILECREDITREP = "";
    public $FILECONSENTFORM = "";
    public $FILEOTHER = "";
    public $FILEOTHER2 = "";
    public $FILEDEBITFORM = "";
    public $FILEOTHER3 = "";
    public $FILEOTHER4 = "";

            
        function __construct()
        {
            $arguments = func_get_args();
            $numberOfArguments = func_num_args();
            include "ffms_generic.php";
        }		
        // Default constructor
        // Overloaded constructor
        public function construct4($Member_id,$Title,$First_name,$Middle_name,$Initials,$Surname,$Gender_id,$Date_of_birth,$MaritualStatus_id,$Marriage_id,
        $Spouse_id,$Address_id,$Life_State_id,$Language_id,$FILEIDDOC,$FILECONTRACT,$FILEBANKSTATEMENT,$FILEPROOFEMP
        ,$FILEFICA,$FILECREDITREP,$FILECONSENTFORM,$FILEOTHER,$FILEOTHER2,$FILEDEBITFORM,$FILEOTHER3,$FILEOTHER4)
        {
            $this->set_Member_id($Member_id);
            $this->set_Title($Title);
            $this->set_Title($First_name);
            $this->set_Title($Middle_name);
            $this->set_Title($Initials);
            $this->set_Title($Surname);
            $this->set_Title($Gender_id);
            $this->set_Title($Date_of_birth);
            $this->set_Title($MaritualStatus_id);
            $this->set_Title($Marriage_id);
            $this->set_Title($Spouse_id);
            $this->set_Title($Address_id);
            $this->set_Title($Life_State_id);
            $this->set_Title($Language_id);
            $this->set_Title($FILEIDDOC);
            $this->set_Title($FILECONTRACT);
            $this->set_Title($FILEBANKSTATEMENT);
            $this->set_Title($FILEPROOFEMP);
            $this->set_Title($FILEFICA);
            $this->set_Title($FILECREDITREP);
            $this->set_Title($FILECONSENTFORM);
            $this->set_Title($FILEOTHER);
            $this->set_Title($FILEOTHER2);
            $this->set_Title($FILEDEBITFORM);
            $this->set_Title($FILEOTHER3);
            $this->set_Title($FILEOTHER4);

        }
        
        //     * @param  the  to set
        function set_Member_id($value)
        {  
            $this->Member_id = $value;
        }
        // *
        //     * @return the 
        function get_Member_id()
        {
            return $this->Member_id;
        }
        // *
        //     * @param  the  to set
        function set_Title($value)
        {
            $this->Title = $value;
        }
        //     * @return the 
        function get_Title()
        {
            return $this->Title;
        }
        function set_First_name($value)
        {
            $this->First_name = $value;
        }
        //     * @return the 
        function get_First_name()
        {
            return $this->First_name;
        }
        function set_Middle_name($value)
        {
            $this->Middle_name = $value;
        }
        //     * @return the 
        function get_Middle_name()
        {
            return $this->Middle_name;
        }
        function set_Initials($value)
        {
            $this->Initials = $value;
        }
        //     * @return the 
        function get_Initials()
        {
            return $this->Initials;
        }
        function set_Surname($value)
        {
            $this->Surname = $value;
        }
        //     * @return the 
        function get_Surname()
        {
            return $this->Surname;
        }
        function set_Gender_id($value)
        {
            $this->Gender_id = $value;
        }
        //     * @return the 
        function get_Gender_id()
        {
            return $this->Gender_id;
        }
        function set_Date_of_birth($value)
        {
            $this->Date_of_birth = $value;
        }
        //     * @return the 
        function get_Date_of_birth()
        {
            return $this->Date_of_birth;
        }
        function set_MaritualStatus_id($value)
        {
            $this->MaritualStatus_id = $value;
        }
        //     * @return the 
        function get_MaritualStatus_id()
        {
            return $this->MaritualStatus_id;
        }
        function set_Marriage_id($value)
        {
            $this->Marriage_id = $value;
        }
        //     * @return the 
        function get_Marriage_id()
        {
            return $this->Marriage_id;
        }
        function set_Spouse_id($value)
        {
            $this->Spouse_id = $value;
        }
        //     * @return the 
        function get_Spouse_id()
        {
            return $this->Spouse_id;
        }
        function set_Address_id($value)
        {
            $this->Address_id = $value;
        }
        //     * @return the 
        function get_Address_id()
        {
            return $this->Address_id;
        }
        function set_Life_State_id($value)
        {
            $this->Life_State_id = $value;
        }
        //     * @return the 
        function get_Life_State_id()
        {
            return $this->Life_State_id;
        }
        function set_Language_id($value)
        {
            $this->Language_id = $value;
        }
        //     * @return the 
        function get_Language_id()
        {
            return $this->Language_id;
        }
        function set_FILEIDDOC($value)
        {
            $this->FILEIDDOC = $value;
        }
        //     * @return the 
        function get_FILEIDDOC()
        {
            return $this->FILEIDDOC;
        }
        function set_FILECONTRACT($value)
        {
            $this->FILECONTRACT = $value;
        }
        //     * @return the 
        function get_FILECONTRACT()
        {
            return $this->FILECONTRACT;
        }
        function set_FILEBANKSTATEMENT($value)
        {
            $this->FILEBANKSTATEMENT = $value;
        }
        //     * @return the 
        function get_FILEBANKSTATEMENT()
        {
            return $this->FILEBANKSTATEMENT;
        }
        function set_FILEPROOFEMP($value)
        {
            $this->FILEPROOFEMP = $value;
        }
        //     * @return the 
        function get_FILEPROOFEMP()
        {
            return $this->FILEPROOFEMP;
        }
        function set_FILEFICA($value)
        {
            $this->FILEFICA = $value;
        }
        //     * @return the 
        function get_FILEFICA()
        {
            return $this->FILEFICA;
        }
        function set_FILECREDITREP($value)
        {
            $this->FILECREDITREP = $value;
        }
        //     * @return the 
        function get_FILECREDITREP()
        {
            return $this->FILECREDITREP;
        }
        function set_FILECONSENTFORM($value)
        {
            $this->FILECONSENTFORM = $value;
        }
        //     * @return the 
        function get_FILECONSENTFORM()
        {
            return $this->FILECONSENTFORM;
        }
        function set_FILEOTHER($value)
        {
            $this->FILEOTHER = $value;
        }
        //     * @return the 
        function get_FILEOTHER()
        {
            return $this->FILEOTHER;
        }
        function set_FILEOTHER2($value)
        {
            $this->FILEOTHER2 = $value;
        }
        //     * @return the 
        function get_FILEOTHER2()
        {
            return $this->FILEOTHER2;
        }
        function set_FILEDEBITFORM($value)
        {
            $this->FILEDEBITFORM = $value;
        }
        //     * @return the 
        function get_FILEDEBITFORM()
        {
            return $this->FILEDEBITFORM;
        }
        function set_FILEOTHER3($value)
        {
            $this->FILEOTHER3 = $value;
        }
        //     * @return the 
        function get_FILEOTHER3()
        {
            return $this->FILEOTHER3;
        }
        function set_FILEOTHER4($value)
        {
            $this->FILEOTHER4 = $value;
        }
        //     * @return the 
        function get_FILEOTHER4()
        {
            return $this->FILEOTHER4;
        }
    
        // -- Load records from the database
        function  member($im_value)
        {
            $obj = new memberDAL();
            return $obj->member($im_value);
        }
        // Insert  
        function Insert($new)
        {
            $object = new memberDAL();
            return $object->Insert($new);
        }
        // Update 
        function Update($Update)
        {
            $object = new memberDAL();
            return $object->Update($Update);
        }
        // Delete 
        function Deleterecord($pkey)
        {
            $object = new memberDAL();
            return $object->Deleterecord($pkey);
        }
}


// package BLL;
// import DAL.MaritalStatusDAL;
// import php.util.List;
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// *
// *
// * @author      Nicodema Maekiso
// * @Date        19-01-2012
// * @Description  MaritalStatus Status
class MaritalStatusBLL
{
    public $MaritalStatus_id = "";
    public $MaritalStatus_name = "";
     // Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}
    // Overloaded constructor
    public function construct2($im_MaritalStatus_id, $im_MaritalStatus_name)
    {
        $this->setMaritalStatus_id($im_MaritalStatus_id);
        $this->setMaritalStatus_name($im_MaritalStatus_name);
    }
    // Overloaded constructor
    public function construct1($im_MaritalStatus_id)
    {
        $this->setMaritalStatus_id($im_MaritalStatus_id);
    }
    // Overloaded constructor
    /*function  __construct($im_MaritalStatus_name)
    {
        $this->setMaritalStatus_name($im_MaritalStatus_name);
    }*/
    // *
    //     * @return the MaritalStatusStatus_id
    function getMaritalStatus_id()
    {
        return $this->MaritalStatus_id;
    }
    // *
    //     * @param MaritalStatus_id the MaritalStatusStatus_id to set
    function setMaritalStatus_id($MaritalStatus_id)
    {
        $this->MaritalStatus_id = $MaritalStatus_id;
    }
    // *
    //     * @return the MaritalStatusStatus_name
    function getMaritalStatus_name()
    {
        return $this->MaritalStatus_name;
    }
    // *
    //     * @param MaritalStatus_name the MaritalStatusStatus_name to set
    function setMaritalStatus_name($MaritalStatus_name)
    {
        $this->MaritalStatus_name = $MaritalStatus_name;
    }
    // Overloaded constructor
    //     public MaritalStatusBLL(String im_MaritalStatus_name)
    //     {
    //         setMaritalStatus_name(im_MaritalStatus_name);
    //     }
    //
    // Get the MaritalStatus(s)- Load records from the database
    function MaritalStatus($im_value)
    {
        $MaritalStatus = new MaritalStatusDAL();
        return $MaritalStatus->MaritalStatus($im_value);
    }
    // Insert the MaritalStatus
    function InsertMaritalStatus($newMaritalStatus)
    {
        $MaritalStatus = new MaritalStatusDAL();
        return $MaritalStatus->InsertMaritalStatus($newMaritalStatus);
    }
    // Update MaritalStatus
    function UpdateMaritalStatus($UpdateMaritalStatus)
    {
        $MaritalStatus = new MaritalStatusDAL();
        return $MaritalStatus->UpdateMaritalStatus($UpdateMaritalStatus);
    }
    // Delete MaritalStatus
    function DeleteMaritalStatus($pkey)
    {
        $MaritalStatus = new MaritalStatusDAL();
        return $MaritalStatus->DeleteMaritalStatus($pkey);
    }
}// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// package BLL;
// import DAL.MarriageDAL;
// import php.util.List;
// *
// *
// * @author Tumelo
class MarriageBLL
{
    public $marriage_id = "";
    public $marriage_name = "";
	// Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}
    public function  construct2($im_marriage_id, $im_marriage_name)
    {
        $this->setMarriage_name($im_marriage_name);
        $this->setMarriage_id($im_marriage_id);
    }
    public function  construct1($im_marriage_name)
    {
        $this->setMarriage_name($im_marriage_name);
    }
    /*function  __construct($im_marriage_id)
    {
        $this->setMarriage_id($im_marriage_id);
    }
	*/
    // *
    //     * @return the marriage_id
    function getMarriage_id()
    {
        return $this->marriage_id;
    }
    // *
    //     * @param marriage_id the marriage_id to set
    function setMarriage_id($marriage_id)
    {
        $this->marriage_id = $marriage_id;
    }
    // *
    //     * @return the marriage_name
    function getMarriage_name()
    {
        return $this->marriage_name;
    }
    // *
    //     * @param marriage_name the marriage_name to set
    function setMarriage_name($marriage_name)
    {
        $this->marriage_name = $marriage_name;
    }
    // Get the Marriage(s)- Load records from the database
    function Marriage($im_value)
    {
        $marriage = new MarriageDAL();
        return $marriage->marriage($im_value);
    }
    // if
    function InsertMarriage($newMarriage)
    {
        $marriage = new MarriageDAL();
        return $marriage->InsertMarriage($newMarriage);
    }
    // Update Marriage
    function UpdateMarriage($UpdateMarriage)
    {
        $marriage = new MarriageDAL();
        return $marriage->UpdateMarriage($UpdateMarriage);
    }
    function DeleteMarriage($pkey)
    {
        $marriage = new MarriageDAL();
        return $marriage->DeleteMarriage($pkey);
    }
}// package BLL;
// import DAL.AddressDAL;
// import php.util.List;
// *
// *
// * @author      Nicodema Maekiso
// * @Date        18-01-2012
// * @Description Address
class AddressBLL
{
    private $address_id = "";
    private $suburb_id = "";
    private  $line_1 = "";
    private  $line_2 = "";
    private  $line_3 = "";
	private  $line_4 = "";
    private  $line_5 = "";
    private  $line_6 = "";
	private  $line_7 = "";
    private  $line_8 = "";
    private  $email  = "";
    private  $fax 	  = "";	
    private  $cellphone = "";
    private  $work_tel = "";
    private  $home_tel = "";
    private  $comm_preference = "";
	// Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}
    // Default constructor
    // Overloaded constructor
    public function construct1($im_address_id)
    {
        $this->setAddress_id($im_address_id);
    }
    // Overloaded constructor
    public function construct10($im_suburb_id, $im_line_1, $im_line_2, $im_line_3, $im_email, $im_fax, $im_cellphone, $im_work_tel, $im_home_tel, $im_comm_preference)
    {
        $this->setSuburb_id($im_suburb_id);
        $this->setLine_1($im_line_1);
        $this->setLine_2($im_line_2);
        $this->setLine_3($im_line_3);
        $this->setEmail($im_email);
        $this->setFax($im_fax);
        $this->setCellphone($im_cellphone);
        $this->setWork_tel($im_work_tel);
        $this->setHome_tel($im_home_tel);
        $this->setComm_preference($im_comm_preference);
    }
    // Overloaded constructor
   public function construct11($im_address_id, $im_suburb_id, $im_line_1, $im_line_2, $im_line_3, $im_email, $im_fax, $im_cellphone, $im_work_tel, $im_home_tel, $im_comm_preference)
    {
        $this->setAddress_id($im_address_id);
        $this->setSuburb_id($im_suburb_id);
        $this->setLine_1($im_line_1);
        $this->setLine_2($im_line_2);
        $this->setLine_3($im_line_3);
        $this->setEmail($im_email);
        $this->setFax($im_fax);
        $this->setCellphone($im_cellphone);
        $this->setWork_tel($im_work_tel);
        $this->setHome_tel($im_home_tel);
        $this->setComm_preference($im_comm_preference);
    }
    // *
    //     * @return the address_id
    function getAddress_id()
    {
        return $this->address_id;
    }
    function setAddress_id($address_id)
    {
        $this->address_id = $address_id;
    }
    // *
    //     * @return the suburb_id
    function getSuburb_id()
    {
        return $this->suburb_id;
    }
    // *
    //     * @param suburb_id the suburb_id to set
    function setSuburb_id($suburb_id)
    {
        $this->suburb_id = $suburb_id;
    }
    function getLine_1()
    {
        return $this->line_1;
    }
    function setLine_1($line_1)
    {
        $this->line_1 = $line_1;
    }
    // *
    //     * @return the line_2
    function getLine_2()
    {
        return $this->line_2;
    }
    // *
    //     * @param line_2 the line_2 to set
    function setLine_2($line_2)
    {
        $this->line_2 = $line_2;
    }
    // *
    //     * @return the line_3
    function getLine_3()
    {
        return $this->line_3;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_3($line_3)
    {
        $this->line_3 = $line_3;
    }
	
	    // *
    //     * @return the line_4
    function getLine_4()
    {
        return $this->line_4;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_4($line_4)
    {
        $this->line_4= $line_4;
    }
	
	    // *
    //     * @return the line_5
    function getLine_5()
    {
        return $this->line_5;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_5($line_5)
    {
        $this->line_5 = $line_5;
    }
	
	    // *
    //     * @return the line_6
    function getLine_6()
    {
        return $this->line_6;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_6($line_6)
    {
        $this->line_6 = $line_6;
    }
	
	    // *
    //     * @return the line_7
    function getLine_7()
    {
        return $this->line_7;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_7($line_7)
    {
        $this->line_7 = $line_7;
    }
	
	    // *
    //     * @return the line_8
    function getLine_8()
    {
        return $this->line_8;
    }
    // *
    //     * @param line_3 the line_3 to set
    function setLine_8($line_8)
    {
        $this->line_8 = $line_8;
    }
	
	
    // Get the Address(s)- Load records from the database
    function Address($im_value)
    {
        $Address = new AddressDAL();
        return $Address->Address($im_value);
    }
    // Insert the Address
    function InsertAddress($newAddress)
    {
        $Address = new AddressDAL();
        return $Address->InsertAddress($newAddress);
    }
    // Update Address
    function UpdateAddress($UpdateAddress)
    {
        $Address = new AddressDAL();
        return $Address->UpdateAddress($UpdateAddress);
    }
    // Delete Address
    function DeleteAddress($pkey)
    {
        $Address = new AddressDAL();
        return $Address->DeleteAddress($pkey);
    }
    // *
    //     * @return the email
    function getEmail()
    {
        return $this->email;
    }
    // *
    //     * @param email the email to set
    function setEmail($email)
    {
        $this->email = $email;
    }
    // *
    //     * @return the fax
    function getFax()
    {
        return $this->fax;
    }
    // *
    //     * @param fax the fax to set
    function setFax($fax)
    {
        $this->fax = $fax;
    }
    // *
    //     * @return the cellphone
    function getCellphone()
    {
        return $this->cellphone;
    }
    // *
    //     * @param cellphone the cellphone to set
    function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;
    }
    // *
    //     * @return the work_tel
    function getWork_tel()
    {
        return $this->work_tel;
    }
    // *
    //     * @param work_tel the work_tel to set
    function setWork_tel($work_tel)
    {
        $this->work_tel = $work_tel;
    }
    function getHome_tel()
    {
        return $this->home_tel;
    }
    // *
    //     * @param home_tel the home_tel to set
    function setHome_tel($home_tel)
    {
        $this->home_tel = $home_tel;
    }
    // *
    //     * @return the comm_preference
    function getComm_preference()
    {
        return $this->comm_preference;
    }
    // *
    //     * @param comm_preference the comm_preference to set
    function setComm_preference($comm_preference)
    {
        $this->comm_preference = $comm_preference;
    }
}// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// package BLL;
// import DAL.Life_StateDAL;
// import php.util.List;
// *
// *
// * @author Tumelo
class Life_StateBLL
{
    private $Life_State_id = "";
    private  $Life_State_name = "";
	// Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}
	
    public function construct2($im_Life_State_id, $im_Life_State_name)
    {
        $this->setLife_state_id($im_Life_State_id);
        $this->setLife_State_name($im_Life_State_name);
    }
    public function construct1($im_Life_State_name)
    {
        $this->setLife_State_name($im_Life_State_name);
    }
    /*function  __construct($im_Life_State_id)
    {
        $this->setLife_state_id($im_Life_State_id);
    }*/
    // *
    //     * @return the Life_state_id
    function getLife_state_id()
    {
        return $this->Life_State_id;
    }
    // *
    //     * @param Life_state_id the Life_state_id to set
    function setLife_state_id($Life_state_id)
    {
        $this->Life_State_id = $Life_state_id;
    }
    // *
    //     * @return the Life_State_name
    function getLife_State_name()
    {
        return $this->Life_State_name;
    }
    // *
    //     * @param Life_State_name the Life_State_name to set
    function setLife_State_name($Life_State_name)
    {
        $this->Life_State_name = $Life_State_name;
    }
    // Get the Life_state(s)- Load records from the database
    function Life_State($im_value)
    {
        $Life_State = new Life_StateDAL();
        return $Life_State->Life_State($im_value);
    }
    // Insert the Life_State
    function InsertLife_State($newLife_State)
    {
        $Life_State = new Life_StateDAL();
        return $Life_State->InsertLife_State($newLife_State);
    }
    // Update Life_State
    function UpdateLife_State($UpdateLife_State)
    {
        $Life_State = new Life_StateDAL();
        return $Life_State->UpdateLife_State($UpdateLife_State);
    }
    // Delete Life_State
    function DeleteLife_State($pkey)
    {
        $Life_State = new Life_StateDAL();
        return $Life_State->DeleteLife_State($pkey);
    }
}// *
// *
// * @author Tumelo
class LanguageBLL
{
    private $language_id = "";
    private  $language = "";
	// Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}

    public function construct1($im_language_id)
    {
        $this->setLanguage_id($im_language_id);
    }
    public function construct2($im_language_id, $im_language)
    {
        $this->setLanguage_id($im_language_id);
        $this->setLanguage($im_language);
    }
    /*function  __construct($im_language)
    {
        $this->setLanguage($im_language);
    }*/
    // *
    //     * @return the language_id
    function getLanguage_id()
    {
        return $this->language_id;
    }
    // *
    //     * @param language_id the language_id to set
    function setLanguage_id($language_id)
    {
        $this->language_id = $language_id;
    }
    // *
    //     * @return the language
    function getLanguage()
    {
        return $this->language;
    }
    // *
    //     * @param language the language to set
    function setLanguage($language)
    {
        $this->language = $language;
    }
    // Get the Language(s)- Load records from the database
    function Language($im_value)
    {
        $Language = new LanguageDAL();
        return $Language->Language($im_value);
    }
    // Insert the Language
    function InsertLanguage($newLanguage)
    {
        $Language = new LanguageDAL();
        return $Language->InsertLanguage($newLanguage);
    }
    // Update Language
    function UpdateLanguage($UpdateLanguage)
    {
        $Language = new LanguageDAL();
        return $Language->UpdateLanguage($UpdateLanguage);
    }
    function DeleteLanguage($pkey)
    {
        $Language = new LanguageDAL();
        return $Language->DeleteLanguage($pkey);
    }
}// *
// *
// * @author      Nicodema Maekiso
// * @Date        18-01-2012
// * @Description Suburb
class SuburbBLL
{
    private $Suburb_id = "";
    private  $Suburb_name = "";
    private $town_id = "";
    private $postal_code = "";
	// Default constructor
	// -- Multiple constructors  inn Php.. 22/04/2022
	// reference source : https://www.geeksforgeeks.org/how-to-mimic-multiple-constructors-in-php/
	// Count number of parameters and add '
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	function  __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		if (method_exists($this, $function = 
            'construct'.$numberOfArguments)) 
			{
				call_user_func_array(array($this, $function), $arguments);
			}
	}
    // Default constructor
    // Overloaded constructor
   public function construct4($im_Suburb_id, $im_Suburb_name, $im_town_id, $im_postal_code)
    {
        $this->setSuburb_id($im_Suburb_id);
        $this->setSuburb_name($im_Suburb_name);
        $this->setTown_id($im_town_id);
        $this->setPostal_code($im_postal_code);
    }
    // Overloaded constructor
    public function construct1($im_Suburb_id)
    {
        $this->setSuburb_id($im_Suburb_id);
    }
    // Overloaded constructor
    public function construct3($im_Suburb_name, $im_town_id, $im_postal_code)
    {
        $this->setSuburb_name($im_Suburb_name);
        $this->setTown_id($im_town_id);
        $this->setPostal_code($im_postal_code);
    }
    // *
    //     * @return the Suburb_id
    function getSuburb_id()
    {
        return $this->Suburb_id;
    }
    // *
    //     * @param Suburb_id the Suburb_id to set
    function setSuburb_id($Suburb_id)
    {
        $this->Suburb_id = $Suburb_id;
    }
    // *
    //     * @return the Suburb_name
    function getSuburb_name()
    {
        return $this->Suburb_name;
    }
    // *
    //     * @param Suburb_name the Suburb_name to set
    function setSuburb_name($Suburb_name)
    {
        $this->Suburb_name = $Suburb_name;
    }
    // Get the Suburb(s)- Load records from the database
    function Suburb($im_value, $im_town, $im_suburb_name)
    {
        $Suburb = new SuburbDAL();
        return $Suburb->Suburb($im_value, $im_town, $im_suburb_name);
    }
    // Insert the Suburb
    function InsertSuburb($newSuburb)
    {
        $Suburb = new SuburbDAL();
        return $Suburb->InsertSuburb($newSuburb);
    }
    // Update Suburb
    function UpdateSuburb($UpdateSuburb)
    {
        $Suburb = new SuburbDAL();
        return $Suburb->UpdateSuburb($UpdateSuburb);
    }
    // Delete Suburb
    function DeleteSuburb($pkey)
    {
        $Suburb = new SuburbDAL();
        return $Suburb->DeleteSuburb($pkey);
    }
    // *
    //     * @return the town_id
    function getTown_id()
    {
        return $this->town_id;
    }
    // *
    //     * @param town_id the town_id to set
    function setTown_id($town_id)
    {
        $this->town_id = $town_id;
    }
    // *
    //     * @return the postal_code
    function getPostal_code()
    {
        return $this->postal_code;
    }
    // *
    //     * @param postal_code the postal_code to set
    function setPostal_code($postal_code)
    {
        $this->postal_code = $postal_code;
    }
}// import DAL.TownDAL;
// import php.util.List;
// *
// *
// * @author      Nicodema Maekiso
// * @Date        19-01-2012
// * @Description Town
class TownBLL
{
    private $town_id = "";
    private  $town_name = "";
    private $province_id = "";
	
	// Default constructor, actual code define in the 
	// ffms_generic.php
	// easy for maintainance. 23/04/2022
	function  __construct()
	{
		include "ffms_generic.php";
	}
    // Default constructor
    // Overloaded constructor
    public function construct3($im_town_id, $im_town_name, $im_province_id)
    {
        $this->setTown_id($im_town_id);
        $this->setTown_name($im_town_name);
        $this->setProvince_id($im_province_id);
    }
    // Overloaded constructor
    public function construct1($im_town_id)
    {
        $this->setTown_id($im_town_id);
    }
    // Overloaded constructor
    public function construct2($im_town_name, $im_province_id)
    {
        $this->setTown_name($im_town_name);
        $this->setProvince_id($im_province_id);
    }
    // *
    //     * @return the town_id
    function getTown_id()
    {
        return $this->town_id;
    }
    // *
    //     * @param town_id the town_id to set
    function setTown_id($town_id)
    {
        $this->town_id = $town_id;
    }
    // *
    //     * @return the town_name
    function getTown_name()
    {
        return $this->town_name;
    }
    // *
    //     * @param town_name the town_id to set
    function setTown_name($town_name)
    {
        $this->town_name = $town_name;
    }
    // *
    //     * @return the Province_id
    function getProvince_id()
    {
        return $this->province_id;
    }
    // *
    //     * @param Province_id the Town_id to set
    function setProvince_id($province_id)
    {
        $this->province_id = $province_id;
    }
    // Get the Town(s)- Load records from the database
    function Town($im_value, $im_province, $im_town_name)
    {
        $Town = new TownDAL();
        return $Town->Town($im_value, $im_province, $im_town_name);
    }
    // Insert the Town
    function InsertTown($newTown)
    {
        $Town = new TownDAL();
        return $Town->InsertTown($newTown);
    }
    function UpdateTown($UpdateTown)
    {
        $Town = new TownDAL();
        return $Town->UpdateTown($UpdateTown);
    }
    function DeleteTown($pkey)
    {
        $Town = new TownDAL();
        return $Town->DeleteTown($pkey);
    }
}// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// package BLL;
// import DAL.ProvinceDAL;
// import php.util.List;
// *
// *
// * @author      Nicodema Maekiso
// * @Date        19-01-2012
// * @Description Province
class ProvinceBLL
{
    private $province_id = "";
    private  $province_name = "";
    private $country_id = "";
	
	// Default constructor, actual code define in the 
	// ffms_generic.php
	// easy for maintainance. 23/04/2022
	// e.g. public function construct2
	function  __construct()
	{
		include "ffms_generic.php";
	}
    // Overloaded constructor
    public function construct3($im_province_id, $im_province_name, $im_country_id)
    {
        $this->setProvince_id($im_province_id);
        $this->setProvince_name($im_province_name);
        $this->setCountry_id($im_country_id);
    }
    public function construct2($im_province_name, $im_country_id)
    {
        $this->setProvince_name($im_province_name);
        $this->setCountry_id($im_country_id);
    }
    public function construct1($im_province_id)
    {
        $this->setProvince_id($im_province_id);
    }
    // *
    //     * @return the province_id
    function getProvince_id()
    {
        return $this->province_id;
    }
    // *
    //     * @param province_id the province_id to set
    function setProvince_id($province_id)
    {
        $this->province_id = $province_id;
    }
    // *
    //     * @return the province_name
    function getProvince_name()
    {
        return $this->province_name;
    }
    // *
    //     * @param province_name the province_name to set
    function setProvince_name($province_name)
    {
        $this->province_name = $province_name;
    }
    // *
    //     * @return the country_id
    function getCountry_id()
    {
        return $this->country_id;
    }
    function setCountry_id($country_id)
    {
        $this->country_id = $country_id;
    }
    function Province($im_value, $im_country, $im_province_name)
    {
        $Province = new ProvinceDAL();
        return $Province->Province($im_value, $im_country, $im_province_name);
    }
    function InsertProvince($newProvince)
    {
        $Province = new ProvinceDAL();
        return $Province->InsertProvince($newProvince);
    }
    // Update Province
    function UpdateProvince($UpdateProvince)
    {
        $Province = new ProvinceDAL();
        return $Province->UpdateProvince($UpdateProvince);
    }
    function DeleteProvince($pkey)
    {
        $Province = new ProvinceDAL();
        return $Province->DeleteProvince($pkey);
    }
}// import DAL.CountryDAL;
// import php.util.List;
// *
// *
// * @author      Nicodema Maekiso
// * @Date        19-01-2012
// * @Description Country
class CountryBLL
{
    private $country_id = "";
    private  $country_name = "";
		// Default constructor, actual code define in the 
	// ffms_generic.php
	// easy for maintainance. 23/04/2022
	// e.g. public function construct2
	function  __construct()
	{
		include "ffms_generic.php";
	}
    // Default constructor
    // Overloaded constructor
    public function construct2($im_country_id, $im_country_name)
    {
        $this->setCountry_id($im_country_id);
        $this->setCountry_name($im_country_name);
    }
    // Overloaded constructor
    public function construct1($im_country_id)
    {
        $this->setCountry_id($im_country_id);
    }
    // Overloaded constructor
    /*function  __construct($im_country_name)
    {
        $this->setCountry_name($im_country_name);
    }*/
    // *
    //     * @return the country_id
    function getCountry_id()
    {
        return $this->country_id;
    }
    // *
    //     * @param country_id the country_id to set
    function setCountry_id($country_id)
    {
        $this->country_id = $country_id;
    }
    // *
    //     * @return the country_name
    function getCountry_name()
    {
        return $this->country_name;
    }
    // *
    //     * @param country_name the country_name to set
    function setCountry_name($country_name)
    {
        $this->country_name = $country_name;
    }
    // Get the Country(s)- Load records from the database
    function Country($im_value, $im_country_name)
    {
        $Country = new CountryDAL();
        return $Country->Country($im_value, $im_country_name);
    }
    // Insert the Country
    function InsertCountry($newCountry)
    {
        $Country = new CountryDAL();
        return $Country->InsertCountry($newCountry);
    }
    // Update Country
    function UpdateCountry($UpdateCountry)
    {
        $Country = new CountryDAL();
        return $Country->UpdateCountry($UpdateCountry);
    }
    // Delete Country
    function DeleteCountry($pkey)
    {
        $Country = new CountryDAL();
        return $Country->DeleteCountry($pkey);
    }
}// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// package DAL;
// import BLL.MarriageBLL;
// *
// *
// * @author Tumelo
// package DAL;
// import BLL.db_connection_read;
// *
// * @author      Nico
// * @Date        2012/Mar/14 10:07:47
// * @Description db_connect
class db_connect
{
	private static $dbNameB = 'fin' ;
	private static $dbHostB = 'ecashpdq.chk4kockgrje.eu-west-2.rds.amazonaws.com';
	private static $dbUsernameB = 'ecashpdq_root';
	private static $dbUserPasswordB = 'nsa{VeIsl)+h';
	
		private static $cont  = null;

		public function __construct()
		{
			exit('Init function is not allowed');
		}
    public static function db_connection()
    {

        try
        {
			self::$cont =  new PDO( "mysql:host=".self::$dbHostB.";"."dbname=".self::$dbNameB, self::$dbUsernameB, self::$dbUserPasswordB);

        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
		 return self::$cont;
    }
}
// * @author      Nicodema Maekiso
// * @Date        18-01-2012
// * @Description Address
class AddressDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    // Load the Address(s)- Load records from the database
    function Address($operation)
    {
    
        $lv_Address = array();
        $sql ="";

        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        $table= "address";
            // Get all records - Initial.
            if ($operation == 0)
            {
                $sql=("SELECT * FROM  $table");
            }
            else
            {
                $sql="SELECT * FROM  $table" . " WHERE Address_id = " . strval($operation);
            }
            $q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);	
			$data = $q->fetchAll();
             // Read the table.
			foreach($data as $row)
            {
                //echo print_r ($row);
                $lv_Address[]=$row;
                //$lv_list[] = new memberBLL($row[0], $row['Title'],$row['First_name'],$row['Middle_name'],$row['Initials'],$row['Surname'],$row['Gender_id'],$row['Date_of_birth'],$row['MaritualStatus_id'],$row['Marriage_id'],$row['Spouse_id'],$row['Address_id'],$row['Life_State_id'],$row['Language_id'],$row['FILEIDDOC'],$row['FILECONTRACT'],$row['FILEBANKSTATEMENT'],$row['FILEPROOFEMP'],$row['FILEFICA'],$row['FILECREDITREP'],$row['FILECONSENTFORM'],$row['FILEOTHER'],$row['FILEOTHER2'],$row['FILEDEBITFORM'],$row['FILEOTHER3'],$row['FILEOTHER4']);
                //$lv_Member[] =$data;
              // echo print_r ($$lv_list);

            }
       
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
          
        }
        return $lv_Address;
    }
    // Insert Address
    function InsertAddress($newAddress)
    {
        // String query = "INSERT INTO Address VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = "INSERT INTO Address (suburb_id,line_1,line_2,line_3,line_4,line_5,line_6,line_7,line_8,Member_email,fax,cellphone,work_tel,home_tel,comm_preference) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return $this->DataManipulator(1, $query, $newAddress);
    }
    // Update Address
    function UpdateAddress($UpdateAddress)
    {
        $query = "UPDATE Address SET suburb_id = ?, line_1 = ? , line_2 = ? , line_3 = ?,line_4 = ? ,line_5 = ? ,line_6 = ? ,line_7 = ?,line_8 = ?,Member_email = ?, fax = ?, cellphone = ? , work_tel = ?, home_tel = ? , comm_preference = ?  WHERE Address_id = ?";
        return $this->DataManipulator(2, $query, $UpdateAddress);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateAddress)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setInt(1,$ManipulateAddress->getSuburb_id()->getSuburb_id());
                $this->ps.setString(2,$ManipulateAddress->getLine_1());
                $this->ps.setString(3,$ManipulateAddress->getLine_2());
                $this->ps.setString(4,$ManipulateAddress->getLine_3());
				$this->ps.setString(4,$ManipulateAddress->getLine_4());
				$this->ps.setString(4,$ManipulateAddress->getLine_5());
				$this->ps.setString(4,$ManipulateAddress->getLine_6());
				$this->ps.setString(4,$ManipulateAddress->getLine_7());
				$this->ps.setString(4,$ManipulateAddress->getLine_8());
                $this->ps.setString(5,$ManipulateAddress->getEmail());
                $this->ps.setString(6,$ManipulateAddress->getFax());
                $this->ps.setString(7,$ManipulateAddress->getCellphone());
                $this->ps.setString(8,$ManipulateAddress->getWork_tel());
                $this->ps.setString(9,$ManipulateAddress->getHome_tel());
                $this->ps.setString(10,$ManipulateAddress->getComm_preference());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setInt(1,$ManipulateAddress->getSuburb_id()->getSuburb_id());
                $this->ps.setString(2,$ManipulateAddress->getLine_1());
                $this->ps.setString(3,$ManipulateAddress->getLine_2());
                $this->ps.setString(4,$ManipulateAddress->getLine_3());
				$this->ps.setString(4,$ManipulateAddress->getLine_4());
				$this->ps.setString(4,$ManipulateAddress->getLine_5());
				$this->ps.setString(4,$ManipulateAddress->getLine_6());
				$this->ps.setString(4,$ManipulateAddress->getLine_7());
				$this->ps.setString(4,$ManipulateAddress->getLine_8());
                $this->ps.setString(5,$ManipulateAddress->getEmail());
                $this->ps.setString(6,$ManipulateAddress->getFax());
                $this->ps.setString(7,$ManipulateAddress->getCellphone());
                $this->ps.setString(8,$ManipulateAddress->getWork_tel());
                $this->ps.setString(9,$ManipulateAddress->getHome_tel());
                $this->ps.setString(10,$ManipulateAddress->getComm_preference());
                $this->ps.setInt(11,$ManipulateAddress->getAddress_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Address
    function DeleteAddress($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Address where Address_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}// else
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// package DAL;
// import BLL.*;
// *
// *
// * @author Tumelo
class memberDAL
{
    // Declarations.
       // Declarations.
       public $connect;
       public $count;
       // Default constructor.
    // Default constructor.
    function memberDAL()
    {
    }
    // Load the Member(s)- Load records from the database
    function member($operation)
    {
                $lv_list = array();
                $sql ="";
   
        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        $table= "member";
            $table2= "member_policy";
            $table3= "address";
            if ($operation == 0)
            {
                $sql="SELECT * FROM (($table AS cl_to inner join $table2  AS cl_to2  on cl_to.Member_id = cl_to2.Member_id)inner join $table3 ON cl_to.Address_id = $table3.Address_id)";
                
             
            }
            else
            {

                $sql = "SELECT * FROM $table";
               
            }
			
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);	
			$data = $q->fetchAll();
            
            // Read the table.
			foreach($data as $row)
            {
                //echo print_r ($row);
                $lv_list[]=$row;
                //$lv_list[] = new memberBLL($row[0], $row['Title'],$row['First_name'],$row['Middle_name'],$row['Initials'],$row['Surname'],$row['Gender_id'],$row['Date_of_birth'],$row['MaritualStatus_id'],$row['Marriage_id'],$row['Spouse_id'],$row['Address_id'],$row['Life_State_id'],$row['Language_id'],$row['FILEIDDOC'],$row['FILECONTRACT'],$row['FILEBANKSTATEMENT'],$row['FILEPROOFEMP'],$row['FILEFICA'],$row['FILECREDITREP'],$row['FILECONSENTFORM'],$row['FILEOTHER'],$row['FILEOTHER2'],$row['FILEDEBITFORM'],$row['FILEOTHER3'],$row['FILEOTHER4']);
                //$lv_Member[] =$data;
              // echo print_r ($$lv_list);

            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
       //echo print_r($lv_list);

        return  $lv_list;
    }
    // Insert Member
    function InsertMember($newMember)
    {
        $query = "INSERT INTO Member (Member_id,title ,First_name, Middle_name, Initials, Surname, Gender_id, Date_of_birth, MaritualStatus_id, Marriage_id, Spouse_id, Address_id, Life_State_id,language_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
        return $this->DataManipulator(1, $query, $newMember);
    }
    // -- Update Member Life state.
    function UpdateMemberLifeState($UpdateMember)
    {
        $query = "UPDATE member SET Life_State_id = ? WHERE Member_id = ?";
        return $this->DataManipulator(3, $query, $UpdateMember);
    }
    // Update Member
    function UpdateMember($UpdateMember)
    {
        $query = "UPDATE member SET  Title = ?,First_name = ?,Middle_name = ?,Initials = ?,Surname = ?,Gender_id = ?,Date_of_birth = ?,MaritualStatus_id = ?,Marriage_id = ?,Spouse_id = ?,Address_id = ?,Life_State_id = ?,language_id = ? WHERE Member_id = ?";
        return $this->DataManipulator(2, $query, $UpdateMember);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateMember)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Insert
            if ($operation == 1)
            {
			  /*$this->ps.setLong(1,$ManipulateMember->getMember_id());
                $this->ps.setString(2,$ManipulateMember->getTitle());
                $this->ps.setString(3,$ManipulateMember->getFirst_name());
                $this->ps.setString(4,$ManipulateMember->getMiddle_name());
                $this->ps.setString(5,$ManipulateMember->getInitials());
                $this->ps.setString(6,$ManipulateMember->getSurname());
                $this->ps.setInt(7,$ManipulateMember->getGender()->getGender_id());
                $this->ps.setDate(8,$this->formatDate($ManipulateMember->getDate_of_birth()));
                $this->ps.setInt(9,$ManipulateMember->getMaritalStatus()->getMaritalStatus_id());
                $this->ps.setInt(10,$ManipulateMember->getMarriage()->getMarriage_id());
                $this->ps.setLong(11,$ManipulateMember->getSpouse_id());
                $this->ps.setInt(12,$ManipulateMember->getAddress()->getAddress_id());
                $this->ps.setInt(13,$ManipulateMember->getLife_State()->getLife_state_id());
                $this->ps.setInt(14,$ManipulateMember->getLanguage_id()->getLanguage_id());
                $this->ps.executeUpdate();
                $this->ps.close();*/
				$q = $this->connect->prepare($query);
				// Coverting object to an array
				$objectArray = (array)$ManipulateMember;
				$q->execute(array($page,$objectArray));
            }
            else
                if ($operation == 3)
                {
                    /*$this->ps.setInt(1,$ManipulateMember->getLife_State()->getLife_state_id());
                    $this->ps.setLong(2,$ManipulateMember->getMember_id());
                    $this->ps.executeUpdate();
                    $this->ps.close();*/
					$q = $this->connect->prepare($query);
					// Coverting object to an array
					$objectArray = (array)$ManipulateMember;
					$q->execute(array($page,$objectArray));
                }
                else
                {
                    /*$this->ps.setString(1,$ManipulateMember->getTitle());
                    $this->ps.setString(2,$ManipulateMember->getFirst_name());
                    $this->ps.setString(3,$ManipulateMember->getMiddle_name());
                    $this->ps.setString(4,$ManipulateMember->getInitials());
                    $this->ps.setString(5,$ManipulateMember->getSurname());
                    $this->ps.setInt(6,$ManipulateMember->getGender()->getGender_id());
                    $this->ps.setDate(7,$this->formatDate($ManipulateMember->getDate_of_birth()));
                    $this->ps.setInt(8,$ManipulateMember->getMaritalStatus()->getMaritalStatus_id());
                    $this->ps.setInt(9,$ManipulateMember->getMarriage()->getMarriage_id());
                    $this->ps.setLong(10,$ManipulateMember->getSpouse_id());
                    $this->ps.setInt(11,$ManipulateMember->getAddress()->getAddress_id());
                    $this->ps.setInt(12,$ManipulateMember->getLife_State()->getLife_state_id());
                    $this->ps.setInt(13,$ManipulateMember->getLanguage_id()->getLanguage_id());
                    $this->ps.setLong(14,$ManipulateMember->getMember_id());
                    $this->ps.executeUpdate();
                    $this->ps.close();*/
					$q = $this->connect->prepare($query);
					// Coverting object to an array
					$objectArray = (array)$ManipulateMember;
					$q->execute(array($page,$objectArray));
                }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    function DeleteMember($pkey)
    {
        $lv_return = "";
        try
        {
			// -- 2022
			$this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$query = "DELETE FROM member WHERE Member_id = ?";
			return $this->DataManipulator(4, $query, $UpdateMember);

            /*$this->connect = (Connection)db_connect::db_connection();
            $this->ps = (PreparedStatement)$this->connect.prepareStatement("DELETE FROM Member where Member_id = ?");
            $this->ps.setLong(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();*/
			
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    function formatDate($date_of_birth)
    {
        $s = array();
        return $s;
    }
}

// *
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description Gender
class GenderDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function GenderDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function Gender($operation)
    {
        // Temporary list.
        $lv_gender = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0)
            {
                $this->s.executeQuery("SELECT gender_id,gender_name FROM Gender");
            }
            else
            {
                $this->s.executeQuery("SELECT gender_id,gender_name FROM Gender" . " WHERE gender_id = " . strval($operation));
            }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_gender,new GenderBLL($this->rs.getInt("gender_id"), $this->rs.getString("gender_name")));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_gender;
    }
    function InsertGender($newGender)
    {
        $query = "INSERT INTO gender (gender_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $newGender);
    }
    function UpdateGender($UpdateGender)
    {
        $query = "UPDATE gender SET gender_name = ? WHERE gender_id = ?";
        return $this->DataManipulator(2, $query, $UpdateGender);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateGender)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateGender->getGender_name());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateGender->getGender_name());
                $this->ps.setInt(2,$ManipulateGender->getGender_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Gender
    function DeleteGender($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM gender where gender_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}// *
// *
// * @author Tumelo
class MaritalStatusDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function MaritalStatusDAL()
    {
    }
    // Load the MaritalStatus(s)- Load records from the database
    function MaritalStatus($operation)
    {
        // Temporary list.
        $lv_list = array();
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM maritualstatus";
		    }
            else
            {
                $sql = "SELECT * FROM maritualstatus" . " WHERE MaritualStatus_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new MaritalStatusBLL($row["MaritualStatus_id"], $row["MaritualStatus_name"]);
            }

        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
    
        }
        return $lv_list;
    }
    // Insert MaritalStatus
    function InsertMaritalStatus($newMaritalStatus)
    {
        $query = "INSERT INTO MaritualStatus (MaritualStatus_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $newMaritalStatus);
    }
    // Update MaritalStatus
    function UpdateMaritalStatus($UpdateMaritalStatus)
    {
        $query = "UPDATE MaritualStatus SET MaritualStatus_name = ? WHERE MaritualStatus_id = ?";
        return $this->DataManipulator(2, $query, $UpdateMaritalStatus);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateMaritalStatus)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateMaritalStatus->getMaritalStatus_name());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateMaritalStatus->getMaritalStatus_name());
                $this->ps.setInt(2,$ManipulateMaritalStatus->getMaritalStatus_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete MaritalStatus
    function DeleteMaritalStatus($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM MaritualStatus where MaritualStatus_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
class MarriageDAL
{
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    function marriage($operation)
    {
        // Temporary list.
        $lv_list = array();
        try
        {
           $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM marriage";
		    }
            else
            {
                $sql = "SELECT * FROM marriage" . " WHERE Marriage_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new MarriageBLL($row["Marriage_id"], $row["Marriage_name"]);
            }

        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
    
        }
        return $lv_list;
    }
    // Data Manipulator.
    function ManipulateDataMarriage($operation, $query, $ManipulateMarriage)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateMarriage->getMarriage_name());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateMarriage->getMarriage_name());
                $this->ps.setInt(2,$ManipulateMarriage->getMarriage_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    function InsertMarriage($newMarriage)
    {
        $query = "INSERT INTO Marriage (marriage_name) VALUES(?)";
        return $this->ManipulateDataMarriage(1, $query, $newMarriage);
    }
    function UpdateMarriage($UpdateMarriage)
    {
        $query = "UPDATE marriage SET marriage_name = ? WHERE marriage_id = ?";
        return $this->ManipulateDataMarriage(2, $query, $UpdateMarriage);
    }
    function DeleteMarriage($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM marriage WHERE marriage_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
class LanguageDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    function Language($operation)
    {
                $lv_Language = array();
        try
        {
            /*$this->connect = (Connection)db_connect::db_connection();
            $this->s = (Statement)$this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0)
            {
                $this->s.executeQuery("SELECT Language_id,Language FROM Language");
            }
            else
            {
                $this->s.executeQuery("SELECT Language_id,Language FROM Language" . " WHERE Language_id = " . strval($operation));
            }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Language,new LanguageBLL($this->rs.getInt("Language_id"), $this->rs.getString("Language")));
            }*/
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Language;
    }
    function InsertLanguage($newLanguage)
    {
        $query = "INSERT INTO Language (Language) VALUES(?)";
        return $this->DataManipulator(1, $query, $newLanguage);
    }
    function UpdateLanguage($UpdateLanguage)
    {
        $query = "UPDATE Language SET Language = ? WHERE Language_id = ?";
        return $this->DataManipulator(2, $query, $UpdateLanguage);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateLanguage)
    {
        $lv_return = "";
        try
        {
            /*$this->connect = (Connection)db_connect::db_connection();
            $this->ps = (PreparedStatement)$this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateLanguage->getLanguage());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateLanguage->getLanguage());
                $this->ps.setInt(2,$ManipulateLanguage->getLanguage_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }*/
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Language
    function DeleteLanguage($pkey)
    {
        $lv_return = "";
        /*try
        {
            $this->connect = (Connection)db_connect::db_connection();
            $this->ps = (PreparedStatement)$this->connect.prepareStatement("DELETE FROM Language where Language_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }*/
        return $lv_return;
    }
}// *
// *
// * @author Tumelo
class Life_StateDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function Life_StateDAL()
    {
    }
    // Load the Life_State(s)- Load records from the database
    function Life_State($operation)
    {
        // Temporary list.
        $lv_Life_State = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0)
            {
                $this->s.executeQuery("SELECT Life_State_id,Life_State_name FROM Life_State");
            }
            else
            {
                $this->s.executeQuery("SELECT Life_State_id,Life_State_name FROM Life_State" . " WHERE Life_State_id = " . strval($operation));
            }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Life_State,new Life_StateBLL($this->rs.getInt("Life_State_id"), $this->rs.getString("Life_State_name")));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Life_State;
    }
    // Insert Life_State
    function InsertLife_State($newLife_State)
    {
        $query = "INSERT INTO Life_State (Life_State_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $newLife_State);
    }
    // Update Life_State
    function UpdateLife_State($UpdateLife_State)
    {
        $query = "UPDATE Life_State SET Life_State_name = ? WHERE Life_State_id = ?";
        return $this->DataManipulator(2, $query, $UpdateLife_State);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateLife_State)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateLife_State->getLife_State_name());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateLife_State->getLife_State_name());
                $this->ps.setInt(2,$ManipulateLife_State->getLife_state_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Life_State
    function DeleteLife_State($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Life_State where Life_State_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}


class CountryDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function CountryDAL()
    {
    }
    // Load the Country(s)- Load records from the database
    function Country($operation, $country_name)
    {
        // Temporary list.
        $lv_Country = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0 && (strcmp("",$country_name)==0))
            {
                $this->s.executeQuery("SELECT Country_id,Country_name FROM Country");
            }
            else
                if ($operation == 0 && !(strcmp("",$country_name)==0))
                {
                    $this->s.executeQuery("SELECT `Country_id`,`Country_name` FROM `Country` WHERE `Country_name` = \'" . $country_name . "\'");
                }
                else
                {
                    $this->s.executeQuery("SELECT Country_id,Country_name FROM Country" . " WHERE Country_id = " . strval($operation));
                }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Country,new CountryBLL($this->rs.getInt("Country_id"), $this->rs.getString("Country_name")));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Country;
    }
    // Insert Country
    function InsertCountry($newCountry)
    {
        $query = "INSERT INTO Country (Country_id, Country_name) VALUES(?,?)";
        return $this->DataManipulator(1, $query, $newCountry);
    }
    // Update Country
    function UpdateCountry($UpdateCountry)
    {
        $query = "UPDATE Country SET Country_name = ? WHERE Country_id = ?";
        return $this->DataManipulator(2, $query, $UpdateCountry);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateCountry)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setInt(1,$ManipulateCountry->getCountry_id());
                $this->ps.setString(2,$ManipulateCountry->getCountry_name());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateCountry->getCountry_name());
                $this->ps.setInt(2,$ManipulateCountry->getCountry_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    function DeleteCountry($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Country where Country_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}// *
// *
// * @author      Nicodema Maekiso
// * @Date        18-01-2012
// * @Description Suburb
class SuburbDAL
{
    // -- Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    // Load the Suburb(s)- Load records from the database
    function Suburb($operation, $town, $suburb_name)
    {
        // Temporary list.
        $lv_Suburb = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0 && $town == 0 && (strcmp("",$suburb_name)==0))
            {
                $this->s.executeQuery("SELECT * FROM Suburb");
            }
            else
                if ($operation == 0 && ($town != 0 || !(strcmp(" ",$suburb_name)==0)))
                {
                    $this->s.executeQuery("SELECT * FROM Suburb" . " WHERE town_id = " . strval($town) . " OR surburb_name = \'" . $suburb_name . "\'");
                }
                else
                {
                    $this->s.executeQuery("SELECT * FROM Suburb" . " WHERE Suburb_id = " . strval($operation));
                }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Suburb,new SuburbBLL($this->rs.getInt("Suburb_id"), $this->rs.getString("surburb_name"), new TownBLL($this->rs.getInt("town_id")), $this->rs.getInt("Postal_code")));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Suburb;
    }
    // Insert Suburb
    function InsertSuburb($newSuburb)
    {
                $query = "INSERT INTO Suburb (surburb_name,town_id,Postal_code) VALUES(?,?,?)";
        return $this->DataManipulator(1, $query, $newSuburb);
    }
    // Update Suburb
    function UpdateSuburb($UpdateSuburb)
    {
        $query = "UPDATE Suburb SET surburb_name = ?, town_id =? , Postal_code = ?  WHERE Suburb_id = ?";
        return $this->DataManipulator(2, $query, $UpdateSuburb);
    }
    function DataManipulator($operation, $query, $ManipulateSuburb)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateSuburb->getSuburb_name());
                $this->ps.setInt(2,$ManipulateSuburb->getTown_id()->getTown_id());
                $this->ps.setInt(3,$ManipulateSuburb->getPostal_code());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateSuburb->getSuburb_name());
                $this->ps.setInt(2,$ManipulateSuburb->getTown_id()->getTown_id());
                $this->ps.setInt(3,$ManipulateSuburb->getPostal_code());
                $this->ps.setInt(4,$ManipulateSuburb->getSuburb_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Suburb
    function DeleteSuburb($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Suburb where Suburb_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
class TownDAL
{
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Load the Town(s)- Load records from the database
    function Town($operation, $province, $town_name)
    {
                $lv_Town = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0 && $province == 0 && (strcmp("",$town_name)==0))
            {
                $this->s.executeQuery("SELECT * FROM Town");
            }
            else
                if ($operation == 0 && ($province != 0 || !(strcmp(" ",$town_name)==0)))
                {
                    $this->s.executeQuery("SELECT * FROM Town" . " WHERE province_id = " . strval($province) . " OR    town_name = \'" . $town_name . "\'");
                }
                else
                {
                    $this->s.executeQuery("SELECT * FROM Town" . " WHERE Town_id = " . strval($operation));
                }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Town,new TownBLL($this->rs.getInt("Town_id"), $this->rs.getString("Town_name"), new ProvinceBLL($this->rs.getInt("province_id"))));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Town;
    }
    // Insert Town
    function InsertTown($newTown)
    {
        // String query = "INSERT INTO Town VALUES(?,?,?)";
        $query = "INSERT INTO Town(Town_name,Province_id) VALUES(?,?)";
        return $this->DataManipulator(1, $query, $newTown);
    }
    // Update Town
    function UpdateTown($UpdateTown)
    {
        $query = "UPDATE Town SET Town_name = ? , province_id  = ? WHERE Town_id = ?";
        return $this->DataManipulator(2, $query, $UpdateTown);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $ManipulateTown)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateTown->getTown_name());
                $this->ps.setInt(2,$ManipulateTown->getProvince_id()->getProvince_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateTown->getTown_name());
                $this->ps.setInt(2,$ManipulateTown->getProvince_id()->getProvince_id());
                $this->ps.setInt(3,$ManipulateTown->getTown_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Town
    function DeleteTown($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Town where Town_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}// *
// *
// * @author      Nicodema Maekiso
// * @Date        19-01-2012
// * @Description Province
class ProvinceDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    // Load the Province(s)- Load records from the database
    function Province($operation, $country, $province_name)
    {
        // Temporary list.
        $lv_Province = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0 && $country == 0 && (strcmp("",$province_name)==0))
            {
                $this->s.executeQuery("SELECT * FROM Province");
            }
            else
                if ($operation == 0 && ($country != 0 || !(strcmp(" ",$province_name)==0)))
                {
                    $this->s.executeQuery("SELECT * FROM `Province` WHERE `Country_id` = " . strval($country) . " OR `Provine_name` = \'" . $province_name . "\'");
                }
                else
                {
                    $this->s.executeQuery("SELECT * FROM Province" . " WHERE Province_id = " . strval($operation));
                }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_Province,new ProvinceBLL($this->rs.getInt("Province_id"), $this->rs.getString("Provine_name"), new CountryBLL($this->rs.getInt("country_id"))));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_Province;
    }
    // Insert Province
    function InsertProvince($newProvince)
    {
        $query = "INSERT INTO Province (provine_name,country_id) VALUES(?,?)";
        return $this->DataManipulator(1, $query, $newProvince);
    }
    // Update Province
    function UpdateProvince($UpdateProvince)
    {
        $query = "UPDATE Province SET Provine_name = ?, country_id =?  WHERE Province_id = ?";
        return $this->DataManipulator(2, $query, $UpdateProvince);
    }
    function DataManipulator($operation, $query, $ManipulateProvince)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            if ($operation == 1)
            {
                $this->ps.setString(1,$ManipulateProvince->getProvince_name());
                $this->ps.setInt(2,$ManipulateProvince->getCountry_id()->getCountry_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateProvince->getProvince_name());
                                $this->ps.setInt(2,$ManipulateProvince->getCountry_id()->getCountry_id());
                $this->ps.setInt(3,$ManipulateProvince->getProvince_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete Province
    function DeleteProvince($pkey)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM Province where Province_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}// *
// *
// * @author Tumelo
class db_connection_read
{
    private  $username = "";
    private  $password = "";
    private  $ipaddress = "";
    private  $dbname = "";
    private  $input;
    function  __construct()
    {
        $this->load_connection_setting();
    }
    function load_connection_setting()
    {
        try
        {
            $in = array();
            $input_temp;
            $end_index = 0;
            while ($in.ready())
            {
                $this->setInput($in.readLine());
                if (substr($this->getInput(), 0, len("String") ) == "String")
                {
                    // -- User name
                    $input_temp = substr($this->getInput(),22,30 - 22);
                    if ((strcmp($input_temp,"userName")==0))
                    {
                        $end_index = strpos($this->getInput(),";");
                        $this->setUsername(substr($this->getInput(),34,$end_index - 1 - 34));
                    }
                    // -- Password    
                    $this->setInput($in.readLine());
                    $input_temp = substr($this->getInput(),22,30 - 22);
                    if ((strcmp($input_temp,"password")==0))
                    {
                        $end_index = strpos($this->getInput(),";");
                        $this->setPassword(substr($this->getInput(),34,$end_index - 1 - 34));
                    }
                    // -- Ip address and database name
                    $this->setInput($in.readLine());
                    $input_temp = substr($this->getInput(),22,25 - 22);
                    if ((strcmp($input_temp,"url")==0))
                    {
                        $end_index = strpos($this->getInput(),";");
                        $this->setIpaddress(substr($this->getInput(),strpos($this->getInput(),"//") + 2,$end_index - 1 - strpos($this->getInput(),"//") + 2));
                        $this->setDbname(explode("/",$this->getIpaddress())[1]);
                        $this->setIpaddress(explode("/",$this->getIpaddress())[0]);
                    }
                }
            }
            $in.close();
        } catch ( Exception $e) {
            echo $e.getMessage();
        }
    }
    // *
    //     * @return the username
    function getUsername()
    {
        return $this->username;
    }
    // *
    //     * @param username the username to set
    function setUsername($username)
    {
        $this->username = $username;
    }
    // *
    //     * @return the password
    function getPassword()
    {
        return $this->password;
    }
    // *
    //     * @param password the password to set
    function setPassword($password)
    {
        $this->password = $password;
    }
    // *
    //     * @return the ipaddress
    function getIpaddress()
    {
        return $this->ipaddress;
    }
    // *
    //     * @param ipaddress the ipaddress to set
    function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;
    }
    // *
    //     * @return the dbname
    function getDbname()
    {
        return $this->dbname;
    }
    // *
    //     * @param dbname the dbname to set
    function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }
    // *
    //     * @return the input
    function getInput()
    {
        return $this->input;
    }
    // *
    //     * @param input the input to set
    function setInput($input)
    {
        $this->input = $input;
    }
}// *
// *
// * @author Tumelo
class global_letterhead
{
    // -- Absolute path determination
   /* public static  $SAMPLES_DIR_NAME = "";
    public static $samplesDir = array();
    // -- Create the Trial Registry Key and value.
    //    private static  int trialCount = 1;
    private static  $value = "";
    private static  $Key = "SOFTWARE";
    private static  $KeyVariable = "cXY";
    public  static $customeraddress = new CustomerAddressBLL("0");
    private static $CustomerAddress = global_letterhead::$customeraddress->CustomerAddress(0);
    public  $text;
    public static $name = global_letterhead::$CustomerAddress[0]->getName();
    public static $businessName = global_letterhead::$CustomerAddress[0]->getBusinessName();
    public static $address = global_letterhead::$CustomerAddress[0]->getAddress1();
    public static $address2 = global_letterhead::$CustomerAddress[0]->getAddress2();
    public static $telephone = "Telephone : " . global_letterhead::$CustomerAddress[0]->getTelephone();
    public static $email = "Email : " . global_letterhead::$CustomerAddress[0]->getEmail();
    public static $fax = "facsimile : " . global_letterhead::$CustomerAddress[0]->getFax();
    public static $copyR = global_letterhead::$CustomerAddress[0]->getCopyR1();
    public static $copyR1 = global_letterhead::$CustomerAddress[0]->getCopyR2();
    public static $copyR2 = global_letterhead::$CustomerAddress[0]->getCopyR3() . "\n";
    public static $logo = global_letterhead::$CustomerAddress[0]->getLogo();
    public static $lincenseKey = global_letterhead::$CustomerAddress[0]->getLincenseKey();
    public static $f1 = new Font();
    public static $TrialVersion = 12;
    public static $TrialLimit = 10;
    // -- Trial Version is limited to 10 use ONLY.
    // -- User role for menu limitation.
    public static $user_role = 1;
    // -- Read Customer Address table.*/
    function TrialVersion() //throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        $trialCount = 1;
        // -- Read the Vogsphere Trial value.
        global_letterhead::$value = NULL;
        // WinRegistry.readString(WinRegistry.HKEY_CURRENT_USER,Key,KeyVariable);
        // -- Check if it exist.
        if (global_letterhead::$value != NULL)
        {
            if (global_letterhead::$value != NULL)
            {
                $trialCount += intval(global_letterhead::$value);
            }
        }
        // -- Remove trial trace.
        // WinRegistry.createKey(
        //           WinRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Vogsphere");
        //          // -- Create & Update the Trial version count.
        //           WinRegistry.createKey(
        //           WinRegistry.HKEY_CURRENT_USER, Key);
        //           WinRegistry.writeStringValue(
        //           WinRegistry.HKEY_CURRENT_USER, Key,KeyVariable, Integer.toString(trialCount));
        // -- Trial Version audit check.
        global_letterhead::$TrialVersion = $trialCount;
    }
}// *
// *
// * @author Tumelo
class CustomerAddressBLL
{
    private $customeraddress_id;
    private $name;
    private $businessName;
    private $address1;
    private $address2;
    private $city;
    private $postal_code;
    private $telephone;
    private $fax;
    private $email;
    private $cellphone;
    private $copyR1;
    private $copyR2;
    private $copyR3;
    private $logo;
    private $lincenseKey;
	// public function construct+(count parameters)' 
	// e.g. public function construct2
	function  __construct()
	{
		include "ffms_generic.php";
	}
	
    public function construct1($initial)
    {
        $this->setLincenseKey($initial);
    }
    public function construct16($im_customeraddress_id, $im_name, $im_businessName, $im_address1, $im_address2, $im_city, $im_postal_code, $im_telephone, $im_fax, $im_email, $im_cellphone, $im_copyR1, $im_copyR2, $im_copyR3, $im_logo, $im_licenseKey)
    {
        $this->setCustomeraddress_id($im_customeraddress_id);
        $this->setName($im_name);
        $this->setBusinessName($im_businessName);
        $this->setAddress1($im_address1);
        $this->setAddress2($im_address2);
        $this->setCity($im_city);
        $this->setPostal_code($im_postal_code);
        $this->setTelephone($im_telephone);
        $this->setFax($im_fax);
        $this->setEmail($im_email);
        $this->setCellphone($im_cellphone);
        $this->setCopyR1($im_copyR1);
        $this->setCopyR2($im_copyR2);
        $this->setCopyR3($im_copyR3);
        $this->setLogo($im_logo);
        $this->setLincenseKey($im_licenseKey);
    }
    // -- Update License Key.
    public function construct2($im_customeraddress_id, $im_lincenseKey)
    {
        $this->setCustomeraddress_id($im_customeraddress_id);
        $this->setLincenseKey($im_lincenseKey);
    }
    // *
    //     * @return the name
    function getName()
    {
        return $this->name;
    }
    // *
    //     * @param name the name to set
    function setName($name)
    {
        $this->name = $name;
    }
    // *
    //     * @return the businessName
    function getBusinessName()
    {
        return $this->businessName;
    }
    // *
    //     * @return the address1
    function getAddress1()
    {
        return $this->address1;
    }
    // *
    //     * @return the address2
    function getAddress2()
    {
        return $this->address2;
    }
    // *
    //     * @return the city
    function getCity()
    {
        return $this->city;
    }
    // *
    //     * @return the postal_code
    function getPostal_code()
    {
        return $this->postal_code;
    }
    // *
    //     * @return the telephone
    function getTelephone()
    {
        return $this->telephone;
    }
    // *
    //     * @return the fax
    function getFax()
    {
        return $this->fax;
    }
    // *
    //     * @return the email
    function getEmail()
    {
        return $this->email;
    }
    // *
    //     * @return the copyR1
    function getCopyR1()
    {
        return $this->copyR1;
    }
    // *
    //     * @return the copyR2
    function getCopyR2()
    {
        return $this->copyR2;
    }
    // *
    //     * @return the copyR3
    function getCopyR3()
    {
        return $this->copyR3;
    }
    // *
    //     * @return the logo
    function getLogo()
    {
        return $this->logo;
    }
    // *
    //     * @return the lincenseKey
    function getLincenseKey()
    {
        return $this->lincenseKey;
    }
    // *
    //     * @param businessName the businessName to set
    function setBusinessName($businessName)
    {
        $this->businessName = $businessName;
    }
    // *
    //     * @param address1 the address1 to set
    function setAddress1($address1)
    {
        $this->address1 = $address1;
    }
    // *
    //     * @param address2 the address2 to set
    function setAddress2($address2)
    {
        $this->address2 = $address2;
    }
    // *
    //     * @param city the city to set
    function setCity($city)
    {
        $this->city = $city;
    }
    // *
    //     * @param postal_code the postal_code to set
    function setPostal_code($postal_code)
    {
        $this->postal_code = $postal_code;
    }
    // *
    //     * @param telephone the telephone to set
    function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }
    // *
    //     * @param fax the fax to set
    function setFax($fax)
    {
        $this->fax = $fax;
    }
    // *
    //     * @param email the email to set
    function setEmail($email)
    {
        $this->email = $email;
    }
    // *
    //     * @param copyR1 the copyR1 to set
    function setCopyR1($copyR1)
    {
        $this->copyR1 = $copyR1;
    }
    // *
    //     * @param copyR2 the copyR2 to set
    function setCopyR2($copyR2)
    {
        $this->copyR2 = $copyR2;
    }
    // *
    //     * @param copyR3 the copyR3 to set
    function setCopyR3($copyR3)
    {
        $this->copyR3 = $copyR3;
    }
    // *
    //     * @param logo the logo to set
    function setLogo($logo)
    {
        $this->logo = $logo;
    }
    // *
    //     * @param lincenseKey the lincenseKey to set
    function setLincenseKey($lincenseKey)
    {
        $this->lincenseKey = $lincenseKey;
    }
    // *
    //     * @return the customeraddress_id
    function getCustomeraddress_id()
    {
        return $this->customeraddress_id;
    }
    // *
    //     * @param customeraddress_id the customeraddress_id to set
    function setCustomeraddress_id($customeraddress_id)
    {
        $this->customeraddress_id = $customeraddress_id;
    }
    // Get the CustomerAddress(s)- Load records from the database
    function CustomerAddress($im_value)
    {
        $customeraddress = new CustomerAddressDAL();
        return $customeraddress->CustomerAddress($im_value);
    }
    // Update CustomerAddress
    function UpdateCustomerAddress($UpdateCustomerAddress, $isLicenseKey)
    {
        $customeraddress = new CustomerAddressDAL();
        return $customeraddress->UpdateCustomerAddress($UpdateCustomerAddress, $isLicenseKey);
    }
    // *
    //     * @return the cellphone
    function getCellphone()
    {
        return $this->cellphone;
    }
    // *
    //     * @param cellphone the cellphone to set
    function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;
    }
}class Font
{
}// *
// *
// * @author Tumelo
class CustomerAddressDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Load the Gender(s)- Load records from the database
    function CustomerAddress($operation)
    {
        // Temporary list.
        $lv_customeraddress = array();
        try
        {
            $this->connect = db_connect::db_connection();
            $this->s = $this->connect.createStatement();
            // Get all records - Initial.
            if ($operation == 0)
            {
                $this->s.executeQuery("SELECT * FROM Customer_address");
            }
            else
            {
                $this->s.executeQuery("SELECT * FROM Customer_address" . " WHERE idCustomer_address = " . strval($operation));
            }
            $this->rs = $this->s.getResultSet();
            // Read the table.
            while ($this->rs.next())
            {
                 array_push($lv_customeraddress,new CustomerAddressBLL($this->rs.getInt("idCustomer_address"), $this->rs.getString("name"), $this->rs.getString("business_name"), $this->rs.getString("address1"), $this->rs.getString("address2"), $this->rs.getString("city"), $this->rs.getString("postal_code"), $this->rs.getString("telephone"), $this->rs.getString("email"), $this->rs.getString("cellphone"), $this->rs.getString("fax"), $this->rs.getString("copyR1"), $this->rs.getString("copyR2"), $this->rs.getString("copyR3"), $this->rs.getString("logo"), $this->rs.getString("licensekey")));
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_customeraddress;
    }
    // Update customeraddress
    function UpdateCustomerAddress($UpdateCustomerAddress, $isLincenseKey)
    {
        $query = "";
        if (!$isLincenseKey)
        {
            $query = "UPDATE customer_address SET name = ?,business_name = ?,address1 = ?,address2 = ?,city = ?,postal_code = ?,telephone = ?,fax = ?,email = ?,cellphone = ?,copyR1 = ?,copyR2 = ?,copyR3 = ?,logo = ? WHERE idCustomer_address = ?";
        }
        else
        {
            $query = "UPDATE customer_address SET licensekey = ? WHERE idCustomer_address = ?";
        }
        return $this->DataManipulator($isLincenseKey, $query, $UpdateCustomerAddress);
    }
    // Data Manipulator.
    function DataManipulator($isLincenseKey, $query, $ManipulateCustomerAddress)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // All Data
            if (!$isLincenseKey)
            {
                $this->ps.setString(1,$ManipulateCustomerAddress->getName());
                $this->ps.setString(2,$ManipulateCustomerAddress->getBusinessName());
                $this->ps.setString(3,$ManipulateCustomerAddress->getAddress1());
                $this->ps.setString(4,$ManipulateCustomerAddress->getAddress2());
                $this->ps.setString(5,$ManipulateCustomerAddress->getCity());
                $this->ps.setString(6,$ManipulateCustomerAddress->getPostal_code());
                $this->ps.setString(7,$ManipulateCustomerAddress->getTelephone());
                $this->ps.setString(8,$ManipulateCustomerAddress->getFax());
                $this->ps.setString(9,$ManipulateCustomerAddress->getEmail());
                $this->ps.setString(10,$ManipulateCustomerAddress->getCellphone());
                $this->ps.setString(11,$ManipulateCustomerAddress->getCopyR1());
                $this->ps.setString(12,$ManipulateCustomerAddress->getCopyR2());
                $this->ps.setString(13,$ManipulateCustomerAddress->getCopyR3());
                $this->ps.setString(14,$ManipulateCustomerAddress->getLogo());
                $this->ps.setInt(15,$ManipulateCustomerAddress->getCustomeraddress_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$ManipulateCustomerAddress->getLincenseKey());
                $this->ps.setInt(2,$ManipulateCustomerAddress->getCustomeraddress_id());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description Source_of_incomeDAL
class Source_of_incomeDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function Source_of_incomeDAL()
    {
    }
    function Source_of_income($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM source_of_income";
		    }
            else
            {
                $sql = "SELECT * FROM source_of_income" . " WHERE Source_of_income_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new Source_of_incomeBLL($row["Source_of_income_id"], $row["Source_of_income_name"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
    function InsertSource_of_income($new)
    {
		$tabname = "source_of_income";
        $query = "INSERT INTO $tabname  (Source_of_income_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function UpdateSource_of_income($Update)
    {
		$tabname = "source_of_income";
        $query = "UPDATE $tabname SET Source_of_income_name = ? WHERE Source_of_income_id = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
              /*$this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.executeUpdate();
                $this->ps.close();*/
            }
            else
            {
               /* $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.setInt(4,$ManipulateGender->getBroker_idBroker());
                $this->ps.executeUpdate();
                $this->ps.close();*/
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
           /* if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }*/
        }
        return $lv_return;
    }
    // Delete 
    function DeleteSource_of_income($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "source_of_income";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where Source_of_income_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description OccupationDAL
class OccupationDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function OccupationDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function Occupation($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM occupation";
		    }
            else
            {
                $sql = "SELECT * FROM occupation" . " WHERE Occupation_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			//print_r($data );
			foreach($data as $row)
            {
                 $lv_list[] = new OccupationBLL($row["Occupation_id"], $row["Occupation_name"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
    function InsertOccupation($new)
    {
		$tabname = "occupation";
        $query = "INSERT INTO $tabname  (Occupation_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function UpdateOccupation($Update)
    {
		$tabname = "occupation";
        $query = "UPDATE $tabname SET Occupation_name = ? WHERE Occupation_id = ?";
        return $this->DataManipulator(2, $query, $Update->Occupation_id,Occupation_name);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
              /*$this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.executeUpdate();
                $this->ps.close();*/
            }
            else
            {
               /* $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.setInt(4,$ManipulateGender->getBroker_idBroker());
                $this->ps.executeUpdate();
                $this->ps.close();*/
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
           /* if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }*/
        }
        return $lv_return;
    }
    // Delete 
    function DeleteOccupation($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "occupation";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where Occupation_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}

///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description BrokerDAL
class BrokerDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function BrokerDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function Broker($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM broker";
		    }
            else
            {
                $sql = "SELECT * FROM broker" . " WHERE idBroker = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new BrokerBLL($row["idBroker"], $row["brokerfirstname"],$row['brokermiddlename'],$row['brokersurname']);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	    function Broker_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation =strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM broker";
		    }
            else
            {
                $sql = "SELECT * FROM broker WHERE IntUserCode = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			//print_r($data);
			foreach($data as $row)
            {
                 $lv_list[] = new BrokerBLL($row["idBroker"], $row["brokerfirstname"],$row['brokermiddlename'],$row['brokersurname']);
           // echo print_r("fIRST".$lv_list);
			}
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
		//print_r($lv_list);
        return $lv_list;
		
    }
    function InsertBroker($new)
    {
		$tabname = "broker";
        $query = "INSERT INTO $tabname  (brokerfirstname,brokermiddlename,brokersurname) VALUES(?,?,?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function UpdateGender($Update)
    {
		$tabname = "gender";
        $query = "UPDATE $tabname SET brokerfirstname = ?,brokermiddlename = ?,brokersurname = ? WHERE idBroker = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.setInt(4,$ManipulateGender->getBroker_idBroker());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete 
    function DeleteBroker($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "broker";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where idBroker = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Mulu Mahwayi
// * @Date        07-05-2023
// * @Description PaymentMethodDAL
class PaymentMethodDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function PaymentMethodDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function PaymentMethod($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM method_of_contribution";
		    }
            else
            {
                $sql = "SELECT * FROM method_of_contribution" . " WHERE idBroker = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new PaymentMethodBLL($row["Method_of_contribution_id"], $row["Method_of_contribution_name"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	function PaymentMethod_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation =strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM method_of_contribution";
		    }
            else
            {
                $sql = "SELECT * FROM method_of_contribution WHERE IntUserCode = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            //print_r($data);
			// Read the table.
			foreach($data as $row)
            {
                $lv_list[] = new PaymentMethodBLL($row["Method_of_contribution_id"], $row['Method_of_contribution_name']);
				//echo print_r("SECOND".$lv_list);
			}
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
		//print_r($lv_list);
        return $lv_list;
    }
    function InsertBroker($new)
    {
		$tabname = "method_of_contribution";
        $query = "INSERT INTO $tabname  (Method_of_contribution_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $new);
    }

    // Delete 
    function DeletePaymentMethod($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "Method_of_contribution";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where Method_of_contribution_id = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
//////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Mulu Mahwayi
// * @Date        07-05-2023
// * @Description ContributionsDAL
class ContributionsDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function ContributionsDAL()
    {
    }
    // Load the Contribution(s)- Load records from the database
    function Contributions($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
            if ($operation == 0)
            {
                $sql = "SELECT * FROM contribution";
		    }
            else
            {
                $sql = "SELECT * FROM contribution" . " WHERE Member_Policy_id = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new ContributionsBLL($row["Member_Policy_id"], $row["Contribution_date"], $row["Contribution_amount"],$row["createdon"],$row["agent_name"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	function Contributions_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation =strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM contribution";
		    }
            else
            {
                $sql = "SELECT * FROM contribution WHERE IntUserCode = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            //print_r($data);
			// Read the table.
			foreach($data as $row)
            {
             $lv_list[] = new ContributionsBLL($row["Member_Policy_id"], $row["Contribution_date"], $row["Contribution_amount"],$row["createdon"],$row["agent_name"]);
				//echo print_r("SECOND".$lv_list);
			}
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
		//print_r($lv_list);
        return $lv_list;
    }
}
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Mulu Mahwayi
// * @Date        20-11-2023
// * @Description CommsMethodDAL
class CommsMethodDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function CommsMethodDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function CommsMethod($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM comms_method";
		    }
            else
            {
                $sql = "SELECT * FROM comms_method" . " WHERE idBroker = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new PaymentMethodBLL($row["idcomms_method"], $row["comms_method_name"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	function CommsMethod_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation =strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM comms_method";
		    }
            else
            {
                $sql = "SELECT * FROM comms_method WHERE IntUserCode = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            //print_r($data);
			// Read the table.
			foreach($data as $row)
            {
                $lv_list[] = new CommsMethodBLL($row["idcomms_method"], $row['comms_method_name']);
				//echo print_r("SECOND".$lv_list);
			}
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
		//print_r($lv_list);
        return $lv_list;
    }
    function InsertCommsMethod($new)
    {
		$tabname = "comms_method";
        $query = "INSERT INTO $tabname  (comms_method_name) VALUES(?)";
        return $this->DataManipulator(1, $query, $new);
    }

    // Delete 
    function DeleteCommsMethod($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "comms_method";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where idcomms_method = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description source_of_incomeBLL
class Source_of_incomeBLL
{
	public $Source_of_income_id    = 0;
	public $Source_of_income_name  = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($Source_of_income_id,$Source_of_income_name)
    {
		$this->setSource_of_income_id($Source_of_income_id);
		$this->setSource_of_income_name($Source_of_income_name);
    }
	
    // Overloaded constructor
    public function construct1($Source_of_income_id)
    {
        $this->setSource_of_income_id($Source_of_income_id);
    }
    //     * @return the 
    function getSource_of_income_name()
    {
        return $this->Source_of_income_name;
    }
    // *
    //     * @param  the  to set
    function setSource_of_income_name($Source_of_income_name)
    {
        $this->Source_of_income_name = $Source_of_income_name;
    }
    // *
    //     * @return the 
    function getSource_of_income_id()
    {
        return $this->Source_of_income_id;
    }
    // *
    //     * @param  the  to set
    function setSource_of_income_id($Source_of_income_id)
    {
        $this->Source_of_income_id = $Source_of_income_id;
    }
	
	// --- Get
	function getBroker_brokermiddlename()
    {
        return $this->brokermiddlename;
    }
	
    function Source_of_income($im_value)
    {
        $Source_of_income = new Source_of_incomeDAL();
        return $Source_of_income->Source_of_income($im_value);
    }
    // Insert  
    function InsertSource_of_income($new)
    {
        $Source_of_income = new Source_of_incomeDAL();
        return $Broker->InsertSource_of_income($new);
    }
    // Update 
    function UpdateSource_of_income($Update)
    {
        $Source_of_income = new Source_of_incomeDAL();
        return $Source_of_income->UpdateSource_of_income($Update);
    }
    // Delete 
    function DeleteSource_of_income($pkey)
    {
        $Source_of_income = new Source_of_incomeDAL();
        return $Source_of_income->DeleteSource_of_income($pkey);
    }
}
//////////////////////////////// source_of_income 
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description OccupationBLL
class OccupationBLL
{
	public $Occupation_id 		  = 0;
	public $Occupation_name  = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($Occupation_id,$Occupation_name)
    {
		$this->setOccupation_id($Occupation_id);
		$this->setOccupation_name($Occupation_name);    
	}
	
    // Overloaded constructor
    public function construct1($Occupation_id)
    {
        $this->setOccupation_id($Occupation_id);
    }
    //     * @return the 
    function getOccupation_id()
    {
        return $this->Occupation_id;
    }
    // *
    //     * @param  the  to set
    function setOccupation_name($Occupation_name)
    {
        $this->Occupation_name = $Occupation_name;
    }
	//     * @param  the  to set
    function setOccupation_id($Occupation_id)
    {
        $this->Occupation_id = $Occupation_id;
    }
    // *
    //     * @return the 
    function getOccupation_name()
    {
        return $this->Occupation_name;
    }
    
    function Occupation($im_value)
    {
        $Occupation = new OccupationDAL();
        return $Occupation->Occupation($im_value);
    }
    // Insert  
    function InsertOccupation($new)
    {
        $Occupation = new OccupationDAL();
        return $Occupation->InsertOccupation($new);
    }
    // Update 
    function UpdateOccupation($Update)
    {
        $Occupation = new OccupationDAL();
        return $Occupation->UpdateOccupation($Update);
    }
    // Delete 
    function DeleteOccupation($pkey)
    {
        $Occupation = new OccupationDAL();
        return $Occupation->DeleteOccupation($pkey);
    }
}
///////////////////// Being BLL ////
// *
// * @author      Mulu Mahwayi
// * @Date        07-05-2023
// * @Description PaymentMethodBLL
class PaymentMethodBLL
{
	public $Method_of_contribution_id= 0;
	public $Method_of_contribution_name  = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($Method_of_contribution_id,$Method_of_contribution_name)
    {
		$this->setMethod_of_contribution_id($Method_of_contribution_id);
		$this->setMethod_of_contribution_name($Method_of_contribution_name);
    }
	
	
    // Overloaded constructor
    public function construct1($Method_of_contribution_id)
    {
        $this->setMethod_of_contribution_id($Method_of_contribution_id);
    }
    // Overloaded constructor
    /*function  __construct($brokerfirstname)
    {
        $this->setBroker_brokerfirstname($brokerfirstname);
    }
    // Overloaded constructor
    function  __construct($brokermiddlename)
    {
        $this->setBroker_brokermiddlename($brokermiddlename);
    }
	// Overloaded constructor
    function  __construct($brokersurname)
    {
        $this->setBroker_brokersurname($brokersurname);
    }*/
    // *
    //     * @return the 
    function getMethod_of_contribution_id()
    {
        return $this->Method_of_contribution_id;
    }
    // *
    //     * @param  the  to set
    function setMethod_of_contribution_id($Method_of_contribution_id)
    {
        $this->Method_of_contribution_id = $Method_of_contribution_id;
    }
    // *
    //     * @return the 
    function getMethod_of_contribution_name()
    {
        return $this->Method_of_contribution_name;
    }
    // *
    //     * @param  the  to set
    function setMethod_of_contribution_name($Method_of_contribution_name)
    {
        $this->Method_of_contribution_name = $Method_of_contribution_name;
    }
	
	
    // Get the Broker(s)- Load records from the database
    function PaymentMethod($im_value)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->PaymentMethod($im_value);
    }
	/* Get the Comms Method(s)- Load records from the database
    function PaymentMethod($im_value)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->PaymentMethod($im_value);
    }*/
    // Insert  
    function InsertPaymentMethod($newPaymentMethod)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->InsertPaymentMethod($newPaymentMethod);
    }
    // Update 
    function UpdatePaymentMethod($UpdatePaymentMethod)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->UpdatePaymentMethod($UpdatePaymentMethod);
    }
    // Delete 
    function DeletePaymentMethod($pkey)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->DeletePaymentMethod($pkey);
    }
}
///////////////////// Being BLL ////
// *
// * @author      Mulu Mahwayi
// * @Date        07-05-2023
// * @Description ContributionBLL
class ContributionsBLL
{
	public $Contribution_id= 0;
	public $Member_Policy_id  = "";
	public $Contribution_date= "";
	public $IntUserCode  = "";
	public $agent_name  = "";
	public $createdon  = "";
	
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($Contribution_id,$Member_Policy_id,$IntUserCode,$agent_name,$createdon)
    {
		$this->setContribution_id($Contribution_id);
		$this->setMember_Policy_id($Member_Policy_id);
		$this->setIntUserCode($IntUserCode);
		$this->setagent_name($agent_name);
		$this->setcreatedon($createdon);
    }
	
	
    // Overloaded constructor
    public function construct1($Contribution_id)
    {
        $this->setContribution_id($Contribution_id);
    }
  
    function getContribution_id()
    {
        return $this->Contribution_id;
    }
    // *
    //     * @param  the  to set
    function setContribution_id($Contribution_id)
    {
        $this->Contribution_id = $Contribution_id;
    }
    // 
    //     * @return the 
    function getMember_Policy_id()
    {
        return $this->Member_Policy_id;
    }
    // *
    //     * @param  the  to set
    function setMember_Policy_id($Member_Policy_id)
    {
        $this->Member_Policy_id = $Member_Policy_id;
    }
	// *
    //     * @return the 
    function getIntUserCode()
    {
        return $this->IntUserCode;
    }
    // *
    //     * @param  the  to set
    function setIntUserCode($IntUserCode)
    {
        $this->IntUserCode = $IntUserCode;
    }
	    // *
    //     * @return the 
    function getagent_name()
    {
        return $this->agent_name;
    }
    // *
    //     * @param  the  to set
    function setagent_name($agent_name)
    {
        $this->agent_name = $agent_name;
    }
		    // *
    //     * @return the 
    function getcreatedon()
    {
        return $this->createdon;
    }
    // *
    //     * @param  the  to set
    function setcreatedon($createdon)
    {
        $this->createdon = $createdon;
    }
	
    // Get the Broker(s)- Load records from the database
    function Contributions($im_value)
    {
        $Contributions = new ContributionsDAL();
        return $Contributions->Contributions($im_value);
    }
	/* Get the Comms Method(s)- Load records from the database
    function PaymentMethod($im_value)
    {
        $PaymentMethod = new PaymentMethodDAL();
        return $PaymentMethod->PaymentMethod($im_value);
    }*/
  
}
///////////////////// Being BLL ////
// *
// * @author      Mulu Mahwayi
// * @Date        07-05-2023
// * @Description PaymentMethodBLL
class CommsMethodBLL
{
	public $idcomms_method= 0;
	public $comms_method_name  = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($idcomms_method,$comms_method_name)
    {
		$this->setidcomms_method($idcomms_method);
		$this->setcomms_method_name($comms_method_name);
    }
	
	
    // Overloaded constructor
    public function construct1($idcomms_method)
    {
        $this->setidcomms_method($idcomms_method);
    }
    // Overloaded constructor
    /*function  __construct($brokerfirstname)
    {
        $this->setBroker_brokerfirstname($brokerfirstname);
    }
    // Overloaded constructor
    function  __construct($brokermiddlename)
    {
        $this->setBroker_brokermiddlename($brokermiddlename);
    }
	// Overloaded constructor
    function  __construct($brokersurname)
    {
        $this->setBroker_brokersurname($brokersurname);
    }*/
    // *
    //     * @return the 
    function getidcomms_method()
    {
        return $this->idcomms_method;
    }
    // *
    //     * @param  the  to set
    function setidcomms_method($idcomms_method)
    {
        $this->idcomms_method = $idcomms_method;
    }
    // *
    //     * @return the 
    function getcomms_method_name()
    {
        return $this->comms_method_name;
    }
    // *
    //     * @param  the  to set
    function setcomms_method_name($comms_method_name)
    {
        $this->comms_method_name = $comms_method_name;
    }
	
	
    // Get the Comms(s)- Load records from the database
    function CommsMethod($im_value)
    {
        $CommsMethod = new CommsMethodDAL();
        return $CommsMethod->CommsMethod($im_value);
    }
    // Insert  
    function InsertCommsMethod($newCommsMethod)
    {
        $CommsMethod = new CommsMethodDAL();
        return $CommsMethod->InsertCommsMethod($newCommsMethod);
    }
    // Update 
    function UpdateCommsMethod($UpdateCommsMethod)
    {
        $CommsMethod = new CommsMethodDAL();
        return $CommsMethod->UpdateCommsMethod($UpdateCommsMethod);
    }
    // Delete 
    function DeleteCommsMethod($pkey)
    {
        $CommsMethod = new CommsMethodDAL();
        return $CommsMethod->DeleteCommsMethod($pkey);
    }
}
///////////////////// Being BLL ////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description BrokerBLL
class BrokerBLL
{
	public $idBroker 		  = 0;
	public $brokerfirstname  = "";
	public $brokermiddlename = "";
	public $brokersurname 	  = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct4($idBroker,$brokerfirstname,$brokermiddlename,$brokersurname)
    {
		$this->setBroker_idBroker($idBroker);
		$this->setBroker_brokerfirstname($brokerfirstname);
		$this->setBroker_brokermiddlename($brokermiddlename);
		$this->setBroker_brokersurname($brokersurname);
    }
	
	
    // Overloaded constructor
    public function construct1($idBroker)
    {
        $this->setBroker_idBroker($idBroker);
    }
    // Overloaded constructor
    /*function  __construct($brokerfirstname)
    {
        $this->setBroker_brokerfirstname($brokerfirstname);
    }
    // Overloaded constructor
    function  __construct($brokermiddlename)
    {
        $this->setBroker_brokermiddlename($brokermiddlename);
    }
	// Overloaded constructor
    function  __construct($brokersurname)
    {
        $this->setBroker_brokersurname($brokersurname);
    }*/
    // *
    //     * @return the 
    function getBroker_idBroker()
    {
        return $this->idBroker;
    }
    // *
    //     * @param  the  to set
    function setBroker_idBroker($idBroker)
    {
        $this->idBroker = $idBroker;
    }
    // *
    //     * @return the 
    function getBroker_brokerfirstname()
    {
        return $this->brokerfirstname;
    }
    // *
    //     * @param  the  to set
    function setBroker_brokerfirstname($brokerfirstname)
    {
        $this->brokerfirstname = $brokerfirstname;
    }
	
	// --- Get
	function getBroker_brokermiddlename()
    {
        return $this->brokermiddlename;
    }
	// -- Set
    function setBroker_brokermiddlename($brokermiddlename)
    {
        $this->brokermiddlename = $brokermiddlename;
    }
	
	// -- Get
	function getBroker_brokersurname()
    {
        return $this->brokersurname;
    }	
	// -- Set
	function setBroker_brokersurname($brokersurname)
    {
        $this->brokersurname = $brokersurname;
    }
    // Get the Broker(s)- Load records from the database
    function Broker($im_value)
    {
        $Broker = new BrokerDAL();
        return $Broker->Broker($im_value);
    }
    // Insert  
    function InsertBroker($newBroker)
    {
        $Broker = new BrokerDAL();
        return $Broker->InsertBroker($newBroker);
    }
    // Update 
    function UpdateBroker($UpdateBroker)
    {
        $Broker = new BrokerDAL();
        return $Broker->UpdateBroker($UpdateBroker);
    }
    // Delete 
    function DeleteBroker($pkey)
    {
        $Broker = new BrokerDAL();
        return $Broker->DeleteBroker($pkey);
    }
}
// --------------------- Begin of Branch DAL -------------------------- 
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description BranchDAL
class BranchDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function BranchDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function Branch($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
            if ($operation == 0)
            {
                $sql = "SELECT * FROM branch";
		    }
            else
            {
               // $sql = "SELECT * FROM branch" . " WHERE idBranch = " . strval($operation);	
				$sql = "SELECT * FROM branch" . " WHERE IntUserCode = " . strval($operation);			   
            }
			//echo 'branch'. $sql.$operation ;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new BranchBLL($row["idBranch"], $row["branchusercode"],$row['branchname'],$row['Street'],$row['suburb'],$row['city'],$row['state'],$row['postalcode'],$row['idBroker']);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	function Branch_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation= strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM branch";
		    }
            else
            {
               // $sql = "SELECT * FROM branch" . " WHERE idBranch = " . strval($operation);	
				$sql = "SELECT * FROM branch WHERE IntUserCode = '$operation'";	
							
            }
			//echo 'Branch_IntUserCode'.$sql.$operation ;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new BranchBLL($row["idBranch"], $row["branchusercode"],$row['branchname'],$row['Street'],$row['suburb'],$row['city'],$row['state'],$row['postalcode'],$row['idBroker']);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
	/*function PaymentMethod _IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$operation= strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM method_of_contribution";
		    }
            else
            {
				$sql = "SELECT * FROM method_of_contribution WHERE IntUserCode = '$operation'";	
							
            }
			//echo 'Branch_IntUserCode'.$sql.$operation ;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new BranchBLL($row["Method_of_contribution_id"], $row["Method_of_contribution_name"],$row['IntUserCode']);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }*/
    function Insert($new)
    {
		$tabname = "branch";
        $query = "INSERT INTO $tabname  (branchusercode,branchname,Street,suburb,city,state,postalcode,idBroker) VALUES(?,?,?,?,?,?,?,?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function Update($Update)
    {
		$tabname = "branch";
        $query = "UPDATE $tabname SET branchusercode = ?,branchname = ?,Street = ?,suburb = ?,city = ?,state = ?,postalcode = ?,idBroker = ? WHERE idBranch = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement($query);
            // Insert
            if ($operation == 1)
            {
                $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.executeUpdate();
                $this->ps.close();
            }
            else
            {
                $this->ps.setString(1,$Manipulate->getBroker_brokerfirstname());
                $this->ps.setString(2,$Manipulate->getBroker_brokermiddlename());
                $this->ps.setString(3,$Manipulate->getBroker_brokersurname());				
                $this->ps.setInt(4,$ManipulateGender->getBroker_idBroker());
                $this->ps.executeUpdate();
                $this->ps.close();
            }
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete 
    function DeleteBroker($pkey)
    {
        $lv_return = "";
        try
        {
			$tabname = "broker";
            $this->connect = db_connect::db_connection();
            $this->ps = $this->connect.prepareStatement("DELETE FROM $tabname  where idBroker = ?");
            $this->ps.setInt(1,$pkey);
            $this->ps.executeUpdate();
            $this->ps.close();
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
}
// --------------------- End of BranchDAL --------------------------
// --------------------- Begin of BranchDLL ------------------------
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description BranchBLL
class BranchBLL
{

public $idBranch			= 0;	
public $branchusercode		= "";
public $branchname			= "";	
public $Street				= "";
public $suburb				= "";
public $city				= "";
public $state				= "";
public $postalcode			= "";
public $idBroker			= 0;
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct9($idBranch,$branchusercode,$branchname,$Street,$suburb,$city,$state,$postalcode,$idBroker)
    {
		$this->setBranch_idBroker($idBranch);
		$this->setBranch_branchusercode($branchusercode);
		$this->setBranch_branchname($branchname);
		$this->setBranch_Street($Street);
		$this->setBranch_suburb($suburb);
		$this->setBranch_city($city);
		$this->setBranch_state($state);
		$this->setBranch_postalcode($postalcode);
		$this->setBranch_idBroker($idBroker);
    }
	
    // Overloaded constructor
    public function construct1($idBranch)
    {
        $this->setBroker_idBranch($idBranch);
    }
	
    //     * @return the 
    function getBranch_idBroker()
    {
        return $this->idBroker;
    }
    // *
    //     * @param  the  to set
    function setBranch_idBroker($idBroker)
    {
        $this->idBroker = $idBroker;
    }
    // *
    //     * @return the 
    function getBranch_branchusercode()
    {
        return $this->branchusercode;
    }
    // *
    //     * @param  the  to set
    function setBranch_branchusercode($branchusercode)
    {
        $this->branchusercode = $branchusercode;
    }
	
	// --- Get
	function getBranch_branchname()
    {
        return $this->branchname;
    }
	// -- Set
    function setBranch_branchname($branchname)
    {
        $this->branchname = $branchname;
    }
	
	// -- Get
	function getBranch_Street()
    {
        return $this->Street;
    }	
	// -- Set
	function setBranch_Street($Street)
    {
        $this->Street = $Street;
    }
	
		// -- Get
	function getBranch_suburb()
    {
        return $this->suburb;
    }	
	// -- Set
	function setBranch_suburb($suburb)
    {
        $this->suburb = $suburb;
    }
	
		// -- Get
	function getBranch_city()
    {
        return $this->city;
    }	
	
	// -- Set
	function setBranch_city($city)
    {
        $this->city = $city;
    }
	
	// -- Set
	function setBranch_state($state)
    {
        $this->state = $state;
    }
	
	// -- Get
	function getBranch_state()
    {
        return $this->state;
    }	
	// -- Set
	function setBranch_postalcode($postalcode)
    {
        $this->postalcode = $postalcode;
    }
	
	// -- Get
	function getBranch_postalcode()
    {
        return $this->postalcode;
    }	
	
    // Get the Branch(s)- Load records from the database
    function Branch($im_value)
    {
        $Branch = new BranchDAL();
        return $Branch->Branch($im_value);
    }
    // Insert  
    function Insert($new)
    {
        $object = new BranchDAL();
        return $object->Insert($new);
    }
    // Update 
    function Update($Update)
    {
        $object = new BranchDAL();
        return $object->UpdateBroker($Update);
    }
    // Delete 
    function DeleteBranch($pkey)
    {
        $object = new BranchDAL();
        return $object->DeleteBranch($pkey);
    }
}
// --------------------- End of BranchBLL   ------------------------    
// --------------------- Begin of PackageOptionDAL -------------------------- 
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description PackageOptionDAL
class PackageOptionDAL
{
    // Declarations.
    public $connect;
    public $s;
    public $ps;
    public $rs;
    public $count;
    // Default constructor.
    function packageoptionDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function packageoption($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$table = "packageoption";
            if ($operation == 0)
            {
                $sql = "SELECT * FROM $table";
		    }
            else
            {
                $sql = "SELECT * FROM $table" . " WHERE PackageOption_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new packageoptionBLL($row["PackageOption_id"], $row["Benefits"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
    function Insert($new)
    {
		$tabname = "packageoption";
        $query = "INSERT INTO $tabname  (Benefits) VALUES(?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function Update($Update)
    {
		$tabname = "packageoption";
        $query = "UPDATE $tabname SET Benefits = ? WHERE PackageOption_id = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// -- ffms generic data manipulator..
			include "ffms_datamanipulator.php";
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete 
    function Deleterecord($pkey)
    {
			$tabname = "packageoption";
			$query = "DELETE FROM $tabname WHERE PackageOption_id = ?";
			return $this->DataManipulator(2, $query, new packageoptionBLL($pkey));
    }
}
// --------------------- End of packageoptionDAL --------------------------
// --------------------- Begin of packageoptionDLL ------------------------
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description packageoptionBLL
class packageoptionBLL
{
public $PackageOption_id			= 0;	
public $Benefits		= "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct2($PackageOption_id,$Benefits)
    {
        $this->set_PackageOption_id($PackageOption_id);
		$this->set_Benefits($Benefits);
    }
	
    // Overloaded constructor
    public function construct1($PackageOption_id)
    {
        $this->set_PackageOption_id($PackageOption_id);
    }
	
    //     * @return the 
    function get_PackageOption_id()
    {
        return $this->PackageOption_id;
    }
    // *
    //     * @param  the  to set
    function set_PackageOption_id($PackageOption_id)
    {
        $this->PackageOption_id = $PackageOption_id;
    }
    // *
    //     * @return the 
    function get_Benefits()
    {
        return $this->Benefits;
    }
    // *
    //     * @param  the  to set
    function set_Benefits($Benefits)
    {
        $this->Benefits = $Benefits;
    }
	
    // Get the Branch(s)- Load records from the database
    function packageoption($im_value)
    {
        $packageoption = new packageoptionDAL();
        return $packageoption->packageoption($im_value);
    }
    // Insert  
    function Insert($new)
    {
        $object = new packageoptionDAL();
        return $object->Insert($new);
    }
    // Update 
    function Update($Update)
    {
        $object = new packageoptionDAL();
        return $object->Update($Update);
    }
    // Delete 
    function Deleterecord($pkey)
    {
        $object = new packageoptionDAL();
        return $object->Deleterecord($pkey);
    }
}
// --------------------- End of packageoptionBLL   ------------------------   
// --------------------- Begin of premiumDAL ------------------------------
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description premiumDAL
class premiumDAL
{
    // Declarations.
    public $connect;
    public $count;
    // Default constructor.
    function premiumDAL()
    {
    }
    // Load the Gender(s)- Load records from the database
    function premium($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$table = "premium";
            if ($operation == 0)
            {
                $sql = "SELECT * FROM $table";
		    }
            else
            {
                $sql = "SELECT * FROM $table" . " WHERE Premium_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
                 $lv_list[] = new packageoptionBLL($row["Premium_id"], $row["Years"],$row["Premium_amount"],$row["PackageOption_id"]);
            }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
        return $lv_list;
    }
    function Insert($new)
    {
		$tabname = "premium";
        $query = "INSERT INTO $tabname  (Years,Premium_amount,PackageOption_id) VALUES(?,?,?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function Update($Update)
    {
		$tabname = "premium";
        $query = "UPDATE $tabname SET Years = ?,Premium_amount = ?,PackageOption_id = ? WHERE PackageOption_id = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// -- ffms generic data manipulator..
			include "ffms_datamanipulator.php";
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete 
    function Deleterecord($pkey)
    {
			$tabname = "premium";
			$query = "DELETE FROM $tabname WHERE Premium_id = ?";
			return $this->DataManipulator(2, $query, new premiumBLL($pkey));
    }
}
// --------------------- End of premiumDAL --------------------------
// --------------------- Begin of premiumDLL ------------------------
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description premiumBLL
class premiumBLL
{
public $Premium_id = "";
public $Years = "";
public $Premium_amount = "";
public $PackageOption_id = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct4($Premium_id,$Years,$Premium_amount,$PackageOption_id)
    {
        $this->set_Premium_id($Premium_id);
		$this->set_Years($Years);
		$this->set_Premium_amount($Premium_amount);
		$this->set_PackageOption_id($PackageOption_id);
    }
	

    //     * @param  the  to set
    function set_Premium_id($Premium_id)
    {
        $this->Premium_id = $Premium_id;
    }
    // *
    //     * @return the 
    function get_Premium_id()
    {
        return $this->Premium_id;
    }
    // *
    //     * @param  the  to set
    function set_Years($Years)
    {
        $this->Years = $Years;
    }
	//     * @return the 
    function get_Years()
    {
        return $this->Years;
    }
    // *
	    //     * @param  the  to set
    function set_Premium_amount($Premium_amount)
    {
        $this->Premium_amount = $Premium_amount;
    }
	//     * @return the 
    function get_Premium_amount()
    {
        return $this->Premium_amount;
    }
    // *
	    //     * @param  the  to set
    function set_PackageOption_id($PackageOption_id)
    {
        $this->PackageOption_id = $PackageOption_id;
    }
	
    //     * @return the 
    function get_PackageOption_id()
    {
        return $this->PackageOption_id;
    }
    // *
	
    // Get the Branch(s)- Load records from the database
    function premium($im_value)
    {
        $obj = new premiumDAL();
        return $obj->premium($im_value);
    }
    // Insert  
    function Insert($new)
    {
        $object = new premiumDAL();
        return $object->Insert($new);
    }
    // Update 
    function Update($Update)
    {
        $object = new premiumDAL();
        return $object->Update($Update);
    }
    // Delete 
    function Deleterecord($pkey)
    {
        $object = new premiumDAL();
        return $object->Deleterecord($pkey);
    }
}

// --------------------- End of premiumBLL   ------------------------
// --------------------- Begin of policyDAL ------------------------------
///////////////////////////////////////////////////////////////////////////////////
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description policyDAL
class policyDAL
{
    // Declarations.
    public $connect;
    public $count;
    // Default constructor.
    function policyDAL()
    {
    }
    // Load the records from the database
    function policy($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$table = "policy";
            if ($operation == 0)
            {
                $sql = "SELECT * FROM $table";
               
		    }
            else
            {
                $sql = "SELECT * FROM $table" . " WHERE Policy_id = " . strval($operation);		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
             
                 $lv_list[] = new policyBLL($row["Policy_id"], $row["Policy_name"],$row["Policy_desc"],$row["PackageOption_id"]);
                //echo print_r("SECOND".$lv_list);
                }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
       
        return $lv_list;
    }
	    function policy_IntUserCode($operation)
    {
        // Temporary list.
        $lv_list = array();
		$sql  = "";
        try
        {
            $this->connect = db_connect::db_connection();			
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$table = "policy";
			$operation= strval($operation);
            if ($operation == '0')
            {
                $sql = "SELECT * FROM $table";
               
		    }
            else
            {
                $sql = "SELECT * FROM $table WHERE IntUserCode = '$operation'";		
            }
			//echo $sql;
			$q = $this->connect->prepare($sql);
			$q = $this->connect->query($sql);				
			$data = $q->fetchAll();
            // Read the table.
			foreach($data as $row)
            {
             
                 $lv_list[] = new policyBLL($row["Policy_id"], $row["Policy_name"],$row["Policy_desc"],$row["PackageOption_id"]);
                //echo print_r("SECOND".$lv_list);
                }
        } catch ( Exception $e) {
            echo $e->getMessage(),"\n";
        }
        finally
        {
        }
       
        return $lv_list;
    }
    function Insert($new)
    {
		$tabname = "policy";
        $query = "INSERT INTO $tabname  (Policy_name,Policy_desc,PackageOption_id) VALUES(?,?,?)";
        return $this->DataManipulator(1, $query, $new);
    }
    function Update($Update)
    {
		$tabname = "policy";
        $query = "UPDATE $tabname SET Policy_name = ?,Policy_desc = ?,PackageOption_id = ? WHERE Policy_id = ?";
        return $this->DataManipulator(2, $query, $Update);
    }
    // Data Manipulator.
    function DataManipulator($operation, $query, $Manipulate)
    {
        $lv_return = "";
        try
        {
            $this->connect = db_connect::db_connection();
			$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			// -- ffms generic data manipulator..
			include "ffms_datamanipulator.php";
        } catch ( Exception $e) {
            echo $e.getMessage(),"\n";
            $lv_return = $e.getMessage();
        }
        finally
        {
            if ($this->connect != NULL)
            {
                try
                {
                    $this->connect.close();
                } catch ( Exception $e) {
                }
            }
        }
        return $lv_return;
    }
    // Delete 
    function Deleterecord($pkey)
    {
			$tabname = "policy";
			$query = "DELETE FROM $tabname WHERE Policy_id = ?";
			return $this->DataManipulator(2, $query, new policyBLL($pkey));
    }
}
// --------------------- End of policyDAL --------------------------
// --------------------- Begin of policyDLL ------------------------
// *
// * @author      Tumelo Modise
// * @Date        18-01-2012
// * @Description policyBLL
class policyBLL
{
public $Policy_id = "";
public $Policy_name = "";
public $Policy_desc = "";
public $PackageOption_id = "";
		
	function __construct()
	{
		$arguments = func_get_args();
        $numberOfArguments = func_num_args();
		include "ffms_generic.php";
	}		
    // Default constructor
    // Overloaded constructor
    public function construct4($Policy_id , $Policy_name , $Policy_desc , $PackageOption_id)
    {
        $this->set_Policy_id($Policy_id);
		$this->set_Policy_name($Policy_name);
		$this->set_Policy_desc($Policy_desc);
		$this->set_PackageOption_id($PackageOption_id);
    }
	
    //     * @param  the  to set
    function set_Policy_id($value)
    {
        $this->Policy_id = $value;
    }
    // *
    //     * @return the 
    function get_Policy_id()
    {
        return $this->Policy_id;
    }
    // *
    //     * @param  the  to set
    function set_Policy_name($value)
    {
        $this->Policy_name = $value;
    }
	//     * @return the 
    function get_Policy_desc()
    {
        return $this->Policy_desc;
    }
    // *
	    //     * @param  the  to set
    function set_Policy_desc($value)
    {
        $this->Policy_desc = $value;
    }
    // *
	    //     * @param  the  to set
    function set_PackageOption_id($PackageOption_id)
    {
        $this->PackageOption_id = $PackageOption_id;
    }
	
    //     * @return the 
    function get_PackageOption_id()
    {
        return $this->PackageOption_id;
    }
    // *
	
    // -- Load records from the database
    function policy($im_value)
    {
        $obj = new policyDAL();
        return $obj->policy($im_value);
    }
    // Insert  
    function Insert($new)
    {
        $object = new policyDAL();
        return $object->Insert($new);
    }
    // Update 
    function Update($Update)
    {
        $object = new policyDAL();
        return $object->Update($Update);
    }
    // Delete 
    function Deleterecord($pkey)
    {
        $object = new policyDAL();
        return $object->Deleterecord($pkey);
    }
}
// --------------------- End of policyBLL   ------------------------


 