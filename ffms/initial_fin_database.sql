SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `FIN` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `FIN` ;



-- -----------------------------------------------------
-- Table `FIN`.`Customer_address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Customer_address`;

CREATE  TABLE IF NOT EXISTS `FIN`.`Customer_address` (
  `idCustomer_address` INT NOT NULL ,
  `name` VARCHAR(100) NULL ,
  `business_name` VARCHAR(100) NULL ,
  `address1` VARCHAR(100) NULL ,
  `address2` VARCHAR(100) NULL ,
  `city` VARCHAR(100) NULL ,
  `postal_code` VARCHAR(8) NULL ,
  `telephone` VARCHAR(45) NULL ,
  `fax` VARCHAR(45) NULL ,
  `email` VARCHAR(100) NULL ,
  `cellphone` VARCHAR(45) NULL ,
  `copyR1` VARCHAR(100) NULL ,
  `copyR2` VARCHAR(100) NULL ,
  `copyR3` VARCHAR(100) NULL ,
  `logo` VARCHAR(100) NULL ,
  `licensekey` VARCHAR(100) NULL ,
  PRIMARY KEY (`idCustomer_address`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-03-28 23:29
*/
INSERT INTO `FIN`.`Customer_address` (`idCustomer_address`,`name`,`business_name`,`address1`,`address2`,`city`,`postal_code`,`telephone`,`fax`,`email`,`cellphone`,`copyR1`,`copyR2`,`copyR3`,`logo`,`licensekey`) VALUES ('1','Any Company, Inc','Any Company','123 Main Street','Pretoria, 0001','Pretoria','0001','012-123-4567','012-123-8910','test@test.com','072-123-4567','Underwritten by Prosperity Life','An Authorised Financial Services','FSP no.: 1901','Test.PNG','');


-- -----------------------------------------------------
-- Table `FIN`.`Gender`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Gender` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Gender` (
  `Gender_id` INT NOT NULL AUTO_INCREMENT ,
  `Gender_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Gender_id`) )
ENGINE = InnoDB;
/*
-- Query: 
-- Date: 2012-02-29 19:37
*/
INSERT INTO `Gender` (`Gender_name`) VALUES ('Male');
INSERT INTO `Gender` (`Gender_name`) VALUES ('Female');

-- -----------------------------------------------------
-- Table `FIN`.`MaritualStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`MaritualStatus` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`MaritualStatus` (
  `MaritualStatus_id` INT NOT NULL AUTO_INCREMENT ,
  `MaritualStatus_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`MaritualStatus_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:03
*/
INSERT INTO `MaritualStatus` (`MaritualStatus_name`) VALUES ('Divorced');
INSERT INTO `MaritualStatus` (`MaritualStatus_name`) VALUES ('Single');
INSERT INTO `MaritualStatus` (`MaritualStatus_name`) VALUES ('Married');
INSERT INTO `MaritualStatus` (`MaritualStatus_name`) VALUES ('Window');

-- -----------------------------------------------------
-- Table `FIN`.`Marriage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Marriage` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Marriage` (
  `Marriage_id` INT NOT NULL AUTO_INCREMENT ,
  `Marriage_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Marriage_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:01
*/
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Common law marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Arranged marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Military marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Cousin marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Same-sex marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Customary marriage');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Incommunity of property');
INSERT INTO `Marriage` (`Marriage_name`) VALUES ('Community of property');

-- -----------------------------------------------------
-- Table `FIN`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Country` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Country` (
  `Country_id` INT NOT NULL AUTO_INCREMENT ,
  `Country_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Country_id`) )
ENGINE = InnoDB;
/*
-- Query: 
-- Date: 2012-02-29 19:41
*/
INSERT INTO `Country` (`Country_name`) VALUES ('South Africa');
INSERT INTO `Country` (`Country_name`) VALUES ('Zimbabwe');
INSERT INTO `Country` (`Country_name`) VALUES ('Lesotho');
INSERT INTO `Country` (`Country_name`) VALUES ('Botswana');
INSERT INTO `Country` (`Country_name`) VALUES ('Mozambique');
INSERT INTO `Country` (`Country_name`) VALUES ('Malawi');
INSERT INTO `Country` (`Country_name`) VALUES ('Swatziland');
INSERT INTO `Country` (`Country_name`) VALUES ('Namibia');
INSERT INTO `Country` (`Country_name`) VALUES ('Zambia');
INSERT INTO `Country` (`Country_name`) VALUES ('Angola');
INSERT INTO `Country` (`Country_name`) VALUES ('DRC');

-- -----------------------------------------------------
-- Table `FIN`.`Province`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Province` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Province` (
  `Province_id` INT NOT NULL AUTO_INCREMENT ,
  `Provine_name` VARCHAR(45) NULL ,
  `Country_id` INT NOT NULL ,
  PRIMARY KEY (`Province_id`) ,
  INDEX `Country_id_fk` (`Country_id` ASC) ,
  CONSTRAINT `Country_id_fk`
    FOREIGN KEY (`Country_id` )
    REFERENCES `FIN`.`Country` (`Country_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 19:45
*/
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Gauteng',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('North West',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Northern Cape',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Free State',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Limpopo',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Mpumalanga',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Kwa Zulu Natal',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Western Cape',1);
INSERT INTO `Province` (`Provine_name`,`Country_id`) VALUES ('Eastern Cape',1);

-- -----------------------------------------------------
-- Table `FIN`.`Town`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Town` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Town` (
  `Town_id` INT NOT NULL AUTO_INCREMENT ,
  `Town_name` VARCHAR(45) NOT NULL ,
  `Province_id` INT NOT NULL ,
  PRIMARY KEY (`Town_id`) ,
  INDEX `Province_id_fk` (`Province_id` ASC) ,
  CONSTRAINT `Province_id_fk`
    FOREIGN KEY (`Province_id` )
    REFERENCES `FIN`.`Province` (`Province_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 19:51
*/
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Pretoria',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Johannesburg',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Rooderpoort',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Benoni',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Boksburg',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Midrand',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Mamelodi',1);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Polokwane',5);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Kuruman',3);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Bloemfontein',4);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Nelspruit',6);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Cape town',8);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Port Elizaberth',9);
INSERT INTO `Town` (`Town_name`,`Province_id`) VALUES ('Mahikeng',2);
-- -----------------------------------------------------
-- Table `FIN`.`Suburb`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Suburb` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Suburb` (
  `Suburb_id` INT NOT NULL AUTO_INCREMENT ,
  `Surburb_name` VARCHAR(45) NOT NULL ,
  `Town_id` INT NOT NULL ,
  `Postal_code` VARCHAR(4) NOT NULL ,
  PRIMARY KEY (`Suburb_id`) ,
  INDEX `Town_id_fk` (`Town_id` ASC) ,
  CONSTRAINT `Town_id_fk`
    FOREIGN KEY (`Town_id` )
    REFERENCES `FIN`.`Town` (`Town_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 19:55
*/
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Mamelodi East',6,'0003');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Pretoria  Central',1,'0001');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Pretoria  East',1,'0002');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Pretoria  South',1,'0003');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Pretoria  North',1,'0004');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Doornfontein',2,'1111');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Jhb CDB',2,'2000');
INSERT INTO `Suburb` (`Surburb_name`,`Town_id`,`Postal_code`) VALUES ('Braamfontein',2,'1111');

-- -----------------------------------------------------
-- Table `FIN`.`Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Address` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Address` (
  `Address_id` INT NOT NULL AUTO_INCREMENT ,
  `Suburb_id` INT NOT NULL ,
  `Line_1` VARCHAR(45) NOT NULL ,
  `Line_2` VARCHAR(45) NULL ,
  `Line_3` VARCHAR(45) NULL ,
  `Email` VARCHAR(255) NULL ,
  `Fax` VARCHAR(45) NULL ,
  `Cellphone` VARCHAR(45) NULL ,
  `Work_tel` VARCHAR(45) NULL ,
  `Home_tel` VARCHAR(45) NULL ,
  `Comm_preference` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Address_id`) ,
  INDEX `Suburb_id_fk` (`Suburb_id` ASC) ,
  CONSTRAINT `Suburb_id_fk`
    FOREIGN KEY (`Suburb_id` )
    REFERENCES `FIN`.`Suburb` (`Suburb_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Life_State`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Life_State` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Life_State` (
  `Life_State_id` INT NOT NULL AUTO_INCREMENT ,
  `Life_State_name` VARCHAR(45) NOT NULL DEFAULT 'Alive' ,
  PRIMARY KEY (`Life_State_id`) )
ENGINE = InnoDB
COMMENT = 'State of the member either \"deceased\" or \"Alive\"';

/*
-- Query: 
-- Date: 2012-02-29 20:14
*/
INSERT INTO `Life_State` (`Life_State_name`) VALUES ('Alive');
INSERT INTO `Life_State` (`Life_State_name`) VALUES ('Deceased');

-- -----------------------------------------------------
-- Table `FIN`.`Language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Language` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Language` (
  `Language_id` INT NOT NULL AUTO_INCREMENT ,
  `Language` VARCHAR(100) NULL ,
  PRIMARY KEY (`Language_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:07
*/
INSERT INTO `Language` (`Language`) VALUES ('Afrikaans');
INSERT INTO `Language` (`Language`) VALUES ('English');
INSERT INTO `Language` (`Language`) VALUES ('Pedi');
INSERT INTO `Language` (`Language`) VALUES ('Sotho');
INSERT INTO `Language` (`Language`) VALUES ('Tswana');
INSERT INTO `Language` (`Language`) VALUES ('Xhosa');
INSERT INTO `Language` (`Language`) VALUES ('Tsonga');
INSERT INTO `Language` (`Language`) VALUES ('Zulu');
INSERT INTO `Language` (`Language`) VALUES ('Swati');
INSERT INTO `Language` (`Language`) VALUES ('Venda');
INSERT INTO `Language` (`Language`) VALUES ('Signs');

-- -----------------------------------------------------
-- Table `FIN`.`Member`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Member` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Member` (
  `Member_id` BIGINT NOT NULL ,
  `Title` VARCHAR(45) NOT NULL ,
  `First_name` VARCHAR(45) NOT NULL ,
  `Middle_name` VARCHAR(45) NULL ,
  `Initials` VARCHAR(45) NOT NULL ,
  `Surname` VARCHAR(45) NOT NULL ,
  `Gender_id` INT NOT NULL ,
  `Date_of_birth` DATE NOT NULL ,
  `MaritualStatus_id` INT NOT NULL ,
  `Marriage_id` INT NOT NULL ,
  `Spouse_id` BIGINT NULL ,
  `Address_id` INT NULL ,
  `Life_State_id` INT NOT NULL ,
  `Language_id` INT NOT NULL ,
  INDEX `Gender_id_fk` (`Gender_id` ASC) ,
  INDEX `MaritualStatus_id_fk` (`MaritualStatus_id` ASC) ,
  INDEX `Marriage_id_fk` (`Marriage_id` ASC) ,
  INDEX `Address_id_fk` (`Address_id` ASC) ,
  INDEX `Life_State_id_fk` (`Life_State_id` ASC) ,
  INDEX `fk_Member_Language1` (`Language_id` ASC) ,
  PRIMARY KEY (`Member_id`) ,
  CONSTRAINT `Gender_id_fk`
    FOREIGN KEY (`Gender_id` )
    REFERENCES `FIN`.`Gender` (`Gender_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `MaritualStatus_id_fk`
    FOREIGN KEY (`MaritualStatus_id` )
    REFERENCES `FIN`.`MaritualStatus` (`MaritualStatus_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Marriage_id_fk`
    FOREIGN KEY (`Marriage_id` )
    REFERENCES `FIN`.`Marriage` (`Marriage_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Address_id_fk`
    FOREIGN KEY (`Address_id` )
    REFERENCES `FIN`.`Address` (`Address_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Life_State_id_fk`
    FOREIGN KEY (`Life_State_id` )
    REFERENCES `FIN`.`Life_State` (`Life_State_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_Language1`
    FOREIGN KEY (`Language_id` )
    REFERENCES `FIN`.`Language` (`Language_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`PackageOption`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`PackageOption` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`PackageOption` (
  `PackageOption_id` INT NOT NULL AUTO_INCREMENT ,
  `Benefits` VARCHAR(225) NOT NULL COMMENT 'A list of of each an every package Option benefits.\n' ,
  PRIMARY KEY (`PackageOption_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:21
*/
INSERT INTO `PackageOption` (`Benefits`) VALUES ('10000');
INSERT INTO `PackageOption` (`Benefits`) VALUES ('20000');
INSERT INTO `PackageOption` (`Benefits`) VALUES ('30000');
INSERT INTO `PackageOption` (`Benefits`) VALUES ('40000');
INSERT INTO `PackageOption` (`Benefits`) VALUES ('50000');
INSERT INTO `PackageOption` (`Benefits`) VALUES ('60000');
-- -----------------------------------------------------
-- Table `FIN`.`Policy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Policy` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Policy` (
  `Policy_id` INT NOT NULL AUTO_INCREMENT ,
  `Policy_name` VARCHAR(45) NOT NULL ,
  `Policy_desc` VARCHAR(45) NOT NULL ,
  `PackageOption_id` INT NOT NULL ,
  PRIMARY KEY (`Policy_id`) ,
  INDEX `PackageOption_id_fk` (`PackageOption_id` ASC) ,
  CONSTRAINT `PackageOption_id_fk`
    FOREIGN KEY (`PackageOption_id` )
    REFERENCES `FIN`.`PackageOption` (`PackageOption_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:23
*/
INSERT INTO `Policy` (`Policy_name`,`Policy_desc`,`PackageOption_id`) VALUES ('A','Full cover',2);
INSERT INTO `Policy` (`Policy_name`,`Policy_desc`,`PackageOption_id`) VALUES ('B','Full cover & Extended member',2);
INSERT INTO `Policy` (`Policy_name`,`Policy_desc`,`PackageOption_id`) VALUES ('C','Maim member cover',1);

-- -----------------------------------------------------
-- Table `FIN`.`Member_Policy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Member_Policy` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Member_Policy` (
  `Member_Policy_id` INT NOT NULL AUTO_INCREMENT ,
  `Member_id` BIGINT NOT NULL ,
  `Policy_id` INT NOT NULL ,
  `Active` TINYINT(1) NOT NULL DEFAULT True ,
  `Remarks` VARCHAR(255) NULL ,
  `Creation_date` DATE NOT NULL COMMENT 'The date the policy was arquired.\n' ,
  PRIMARY KEY (`Member_Policy_id`) ,
  INDEX `Member_id_fk` (`Member_id` ASC) ,
  INDEX `Policy_id_fk` (`Policy_id` ASC) ,
  CONSTRAINT `Member_id_fk`
    FOREIGN KEY (`Member_id` )
    REFERENCES `FIN`.`Member` (`Member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Policy_id_fk`
    FOREIGN KEY (`Policy_id` )
    REFERENCES `FIN`.`Policy` (`Policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Method_of_contribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Method_of_contribution` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Method_of_contribution` (
  `Method_of_contribution_id` INT NOT NULL AUTO_INCREMENT ,
  `Method_of_contribution_name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Method_of_contribution_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:26
*/
INSERT INTO `Method_of_contribution` (`Method_of_contribution_name`) VALUES ('Debit Order');
INSERT INTO `Method_of_contribution` (`Method_of_contribution_name`) VALUES ('Persal');
INSERT INTO `Method_of_contribution` (`Method_of_contribution_name`) VALUES ('EFT');
INSERT INTO `Method_of_contribution` (`Method_of_contribution_name`) VALUES ('Cash');

-- -----------------------------------------------------
-- Table `FIN`.`Member_bank_account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Member_bank_account` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Member_bank_account` (
  `Member_bank_account_id` INT NOT NULL AUTO_INCREMENT ,
  `Main_member_id` BIGINT NOT NULL ,
  `Account_number` INT NOT NULL ,
  `Account_type` VARCHAR(45) NOT NULL ,
  `Bank` VARCHAR(45) NOT NULL ,
  `Branch_name` VARCHAR(45) NOT NULL ,
  `Branch_code` VARCHAR(45) NOT NULL ,
  `Personal_number` VARCHAR(45) NULL ,
  `Debit_date` DATE NOT NULL ,
  `Commence_date` DATE NOT NULL ,
  PRIMARY KEY (`Member_bank_account_id`) ,
  INDEX `Main_member_id_fk` (`Main_member_id` ASC) ,
  CONSTRAINT `Main_member_id_fk`
    FOREIGN KEY (`Main_member_id` )
    REFERENCES `FIN`.`Member` (`Member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Contribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Contribution` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Contribution` (
  `Contribution_id` INT NOT NULL AUTO_INCREMENT ,
  `Member_Policy_id` INT NOT NULL ,
  `Contribution_date` DATE NOT NULL ,
  `Contribution_amount` DECIMAL(10) NOT NULL DEFAULT 0.00 ,
  `Outstanding_amount` DECIMAL(10) NOT NULL DEFAULT 0.00 ,
  `Method_of_contribution_id` INT NOT NULL ,
  `Member_bank_account_id` INT NULL ,
  PRIMARY KEY (`Contribution_id`) ,
  INDEX `Member_Policy_id_fk` (`Member_Policy_id` ASC) ,
  INDEX `Method_of_contribution_id_fk` (`Method_of_contribution_id` ASC) ,
  INDEX `Member_bank_account_id_fk` (`Member_bank_account_id` ASC) ,
  CONSTRAINT `Member_Policy_id_fk`
    FOREIGN KEY (`Member_Policy_id` )
    REFERENCES `FIN`.`Member_Policy` (`Member_Policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Method_of_contribution_id_fk`
    FOREIGN KEY (`Method_of_contribution_id` )
    REFERENCES `FIN`.`Method_of_contribution` (`Method_of_contribution_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Member_bank_account_id_fk`
    FOREIGN KEY (`Member_bank_account_id` )
    REFERENCES `FIN`.`Member_bank_account` (`Member_bank_account_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Premium`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Premium` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Premium` (
  `Premium_id` INT NOT NULL AUTO_INCREMENT ,
  `Years` INT NOT NULL DEFAULT 0 ,
  `Premium_amount` INT NOT NULL ,
  `PackageOption_id` INT NOT NULL ,
  PRIMARY KEY (`Premium_id`), 
  INDEX `Pofk` (`PackageOption_id` ASC) ,
  CONSTRAINT `Pofk`
    FOREIGN KEY (`PackageOption_id` )
    REFERENCES `FIN`.`PackageOption` (`PackageOption_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-03-05 18:21
*/
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (1,200,1);
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (2,210,2);
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (3,250,3);
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (4,260,4);
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (5,265,5);
INSERT INTO `Premium` (`Years`,`Premium_amount`,`PackageOption_id`) VALUES (6,270,6);


-- -----------------------------------------------------
-- Table `FIN`.`Extendend_Member_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Extendend_Member_Type` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Extendend_Member_Type` (
  `Ext_memberType_id` INT NOT NULL AUTO_INCREMENT ,
  `Ext_memberType_descr` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`Ext_memberType_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:13
*/
INSERT INTO `Extendend_Member_Type` (`Ext_memberType_descr`) VALUES ('Spouse');
INSERT INTO `Extendend_Member_Type` (`Ext_memberType_descr`) VALUES ('Benefiary');
INSERT INTO `Extendend_Member_Type` (`Ext_memberType_descr`) VALUES ('Depandant');
INSERT INTO `Extendend_Member_Type` (`Ext_memberType_descr`) VALUES ('Extended family');

-- -----------------------------------------------------
-- Table `FIN`.`Extendend_Member`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Extendend_Member` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Extendend_Member` (
  `Ext_member_id` BIGINT NOT NULL ,
  `Ext_member_name` VARCHAR(45) NOT NULL ,
  `Ext_member_surname` VARCHAR(45) NOT NULL ,
  `Ext_memberType_id` INT NOT NULL ,
  `Relationship` VARCHAR(45) NOT NULL ,
  `Life_state_id` INT NOT NULL ,
  `Title` VARCHAR(45) NOT NULL ,
  `Date_of_birth` DATE NOT NULL ,
  PRIMARY KEY (`Ext_member_id`) ,
  INDEX `Ext_memberType_id_fk` (`Ext_memberType_id` ASC) ,
  INDEX `Life_state_id_fk2` (`Life_state_id` ASC) ,
  CONSTRAINT `Ext_memberType_id_fk`
    FOREIGN KEY (`Ext_memberType_id` )
    REFERENCES `FIN`.`Extendend_Member_Type` (`Ext_memberType_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Life_state_id_fk2`
    FOREIGN KEY (`Life_state_id` )
    REFERENCES `FIN`.`Life_State` (`Life_State_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Claim_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Claim_status` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Claim_status` (
  `Claim_status_id` INT NOT NULL AUTO_INCREMENT ,
  `Claim_status_description` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Claim_status_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:29
*/
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('In progress');
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('Paid out');
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('Rejected');
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('Pending');
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('Accepted');
INSERT INTO `Claim_status` (`Claim_status_description`) VALUES ('Withdrawn');

-- -----------------------------------------------------
-- Table `FIN`.`Claims`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Claims` ;
CREATE  TABLE IF NOT EXISTS `FIN`.`Claims` (
  `Claim_id` INT NOT NULL AUTO_INCREMENT ,
  `Claim_status_id` INT NOT NULL ,
  `Member_policy_id` INT NOT NULL COMMENT 'The member can be a ONLY be a benefiary,the main member, or the spouse.' ,
  `Claim_description` VARCHAR(45) NOT NULL ,
  `Updated_date` DATE NOT NULL ,
  `Amount_claimed` INT NOT NULL ,
  `Ampunt_paid` INT NOT NULL ,
  `Claim_for_id` BIGINT NOT NULL ,
  `Claim_for_main_member` TINYINT(1) NULL ,
  `claim_by_id` BIGINT NULL ,
  PRIMARY KEY (`Claim_id`) ,
  INDEX `Claim_status_id_fk` (`Claim_status_id` ASC) ,
  INDEX `Member_policy_id_fk` (`Member_policy_id` ASC) ,
  CONSTRAINT `Claim_status_id_fk`
    FOREIGN KEY (`Claim_status_id` )
    REFERENCES `FIN`.`Claim_status` (`Claim_status_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Member_policy_id`
    FOREIGN KEY (`Member_policy_id` )
    REFERENCES `FIN`.`Member_Policy` (`Member_Policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `FIN`.`Extendend_Member_Policy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Extendend_Member_Policy` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Extendend_Member_Policy` (
  `Main_member_policy_id` INT NOT NULL ,
  `Ext_member_id` BIGINT NOT NULL ,
  `Premium_id` INT NULL ,
  INDEX `Main_member_policy_fk` (`Main_member_policy_id` ASC) ,
  INDEX `Ext_member_fk` (`Ext_member_id` ASC) ,
  INDEX `Premium_fk` (`Premium_id` ASC) ,
  PRIMARY KEY (`Main_member_policy_id`, `Ext_member_id`) ,
  CONSTRAINT `Main_member_policy_fk`
    FOREIGN KEY (`Main_member_policy_id` )
    REFERENCES `FIN`.`Member_Policy` (`Member_Policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Ext_member_fk`
    FOREIGN KEY (`Ext_member_id` )
    REFERENCES `FIN`.`Extendend_Member` (`Ext_member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Premium_fk`
    FOREIGN KEY (`Premium_id` )
    REFERENCES `FIN`.`Premium` (`Premium_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`Occupation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Occupation` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Occupation` (
  `Occupation_id` INT NOT NULL AUTO_INCREMENT ,
  `Occupation_name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`Occupation_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:55
*/
INSERT INTO `Occupation` (`Occupation_name`) VALUES ('Employed');
INSERT INTO `Occupation` (`Occupation_name`) VALUES ('Unemployed');
INSERT INTO `Occupation` (`Occupation_name`) VALUES ('Selfemployed');
INSERT INTO `Occupation` (`Occupation_name`) VALUES ('Student');


-- -----------------------------------------------------
-- Table `FIN`.`Source_of_income`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Source_of_income` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Source_of_income` (
  `Source_of_income_id` INT NOT NULL AUTO_INCREMENT ,
  `Source_of_income_name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`Source_of_income_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:18
*/
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Self employed');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Pension');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Social grants');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Trust fund');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Employment income');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Child support');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Disability benefit');
INSERT INTO `Source_of_income` (`Source_of_income_name`) VALUES ('Parent income');

-- -----------------------------------------------------
-- Table `FIN`.`Member_income`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Member_income` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Member_income` (
  `Member_income_id` INT NOT NULL AUTO_INCREMENT ,
  `Source_of_income_id` INT NOT NULL ,
  `Occupation_id` INT NOT NULL ,
  `Member_id` BIGINT NOT NULL ,
  `Income_provider_name` VARCHAR(255) NULL ,
  `Telelphone_nr` VARCHAR(45) NULL ,
  `Email` VARCHAR(255) NULL ,
  PRIMARY KEY (`Member_income_id`) ,
  INDEX `fk_Member_income_Source_of_income1` (`Source_of_income_id` ASC) ,
  INDEX `fk_Member_income_Occupation1` (`Occupation_id` ASC) ,
  INDEX `fk_Member_income_Member1` (`Member_id` ASC) ,
  CONSTRAINT `fk_Member_income_Source_of_income1`
    FOREIGN KEY (`Source_of_income_id` )
    REFERENCES `FIN`.`Source_of_income` (`Source_of_income_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_income_Occupation1`
    FOREIGN KEY (`Occupation_id` )
    REFERENCES `FIN`.`Occupation` (`Occupation_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_income_Member1`
    FOREIGN KEY (`Member_id` )
    REFERENCES `FIN`.`Member` (`Member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FIN`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`User` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`User` (
  `User_id` INT NOT NULL AUTO_INCREMENT ,
  `User_name` VARCHAR(255) NULL ,
  `User_password` VARCHAR(255) NULL ,
  PRIMARY KEY (`User_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:08
*/
INSERT INTO `User` (`User_name`,`User_password`) VALUES ('admin','admin');

-- -----------------------------------------------------
-- Table `FIN`.`Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`Role` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`Role` (
  `Role_id` INT NOT NULL AUTO_INCREMENT ,
  `Role_name` VARCHAR(255) NULL ,
  PRIMARY KEY (`Role_id`) )
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:10
*/
INSERT INTO `Role` (`Role_name`) VALUES ('Administration');

-- -----------------------------------------------------
-- Table `FIN`.`User_Roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FIN`.`User_Roles` ;

CREATE  TABLE IF NOT EXISTS `FIN`.`User_Roles` (
  `User_role_id` INT NOT NULL AUTO_INCREMENT ,
  `User_id` INT NOT NULL ,
  `Role_id` INT NOT NULL ,
  PRIMARY KEY (`User_role_id`) ,
  INDEX `fk_User_Roles_User1` (`User_id` ASC) ,
  INDEX `fk_User_Roles_Role1` (`Role_id` ASC) ,
  CONSTRAINT `fk_User_Roles_User1`
    FOREIGN KEY (`User_id` )
    REFERENCES `FIN`.`User` (`User_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Roles_Role1`
    FOREIGN KEY (`Role_id` )
    REFERENCES `FIN`.`Role` (`Role_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

/*
-- Query: 
-- Date: 2012-02-29 20:11
*/
INSERT INTO `User_Roles` (`User_id`,`Role_id`) VALUES (1,1);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
