<?php
date_default_timezone_set('Africa/Johannesburg');
require 'database.php';
$url = getbaseurl();
$url = str_replace("script_emailproofofpayment.php",'',$url);

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$message = '';
$list = getpost('list');
echo $url;
// -- PaymentID
$paymentid =$list['paymentid'];

if(!empty($paymentid))
{
	// -- Payment was made.
	$message = 'success|'.$paymentid;	
	// -- SMS
	if($list['Notify'] == 'SMS')
	{
		$smstemplatename = 'payments_notification';
		$applicationid = 0;		
		$contents = auto_template_content($userid,$smstemplatename,$applicationid,$list['Bank_Reference'],$list['Action_Date']);
		if(!empty($contents) && !empty($list['Contact_No']))
		{	
			$notification['phone'] 		 	= $list['Contact_No'];
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= $list['Bank_Reference'];	
			$notification['usercode'] 	 	= $list['UserCode'];
			$notification['createdby'] 	 	= $list['createdby'];
			$notification['IntUserCode'] 	= $list['IntUserCode'];
			$notification['amount'] 		= $list['Amount'];
			$notification['Account_Holder'] = $list['Account_Holder'];										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList); }
		}
	}
	
	// -- EMAIL
	if($list['Notify'] == 'EMAIL')
	{
		ob_start();
		$paymentidEncode = urlencode(base64_encode($paymentid));
		$proofofpaymenturl = $url.'eproofofpayment.php?paymentid='.$paymentidEncode;
		$subject     = 'Proof of Payment';
		$filename 	 = 'emailtemplate_proofofpayment.html';
		$IntUserCode = $list['IntUserCode'];
		$email 		 = $list['Contact_No'];
		$amount 	 = number_format($list['Amount'],2);
		$phone		 = '';
		//$filecontent = file_get_contents($filename);
		include $filename;
		$filecontent = ob_get_contents(); // get contents of buffer
		$filecontent = str_replace("[Account_Holder]",$list['Account_Holder'],$filecontent);
		$filecontent = str_replace("[companyname]",'eCashMeUp',$filecontent);
		$filecontent = str_replace("[currency]",'R',$filecontent);
		$filecontent = str_replace("[Amount]",$amount,$filecontent);		
		$filecontent = str_replace("[Client_Reference_1]",$list['Client_Reference_1'],$filecontent);
		$filecontent = str_replace("[URL]",$proofofpaymenturl,$filecontent);
		$content 	 = $filecontent;
		$status 	 = 'success';	
		SendEmail($content,$email,$subject);
	    AddCommunicatioHistoryGlobal($content,$IntUserCode,$IntUserCode,$IntUserCode,$status,$email,$phone,'email');		
		ob_end_clean();
	}
}
else
{
	$message = 'ERROR|'.$rowData[1];
}

/*if(isset($dataPayBenefiaryObj['benefiaryid']))
{
	// -- Payment warning message.
	$message =  $message.'|Warning:Payment to Beneficiary already exist for '.
	$dataPayBenefiaryObj['Action_Date'].' to bank account number '.
	$dataPayBenefiaryObj['Account_Number'].' with client reference '.
	$dataPayBenefiaryObj['Client_Reference_1'];			
}*/
 echo $message; 
?>