<?php
// -- 17/12/2021 -- csv files.
if(!function_exists('bankstatement_processcsvfile'))
	{
		function bankstatement_processcsvfile($tmp_name)
		{
			$output = null;
			$column = null;
			$row_data = null;
			$emailContents = null;
			$contents = null;
			$message = '';
			//$tmp_name    = '';
			$separator = '';
			try{

			if(!empty($tmp_name))
			{
			 $separator = getCsvDelimiter($tmp_name);
			 $file_data = fopen($tmp_name, 'r');
			 $column = fgetcsv($file_data,0,$separator);
			 array_unshift($column,'Feedback');
			 array_unshift($column,'UserCode');
			 array_unshift($column,'Filename');
			 $colCount    = count($column) - 3;
			 $colCountAft = count($column);
			 array_unshift($column,'Count');
			 $rowCount = 1;

			$file_parts = pathinfo($tmp_name);
			//print_r($file_parts);
			$filename     = $file_parts['basename'];
			$fileusercode = substr($filename,0,4);
				$firstRow = true;

			 while($row = fgetcsv($file_data,0,$separator))
			 {
				// -- Scan the File Convert all record to UTF-8
				for($x=0;$x<$colCount;$x++ )
				{
					 $row[$x] = scanfile_to_UTF8($row[$x]);
				}
				//$message = extractcsvfileIinstructions($row,$cachedData);
				$message = '';
				array_unshift($row,$message);
				array_unshift($row,$fileusercode);
				array_unshift($row,$filename);
				array_unshift($row,$rowCount);
				//print_r($row);

				// -- Check if Record Exist first.
				if(empty(get_bankstatement_record($row)))
				{
					// -- If does not exit create a new entry.
					$message = create_bankstatement_record($row);
					//$message = 'success';
					//echo $message;
				}
				else
				{
					$message = 'Duplicate';

				}
				array_unshift($row,$message);
				$rowCount = $rowCount + 1;
				$row_data[] = $row;

			 }
			 /*$output = array(
			  'column'  => $column,
			  'row_data'  => $row_data
			 );*/
			  $output = $row_data;
					// -- EOC 29.05.2021
			//$filenameEncryp = moveuploadedfile($name,$tmp_name);
			}
					  //$output = json_encode($output);
			}
			catch (Exception $e)
				{
					$message = $e->getMessage();
				}

		 return $output;
	  }
	}
// -- Scan the File Convert all record to UTF-8
	if(!function_exists('scanfile_to_UTF8'))
	{
	   function scanfile_to_UTF8($data)
	   {
			$data = iconv('UTF-8', 'ASCII//IGNORE', $data);
			$data = trim($data);
			return $data;
	   }
	}

	////////////////////////////////////////////////////////////////////////
	if(!function_exists('getCsvDelimiter'))
	{
	   function getCsvDelimiter($filePath)
	   {
		  $checkLines = 1;
		  $delimiters =[',', ';', '\t'];

		  $default =',';

		   $fileObject = new \SplFileObject($filePath);
		   $results = [];
		   $counter = 0;
		   while ($fileObject->valid() && $counter <= $checkLines) {
			   $line = $fileObject->fgets();
			   foreach ($delimiters as $delimiter) {
				   $fields = explode($delimiter, $line);
				   $totalFields = count($fields);
				   if ($totalFields > 1) {
					   if (!empty($results[$delimiter])) {
						   $results[$delimiter] += $totalFields;
					   } else {
						   $results[$delimiter] = $totalFields;
					   }
				   }
			   }
			   $counter++;
		   }
		   if (!empty($results)) {
			   $results = array_keys($results, max($results));

			   return $results[0];
		   }
	return $default;
	}
	}
if(!function_exists('create_bankstatement_record'))
	{
		function create_bankstatement_record($dataFilter)
		{
			////////////////////////////////////////////////////////////////////////
			// -- Database Declarations and config:

			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			////////////////////////////////////////////////////////////////////////
			$message = '';
			$id  = 0;
			$ret = '';
			//print_r( $dataFilter);print array
			try
			{
			$tbl = 'bankstatements';
			$sql = "INSERT INTO $tbl (user_reference,usercode,transaction_date,amount,default_1,account_balance,default_2,file_sequence_number,transaction_code,account_number,filename) VALUES(:user_reference,:usercode,:transaction_date,:amount,:default_1,:account_balance,:default_2,:file_sequence_number,:transaction_code,:account_number,:filename)";
			$q = $pdo->prepare($sql);
			$q->execute(array(':user_reference'=>$dataFilter[4],':usercode'=>$dataFilter[2],':transaction_date'=>$dataFilter[5],':amount'=>$dataFilter[6],':default_1'=>$dataFilter[7],':account_balance'=>$dataFilter[8],':default_2'=>$dataFilter[9],':file_sequence_number'=>$dataFilter[10],':transaction_code'=>$dataFilter[11],
			':account_number'=>$dataFilter[12],':filename'=>$dataFilter[1]));
			/*array(
			$dataFilter[0],
			$dataFilter[1],
			$dataFilter[2],
			$dataFilter[3],
			$dataFilter[4],
			$dataFilter[5],
			$dataFilter[6],
			$dataFilter[7],
			$dataFilter[8],
			$dataFilter[9],
			$dataFilter[10],
			$dataFilter[11],
			$dataFilter[12]));*/
			Database::disconnect();
			$message = 'success';

			}
		  catch (Exception $e)
		  {
			$message = 'Caught exception: '.  $e->getMessage(). "\n";
		  }
			return $message;
	   }
	}

	if(!function_exists('get_bankstatement_record'))
	{
		function get_bankstatement_record($dataFilter)
		{
		   try
		   {
			// -- check with benefiaryid,Client_Reference_1, Account_Number,Action_Date,IntUserCode.
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl  = 'bankstatements';
		//	$sql = "select * from $tbl where
		//	count  = ? AND filename  = ? AND usercode  = ?  AND user_reference  = ?
		//	AND transaction_date  = ? AND amount  = ? AND default  = ? AND account_balance  = ? AND
		//	default_2  = ? AND file_sequence_number  = ? AND transaction_code = ? AND account_number = ?";
	 	$sql = "select * from $tbl where count  = ?";
		//	count  = ?
			$q = $pdo->prepare($sql);
			$q->execute($dataFilter);/*array($dataFilter[0],$dataFilter[1],$dataFilter[2],$dataFilter[3],
							  $dataFilter[4],$dataFilter[5],$dataFilter[6],$dataFilter[7],
							  $dataFilter[8],$dataFilter[9],$dataFilter[10],$dataFilter[11],
							  $dataFilter[12]));*/
			$data = $q->fetch(PDO::FETCH_ASSOC);
						}
		  catch (Exception $e)
		  {
			$message = 'Caught exception: '.  $e->getMessage(). "\n";
		  }
			return $data;
		}
	}
	////////////////////////////////////// bankstatement_processcsvfile  CSV files ////////////////////////////////
// -- 17/12/2021 -- csv files.
if(!function_exists('convertActionDate'))
{
	// -- Convert from YYMMDD to YYYY-MM-DD
function convertActionDate($yyddmm)
{
$yyy_dd_mm = '';
$yyy_dd_mm = '20'.substr($yyddmm,0,2)."-".substr($yyddmm,2,2)."-".substr($yyddmm,4,2);

return $yyy_dd_mm;
}
}
if(!function_exists('lreplace'))
{
	function lreplace($search, $replace, $subject)
	{
		$pos = strrpos($subject, $search);
		if($pos !== false){
			$subject = substr_replace($subject, $replace, $pos, strlen($search));
		}
		return $subject;
	}
}
if(!function_exists('getbaseurl'))
{
function getbaseurl()
{
$url = '';
// -- Require https
if (isset($_SERVER['HTTPS']) != "on")
{
$url = "http://";
}
else
{
$url = "https://";
}

$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url2);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
$dir .= $parts[$i] . "/";
}
return $url.$dir;
}
}
$rootUrl = getbaseurl();// -- str_replace("/ecashmeup","",getbaseurl());

		error_reporting( E_ALL );
		ini_set('display_errors', 1);

$title='Script Upload EFT Bank Unpaid Mecantile';
// -- Declarations
require 'database.php';
$valid_formats = array("upl","txt","xpresp");
$max_file_size = 1024*10000; //10MB
$path 		= "Files/Collections/In/"; // -- Download directory
$backuppath = "Files/Collections/Backup/In"; // -- Backup directory
$filename 	= '';
// -- Read files from FILE/In with .UPL extenstion

// -- Read files from FILE/In with .txt extenstion

// -- Move files to backup directory..
if(!function_exists('move_file'))
{
 function move_file($file, $to)
 {
    $path_parts = pathinfo($file);
    $newplace   = "$to/{$path_parts['basename']}";
    if(rename($file, $newplace))
        return $newplace;
    return null;
 }
}

// -- send notification unpaid...
if(!function_exists('send_notification_unpaid'))
{
 function send_notification_unpaid($unpaidlist)
 {
	$tablename = 'customer';
	$dbnameobj = 'ecashpdq_eloan';
	$subject   = 'Unpaid Debit Orders - '.date('Y-m-d');
	$filename 	 	   = 'emailtemplate_unpaidlist.html';
	$filecontent 	   = 'emailtemplate_unpaidlist.html';
	$filefooter = 'emailtemplate_unpaidlist_footer.html';
	$filecontentfooter = file_get_contents($filefooter);
	$rowData 		   = '';
	//var_dump($unpaidlist);
	$htmlEmail   = array();
	$arrayEmails = array();
	$htmlEmailwithFooter = array();
	$InterUserCode 		 = '';
	$index = '';
	foreach($unpaidlist as $row)
	{
		$index = "'".$row['IntUserCode']."'";
		if(empty($InterUserCode))
		{
			$InterUserCode = $row['IntUserCode'];
		}

		if(!isset($htmlEmail[$index]))
		{
			$filecontent = file_get_contents($filename);
			$filecontent = str_replace("[IntUserCode]",$row['IntUserCode'],$filecontent);
			$htmlEmail[$index ] = $filecontent;
			$rowData	 = '<tr><td>'.$row['Client_Reference_1'].
			'</td><td>'.$row['Account_Holder'].'</td><td>'.$row['Amount'].'</td><td>'.$row['Action_Date'].'</td><td>'.$row['Status_Description'].'</td></tr>';
			$htmlEmail[$index] = $htmlEmail[$index].$rowData;
		}
		else
		{
			$rowData	 = '<tr><td>'.$row['Client_Reference_1'].
			'</td><td>'.$row['Account_Holder'].'</td><td>'.$row['Amount'].'</td><td>'.$row['Action_Date'].'</td><td>'.$row['Status_Description'].'</td></tr>';
			$htmlEmail[$index] = $htmlEmail[$index].$rowData;
		}
	}
	//print_r($htmlEmail);
	// -- Unpaid/Paid emails.
	foreach($htmlEmail as $row)
	{
		$htmlEmailwithFooter[] = $row.$filecontentfooter;
	}
	$message = implode(" ",$htmlEmailwithFooter);

	// -- Get Email to reciepients
	$emails = GetEmailRecipients($InterUserCode,$dbnameobj,$tablename);
	// -- Send Email.
	if(!empty($emails))
	{
	  // -- Log Email sent to recipients.
	  foreach($emails as $row)
	  {
		$email = $row['email2'];
		SendEmail($message,$row['email2'],$subject);
	    $phone = $row['phone2'];
	    $status = 'success';
	    AddCommunicatioHistoryGlobal($message,$InterUserCode,$InterUserCode,$InterUserCode,$status,$email,$phone,'email');
	  }
	 }
 }
}

if(!function_exists('read_files'))
{
// -- Read directory for list of files.
function read_files($dir)
{
	$files = null;
	$fileCount = 0;
	if (is_dir($dir))
	{
		if ($dh = opendir($dir))
		{
			while (($file = readdir($dh)) !== false) {
				if(file_exists($file) == 0)
				{
				  //echo "filename: ".$file."<br />";
				  $files[$fileCount] = $file;
				  $fileCount = $fileCount + 1;
				}
			}
			closedir($dh);
		}
	}
	return $files;
}
}
// -- files read are sorted based on date & time in ASC or DESC
if(!function_exists('read_files_date_sorted'))
{
	// -- Read directory for list of files.
	function read_files_date_sorted($dir)
	{
		// current directory
		$homedir = getcwd();//'/xampp/htdocs/ecashmeup';// . "\n";
		str_replace("/", DIRECTORY_SEPARATOR, $homedir);
		$files = null;
		// -- make files path make path to be able to sort
		chdir($dir);
		array_multisort(array_map('filemtime', ($files = glob("*.*"))), SORT_ASC, $files);
		// -- restore the root directory
		chdir($homedir);
		return $files;
	}
}
// -- flat file to an array.
if(!function_exists('flatfile_in_arrayOUT'))
{
	function flatfile_in_arrayOUT($url,$ignore,$delimiter)
	{
		$out = null;
		$flatfile = file($url);   // ---- Flatfile rows to array ----
		$index = 0;
		$z = 0;
		// -- ignore which header rows.
		//print_r($flatfile);

		// Loop through our array.
		foreach ($flatfile as $line_num => $line)
		{
			// -- ignore header lines
			if($line_num >= $ignore && !empty(htmlspecialchars($line)))
			{
				$flatfilexrow[$index] = $line;
				$flatfilexrow[$index] = chop($flatfilexrow[$index]);
				$flatfile_data[$index] = explode($delimiter,$flatfilexrow[$index]);
				$i=0;
				/*foreach($flatfile_data as $key)
				{
					$out[$z][$i] = $flatfile_data[$index][$i];
					$i++;
					//print_r($flatfile_data);
				}  */
				//print_r($flatfile_data);
				$index = $index + 1;
			}
		}
		return $flatfile_data;
	}
}

// -- GetCollectinobjout
if(!function_exists('GetCollectinobjout'))
{
	function GetCollectinobjout($accountnumber,$actiondate,$dbnameobj,$tablename)
	{
		$queryFields = null;
		$arrayCollections = null;
		$whereArray = null;
		$whereArray[] = "Account_Number = '{$accountnumber}'"; // -- Action Date.
		$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
		$queryFields[] = '*';
		$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
		return $collectionObj;
	}
}

// -- GetEmailRecipients
if(!function_exists('GetEmailRecipients'))
{
	function GetEmailRecipients($InterUserCode,$dbnameobj,$tablename)
	{
		$space = '';
		$queryFields = null;
		$arrayCollections = null;
		$whereArray = null;
		$whereArray[] = "IntUserCode = '{$InterUserCode}'"; // -- InterUserCode
		$whereArray[] = "email2 <> '{$space}'"; // -- Email2.
		$queryFields[] = '*';
		$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
		return $collectionObj;
	}
}

// -- flat file with OUT error to an array.
if(!function_exists('flatfile_in_error'))
{
	function flatfile_in_error($url,$ignore,$delimiter)
	{
		$out = null;
		$flatfile = file($url);   // ---- Flatfile rows to array ----
		$index = 0;
		$z = 0;
		$flatfile_data = null;
		// -- ignore which header rows.
		//print_r($flatfile);

		// Loop through our array.
		foreach ($flatfile as $line_num => $line)
		{
			// -- ignore header lines
			if($line_num >= $ignore && !empty(htmlspecialchars($line)))
			{
				$delimiter = "/END";
				$line = $line.$delimiter;
				//print(strlen($line).'=>');
				//$flatfilexrow[$index] = chop($flatfilexrow[$index]);
				// if is not an empty line in the file.
				// lenght 6 is empty line only with " /END"
				//if(strlen($line) > 6) -- 08.02.2021
				{
				$flatfilexrow[$index] = $line.$delimiter;
				//print($line);
				$flatfile_data[$index] = explode($delimiter,$flatfilexrow[$index]);
				$i=0;
				/*foreach($flatfile_data as $key)
				{
					$out[$z][$i] = $flatfile_data[$index][$i];
					$i++;
					//print_r($flatfile_data);
				}  */
				//print_r($flatfile_data);
				$index = $index + 1;
				}
			}
		}
		return $flatfile_data;
	}
}
// -- flat file to an array.

// -- Getbankrespcodeout
if(!function_exists('Getbankrespcodeout'))
{
	function Getbankrespcodeout($statusCode,$dbnameobj,$tablename)
	{
		$queryFields = null;
		$whereArray = null;
		$queryFields[] = '*';
		$whereArray[] = "bankresponsecode = '{$statusCode}'"; // -- Bank Response Code.
		$tablename = 'bankresponsecodes';
		$bankresponsecodeObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;
		return $bankresponsecodeObj ;
	}
}

// -- updated --
// -- bankqueue --
// -- updatebankqueuefiles
if (!function_exists('updatebankqueuefiles'))
{
	function updatebankqueuefiles($filenamewithparts,$filename,$status,$RidCodeArray,$FooterArray)
	{
	 try
	  {
		// -- search_dynamic by UserCode & gennumber() by substr 1st four(4) usercode & next four(4) gen number.
		$gennumber = substr($filename,4,4);
		$gennumber = ltrim($gennumber, "0");  // -- remove leading zeros
		$usercodefromfile = substr($filename,0,4);
		// -- 13/05/2022 -- //		
		$accepted_pre_check  = ''; 
		$accepted_processing = '';
		
		//print_r($FooterArray);
		if(!empty($FooterArray))
		{
			//print_r($FooterArray);
		if(in_array("*** FILE ACCEPTED FOR PROCESSING ***", $FooterArray))
		{
			$accepted_processing = '*** FILE ACCEPTED FOR PROCESSING ***';
		}
		if(in_array("*** FILE ACCEPTED ON PRE-CHECK ***", $FooterArray))
		{
			$accepted_pre_check  = '*** FILE ACCEPTED ON PRE-CHECK ***';	
	    }}
				
		// -- 13/05/2022 -- //
		$errorcheck = strtolower($status);

		$arrayCollections[1] = '';

		if(strpos($errorcheck,'error') !== false) //-- Rejected
		{
			$arrayCollections[1] = 'rejected';
		}
		else // Accepted
		{
			$arrayCollections[1] = 'accepted';
		}

		// -- if both 50 & 52 RecordId Code then its a partial file.
		if(in_array("50", $RidCodeArray) && in_array("52", $RidCodeArray))
		{
			$arrayCollections[1] = 'partial';
		}
		//print('<br/>');
		//print_r($RidCodeArray);

		//$usercodefromfile  = '';
		//echo $gennumber .' '.$usercodefromfile;
		// -- Acknowledge the received from bank.
		$arrayCollections[2] = '../'.$filenamewithparts;
		$arrayCollections[3] = $status;

		$idbankqueue    = '';
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// -- Search for if value exist.
		$sql = 'SELECT * FROM bankqueue WHERE gennumber = ? and usercode = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($gennumber,$usercodefromfile));
		$data = $q->fetch(PDO::FETCH_ASSOC);
// ---------------------------------------------------------------------------
	   if(isset($data['accepted_processing'])) // -- Dont note overwrite #1...
	   {
		   if(!empty($data['accepted_processing'])){$accepted_processing = $data['accepted_processing'];}
	   }
	   if(isset($data['accepted_pre_check'])) // -- Dont note overwrite #2...
	   {
		   if(!empty($data['accepted_pre_check'])){$accepted_pre_check = $data['accepted_pre_check'];}
	   }
// ---------------------------------------------------------------------------	   
	  // -- Update Record if found
	  if(isset($data['idbankqueue']))
		{
			$idbankqueue = $data['idbankqueue'];
		// -- Update
		$sql = "UPDATE bankqueue
		SET
		status = ?,
		filename_out = ?,
		comment = ?,
		accepted_processing = ?,
		accepted_pre_check = ?
		WHERE idbankqueue = ?";
		$q = $pdo->prepare($sql);
		//echo $sql;
		//print_r($arrayCollections);
		$q->execute(array(
		$arrayCollections[1],
		$arrayCollections[2],
		$arrayCollections[3],
		$accepted_processing,
		$accepted_pre_check,
		$idbankqueue));
		}
		// -- Create file entry.
		else
		{
// -- Insert
			$timeprocessed   = Date('Y-m-d H:m:s');
			$ack 	     	 = $arrayCollections[1];
			$transactiontype = 'Debit Order';

			$sql = 'INSERT INTO bankqueue (filename,usercode,encodeddata,timeprocessed,transactiontype,gennumber,
			status,filename_out,comment,accepted_processing,accepted_pre_check)
			VALUES (?,?,?,?,?,?,?,?,?,?,?)';
			$q = $pdo->prepare($sql);
			$q->execute(array('',
			$usercodefromfile,
			'',
			$timeprocessed,
			$transactiontype,
			$gennumber,
			$ack,
			$arrayCollections[2],
			$status,
			$accepted_processing,
			$accepted_pre_check));
		}
		// -- send an error message as SMS/Email
	  }
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

if(!function_exists('get_file_out_error'))
{
	function get_file_out_error($filename_with_path,$delimiter)
	{
		$errorMessage = '';
		$countError = 0;
		$flatfiledataError = flatfile_in_error($filename_with_path,0,$delimiter);
		$countError = count($flatfiledataError);
		// -- Row
		for($x=0; $x<$countError; $x++)
		{ // -- Column
			$countInside = arraycolumncounter($flatfiledataError);
			for($z=0;$z<$countInside;$z++)
			{
				// -- Error Message
				// sample : Error File Gen Number out of Sync. Expected : 328. Found : 327.
				if($x == 7)
				{
					$msg = strtolower($flatfiledataError[$x][$z]);
					if (strpos($msg, 'error') !== false)
					{
							$errorMessage = $flatfiledataError[$x][$z];
					}
					return $errorMessage;
				}
			}
			//echo "<br/>";
		}
		return $errorMessage;
										//print_r($flatfiledataError);
	}
}
// -- updateprocessedfiles
if (!function_exists('updateprocessedfiles'))
{
	function updateprocessedfiles($filenamewithparts,$filename,$status)
	{
	 try
	  {
		// -- search_dynamic by UserCode & gennumber() by substr 1st four(4) usercode & next four(4) gen number.
		$gennumber = substr($filename,4,4);
		$usercodefromfile = substr($filename,0,4);
		//$usercodefromfile = '';
		//echo $gennumber .' '.$usercodefromfile;
		// -- Acknowledge the received from bank.
		$arrayCollections[1] = 'received';
		$arrayCollections[2] = '../'.$filenamewithparts;

		$idprocessedfiles = '';
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Search for if value exist.
		$sql = 'SELECT * FROM processedfiles WHERE gennumber = ? and usercode = ?';
		$q = $pdo->prepare($sql);
		$q->execute(array($gennumber,$usercodefromfile));
		$data = $q->fetch(PDO::FETCH_ASSOC);

			$timeprocessed   = Date('Y-m-d H:m:s');
			//$status 	     = 'processed';
			$ack   		     = 'received';
			$transactiontype = 'Debit Order';
			$idbankqueue = '00';
	  if(isset($data['idprocessedfiles']))
		{
			$idprocessedfiles = $data['idprocessedfiles'];

// -- Update
		$sql = "UPDATE processedfiles
		SET
		ack = ?,
		bankresponsefile = ?
		WHERE idprocessedfiles = ?";
		$q = $pdo->prepare($sql);
		//echo $sql;
		//print_r($arrayCollections);
		$q->execute(array(
		$arrayCollections[1],
		$arrayCollections[2],
		$idprocessedfiles));
		}
		// -- Create file entry.
		else
		{
// -- Insert
			$sql = 'INSERT INTO processedfiles (usercode,timeprocessed,status,idbankqueue,transactiontype,gennumber,ack,bankresponsefile)
			VALUES (?,?,?,?,?,?,?,?)';
			$q = $pdo->prepare($sql);
			$q->execute(array(
			$usercodefromfile,
			$timeprocessed,
			$status,
			$idbankqueue,
			$transactiontype,
			$gennumber,
			$arrayCollections[1],
			$filename));
		}
				Database::disconnect();

		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

// -- Process DebitCheck File Responses
if (!function_exists('process_acresp_file'))
{
	function process_acresp_file($filename_with_path,$backuppath)
	{
	 try
	  {
		  $html 		= "";
		  $errorMessage = "success";
		  $FileMove 	= false;
		  $RId			= "DEBICHECK";
		  $flatfiledata = null;
// -- File content
		  $ignoreHeaderLines = 0; // -- number of first lines
		  $delimiter = " ";
// -- Filedata Declarations
		$count  = 0;
		$x		= 0;
		$z 		= 0;
		$actiondate    = "";
		$statusCode    = '';
		$accountnumber = '';
// -- Content the file content into an array[]
		  $flatfiledata = flatfile_in_array($filename_with_path,$ignoreHeaderLines,$delimiter);
		  //print_r($flatfiledata);
		  if(!empty($flatfiledata)){$count = count($flatfiledata); }
		  for($x=0; $x<$count; $x++)
		  {
			$countInside = arraycolumncounter($flatfiledata);
			$RidCode = ''; // -- only accepted ID are processed 61
			$message = '';
			for($z=0;$z<$countInside;$z++)
			{

			   if( $z == 0 && isset($flatfiledata[$x][$z]))
			   {
				// -- Header Record -- //
				if($x == 0)
				{
				// -- Record Identifier 1 – 3
				// -- User Installation Code 4 – 7
				// -- Creation Date 8 – 17 - Format: YYYY-MM-DD
				$actiondate = substr($flatfiledata[$x][$z],7,10);
				// -- File sequence Number 18 – 21
				// -- Service Identification 22 – 26
				}
				// -- Mandate Response Record
				if($x == 1)
				{
					// -- Original Contract Reference 11 – 24
					$accountnumber = substr($flatfiledata[$x][$z],10,14);
					// -- BANK Response Code 31 – 36
				    $statusCode = substr($flatfiledata[$x][$z],30,6);
				}

			}}}

			// -- Call to update DebitCheck to Mecantile.
			  if(!empty($accountnumber))
			  {
				$arrayCollections = null;
				$message = $errorMessage.$filename_with_path;
				$mandateObj = get_mandate($accountnumber,$actiondate);
				if($mandateObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path;$FileMove = false;}
				else
				{
					$bankresponsecodeObj = get_bank_responsecode($statusCode);
					foreach($bankresponsecodeObj as $row)
					{
						$arrayCollections[0] = $row['mainstatus'];
						$arrayCollections[1] = $row['bankresponsecode'];
						$arrayCollections[2] = $row['bankresponsecodedesc'];
					}

					foreach($mandateObj as $row)
					{
						$arrayCollections[3] = $row['idmandate'];
						$rowCol = $row;
					}
					$arrayCollections[4] = $filename_with_path;
					updatemandateStatus($arrayCollections);
					$FileMove  = true;
				}
			  }
			  else
			  {
				$message = 'no record found to update empty reference ; '.$x.' '.$z.$filename_with_path; $FileMove = false;
			  }
		// -- Display the processed records..
		  $html = "<tr>";
		  $html = $html."<td>$RId</td>";
		  $html = $html. "<td>$actiondate</td>";
		  $html = $html. "<td>$statusCode</td>";
		  $html = $html. "<td>$accountnumber</td>";
		  $html = $html. "<td></td>";
		  $html = $html. "<td>$message</td>";
		  $html = $html. "<td>$FileMove</td>";
		  $html = $html."</tr>";
		// -- Move the file to Backup directory.
		if($FileMove)
		{
			// -- $unpaidlist = null;
			move_file($filename_with_path, $backuppath);
			//updateprocessedfiles($filename_with_path,$filename,'processed');
		}
		return $html;
	  }
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
}}

// -- Get Mandates
if (!function_exists('get_mandate'))
{
	function get_mandate($accountnumber,$actiondate)
	{
	 try
	  {
		$dbnameobj = 'ecashpdq_paycollections';
		$tablename = 'mandates';
		$arrayCollections = null;
		$whereArray = null;
		$queryFields = null;
		$whereArray[] = "contract_reference = '{$accountnumber}'"; // -- Action Date.
	  //$whereArray[] = "createdon = '{$actiondate}'"; // -- Client Reference.
		$queryFields[] = '*';
		//print_r($whereArray);
		$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
		return $collectionObj;
	   }
	catch (Exception $e)
	{
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}}

// -- Get Bank Response code..
if (!function_exists('get_bank_responsecode'))
{
	function get_bank_responsecode($statusCode)
	{
	 try
	  {
		$dbnameobj = 'ecashpdq_paycollections';
		$tablename = 'debicheck_responsecodes';
		$arrayCollections = null;
		$whereArray = null;
		$queryFields = null;
		$whereArray[] = "bankresponsecode = '{$statusCode}'"; // -- Action Date.
		$queryFields[] = '*';
		//print_r($whereArray);
		$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
		return $collectionObj;
	   }
	catch (Exception $e)
	{
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}}

// -- Update Status of the debit Order.
if (!function_exists('updatemandateStatus'))
{
	function updatemandateStatus($arrayCollections)
	{
	 try
	  {
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "UPDATE mandates
		SET
		status_code = ?,
		status_desc = ?,
		filename = ?
		WHERE idmandate = ?";
		if(!isset($arrayCollections[4])){$arrayCollections[4] = '';}
		$q = $pdo->prepare($sql);
		//echo $sql;
		//print_r($arrayCollections);
		$q->execute(array(
		$arrayCollections[1],
		$arrayCollections[2],
		$arrayCollections[4],
		$arrayCollections[3]));

		//return "success";
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<?php //echo $rootUrl; ?>
<!-- Template CSS -->
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/animate.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/nexus.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/responsive.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/custom.css" rel="stylesheet">
<link href="<?php echo $rootUrl;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $rootUrl;?>assets/css/bootstrap.css" rel="stylesheet">

</head>
<body>
<div id="body-bg">
<div class='container background-white' id="add-recipients-content">
<div class="row">
  <div class="table-responsive">
	<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
	  <tr>
	  <th>RI</th>
	  <th>Action Date</th>
	  <th>Status Code</th>
	  <th>Account Number</th>
	  <th>Amount</th>
	  <th>Feedback</th>
	  <th>Files Moved?</th>
	  </tr>
				<?php
				$Error = 'Error';

				// -- Loop through file in the directory & process->move to back directory. -- //
					$FListLengh = 0;
					$files_to_load = null;
					$ext = '';
					$message = '';
					$FileMove   = true;
					$unpaidlist = array();
					// -- Get the file list from the directory.
					//$files_to_load = read_files($path);
					$files_to_load = read_files_date_sorted($path);
					//print_r($files);
					if($files_to_load != null)
					{ $FListLengh = count($files_to_load);}
					else
					{ echo 'no files to process';}
					//print_r($files_to_load);
				for($ftx=0; $ftx<$FListLengh; $ftx++)
				{	$filename_with_path = $path.$files_to_load[$ftx];
					$filename = $files_to_load[$ftx];
					$file_parts = pathinfo($files_to_load[$ftx]);
					$FileMove = true;
					 //$file_parts;
					$ext = strtolower($file_parts['extension']);
					//echo $ext;
					switch($ext)
					{
					// -- NAEDO equals .txt
						case "txt":
						{
						// ----- call NAEDO file process ------
						$FileMove = true;
						$flatfiledata = null;
						$count = 0;
						$x=0;
						$z = 0;
						$whereArray = array();
						$collectionObj = null;
						$bankresponsecodeObj = null;

						$RI = '';
						$actiondate = '';
						$accountnumber = '';
						$statusCode = '';

						$ignoreHeaderLines = 1; // -- number of first lines
						$message = " ";
						$delimiter = " ";
						// -- Update the file in table modify script to update the processedfiles
						// -- table field ="ack" with value  "received"
						//updateprocessedfiles($filename_with_path,$filename);

						$flatfiledata = flatfile_in_arrayNAEDO($filename_with_path,$ignoreHeaderLines,$delimiter);
						//print_r($flatfiledata);
						$count = count($flatfiledata) - 1; // -- avoid reading last line cause is a footer.
						for($x=0; $x<$count; $x++)
						{
								$countInside = arraycolumncounter($flatfiledata);
								$actiondate    = '';
								$accountnumber = '';
								$statusCode    = '';

								for($z=0;$z<$countInside;$z++)
								{
										$actiondate    = '';
										$accountnumber = '';
										$statusCode    = '';

										if($z == 0 && isset($flatfiledata[$x][$z]))
										{
											echo"<tr>";
											// - RI postition 1 – 2
												$RI = substr($flatfiledata[$x][$z],0,2);

											// -- Action Date position 59 - 64 [YYMMDD]
												$actiondate = substr($flatfiledata[$x][$z],94,6);
												$cuY = substr(date('Y'),0,2);
												$actiondate = date('Y-m-d', strtotime($cuY.$actiondate));
											// -- status code position 68 – 70
												$statusCode = substr($flatfiledata[$x][$z],67,2);
											// -- Contract Reference position 14 81 – 94.
												$accountnumber = substr($flatfiledata[$x][$z],80,14);
											// -- remove trailing spaces
												$accountnumber = rtrim($accountnumber);

											// --print table
												echo "<td>".$RI."</td>";
												echo "<td>".$actiondate."</td>";
												echo "<td>".$statusCode."</td>";
											    echo "<td>".$accountnumber."</td>";
												echo "<td></td>";// -- amount..08.03.2021

								// -- Call to update EFT Bank Unpaid Mecantile.
								if(!empty($accountnumber))
								{
									$dbnameobj = 'ecashpdq_paycollections';
									$queryFields = null;
									$whereArray = null;
									$arrayCollections = null;
									$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
									$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
									$queryFields[] = '*';
									$collectionObj = search_dynamic($dbnameobj,'collection',$whereArray,$queryFields);
									$whereArray = null;
									$whereArray[] = "bankresponsecode = '{$statusCode}'"; // -- Bank Response Code.
									$queryFields = null;

									//$queryFields[] = 'collectionid';
									$queryFields[] = 'bankresponsecode';
									$queryFields[] = 'bankresponsecodedesc';

									$bankresponsecodeObj = search_dynamic($dbnameobj,'bankresponsecodes',$whereArray,$queryFields);;
									if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path; $FileMove = false; }
									else
									{
										$rowTxt = null;
										foreach($bankresponsecodeObj as $row)
										{
											$arrayCollections[0] = $row['mainstatus'];
											$arrayCollections[1] = $row['bankresponsecode'];
											$arrayCollections[2] = $row['bankresponsecodedesc'];
										}
										foreach($collectionObj as $row)
										{
											$arrayCollections[3] = $row['collectionid'];
											$rowTxt = $row;
										}

										if(isset($arrayCollections[1]))
										{
											// -- Unpaid ~ Add to UnpaidList array.
											if($arrayCollections[0] == 'UnPaid')
											{
												$rowTxt['Status_Description'] = $arrayCollections[2];
											    // -- Send auto Welcome SMS..
												$unpaidlist[] 	 = $rowTxt;
											}
										}
										$arrayCollections[4] = $filename_with_path;
										$message = updatecollectionStatus($arrayCollections);
									}
								}
								else
								{
									$message = 'no record found to update empty reference ; '.$filename_with_path; $FileMove = false;
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";

								}
								if(empty($message)){$message = 'success, '.$filename_with_path;}

								if(!empty($accountnumber))
								{
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";
								}
								echo"</tr>";

								}
							  }
						   }
							// -- if file is empty or does not belong in this directory.
							if($count <= 0 )
							{
								echo"<tr>";
								echo "<td>$Error</td>";
								echo "<td></td>";
								echo "<td></td>";
								echo "<td></td>";
								$message = 'empty or incorrect file, please check ; '.$filename_with_path; $FileMove = false;
								echo "<td>$message</td>";
								echo "<td>$FileMove</td>";
								echo"</tr>";
							}
						break;
						}
					  // -- EFT equals .upl
						case "upl":
						{
							// ----- call operation & file content ------ //
						    $FileMove = true;
							$flatfiledata = null;
							$count = 0;
							$x=0;
							$z = 0;
							$whereArray = array();
							$collectionObj = null;
							$bankresponsecodeObj = null;

							$actiondate = '';
							$accountnumber = '';
							$statusCode = '';

							$ignoreHeaderLines = 11; // -- number of first lines
							$message = " ";
							$delimiter = " ";
							// -- Update the file in table modify script to update the processedfiles
							// -- table field ="ack" with value  "received"
							//updateprocessedfiles($filename_with_path,$filename);

							$flatfiledata = flatfile_in_array($filename_with_path,$ignoreHeaderLines,$delimiter);
							//	print_r($flatfiledata);

							$flatfiledata = array_filter($flatfiledata);
							array_filter($flatfiledata, function($value) { return !is_null($value) && $value !== ''; });
							$count = count($flatfiledata);

							$RId = 'EFT';
							for($x=0; $x<$count; $x++)
								{
									$countInside = arraycolumncounter($flatfiledata);
									$actiondate    = '';
									$accountnumber = '';
									$statusCode    = '';
									$RidCode = ''; // -- only accepted ID are processed 61

									for($z=0;$z<$countInside;$z++)
									{
										$actiondate    = '';
										$accountnumber = '';
										$statusCode    = '';
										$RidCode = ''; // -- only accepted ID are processed 61
										$fileamount = '';
										if( $z == 0 && isset($flatfiledata[$x][$z]))
										{
											// - Check for allowed RI Code postition 1 – 2
											$RidCode = substr($flatfiledata[$x][$z],0,2);

											if($RidCode != '61')
											{
												break; // -- only process 61
											}
											// -- Action Date position 81 - 91 [YYYY/MM/DD]
											//	$actiondate = substr($flatfiledata[$x][$z],80,10);
											// -- Capured Action Date position 194 - 200 [YYMMDD]
												$actiondate = substr($flatfiledata[$x][$z],193,6);

											// -- remove trailing spaces
												$actiondate = rtrim($actiondate);
												$actiondate = convertActionDate($actiondate);
											// -- status code position 120 – 122
												$statusCode = substr($flatfiledata[$x][$z],119,2);
											// -- Client Reference. position 180 - 194
												$accountnumber = substr($flatfiledata[$x][$z],179,14);
												// -- remove trailing spaces
												$accountnumber = rtrim($accountnumber);
											// -- Unpaid Amount position 62 - 79
												$fileamount = substr($flatfiledata[$x][$z],61,18);
												$fileamount = rtrim($fileamount);
												// -- Remove Leading zeros
												$fileamount = ltrim($fileamount, "0");
												// -- Last to values are decimal places.
												$position = strlen($fileamount) - 2;
												$replace = substr($fileamount,$position,2);
												$replace_with = '.'.$replace;
												// -- Add a '.' to separe into decimal places.
												$fileamount = lreplace($replace, $replace_with, $fileamount);

											// -- pint to table
											echo"<tr>";
											echo "<td>".$RId."</td>";
											echo "<td>".$actiondate."</td>";
											echo "<td>".$statusCode."</td>";
											echo "<td>".$accountnumber."</td>";
											echo "<td>".$fileamount."</td>";// -- amount..08.03.2021

									// -- Call to update EFT Bank Unpaid Mecantile.
									$tablename = 'collection';
									$dbnameobj = 'ecashpdq_paycollections';
									$queryFields = null;

									if(!empty($accountnumber)){
										$arrayCollections = null;
										$whereArray = null;
										$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
										$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
										$whereArray[] = "Amount = '{$fileamount}'";
										$queryFields[] = '*';
										$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);

										$whereArray   = null;
										$whereArray[] = "bankresponsecode = {$statusCode}"; // -- Bank Response Code.
										$tablename	  = 'bankresponsecodes';
										$bankresponsecodeObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;

										if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path;$FileMove = false;}
										else
										{
											foreach($bankresponsecodeObj as $row)
											{
												$arrayCollections[0] = $row['mainstatus'];
												$arrayCollections[1] = $row['bankresponsecode'];
												$arrayCollections[2] = $row['bankresponsecodedesc'];
											}
											$rowOut = null;
											foreach($collectionObj as $row)
											{
												$arrayCollections[3] = $row['collectionid'];
												$rowOut = $row;
											}
											// -- if response code = '00' -> successfully.
											if(isset($arrayCollections[1]))
											{
												// -- Unpaid ~ Add to UnpaidList array.
												if($arrayCollections[0] == 'UnPaid')
												{
													$rowOut['Status_Description'] = $arrayCollections[2];
													$unpaidlist[] 	 = $rowOut;
												}
											}
											//print_r($unpaidlist);
											$arrayCollections[4] = $filename_with_path;
											$message = updatecollectionStatus($arrayCollections);
										}
									}
									else
									{
										$message = 'no record found to update empty reference ; '.$filename_with_path; $FileMove = false;
										echo "<td>$message</td>";
										echo "<td>$FileMove</td>";
									}
									if(empty($message)){$message = 'success, '.$filename_with_path;}
									if(!empty($accountnumber))
									{
										echo "<td>$message</td>";
										echo "<td>$FileMove</td>";
									}
									echo"</tr>";
								   }
								 }
							    }
								// -- if file is empty or does not belong in this directory.
								if($count <= 0 )
								{
									echo"<tr>";
									echo "<td>$Error</td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									$message = 'empty or incorrect file, please check ; '.$filename_with_path; $FileMove = false;
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";
									echo"</tr>";
								}
								break;
							   }
							    // -- EFT equals .upl
								case "out":
								{
								// ----- call operation & file content ------ //
								    $FileMove = true;
									$flatfiledata = null;
									$count = 0;
									$x=0;
									$z = 0;
									$whereArray = array();
									$collectionObj = null;
									$bankresponsecodeObj = null;

									$actiondate = '';
									$accountnumber = '';
									$statusCode = '';

									$ignoreHeaderLines = 10; // -- number of first lines
									$message = "";
									$delimiter = " ";
									$RidCodeArray = null;
									$FooterArray  = null; // -- Get footer - FILE ACCEPTED ON PRE-CHECK(13/05/2022)
									// -- Update the file in table modify script to update the processedfiles
									// -- table field ="ack" with value  "received"

									// -- Get the content of the file.
									$flatfiledata = flatfile_in_array($filename_with_path,$ignoreHeaderLines,$delimiter);

									$flatfiledataError = null;
									$errorMessage 	   = '';
									if(!empty($flatfiledata))
									{
										$count = count($flatfiledata);
									}

									// -- Check for any error Message
									$errorMessage = get_file_out_error($filename_with_path,$delimiter);
									if(!empty($errorMessage))
									{
										$count = 0;
									}

									//print_r($flatfiledata);

									$RId = 'EFT';
								for($x=0; $x<$count; $x++)
								{
									$countInside = arraycolumncounter($flatfiledata);
									$actiondate    = '';
									$accountnumber = '';
									$statusCode    = '';
									$RidCode = ''; // -- only accepted ID are processed 61
									$message = '';

									for($z=0;$z<$countInside;$z++)
									{
										$RidCode = '';
										$accountnumber = '';
										$actiondate    = '';
										$statusCode    = '';

										// -- Only focus on Status Code 50
										if( $z == 0 && isset($flatfiledata[$x][$z]))
										{
																																		// -- print('<br/>');
										// -- print($flatfiledata[$x][$z]);

											// - Check for allowed RI Code postition 1 – 2
											$RidCode = substr($flatfiledata[$x][$z],0,2);
											$RidCodeArray[] = $RidCode;
	
											// -- Get content of the footer - FILE ACCEPTED ON PRE-CHECK(13/05/2022)
											$str = trim($flatfiledata[$x][$z]);
											if($str == '*** FILE ACCEPTED FOR PROCESSING ***'
												or $str == '*** FILE ACCEPTED ON PRE-CHECK ***')
											{
													$FooterArray[] = $str;
											}
											// -- 13/05/2022....
											if($RidCode != '50')
											{
												continue; // -- only process 50
											}

											// -- Action Date position 80 - 90 [YYYY/MM/DD]
											//	$actiondate = substr($flatfiledata[$x][$z],80,10);
											// -- remove trailing spaces
											//	$actiondate = rtrim($actiondate);

											// -- Action Date position 81 - 91 [YYYY/MM/DD]
											//	$actiondate = substr($flatfiledata[$x][$z],80,10);
											// -- Capured Action Date position 194 - 200 [YYMMDD]
												$actiondate = substr($flatfiledata[$x][$z],193,6);

											// -- remove trailing spaces
												$actiondate = rtrim($actiondate);
												$actiondate = convertActionDate($actiondate);

											// -- status code position 119 – 121
												$statusCode = substr($flatfiledata[$x][$z],119,2);
											// -- Client Reference. position 179 - 193
												$accountnumber = substr($flatfiledata[$x][$z],179,14);
											$accountnumber = rtrim($accountnumber);
											// pint to table
											echo"<tr>";
											echo "<td>".$RId."</td>";
											echo "<td>".$actiondate."</td>";
											echo "<td>".$statusCode."</td>";
											echo "<td>".$accountnumber."</td>";
											echo "<td></td>";// -- amount..08.03.2021



								   // -- Call to update EFT Bank Unpaid Mecantile.
									$tablename = 'collection';
									$dbnameobj = 'ecashpdq_paycollections';
									  if(!empty($accountnumber))
									  {
										/*$arrayCollections = null;
										$collectionObj = GetCollectinobjout($accountnumber,$actiondate,$dbnameobj,$tablename);
										$bankresponsecodeObj = Getbankrespcodeout($statusCode,$dbnameobj,$tablename);
										*/

										$arrayCollections = null;
										$whereArray = null;
										$queryFields = null;
										$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
										$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
										$queryFields[] = '*';
										//print_r($whereArray);
										$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);

										$whereArray = null;
										$whereArray[] = "bankresponsecode = {$statusCode}"; // -- Bank Response Code.
										$tablename = 'bankresponsecodes';
										$bankresponsecodeObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;

										if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path;$FileMove = false;}
										else
										{

											foreach($bankresponsecodeObj as $row)
											{
												$arrayCollections[0] = $row['mainstatus'];
												$arrayCollections[1] = $row['bankresponsecode'];
												$arrayCollections[2] = $row['bankresponsecodedesc'];
											}
											$rowCol = null;

											foreach($collectionObj as $row)
											{
												$arrayCollections[3] = $row['collectionid'];
												$rowCol = $row;
											}

											if(isset($arrayCollections[1]))
											{
												// -- Unpaid ~ Add to UnpaidList array.
												if($arrayCollections[0] == 'UnPaid')
												{
													$rowCol['Status_Description'] = $arrayCollections[2];
													// -- Send auto Welcome SMS..
													$unpaidlist[] = $rowCol;
												}
											}
											$arrayCollections[4] = $filename_with_path;
											$message = updatecollectionStatus($arrayCollections);
										}
									 }
									else
									{
										$message = 'no record found to update empty reference ; '.$x.' '.$z.$filename_with_path; $FileMove = false;
										echo "<td>$message</td>";
										echo "<td>$FileMove</td>";
									}

									}
									if(empty($message)){$message = 'success, '.$filename_with_path;}
									if(!empty($accountnumber))
									  {
										echo "<td>$message</td>";
										echo "<td>$FileMove</td>";									// -- Update BankQueue for RID = 50
										updatebankqueuefiles($filename_with_path,$filename,$message,$RidCodeArray,$FooterArray);
									  }
									echo"</tr>";
									}
								}

								// -- if file is empty or does not belong in this directory.
								if($count <= 0 )
								{
									echo"<tr>";
									echo "<td>$Error</td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									if(!empty($errorMessage))
									{
										 $message = $errorMessage.$filename_with_path;
									}
									else
									{
										$message = 'empty or incorrect file, please check ; '.$filename_with_path;

									}
									$FileMove = false;
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";										echo"</tr>";
									// -- Update BankQueue
									updatebankqueuefiles($filename_with_path,$filename,$message,$RidCodeArray,$FooterArray);
								}
								else
								{
									$errorMessage = 'success';
									$message = $errorMessage.$filename_with_path;
									echo"<tr>";
									echo "<td>$RId</td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";
									echo"</tr>";

									// -- Update BankQueue
									updatebankqueuefiles($filename_with_path,$filename,$message,$RidCodeArray,$FooterArray);

									//print_r($RidCodeArray);
								}
									break;
								}
								// -- Response equals .acresp
								case "acresp":
								{
									// -- Process Debit Check Responses
									// -- Parameters Filename $filename_with_path
									$html = process_acresp_file($filename_with_path,$backuppath);
									$FileMove 	= false;
									echo $html;
									break;
								}
								// -- csv files .csv 17/12/2021
								case "csv":
								{
									$output = bankstatement_processcsvfile($filename_with_path);
									foreach($output as $key => $values)
									{
											echo "<tr><td>{$values[1]}</td>";
											echo "<td>{$values[6]}</td>";
											echo "<td>{$values[3]}</td>";
											echo "<td>{$values[13]}</td>";
											echo "<td>{$values[7]}</td>";
											echo "<td>{$values[0]}</td>";
											echo"</tr>";
									}
									$FileMove = true;
									break;
								}
								case "xpresp":
								{
										$RI = '';
										$actiondate = '';
										$accountnumber = '';
										$statusCode = '';

										$ignoreHeaderLines = 1; // -- number of first lines
										$message = " ";
										$delimiter = " ";
										$flatfiledata = flatfile_in_arrayNAEDO($filename_with_path,$ignoreHeaderLines,$delimiter); 
										$count = count($flatfiledata) - 1; // -- avoid reading last line cause is a footer. 								
										print_r($flatfiledata);

										echo"<tr>";
										// -- RI
										echo "<td>".$RId."</td>";
										// -- Action Date.
										echo "<td>".$actiondate."</td>";
										// -- Status Code.
										echo "<td>".$statusCode."</td>";		
										// -- Account number.
										echo "<td>".$accountnumber."</td>";	
										// -- Amount.
										echo "<td>".$accountnumber."</td>";

										//Feedback
										if(empty($message)){$message = 'success';}
										if(!empty($accountnumber)){
										echo "<td>$message</td>";}
										echo"</tr>";
										
										$FileMove = false;
										break;
								}
								default:
								{
									echo"<tr>";
									echo "<td>$Error</td>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td></td>";
									$message = 'incorrect file extension .'.$ext. ', please check valid files .out,.txt,.upl; '.$filename_with_path; $FileMove = false;
									echo "<td>$message</td>";
									echo "<td>$FileMove</td>";
									echo"</tr>";
									break;
								}
					}
						// -- Send a list of unpaid transaction.
						// -- Internal UserCode.
						if(!empty($unpaidlist))
						{
							// -- Extension .upl,.eft & .out
							send_notification_unpaid($unpaidlist);
						}
						// -- Move the file to Backup directory.
						if($FileMove)
						{
							$unpaidlist = null;
							move_file($filename_with_path, $backuppath);
							//updateprocessedfiles($filename_with_path,$filename,'processed');
						}
						else
						{
							//updateprocessedfiles($filename_with_path,$filename,'ERROR');
						}
					}
					   ?>
	<table>
   </div>
</div>
</body>
</html>
