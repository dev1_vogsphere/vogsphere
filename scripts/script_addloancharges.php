<?php
/* All the Approved = APPR Loans to scheduled to run monthly to add Service Fees
$title='eLoan - Add Loan Charges';
$menu_item = 3;
$page_content='content/text_addloancharges.php';
include_once('master.php');
*/
if(!function_exists('getbaseurl'))
{
	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if ($_SERVER['HTTPS'] != "on")
			{
				$url = "http://";
			}
			else
			{
				$url = "http://";
			}

			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			return $url.$dir;
	}
}
$dir = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
$dir = str_replace("scripts","",$dir);
require $dir.'database.php';
$baseurl = getbaseurl();
$baseurl = str_replace("/scripts","",$baseurl);
$WebServiceURL = $baseurl."webservices/ws_paymentschedules?status=APPR";
$chargeid = '8'; // -- Monthly Service Fee.-2018-05-21 to 2022-03-31
$chargename="Monthly Service Fee";
$message  = '';
$count    = '';
$applicationId = '';
$scheduleddate = '';

// -- sending Email
$emailAdd = 'info@ecashmeup.com';
$adminemail = 'info@ecashmeup.com';
$companyname = 'ecashmeup';

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// And if you want to fetch multiple paymentschedule IDs:
foreach($sxml->paymentschedule as $paymentschedule)
{
	// -- 1. Check if the chargeid,applicationId does not already exist.
	$valid = CheckFunction($paymentschedule->applicationId,$chargename,$paymentschedule->item);
	$applicationId  = $paymentschedule->applicationId;
	$scheduleddate  = $paymentschedule->scheduleddate;
	$id = $paymentschedule->id;
	$today = date("Y-m-d");

	if($valid == false)
	{	$message = $message."<p class='alert alert-error'>Admin Fee Charges - ($applicationId for $scheduleddate)</p><br/>";
		echo "<br/>Admin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - Already exist.\n";
		$message = $message."\nAdmin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - Already exist.<br/>";
	}
	else
	{
		// -- 2. ScheduledDate Must be Smaller than TodayDate.
		$date1=date_create($scheduleddate);	// -- ScheduledDate
		$date2=date_create($today);
		$diff=date_diff($date1,$date2);		// -- TodayDate
		$difference = 0;

		//$difference = $diff->format("%R%a");//%M
		$difference = $diff->format("%R%M");

		// -- if the scheduledDate older than Today then it must add the charges.
		if($difference >= 0)
		{
			if($paymentschedule->item >= 0)
			{
				$count = AddCharges($paymentschedule->applicationId,$chargeid,$paymentschedule->item,$chargename);
				if($count > 0)
				{
				$message = $message."<p class='alert alert-error'>Admin Fee Charges - ($applicationId for $scheduleddate)</p><br/>";
				echo "<br/>Admin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - successful.\n";
				$message = $message."\nAdmin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - successful.<br/>";
				}
			}
		}
	}
}

// -- Get Global Settings.
GetGlobalSetting();

// -- Send an Email
//SendEmail($message,$emailAdd);

// ------------ Start Send an Email ---------------- //
	function SendEmail($emailContent,$emailAdd)
	{
		$dir = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
		$dir = str_replace("scripts","",$dir);
		$dir  = $dir.'class/class.phpmailer.php';
		require_once($dir);

			Global $adminemail;
			Global $companyname;


			try
			{

	// -- Email Server Hosting -- //
		$Globalmailhost = $_SESSION['Globalmailhost'];
		$Globalport = $_SESSION['Globalport'];
		$Globalmailusername = $_SESSION['Globalmailusername'];
		$Globalmailpassword = $_SESSION['Globalmailpassword'];
		$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
		$Globaldev = $_SESSION['Globaldev'];
		$Globalprod = $_SESSION['Globalprod'];

	// -- Email Server Hosting -- //
	/*	Production - Live System */
		if($Globalprod == 'p')
		{
			$mail=new PHPMailer();
			$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
			$mail->SetFrom($adminemail,$companyname);
			$mail->AddReplyTo($adminemail,$companyname);
		}
	/*	Development - Testing System */
		else
		{
				$mail=new PHPMailer();
				$mail->IsSMTP();
				$mail->SMTPDebug=1;
				$mail->SMTPAuth=true;
				$mail->SMTPSecure=$GlobalSMTPSecure;
				$mail->Host=$Globalmailhost;
				$mail->Port= $Globalport;
				$mail->Username=$Globalmailusername;
				$mail->Password=$Globalmailpassword;
		}
				$mail->SetFrom($adminemail,$companyname);

						// -- The Code
						$mail->Subject=  $companyname." - AddLoanCharges Scheduled Job Logs for ".date('Y-m-d');
						//ob_start();
						//include 'text_emailForgottenPassword.php';
						$body = $emailContent;//ob_get_clean();
						$fullname = 'Testing';//$FirstName.' '.$LastName;
						$email = $emailAdd;//$_SESSION['email'];
						$mail->MsgHTML($body );
						$mail->AddAddress($email, $fullname);
						$mail->AddBCC($adminemail, $companyname);

						if($mail->Send())
						{
						$successMessage = "Your Job scheduled log has been sent to your e-mail address.";
						}
						else
						{
						  // echo 'mail not sent';
						$successMessage = "Your e-mail address seems to be invalid!";
						}
				}
				catch(phpmailerException $e)
				{
					$successMessage = $e->errorMessage();
				}
				catch(Exception $e)
				{
					$successMessage = $e->getMessage();
				}
}
// -- Add LoanCharges
function AddCharges($applicationid,$chargeid,$item,$chargename)
{
	if($chargeid == ''){$chargeid = 0;}

	if($item == ''){$item = 0;}

	if($applicationid == ''){$applicationid = 0;}

		$count = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO loanappcharges  (chargeid,applicationid, item,chargename) VALUES (?,?,?,?)";

		$q = $pdo->prepare($sql);
		$q->execute(array($chargeid,$applicationid,$item,$chargename));
		Database::disconnect();
		$count = $count + 1;

	return $count;
}
// -- Check if Charges Exist.
function CheckFunction($applicationid,$chargename,$item)
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	//if($chargeid == ''){$chargeid = 0;}
  if($chargename == ''){$chargename = 0;}

	if($item == ''){$item = 0;}

	if($applicationid == ''){$applicationid = 0;}

	$valid = true;

 // -- Check Payment Charges.
	$sql = 'SELECT * FROM loanappcharges WHERE ApplicationId = ? AND item = ? AND  chargename = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($applicationid,$item,$chargename));
    $dataloanappcharges = $q->fetchAll();

	Database::disconnect();

  // -- SizeOf an Array.
	if(sizeof($dataloanappcharges) > 0)
	{
		$valid = false;
	}
  return $valid;
}

// --  Get the GlobalSettings.
function GetGlobalSetting()
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();

	// -- Update Address & Company details.
	if($data['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $data['name'];
		$phone    = $data['phone'];
		$fax      = $data['fax'];
		// -- BOC Encrption 16.09.2017
		$_SESSION['phone'] = $phone;
		$_SESSION['fax']   = $fax;
		$website  = $data['website'];
		$_SESSION['website'] = $website;
		// -- EOC Encrption 16.09.2017
		$email    = $data['email'];
		$_SESSION['adminemail'] =  $email;// -- Encrption 16.09.2017
		$adminemail = $_SESSION['adminemail'];
		$_SESSION['companyname'] =  $Company; // -- Encrption 16.09.2017
		$logo     = $data['logo'];
		$logoTemp = $logo;
		$_SESSION['logo'] =  $logo;// -- Encrption 16.09.2017

		$currency = $data['currency'];
		$legalterms = $data['legaltext'];

		$street = $data['street'];
		$suburb = $data['suburb'];
		$city = $data['city'];
		$State = $data['province'];
		$province = $State;
		$PostCode = $data['postcode'];
		$registrationnumber = $data['registrationnumber'];


		$terms = $data['terms'];
		$ncr = $data['ncr'];

		// --- Banking Details ------ //
		$disclaimer = $data['disclaimer'];
		$bankname = $data['bankname'];
		$accountholdername = $data['accountholdername'];
		$accountnumber = $data['accountnumber'];
		$branchcode  = $data['branchcode'];
		$accounttype = $data['accounttype'];

	// -- BOC Global Session Values --- //
		$_SESSION['MasterCompany']  = $data['name'];
		$_SESSION['GlobalCompany']  = $data['name'];
		$_SESSION['Globalphone']    = $data['phone'];
		$_SESSION['Globalfax']      = $data['fax'];
		$_SESSION['Globalemail']    = $data['email'];
		$_SESSION['Globalwebsite']  = $data['website'];
		$_SESSION['Masterlogo']     = $logo;
		$_SESSION['GloballogoTemp'] = $data['logo'];
		$_SESSION['Globalcurrency'] = $data['currency'];
		$_SESSION['Globallegalterms'] = $data['legaltext'];
		$_SESSION['Globalterms'] = $data['terms'];
		$_SESSION['Globalstreet'] = $data['street'];
		$_SESSION['Globalsuburb'] = $data['suburb'];
		$_SESSION['Globalcity'] = $data['city'];
		$_SESSION['Globalstate'] = $data['province'];
		$_SESSION['Globalpostcode'] = $data['postcode'];
		$_SESSION['Globalregistrationnumber'] = $data['registrationnumber'];

		// -- Email Server Hosting
		$_SESSION['Globalmailhost']  = $data['mailhost'];
		$_SESSION['Globalport'] = $data['port'];
		$_SESSION['Globalmailusername'] = $data['username'];
		$_SESSION['Globalmailpassword'] = $data['password'];
		$_SESSION['GlobalSMTPSecure'] = $data['SMTPSecure'];
		$_SESSION['Globaldev'] = $data['dev'];
		$_SESSION['Globalprod'] = $data['prod'];
	// -- EOC Master Session Values --- //
	}
}
?>
