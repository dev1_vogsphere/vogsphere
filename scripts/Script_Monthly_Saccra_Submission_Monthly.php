<?php
//Global Variables
$Passport="";
$IDnumber="";
$senderId="XY2137";
$recipientId="ALL";
$fileType="T702"; //QA
//$fileType="CATB";  //Production
$frequency="M";
$date="20231004" ;//Automate to be the 4th of every month
$number_of_files="01";
$sequence="2"; //Automate to be the 4th of every month
$file_extension=".txt";
$header="H";
$version_number="06";
$Creation_Date=date("Ymd");
$previousMonthEndDate=date('Ymd', strtotime('last day of previous month'));
$monthEndDate= new DateTime($Creation_Date); 
$monthEndDate->format('Y-m-t');
$tradingname="ECASHMEUP";
$fillers=" PERSONAL LOAN";
$fillersT=" ";
//$Filename ='../Files/Collections/Out/'.$senderId."_".$recipientId."_".$fileType."_".$frequency."_".$date."_".$number_of_files."_".$sequence;
$Filename ='../Files/Sacrra/Out/'.$senderId."_".$recipientId."_".$fileType."_".$frequency."_".$previousMonthEndDate."_".$number_of_files."_".$sequence.$file_extension;
$Trailer="T";

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_eloan");
// Check connection
if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL:JDBC query get get sequence number " . mysqli_connect_error();
}
// Get All Loans
$getclient ="SELECT * FROM customer INNER JOIN loanapp ON customer.CustomerId = loanapp.CustomerId where loanapp.ExecApproval='DEF' Limit 1";
$loanapp = mysqli_query($connect,$getclient);

if($loanapp->num_rows > 0)
{
		//Create file
		$fileHandle = fopen($Filename, 'w') or die ("Can't open file");

		//Header Record
        fwrite ($fileHandle, $header);
		$supplier_reference_number=str_pad($senderId, 10, ' ', STR_PAD_RIGHT);
        fwrite ($fileHandle, $supplier_reference_number);
        fwrite ($fileHandle, $previousMonthEndDate);
        fwrite ($fileHandle, $version_number);
        fwrite ($fileHandle, $Creation_Date);
        fwrite ($fileHandle, $tradingname);
		$fillers=str_pad($fillers, 610, ' ', STR_PAD_RIGHT);
        fwrite ($fileHandle, $fillers);
        fwrite ($fileHandle, "\n");

		//Record
	 while($row = $loanapp->fetch_assoc())
	  {
		//Check if is ID number or Passport  
		if(preg_match("/[a-z]/i",$row["CustomerId"])){
		 
		 $Passport=$row["CustomerId"];
		}
		else{
		  $IDnumber=$row["CustomerId"];
		}
		//Get Gender
		if(strpos($row["Title"],'Mr')!== false)
		{
		 $Gender="M";
		}
		else{
		  $Gender="F";
		}
		$DateofBirth=date("Ymd",strtotime($row["Dob"]));
		$BranchCode="";
		$Account_no=strrev($IDnumber);
		$Account_no=substr($Account_no,0, 9);
		$Sub_Account_no="";
		$Surname=$row["LastName"];
		$Title=$row["Title"];
		$Forename1=$row["FirstName"];
		$Forename2="";
		$Forename3="";
		$Residential_1=$row["Street"];
		$Residential_2=$row["Suburb"];
		$Residential_3=$row["City"];
		$Residential_4=$row["State"];
		$Residential_Code=$row["PostCode"];
		$Owner_Tenant="";
		$postal_1="";
		$postal_2="";
		$postal_3="";
		$postal_4="";
		$postal_Code="";
		$Ownership_Type="00";
		$loan_Reason_Code="C";
		$payment_Type="00";
		$type_of_account="F";//type of account
		$date_account_opened=date("Ymd",strtotime($row["DateAccpt"]));
		$deferred_payment_date="";
		$last_payment_date=date("Ymd",strtotime($row["lastPaymentDate"]));
		$opening_balance="0";// remove amount separator
        $current_balance=str_replace(".00","",strval($row["Repayment"]));// remove amount separator
		$amount_overdue=str_replace(".00","",strval($row["Repayment"]));// remove amount separator
		$instalment_amount=str_replace(".00","",strval($row["monthlypayment"]));// remove amount separator
		$current_balance_indicator="D"; //to be update on the system
		$months_in_arrears="06";//calculate the current balance
		$status_code="";
		$repayment_frequency="03";
		$terms=$row["LoanDuration"];
		$status_date="";
		$old_supplier_branch_code="";
		$old_account_number="";
		$old_sub_account_number="";
		$old_supplier_reference_number="";
		$home_telephone="";
		$cellular_telephone=$row["phone"];
		$work_telephone="";
		$employer_detail="";
		$income="";
		$income_frequency="";
		$occupation="";
		$third_party_name="";
		$account_sold_to_third_party="";
		$no_of_participants_in_joint_loan="";
		$transaction_date=Creation_Date;
		
		$data=str_pad("D", 1, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $data);
		$IDnumber=str_pad($IDnumber, 13, '0', STR_PAD_RIGHT);
		fwrite ($fileHandle, $IDnumber);
		$Passport=str_pad($Passport, 16, ' ', STR_PAD_LEFT);//gap
		fwrite ($fileHandle, $Passport);
		$Gender=str_pad($Gender, 1, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Gender);
		$DateofBirth=str_pad($DateofBirth, 8, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $DateofBirth);
		$BranchCode=str_pad($BranchCode, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $BranchCode);
		$Account_no=str_pad($Account_no, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Account_no);
		$Sub_Account_no=str_pad($Sub_Account_no, 4, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Sub_Account_no);
		$Surname=str_pad($Surname, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Surname);
		$Title=str_pad($Title, 5, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Title);
		$Forename1=str_pad($Forename1, 14, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Forename1);
		$Forename2=str_pad($Forename2, 14, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Forename2);
		$Forename3=str_pad($Forename3, 14, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Forename3);
		$Residential_1=str_pad($Residential_1, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Residential_1);
		$Residential_2=str_pad($Residential_2, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Residential_2);
		$Residential_3=str_pad($Residential_3, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Residential_3);
		$Residential_4=str_pad($Residential_4, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Residential_4);
		$Residential_Code=str_pad($Residential_Code, 6, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $Residential_Code);
		$Owner_Tenant=str_pad($Owner_Tenant, 1, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Owner_Tenant);
		$postal_1=str_pad($postal_1, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $postal_1);
		$postal_2=str_pad($postal_2, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $postal_2);
		$postal_3=str_pad($postal_3, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $postal_3);
		$postal_4=str_pad($postal_4, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $postal_4);
		$postal_Code=str_pad($postal_Code, 6, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $postal_Code);
		$Ownership_Type=str_pad($Ownership_Type, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $Ownership_Type);
		$loan_Reason_Code=str_pad($loan_Reason_Code, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $loan_Reason_Code);
		$payment_Type=str_pad($payment_Type, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $payment_Type);
		$type_of_account=str_pad($type_of_account, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $type_of_account);
		$date_account_opened=str_pad($date_account_opened, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $date_account_opened);
		$deferred_payment_date=str_pad($deferred_payment_date, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $deferred_payment_date);
		$last_payment_date=str_pad($last_payment_date, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $last_payment_date);
		$opening_balance=str_pad($opening_balance, 9, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $opening_balance);
		$current_balance=str_pad($current_balance, 9, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $current_balance);
		$current_balance_indicator=str_pad($current_balance_indicator, 1, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $current_balance_indicator);
		$amount_overdue=str_pad($amount_overdue, 9, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $amount_overdue);	
		$instalment_amount=str_pad($instalment_amount, 9, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $instalment_amount);		
		$months_in_arrears=str_pad($months_in_arrears, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $months_in_arrears);	
		$status_code=str_pad($status_code, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $status_code);			
		$repayment_frequency=str_pad($repayment_frequency, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $repayment_frequency);			
		$terms=str_pad($terms, 4, '0', STR_PAD_LEFT);
		fwrite ($fileHandle, $terms);			
		$status_date=str_pad($status_date, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $status_date);
		$old_supplier_branch_code=str_pad($old_supplier_branch_code, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $old_supplier_branch_code);
		$old_account_number=str_pad($old_account_number, 25, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $old_account_number);
		$old_sub_account_number=str_pad($old_sub_account_number, 4, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $old_sub_account_number);
		$old_supplier_reference_number=str_pad($old_supplier_reference_number, 10, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $old_supplier_reference_number);
		$home_telephone=str_pad($home_telephone, 16, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $home_telephone);
		$cellular_telephone=str_pad($cellular_telephone, 16, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $cellular_telephone);	
		$work_telephone=str_pad($work_telephone, 16, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $work_telephone);	
		$employer_detail=str_pad($employer_detail, 60, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $employer_detail);	
		$income=str_pad($income, 9, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $income);	
		$income_frequency=str_pad($income_frequency, 1, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $income_frequency);	
		$occupation=str_pad($occupation, 20, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $occupation);	
		$third_party_name=str_pad($third_party_name, 60, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $third_party_name);	
		$account_sold_to_third_party=str_pad($account_sold_to_third_party, 2, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $account_sold_to_third_party);	
		$no_of_participants_in_joint_loan=str_pad($no_of_participants_in_joint_loan, 3, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $no_of_participants_in_joint_loan);	
		$fillersT=str_pad($fillersT, 2, ' ', STR_PAD_RIGHT);
        fwrite ($fileHandle, $fillersT);
		$supplier_reference_number=str_pad($senderId, 10, ' ', STR_PAD_RIGHT);
		fwrite ($fileHandle, $supplier_reference_number);
		$transaction_date=str_pad($transaction_date, 8, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $transaction_date);		
        fwrite ($fileHandle, "\n");
	  }

		//Trailer Record
		fwrite ($fileHandle, $Trailer);
		$lines_file=COUNT(FILE($Filename)); // Number of Transactions
		$lines_file=str_pad($lines_file, 8, '0', STR_PAD_LEFT);
		fwrite ($fileHandle, $lines_file); //Transaction and Header count to calculate
		$fillersT=str_pad($fillersT, 689, ' ', STR_PAD_LEFT);
		fwrite ($fileHandle, $fillersT);

}
else {
echo "Bad";
echo "0 results";
}
//Close Database connection
//$connect->close();
echo "<br>"." File Detele... : ".$fileHandle."<br>";
//unlink($fileHandle);

//Close Database connection
$connect->close();

?>