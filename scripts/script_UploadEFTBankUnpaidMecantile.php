<?php
$title='Script Upload EFT Bank Unpaid Mecantile';
// -- Declarations
require 'database.php';
$valid_formats = array("upl","txt","xpresp");
$max_file_size = 1024*10000; //10MB
$path = "Files/Collections/In/"; // Download directory
$backuppath = "Files/Collections/Backup/In/"; // Backup directory

// -- Read files from FILE/In with .UPL extenstion  

// -- Read files from FILE/In with .txt extenstion 

// -- Read files from FILE/In with .xpresp extenstion 

// -- Move files to backup directory..
if(!function_exists('move_file'))
{
 function move_file($file, $to)
 {
    $path_parts = pathinfo($file);
    $newplace   = "$to/{$path_parts['basename']}";
    if(rename($file, $newplace))
        return $newplace;
    return null;
 }
}
if(!function_exists('read_files'))
{
// -- Read directory for list of files.
function read_files($dir)
{
	$files = null;
	$fileCount = 0;
	if (is_dir($dir)) 
	{
		if ($dh = opendir($dir)) 
		{
			while (($file = readdir($dh)) !== false) {
				if(file_exists($file) == 0)
				{
				  //echo "filename: ".$file."<br />";
				  $files[$fileCount] = $file;
				  $fileCount = $fileCount + 1;
				}
			}
			closedir($dh);
		}
	}
	return $files;
}
}
// -- flat file to an array.
if(!function_exists('flatfile_in_arrayOUT'))
{
	function flatfile_in_arrayOUT($url,$ignore,$delimiter)
	{
		$out = null;
		$flatfile = file($url);   // ---- Flatfile rows to array ----
		$index = 0;
		$z = 0;
		// -- ignore which header rows.
		//print_r($flatfile);

		// Loop through our array.
		foreach ($flatfile as $line_num => $line)
		{
			// -- ignore header lines
			if($line_num >= $ignore && !empty(htmlspecialchars($line)))
			{
				$flatfilexrow[$index] = $line;
				$flatfilexrow[$index] = chop($flatfilexrow[$index]);
				$flatfile_data[$index] = explode($delimiter,$flatfilexrow[$index]);
				$i=0;
				/*foreach($flatfile_data as $key)
				{
					$out[$z][$i] = $flatfile_data[$index][$i];
					$i++;
					//print_r($flatfile_data);
				}  */
				//print_r($flatfile_data);
				$index = $index + 1;
			}
		}
		return $flatfile_data;
	}
}

// -- GetCollectinobjout
if(!function_exists('GetCollectinobjout'))
{
	function GetCollectinobjout($accountnumber,$actiondate,$dbnameobj,$tablename)
	{
		$queryFields = null;		
		$arrayCollections = null;
		$whereArray = null;								
		$whereArray[] = "Account_Number = '{$accountnumber}'"; // -- Action Date.
		$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
		$queryFields[] = '*';
		$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
		return $collectionObj;
	}
}
																			
										

// -- Getbankrespcodeout
if(!function_exists('Getbankrespcodeout'))
{
	function Getbankrespcodeout($statusCode,$dbnameobj,$tablename)
	{
		$queryFields = null;
		$whereArray = null;
		$queryFields[] = '*';
		$whereArray[] = "bankresponsecode = '{$statusCode}'"; // -- Bank Response Code.
		$tablename = 'bankresponsecodes';
		$bankresponsecodeObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;		
		return $bankresponsecodeObj ;
	}
}
?>
<div class="row">
  <div class="table-responsive">
	<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
	  <tr>
	  <td>RI</td>					  
	  <td>Action Date</td>
	  <td>Status Code</td>					  
	  <td>Account Number</td>
	  <td>Feedback</td>
	  </tr>	
				<?php 
				// -- Loop through file in the directory & process->move to back directory. -- //
					$FListLengh = 0;
					$files_to_load = null;
					$ext = '';
					$message = '';
					$FileMove = true; 
				// -- Get the file list from the directory.
					$files_to_load = read_files($path);
					if($files_to_load != null)
					{$FListLengh = count($files_to_load);}
					else{ echo 'no files to process';}
					print_r($files_to_load);
				for($ftx=0; $ftx<$FListLengh; $ftx++)
				{	$filename_with_path = $path.$files_to_load[$ftx];
					$file_parts = pathinfo($files_to_load[$ftx]);
					$FileMove = true; 
					 //$file_parts;
					$ext = strtolower($file_parts['extension']); 
					//echo $ext;
					switch($ext)
					{
					// -- NAEDO equals .txt	
						case "txt":
						{
						// ----- call NAEDO file process ------ 
						$flatfiledata = null;
						$count = 0;
						$x=0;
						$z = 0;
						$whereArray = array();
						$collectionObj = null;
						$bankresponsecodeObj = null;
						
						$RI = '';
						$actiondate = '';
						$accountnumber = '';
						$statusCode = '';
						
						$ignoreHeaderLines = 1; // -- number of first lines
						$message = " ";
						$delimiter = " ";
						$flatfiledata = flatfile_in_arrayNAEDO($filename_with_path,$ignoreHeaderLines,$delimiter); 
						$count = count($flatfiledata) - 1; // -- avoid reading last line cause is a footer. 
						for($x=0; $x<$count; $x++) 
						{ 
								$countInside = arraycolumncounter($flatfiledata);
								$actiondate    = '';
								$accountnumber = '';
								$statusCode    = '';
								echo"<tr>";
								for($z=0;$z<$countInside;$z++)
								{
										if($z == 0 && isset($flatfiledata[$x][$z]))
										{	
											//print($flatfiledata[$x][$z]);
											// - RI postition 1 – 2
												$RI = substr($flatfiledata[$x][$z],0,2);
												echo "<td>".$RI."</td>";
											// -- Action Date position 59 - 64 [YYMMDD]
												$actiondate = substr($flatfiledata[$x][$z],94,6);
												//$message = $actiondate;

												//$actiondate = $date->format('Y-m-d');
												$cuY = substr(date('Y'),0,2);
												$actiondate = date('Y-m-d', strtotime($cuY.$actiondate));
												echo "<td>".$actiondate."</td>";

											// -- status code position 68 – 70  
												$statusCode = substr($flatfiledata[$x][$z],67,2);
												echo "<td>".$statusCode."</td>";
										}
											// -- Contract Reference position 14 81 – 94. 
										if($z == 0 && isset($flatfiledata[$x][$z]))
										{   $accountnumber = substr($flatfiledata[$x][$z],80,9);
											echo "<td>".$accountnumber."</td>";
										}
								}
								// -- Call to update EFT Bank Unpaid Mecantile.
								if(!empty($accountnumber)){
									$dbnameobj = 'ecashpdq_paycollections';
									$queryFields = null;
									$whereArray = null;
									$arrayCollections = null;
									$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
									$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
									$queryFields[] = '*';
									$collectionObj = search_dynamic($dbnameobj,'collection',$whereArray,$queryFields);
									$whereArray = null;
									$whereArray[] = "bankresponsecode = '{$statusCode}'"; // -- Bank Response Code.
									$queryFields = null;
									
									//$queryFields[] = 'collectionid';
									$queryFields[] = 'bankresponsecode';
									$queryFields[] = 'bankresponsecodedesc';

									$bankresponsecodeObj = search_dynamic($dbnameobj,'bankresponsecodes',$whereArray,$queryFields);;
									if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path; $FileMove = false; }
									else
									{								
										foreach($bankresponsecodeObj as $row)
										{
											$arrayCollections[1] = $row['bankresponsecode'];
											$arrayCollections[2] = $row['bankresponsecodedesc'];									
										}
										foreach($collectionObj as $row)
										{$arrayCollections[3] = $row['collectionid'];}
										
										// -- if response code = '00' -> successfully.
										if(isset($arrayCollections[1]))
										{
											if($arrayCollections[1] == '00')
											{
												if(!empty($collectionObj)){
												// -- Notify the client about successfully Debit Order.
												// -- Welcome SMS Template.
												$smstemplatename = 'crpaid';
												if(isset($row['CustomerId']))
												{$idnumber 		 = $row['CustomerId'];}
												else{$idnumber = '0';}
												
												if(isset($row['ApplicationId']))
												{$ApplicationID   = $row['ApplicationId'];}
												else{$ApplicationID = '0';}
												// -- Send auto Welcome SMS. 
												//auto_sms($idnumber,$smstemplatename,$ApplicationID);
												// -- Welcome Email Template.
												//$emailtemplate = 'crwelcome';
												// -- Send auto Welcome Email.
												//auto_email($idnumber,$emailtemplate,$ApplicationID);
												}
											}
										}
										$message = updatecollectionStatus($arrayCollections);
									}
								}
								if(empty($message)){$message = 'success';}
								
								if(!empty($accountnumber)){
								echo "<td>$message</td>";
								}		
								echo"</tr>";
						     }	
						break;
						}
					  // -- EFT equals .upl							
						case "upl":
						{
							// ----- call operation & file content ------ //
							$flatfiledata = null;
							$count = 0;
							$x=0;
							$z = 0;
							$whereArray = array();
							$collectionObj = null;
							$bankresponsecodeObj = null;

							$actiondate = '';
							$accountnumber = '';
							$statusCode = '';

							$ignoreHeaderLines = 11; // -- number of first lines
							$message = " ";
							$delimiter = " ";

							$flatfiledata = flatfile_in_array($filename_with_path,$ignoreHeaderLines,$delimiter); 
							$flatfiledata = array_filter($flatfiledata);
							array_filter($flatfiledata, function($value) { return !is_null($value) && $value !== ''; });
							$count = count($flatfiledata); 
							// -- Elimenates Spaces -- // 
								$arrayContentUPL = null;
															
								for($x=0; $x<$count; $x++) 
								{
									//$arrayContentUPL = array();
									$i = 0;
									$countInside = arraycolumncounter($flatfiledata);
									for($z=-1;$z<$countInside;$z++)
									{	
										if(isset($flatfiledata[$x][$z]))
										{
											if($flatfiledata[$x][$z] == '')
											{ // -- Elimite Spaces
												unset($flatfiledata[$x][$z]);
											}
											else
											{ // -- Build a new array without spaces
												$arrayContentUPL[$x][$i]=$flatfiledata[$x][$z];
												$i = $i + 1;
											}
											
										}
										

									}
								}
							// Debug here print_r($arrayContentUPL);
							$RId = 'EFT';
							for($x=0; $x<$count; $x++) 
								{ 
									$countInside = arraycolumncounter($arrayContentUPL);
									$actiondate    = '';
									$accountnumber = '';
									$statusCode    = '';
									echo"<tr>";
									for($z=-1;$z<$countInside;$z++)
									{
										if( $z == 0 && isset($arrayContentUPL[$x][$z]))
										{
											if($arrayContentUPL[$x][$z] == '61')
											{
											}
											else
											{
												break;
											}
										}
											// => 61
											// -- Action Date [0][9]	 isset($flatfiledata[$x][9]) && 
											if($z == 9 && isset($arrayContentUPL[$x][$z]))
											{   echo "<td>".$RId."</td>";
												$actiondate = $arrayContentUPL[$x][$z];
												echo "<td>".$arrayContentUPL[$x][$z]."</td>";}
											// -- status code [0][16] isset($flatfiledata[$x][15]) && 
											if($z == 15 && isset($arrayContentUPL[$x][$z]))
											{   $statusCode = $arrayContentUPL[$x][$z];
												echo "<td>".$arrayContentUPL[$x][$z]."</td>";}
											// -- Client Reference. [0][24] isset($flatfiledata[$x][24]) && 
											if($z == 24 && isset($arrayContentUPL[$x][$z]))
											{   $accountnumber = $arrayContentUPL[$x][$z];
												echo "<td>".$arrayContentUPL[$x][$z]."</td>";
											}
											//if(isset($flatfiledata[$x][$z]))
											//{echo $flatfiledata[$x][$z]."<br/>";}
									}
									// -- Call to update EFT Bank Unpaid Mecantile.
									$tablename = 'collection';
									$dbnameobj = 'ecashpdq_paycollections';
									$queryFields = null;

									if(!empty($accountnumber)){
										$arrayCollections = null;
										$whereArray = null;								
										$whereArray[] = "Client_Reference_1 = '{$accountnumber}'"; // -- Action Date.
										$whereArray[] = "Action_Date = '{$actiondate}'"; // -- Client Reference.
										$queryFields[] = '*';
										$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
										
										$whereArray = null;
										$whereArray[] = "bankresponsecode = {$statusCode}"; // -- Bank Response Code.
										$tablename = 'bankresponsecodes';
										$bankresponsecodeObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);;
										
										if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path;$FileMove = false;}
										else
										{								
											foreach($bankresponsecodeObj as $row)
											{
												$arrayCollections[1] = $row['bankresponsecode'];
												$arrayCollections[2] = $row['bankresponsecodedesc'];									
											}
											foreach($collectionObj as $row)
											{$arrayCollections[3] = $row['collectionid'];}
											
											// -- if response code = '00' -> successfully.
											if(isset($arrayCollections[1]))
											{
												if($arrayCollections[1] == '00')
												{
													// -- Notify the client about successfully Debit Order.
													// -- Welcome SMS Template.
													$smstemplatename = 'crpaid';
													$idnumber 		 = $collectionObj['CustomerId'];
													$ApplicationID   = $collectionObj['ApplicationId'];
													// -- Send auto Welcome SMS. 
													//auto_sms($idnumber,$smstemplatename,$ApplicationID);
													// -- Welcome Email Template.
													//$emailtemplate = 'crwelcome';
													// -- Send auto Welcome Email.
													//auto_email($idnumber,$emailtemplate,$ApplicationID);
												}
											}
											//print_r($arrayCollections);
											$message = updatecollectionStatus($arrayCollections);
										}
									}
									if(empty($message)){$message = 'success';}
									if(!empty($accountnumber)){
									echo "<td>$message</td>";}
									echo"</tr>";
								}	
								break;
							   }
							    // -- EFT equals .upl							
								case "out":
								{
									// ----- call operation & file content ------ //
									$flatfiledata = null;
									$count = 0;
									$x=0;
									$z = 0;
									$whereArray = array();
									$collectionObj = null;
									$bankresponsecodeObj = null;

									$actiondate = '';
									$accountnumber = '';
									$statusCode = '';

									$ignoreHeaderLines = 10; // -- number of first lines
									$message = "";
									$delimiter = " ";

									$flatfiledata = flatfile_in_arrayOUT($filename_with_path,$ignoreHeaderLines,$delimiter); 
									//print_r($flatfiledata);
									$count = count($flatfiledata); 
									$RId = 'EFT';
								for($x=0; $x<$count; $x++) 
								{ 
									$countInside = arraycolumncounter($flatfiledata);
									$actiondate    = '';
									$accountnumber = '';
									$statusCode    = '';
									
									echo"<tr>";
									for($z=-1;$z<$countInside;$z++)
									{
										// -- Only focus on Status Code 50
										// -- status code [0][16] isset($flatfiledata[$x][15]) && 
										if($z == 0 && isset($flatfiledata[$x][$z]))
										{   
											$statusCode = $flatfiledata[$x][$z];
										}
										 if($statusCode == '50')
										 {
											// -- Action Date [0][9]	 isset($flatfiledata[$x][9]) && 
											if($z == 9 && isset($flatfiledata[$x][$z]))
											{   
												$actiondate = $flatfiledata[$x][$z];
												// -- convert from yyyy/mm/dd to yyyy-m-d.
												$actiondate = date('Y-m-d', strtotime($actiondate));												
											}
							
											// -- Client Reference. [0][24] isset($flatfiledata[$x][24]) && 
											if($z == 11 && isset($flatfiledata[$x][$z]))
											{   $accountnumber = $flatfiledata[$x][$z];
											}
										 }
									}
								   // -- Mapp to display on page.
								   if($statusCode == '50')
								   {
									   // -- RI
									   echo "<td>".$RId."</td>";
									   // -- Action Date.
									   echo "<td>".$actiondate."</td>";
									   // -- Status Code.
									   echo "<td>".$statusCode."</td>";		
									   // -- Account number.
									   echo "<td>".$accountnumber."</td>";											   
								   }
								   // -- Call to update EFT Bank Unpaid Mecantile.
									$tablename = 'collection';
									$dbnameobj = 'ecashpdq_paycollections';
									if(!empty($accountnumber))
									{
										$arrayCollections = null;
										$collectionObj = GetCollectinobjout($accountnumber,$actiondate,$dbnameobj,$tablename);
										$bankresponsecodeObj = Getbankrespcodeout($statusCode,$dbnameobj,$tablename);
										if($collectionObj->rowCount() <= 0){$message = 'no record found to update ; '.$filename_with_path;$FileMove = false;}
										else
										{								
											foreach($bankresponsecodeObj as $row)
											{
												$arrayCollections[1] = $row['bankresponsecode'];
												$arrayCollections[2] = $row['bankresponsecodedesc'];									
											}
											foreach($collectionObj as $row)
											{$arrayCollections[3] = $row['collectionid'];}
											$message = updatecollectionStatus($arrayCollections);
										}
									}
									
									if(empty($message)){$message = 'success';}
									if(!empty($accountnumber)){
									echo "<td>$message</td>";}
									echo"</tr>";
								}
									break;
								}
								// Debi Check Mandate Response 
								case "xpresp":
								{
									$RI = '';
									$actiondate = '';
									$accountnumber = '';
									$statusCode = '';

									$ignoreHeaderLines = 1; // -- number of first lines
									$message = " ";
									$delimiter = " ";
									$flatfiledata = flatfile_in_arrayNAEDO($filename_with_path,$ignoreHeaderLines,$delimiter); 
									$count = count($flatfiledata) - 1; // -- avoid reading last line cause is a footer. 								
									print_r($flatfiledata);
								
								echo"<tr>";
									   // -- RI
									   echo "<td>".$RId."</td>";
									   // -- Action Date.
									   echo "<td>".$actiondate."</td>";
									   // -- Status Code.
									   echo "<td>".$statusCode."</td>";		
									   // -- Account number.
									   echo "<td>".$accountnumber."</td>";	
									   // -- Amount.
									   echo "<td>".$accountnumber."</td>";

								//Feedback
								if(empty($message)){$message = 'success';}
									if(!empty($accountnumber)){
									echo "<td>$message</td>";}
									echo"</tr>";
								}
								break;
								
					}			
						// -- Move the file to Backup directory.
						if($FileMove)
						{move_file($filename_with_path, $backuppath);}
					}	
					   ?>	  
	<table>
   </div>	
</div>   
