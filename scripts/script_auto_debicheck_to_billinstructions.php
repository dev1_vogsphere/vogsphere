<?php
/*
19/02/2023
Rules :
- Get the debit check with below conditions, the create debit order instruction.
- Status_Code in ('Mandate Accepted') in a list current 'Mandate Accepted'
*/
$title='script_auto_debicheck_to_billinstructions';

// -- Declarations
$dir = $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
$dir = str_replace("scripts","",$dir);
require $dir.'database.php';
require $dir.'script_rule_two_day_service.php'; // -- 18.09.2021 Rule #1 Two Day Service..
$message = '';
$arrayInstruction = null;
$collection_data = null;
// -- Dynamically API for data collection.
$tablename = 'mandates';
$dbnameobj = 'ecashpdq_paycollections';
$queryFields = null;
$whereArray = null;
//$statusList = "('Mandate Accepted','Mandate Accepted RMS')"; // -- Commented 2024/01/30 Mulu
$statusList = "('Mandate Authenticated by the Client')"; // -- Added 2024/01/30 Mulu
$today = date('Y-m-d');
// -- Payment Run Count
?>
<div class="row">
  <div class="table-responsive">
	<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">
	  <tr>
					  <td>Feedback</td>
					  <td>Client_Id</td>
					  <td>Client_Reference_1</td>
					  <td>Client_Reference_2</td>
					  <td>Client_ID_Type 		</td>
					  <td>Client_ID_No 		</td>
					  <td>Initials 			</td>
					  <td>Account_Holder 		</td>
					  <td>Account_Type 		</td>
					  <td>Account_Number 		</td>
					  <td>Branch_Code 		</td>
					  <td>No_Payments 		</td>
					  <td>Service_Type 		</td>
					  <td>Service_Mode 		</td>
					  <td>Frequency 			</td>
					  <td>Action_Date 		</td>
					  <td>Bank_Reference 		</td>
					  <td>Amount 				</td>
		</tr>
				<?php
				$arrayCollections = null;
				// -- from first of the previous month till today.
				// --  first day of last month
				$datestring=date('Y-m-d').' first day of last month';
				$dt = date_create($datestring);
				$startdate = $dt->format('Y-m-01'); //2011-02
				$whereArray = null;
				//$whereArray[] = "pAIN012_response_code IN {$statusList} and mandate_current_status='INACTIVE' and not debitOrderStatus in ('Scheduled') order by first_collection_date limit 10000000"; // -- Action Date. //group by Client_Reference_1
				$whereArray[] = "pAIN012_response_code IN {$statusList} and debitOrderStatus IS NULL or debitOrderStatus=' '  AND  createdon > (NOW() - INTERVAL 30 DAY) order by first_collection_date limit 10000000"; // -- Action Date. //group by Client_Reference_1
				$queryFields[] = '*';
				$collectionObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
				//get Abbreviated Short Company name
				/* -- Database Declarations and config:*/
				$pdo = Database::connectDB();
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
							
				//print_r($collectionObj);
				if(empty($collectionObj))
				{
					$message = 'no record found to update';
					echo"<tr><td>".$message."</td><td></tr>";
				}
				else
				{
					foreach($collectionObj as $row)
					{	$valid = true;
						$message = '';
						if(isset($row['usercode']))
						{
	
							$arrayInstruction[1] =  $row['usercode'];
							//get shortname
							$sql = 'SELECT * FROM client WHERE usercode = ?';
							$q = $pdo->prepare($sql);
							$q->execute(array($row['usercode']));
							$shortname = $q->fetch(PDO::FETCH_ASSOC);
							
						}else{$arrayInstruction[1] = '';}
						if(isset($row['contract_reference'])){$arrayInstruction[2] =  $row['contract_reference'];}else{$arrayInstruction[2] = '';}
						if(isset($row['contract_reference'])){$arrayInstruction[3] =  $row['contract_reference'];}else{$arrayInstruction[3] = '';}
						if(isset($row['document_type']    )){$arrayInstruction[4] =  $row['document_type']    ;}else{$arrayInstruction[4] = '';}
						if(isset($row['debtor_identification']      )){$arrayInstruction[5] =  $row['debtor_identification']      ;}else{$arrayInstruction[5] = '';}
						if(isset($row['debtor_account_name']		  )) {$arrayInstruction[6] = substr($row['debtor_account_name'], 0, 1);  }else{$arrayInstruction[6] = '';}
						if(isset($row['debtor_account_name']    )){$arrayInstruction[7] =  $row['debtor_account_name']    ;}else{$arrayInstruction[7] = '';}
						if(isset($row['debtor_account_type']      ))
						{
							//$arrayInstruction[8] =  $row['debtor_account_type'];
							if($row['debtor_account_type']=="CURRENT")
							{
								$arrayInstruction[8] =1;
							}
							if($row['debtor_account_type']=="SAVINGS")
							{
								$arrayInstruction[8] =2;
							}
							if($row['debtor_account_type']=="TRANSMISSION")
							{
								$arrayInstruction[8] =3;
							}
						}
						else
						{
							$arrayInstruction[8] = '';
						}
						if(isset($row['debtor_account_number']    )){$arrayInstruction[9] =  $row['debtor_account_number']    ;}else{$arrayInstruction[9] = '';}
						if(isset($row['debtor_branch_number']       )){$arrayInstruction[10] = $row['debtor_branch_number']       ;}else{$arrayInstruction[10] ='';}
						if(isset($row['No_Payments']       )){$arrayInstruction[11] = $row['No_Payments'];/*'99';*/}else{$arrayInstruction[11] ='';}
						if(isset($row['Service_Type']      )){$arrayInstruction[12] = $row['Service_Type']      ;}else{$arrayInstruction[12] ='NADREQ';}
						if(isset($row['Service_Mode']      )){$arrayInstruction[13] = $row['Service_Mode']      ;}else{$arrayInstruction[13] ='NADEO';}
						echo "I am here";
						if(isset($row['frequency']))
						{
							echo $row['frequency'];
							//$arrayInstruction[14] = $row['frequency'];
							if($row['frequency']=="WEEK")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
							if($row['frequency']=="FRTN")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
							if($row['frequency']=="MNTH")
							{
								$arrayInstruction[14] ="Monthly";
								
							}
							if($row['frequency']=="QURT")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
							if($row['frequency']=="MIAN")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
							if($row['frequency']=="YEAR")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
							if($row['frequency']=="ADHO")
							{
								$arrayInstruction[14] = $row['frequency'];
							}
						
							
						}	else{
							$arrayInstruction[14] ='';
							}
					    echo $arrayInstruction[14];
						
						if(isset($row['first_collection_date']       )){$arrayInstruction[15] = $row['first_collection_date'];
						if(isset($row['contract_reference']    )){$arrayInstruction[16] = $shortname['shortname']  ;}else{$arrayInstruction[16] ='';}
						if(isset($row['collection_amount']            )){$arrayInstruction[17] = $row['collection_amount']            ;}else{$arrayInstruction[17] ='';}
						$arrayInstruction[18] = '1';//$keydata['Status_Code'];
						$arrayInstruction[19] = 'pending';//$keydata['Status_Description'];
						$arrayInstruction[20] = '';//$keydata['mandateid'];
						$arrayInstruction[21] = '';//$keydata['mandateitem'];
						if(isset($row['IntUserCode']       )){$arrayInstruction[22] = $row['IntUserCode']       ;}else{$arrayInstruction[22] ='';}
						if(isset($row['usercode']          )){$arrayInstruction[23] = $row['usercode']          ;}else{$arrayInstruction[23] ='';}

						if(isset($row['tracking_entry_class'])){$arrayInstruction[24] = $row['tracking_entry_class'];}else{$arrayInstruction[24] = 0;}
						if(isset($row['Contact_No'])){$arrayInstruction[25] = $row['Contact_No'];}else{$arrayInstruction[25] = '';}
						if(isset($row['Notify'])){$arrayInstruction[26] = $row['Notify'];}else{$arrayInstruction[26] = '';}
						// -- Debit Order Date - 03/12/2021
						$arrayInstruction[30] = $row['first_collection_date'];

						// -- System user - 9901019999999
						$arrayInstruction[27] = '9901019999999';

						// -- Rule #1 if payment is zero or empty (it means its an infinite run until otherwise instructed.), else check total count of run against
						// -- number of payment runs
						// -- initialise to zero
						$arrayInstruction[11] = '00';
						$ruleTwoDayMsg = '';
						if(!empty($arrayInstruction[15]))
						{
						// -- Rule #1 - 18.09.2021 = Validate Rule Two Day Services -- //
							if($arrayInstruction[12] == 'TWO DAY' or $arrayInstruction[12] == 'SAMEDAY' or $arrayInstruction[12] == 'TWODAY' or $arrayInstruction[12] == 'SAME DAY') // -- Service Day
							{
								// -- Action Date Validation.
								$result = rule_two_day_service($arrayInstruction[15]);
								$results = explode("|", $result);
								if(isset($results[1]))// -- There is a proposed Action Date
								{
									$ruleTwoDayMsg = '.<b>Action Date :</b>'.$arrayInstruction[15].' is a weekend or holiday. system auto generated <b>Action Date:</b>'.$results[1];
									$arrayInstruction[15] = $results[1];
								}
							}
						///////////////////////////////////////////////////////////////////////////////
						}
						else{$message = 'no actiondate'; $valid = false;}
						// -- if its valid process the creating of the batch instruction.
						if ($valid)
						{
						  $message = auto_create_billinstruction($arrayInstruction);
						  //echo ('mULU TEST '.$message);
						  error_log( print_r( $message, true ) );
						  //Update status Mandate table debitOrderStatus
						  $idmandate=$row["idmandate"];
						  $sql = "UPDATE ecashpdq_paycollections.mandates SET debitOrderStatus='Scheduled' WHERE idmandate=$idmandate";
								
						  if ($message=='success') 
						  {
							  if ($pdo ->query($sql) === TRUE) 
							  {
								  $message = $message.";Status updated";
							  }
						  
						  } else {
						  	$message = $message.";Error: Not scheduled";
							error_log( print_r( $message, true ) );
						  }
		
						}
						if(empty($message)){$message = 'success'.$ruleTwoDayMsg;}
						
						echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";
					}
				}}
				//echo"<tr><td>".$message."</td><td>".$arrayInstruction[1]."</td><td>".$arrayInstruction[2]."</td><td>".$arrayInstruction[3]."</td><td>".$arrayInstruction[4]."</td><td>".$arrayInstruction[5]."</td><td>".$arrayInstruction[6]."</td><td>".$arrayInstruction[7]."</td><td>".$arrayInstruction[8]."</td><td>".$arrayInstruction[9]."</td><td>".$arrayInstruction[10]."</td><td>".$arrayInstruction[11]."</td><td>".$arrayInstruction[12]."</td><td>".$arrayInstruction[13]."</td><td>".$arrayInstruction[14]."</td><td>".$arrayInstruction[15]."</td><td>".$arrayInstruction[16]."</td><td>".$arrayInstruction[17]."</td></tr>";
				//print_r($arrayInstruction);	
				?>
	<table>
   </div>
</div>
