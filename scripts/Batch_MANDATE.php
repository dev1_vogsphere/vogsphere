<?php
//Global Variables
$Record_Identifier="MRH";
$CurrentDate=date("Ymdhisa");
//$User_Code="503A"; // to be added to the database
$Creation_Date=date("Y-m-d");//file created date
$Service_Identification="MANIN";
$Nominated_Account_Number="003";
$Authentication_Type="BATCH";
$debtor_auth_code="0227";  //Batch or Delayed Real Time Authentication
$Processing_Type="Debi Mandate";
$Filename ='../Files/Collections/Out/Mandate'.$User_Code.$CurrentDate.".txt";
$Processing_Bank="Mercantile";

$connect = mysqli_connect("localhost","ecashpdq_root","nsa{VeIsl)+h","ecashpdq_paycollections");
//$connect = mysqli_connect("localhost","root","Mulu$2625","ecashpdq_paycollections");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL:JDBC query get get sequence number " . mysqli_connect_error();
  }

// Get user codes
  $getclient ="SELECT * FROM ecashpdq_paycollections.client";
  $resultclients = mysqli_query($connect,$getclient);

  while($row = $resultclients->fetch_assoc())
  {
    $User_Code=($row["usercode"]);
    $Debicheck_User_Code=($row["debicheckusercode"]);
    $Filename ='../Files/Collections/Out/'.$User_Code.$CurrentDate.".txt";

    //Query gen numbers
    $gennumbers ="SELECT next_seq_number,usercode FROM ecashpdq_paycollections.mandates_gen_number WHERE usercode='$User_Code'";
    $gennumbers = mysqli_query($connect,$gennumbers);
    //$Generation_Number=$result; //Starts 0001 increment by one per Batch Run
    if ($gennumbers->num_rows > 0)
    {
      while($row = $gennumbers->fetch_assoc())
      {
          $previous_seq_number=($row["next_seq_number"]);
          $Generation_Number=($row["next_seq_number"]);
     	  $New_Seq_Number= (($row["next_seq_number"])+1);// increment next sequence number
    	  $New_Seq_Number=str_pad($New_Seq_Number,4,"0",STR_PAD_LEFT);
          $usercode=$row["usercode"];
      }
    }
    else
    {
        echo "Bad";
        echo "0 results";
    }

//Create file
$query ="SELECT * FROM ecashpdq_paycollections.mandates WHERE IntUserCode='$User_Code' and debtor_auth_code='$debtor_auth_code' and Status_desc='pending'";
$result = mysqli_query($connect,$query);
$fileHandle = fopen($Filename, 'w') or die ("Can't open file");

if ($result->num_rows > 0)
{

//update gen number
 $sql = "UPDATE ecashpdq_paycollections.mandates_gen_number SET next_seq_number='$New_Seq_Number',previous_seq_number='$previous_seq_number' WHERE usercode='$User_Code'";
    if ($connect->query($sql) === TRUE) {
      echo "Record updated Generation Number ";
    } else {
      echo "Error: " . $sql . "<br>" . $connect->error;
    }

//Header Record
fwrite ($fileHandle, $Record_Identifier);
fwrite ($fileHandle, $Debicheck_User_Code);
fwrite ($fileHandle, $Creation_Date);
fwrite ($fileHandle, $Generation_Number);
fwrite ($fileHandle, $Service_Identification);
fwrite ($fileHandle, $Nominated_Account_Number);
fwrite ($fileHandle, $Authentication_Type);
fwrite ($fileHandle, "\n");

//Initiation Request Record
 while($row = $result->fetch_assoc())
	{
          fwrite ($fileHandle, "M"); //Record Identifier
          $reference =str_pad($row["reference"], 35, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $reference); // 35 User Reference
		  $contract_reference =str_pad($row["contract_reference"], 14, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $contract_reference); // 14 Contract Reference
		  $tracking_indicator =str_pad($row["tracking_indicator"], 1, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $tracking_indicator); // 1 Contract Reference
		  $debtor_auth_code =str_pad($row["debtor_auth_code"], 4, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $debtor_auth_code); // 4 Debtor Authentication Code
		  $instalment_occurence =str_pad($row["instalment_occurence"], 4, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $instalment_occurence); // 4 Instalment Occurence
		  $frequency =str_pad($row["frequency"], 4, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $frequency); // 4 Frequency
		  $mandate_initiation_date=str_pad($row["mandate_initiation_date"],10);
          fwrite ($fileHandle, $mandate_initiation_date); // 10 Mandate Initiation Date
		  $first_collection_date=str_pad($row["first_collection_date"],10);
          fwrite ($fileHandle, $first_collection_date); // 10 First Collection Date
		  $collection_amount_currency =str_pad($row["collection_amount_currency"], 3, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $collection_amount_currency); // 3 Collection Amount Currency
		  //$collection_amount=str_replace(".","",strval($row["collection_amount"]));// remove amount separator
          $collection_amount=str_pad($row["collection_amount"], 14, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $collection_amount); // 14 Collection  Amount

		  $Maximum_Collection_Currency =str_pad($row["maximum_collection_currency"], 3, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $Maximum_Collection_Currency); // 3 Maximum_Collection_Currency

		  $Maximum_Collection_Amount =str_pad($row["maximum_collection_amount"], 14, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $Maximum_Collection_Amount); // 14 Maximum_Collection_Amount

		  $entry_class =str_pad($row["entry_class"], 4, '0', STR_PAD_LEFT);
		  fwrite ($fileHandle, $entry_class); // 4 Entry Class

		  $debtor_account_name =str_pad($row["debtor_account_name"], 35, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debtor_account_name); // 35 Debtor Account Name

		  $debtor_identification =str_pad($row["debtor_identification_type"].$row["debtor_identification"], 35, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debtor_identification); // 35 Debtor Identification

		  $debtor_account_number =str_pad($row["debtor_account_number"], 19, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $debtor_account_number); // 19 Debtor Account Number
		  //Hash Total
          $Hash_Total+=$row["debtor_account_number"];// Total number of Debit value

		  $debtor_account_type =str_pad($row["debtor_account_type"], 12, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debtor_account_type); // 12 Debtor Account Type

		  $debtor_branch_number =str_pad($row["debtor_branch_number"], 6, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $debtor_branch_number); // 6 Debtor Branch Number

		  $debtor_contact_number =str_pad($row["debtor_contact_number"], 30, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debtor_contact_number); // 30 Debtor Telephone Contact Details

		  $debtor_email =str_pad($row["debtor_email"], 50, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debtor_email); // 4 Debtor Email Contact Details

		  $collection_date =str_pad($row["collection_date"], 2, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $collection_date); // 2 Collection Day

		  $date_adjustment_rule_indicator =str_pad($row["date_adjustment_rule_indicator"], 1, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $date_adjustment_rule_indicator); // 1 Date Adjustment Rule Indicator

		  $adjustment_category =str_pad($row["adjustment_category"], 1, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $adjustment_category); // 1 Adjustment Category

		  $adjustment_rate =str_pad($row["adjustment_rate"], 6, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $adjustment_rate); // 6 Adjustment Rate

		  $adjustment_amount_currency =str_pad($row["adjustment_amount_currency"], 3, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $adjustment_amount_currency); // 3 Adjustment Amount Currency

		  $adjustment_amount =str_pad($row["adjustment_amount"], 14, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $adjustment_amount); // 14 Adjustment Amount

		  $first_collection_amount_currency =str_pad($row["first_collection_amount_currency"], 3, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $first_collection_amount_currency); // 3 First Collection Amount Currency

		  $first_collection_amount =str_pad($row["first_collection_amount"], 14, '0', STR_PAD_LEFT);
          fwrite ($fileHandle, $first_collection_amount); // 14 First Collection Amount

		  $debit_value_type =str_pad($row["debit_value_type"], 11, ' ', STR_PAD_RIGHT);
          fwrite ($fileHandle, $debit_value_type); // 11 Debit Value Type
		  fwrite ($fileHandle, "\n");

		  //Update status Mandate table
		  $idmandate=$row["idmandate"];
		  $sql = "UPDATE ecashpdq_paycollections.mandates SET status_desc='Submitted',status_code='00' WHERE idmandate=$idmandate";
          if ($connect->query($sql) === TRUE) {
            echo "Status updated";
          } else {
            echo "Error: " . $sql . "<br>" . $connect->error;
          }
	}
}else {
    echo "Bad";
    echo "0 results";
	}

//EFT Trailer Record
fwrite ($fileHandle, "MRT"); // 3 Record Identifier
fwrite ($fileHandle, $User_Code); //User Code add to database

$lines_file=COUNT(FILE($Filename))-1; // Transaction and Header count to calculate
$lines_file_mandate=COUNT(FILE($Filename))-2; //Number of Debit Transaction to calculate
$lines_file=str_pad($lines_file, 6, '0', STR_PAD_LEFT);

$lines_file_mandate=str_pad($lines_file_mandate, 6, '0', STR_PAD_LEFT);
$lines_file_Credit=str_pad("0", 6, '0', STR_PAD_LEFT);

fwrite ($fileHandle, $lines_file); //Mandate and Header Record Count
fwrite ($fileHandle, $lines_file_mandate); //Mandate Record Count

//Hash total
$Hash_Total=str_pad($Hash_Total, 18, '0', STR_PAD_LEFT);
$Hash_Total=substr($Hash_Total,6,12);
$Hash_Total=str_pad($Hash_Total, 16, '0', STR_PAD_LEFT);
fwrite ($fileHandle, $Hash_Total); //Hash Total to calculate

fclose($fileHandle);//close file
//Get file and file size
$generated_file=file_get_contents($Filename);
$filesize=filesize($Filename);
//$filesize = round($filesize / 1024, 2); // kilobytes with two digits
// Encode the image string data into base64
$data = base64_encode($generated_file);

// Display the output
//echo $data;
//Insert to queue
if($filesize>143)
{
  $sql1 ="insert into bankqueue (filename,usercode,encodeddata,date,status,transactiontype,gennumber,bankname)VALUES('$Filename','$User_Code','$data','$Creation_Date','pending','$Processing_Type',$Generation_Number,'$Processing_Bank')";
 if ($connect->query($sql1) === TRUE) {
      echo "Record updated Generation Number";
    } else {
      echo "Error: " . $sql1 . "<br>" . $connect->error;
    }
}
}
$connect->close();//Close Database connection
unlink($fileHandle);
$connect->close();//Close Database connection
?>
