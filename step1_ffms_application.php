<div class="row setup-content" id="step-1">
	<div class="controls">
		<div class="container-fluid" style="border:1px solid #ccc ">
             </br>
			 <div class="panel-body">


				<div class="row">

					<div class="col-md-3">
				 		<div class="form-group">
				 			<label for="form_branch_name">Branch Name</label>
				 				<select class="form-control" id="branch_name" name="branch_name">
				 					<?php
				 					$idBranchSelect = $branch_name;
				 					foreach($BranchList as $key => $rowData)
				 					{
				 						if($rowData->idBranch  == $idBranchSelect)
				 						{echo  '<option value="'.$rowData->branchusercode.'" selected>'.$rowData->branchusercode.' '.$rowData->branchname.'</option>';}
				 						else
				 						{echo  '<option value="'.$rowData->branchusercode.'">'.$rowData->branchusercode.' '.$rowData->branchname.'</option>';}
				 					}
				 					if(empty($BranchList ))
				 					{echo  '<option value="0" selected>No branch</option>';}

				 					?>
				 				</select>
				 						 <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
				 					<div class="help-block with-errors"></div>
				 			</div>
				  </div>

						 <div class="col-md-3">
						 	 <div class="form-group">
						 		 <label for="form_broker_name">Broker Name</label>
						 			 <select class="form-control" id="broker_name" name="broker_name">
						 				 <?php
						 				 $brokerSelect = $brokerList;
										foreach($brokerList as $key => $rowData )
										{
						 					 if($rowData->idBroker == $brokerSelect)
											 
						 					 {
												 echo  '<option value="'.$rowData->idBroker.'" selected>'.$rowData->brokerfirstname.' '.$rowData->brokersurname.'</option>';}
						 					 else
						 					 {
												 echo  '<option value="'.$rowData->idBroker.'">'.$rowData->brokerfirstname.' '.$rowData->brokersurname.'</option>';}
										}
										 if(empty($brokerList ))
										 {echo  '<option value="0" selected>No branch</option>';}

						 				 ?>
						 				 </select>
						 						<!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
						 				 <div class="help-block with-errors"></div>
						 		 </div>
						 </div>

                 <div class="col-md-3">
		                <div class="form-group">
		                  <label for="form_product_service">Product/Service</label>
		                    <select class="form-control" id="product_service" name="product_service">
												  <?php
												  $Policy_idSelect = $product_service;
												  foreach($PolicyList as $key => $rowData)
												  {
													  
													 if($rowData->Policy_id  == $Policy_idSelect)
													  {
														  echo  '<option value="'.$rowData->Policy_id.'" selected>'.$rowData->Policy_name.' - '.$rowData->Policy_desc.'</option>';
														 // $Policy2=$rowData->Policy_id;
													}
													  else
													  {echo  '<option value="'.$rowData->Policy_id.'">'.$rowData->Policy_name.' - '.$rowData->Policy_desc.'</option>';
												      //$Policy2=$rowData->Policy_id;
														}
												  }
												  if(empty($PolicyList ))
												  {echo  '<option value="0" selected>No product/service</option>';}

												  ?>
		                      </select>
		                         <!--<input id="Client_ID_Type" type="text" name="Client_ID_Type" class="form-control" placeholder="" required="required" data-error="Client ID Type is required.">-->
		                      <div class="help-block with-errors"></div>
		                  </div>
                 </div>

        </div>


           </div>
     </div>
 </div>
</br>
<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
<button class="btn btn-primary previousBtn btn-lg pull-right" type="button" style="visibility: hidden;" >space</button>
<!-- <button class="btn btn-primary previousBtn btn-lg pull-right" id="Back" type="button" >Back</button>-->
<a style="<?php echo $backHTML;?>" class="btn btn-primary previousBtn btn-lg pull-right" href="finfindmember">Back</a>

</div>
<br/>
