<?php
// -- BOC 2017.06.24
// -- To avoid : Fatal error: Cannot redeclare class.
// -- EOC 2017.06.24
if (!class_exists('Database')) 
{
class Database 
{
	private static $dbName = 'ecashpdq_eloan' ; 
	private static $dbHost = 'localhost' ;
	private static $dbUsername = 'ecashpdq_root';
	private static $dbUserPassword = 'nsa{VeIsl)+h';
	
	private static $cont  = null;
	public static $conn;
	
	public function __construct() 
	{
		exit('Init function is not allowed');
	}
	
		public static function connectDB()
		{
			self::$conn = mysql_connect("localhost", self::$dbUsername, self::$dbUserPassword) or die(mysql_error());
			return self::$conn;
		}
	
	public static function connect()
	{
	   // One connection through whole application
       if ( null == self::$cont )
       {      
        try 
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);  
        }
        catch(PDOException $e) 
        {
          die($e->getMessage());  
        }
       } 
       return self::$cont;
	}
	
	public static function disconnect()
	{
		self::$cont = null;
	}
	//  -- BOC encrypt password - 16.09.2017.	
	public static function encryptPassword($password)
	{
			/**
			 * In this case, we want to increase the default cost for BCRYPT to 12.
			 * Note that we also switched to BCRYPT, which will always be 60 characters.
			 */
			$options = ['cost' => 12,];
			return password_hash($password, PASSWORD_BCRYPT, $options);
	}
	// 	-- EOC encrypt password - 16.09.2017.
}

// -- Hash Every Page.
function redirect($url) 
{
    ob_start();
	$hash="?guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}

// -- Hash to Page with parameters.
function redirectParam($url) 
{
    ob_start();
	$hash="&guid=".md5(date("h:i:sa"));
    header('Location: '.$url.$hash);
    ob_end_flush();
    die();
}
// -- Contains Characters
// returns true if $needle is a substring of $haystack
function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}
}
?>