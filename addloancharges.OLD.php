<?php
/* All the Approved = APPR Loans to scheduled to run monthly to add Service Fees 

$title='eLoan - Add Loan Charges';
$menu_item = 3;
 $page_content='content/text_addloancharges.php';
  include_once('master.php');

*/
require 'database.php';

$WebServiceURL = "http://ecashmeup.com/ws_paymentschedules?status=APPR";
$chargeid = '3'; // -- Monthly Service Fee.
$message  = '';
$count    = '';
$applicationId = '';
$scheduleddate = '';

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// And if you want to fetch multiple paymentschedule IDs:
foreach($sxml->paymentschedule as $paymentschedule)
{	
	// -- 1. Check if the chargeid,applicationId does not already exist.				
	$valid = CheckFunction($paymentschedule->applicationId,$chargeid,$paymentschedule->item);
	$applicationId  = $paymentschedule->applicationId;
	$scheduleddate  = $paymentschedule->scheduleddate;
	$id = $paymentschedule->id;
	$today = date("Y-m-d");
	
	if($valid == false)
	{	$message = $message."<p class='alert alert-error'>Admin Fee Charges - ($applicationId for $scheduleddate)</p><br/>";
		echo "\nAdmin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - Already exist.\n"; 
	}
	else
	{
		// -- 2. ScheduledDate Must be Smaller than TodayDate.
		$date1=date_create($scheduleddate);	// -- ScheduledDate
		$date2=date_create($today);
		$diff=date_diff($date1,$date2);		// -- TodayDate
		$difference = 0;
		
		$difference = $diff->format("%R%a");
		
		// -- if the scheduledDate older than Today then it must add the charges.
		if($difference >= 0)
		{
			if($paymentschedule->item >= 0)
			{
				$count = AddCharges($paymentschedule->applicationId,$chargeid,$paymentschedule->item);
				if($count > 0)
				{
				$message = $message."<p class='alert alert-error'>Admin Fee Charges - ($applicationId for $scheduleddate)</p><br/>";	
				echo "\nAdmin Fee Charges - (ID : $id,Loan ApplicationId : $applicationId for Scheduled Date : $scheduleddate) - successful.\n"; 
				}
			}
		}
	}
}

// -- Add LoanCharges
function AddCharges($applicationid,$chargeid,$item)
{
	if($chargeid == ''){$chargeid = 0;}
	
	if($item == ''){$item = 0;}
	
	if($applicationid == ''){$applicationid = 0;}
	
		$count = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO loanappcharges  (chargeid,applicationid, item) VALUES (?,?,?)"; 	
			
		$q = $pdo->prepare($sql);
		$q->execute(array($chargeid,$applicationid,$item));
		Database::disconnect();
		$count = $count + 1;
			
	return $count;
}
// -- Check if Charges Exist.
function CheckFunction($applicationid,$chargeid,$item)
{
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	if($chargeid == ''){$chargeid = 0;}
	
	if($item == ''){$item = 0;}
	
	if($applicationid == ''){$applicationid = 0;}
	
	$valid = true;
  
 // -- Check Payment Charges.
	$sql = 'SELECT * FROM loanappcharges WHERE ApplicationId = ? AND item = ? AND chargeid = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($applicationid,$item,$chargeid));
    $dataloanappcharges = $q->fetchAll();	
  
	Database::disconnect();
  
  // -- SizeOf an Array.
	if(sizeof($dataloanappcharges) > 0)
	{
		$valid = false;
	}
  return $valid;
}
?>