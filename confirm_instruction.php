<style>
.font-bold {
  font-weight: bold;
}
</style>
<div class="controls">
<div class="container-fluid" style="border:1px solid #ccc ">
<div class="row">
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Initials</label>
				<label name="InitialsLbl" id="InitialsLbl"></label>
			</div>
		</div>	
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Customer Reference</label>
				<label name="Reference_1Lbl" id="Reference_1Lbl"></label>
			</div>
		</div>	
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Customer Contract Reference:</label>
				<label name="Reference_2Lbl" id="Reference_2Lbl"></label>
			</div>
		</div>
		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Customer Identification Type:</label>
				<label name="Client_ID_TypeLbl" id="Client_ID_TypeLbl"></label>
			</div>
		</div>

		<div class="col-md-3">	
			<div class="form-group">
				<label class="font-bold">Customer Identification Number:</label>
				<label name="Client_ID_NoLbl" id="Client_ID_NoLbl"></label>
			</div>
		</div>		
</div>	
<div class="row">	
	<div class="col-md-3">	
        <div class="form-group">
			<label class="font-bold">Account Holder Name:</label>
			<label name="Account_HolderLbl" id="Account_HolderLbl"></label>
		</div>
	</div>	
    <div class="col-md-3">
		<div class="form-group">
			<label class="font-bold">Bank Account Type:</label>
			<label name="Account_TypeLbl" id="Account_TypeLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Bank Account Number:</label>
			<label name="Account_NumberLbl" id="Account_NumberLbl"></label>
		</div>
	</div>			
	<div class="col-md-3">	
	<div class="form-group">
			<label class="font-bold">Bank Name</label>
			<label name="Branch_CodeLbl" id="Branch_CodeLbl"></label>
	</div>
	</div>
</div>	
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Bank Reference</label>
			<label name="Bank_ReferenceLbl" id="Bank_ReferenceLbl"></label>
		</div>
	</div>		
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Notify</label>
			<label name="NotifyLbl" id="NotifyLbl"></label>
		</div>
	</div>
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Contact Number</label>
			<label name="contact_numberLbl" id="contact_numberLbl"></label>
		</div>
	</div>	
</div>
<div class="row">	
	<div class="col-md-3">	
		<div class="form-group">
			<label class="font-bold">Number of Instalments(0 = unspecified)</label>
			<label name="No_PaymentsLbl" id="No_PaymentsLbl"></label>
		</div>
	</div>
	<div class="col-md-3">	
	    <div class="form-group">
			<label class="font-bold">Amount</label>
			<label name="AmountLbl" id="AmountLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Frequency</label>
			<label name="FrequencyLbl" id="FrequencyLbl"></label>
		</div>
	</div>	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Action Date</label>
			<label name="Action_DateLbl" id="Action_DateLbl"></label>
		</div>
	</div>
</div>	
<div class="row">

	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Tracking</label>
			<label name="entry_classLbl" id="entry_classLbl"></label>
		</div>
	</div>	
	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Service Type</label>
			<label name="Service_TypeLbl" id="Service_TypeLbl"></label>
		</div>
	</div>	
	
	<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Service Mode</label>
			<label name="Service_ModeLbl" id="Service_ModeLbl"></label>
		</div>
	</div>	
		<div class="col-md-3">			
	    <div class="form-group">
			<label class="font-bold">Company Branch</label>
			<label name="companybranchLbl" id="companybranchLbl"></label>
		</div>
	</div>	

</div>		
</div>
</div>