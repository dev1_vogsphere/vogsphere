<?php
require 'database.php';
$count = 0;
$Globalterms="";
$emailError="";
$idnumberError="";
$role="avoSMME";
$Title="";



$titleArr = array();
$titleArr[] = "";
$titleArr[] = "Mr";
$titleArr[] = "Mrs";
$titleArr[] = "Miss";
$titleArr[] = "Ms";
$titleArr[] = "Dr";
$userCode="UFAV";
$IntUserCode="UFAV";

$_GET["idnumber"]="";
$_GET["password1"]="";
$_GET["email"]="";
$_GET["FirstName"]="";
$_GET["LastName"]="";
$_GET["phone"]="";
$_GET["Title"]="";

Database::disconnect();
// --------------------- EOC 2017.04.15 ------------------------- //

// For Send E-mail
$email = "";
$status="Active";
$FirstName = "";
$LastName = "";
$phone="";
$adminemail = "info@ecashmeup.com";
$MessageErrorMail = "";

// -- Terms and Conditions. --- //
$agreeError = null;
$agree = null;
// -- Terms and conditions. -- //

// -- BOC - Immediate Payment -- //
// -- 14/01/2023 -- 
$ImmediatePay = null;
$insuranced = null;
// -- EOC - Immediate Payment -- //
// -- Company Name:
$companyname = "";
if(isset($_SESSION['GlobalCompany']))
{
$companyname = $_SESSION['GlobalCompany'];
}
else
{
$companyname = "Vogsphere (PTY) LTD";
}

$password1 = '';
$idnumber  = '';
$tbl_user="user"; // Table name
$tbl_customer ="customer"; // Table name
$tbl_loanapp ="loanapp"; // Table name
$db_eloan = "ecashpdq_eloan";

$valid =false;

/*$idnumber="3912265761086";
$password1="Mulu$262555555";
$email="mahwaymp68568@gmail.com";
$FirstName="Mulu";
$LastName="Mahwayi";
$phone="0813073233";
$Title="Mr";*/

			

if (isset($_POST['profile'])) // If refresh button is pressed just to generate Image.
{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$idnumber=$_POST["idnumber"];
			$password1=$_POST["password1"];
			$email=$_POST["email"];
			$FirstName=$_POST["FirstName"];
			$LastName=$_POST["LastName"];
			$phone=$_POST["phone"];
			$Title=$_POST["Title"];
			
			if (empty($idnumber)){$idnumberError = "please enter identity number";$valid = false;}else{$valid =true;}
			if (empty($idnumber)){$emailError = "please enter email address";$valid = false;}else{$valid =true;}
			
	
			// ---------------- User ---------------
			//User - UserID & E-mail Duplicate Validation
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));
			if ($q->rowCount() >= 1)
			{$idnumberError = 'ID number already exists.'; $valid = false;}
	
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
		
			
			if ($valid)
			{
		
			
		
				// -- Validate Password Only if not loggedin
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
			{
				//  -- BOC encrypt password - 16.09.2017.
				$password1 = Database::encryptPassword($password1);
				//  -- EOC encrypt password - 16.09.2017.

				$sql = "INSERT INTO $tbl_user(userid,password,email,role,account,status,alias) VALUES (?,?,?,?,?,?,?)";
				$q = $pdo->prepare($sql);
				$q->execute(array($idnumber,$password1,$email,$role,$userCode,$status,$idnumber));
			}

			// Customer - UserID & E-mail Duplicate Validation

			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,phone,UserCode,IntUserCode,alias,phone2)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$phone,$userCode,$IntUserCode,$idnumber,$phone));

			/*// Insert Application Loan
			$sql = "INSERT INTO $tbl_loanapp(CustomerId,loantypeid,MonthlyIncome,MonthlyExpenditure,TotalAssets,ReqLoadValue,ExecApproval,";
			$sql = $sql."DateAccpt,StaffId,InterestRate,LoanDuration,LoanValue,surety,Repayment,paymentmethod,FirstPaymentDate,lastPaymentDate,monthlypayment,paymentfrequency,accountholdername,bankname,accountnumber,branchcode,accounttype,createdon,immediate_payment,insuranced)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //26 
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$loantypeid,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,$ReqLoadValue,$ExecApproval,$DateAccpt,$StaffId,$InterestRate,$LoanDuration,$LoanValue,$Surety,$Repayment,$paymentmethod,$FirstPaymentDate,$lastPaymentDate,$monthlypayment,$paymentfrequency,$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType,$DateAccpt,$ImmediatePay,$insuranced));//26
			/*
			 Added Banking Details
			 $AccountHolder = $_POST['AccountHolder'];
			$AccountType = $_POST['AccountType'];
			$AccountNumber = $_POST['AccountNumber'];
			$BranchCode = $_POST['BranchCode'];
			$BankName = $_POST['BankName'];

			*/
			$count = $count + 1;
			Database::disconnect();
		// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = $password1;

			// Id number
			$_SESSION['idnumber']  = $idnumber;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			// phone
			$_SESSION['phone'] = $phone;
			$_SESSION['Customerphone'] = $phone;
			//-------------------------------------------------------------------
			//           				LOAN APP DATA							-
			//-------------------------------------------------------------------
			
			// -- BOC T.M Modise, 21.06.2017 - Adding Charges.
		//	$ApplicationID = GetRecentApplicationID($idnumber);

		//	$_SESSION['ApplicationId'] = $ApplicationID;

		// --------------------------- End Session Declarations ---------------------//

		// --------------------------- SendEmail to the clients to inform them about the e-loan application -----//
			//SendEmail();
		//	SendEmailApplicationForm();
		  }
		  else
		  {
			  $count = 0;
		  }
		}
		else
		{
			  $count = 0;
		}



?>