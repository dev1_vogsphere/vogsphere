<?php
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$paregroup = $_POST['groupid'];
$IntUserCode = $_POST['IntUserCode'];

$ext = '';

$html = "<SELECT class='form-control' id='item' multiple name='item[]' size='1'>";

if(empty($paregroup))
{
	// -- Select item.
	$sql = 'SELECT * FROM subgroup WHERE IntUserCode = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($IntUserCode));	
}
else
{	
	// -- Select item.
	$sql = 'SELECT * FROM subgroup WHERE parentgroup = ? AND IntUserCode = ?';
	$q = $pdo->prepare($sql);
	$q->execute(array($paregroup,$IntUserCode));
}
$dataSubGroups = $q->fetchAll();		

// -- Add the All Payment Schedule Items.
	$html = "<OPTION value='' selected>All</OPTION>";
	
// -- dataSubGroups
	foreach($dataSubGroups as $row)
	{
		// -- Branches of a selected Bank
		$ext = $row['ext'];
		$PopularName = $row['PopularName'];
													
		// -- Select the Selected Bank $PopularName $ext -
		$html = $html."<OPTION value=$ext>$ext</OPTION>";
	}
										
		if(empty($dataSubGroups))
		{
			$html = $html."<OPTION value=0>No Subgroups</OPTION>";
		}
	$html=$html."</SELECT>";
	
 echo $html;		
?>