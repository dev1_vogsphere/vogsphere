<?php
//-------------------------------------------------------------------
//						Contract
//-------------------------------------------------------------------
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
if(!function_exists('mapping_data_rentals'))
{
	function mapping_data_rentals($html,$dataComp,$dataRentalapplication )
	{
		$html = str_replace("[title]",$dataRentalapplication['title'].'.',$html);
		$html = str_replace("[firstname]",$dataRentalapplication['firstname'].' ',$html);
		$html = str_replace("[lastname]",$dataRentalapplication['lastname'].' ',$html);
		$html = str_replace("[customerId]",$dataRentalapplication['customerId'],$html);

		$html = str_replace("[street]",$dataRentalapplication['street'],$html);
		$html = str_replace("[suburb]",$dataRentalapplication['suburb'],$html);
		$html = str_replace("[province]",$dataRentalapplication['province'],$html);
		$html = str_replace("[postalcode]",$dataRentalapplication['postalcode'],$html);
		$html = str_replace("[rentalpropertyname]",$dataRentalapplication['rentalpropertyname'],$html);

		$html = str_replace("[leasedurarion]",$dataRentalapplication['leasedurarion'],$html);
		$html = str_replace("[desiredate]",$dataRentalapplication['desiredate'],$html);
		$html = str_replace("[enddate]",$dataRentalapplication['enddate'],$html);
		$html = str_replace("[rentalpropertyprice]",$dataRentalapplication['rentalpropertyprice'],$html);
		$html = str_replace("[total]",$dataRentalapplication['total'],$html);

$prevapartmentname = 'N/A';
$currapartmentname = 'N/A';

if(!empty($dataRentalapplication['previousaddress']))
{$prevapartmentname = $dataRentalapplication['previousaddress'];}

if(!empty($dataRentalapplication['currapartmentname']))
{$currapartmentname = $dataRentalapplication['currapartmentname'];}

$html = str_replace("[previousaddress]",$prevapartmentname ,$html);
$html = str_replace("[alternativeaddress]",$currapartmentname,$html);
$html = str_replace("[presentaddress]",$dataRentalapplication['Street'].",".
$dataRentalapplication['Suburb'].",".$dataRentalapplication['City'].",".
$dataRentalapplication['PostCode'].",".$dataRentalapplication['province'],$html);

$html = str_replace("[accountholdername]",$dataComp['accountholdername'],$html);
$html = str_replace("[accounttype]",$dataComp['accounttype'],$html);
$html = str_replace("[accountnumber]",$dataComp['accountnumber'],$html);
$html = str_replace("[bankname]",$dataComp['bankname'],$html);
$html = str_replace("[branchcode]",$dataComp['branchcode'],$html);

$html = str_replace("[increaserate]",$dataComp['increaserate'],$html);
$html = str_replace("[rate]",$dataComp['rate'],$html);
$html = str_replace("[typeofbusiness]",$dataComp['typeofbusiness'],$html);
$html = str_replace("[leasecost]",$dataComp['leasecost'],$html);
$html = str_replace("[penaltyfees]",$dataComp['penaltyfees'],$html);
$html = str_replace("[specialinclusions]",$dataComp['specialinclusions'],$html);

		return $html;
	}
}
if(!function_exists('get_rentalapplication'))
{
	function get_rentalapplication($dataFilter)
	{
		$data = null;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from customer, rentalapp where customer.CustomerId = ? AND rentalappid = ?";

		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['customerid'],$dataFilter['ApplicationId']));
		$data = $q->fetch(PDO::FETCH_ASSOC);

		$_SESSION['ApplicationId'] = $dataFilter['ApplicationId'];
		$_SESSION['customerid']    = $dataFilter['customerid'];
		Database::disconnect();
		return $data;
}}
if(!function_exists('get_renter'))
{
	function get_renter($dataFilter)
	{
		$data = null;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM user WHERE userid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['customerid']));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
}}
if(!function_exists('get_property'))
{
	function get_property($dataFilter)
	{
		$data = null;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM rentalproperty WHERE id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['property']));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		return $data;
}}
require 'database.php';
$customerid = null;
$ApplicationId = null;

$tbl_customer = "customer";
$tbl_loanapp = "rentalapp";
$dbname = "eloan";
$dataComp = null;
$dataRentalapplication= null;
// --------- Add New Charges --------------	///
// ---------------------------------------- ///
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		$customerid = base64_decode(urldecode($customerid));
	}

   if ( !empty($_GET['ApplicationId']))
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		$ApplicationId = base64_decode(urldecode($ApplicationId));
	}

	if ( null==$customerid )
	    {
		header("Location: index.php");
		}
		else if( null==$ApplicationId )
		{
		header("Location: index.php");
		}
	 else
	 {
	 }
//-------------------------------------------------------------------
//           				RENTAL APP DATA							-
//-------------------------------------------------------------------
// -- Get rental application.
$dataFilter['ApplicationId'] = $ApplicationId;
$dataFilter['customerid']    = $customerid;
$dataRentalapplication 		 = get_rentalapplication($dataFilter);
// -- Get Property Details
$dataProperty = get_property($dataRentalapplication);
$dataRentalapplication['street']=$dataProperty['street'];
$dataRentalapplication['suburb']=$dataProperty['suburb'];
$dataRentalapplication['province']=$dataProperty['province'];
$dataRentalapplication['postalcode']=$dataProperty['postalcode'];
$dataRentalapplication['rentalpropertyname']=$dataProperty['rentalpropertyname'];

// -- Calculated Variables
$desiredate    = $dataRentalapplication['desiredate'];
$leasedurarion = $dataRentalapplication['leasedurarion'];
$endDate = date($desiredate, strtotime(' + '.$leasedurarion.' months'));

$dataRentalapplication['enddate']			  =	$endDate;
$dataRentalapplication['rentalpropertyprice'] =	$dataProperty['rentalpropertyprice'];
$total = $dataRentalapplication['rentalpropertyprice'] * $leasedurarion;
$dataRentalapplication['total']				  =	$total;
//-------------------------------------------------------------------
//           				RENTER DATA							-
//-------------------------------------------------------------------
	$dataUser = get_renter($dataFilter);
//-------------------------------------------------------------------
//           				COMPANY DATA							-
//-------------------------------------------------------------------
$dataComp['name'] 				= 'Intelli Property Management';
$dataComp['street']				= 'Clearwater Office Park';
$dataComp['suburb'] 			= 'Strubens Valley';
$dataComp['postcodel'] 			= '1735';
$dataComp['city'] 				= 'Johannesburg';
$dataComp['phone'] 				= '010 007 5969';
$dataComp['registrationnumber'] = '2020/468800/07';
$dataComp['logo']   			= 'logo.png';
$dataComp['email']  			= 'info@intellipropertymanagement.com';
$dataComp['website']			= 'https://intellipropertymanagement.com/';

$dataComp['accountholdername']	= 'Intelli Property Management';
$dataComp['accounttype']		= 'Cheque';
$dataComp['accountnumber']		= '62857960770';
$dataComp['bankname']			= 'FNB';
$dataComp['branchcode']			= '250655';

$dataComp['increaserate']		= '250655';
$dataComp['rate']				= '8';
$dataComp['typeofbusiness']		= 'FAMILY TOWNHOUSE';

$dataComp['leasecost']			= '500.00';
$dataComp['penaltyfees']		= '500.00';

$dataComp['specialinclusions']	= 'N/A';
//-------------------------------------------------------------------
//           				EXTRACT DATA INTO LAYOUT				-
//-------------------------------------------------------------------
$html = '';
$rules = '';
$houserules = '';
$conditions = '';

$cover 		= file_get_contents('layout/rentalleaseagreement_cover.php');
$body 		= file_get_contents('layout/rentalleaseagreement_body.php');
$conditions = file_get_contents('layout/rentalleaseagreement_conditions.php');
$rules 		= file_get_contents('layout/rentalleaseagreement_rules.php');
$houserules = file_get_contents('layout/rentalleaseagreement_houserules.php');
$html 		= $cover.$body.$conditions.$rules.$houserules;
$html 		= mapping_data_rentals($html,$dataComp,$dataRentalapplication );
// ------------------------------------------------------------------
//							TCPDF SETUP			     				-
// ------------------------------------------------------------------
require_once('tcpdf_include.php');
require_once "FPDI/fpdi.php";
// Extend the FPDI class to create custom Header and Footer
class MYPDF extends FPDI
{
 // Page footer
    public function Footer()
	{
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
		$footer = file_get_contents('layout/rentalleaseagreement_footer.php');
		$x = 0;
		$y = -25;
		$this->writeHTMLCell(200, 100, $x, $y, $footer, 0, 0, 0, false, '', true);
        // Page number
       $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
// create new PDF document
$pdf = new MYPDF( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF
$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

$pdf_title = $gtab.$dataComp['name'].' - eStatement';
define ('PDF_HEADER_STRING2', $tab.$dataComp['street']."\n"
							 .$tab.$dataComp['suburb']."\n"
							 .$tab.$dataComp['postcodel']."\n"
							 .$tab.$dataComp['city']."\n"
							 .$tab.$dataComp['phone']."\n"
							 .$tab.$dataComp['email']."\n"
							 .$tab.$dataComp['website']."\n"
							 .$tab.$dataComp['registrationnumber']."\n"
							 //.$tab.$ncr
							 );
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);

// set document information $tab.$dataComp['name']."\n"
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($dataComp['name']);
$pdf->SetTitle($dataComp['name']);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject($dataComp['name']." - Rental Lease Agreement");
$pdf->SetKeywords('Contract, Agreement, Money');
// -- tcpdf_autoconfig.php where pdfPDF_HEADER are defined.
// -- set default header data
// -- Logo
define ('PDF_HEADER_LOGO2',$dataComp['logo']);
define ('PDF_HEADER_LOGO_WIDTH2',70);

$pdf_title = $gtab.$dataComp['name'];
$pdf->SetHeaderData(PDF_HEADER_LOGO2, PDF_HEADER_LOGO_WIDTH2, $pdf_title, PDF_HEADER_STRING2, array(0,64,1), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// -- BOC Watermark -- //
		$xxx = 30;
$yyy = 120;
$op = 100;
$outdir = true;
$name = "5985efa9aee18";
// -- EOC Watermark -- //
// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// -- Watermark
$watermark = "CONFIDENTIAL";
$watermarkImage = 'watermark2.png';

// -- watermark RGB
$waterHPos = 20;
$waterLPos = 230;
$waterSize = 230;

$FILEIDDOC  = 'ss';

// -- Page 1.
$pdf->RotatedImage($watermarkImage,$waterHPos,$waterLPos,$waterSize,16,45);
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->setAlpha(1,"Normal",1,false);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('dejavusans', '', 10, '', true);

//$pdf->AddPage();
//$pdf->setPrintFooter(false);
$x = 100;
$y = 100;
$angle = 45;
// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";

// -- Get the root directory
$directory = "uploads/";
$filename = $directory.$FILEIDDOC;

// Close and output PDF document
// ============ SEND DONWLOAD AND UPLOAD CONTARCT ===================== //
$fileName = 'contract.pdf';

$fileatt = $pdf->Output($fileName, 'S');
//$attachment = chunk_split($fileatt);
//SendEmail($fileatt);
// ======================================== //
// This method has several options, check the source code documentation for more information.
$pdf->Output($fileName, 'I');
//$pdf = PlaceWatermark($pdf,$fileName, "This is a lazy, but still simple test\n This should stand on a new line!", 30, 120, 100,TRUE);
$text = "confidential";
$xxx = 30;
$yyy = 120;
$op = 100;
$outdir = true;
$file = $fileName;
$pdf->setPdfVersion('1.4');

//============================================================+
// 	SEND A CONTRACT ONCE PROCESSED FILE
//============================================================+
// ------------ Start Send an Email ----------------//
if(!function_exists('SendEmail'))
{
	function SendEmail($att)
	{
			require_once('class/class.phpmailer.php');

			try
			{
						$mail=new PHPMailer();
						$mail->IsSMTP();
						$mail->SMTPDebug=1;
						$mail->SMTPAuth=true;
						$mail->SMTPSecure="tls";
						$mail->Host="smtp.vogsphere.co.za";
						$mail->Port= 587;//465;
						$mail->Username="vogspesw";
						$mail->Password="fCAnzmz9";
						$mail->SetFrom("info@vogsphere.co.za","e-Loan - Vogsphere (PTY) LTD");
						// -- The Code
						//$mail->AddAttachment($att);
						$mail->AddStringAttachment($att, 'filename.pdf');

						//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
						$mail->Subject=  "Your Password has been Recovered";
						//$mail->Body= $email_text;//"This is the HTML message body <b>in bold!</b>";
						// Enable output buffering

			ob_start();
			include 'content\text_emailForgottenPassword.php';//execute the file as php
			$body = ob_get_clean();

			$fullname = $FirstName.' '.$LastName;

			$email = 'tumelomodise4@gmail.com';//$_SESSION['email'];

						// print $body;
						$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
						$mail->AddAddress($email, $fullname);

						if($mail->Send())
						{
						$successMessage = "Your e-Loan application is successfully registered. An e-mail is sent your Inbox!";
						}
						else
						{
						  // echo 'mail not sent';
						$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";

						}
				}
				catch(phpmailerException $e)
				{
					$successMessage = $e->errorMessage();
				}
				catch(Exception $e)
				{
					$successMessage = $e->getMessage();
				}
	}
}
//============================================================+
// END OF FILE
//============================================================+
?>
