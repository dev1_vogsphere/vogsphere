<?php
//-------------------------------------------------------------------
//							eInvoice 
//------------------------------------------------------------------- 
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
	require 'database.php';
	$customerid = null;
	$ApplicationId = null;
	$repaymentAmount = 0.00;
	$tot_paid = 0.00;
	$tot_outstanding = 0.0;
	$emailComp = '';
	$email = '';
	$repaymentAmount = 0.00;
	$RemainingTerm = 0;	
	$Term = 0;	
	$dataUser = null;
	$credit = 0.00; // -- Total Amount paid
	$newcharges = 0.00; // -- Interest amount
	$installment = 0.00;
// ================================ send an e-mail =========================
	$emailFlag = '';
if ( !empty($_GET['email'])) 
	{
		$emailFlag = $_REQUEST['email'];
	}
	else
	{
	// -- Send Multiple Emails
	 if(isset($_POST['email']))
	 {
	    $emailFlag = $_POST['email'];
	 }
	}
// ================================ End Send an e-mail =====================	
	
	if ( !empty($_GET['repayment'])) 
	{
		$repaymentAmount = $_REQUEST['repayment'];
		// -- BOC Decode Encrypt 16.09.2017 - Parameter Data.
		if(isurlencode($repaymentAmount))
		{
			$repaymentAmount = base64_decode(urldecode($repaymentAmount)); 
		}
		// -- EOC Decode Encrypt 17.09.2017 - Parameter Data.
	}
	else
	{
		 if(isset($_POST['repayment']))
		  {
		  	$repaymentAmount = $_POST['repayment'];
		  }
	}
								
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
		// -- BOC Decode Encrypt 16.09.2017 - Parameter Data.
		if(isurlencode($customerid))
		{
			$customerid = base64_decode(urldecode($customerid)); 
		}
		// -- EOC Decode Encrypt 17.09.2017 - Parameter Data.
	}
	else
	{
		if(isset($_POST['customerid']))
		  {
		  	$customerid = $_POST['customerid'];
		  }
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
		// -- BOC Decode Encrypt 16.09.2017 - Parameter Data.
		if(isurlencode($ApplicationId))
		{
			$ApplicationId = base64_decode(urldecode($ApplicationId));
		}
		// -- EOC Decode Encrypt 17.09.2017 - Parameter Data.
	}
	else
	{
		if(isset($_POST['ApplicationId']))
		  {
		  	$ApplicationId = $_POST['ApplicationId'];
		  }
	}
	
	if ( null==$customerid ) 
	    {
		header("Location: custAuthenticate.php");
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: custAuthenticate.php");
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customer where id = ?";		
		$sql =  "select * from customer, loanapp where customer.CustomerId = ? AND ApplicationId = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		// -- BOC Encrypt 16.09.2017 - Parameter Data.Go Back if someone is trying their luck to hack :-).
		if(empty($data))
		{
			header("Location: custAuthenticate");
		}
		// -- EOC Encrypt 16.09.2017 - Parameter Data.
		
		// -- Read e-mail address
		//User - UserID & E-mail Duplicate Validation
		$query = "SELECT * FROM user WHERE userid = ?";
		$q = $pdo->prepare($query);
		$q->execute(array($customerid));
		$dataUser = $q->fetch(PDO::FETCH_ASSOC);
		
		Database::disconnect();
		
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;

	}
//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$dataComp = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();
	
	// -- Update Address & Company details.
	if($dataComp['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $dataComp['name'];
		$phone    = $dataComp['phone'];
		$fax      = $dataComp['fax'];
		$emailComp    = $dataComp['email'];
		$website  = $dataComp['website']; 
		$logo     = $dataComp['logo'];
		$logoTemp = $logo;
		$currency = $dataComp['currency']; 
		$legalterms = $dataComp['legaltext'];

		$street = $dataComp['street'];
		$suburb = $dataComp['suburb'];
		$city = $dataComp['city'];
		$State = $dataComp['province'];
		$PostCode = $dataComp['postcode'];	
		$registrationnumber = $dataComp['registrationnumber'];
		
		$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';
	}
	
//$currency = 'R';
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
			// Id number
			$idnumber = $data['CustomerId'];

			// Title
			$Title = $data['Title'];	
			// First name
			$FirstName = $data['FirstName'];
			// Last Name
			$LastName = $data['LastName'];
			
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			
			// Phone numbers
			$phone = $data['phone'];
			
			// Email
			$email = $dataUser['email'];
			
			// Street
			$Street = $data['Street'];

			// Suburb
			$Suburb = $data['Suburb'];

			// City
			$City = $data['City'];
			// State
			$State = $data['State'];
			
			// PostCode
			$PostCode = $data['PostCode'];
			
			// Dob
			$Dob = $data['Dob'];
			
			// phone
			$phone = $data['phone'];

//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
 $loandate = $data['DateAccpt'];// "01-08-2016";
 $loanamount = $currency.''.$data['ReqLoadValue'];// "R2000";
 $loanpaymentamount = $currency.''.number_format($data['Repayment'],2); //"R2400";
 $loanpayment = $data['Repayment'];//loan payment.
 $loanpaymentInterest = $data['InterestRate'];// -- 30.01.2017 - Added Interest Rate.
 $loanpaymentInterest = number_format($loanpaymentInterest).'%';
 $percentage = $loanpaymentInterest;		  // -- 30.01.2017 - Added Interest Rate.
 $Instalment = $currency.''.$data['monthlypayment'];	// -- 30.01.2017 - Added Instalment.
 $FirstPaymentDate = $data['FirstPaymentDate'];
 $LastPaymentDate = $data['lastPaymentDate'];
 $FILEIDDOC = $data['FILEIDDOC'];
 $paymentmethod = $data['paymentmethod'];
 $c_DebitOrder = "Debit Order";
 $statementdate = date('Y-m-d'); 
 $RemainingTerm = $data['LoanDuration'];
 $LoanDuration = $data['LoanDuration'];
// -------- BOC 18.06.2017 -------------------- //
 $interest =  $data['InterestRate'];
 /*
 $interest =  $data['LoanDuration'];
 $interest =  $data['LoanDuration'];
 */

// -------- EOC 18.06.2017 -------------------- //
 
 // ------------------------------------------------------------------
 //							CONTRACT OPTIONAL DATA					 -
 // ------------------------------------------------------------------
$annexureB = "AND ANNEXURE 'B'";
$annexureC = "AND ANNEXURE 'C'";

$option2 =  "<p><b>OPTION 2 to CLAUSE 3</b><br/>
I elect to repay the loan and the interest thereof by means of debit order. I consent to the debit order on an appropriate form attached hereto as <b>annexure ‘C’</b> to this agreement.</p>";

$clause = 
"<p><b>CLAUSE TO BE USED AS AND WHEN APPLICABLE</b>
I elect _______________________with ID number_________________as my surety to this agreement. The surety and debtor will be jointly and severally liable for the principle debt and all terms and conditions of this agreement. A surety agreement is attached to this agreement and thus incorporated as part of this agreement.</p>";
	
// -- Only show Option Clause 2 to clause 3.
if($paymentmethod != $c_DebitOrder)
{
	$option2 = "<p></p><p></p><p></p><p></p><p></p><!-- Space -->";
	$annexureC = "";
}

// Logo are update on page tcpdf_autoconfig.php (Under root directory)
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once "FPDI/fpdi.php";

$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$companyname = $Company;//"Vogsphere Pty Ltd";
$companyStreet = $street;//"43 Mulder Street";
$companySuburb = $suburb;//"The reeds";
$companyPostCode = $PostCode;//"0157";
$companyCity = $city;//"Centurion";
$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';

$address = $companyname.','.$companyStreet.','.$companySuburb.','.$companyPostCode.','.$companyCity.','.$companyphone.','.$emailComp;
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);
							 

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($companyname);
$pdf->SetTitle($companyname);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject("$companyname - eStatement");
$pdf->SetKeywords('Invoice, eStatement');
//PDF_HEADER_TITLE = 'Modise';
//\t\t\t\t\t
$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

$pdf_title = $gtab.$companyname.' - eStatement';

define ('PDF_HEADER_STRING2',$tab.$companyname."\n"
							 .$tab.$companyStreet."\n"
							 .$tab.$companySuburb."\n"
							 .$tab.$companyPostCode."\n"
							 .$tab.$companyCity."\n"
							 .$tab.$companyphone."\n"
							 .$tab.$emailComp);

define ('PDF_HEADER_LOGO_CUSTOM', $logo);
define ('PDF_HEADER_LOGO_WIDTH_CUSTOM', 90);

// set default header data PDF_HEADER_STRING // -- array(0,64,255) rgb color:
$pdf->SetHeaderData(PDF_HEADER_LOGO_CUSTOM, PDF_HEADER_LOGO_WIDTH_CUSTOM, $pdf_title, PDF_HEADER_STRING2, array(0,64,255), array(0,64,128));

$pdf->setFooterData(array(0,64,0), array(0,64,128));
		
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
 
// ----------------------------------------------------------------------------------- //
// 					BEGIN - IMS Web Service URL & Read the Data From IMS 			   //
// ----------------------------------------------------------------------------------- //				 
// set feed URL
$WebServiceURL = "http://www.vogsphere.co.za/eloan/ws_Payments.php?repayment=".$_GET['repayment']."&customerid=".$_GET['customerid']."&ApplicationId=".$_GET['ApplicationId']."&RemainingTerm=".$RemainingTerm;

// read feed into SimpleXML object
$sxml = simplexml_load_file($WebServiceURL);

// pre tags to format nicely
//echo '<pre>';
//print_r($WebServiceURL);
//echo '</pre>';

// And if you want to fetch multiple Invoice IDs:
foreach($sxml->invoice as $invoice)
{					
				$paymentid = $invoice->paymentid;			
				$tableDetails = $tableDetails.'<tr>';
								$tableDetails = $tableDetails.'<td>'.$invoice->date. '</td>';
							   	$tableDetails = $tableDetails.'<td>'.$invoice->invoice_id. '</td>';								
								$tableDetails = $tableDetails.'<td>'.$invoice->paymentreference.'</td>';
							   	$tableDetails = $tableDetails.'<td>'.$currency.$invoice->amount. '</td>';
								$tableDetails = $tableDetails.'<td>'.$currency.$invoice->amount. '</td>';
								$tableDetails = $tableDetails.'<td>'.$currency.$invoice->outstandingamount.'</td>'; // -- Balance
							    $tableDetails = $tableDetails.'</tr>';	
echo '<pre>';

								print_r($tableDetails);
echo '</pre>';
								
}
$tot_outstanding = $sxml->totaloutstanding;
$RemainingTerm = $sxml->RemainingTerm;
$repaymentAmount = $sxml->totaldebt;

if(!empty($paymentid))
{
	$tableDetails = $tableDetails.'<tr><td>'.$statementdate.'</td><td></td><td>Closing balance</td><td></td><td></td><td>'.$currency.$tot_outstanding.'</td></tr>';
}
else
{ 
	$tableDetails = $tableDetails.'<tr><td>'.$statementdate.'</td><td></td><td>Closing balance</td><td></td><td></td><td>'.$currency.$repaymentAmount.'</td></tr>';	
}
// ----------------------------------------------------------------------------------- //
// 					END - IMS Web Service URL & Read the Data From IMS 			       //
// ----------------------------------------------------------------------------------- //				 



// --------------------- DETERMINE THE PAYMENT DUE DATE ----------------------------- ///
// 																					  ///
// ---------------------------------------------------------------------------------- ///
$FirstPaymentDay = substr($FirstPaymentDate,-2); 
$PaymentDueDate = '';
$HTMLpaymentduedate = '';

// -- Get the current day, month, year.
$cyear = date('Y');
$cmonth = date('m');
$cday = date('d');

// -- Check if Outstanding amount is greater than 0. Then payment due date is required. 
if ($tot_outstanding > 0)
{
	// if (first payment day > current day)
	if($FirstPaymentDay > $cday)
	{
		$PaymentDueDate = $FirstPaymentDay.'-'.$cmonth.'-'.$cyear;
		$HTMLpaymentduedate = '<td></td><td width="30%" align="right">Payment Due Date:</td><td align="right">'.$PaymentDueDate.'</td>';
	}
	else
	{
		if($cmonth + 1 > 12)
		{
		 $cmonth = "0".'1';
		 $cyear = $cyear + 1;
		}
		else
		{
		  $cmonth = $cmonth + 1;
		  if($cmonth < 10)
		  {
		  // -- Put zero in front. 
		    $cmonth = "0".$cmonth;
		  }
		}
		
		$PaymentDueDate = $FirstPaymentDay.'-'.$cmonth.'-'.$cyear;
		$HTMLpaymentduedate = '<td></td><td width="30%" align="right">Payment Due Date:</td><td align="right">'.$PaymentDueDate.'</td>';
	}
}
else // -- Hide the payment due date.
{
  $HTMLpaymentduedate = '<td></td><td></td><td></td>';
}

// ----------------------------- END  PAYMENT DUE DATE ------------------------------ ///
// Set some content to print
$tot_outstanding = number_format($tot_outstanding,2);
// -- if the outstanding amount is greater than the instalment amount then, send the client the instalment.
// -- Else send the Outstanding amount left.
/*if($tot_outstanding >= $installment)
{
$tot_outstanding = $installment;
}
*/
// ------------------------------- BOC 01.06.2017 ------------------------------- ///
// 			     					Payment Schedule
require 'Runpaymentschedule.php';

//$href = "'".'href=eInvoice.php?repayment=0&Duration=0&ApplicationId='.$ApplicationId.'&FirstPaymentDate=0&ReqLoadValue=0&monthlypayment=0&DateAccpt=0&customername=0&customerid=0&interest=0&display='.$display."'";

$display = 'X';

$start_div = '';//'<div id="divPaymentSchedules" style="top-margin:100px;">';
$end_div  = '';//'</div>';									

//$_SESSION['terms'] 		= $dataComp['terms'];	
//						$_SESSION['ncr'] 								

$tableDetailsPaymentSchedule = $start_div.PaymentSchedule($ApplicationId,0,0,0,0,0,0, 0,0,0,$display).$end_div;
//$tableDetailsPaymentSchedule = '';

// ----- BOC -- 2017.06.04 ---------- //
$_SESSION['terms'] 		= $dataComp['terms'];	
$_SESSION['ncr'] 		= $dataComp['ncr'];

// -- Company Banking Details ------- //
$_SESSION['disclaimer'] = $dataComp['disclaimer'];
$_SESSION['accountholdername'] 		= $dataComp['accountholdername'];
$_SESSION['bankname'] 		= $dataComp['bankname'];
$_SESSION['accountnumber'] 		= $dataComp['accountnumber'];
$_SESSION['branchcode'] 		= $dataComp['branchcode'];
$_SESSION['accounttype'] 		= $dataComp['accounttype'];

// ----- EOC -- 2017.06.04 ---------- //
$tableCompTerms = '';

// ------------------------------- EOC 01.06.2017 ---------------------------------///
// -- eStatement Payment Schedule

// -------------------------- Terms and Conditions ------------------------------- //
$tableTermsAndConditions = 
'<table><tr><th bgcolor="blue" color="White">Terms and Conditions</th></tr>
<tr><td>'.ReplacePeriodWithSpace(trim_input($_SESSION['terms'])).'</td></tr></table>';

// -- Generate Account Number
$ReferenceNumber = GenerateAccountNumber($customerid);

// -------------------------- Company Banking Details ------------------------------- //
$tableCompanyBankingDetails  = 
'<table><tr><th bgcolor="blue" color="White" colspan="2" align="right">Company Banking Details</th></tr>
<tr><td align="right">Beneficiary Name:</td><td align="right">'.trim_input($_SESSION['accountholdername']).'</td></tr>
<tr><td align="right">Bank Name:</td><td align="right">'.trim_input($_SESSION['bankname']).'</td></tr>
<tr><td align="right">Account Number:</td><td align="right">'.trim_input($_SESSION['accountnumber']).'</td></tr>
<tr><td align="right">Branch Code:</td><td align="right">'.trim_input($_SESSION['branchcode']).'</td></tr>
<tr><td align="right">Reference Number:</td><td align="right">'.$ReferenceNumber.'</td></tr>
</table>';

// -------------------------- Company NRC/FSP Registration ------------------------------- //
$tableCompanyNCRRegistration = 
'<table><tr><th bgcolor="blue" color="White">Company Compliance Disclaimer</th></tr>
<tr><td>'.ReplacePeriodWithSpace(trim_input($_SESSION['disclaimer'])).'</td>

</tr></table>';

// --------------- Company Details -------------------------------------------------
$tableCompanyDetails = '';

// -- Loan Insurance or Protection Plan.
$ProtectionPlan = $currency.''.number_format(GetProtectionPlan(),2);

// ---------------------------------------------------------------------------------
$credit = $currency.number_format($credit,2);
$newcharges = number_format($newcharges,2);
$html = <<<EOD
<style>
</style>
<table>
   <tr><th bgcolor="blue" color="White" width="40%">Client</th><th bgcolor="blue" color="White" width="60%" align="right">Statement Summary</th></tr>
   <tr><td>$clientname</td><td width="30%" align="right">Account Number:</td><td width="30%" align="right">$ReferenceNumber</td></tr>
   <tr><td>$Street</td><td width="30%" align="right">Loan application number:</td><td width="30%" align="right">$ApplicationId</td></tr>
   <tr><td>$Suburb</td><td width="30%" align="right">Statement Date:</td><td align="right">$statementdate</td></tr>
   <tr><td>$City</td><td width="30%" align="right">Loan Agreement Amount:</td><td align="right">$loanpaymentamount</td></tr>
   <tr><td>$State</td><td width="30%" align="right">Interest:</td><td width="30%" align="right">$loanpaymentInterest</td></tr>
   <tr><td>$PostCode</td><td width="30%" align="right">Remaining months:</td><td align="right">$RemainingTerm</td></tr>
   
   <tr><td>Cell:$phone</td><td width="30%" align="right">Total Monthly Instalment:</td><td align="right">$Instalment</td></tr>
   <tr><td>Email:$email</td><td width="30%" align="right">Credits:</td><td align="right">$credit</td></tr>
<!--   <tr><td></td><td width="30%" align="right">New Charges:</td><td align="right">$currency$newcharges</td></tr>
   <tr><td></td><td width="30%" align="right">Total Amount Due:</td><td align="right">$currency$tot_outstanding</td></tr>  -->
   <tr><td></td><td width="30%" align="right">Loan Protection Plan:</td><td width="30%" align="right">$ProtectionPlan</td></tr>

   <tr>$HTMLpaymentduedate</tr>
    <!--<tr><td></td><td></td><td>Amount Overdue:  $currency$tot_outstanding</td></tr> -->
   <tr><td></td><td></td><td></td></tr> 
</table>
<!--<table>
   <tr bgcolor="blue" color="White"><th>Date</th><th>Invoice number</th><th>Details</th><th>Charges</th><th>Credits</th><th>Balance</th></tr>
    <tr><td>$FirstPaymentDate</td><td></td><td>Opening balance</td><td></td><td></td><td>$loanpaymentamount</td></tr>
    $tableDetails

</table> -->

<style>
	th{ background-color:blue;color:White;}
</style>
	$tableDetailsPaymentSchedule
	$tableCompanyBankingDetails
	$tableCompanyNCRRegistration
	$tableTermsAndConditions 
EOD;

// =========================================== Store Values for e-mail eStatement =======
						$space = '';
						$_SESSION['invTitle'] 		  = $Title;
						$_SESSION['invFirstName']     = $FirstName;
						$_SESSION['invLastName']      = $LastName;
						$_SESSION['invidnumber']      = $idnumber;
						$_SESSION['invAccountNumber'] = $ApplicationId;
						$_SESSION['invInvoiceNumber'] = $ApplicationId;
						$_SESSION['invDueDate'] 	  = $PaymentDueDate;
						$_SESSION['invAmountDue'] 	  = $tot_outstanding;
						$_SESSION['invCurrency'] 	  = $currency;
						$_SESSION['invEmail'] 		  = $email;
						
						$_SESSION['GlobalCompany']  = $dataComp['name'];
						$_SESSION['Globalphone']    = $dataComp['phone'];
						$_SESSION['Globalfax']      = $dataComp['fax'];
						$_SESSION['Globalemail']    = $dataComp['email'];
						$_SESSION['Globalwebsite']  = $dataComp['website']; 
						$_SESSION['Globallogo']     = $dataComp['logo'];
						$_SESSION['GloballogoTemp'] = $dataComp['logo'];
						$_SESSION['Globalcurrency'] = $dataComp['currency']; 
						$_SESSION['Globallegalterms'] = $dataComp['legaltext'];
						$_SESSION['Globalstreet'] = $dataComp['street'];
						$_SESSION['Globalsuburb'] = $dataComp['suburb'];
						$_SESSION['Globalcity'] = $dataComp['city'];
						$_SESSION['Globalstate'] = $dataComp['province'];
						$_SESSION['Globalpostcode'] = $dataComp['postcode'];	
						$_SESSION['Globalregistrationnumber'] = $dataComp['registrationnumber'];		

						// -- Email Server Hosting
						$_SESSION['Globalmailhost']  = $dataComp['mailhost'];
						$_SESSION['Globalport'] = $dataComp['port'];
						$_SESSION['Globalmailusername'] = $dataComp['username'];
						$_SESSION['Globalmailpassword'] = $dataComp['password'];
						$_SESSION['GlobalSMTPSecure'] = $dataComp['SMTPSecure'];
						$_SESSION['Globaldev'] =  $dataComp['dev'];
						$_SESSION['Globalprod'] = $dataComp['prod'];		
					// -- EOC Master Session Values --- //	
// =========================================== ECO Store Values ==========================			

// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //

// Set some content to print
// $tot_outstanding = number_format($tot_outstanding,2);
// $credit = number_format($credit,2);
// $newcharges = number_format($newcharges,2);

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";
$eStatement = 'eStatement_'.$ApplicationId;
// Close and output PDF document
// This method has several options, check the source code documentation for more information.

$fileName = $eStatement.'.pdf';
$_SESSION['invFileName'] = $fileName;

if(empty($emailFlag))
{
$pdf->Output($fileName, 'I');
}
else // Send an e-mail
{
 $fileatt = $pdf->Output($fileName, 'S');
 SendEmail($fileatt);

 include 'success.php';//execute the file as php
}
//============================================================+
// 	SEND AN eInvoice to client(s) - 
//============================================================+
// ------------ Start Send an Email ----------------//
function SendEmail($att)
{
		require_once('class/class.phpmailer.php');
		try
		{
					// -- Email Server Hosting -- //
						$Globalmailhost = $_SESSION['Globalmailhost'];
						$Globalport = $_SESSION['Globalport'];
						$Globalmailusername = $_SESSION['Globalmailusername'];
						$Globalmailpassword = $_SESSION['Globalmailpassword'];
						$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
						$Globaldev = $_SESSION['Globaldev'];
						$Globalprod = $_SESSION['Globalprod'];	
						$adminemail = $_SESSION['Globalemail'];
						$companyname = $_SESSION['GlobalCompany'];
						// -- Email Server Hosting -- //
					 
					/*	Production - Live System */
						if($Globalprod == 'p')
						{
							$mail=new PHPMailer();
							$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
							$mail->SetFrom($adminemail,$companyname);
							$mail->AddReplyTo($adminemail,$companyname);
						}
					/*	Development - Testing System */	
						else
						{
								$mail=new PHPMailer();
								$mail->IsSMTP();
								$mail->SMTPDebug=1;
								$mail->SMTPAuth=true;
								$mail->SMTPSecure= $GlobalSMTPSecure;//"tls";//
								$mail->Host=$Globalmailhost;//"smtp.vogsphere.co.za";//
								$mail->Port= $Globalport;//587;//
								$mail->Username=$Globalmailusername;//"vogspesw";//
								$mail->Password=$Globalmailpassword;//"fCAnzmz9";//
						}	
								$dateForm = date('F')."  ".date('Y');
								
								$mail->SetFrom($adminemail,$companyname);

								$mail->AddStringAttachment($att, 'statement.pdf');
								
								//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
								$mail->Subject=  $companyname." statement of account :".$dateForm;//$_SESSION['invAccountNumber'];
								// Enable output buffering
								$_SESSION['invDateForm'] = $dateForm;			
											
								ob_start();
								include 'content/text_emailInvoice.php';//execute the file as php
								$body = ob_get_clean();		
								$fullname = $FirstName.' '.$LastName;
								$email = $_SESSION['invEmail'];

								// print $body;
								$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
								$mail->AddAddress($email, $fullname);
								$mail->AddBCC($adminemail, $companyname);


								if($mail->Send()) 
								{
						$successMessage = "Your e-Loan application is successfully registered. An e-mail is sent your Inbox!";
						// -- Somehow the code below stops the buffer and returns called jQuery  code page, on the eStatement.php.
						// -- Mystery code. This code is mysterous do not understand at this point.25.06.2017
						echo "<script>alert('".$successMessage."')</script>";

								}
								else 
								{
								  // echo 'mail not sent';
								$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";
								
								}
			}		
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}	
}
// -- To avoid : Redeclaring trim.
//if (!function_exists('trim_input'))   
//{
	// -- Trim Input
	function trim_input($data) 
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  
	  return $data;
	}
//}
// -- Replace the Period "." with new line.<br /> 
function ReplacePeriodWithSpace($content)
{
  $content = str_replace(".",".<br/>",$content);
  return $content;
}

// -- BOC T.M Modise, 18.06.2017 
// -- Get the loan protection plan ---- //
// -- Which is the insurance amount fee multiply with the loan duration. -- //
function GetProtectionPlan()
{ 
$protectionplan = 0.00;
$fee = 0.00;

// -- Database Connections.
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

global $LoanDuration;
global $customerid;
global $ApplicationId;

$charge = 4;
  
$sql =  "select * from loanappcharges where ApplicationId = ? AND chargeid = ?";
$q = $pdo->prepare($sql);
$q->execute(array($ApplicationId,$charge));
$dataChargeAdded = $q->fetchAll(PDO::FETCH_ASSOC);  

// -- if the Insurance Charges has been added.
if(!empty($dataChargeAdded))
{
// -- Get the insurance charge amount.
	$sql =  "select * from charge where chargeid = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($charge));
	$dataCharge = $q->fetch	(PDO::FETCH_ASSOC);  

	$fee = $dataCharge['amount']; 
// -- Amount * Duration = Protection Plan.
	if($fee > 0)
	{
	  if($LoanDuration > 0)
	  {
	    $protectionplan = $fee * $LoanDuration;
	  }
	}
}
 
return $protectionplan;
} 

// -- Account/Reference must be the 9 digits derived from the ID number.
// -- Reading from the ID backward.  
function GenerateAccountNumber($customerid)
{
   $AccountNumber = '';
   
   if(!empty($customerid))
   {
     $AccountNumber = strrev($customerid);
	 
	 // -- 9 digits.
	 $AccountNumber = substr($AccountNumber,0,9); 
   }
   return $AccountNumber;
}
// -- EOC T.M Modise, 18.06.2017 

// -- BOC Check if parameters are Encoded & Decoded. 17.09.2017.
function isurlencode($param)
{
	//if ( urlencode(base64_encode(base64_decode(urldecode($param)))) === $param)
	if (preg_match("@^[a-zA-Z0-9%+-_]*$@", $param)) //{ // variable is urlencoded. }
	{
		return true;
	} 
	else 
	{
		return false;
	}
}
// -- EOC Check if parameters are Encoded & Decoded. 17.09.2017.

//============================================================+
// END OF FILE
//============================================================+
?>