<?php
//-------------------------------------------------------------------
//						Contract 
//------------------------------------------------------------------- 
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
 require 'database.php';
	$customerid = null;
	$ApplicationId = null;
	
	if ( !empty($_GET['customerid'])) 
	{
		$customerid = $_REQUEST['customerid'];
	}
	
   if ( !empty($_GET['ApplicationId'])) 
	{
		$ApplicationId = $_REQUEST['ApplicationId'];
	}
	
	if ( null==$customerid ) 
	    {
		header("Location: custAuthenticate.php");
		}
		else if( null==$ApplicationId ) 
		{
		header("Location: custAuthenticate.php");
		}
	 else {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$sql = "SELECT * FROM customers where id = ?";		
		$sql =  "select * from Customer, LoanApp where Customer.CustomerId = ? AND ApplicationId = ?";
          
		$q = $pdo->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
		
		$_SESSION['ApplicationId'] = $ApplicationId;
		$_SESSION['customerid'] = $customerid;

	}
//------------------------------------------------------------------- 
//           				COMPANY DATA							-
//-------------------------------------------------------------------
// ----- 10.02.2017	
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
// -- Select Address & Company Details.
	$sql = "select * from globalsettings";
	$q = $pdo->prepare($sql);
	//$data = $pdo->query($sql);
	$q->execute();
	$dataComp = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();
	
	// -- Update Address & Company details.
	if($dataComp['globalsettingsid'] >= 1)
	{
		// -- Company details from Form.
		$Company  = $dataComp['name'];
		$phone    = $dataComp['phone'];
		$fax      = $dataComp['fax'];
		$email    = $dataComp['email'];
		$website  = $dataComp['website']; 
		$logo     = $dataComp['logo'];
		$logoTemp = $logo;
		$currency = $dataComp['currency']; 
		$legalterms = $dataComp['legaltext'];

		$street = $dataComp['street'];
		$suburb = $dataComp['suburb'];
		$city = $dataComp['city'];
		$State = $dataComp['province'];
		$PostCode = $dataComp['postcode'];	
		$registrationnumber = $dataComp['registrationnumber'];
		
		$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';
	} // ----- 10.02.2017
	
//------------------------------------------------------------------- 
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------
			// Id number
			$idnumber = $data['CustomerId'];

			// Title
			$Title = $data['Title'];	
			// First name
			$FirstName = $data['FirstName'];
			// Last Name
			$LastName = $data['LastName'];
			
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;
			
			// Phone numbers
			$phone = $data['phone'];
			// Email
			$email = "tumelomodise4@yahoo.com";//$data['email'];
			// Street
			$Street = $data['Street'];

			// Suburb
			$Suburb = $data['Suburb'];

			// City
			$City = $data['City'];
			// State
			$State = $data['State'];
			
			// PostCode
			$PostCode = $data['PostCode'];
			
			// Dob
			$Dob = $data['Dob'];
			
			// phone
			$phone = $data['phone'];

//------------------------------------------------------------------- 
//           				LOAN APP DATA							-
//-------------------------------------------------------------------			
 $loandate = $data['DateAccpt'];// "01-08-2016";
 $loanamount = $currency.''.$data['ReqLoadValue'];// "R2000";
 $loanpaymentamount = $currency.''.$data['Repayment']; //"R2400";
 $loanpaymentInterest = $data['InterestRate'];// -- 30.01.2017 - Added Interest Rate.
 $loanpaymentInterest = number_format($loanpaymentInterest).'%';
 $percentage = $loanpaymentInterest;		  // -- 30.01.2017 - Added Interest Rate.
 $Instalment = $currency.''.$data['monthlypayment'];	// -- 30.01.2017 - Added Instalment.
 $FirstPaymentDate = $data['FirstPaymentDate'];
 $LastPaymentDate = $data['lastPaymentDate'];
 $FILEIDDOC = $data['FILEIDDOC'];
 $paymentmethod = $data['paymentmethod'];
 $c_DebitOrder = "Debit Order";
  
 // ------------------------------------------------------------------
 //							CONTRACT OPTIONAL DATA					 -
 // ------------------------------------------------------------------
$annexureB = "AND ANNEXURE 'B'";
$annexureC = "AND ANNEXURE 'C'";

$option2 =  "<p><b>OPTION 2 to CLAUSE 3</b><br/>
I elect to repay the loan and the interest thereof by means of debit order. I consent to the debit order on an appropriate form attached hereto as <b>annexure ‘C’</b> to this agreement.</p>";

$clause = 
"<p><b>CLAUSE TO BE USED AS AND WHEN APPLICABLE</b>
I elect _______________________with ID number_________________as my surety to this agreement. The surety and debtor will be jointly and severally liable for the principle debt and all terms and conditions of this agreement. A surety agreement is attached to this agreement and thus incorporated as part of this agreement.</p>";
	
// -- Only show Option Clause 2 to clause 3.
if($paymentmethod != $c_DebitOrder)
{
	$option2 = "<p></p><p></p><p></p><p></p><p></p><!-- Space -->";
	$annexureC = "";
}

// Logo are update on page tcpdf_autoconfig.php (Under root directory)
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

require_once "FPDI/fpdi.php";
$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LETTER' ); //FPDI extends TCPDF

// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$companyname = $Company;//"Vogsphere Pty Ltd";
		$companyStreet = $street;//"43 Mulder Street";
		$companySuburb = $suburb;//"The reeds";
		$companyPostCode = $PostCode;//"0157";
		$companyCity = $city;//"Centurion";
		$companyphone = $phone.'/'.$fax;//'0793401754/0731238839';


// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($companyname);
$pdf->SetTitle($companyname);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject("$companyname - Loan Agreement");
$pdf->SetKeywords('Loan, Contract, Agreement, Money');
// -- tcpdf_autoconfig.php where pdfPDF_HEADER are defined.
// -- set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

/* <h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
 */
 
// Set some content to print
$html = <<<EOD

<p align="center"><h5><b>LOAN AGREEMENT AND ACKNOWLEDGEMENT OF DEBT</b></h5>
    I, the undersigned,<br/>
	<b>$clientname</b><br/>
	<b>(Identity Number : $idnumber)</b><br/>
	 (Hereinafter referred to as "the debtor")
</p>

<p>This Agreement has been drafted and entered into on the <b>$loandate</b>
Between <b>$companyname</b> (the creditor) and <b>$clientname</b> herein referred to as the debtor. A certified copy of the debtors ID copy is attached hereto as <b>annexure ‘A’</b></p>

<p>The debtor is a <b>$companyname</b> customer and has requested a loan for personal reasons. The loan is granted on the below terms and conditions:</p>

<p>This Client Loan Agreement has been drafted on the <b>$loandate</b> by and between <b>$companyname</b>, the Loaner and <b>$clientname</b> the Client.
The Client is a <b>$companyname</b> customer and has requested for a loan for personal reasons which will be granted if he/she agrees to the below mentioned rules and regulations:</p>
<ol>
<li>I Admit liability to <b>$companyname</b> (hereinafter referred to as "the Creditor") for the payment of capital in the amount of <b>$loanamount</b></li>

<li>I agree to pay the aforementioned capital and the interest at <b>$percentage</b> to the Creditor within 6 months from date of issue. I agree to repay the principle debt together with the interest calculated in accordance with the quotation and dates stipulated in the quotation. The quotation is attached hereto as annexure ‘B’.
</li>

<li>I will repay the loan and the interest thereof calculated at <b>$percentage</b> in accordance with the repayment schedule and dates stated in the quotation. Repayments will be made by means of cash deposit on the date stipulated in the payment schedule and each subsequent instalment to be paid on or before the date stipulated in the schedule until the capital, has been paid in full.(Payments to be explored)
$option2
$clause
</li>

<li>In the event of my failing to comply with my undertaking in terms of Section 57 of Act 32 of 1944 (as amended), I hereby consent to judgment being entered against me in terms of Section 58 of Act 32 of 1944 (as amended) without further notice to myself as follows:-
</li>

<li>Judgment for the full outstanding balance of the capital and interest in terms of this admission, as stipulated in a certificate signed by Creditor and/or his attorney together with all legal costs as well as the costs for the Application for Judgment, on the Attorney and Client scale; and
</li>
<li>The Creditor shall be entitled to issue a warrant of execution against my movable and/or immovable assets, for recovery of the full amount for which judgment was granted.</li>

<li>The Creditor shall be entitled to obtain an Emoluments attachment order against my earnings. I confirm that in the event of an emoluments attachment order being granted against my earnings. I will have sufficient funds to maintain myself and my dependants.</li>


<li>I Consent in terms of Section 45 of Act 32 of 1944 (as amended) to the jurisdiction of such Magistrate's Court that has jurisdiction over me in terms of Section 28 of the aforementioned Act, notwithstanding the fact that the amount claimed from me 

would otherwise have exceeded the jurisdiction of such Court. The Creditor nevertheless retains the right to elect to institute proceedings in the Supreme Court.</li>

<li>I Consent in terms of Section 65J(1)(d) of Act 32 of 1944 (as amended) to the granting and issue of an emoluments attachment order immediately upon judgment being entered against me in terms of Section 58 of Act 32 of 1944 (as amended)</li>

<li>I acknowledge and accept that notwithstanding anything contained herein, the Creditor may at any time hand this matter over to their attorneys of record for collection in which event I undertake to pay all costs on the scale as between attorney and own client in respect of any legal action instituted by such attorneys for recovery of the balance of the monies owing by myself together with collection commission and any other related costs
</li>

<li>I Undertake to notify the Creditor and its Attorneys in writing within 14 days after I have changed my residence or work or business address, of such new address and I furthermore accept liability for the payment of tracing costs should I fail to abide by the terms of this clause and cause the Creditor to incur costs in tracing me.</li>

<li>Notwithstanding the a foregoing, I agree that the full amount outstanding shall immediately become due, owing and payable in any of the following events:-
<br/><br/>12.1 Should I surrender or assign my Estate; or
<br/>12.2 In the event of me being placed under provisional or final sequestrated or declared insolvent or
<br/>12.3 An order be granted for the administration of my Estate, or placed under curatorship; or
<br/>12.4 In the event of my death;
<br/>12.5 Should I depart temporarily or permanently from the Republic of South Africa;
<br/>12.6. In the event of any payment not being made on due date.
</li>

<li>I choose as my domicilium citandi et executandi the following address:
<br/>________________________________________________________________
<br/>________________________________________________________________
<br/>________________________________________________________________
<br/>________________________________________________________________
</li>

<li>I acknowledge that this document constitutes the whole agreement between myself and Creditor and that no amendment, addition, deletion or cancellation of this agreement or any of its provisions shall be of any force or effect unless reduced to writing and signed by or on behalf of the parties.
</li>
<li>No indulgences granted by the Creditor shall be construed as a waiver of any of the Creditor's rights under this agreement or at law. The terms and conditions of this agreement are in accordance with section 92 of the NCA 34 of 2005.
</li>

</ol>

<p>DATED AT ___________________ ON THE _______ DAY OF ______________ 20___.</p>


<p>As witnesses:-
<ol>
<li>______________________________                    ____________________________
                                                                                                                             <b>DEBTOR</b><li>

<li>______________________________</li>
</ol>
<p><h2>ANNEXURE 'A' $annexureB $annexureC</h2></p>
<br>
<table>
   <tr><th colspan="2" bgcolor="Black" color="White">TOTAL COST AND INTEREST</th></tr>
   <tr><td>Amount Loaned</td><td>$loanamount</td></tr>
   <tr><td>Percentage of loan amount (monthly)</td><td>$loanpaymentInterest</td></tr>
   <tr><td>Total amount repayment</td><td>$loanpaymentamount</td></tr>
   <tr><th colspan="2" bgcolor="Black" color="White">REPAYMENT ARRANGEMENTS</th><th></th></tr>
   <tr><td>Frequency of payment</td><td>Monthly</td></tr>
   <tr><td>Method of payment</td><td>EFT</td></tr>
   <tr><td>Instalment</td><td>$Instalment</td></tr>
   <tr><td>First payment date</td><td>$FirstPaymentDate</td></tr>
   <tr><td>Last payment date</td><td>$LastPaymentDate</td></tr>
</table>
<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p> <!-- Space -->

EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";

// -- Get the root directory
$directory = "uploads/";
$filename = $directory.$FILEIDDOC;

// -- Check if the file exist.
// -- $pages = $pdf->setSourceFile( 'contractExample.pdf' );

//$filename = "uploads/23-10-2016-12-15-50DOC001.PDF";
try
{
// -- Add the new page the last 
// -- Display an ID document.
if ( file_exists($directory.$FILEIDDOC) )
{

// -- Check if file name is not empty,
if(!empty($FILEIDDOC))
{
$pdf->AddPage();

// -- Get the last page.  
  $pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($filename);			
		
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		//$pdf->AddPage();
		// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
		
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 0);
	}
	
	}// -- End File name check.
}
else
{
// File does not exist.
  // $html = $html  + '<h2>File does not exist</h2>'; 
}
}
catch(Exception $e) 
{}

$pdf->AddPage();
// ----------------------- annexure ‘C’ ------------------------------
//																	 -
// -------------------------------------------------------------------
$debitorderform = "uploads/debitorderform.pdf";
$sw_bookmark = 0;
$pages = "";
// -- Show Debit Order Only if Debit Order is choosen.
if($paymentmethod == $c_DebitOrder)
{
if ( file_exists($debitorderform) )
{
// -- Get the last page.  
  $pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($debitorderform);
  
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		//$pdf->AddPage();
		// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
				
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 18);
	}
}
else
{
// File does not exist.
  // $html = $html  + '<h2>File does not exist</h2>'; 
}
$pdf->AddPage();

// ----------------------- annexure ‘C2’ ------------------------------
//																	 -
// -------------------------------------------------------------------
$debitorderform = "uploads/debitorderform2.pdf";
$sw_bookmark = 0;
$pages = "";

if ( file_exists($debitorderform) )
{
// -- Get the last page.  
  $pdf->lastPage();

// -- get the file content
$pages = $pdf->setSourceFile($debitorderform);
  
// -- Read all the pages and add them.
	for ($i = 1; $i <= $pages; $i++)
	{
		//$pdf->AddPage();
		// Add to the next position. 
		if ( $sw_bookmark == 0 )
		{
			$pdf->Bookmark('to add bookmark if', 1, 0, '', 'B', array(0,64,128));
			$sw_bookmark = 1;
		}
		$page = $pdf->importPage($i);
		$pdf->useTemplate($page, 0, 18);
	}
}
else
{
// File does not exist.
  // $html = $html  + '<h2>File does not exist</h2>'; 
}
} // Debit Order Form

// -- $page = $pdf->ImportPage( 1 );
//-- $pdf->useTemplate( $page, 0, 0 );

//header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="file.pdf"');

// Close and output PDF document
// ============ SEND DONWLOAD AND UPLOAD CONTARCT ===================== //
$fileName = 'example_001.pdf';
$fileatt = $pdf->Output($fileName, 'S');
//$attachment = chunk_split($fileatt);
SendEmail($fileatt);
// ======================================== //
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I'); 

//============================================================+
// 	SEND A CONTRACT ONCE PROCESSED FILE
//============================================================+
// ------------ Start Send an Email ----------------//
function SendEmail($att)
{
		require_once('class/class.phpmailer.php');

		try
		{
					$mail=new PHPMailer();
					$mail->IsSMTP();
					$mail->SMTPDebug=1;
					$mail->SMTPAuth=true;
					$mail->SMTPSecure="tls";
					$mail->Host="smtp.vogsphere.co.za";
					$mail->Port= 587;//465;
					$mail->Username="vogspesw";
					$mail->Password="fCAnzmz9";
					$mail->SetFrom("info@vogsphere.co.za","e-Loan - Vogsphere (PTY) LTD");
					// -- The Code
					//$mail->AddAttachment($att);
					$mail->AddStringAttachment($att, 'filename.pdf');
					
					//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
					$mail->Subject=  "Your Password has been Recovered";
					//$mail->Body= $email_text;//"This is the HTML message body <b>in bold!</b>";
					// Enable output buffering
					
		ob_start();
		include 'content\text_emailForgottenPassword.php';//execute the file as php
		$body = ob_get_clean();			
					
		$fullname = $FirstName.' '.$LastName;

		$email = 'tumelomodise4@gmail.com';//$_SESSION['email'];
		
					// print $body;
					$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
					$mail->AddAddress($email, $fullname);

					if($mail->Send()) 
					{
					$successMessage = "Your e-Loan application is successfully registered. An e-mail is sent your Inbox!";
					}
					else 
					{
					  // echo 'mail not sent';
					$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";
					
					}
			}		
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}	
}
//============================================================+
// END OF FILE
//============================================================+
?>
