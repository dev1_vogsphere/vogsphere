/*Query to get all admin charge from 01.03.2018 to 28.02.2019 */
SELECT chargename,lc.chargeid,lc.applicationid,lc.item,amount,scheduleddate,chargedate 
FROM `loanappcharges` as lc INNER join charge as c on lc.chargeid = c.chargeid 
INNER join paymentschedule as p on lc.applicationid = p.ApplicationId and lc.item = p.item 
WHERE scheduleddate BETWEEN '2018-03-01' and '2019-02-28' ORDER BY `lc`.`applicationid` ASC

/* Query to get all admin charge from 01.03.2018 to 28.02.2019 - No Items latest with chargedate populated */
SELECT chargename,lc.chargeid,lc.applicationid,lc.item,amount,chargedate 
FROM `loanappcharges` as lc INNER join charge as c on lc.chargeid = c.chargeid 
WHERE chargedate BETWEEN '2018-03-01' and '2019-02-28' 
AND chargedate <> '0000-00-00'
ORDER BY `lc`.`applicationid` ASC


Financial analysis
/* Query to get all invoice from 01.03.2018 to 28.02.2019 - IMS */
SELECT c.id,c.customer_name,c.issue_date,c.due_date,c.status,i.quantity,i.unitary_cost,(i.quantity*i.unitary_cost) as paidamount FROM `common` as c inner join item as i on c.id = i.common_id 
WHERE type = 'Invoice' and issue_date BETWEEN '2018-03-01' and '2019-02-28'; 

/* Query to get all sales*/
SELECT c.id as Invoice_Number,c.customer_name As Customer_name,c.issue_date as Issue_Date,c.due_date As Due_Date,i.description As Product,c.status,i.quantity,i.unitary_cost,(i.quantity*i.unitary_cost) as Amount_Line_Item, (select organisation from customer where id=c.customer_id) as org, c.customer_id FROM `common` as c inner join item as i on c.id = i.common_id
WHERE type = 'Invoice' and issue_date BETWEEN '2019-03-31' and '2019-11-31' having org = 'Vogsphere Pty Ltd'; 

/* Query to get all paid invoices*/
SELECT c.id as Invoice_Number,c.customer_name As Customer_name,c.issue_date as Issue_Date,c.due_date As Due_Date,c.status, i.amount as Paid_Amount, (select organisation from customer where id=c.customer_id) as org, c.customer_id FROM `common` as c inner join payment as i on c.id = i.invoice_id
WHERE type = 'Invoice' and issue_date BETWEEN '2019-03-31' and '2019-11-31' having org = 'eCashMeUp Pty Ltd'; 

/* Query to get all unpaid invoices*/
SELECT  c.base_amount,(select distinct amount from payment where invoice_id = c.id) as unpaidamnt,c.id as Invoice_Number,c.customer_name As Customer_name,c.issue_date as Issue_Date,c.due_date As Due_Date,c.status, (select organisation from customer where id=c.customer_id) as org, c.customer_id FROM `common` as c  
WHERE  c.id not in (select distinct invoice_id from payment where invoice_id is not null) and type = 'Invoice' and issue_date BETWEEN '2019-03-31' and '2019-11-31' having org = 'Vogsphere Pty Ltd'; 
