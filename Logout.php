<?php  
date_default_timezone_set('Africa/Johannesburg');
$userid = '';
require 'database.php';
session_start();//session is a way to store information (in variables) to be used across multiple pages. 
if(isset($_SESSION['username']))
{
	$userid = $_SESSION['username'];
}
if(empty($userid))
{
	if(isset($_POST['username']))
	{
		$userid = $_POST['username'];
	}
}
session_unset(); 
session_destroy();  
$url = "customerLogin";
if(isset($_GET["session_expired"])) {
	$url .= "?session_expired=" . $_GET["session_expired"];
}
// -- Audit Log. 
$arrayLog   	  = array();
$arrayLog[] 	  = 'Active';
$arrayLog[] 	  =  $userid;	
$dataAuditLogUser = GetAuditActiveUser($arrayLog);
// -- Update the Audit Log.
$arrayLogOut      = array();
if(isset($dataAuditLogUser['audittrailid']))
{
	$logoutdatetime = date("Y-m-d H:i:s");
	$audittrailid   = $dataAuditLogUser['audittrailid'];
	$arrayLogOut[] = 'Inactive';
	$arrayLogOut[] = $logoutdatetime;
	$arrayLogOut[] = $audittrailid;
	UpdateAuditLog($arrayLogOut);
}
header("Location:$url");
?>  