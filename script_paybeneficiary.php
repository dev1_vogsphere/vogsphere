<?php
date_default_timezone_set('Africa/Johannesburg');
require 'database.php';
 $paysingle = ''; 
if(!function_exists('sql_associative_array'))
{
	function sql_associative_array($data)
	{
		$sqlArray = array();
		$fields=array_keys($data); 
		$values=array_values($data);
		$fieldlist=implode(',',$fields); 
		$qs=str_repeat("?,",count($fields)-1);
		
		$sqlArray[0] = $fieldlist;
		$sqlArray[1] = $qs;
		$sqlArray[2] = $values;
	
		return $sqlArray;
	}
}
if(!function_exists('get_paybenefiary'))
{
	function get_paybenefiary($dataFilter)
	{
		// -- check with benefiaryid,Client_Reference_1, Account_Number,Action_Date,IntUserCode.
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'payments';			
		$sql = "select * from $tbl where 
		benefiaryid  = ? and Action_Date  = ? and Client_Reference_1 = ? and IntUserCode = ? and 
		Account_Number = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['benefiaryid'],$dataFilter['Action_Date'],
						  $dataFilter['Client_Reference_1'],$dataFilter['IntUserCode'],
						  $dataFilter['Account_Number']));
		$data = $q->fetch(PDO::FETCH_ASSOC);			  
		return $data;		
	}
}

if(!function_exists('update_benefiary'))
{
	function update_benefiary($dataFilter)
	{
		// -- Update Last Transaction 
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl  = 'benefiary';			
		$sql = "UPDATE $tbl SET lasttransaction = ? WHERE benefiaryid = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['lasttransaction'],$dataFilter['benefiaryid']));
		return $data;		
	}
}

if(!function_exists('pay_to_beneficiary'))
{
	function pay_to_beneficiary($data)
	{
		$message = '';
		$id  = 0;
		$ret = '';
		
		try
		{
			$tbl = 'payments';
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sqlData = sql_associative_array($data);
			$sql = "INSERT INTO $tbl
			({$sqlData[0]})
			VALUES ({$sqlData[1]}?)";
			$q   = $pdo->prepare($sql);
			$q->execute($sqlData[2]);
			$id = $pdo->lastInsertId();	
			$pdo->commit();
	    }
		catch (PDOException $e)
		{
			$message = $e->getMessage();
		}
		$ret = $id."|".$message;
		return $ret;	
	}
}

$url = getbaseurl();
$url = str_replace("script_paybeneficiary.php",'',$url);

// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Database Declarations and config:  
$pdo = Database::connectDB();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$message = '';
$list = getpost('list');
$userid = $list['createdby'];
$list['createdon'] = date('Y-m-d');
$list['createdat'] = date("H:i:s");

$list['Status_Code'] = '1'; 									
$list['Status_Description'] = 'pending';	
		
$dataPayBenefiaryObj = null;
// -- Check Pay benefiary details
$dataPayBenefiaryObj = get_paybenefiary($list);

// -- Create Payment to Benefiary	
$retCollection = pay_to_beneficiary($list);
$rowData = explode(utf8_encode('|'), $retCollection);  // -- for UTF-8
$paymentid = $rowData[0];

if(!empty($paymentid))
{
	// -- Payment was made.
	$message = 'success|'.$paymentid;	
	// -- SMS
	if($list['Notify'] == 'SMS')
	{
		$smstemplatename = 'payments_notification';
		$applicationid = 0;		
		$contents = auto_template_content($userid,$smstemplatename,$applicationid,$list['Bank_Reference'],$list['Action_Date']);
		if(!empty($contents) && !empty($list['Contact_No']))
		{	
			$notification['phone'] 		 	= $list['Contact_No'];
			$notification['content'] 	 	= $contents;
			$notification['reference_1'] 	= $list['Bank_Reference'];	
			$notification['usercode'] 	 	= $list['UserCode'];
			$notification['createdby'] 	 	= $list['createdby'];
			$notification['IntUserCode'] 	= $list['IntUserCode'];
			$notification['amount'] 		= $list['Amount'];
			$notification['Account_Holder'] = $list['Account_Holder'];										
			// -- 
			$notificationsList[] = $notification;	// add into list
			// -- notify clients who signed for notifications(queue)
			if(!empty($notificationsList)){auto_sms_queue($notificationsList); }
		}
	}
	
	// -- EMAIL
	if($list['Notify'] == 'EMAIL')
	{
		$paymentidEncode = urlencode(base64_encode($paymentid));
		$proofofpaymenturl = $url.'eproofofpayment.php?paymentid='.$paymentidEncode;
		$subject     = 'Proof of Payment';
		$filename 	 = 'emailtemplate_proofofpayment.html';
		$IntUserCode = $list['IntUserCode'];
		$email 		 = $list['Contact_No'];
		$amount 	 = number_format($list['Amount'],2);
		$phone		 = '';
		$filecontent = file_get_contents($filename);
		$filecontent = str_replace("[Account_Holder]",$list['Account_Holder'],$filecontent);
		$filecontent = str_replace("[companyname]",'eCashMeUp',$filecontent);
		$filecontent = str_replace("[currency]",'R',$filecontent);
		$filecontent = str_replace("[Amount]",$amount,$filecontent);		
		$filecontent = str_replace("[Client_Reference_1]",$list['Client_Reference_1'],$filecontent);
		$filecontent = str_replace("[URL]",$proofofpaymenturl,$filecontent);
		$contents 	 = $filecontent;
		$status 	 = 'success';	

		// -- Database Declarations and config:
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		SendEmail($contents,$email,$subject);
	    AddCommunicatioHistoryGlobal($contents,$IntUserCode,$IntUserCode,$IntUserCode,$status,$email,$phone,'email');		
	}

	// -- Database Declarations and config:  
	$pdo = Database::connectDB();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
	
	// -- Update Benefiaciary's last transaction performed.
	$dataBenefiary['benefiaryid'] = $list['benefiaryid'];
	$dataBenefiary['lasttransaction'] = 'Last transaction: '.date('Y-m-d').'/ R'.$list['Amount'];	
	update_benefiary($dataBenefiary);
}
else
{
	$message = 'ERROR|'.$rowData[1];
}

/*if(isset($dataPayBenefiaryObj['benefiaryid']))
{
	// -- Payment warning message.
	$message =  $message.'|Warning:Payment to Beneficiary already exist for '.
	$dataPayBenefiaryObj['Action_Date'].' to bank account number '.
	$dataPayBenefiaryObj['Account_Number'].' with client reference '.
	$dataPayBenefiaryObj['Client_Reference_1'];			
}*/
 echo $message; 
?>