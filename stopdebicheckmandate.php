<?php
require 'database.php';

if (!function_exists('stopdebicheck'))
{
	function stopdebicheck($id,$arrayCollections)
	{
	 try
	  {
		//Global $pdo;
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "UPDATE mandates
		SET
		tracking_cancellation_indicator					= ?,  	
		cancellation_reason                  			= ?,
		status_code				= ?,
		status_desc				= ?,
		changedby = ?,
		changedon = ?,
		changedat = ?
		WHERE reference = ?";
		$q = $pdo->prepare($sql);

		//print_r($arrayCollections );	
		$arrayCollections['changedon'] = date('Y-m-d');
		$arrayCollections['changedat'] = date('h:m:s');;
		$arrayCollections['status_code'] = '04';
		$arrayCollections['status_desc'] = 'Cancelled';		
		
		$q->execute(array(
		$arrayCollections['tracking_cancellation_indicator'],				
		$arrayCollections['cancellation_reason'], 
		$arrayCollections['status_code'],
		$arrayCollections['status_desc'],		
		$arrayCollections['changedby'],
		$arrayCollections['changedon'],
		$arrayCollections['changedat'],
		$id));
		
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

$reference 			 = getpost('id_'); 		
$list		         = getpost('list');			

// -- Cancel DebiCheck Mandate.
$message = '';

// -- Function.
$message = stopdebicheck($reference,$list);
if(empty($message))
{
	 $message = 'Reference : '.$reference.' was stopped successfully.';
}

echo $message;
//print_r($list );	
?>