<?php
//phpinfo();
// -- Data File.
$db_path = '../database.php';
require $db_path;
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$key 		   = ''; //--UFFT
$format		   = ''; // json or xml
$AddressLine1  = '';//'B 2165 Newlands Heights'
$Id   		   = '';//'9606150675085'
$Forename1     = '';//'BUSISWA'
$Surname       = '';//'MAJOZI'
$json_object   = null;
// -- Get credit report.
if(!function_exists('get_count_of_creditreports_per_month'))
{
		function get_count_of_creditreports_per_month($key,$CurrentMonth)
		{
			$total_count = 0;
			// -- Read table api_webservice_moni
			// -- Check Id if it exist in current month.
			$tablename = 'api_webservice_moni';
			$dbnameobj = 'ecashpdq_eloan';
			$creditreportdata = null;

			$queryFields[] 	 = '*';
			$whereArray[]    = "usercode = '{$key}'"; // -- Client Reference.
			// 17/06/2022 -- $whereArray[]    = "date LIKE '%{$CurrentMonth}%'"; // -- Client Reference.
			// 17/06/2022 -- Transunion disabled eCashMeUp Api, so read archive data..
			// -- read data quickly.
			$creditreportObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
			
			$total_count = $creditreportObj->rowCount();
			return $total_count;
		}
}
// Request Structure
if(isset($_GET['format']))
{
	$format = $_GET['format'];
}

if(isset($_GET['AddressLine1']))
{
	$AddressLine1 = $_GET['AddressLine1'];
}


if(isset($_GET['Id']))
{
	$Id = $_GET['Id'];
}
//echo $Id;
 
if(isset($_GET['Forename1']))
{
	$Forename1 = $_GET['Forename1'];
}


if(isset($_GET['Surname']))
{
	$Surname = $_GET['Surname'];
}

if(isset($_GET['key']))
{
	$key = $_GET['key'];
}

function objectToArray($d) 
{
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    } else {
        // Return array
        return $d;
    }
}
// - format
	if(empty($format) || $format == 'json')
	{
		// -- modify header.
		header("Content-type:application/json;charset=UTF-8"); 
	}
	if($format == 'xml')
	{
		//header("Content-type:application/xml"); 
	}
	
	$valid = true;
	$requestLimits = 1400; // -- 40 in  a month for Chomza Technologies Pty Ltd(UFCT).
	$reachedlimit = false;
	
if(empty($Id)){  $valid = false;}
if(empty($key)){ $valid = false;}
else{
// -- Only allow eCashMeUp(UFEC),Chomza Technologies Pty Ltd(UFCT) for now.
	if($key != 'UFEC' and  $key != 'UFCT' and $key != 'UFUG' )
		{
			$valid = false;
			
		}
// -- number of limit per web service call per month.		
		else
		{
			/*if($key == 'UFCT' )
			{
				$CurrentMonth = date('Y-m');
				if(get_count_of_creditreports_per_month($key,$CurrentMonth) >= $requestLimits)
				{
					$reachedlimit = true;
					$valid = false;
				}
			}*/
		}
	}

// -- Get credit report from transunion.
if($valid)
{
	//17/06/2022
	$dataReturn = get_creditreport($Id,$key,$format,$pdo);
	if($dataReturn != 'null')
	{	
		//echo 'No';
		echo $dataReturn;
	}
	else //17/06/2022
	{
		echo 'No archived data for the seleteced Id:'.$key.'.Please note that the live system is under maintainance.';
	}
}	
else
{
	if($reachedlimit)
	{
		echo 'Your key ('.$key.') have reached limit of '.$requestLimits.' for '.$CurrentMonth;
	}
	else
	{
		echo 'You are not authorised.';
	}
}
?>