<?php 
// -- Disconnects from the Caller->Jquery->Ajax call, Making it a true Asynchronous.
// -- This will avoid Error:Network,Error:IO.Suspended
//ignore_user_abort(1); // Let the script run even if user leaves the page
//set_time_limit(0);    // Let script run forever

$absolute_path = null;
$path = '';
// -- Accept Array from jQuery
if(isset($_REQUEST['absolute_path']))
{
	$absolute_path = $_REQUEST['absolute_path'];
	$path = $absolute_path[0];
}
	$href = $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	$href = str_replace("script_sendbulksms.php","",$href);



$db_path = $_SERVER['DOCUMENT_ROOT'].str_replace('script_sendbulksms.php','database.php',$_SERVER['PHP_SELF']);
//echo $db_path;

// -- Data File.
require $db_path;
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(!function_exists('insert_api_response'))
	{
			function insert_api_response($phone,$message,$status,$pdo,$url)
			{
				// -- Insert into Db for tracing & log.
				    $costStr = '';
					$cost = 0.18;
					$no_of_sms_credit_required = ceil(strlen($message)/160.00);
					
					if($no_of_sms_credit_required>1){$cost = $cost * $no_of_sms_credit_required;}
					$costStr = ''.$cost;	
					$today = date("Y-m-d H:i:s");
					$sql = "INSERT INTO api_response(phone,message,status,date,url,cost) VALUES (?,?,?,?,?,?)";
					$q = $pdo->prepare($sql);
					$q->execute(array($phone,$message,$status,$today,$url,$costStr));	
			}
	}
  
if(!class_exists('CMSMSA'))
{
  class CMSMSA
  {
	function __construct()
	{
		
	}
    public function buildMessageXml($recipient, $message) 
	{
	  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', $ProductToken);

      $msg = $xml->addChild('MSG');
	  $msg->addChild('CHANNEL', 'SMS');

      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);
	  
	//Multipart SMS.Recipient will get more then 160 char at once
	  $no_of_sms_credit_required=ceil(strlen($message)/160.00);
	  
	  if($no_of_sms_credit_required>1)
      {
		  $msg->addChild('MINIMUMNUMBEROFMESSAGEPARTS',1);
		  $msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS',2); 
	  }
      $msg->addChild('BODY', $message);
	  
      return $xml->asXML();
    }

     public function sendMessage($recipient, $message) 
	{
      $xml = self::buildMessageXml($recipient, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://gw.cmtelecom.co.za/gateway.ashx',//'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);
	  //echo '<h3>'.curl_error($ch).'</h3>';
	  $responseError = curl_error($ch);
	  $info = curl_getinfo($ch);

      curl_close($ch);

      return $response;
    }
	
     public function numbervalidation($recipient) 
	 {
		 			  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
			
		  // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
		  $data = json_encode(array("phonenumber" => "string"));

		  $url = 'https://api.cmtelecom.com/v1.1/numbervalidation/'.$recipient;

		  $ch = curl_init();
		  curl_setopt_array($ch, array(
			 CURLOPT_URL            => $url,
			 CURLOPT_HTTPHEADER     => array(
										 'X-CM-PRODUCTTOKEN: f84364fb-717a-4bee-beb0-8f9ac43bf3ec'
										 ),
			 CURLOPT_HEADER         => TRUE,
			 CURLOPT_RETURNTRANSFER => TRUE
			)
		  );

		  $result = curl_exec($ch);

		  $json_start_pos = strpos($result,"Origin")+6;
		  $json_str = substr($result,$json_start_pos);
		  $json_str =  trim($json_str);
		  $json_str = utf8_encode($json_str);
		  //echo $json_str;

		  $obj  = json_decode($json_str);
		  //print_r($obj);
		  //var_dump($obj);
		  // Converting object to associative array 
		  $obj = json_decode(json_encode($obj), true); 
		  curl_close($ch);
		  return $obj;
	  }
  
  }
}
$url = '';

try
{
$myArray = null;
$urlArray = null;
$newArr = [];
$phone = '';
$message = '';
// -- Accept Array from jQuery
if(isset($_REQUEST['arrayFromPHP']))
{
	$myArray = $_REQUEST['arrayFromPHP'];
}
if(isset($_REQUEST['url']))
{
	$urlArray = $_REQUEST['url'];
}

// -- Get the URi file from jQuery array
if(!empty($urlArray))
{$url = $urlArray[0];}

// -- Create an object for sending SMS.
$smsRef = new CMSMSA();
$response = null;

// == Convert from 3 D array to an undertandable format
if(!empty($myArray))
{
foreach($myArray as $data => $val)
    {
		foreach($val as $key2 => $val2)
       {
           $newArr[] = $val2;
       }
	}
// -- Is a Mixed Array.
foreach($newArr as $key => $val)
    {
		if(is_array($val))
		{
			if(isset($val['da']))
			{
				$phone = $val['da']; 
			    if(!empty($message) && !empty($phone))
				 {	 // -- sms length is 160 characters,split every 160 characters, and sent as separate message.
					 $constant_smslength = 160;
					 $smsLength = strlen($message);
					 $lengthDiff = $smsLength  - $constant_smslength;
					 //echo $smsLength.'-'.$constant_smslength.'='.$lengthDiff;
					 // -- print($message);
					 // -- Add Country code : 0027 trim '+' and add '00'(+27)
					 $phone_plus = $phone;
					 $phone = trim("00".substr($phone,1));
					 $smsRef = new CMSMSA();
					 $response = $smsRef->numbervalidation($phone);
					 if(empty($response['valid_number']))
					 { // -- Phone number is invalid
						$status = 'failed'; 
						insert_api_response($phone_plus,$message,$status,$pdo,$url);														
					 }
					 else
					 {
						$status = 'sent'; 
						//if($lengthDiff <= 0)// -- Length within 160 characters
						//{
							$response = $smsRef->sendMessage($phone, $message);
							if($response=="")
							{$status = 'sent'; }
							else
							{$status = 'failed'; 
								//print_r($response);
							}
							insert_api_response($phone_plus,$message,$status,$pdo,$url);
					 }
						/*else // -- Length is more than 160 characters
						{
							// -- Split message until differe is <=0
							$messages = str_split($message, $constant_smslength);
							$itemCount = 0;
							foreach($messages as $content)
							{
								//echo $content; 
								$smsRef = new CMSMSA();
								$response = $smsRef->sendMessage($phone, $content);
								
								insert_api_response($phone_plus,$content,$status,$pdo,$url);								
								$itemCount = $itemCount + 1;
							}
						}*/
					 }
				 }

			}
			else
			{
				$message = $val;	
			}
    }
}	
}
//catch exception
catch(Exception $e) 
{
	$phone = '0027731238839';
	$content = $url.'Error : '.$e->getMessage();
	$smsRef = new CMSMSA();
	$response = $smsRef->sendMessage($phone, $content);	
    //echo 'Message: ' .$e->getMessage();
}
?>