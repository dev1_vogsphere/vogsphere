<?php
// -- Chenosis API -- 1). GET access token 
if(!function_exists('getaccesstoken'))
{
	function getaccesstoken($data)
	{
		try
		{
			 $dataresponse = null;
			 $dataresponse['response'] = 'Error : failed API call no data';
			 $curl = curl_init();
			 curl_setopt_array($curl, [
			  CURLOPT_URL => "https://api.chenosis.io/oauth/client/accesstoken?grant_type=client_credentials",//"https://sandbox.api.chenosis.io/oauth/client/accesstoken",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_SSL_VERIFYHOST =>false,
			  CURLOPT_SSL_VERIFYPEER => false,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "grant_type=client_credentials&client_id={$data['client_id']}&client_secret={$data['client_secret']}",
			  CURLOPT_HTTPHEADER => [
				"content-type: application/x-www-form-urlencoded"
			  ],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  $dataresponse['response'] = "cURL Error #:" . $err;
			} else {
			  $dataresponse['response'] = $response;
			}
			$obj = json_decode($dataresponse['response']);
			return $obj;
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- Chenosis API -- 2). Create verification 
if(!function_exists('createverification'))
{
	function createverification($data)
	{
		try
		{
			 $dataresponse = null;
			 $dataresponse['response'] = 'Error : failed API call no data';
			 // -- json format {"channel": "sms", "to":"+27xxxxxxxxxx"}
			 $jsonobj = json_encode($data);
			 
			 $curl = curl_init();
			 curl_setopt_array($curl, [
			  CURLOPT_URL => "https://api.chenosis.io/verify-za/v1/identity/verification",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_SSL_VERIFYHOST =>false,
			  CURLOPT_SSL_VERIFYPEER => false,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $jsonobj,
			  CURLOPT_HTTPHEADER => ["authorization: Bearer {$data['access_token']}",
				"content-type: application/json"],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  $dataresponse['response'] = "cURL Error #:" . $err;
			} else {
			  $dataresponse['response'] = $response;
			}
			$obj = json_decode($dataresponse['response']);
			return $obj;			
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
// -- Chenosis API -- 3). Verify verification code. 
if(!function_exists('verifycode'))
{
	function verifycode($data)
	{
		try
		{
			 $dataresponse = null;
			 $dataresponse['response'] = 'Error : failed API call no data';
			 // -- json format {"code": "855839"}
			 $jsonobj = json_encode($data);
			 $curl = curl_init();
			 curl_setopt_array($curl, [
			  CURLOPT_URL => "https://api.chenosis.io/verify-za/v1/identity/verification/{$data['verificationId']}",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_SSL_VERIFYHOST =>false,
			  CURLOPT_SSL_VERIFYPEER => false,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PATCH",
			  CURLOPT_POSTFIELDS => $jsonobj,
			  CURLOPT_HTTPHEADER => ["authorization: Bearer {$data['access_token']}",
				"content-type: application/json"],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  $dataresponse['response'] = "cURL Error #:" . $err;
			} else {
			  $dataresponse['response'] = $response;
			}
			$obj = json_decode($dataresponse['response']);
			return $obj;			
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}
/*
// -- Chenosis API -- 1). GET access token 
$data = null;
$access_token   = '';
$verificationId = '';
$status 		= '';

$client_id 	   = 'pxoB6cy1UgoDxd6dgt63TrSVu907ezVp';//'ANOpWzAvThjdNq9wHXX8NCBPANJwt7Fx';
$client_secret = 'tosW6fAH5KR0AtYq';//'ik6N5zRH9G8YuPPv';
$data['client_id']     = $client_id ;
$data['client_secret'] = $client_secret ;			
// -- API Feedback..
$obj = getaccesstoken($data);
if(isset($obj->access_token))
{ 
	$access_token = $obj->access_token;
}
// -- Chenosis API -- 2). Create verification 
if(!empty($access_token))
{
	$dataCreate 			   = null;
	$dataCreate['channel']     = "sms" ;
	$dataCreate['to'] = "+27813073233" ;
	$dataCreate['access_token'] = $access_token;
	$obj 		  = null;
	$obj = createverification($dataCreate);
	echo 'access_token:'.$access_token;	
	// -- print_r($obj);
	echo '<br/>';	
	if(isset($obj->data->verificationId))
	{ 
	  $verificationId = $obj->data->verificationId;
	  echo 'verificationId:'.$verificationId;
	}
}
// -- Chenosis API -- 3). Verify verification code. 
if(!empty($verificationId))
{
	$dataVerifycode = null;
	$dataVerifycode['code']           = "855839" ;
	$dataVerifycode['access_token']   = $access_token;
	$dataVerifycode['verificationId'] = $verificationId;
	$obj 		  = null;
	$obj = verifycode($dataVerifycode);
	if(isset($obj->data->status))
	{
		$status = $obj->data->status;
		echo '<br/>';
		echo 'status:'.$status;
	}	
}
if(isset($obj->Error))
{
	echo 'Error = '.$obj->Error;
}*/
?>
