<?php 
$db_path = $_SERVER['DOCUMENT_ROOT'].str_replace('cronjob_script_sendbulksms.php','database.php',$_SERVER['PHP_SELF']);
// -- Data File.
require $db_path;
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(!function_exists('insert_api_response'))
	{
			function insert_api_response($phone,$message,$status,$pdo,$url,$costStr,$today)
			{
				// -- Insert Queue.
					$pdo->beginTransaction(); // also helps speed up your inserts.
					try 
					{
						$sql = "INSERT INTO api_response(phone,message,status,date,url,cost) VALUES (?,?,?,?,?,?)";
						$q = $pdo->prepare($sql);
						$q->execute(array($phone,$message,$status,$today,$url,$costStr));						
					} 
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
					$pdo->commit();
			}
	}

if(!function_exists('remove_api_responsequeue'))
	{
			function remove_api_responsequeue($id,$pdo)
			{
				// -- Remove from the Queue.
					try 
					{
					$sql = "Delete From api_responsequeue where id = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($id));						
					} 
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}
	
if(!function_exists('check_api_responsequeue'))
	{
			function check_api_responsequeue($id,$pdo)
			{
				// -- Remove from the Queue.
					try 
					{
					$sql = "select * from api_responsequeue where id = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($id));
					//--
					$dataDapi_responsequeue = $q->fetch(PDO::FETCH_ASSOC);
					return $dataDapi_responsequeue;
					//--					
					} 
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}


if(!class_exists('TextClientStatusCodes'))
{	
/**
 * Class TextClientStatusCodes
 *
 * Translations for the errorCode property from the gateway response body.
 *
 * @package CMText
 */
class TextClientStatusCodes
{
    /**
     * All is well.
     */
    const OK = 0;

    /**
     * Authentication of the request failed.
     */
    const AUTHENTICATION_FAILED = 101;

    /**
     * The account using this authentication has insufficient balance.
     */
    const BALANCE_INSUFFICIENT = 102;

    /**
     * The product token is incorrect.
     */
    const APIKEY_INCORRECT = 103;

    /**
     * This request has one or more errors in its messages. Some or all messages have not been sent. See MSGs for details.
     */
    const REQUEST_NOT_ALL_SENT = 201;

    /**
     * This request is malformed, please confirm the JSON and that the correct data types are used.
     */
    const REQUEST_MALFORMED = 202;

    /**
     * The request's MSG array is incorrect.
     */
    const MSG_ARRAY_INCORRECT = 203;

    /**
     * This MSG has an invalid From field (per msg).
     */
    const MSG_INVALID_FROM = 301;

    /**
     * This MSG has an invalid To field (per msg).
     */
    const MSG_INVALID_TO = 302;

    /**
     * This MSG has an invalid MSISDN in the To field (per msg).
     */
    const MSG_INVALID_MSISDN = 303;

    /**
     * This MSG has an invalid Body field (per msg).
     */
    const MSG_INVALID_BODY = 304;

    /**
     * This MSG has an invalid field. Please confirm with the documentation (per msg).
     */
    const MSG_INVALID_FIELD = 305;

    /**
     * Message has been spam filtered.
     */
    const SPAM = 401;

    /**
     * Message has been blacklisted.
     */
    const BLACKLISTED = 402;

    /**
     * Message has been rejected.
     */
    const REJECTED = 403;

    /**
     * An internal error has occurred.
     */
    const INTERNAL_ERROR = 500;

    /**
     * Unknown error, please contact CM support.
     */
    const UNKNOWN = 999;
}
}
if(!class_exists('jsonserialized'))
{
	class jsonserialized implements JsonSerializable 
	{
			function __construct($value) { $this->value = $value; }
			function jsonSerialize() { return $this->value; }
	}
}	
if(!class_exists('CMSMSA'))
{
  class CMSMSA
  {
	function __construct()
	{
		
	}
    public function buildMessageXml($recipient, $message) 
	{
	  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', $ProductToken);

      $msg = $xml->addChild('MSG');
	  $msg->addChild('CHANNEL', 'SMS');

      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);
	  
	//Multipart SMS.Recipient will get more then 160 char at once
	  $no_of_sms_credit_required=ceil(strlen($message)/160.00);
	  
	  if($no_of_sms_credit_required>1)
      {
		  $msg->addChild('MINIMUMNUMBEROFMESSAGEPARTS',1);
		  $msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS',2); 
	  }
      $msg->addChild('BODY', $message);
	  
      return $xml->asXML();
    }

    public function sendMessage($recipient, $message) 
	{
      $xml = self::buildMessageXml($recipient, $message);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://gw.cmtelecom.co.za/gateway.ashx',//'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);
	  //echo '<h3>'.curl_error($ch).'</h3>';
	  $responseError = curl_error($ch);
	  $info = curl_getinfo($ch);

      curl_close($ch);

      return $response;
    }
	
	public function buildMessageJson($recipient, $message) 
	{
		/**
		* SDK Version constant
		*/
		 $VERSION = '1.1.3';
		 
		//Multipart SMS.Recipient will get more then 160 char at once
		$no_of_sms_credit_required=ceil(strlen($message)/160.00);
	  // -- Build Message Body.
	  $messagebody = array();
	  $messagebody = ['type' => "AUTO",
            'content' => function_exists('mb_convert_encoding') ?
                mb_convert_encoding($message, 'UTF-8', mb_detect_encoding($message)) : $message
            ];
		
		$return = array();
		$return = [
			'allowedChannels' => ['SMS'],
			'from'      => 'STLM',
            'to'        => array_map(function ($number){
                return [
                    'number' => $number,
                ];
            }, $recipient),
			'body'      => $messagebody,
			'reference' => "STLM",
            'customgrouping3' => 'text-sdk-php-' . $VERSION,
            'minimumNumberOfMessageParts' => 1,
            'maximumNumberOfMessageParts' => $no_of_sms_credit_required,
        ];
		
		return (object)$return;
	}
	
	public function sendMessagejson($recipient, $message) 
	{
		// -- For now is one phone number at a time.
	  	$recipients[] = $recipient;
		$jsonContent = array();
		$jsonContent[] = self::buildMessageJson($recipients, $message);
		//$jsonContent = json_encode(new jsonserialized($jsonContent));
		//print_r($jsonContent);
	  /**
		* SDK Version constant
		*/
		 $VERSION = '1.1.3';
	  /**
      * South Africa local gateway
      */
		$gateway = 'https://gw.cmtelecom.co.za/v1.0/message';
		$ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
		$requestModel =(object)['messages' => (object)['authentication' => (object)['producttoken' => $ProductToken,],
						'msg' => $jsonContent]];
		$requestModel = new jsonserialized($requestModel);
						
		//print_r($requestModel);				
		// -- Recipients Array.
		
		// -- Message.

		// -- 
		 $ch = curl_init($gateway);
		 
		 // ----------------------------------- BOC ---------------
		 try {
            curl_setopt_array($ch, [
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($requestModel),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json; charset=utf-8',
                    'Content-Length: ' . strlen( json_encode($requestModel) ),
                    'X-CM-SDK: ' . 'text-sdk-php-' . $VERSION,
                ],
                CURLOPT_TIMEOUT => 20,
                CURLOPT_CONNECTTIMEOUT => 5,
            ]);

            $response = curl_exec($ch);
            $statuscode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

            // curl errors will raise an exception
            //if( curl_error($ch) ){
             //   throw new \Exception( curl_error($ch) );
            //}

        }catch (Exception $exception)
		{
            $response = json_encode(['details' => $exception->getMessage()]);
            $statuscode = TextClientStatusCodes::UNKNOWN;

        }finally
		{
            curl_close($ch);
        }
		return 
		// decode the response
        $json = json_decode($response, false, 5);
		 // ----------------------------------- EOC -------------

	}
	
    public function numbervalidation($recipient) 
	{
		 $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
			
		  // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
		  $data = json_encode(array("phonenumber" => "string"));

		  $url = 'https://api.cmtelecom.com/v1.1/numbervalidation/'.$recipient;

		  $ch = curl_init();
		  curl_setopt_array($ch, array(
			 CURLOPT_URL            => $url,
			 CURLOPT_HTTPHEADER     => array(
										 'X-CM-PRODUCTTOKEN: f84364fb-717a-4bee-beb0-8f9ac43bf3ec'
										 ),
			 CURLOPT_HEADER         => TRUE,
			 CURLOPT_RETURNTRANSFER => TRUE
			)
		  );

		  $result = curl_exec($ch);

		  $json_start_pos = strpos($result,"Origin")+6;
		  $json_str = substr($result,$json_start_pos);
		  $json_str =  trim($json_str);
		  $json_str = utf8_encode($json_str);
		  //echo $json_str;

		  $obj  = json_decode($json_str);
		  // Converting object to associative array 
		  $obj = json_decode(json_encode($obj), true); 
		  curl_close($ch);
		  return $obj;
	  }
	  
	  public function getaccountbalance($accountID)
	  {
		  // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
		  $parameters = array(
			'accountID' => 'accountID'
		  );

		  $url = 'https://api.cmtelecom.com/accountbalance/v1.0/accountbalance/'
				 .$accountID;

		  $ch = curl_init();
		  curl_setopt_array($ch, array(
			 CURLOPT_URL            => $url,
			 CURLOPT_HTTPHEADER     => array(
										 'Content-Type: application/json'
										 ),
			 CURLOPT_HEADER       => TRUE,
			 CURLOPT_RETURNTRANSFER => true
			)
		  );
		  $result = curl_exec($ch);
		  $HeaderInfo = curl_getinfo($ch);
		  $HeaderSize=$HeaderInfo['header_size'];
		  $Body = trim(mb_substr($result, $HeaderSize));
		  $ResponseHeader = explode('\n',trim(mb_substr($result, 0, $HeaderSize)));
		  unset($ResponseHeader[0]);
		  $Headers = array();
		  foreach($ResponseHeader as $line){
			  list($key,$val) = explode(':',$line,2);
			  $Headers[strtolower($key)] = trim($val);
		  }

		  var_dump( array(
			  'Body' => $Body,
			  'Headers' => $Headers
		  ));

	  }
  
	public function objectToArray($d) 
	{
        if (is_object($d)) 
		{
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }
        // Return array
            return $d;
    }
  
  }
}
$url = '';

try
{
	
// -- Query Db Communications.
 $tablename = 'api_responsequeue';
 $dbnameobj = 'ecashpdq_eloan';
 $queryFields = null;
 $queryFields[] = '*';
 $whereArray  = null;		
 $status = '"queued"';
 $whereArr[] = "status = {$status}";
 $data = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);;			

if($data->rowCount() < 1){ echo 'no SMSs in a queue';}
$count = 0;
foreach($data as $row)
{ 
	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
	// -- Create an object for sending SMS.
	 $smsRef    = new CMSMSA();
	 $response  = null;
	 $phone 	= $row['phone'];
	 $message   = $row['message'];
	 $url 	    = $row['url'];
	 $costStr   = $row['cost'];
	 $today = date("Y-m-d H:i:s");
	 $dataDapi_responsequeue = check_api_responsequeue($row['id'],$pdo);
	 
	 // -- Check if the Sequence is still in DB.
	 if(!empty($dataDapi_responsequeue))
	 {	 $phone_plus = $phone;
		 $phone = trim("00".substr($phone,1));
		 $response = $smsRef->numbervalidation($phone);
		 if(empty($response['valid_number']))
		 { // -- Phone number is invalid
			$status = 'failed'; 
			insert_api_response($phone_plus,$message,$status,$pdo,$url,$costStr,$today);														
		 }
		 else
		 {	    $statusCode = '';
				/* -- xml message
				$status = 'sent'; 
				$response = $smsRef->sendMessage($phone, $message);
				if($response=="")
				{
					$status = 'sent'; 
				}
				else
				{
					$status = 'failed'; 
				}
				*/
				// -------- BOC Json Message ------- //
				$responsejson = $smsRef->sendMessagejson($phone, $message);	
					//print_r($responsejson);
				
				if(null === $responsejson)
				{
					$statusCode    = TextClientStatusCodes::UNKNOWN;
				    $status = 'failed'; 
				}else
				{
					//$this->statusMessage = $json->details   ?? 'An error occurred';
					//$statusCode    = $json->errorCode ?? TextClientStatusCodes::UNKNOWN;
					//$this->details       = $json->messages  ?? [];
					$status = 'sent'; 
					 
					$stdClass = $responsejson->messages[0];
					$statusArray = $smsRef->objectToArray($stdClass);
					print_r($statusArray);

					$status = $statusArray['status'];
				}
				// --------- EOC json MESSAGES ----- //
				
				insert_api_response($phone_plus,$message,$status,$pdo,$url,$costStr,$today);
		 }
		 // -- Delete Record from the Queue.
		 remove_api_responsequeue($row['id'],$pdo);
		 $count = $count + 1;
	 }
	 
}
if($count >0){echo 'Total Number '.$count.' of SMSs were processed.';}

}
//catch exception
catch(Exception $e) 
{
	$phone = '0027731238839';
	$content = $url.'Error : '.$e->getMessage();
	$smsRef = new CMSMSA();
	$response = $smsRef->sendMessage($phone, $content);	
    echo 'Message: ' .$e->getMessage();
}
?>