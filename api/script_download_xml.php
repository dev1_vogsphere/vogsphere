<?php
$urlArray = null;
$url 	  = '';

// -- URL.
if(isset($_REQUEST['url']))
{
	$urlArray = $_REQUEST['url'];
}

// -- Get the URi file from jQuery.
if(!empty($urlArray))
{
	$url = $urlArray[0];
}

// -- Download XML.
downloadFile($url,$url);

function downloadFile($url, $filename) 
{
    $cURL = curl_init($url);
    curl_setopt_array($cURL, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_FILE           => fopen("Downloads/$filename", "w+"),
        CURLOPT_USERAGENT      => $_SERVER["HTTP_USER_AGENT"]
    ]);

    $data = curl_exec($cURL);
    curl_close($cURL);
    header("Content-Disposition: attachment; filename=\"$filename\"");
    echo $data;
}
?>