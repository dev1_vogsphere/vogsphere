<?php 
// -- Globalisation.
$db_path = $_SERVER['DOCUMENT_ROOT'].str_replace('script_responses.php','database.php',$_SERVER['PHP_SELF']);
// -- Data File.
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- https://gateway.cmtelecom.com/en/f5a363bf-111a-4150-b767-60fe9b3e13f7
$DATETIME_S 		= '';
$GSM 				= '';
$STANDARDERRORTEXT 	= '';
$STATUS 			= '';
$message            = '';
$reference 			= '';

if(!function_exists('update_api_response'))
	{
			function update_api_response($phone,$url,$status,$pdo)
			{
				// --Update the Status Queue.
				$tabname = 'api_response';
					try 
					{
					$sql = "select * from $tabname where phone = ? and url = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($phone,$url));
					//--
					$dataDapi_responsequeue = $q->fetch(PDO::FETCH_ASSOC);
					// -- Change Status to pending.
					if(!empty($dataDapi_responsequeue))
					{	$id = $dataDapi_responsequeue['id'];
						$sql = "UPDATE $tabname SET status = ? where id = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($status,$id));
					}
					return $dataDapi_responsequeue;
					//--					
					} 
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}	

// -- Accept Array from jQuery
if(isset($_REQUEST['DATETIME_S']))
{
	$DATETIME_S = $_REQUEST['DATETIME_S'];
}

if(isset($_REQUEST['GSM']))
{
	$GSM = $_REQUEST['GSM'];
}
if(isset($_REQUEST['STANDARDERRORTEXT']))
{
	$STANDARDERRORTEXT = $_REQUEST['STANDARDERRORTEXT'];
}
if(isset($_REQUEST['STATUS']))
{
	$STATUS = $_REQUEST['STATUS'];
	switch($STATUS)
	{
	  	case 0:
		 $STATUS = 'accepted';
		break;
		case 1:
		 $STATUS = 'rejected';
		break;
		case 2:
		 $STATUS = 'delivered';		
		break;
		case 3:
		 $STATUS = 'failed';				
		break;
		case 4:
		 $STATUS = 'Read';						
		break;
		default:
		 $STATUS = 'accepted';								
		break;
	}
	/*  0 = accepted by the operator
		1 = rejected by CM or operator
		2 = delivered
		3 = failed (message was not and will not be delivered)
		4 = Read (Not for SMS, only other channels)
	*/
}

if(isset($_REQUEST['REFERENCE']))
{
	$reference = $_REQUEST['REFERENCE'];
}

// -- Log Responses
if(!empty($GSM) && !empty($reference))
{
	// == +2773xxx
	$GSMTemp = $GSM;
	$GSM = trim("+".substr($GSM,2));
	update_api_response($GSM,$reference,$STATUS,$pdo);		
	echo 'Sucess Updated: REFERENCE = '.$reference.', Phone number : '. $GSM;
	
	// == 073xxxx
	$GSM = trim("0".substr($GSMTemp,3));
	update_api_response($GSM,$reference,$STATUS,$pdo);		
	echo 'Sucess Updated: REFERENCE = '.$reference.', Phone number : '. $GSM;
}
else
{
	echo 'no phone number to update';
}	

?>