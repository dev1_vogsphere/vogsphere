<?php
$db_path = $_SERVER['DOCUMENT_ROOT'].str_replace('cronjob_script_sendbulksms.php','database.php',$_SERVER['PHP_SELF']);
// -- Data File.
require $db_path;
// -- Database Declarations and config:
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(!function_exists('insert_api_response'))
	{
			function insert_api_response($phone,$message,$status,$pdo,$url,$costStr,$today,$usercode,$amount,$createdby)
			{
				// -- Insert Queue.
					$pdo->beginTransaction(); // also helps speed up your inserts.
					try
					{
						$sql = "INSERT INTO api_response(phone,message,status,date,url,cost,usercode,createdby,amount) VALUES (?,?,?,?,?,?,?,?,?)";
						$q = $pdo->prepare($sql);
						$q->execute(array($phone,$message,$status,$today,$url,$costStr,$usercode,$createdby,$amount));
					}
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
					$pdo->commit();
			}
	}

if(!function_exists('remove_api_responsequeue'))
	{
			function remove_api_responsequeue($id,$pdo)
			{
				// -- Remove from the Queue.
					try
					{
					$sql = "Delete From api_responsequeue where id = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($id));
					}
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}

if(!function_exists('check_api_responsequeue'))
	{
			function check_api_responsequeue($id,$pdo)
			{
				// -- Remove from the Queue.
					$status = 'queued';
					$statusPending = 'pending';
					try
					{
					$sql = "select * from api_responsequeue where id = ? and status = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($id,$status));
					//--
					$dataDapi_responsequeue = $q->fetch(PDO::FETCH_ASSOC);
					// -- Change Status to pending.
					if(!empty($dataDapi_responsequeue))
					{	$sql = "UPDATE api_responsequeue SET status = ? where id = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($statusPending,$id));
					}
					return $dataDapi_responsequeue;
					//--
					}
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}

// -- Check Duplicates
if(!function_exists('check_api_response_duplicates'))
	{
			function check_api_response_duplicates($phone,$url,$date,$pdo)
			{
					try
					{
					$dataDapi_response_duplicate = null;
					$sql = "select * from api_response where phone = ? and url = ? and date = ?";
					$q = $pdo->prepare($sql);
					$q->execute(array($phone,$url,$date));
					//-- Check if exists
					$dataDapi_response_duplicate = $q->fetch(PDO::FETCH_ASSOC);
					return $dataDapi_response_duplicate;
					//--
					}
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}
// -- Mark queue message as duplicate.
if(!function_exists('mark_api_responsequeue_duplicate'))
	{
			function mark_api_responsequeue_duplicate($id,$pdo)
			{
				// -- mark from the Queue as duplicate.
					$status = 'duplicate';
					try
					{

					// -- Change Status to duplicate.
						$sql = "UPDATE api_responsequeue SET status = ? where id = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($status,$id));
					}
					catch (PDOException $e)
					{
					echo $e->getMessage();
					}
			}
	}

if(!class_exists('CMSMSA'))
{
  class CMSMSA
  {
	function __construct()
	{

	}
    public function buildMessageXml($recipient, $message,$reference)
	{
	  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';
      $xml = new SimpleXMLElement('<MESSAGES/>');

      $authentication = $xml->addChild('AUTHENTICATION');
      $authentication->addChild('PRODUCTTOKEN', $ProductToken);

//Helpful for assign cusotm SMS ID
	 if($reference!="")
	 {
		  $xml->addChild('REFERENCE',$reference);
	 }

      $msg = $xml->addChild('MSG');
	  $msg->addChild('CHANNEL', 'SMS');

      $msg->addChild('FROM', 'Company');
      $msg->addChild('TO', $recipient);

	//Multipart SMS.Recipient will get more then 160 char at once
	  $no_of_sms_credit_required=ceil(strlen($message)/160.00);

	  if($no_of_sms_credit_required>1)
      {
		  $msg->addChild('MINIMUMNUMBEROFMESSAGEPARTS',1);
		  $msg->addChild('MAXIMUMNUMBEROFMESSAGEPARTS',2);
	  }
      $msg->addChild('BODY', $message);

      return $xml->asXML();
    }

     public function sendMessage($recipient, $message,$reference)
	{
      $xml = self::buildMessageXml($recipient, $message,$reference);

      $ch = curl_init(); // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
      curl_setopt_array($ch, array(
          CURLOPT_URL            => 'https://gw.cmtelecom.co.za/gateway.ashx',//'https://sgw01.cm.nl/gateway.ashx',
          CURLOPT_HTTPHEADER     => array('Content-Type: application/xml'),
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => $xml,
          CURLOPT_RETURNTRANSFER => true
        )
      );

      $response = curl_exec($ch);
	  //echo '<h3>'.curl_error($ch).'</h3>';
	  $responseError = curl_error($ch);
	  $info = curl_getinfo($ch);

      curl_close($ch);

      return $response;
    }

     public function numbervalidation($recipient)
	 {
		 			  $ProductToken = 'f84364fb-717a-4bee-beb0-8f9ac43bf3ec';

		  // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
		  $data = json_encode(array("phonenumber" => "string"));

		  $url = 'https://api.cmtelecom.com/v1.1/numbervalidation/'.$recipient;

		  $ch = curl_init();
		  curl_setopt_array($ch, array(
			 CURLOPT_URL            => $url,
			 CURLOPT_HTTPHEADER     => array(
										 'X-CM-PRODUCTTOKEN: f84364fb-717a-4bee-beb0-8f9ac43bf3ec'
										 ),
			 CURLOPT_HEADER         => TRUE,
			 CURLOPT_RETURNTRANSFER => TRUE
			)
		  );

		  $result = curl_exec($ch);

		  $json_start_pos = strpos($result,"Origin")+6;
		  $json_str = substr($result,$json_start_pos);
		  $json_str =  trim($json_str);
		  $json_str = utf8_encode($json_str);
		  //echo $json_str;

		  $obj  = json_decode($json_str);
			//echo $obj;
		  // Converting object to associative array
		  $obj = json_decode(json_encode($obj), true);

		  curl_close($ch);
		  return $obj;
	  }

	  public function getaccountbalance($accountID)
	  {
		  // cURL v7.18.1+ and OpenSSL 0.9.8j+ are required
		  $parameters = array(
			'accountID' => 'accountID'
		  );

		  $url = 'https://api.cmtelecom.com/accountbalance/v1.0/accountbalance/'
				 .$accountID;

		  $ch = curl_init();
		  curl_setopt_array($ch, array(
			 CURLOPT_URL            => $url,
			 CURLOPT_HTTPHEADER     => array(
										 'Content-Type: application/json'
										 ),
			 CURLOPT_HEADER       => TRUE,
			 CURLOPT_RETURNTRANSFER => true
			)
		  );
		  $result = curl_exec($ch);
		  $HeaderInfo = curl_getinfo($ch);
		  $HeaderSize=$HeaderInfo['header_size'];
		  $Body = trim(mb_substr($result, $HeaderSize));
		  $ResponseHeader = explode('\n',trim(mb_substr($result, 0, $HeaderSize)));
		  unset($ResponseHeader[0]);
		  $Headers = array();
		  foreach($ResponseHeader as $line){
			  list($key,$val) = explode(':',$line,2);
			  $Headers[strtolower($key)] = trim($val);
		  }

		  var_dump( array(
			  'Body' => $Body,
			  'Headers' => $Headers
		  ));

	  }

  }
}
$url = '';

try
{

// -- Query Db Communications.
 $tablename = 'api_responsequeue';
 $dbnameobj = 'ecashpdq_eloan';
 $queryFields = null;
 $queryFields[] = '*';
 $whereArray  = null;
 $status = '"queued"';
 $whereArr[] = "status = {$status}";
 $data = search_dynamic($dbnameobj,$tablename,$whereArr,$queryFields);;

if($data->rowCount() < 1){ echo 'no SMSs in a queue';}
$count = 0;
foreach($data as $row)
{
	foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
	// -- Create an object for sending SMS.
	 $usercode = $row['usercode'];
	 $smsRef    = new CMSMSA();
	 $response  = null;
	 $phone 	= $row['phone'];
	 $message   = $row['message'];
	 $url 	    = $row['url'];
	 $costStr   = $row['cost'];
	 $today = date("Y-m-d H:i:s");
	 $amount = $row['amount'];
	 $createdby = $row['createdby'];
	 $dataDapi_responsequeue = check_api_responsequeue($row['id'],$pdo);

	 // -- Check if the Sequence is still in DB.
	 if(!empty($dataDapi_responsequeue))
	 {	 $phone_plus = $phone;
		 $phone = trim("0027".substr($phone,1));// assumption that all number are saved as 082-replace 1st latter with 0 with 0027

		 $response = $smsRef->numbervalidation($phone);
		 /*if(empty($response['valid_number']))- Commented
		 { // -- Phone number is invalid
			$status = 'failed';
			insert_api_response($phone_plus,$message,$status,$pdo,$url,$costStr,$today,$usercode,$amount,$createdby);
		 }
		 else*/
		 {
				$status = 'sent';
				// -- Check for Duplicates
				if(empty(check_api_response_duplicates($phone_plus,$url,$today,$pdo)))
				{
					$response = $smsRef->sendMessage($phone, $message,$url);
					if($response=="")
					{
						$status = 'sent';
					}
					else
					{
						$status = 'failed';
					}
					insert_api_response($phone_plus,$message,$status,$pdo,$url,$costStr,$today,$usercode,$amount,$createdby);
				}
				else
				{
					// -- Mark Queue as duplicate
					 mark_api_responsequeue_duplicate($id,$pdo);
				}
		 }
		 // -- Delete Record from the Queue.
		 remove_api_responsequeue($row['id'],$pdo);
		 $count = $count + 1;
	 }

}
if($count >0){echo 'Total Number '.$count.' of SMSs were processed for url = '.$url;}

}
//catch exception
catch(Exception $e)
{
	$phone = '0027731238839';
	$content = $url.'Error : '.$e->getMessage();
	$smsRef = new CMSMSA();
	//$response = $smsRef->sendMessage($phone, $content);
    echo 'Message: ' .$e->getMessage();
}
?>
