<?php
// -- Data File.
require 'database.php';
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$url = '';
$sxml = null;
$page = '';
$href = '';
$tbl_name  = 'api_response';
$tbl_name1 = 'api_responsequeue';
$array = array();
$count = 0;
if(isset($_GET['url']))
{
	// -- API_Response
	$url = $_GET['url'];
	$sql = "SELECT * FROM $tbl_name WHERE url = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($url));	
	$data = $q->fetchAll();
	
	// -- API_Response Queue
	$url = $_GET['url'];
	$sql = "SELECT * FROM $tbl_name1 WHERE url = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($url));	
	$data1 = $q->fetchAll();
	
	// -- Merge ResponseQueue & Responses
	$data = array_merge($data,$data1);
	
	// Heading
	/*echo "<table border=1>"; 
	echo "<tr>"; 
	echo "<td><b>Cell Phone</b></td>"; 
	echo "<td><b>Date & Time</b></td>";
		echo "<td><b>Status</b></td>"; 

	echo "<td><b>Message</b></td>";
	echo "</tr>"; 	*/
	foreach($data as $row)
	{ 
		foreach($row AS $key => $value) { $row[$key] = stripslashes($value); }
		$array[$count][0] = $row['phone'];
		$array[$count][1] = $row['date'];
		$array[$count][2] = $row['status'];
		$array[$count][3] = $row['message'];
		$count = $count + 1;
		/*echo "<tr>"; 
		echo "<td valign='top'>" . nl2br( $row['phone']) . "</td>";  
		echo "<td valign='top'>" . nl2br( $row['date']) . "</td>";  
		echo "<td valign='top'>" . nl2br( $row['message']) . "</td>"; 
echo "</tr>"; */		
	}
	echo "</table>";
}

//print_r($array);

$myjson = json_encode($array);
echo $myjson;
?>