<?php 

// Table name - PaymentSchedule.  
$tbl_name ="paymentschedule"; 
$item = 0;
$days = 0;
$paidby = null;
$balance = null;//7.
$interestdue = null;//8.
$fees = null;//9.
$penalty = null;//10.
$loanedInterestAmt = 0.00;//4.
$chargeid = 0;
$HTML = "";
$scheduleddate = null;			//2.
$eStamentArray = '';
$eStamentPayments = ''; 
// -- EOC - Local Variable Declarations : PaymentSchedule
// ----- Payment Allocated Charges ----------------- //
$indexInside = 0;
$ArrayCharges = '';
$dataCharges = '';
// ------ Payment Charges description -------------- //

// Only Require a DataBase if we coming from another Page separately via jQuery or ...
if (!empty($_POST)) 
{  
// -- BOC - $_POST Variable Declarations : PaymentSchedule
$ApplicationId = 0;				//1.
$disbursementdate = null;		//3.
$disbursement = 0.00;			//5.
$repayment = 0.00;
$amountdue = null;//6.
$Duration = 0;//11.
$interest = 0;
$customerid = '';
$customername = '';
$display = '';
}
// Determine if the page comes from a jquery, we must initiate the pos.
else
{
    
}
// -- EOC - $_POST Variable Declarations : PaymentSchedule
 
// -- 1.Duration.
if(isset($_POST['Duration']))
{
	$Duration = $_POST['Duration'];
}

// -- 2.ApplicationId.
if(isset($_POST['ApplicationId']))
{
	$ApplicationId = $_POST['ApplicationId'];
}

// -- 3.First Payment Date.
if(isset($_POST['FirstPaymentDate']))
{	
	$scheduleddate = $_POST['FirstPaymentDate'];
}

// -- 4.Disbursement Amount.
if(isset($_POST['ReqLoadValue']))
{
	$disbursement = $_POST['ReqLoadValue'];
}

// -- 5.Instalment Amount.
if(isset($_POST['monthlypayment']))
{
	$amountdue = $_POST['monthlypayment'];
}

// -- 6.Disbursement Date.
if(isset($_POST['DateAccpt']))
{
   $disbursementdate = $_POST['DateAccpt'];
}

// -- 7.customerid.
if(isset($_POST['customerid']))
{
   $customerid = $_POST['customerid'];
}

// -- 8.customername.
if(isset($_POST['customername']))
{
   $customername = $_POST['customername'];
}

// 9.Loan amount with Interest.
if(isset($_POST['repayment']))
{
   $repayment = $_POST['repayment'];
}

// 10.Display - Payment Schedules
if(isset($_POST['display']))
{
   $display = $_POST['display'];
}


// -- 11.Interest Rate.
if(isset($_POST['interest']))
{
   $interest = $_POST['interest'];
}

/* -- 12. Email the eStament - 23.06.2017
No need to do the check because Runpaymentschedule is an included file.
$emailFlag = '';
if(isset($_POST['email']))
{
   $emailFlag = $_POST['email'];
}*/

// -- Different file names.
$base = '';
$checkDB = 'database.php';
$checkDB2 = 'database_invoice2';
$DoNotDB = '';
$DoNotDB2 = '';
$fileNames = '';

// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
if (!empty($_POST)) 
{   

$included_files = get_included_files();
//require_once 'database_invoice2.php';

foreach ($included_files as $filename) 
{

    if($filename == $checkDB)
	{
	  $DoNotDB = 'yes';
	}
	
	if($filename == $checkDB2)
	{
	 $DoNotDB2 = 'yes';
	}
	
	$fileNames = $fileNames.$filename."\n"; 
}
//echo "<script>alert('Modise')</script>";

	// echo "$fileNames";

//echo "<script>alert($fileNames)</script>";

if(empty($DoNotDB))
{require 'database.php';}

/*if(empty($DoNotDB2))
{require $checkDB2;}
*/

// -- BOC - Local Variable Declarations : PaymentSchedule
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(empty($emailFlag))
{
$HTML = PaymentSchedule($ApplicationId,$disbursementdate,$disbursement,$repayment,$amountdue,$scheduleddate,$Duration, $interest,$customerid,$customername,$display );
echo $HTML;				
			//echo 'Error creating Payment Schedule.';
}
else
{
	$HTML = "";
}
			
}
// -- Create/Display - Payment Schedule(s)
function  PaymentSchedule($ApplicationId,$disbursementdate,$disbursement,$repayment,$amountdue,$scheduleddate,$Duration, $interest,$customerid,$customername,$display )
{
// -- BOC - Local Variable Declarations : PaymentSchedule
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Table name - PaymentSchedule.  
global $tbl_name; 
global $item;
$days = 0;
global $paidby;
global $balance;
global $interestdue;
global $fees;
global $penalty;
global $loanedInterestAmt;
global $chargeid;
global $HTML;
// -- EOC - Local Variable Declarations : PaymentSchedule

global $emailFlag;

		$valid = true;
	 
		//$data = $q->fetch(PDO::FETCH_ASSOC);
		//$ApplicationId = $data['ApplicationId'];

	 	//if ($q->rowCount() >= 1)
		//{$valid = false;}
		// -- Display = 'X' display ONLY, display = space(Display and Create Payment Schedule)
		if(empty($display ))
		{
			// -- Create a Disbursement Entry.
			CreatePaymentSchedules(-1,$ApplicationId,$disbursementdate,$disbursement,'0.00',$repayment);
			
					//echo "valid = ".$valid;

			if ($valid) 
			{	
				for ($i = 0; $i < $Duration; $i++)
				{
					CreatePaymentSchedules($i,$ApplicationId,$scheduleddate,$disbursement,$amountdue,$repayment);
				}
			}
		}
	// -- Display ONLY, must go search for an ApplicationId & customer Details.
		else
		{
		
		// -- $ApplicationId - Get the Payment ID.
				$sql = 'SELECT * FROM loanapp as L WHERE ApplicationId = ?';
				$q = $pdo->prepare($sql);
				$q->execute(array($ApplicationId));	
				$dataLoanApp = $q->fetch(PDO::FETCH_ASSOC); 

				// -- Get Customer Data name.
				$sql = 'SELECT * FROM customer WHERE CustomerId = ?';
				$q = $pdo->prepare($sql);
				$q->execute(array($dataLoanApp['CustomerId']));
				$dataCustomer = $q->fetch(PDO::FETCH_ASSOC); 
				
				$FirstName = $dataCustomer['FirstName'];
				$LastName = $dataCustomer['LastName'];
				$Title = $dataCustomer['Title'];
				
				$customerid = $dataLoanApp['CustomerId'];
				$customername = $Title.' '.$FirstName.' '.$LastName;
				$interest = $dataLoanApp['InterestRate'];
				$Duration = $dataLoanApp['LoanDuration'];
				$disbursement = $dataLoanApp['ReqLoadValue'];
				$repayment = $dataLoanApp['Repayment'];
				$disbursementdate = $dataLoanApp['DateAccpt'];				
		}
					// -- PaymentSchedule Clients Details.
					// -- Client name: , Interest Rate : , Loan Duration : , Report Run Date : , Loan Requested Amount, Loan Repayment 		Amount with Interest:, Date Approved:, Customer ID:, Status: 
					// -- PaymentSchedule - fields.
						$item = 0;
						$repoRunDate = DATE('Y-m-d');
						$days = 0;
						$Totaldays = 0;
						$paidby = '';
						$amountdue = 0;
						$balance = 0;
					// -- Display on Screen.	
						$Displaybalance = 0;
						$DisplayTotalDue = 0;
						$DisplayTotalOutstanding = 0;
						$Displayamountpaid = 0;
						$Displayfees = 0;
						$Displayinterestdue = 0;
						$Displaypenalty = 0;
							
						$interestdue = 0;
						$fees = 0;
						$penalty = 0;
						$valid = true;
						$amountpaid = '0.00';
						$interest = number_format($interest,2);
						
						$TotalOutstanding = '0.00';
						$TotalPaid = '0.00';
					    $TotalDue = '0.00';
						
						// -- Totals.
						$Totaldisbursement = 0;
						$Totalamountdue = 0;
						$Totalbalance = 0;
						$Totalinterestdue = 0;
						$Totalfees = 0;
						$Totalpenalty = 0;
						$Totalamountpaid = 0;
						$TotalsOutstanding = 0;
						$TotalsPaid = 0;
						$TotalsDue = 0.00;
						
						// -- TotalsAmtPaid -- //
						$TotalsAmtPaid = GetPayments($customerid,$ApplicationId,$repayment);
						
						$pdo = Database::connect();
						$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$sql =  "select * from $tbl_name where ApplicationId = ?";
						$q = $pdo->prepare($sql);
						$q->execute(array($ApplicationId));		
						$data = $q->fetchAll();

						// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
						// -- We need not to send an e-mail as attachment.23.06.2017.
							if (!empty($_POST) && empty($emailFlag))						
							{						
							$HTML = '	<link href="assets/css/bootstrap.min.css" rel="stylesheet">

							<link href="media/css/dataTables.bootstrap.min.css" rel="stylesheet">
							<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
							<link rel="stylesheet" type="text/css" href="resources/syntax/shCore.css">
							<link rel="stylesheet" type="text/css" href="resources/demo.css">
							<style type="text/css" class="init">
							div.dataTables_wrapper {margin-bottom: 3em;}
							</style>		
							<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
							</script>
							<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
							</script>
							<script type="text/javascript" language="javascript" src="resources/syntax/shCore.js">
							</script>
							<script type="text/javascript" language="javascript" src="resources/demo.js">
							</script>
							<script type="text/javascript" language="javascript" class="init">
								$(document).ready(function() {
							$("table.display").DataTable();
						} );
							</script>
								<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
										<link rel="stylesheet" href="assets/css/font-awesome.css" rel="stylesheet">

							
						';
												
						$HTML = $HTML.'<div style="overflow-y:auto;" style="z-index:10000;">';
						$HTML = $HTML.'<table class="table table-striped table-bordered" style="white-space: nowrap;">
												<tr><td><b>ID:</b> '.$customerid.'<b> Name:</b> '.$customername.'</td>
												
												<td><b>Interest Rate :</b> '.$interest.'%<b>  Duration :</b> '.$Duration.' <b>Application ID :</b>'.$ApplicationId.'</td>
												</tr>
												<tr><td><b>Report Date :</b> '.$repoRunDate.'<b>  Requested Amount:</b> '.$disbursement.'</td>
												
												<td><b>Repayment Amount:</b> '.$repayment.'<b>  Date Approved:</b> '.$disbursementdate.'</td>
												</tr>
												</table>';
						}
						// -- Get the Charges. 
						else
						{
							// -- Gets the Charge Description.
							GetCharges();
						}

						$RowCount  = 0;
						$IndexCount  = 0;
						$IndexStart  = 0;
						$ArrayLength  = 0;

						$entries = 10;
						$LastRow = $q->rowCount();
						$DoCalculations = 'yes';
						$date1 = '';
						$date2 = '';
					
						foreach ($data as $row) 	
						{
						  $RowCount = $RowCount + 1;
						  
						// -- Print Head and Foot. Every 10 record
						  if (($RowCount % 40) == 0) 
						  {
						 // -- Close Table 
						// -- Totals --
						$TotalsOutstanding = number_format($TotalsOutstanding,2);
						$TotalsDue = number_format($TotalsDue,2);
						$TotalsPaid = number_format($TotalsPaid,2);
						//$Totalbalance = number_format($Totalbalance,2);
						$Totalinterestdue = number_format($Totalinterestdue,2);
						$Totalfees = number_format($Totalfees,2);
						$Totalpenalty = number_format($Totalpenalty,2);
						$Totalamountpaid = number_format($Totalamountpaid,2);
						$Totalamountdue  = number_format($Totalamountdue,2);
						$Totalbalance = number_format($balance,2);
						
						//$TotalsDue = number_format($TotalsDue,2);
						$HTML = $HTML."<tr><td></td><td><b><u>Total</u></b></td><td><b><u>$Totaldays</u></b></td>
						<!--<td></td>--><td><b><u>$Totaldisbursement</u></b></td><td><b><u>$Totalamountdue</u></b></td>
						<td><b><u>$Totalbalance</u></b></td><td><b><u>$Totalinterestdue</u></b></td><td><b><u>$Totalfees</u></b></td>
						
						<td><b><u>$Totalpenalty</u></b></td>
						<td><b><u>$TotalsDue</u></b></td><td><b><u>$TotalsPaid</u></b></td><td><b><u>$TotalsOutstanding</u></b></td></tr>";
							$HTML = $HTML.'</tbody></table>';
							
						 // -- Open a new one	
						  $HTML = $HTML.'
						  <table id="" class="display" cellspacing="0" style="white-space: nowrap;">';

						$HTML = $HTML.'<thead>
						<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
						<th colspan="3">Total Cost of Loan</th></tr> -->';
						$HTML = $HTML.'<tr><th>Item</th><th>Date</th><th>Days</th>
						<!-- <th>Paid by</th> -->
						<th>Disbursement</th>
						<th>Amount Due</th><th>Amount Balance</th><th>Interest Due</th><th>Fees</th><th>Penalties</th>
						<th>Total Due</th><th>Total Paid</th><th>Total Outstanding</th></tr></thead>';

						  }
						// -- First Time Print Head and Foot.
						  if ($RowCount == 1) 
						  {
						  						 // -- Open a new one	
						  $HTML = $HTML.'
						  <table id="" class="display" cellspacing="0" style="white-space: nowrap;">';

							// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
							// -- We need not to send an e-mail as attachment.23.06.2017.
							if (!empty($_POST) && empty($emailFlag)) 
							{	  
								$HTML = $HTML.'<thead>
								<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
								<th colspan="3">Total Cost of Loan</th></tr> -->';
								
								$HTML = $HTML.'<tr><th>#</th>
												   <th>Date</th>
												   <th>Days</th>
												   <th>Disbursement</th>
												   <th>Amount Due</th>
												   <th>Amount Balance</th>
												   <th>Interest Due</th>
												   <th>Fees</th>
												   <th>Penalties</th>
												   <th>Total Due</th>
												   <th>Total Paid</th>
												   <th>Total Outstanding</th>
											  </tr></thead>';
							}
							// -- This for eInvoice ONLY, remove : Item,Acount Balance.
							else
							{
								$HTML = $HTML.'<thead>
								<!-- <tr><td colspan="4"></td><th colspan="3">Loan Amount and Balance</th>
								<th colspan="3">Total Cost of Loan</th></tr> -->';
								
								$HTML = $HTML.'<tr>
												   <th>Date</th>
												   <th colspan="2">Description</th>
												   <th>Transaction Value</th>
												   <th align="right">Balance</th>
												   <!--<th>Interest Due</th>
												   <th>Fees</th>
												   <th>Penalties</th>
												   <th>Total Due</th>
												   <th>Total Paid</th>
												   <th>Total Outstanding</th> -->
												</tr></thead>';
							}
						  }
						
						$item = $row['item']; $scheduleddate = $row['scheduleddate'];$days = $row['days'];
						$paidby = $row['paidby'];$disbursement = $row['disbursement'];$amountdue = $row['amountdue'];
						//$balance = $row['balance'];
						
						$interestdue = $row['interestdue'];$fees = $row['fees'];$penalty = $row['penalty'];
						$amountpaid = $row['amountpaid'];
						// -- if its Disbursement Entry.
						if($item < 0)
						{
						   $TotalOutstanding = $fees;
						   $TotalsOutstanding = $TotalOutstanding + $TotalsOutstanding;
						   $Totaldisbursement = $disbursement;
						   
						   $disbursement = number_format($disbursement,2);
						   $fees = number_format($fees,2);
						   $TotalsOutstanding = number_format($TotalsOutstanding,2);
						   $Totaldisbursement = number_format($Totaldisbursement,2);
						   // -- Fee : Insurance Fee.
						   $chargeid = 1; // -- Will Change once Implemented the UI.
						   $fees = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
						   $fees = number_format($fees,2);

						   // -- $interestdue + 
						   // -- Blank fields not part of disbursement.
						   $amountdue = '';
						   $balance = '';
						   $penalty = '';
						   $amountpaid = 0.00;
						   $TotalDue = '';
						   
						   $Displayfees = number_format($fees,2);

						   // -- Adjust the Fees Depending on the paid amount.
						   if ($fees <= $TotalsAmtPaid)// && $fees != 0)
							{
							   $amountpaid = $fees; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							   $fees  = 0.00; // -- This has been paid.
							}
							elseif($TotalsAmtPaid >0)
							{
							   $amountpaid = $TotalsAmtPaid; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							   $fees  = $fees - $TotalsAmtPaid; // -- This has been paid.
							}
							
						   $days = '';
						   $interestdue = '';
						   
						    $TotalOutstanding  = $fees;
							$TotalsOutstanding = $TotalOutstanding;
						   
						   	// -- $DisplayTotalDue	 = number_format($TotalDue,2);
							$DisplayTotalOutstanding = number_format($TotalOutstanding,2);
							$Displayamountpaid = number_format($amountpaid,2);
							
							// -- Total Fees.
							$Totalfees =  $fees;
						}
						// -- For all payment schedule.
						else
						{
							// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
							if (empty($_POST)) 
							{
							  // -- Do Calculations 
							   $DoCalculations = DoCalculations($scheduleddate);
							} 
							
						// -- Do the Calculations.	
						//if(!empty($DoCalculations))
						 {		
							$IndexCount = $IndexCount + 1;
						    $IndexStart = $IndexCount;
						    $ArrayLength = 0;
						  
						  // -- PaymentDaysOverDue
							$days = PaymentDaysOverDue($scheduleddate);
							$Totaldays = $Totaldays + $days;
							
							// -- Fee : Insurance Fee.
							$chargeid = 4; // -- Will Change once Implemented the UI.
							$fees = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
							// -- Interest Due : Bounced Fee.
							$chargeid = 3; // -- Will Change once Implemented the UI.
							$interestdue = Fee($ApplicationId,$chargeid,$item);//'42.00';
							
							// -- Penalties : Over on Instalment.
							$chargeid = 2; // -- Will Change once Implemented the UI.
							$penalty = OverDuePenalty($days,$ApplicationId,$chargeid, $item);
			
							$balance = $balance - $amountdue;
// -- BOC - Reconcile(decimal place values) : The Balance Due and Amount Due -- 02.07.2017 -- //							
							if($LastRow == $RowCount)
							{
								// -- At this stage Balance must be zero.
								if($balance != 0)
								{

								// -- if its positive we add on the amount Due.
								  if($balance > 0)
								  {
								  	// -- echo "<script>alert($amountdue)</script>";

								    $amountdue = $amountdue + $balance;
									$balance = 0.00;
									
									// -- echo "<script>alert($amountdue)</script>";

								  }
								// -- if its negative we subtract from the amount Due.
								  if($balance < 0)
								  {

								    $amountdue = $amountdue + $balance;
								    $balance = 0.00;

								  }
								}
							}
							
							// -- Populate the Payment Schedule.	
							$TotalDue =  $amountdue + $fees +  $penalty + $interestdue;
							
						   // -- Total Amount Due	
							$Totalamountdue = $Totalamountdue + $amountdue;
							
							// -- 
							$TotalsDue = $TotalDue + $TotalsDue;
							
// -- EOC - Reconcile(decimal place values) : The Balance Due and Amount Due -- 02.07.2017 -- //
							// -- Amount Paid.
							$amountpaid = 0;
							
							 //echo 'TotalDue : '.$TotalDue.' ; Total : '.$TotalsAmtPaid;

							if ($TotalDue <= $TotalsAmtPaid && $TotalDue != 0)
							{
							   $amountpaid = $TotalDue; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							}
							elseif($TotalsAmtPaid >0)
							{
							   $amountpaid = $TotalsAmtPaid; 
							   $TotalsAmtPaid = $TotalsAmtPaid - $amountpaid;
							   $TotalsPaid = $TotalsPaid + $amountpaid;
							}
							
							//echo 'Paid : '.$amountpaid.' ; Total : '.$TotalsAmtPaid;

							$TotalOutstanding  = ($TotalDue -  $amountpaid);
							$TotalsOutstanding = $TotalsOutstanding + $TotalOutstanding;
							
							// -- Total Fees.
							$Totalfees =  $Totalfees + $fees;
							$Totalpenalty = $Totalpenalty + $penalty;
							$Totalinterestdue = $Totalinterestdue + $interestdue;

						    // -- Blank fields not part of paymentschedule.
							$disbursement = '';
							$Displaybalance = number_format($balance,2);
							$DisplayTotalDue	 = number_format($TotalDue,2);
							$DisplayTotalOutstanding = number_format($TotalOutstanding,2);
							$Displayamountpaid = number_format($amountpaid,2);
							$Displayfees = number_format($fees,2);
							$Displayinterestdue = number_format($interestdue,2);
							$Displaypenalty = number_format($penalty,2);
						 }
						}
						
						// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
						// -- We need not to send an e-mail as attachment.23.06.2017.
							if (!empty($_POST) && empty($emailFlag)) 
							{
								$HTML = $HTML."
										<tr>
											<td>$item</td>
											<td>$scheduleddate</td>
											<td>$days</td>
											<td>$disbursement</td>
											<td>$amountdue</td>
											<td>$Displaybalance</td>
											<td>$Displayinterestdue</td>
											<td>$Displayfees</td>
											<td>$Displaypenalty</td>
											<td>$DisplayTotalDue</td>
											<td>$Displayamountpaid</td>
											<td>$DisplayTotalOutstanding</td>
										</tr>";
							}
						// -- This for eInvoice ONLY, remove : Item,Acount Balance.
							else
							{
							
							// -- Do the Calculations.	
								//if(!empty($DoCalculations))
								{
								
								 
								 // -- if its Disbursement Entry.
								 if($item < 0)
								 {
								 // -- Build and eStamentArray - Date[0], Description[1],Transaction Value[2],Outstanding Balance[3] 
								    $eStamentArray[$IndexCount][0] = $scheduleddate; // -- Date[0]
									$eStamentArray[$IndexCount][1] = 'Opening Balance';   // -- Description[1]
									$eStamentArray[$IndexCount][3] = $repayment; // -- Outstanding Balance[3]
									$eStamentArray[$IndexCount][2] = $amountdue; // -- Transaction Value[2]
								 }
								 else
								 {
								 // -- Build and eStamentArray - Date[0], Description[1],Transaction Value[2],Outstanding Balance[3] 
								    $eStamentArray[$IndexCount][0] = $scheduleddate; // -- Date[0]
								    $eStamentArray[$IndexCount][2] = $amountdue; // -- Transaction Value[2]
									$eStamentArray[$IndexCount][1] = 'Instalment';   // -- Description[1]
									$eStamentArray[$IndexCount][3] = $DisplayTotalOutstanding; // -- Outstanding Balance[3]
								 }
								 
								// -- Interest is not initial display it.
								if(!empty($Displayinterestdue))
								{
								   	if($Displayinterestdue > 0)
									{   // -- ChargeID = 3
										$IndexCount = $IndexCount+1;
										$eStamentArray[$IndexCount][0] = $scheduleddate; // -- Date[0]
										$eStamentArray[$IndexCount][1] = GetCharge(3);//'Interest Due';   // -- Description[1]
										$eStamentArray[$IndexCount][2] = $Displayinterestdue; // -- Transaction Value[2]
										$eStamentArray[$IndexCount][3] = ''; // -- Outstanding Balance[3]	
									}
								}
										// -- Fees is not blank display it.
								if(!empty($Displayfees))
								{
								   if($Displayfees > 0)
								    {// -- ChargeID = 4
										$IndexCount = $IndexCount+1;
										$eStamentArray[$IndexCount][0] = $scheduleddate; 	  // -- Date[0]	
										if($item >= 0)
										{
										  $eStamentArray[$IndexCount][1] = GetCharge(4);//'Fees';
										  $eStamentArray[$IndexCount][3] = ''; // -- Outstanding Balance[3]	
										}   			  // -- Description[1]
										else
										{ 
									 // -- ChargeID = 1	
										  $eStamentArray[$IndexCount][1] = GetCharge(1);//'Immediate disbursement fee';
										  $eStamentArray[$IndexCount][3] = ''; // -- Outstanding Balance[3]	
										}
										
										$eStamentArray[$IndexCount][2] = $Displayfees; // -- Transaction Value[2]
									}									
								}
								// -- Penalty is not blank display it.
								if(!empty($Displaypenalty))
								{
									if($Displaypenalty > 0)
									{// -- ChargeID = 2
										$IndexCount = $IndexCount+1;
										$eStamentArray[$IndexCount][0] = $scheduleddate; // -- Date[0]									
										$eStamentArray[$IndexCount][1] = GetCharge(2);//'Penalties';   // -- Description[1]
										$eStamentArray[$IndexCount][2] = $Displaypenalty; // -- Transaction Value[2]
										$eStamentArray[$IndexCount][3] = ''; // -- Outstanding Balance[3]	
									}
								}
								
								// -- Amount Paid is not blank display it.
								if(!empty($Displayamountpaid) && $Displayamountpaid != '0.00')
								{
								    $IndexCount = $IndexCount+1;
									$eStamentArray[$IndexCount][0] = $scheduleddate; // -- Date[0]									
									$eStamentArray[$IndexCount][1] = 'Payment Allocated';   // -- Description[1]
									$eStamentArray[$IndexCount][2] = $Displayamountpaid; // -- Transaction Value[2]
									$eStamentArray[$IndexCount][3] = $DisplayTotalOutstanding; // -- Outstanding Balance[3]		
								}
								
								
								}
							}
						// -- Total Repayment Amount with Interest Rate.
							if($item < 0)
							{$balance = $repayment;}
							
						// -- Last Row - Please close.
						if($LastRow == $RowCount)
						{
						// -- BOC - Reconcile(decimal place values) : The Balance Due and Amount Due -- 02.07.2017 -- //
						
						// -- EOC - Reconcile(decimal place values) : The Balance Due and Amount Due -- 02.07.2017 -- 
						// -- Totals --
						$TotalsOutstanding = number_format($TotalsOutstanding,2);
						$TotalsDue = number_format($TotalsDue,2);
						$TotalsPaid = number_format($TotalsPaid,2);
						//$Totalbalance = number_format($Totalbalance,2);
						$Totalinterestdue = number_format($Totalinterestdue,2);
						$Totalfees = number_format($Totalfees,2);
						$Totalpenalty = number_format($Totalpenalty,2);
						$Totalamountpaid = number_format($Totalamountpaid,2);
						$Totalamountdue  = number_format($Totalamountdue,2);
						$Totalbalance = number_format($balance,2);
						
					// Only Require a DataBase if we coming from another Page separatly via jQuery or ...
					// -- We need not to send an e-mail as attachment.23.06.2017.
							if (!empty($_POST) && empty($emailFlag)) 
							{
								$HTML = $HTML."<tr><td></td><td><b><u>Total</u></b></td><td><b><u>$Totaldays</u></b></td>
								<!--<td></td>--><td><b><u>$Totaldisbursement</u></b></td><td><b><u>$Totalamountdue</u></b></td>
								<td><b><u>$Totalbalance</u></b></td><td><b><u>$Totalinterestdue</u></b></td><td><b><u>$Totalfees</u></b></td>
								
								<td><b><u>$Totalpenalty</u></b></td>
								<td><b><u>$TotalsDue</u></b></td><td><b><u>$TotalsPaid</u></b></td><td><b><u>$TotalsOutstanding</u></b></td></tr>";
									$HTML = $HTML.'</tbody></table>';
							}
					// -- This for eInvoice ONLY, remove : Item,Acount Balance.		
							else
							{
							// --------------------------- Print Invoices ------------------------------- //
							// -- merge paymentschedule and payments array.
								 Global $eStamentPayments;
							
							// -- Only Merge eStamentPayments if there was payments.
								 if(!empty($eStamentPayments))
							     {$eStamentArray = array_merge($eStamentArray,$eStamentPayments);}
								 
							// -- Remove Instalment from the row - Instalment.
								$eStamentArray = removeElementWithValue($eStamentArray, 1, "Instalment");
							
							// -- Remove Instalment from the row - Payment Allocated.
								$eStamentArray = removeElementWithValue($eStamentArray, 1, "Payment Allocated");							
							
							// -- Sort Ascending - array based on the date."CMP" is function to do sorting.
								 usort($eStamentArray, "cmp");
								 
								 $IndexStart = 0;
								 
							// -- Print all row per instalment.
								 $ArrayLength = count($eStamentArray);
						  
								// -- Print all row per instalment.
									for($j=$IndexStart;$IndexStart<$ArrayLength;$IndexStart++)
									{
										$DATEStatement = $eStamentArray[$IndexStart][0];
										$DescriptionStatement = $eStamentArray[$IndexStart][1];
										$TransactionStatement = $eStamentArray[$IndexStart][2];
										$OutstandingStatement = $eStamentArray[$IndexStart][3];
							
										$HTML = $HTML.'
											<tr>
												<td>'.$DATEStatement.'</td>
												<td colspan="2">'.$DescriptionStatement.'</td>
												<td align="right">'.$TransactionStatement.'</td>
												<td align="right">'.$OutstandingStatement.'</td>	
												
												<!-- <td>$days</td>
												<td>$disbursement</td>
												<td>$amountdue</td>
												<td>$Displayinterestdue</td>
												<td>$Displayfees</td>
												<td>$Displaypenalty</td>
												<td>$DisplayTotalDue</td>
												<td>$Displayamountpaid</td>
												<td>$DisplayTotalOutstanding</td> -->
											</tr>';	
									}
							// --------------------------- EOC Print Invoices --------------------------- //
							
								// -- $TotalsOutstanding = $TotalsOutstanding + $repayment;
								$Now = date('Y-m-d');
								$HTML = $HTML."<tr><!--<td><b><u>Total</u></b></td><td><b><u>$Totaldays</u></b></td>
												   <td><b><u>$Totaldisbursement</u></b></td>
												   <td><b><u>$Totalamountdue</u></b></td>
												   <td><b><u>$Totalinterestdue</u></b></td>
												   <td><b><u>$Totalfees</u></b></td>
												   <td><b><u>$Totalpenalty</u></b></td>
												   <td><b><u>$TotalsDue</u></b></td>
												   <td><b><u>$TotalsPaid</u></b></td>-->
												   
												   <td>$Now</td>
												   <td>Closing balance</td>".
												   '<td colspan="2"></td>
												    <td align="right">'.$TotalsOutstanding."</td>
											   </tr>";
									$HTML = $HTML.'</tbody></table>';
							}
						}
							
						}
						
					$HTML = $HTML.'</div>';
return $HTML;
} 

// -- Remove the Instalment - row from a multideminsional array. 
function removeElementWithValue($array, $key, $value)
{
     foreach($array as $subKey => $subArray){
          if($subArray[$key] == $value){
               unset($array[$subKey]);
          }
     }
     return $array;
}

// -- Determine if the installment date is inline with current date 
// -- if so do calculation for the invoice else do not calculate for invoice.
function DoCalculations($scheduleddate)
{
	$doCalculate = '';
	$date1=date_create($scheduleddate);// first day
	$date2=date_create(date('Y-m-d'));// second day
	$diff=date_diff($date1,$date2);
	$value = 0;
	
	$value = $diff->format("%R%a");

	if($value > 0)
	{
	  $doCalculate = 'Yes';
	}
	
	return $doCalculate;
}

// -- Sort Array with Date which is the first array value.
function cmp($a, $b)
{
    $a = strtotime($a[0]); // -- DATE[0]
    $b = strtotime($b[0]); // -- DATE[0]

    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

// -- Create - PaymentSchedules.
function CreatePaymentSchedules($i,$ApplicationId,$scheduleddate,$disbursement,$amountdue,$repayment)
{
// -- PaymentSchedule - fields.
	$item = '0.00';
	$days = '0.00';
	$paidby = '';
	$balance = '0.00';
	$interestdue = '0.00';
	$fees = '0.00';
	$penalty = '0.00';
	$tbl_name ="paymentschedule"; 
	$amountpaid = '0.00';
	$TotalOutstanding = '0.00';
	$TotalPaid = '0.00';
	$chargeid = 0;
	
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// if its 
	// -- Get the Next Payment Next Schedule Due Date
			   if($i >= 0)
			   {
			    // -- First Item DO NOT go get next payment date.
				if($i != 0)
				 {
				   global $scheduleddate;
			       $scheduleddate = PaymentNextScheduleDue($scheduleddate);
				 }

				$disbursement =  0.00;
				 
				 // -- PaymentDaysOverDue
				$days = PaymentDaysOverDue($scheduleddate);
				 
				// -- Fee : Insurance Fee.
				$chargeid = 4; // -- Will Change once Implemented the UI.
				$fees = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Interest Due : Bounced Fee.
				$chargeid = 3; // -- Will Change once Implemented the UI.
				$interestdue = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Penalties : Over on Instalment.
				$chargeid = 2; // -- Will Change once Implemented the UI.
				$penalty = OverDuePenalty($days,$ApplicationId,$chargeid, $i);

			   }
			   // -- Disbursement Entry. Determine the Interest Due le Fees
			   else
			   {
				// -- Interest Due Loan Repayment Amount - Loaned Amount.
				//$interestdue = $repayment - $disbursement;
				
				// -- If Immediate Tranfer was requested.
				$chargeid = 1; // -- Will Change once Implemented the UI.
				$fees = Fee($ApplicationId,$chargeid,$i);//'42.00';
				
				// -- Determine if the Disbursement Immediate Disbursement Fee is Charged.
				
				// -- Blank fields that wont be filled.				
			   }
				if(PaymentScheduleExit($ApplicationId,$i) < 1 )
				{
					$item = $i;
					
					$sql = "INSERT INTO $tbl_name
					(ApplicationId,item,scheduleddate,days,paidby,disbursement,amountdue,balance,interestdue,fees,penalty,amountpaid)
					VALUES
					(?,?,?,?,?,?,?,?,?,?,?,?)";
				
					$q = $pdo->prepare($sql);
					$q->execute(array($ApplicationId,$item,$scheduleddate,$days,$paidby,$disbursement,$amountdue,$balance,$interestdue,$fees,$penalty,$amountpaid));
	
					Database::disconnect();
				}
} 

// -- Get the Payment Schedule.
function PaymentScheduleExit($AppID, $Item)
{
	
		$tbl_name ="paymentschedule"; 

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql =  "select * from $tbl_name where ApplicationId = ? AND item = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$Item));
		
		if ($q->rowCount() >= 1)
		{ 
		  return 1;
		}
		else
		{
		  return 0;		
		}
}
// ----------------------------------------------------------------------------------------- //
//								Penalties AND Fees, Interest								 // 
// ----------------------------------------------------------------------------------------- //
// -- Disbursement Fee,Insurance Fee.
function Fee($AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 

		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		if ($q->rowCount() >= 1)
		{ 
		// -- Return the Fee.
		  return $dataDisbursementFee['amount'];
		}
		else
		{
		  return 0;		
		}
}

// -- OverDue Penalties.
function OverDuePenalty($OverDays,$AppID,$chargeid, $Item)
{
		$tbl_loanappcharges ="loanappcharges"; 
		$tbl_charge ="charge"; 
		$minDays = 0;
		$maxDays = 0;
		$Penalty = '0.00';
		$graceperiod = 0.00;
		$arreas = 1;
		
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM $tbl_loanappcharges as A INNER JOIN $tbl_charge as B 
				ON  A.chargeid = B.chargeid  where applicationid = ? AND A.chargeid = ? AND item = ? AND active = 'X'";
		$q = $pdo->prepare($sql);
		$q->execute(array($AppID,$chargeid,$Item));
		$dataDisbursementFee = $q->fetch(PDO::FETCH_ASSOC);

		
		if ($q->rowCount() >= 1)
		{ 
		
			// -- OverDue days must be in range.
			$minDays = $dataDisbursementFee['min'];
			$maxDays = $dataDisbursementFee['max'];
			$Penalty =  $dataDisbursementFee['amount'];
			$graceperiod = $dataDisbursementFee['graceperiod'];
			
			// -- OverDue days must be in between range.
			if($OverDays >= $minDays AND $OverDays > 0)
			{
			// -- Get the start days to calculate arreas.
				$OverDays = $OverDays - $minDays;
			
			// -- Maximum - days.
				if($OverDays >= $maxDays)
				{
				  $OverDays = $maxDays;
				}
				
			// -- if there is grace period.
			   if($graceperiod > 0 AND $graceperiod <= $OverDays)
			   { $OverDays = $OverDays - $graceperiod; }
			   elseif($graceperiod > $OverDays)
			   { $OverDays = 0;} 
			   
			// -- ensure we do not catch exception multiple by 0.   
			   if($OverDays > 0)
			   {
				$Penalty = $Penalty * $OverDays;
			   }
			   else
			   {
			     $Penalty = 0.00; 
			   }
			}
			else
			{
				$Penalty = 0.00;
			}
			
			// -- Return the Fee.
			  return $Penalty;
		}
		else
		{
		  return '0.00';		
		}
}

// ----------------------------------------------------------------------------------------- //
//							End-Penalties AND Fees, Interest								 // 
// ----------------------------------------------------------------------------------------- //
function PaymentDaysOverDue($PaymentDate)
{

	$date1=date_create($PaymentDate);// first day
	$date2=date_create(date('Y-m-d'));// second day
	$diff=date_diff($date1,$date2);


	$value = $diff->format("%R%a");

	if($value < 0)
	{
	   $value  = 0;
	}
	else
	{$value = substr($value,1);}

	$daysOverDue = $value;

	return $daysOverDue;
}


// -- Get the Next Payment Schedule Due Date.
function PaymentNextScheduleDue($FirstPaymentDate)
{
	$day = substr($FirstPaymentDate,-2);
	$month = substr($FirstPaymentDate,5,2) + 1;
	$year = substr($FirstPaymentDate,0,4);
	$PaymentDueDate = '';
	$daysInMonth = 0;
	global $days;
	
	// -- Get the current day, month, year.
	$cyear = date('Y');
	$cmonth = date('m');
	$cday = date('d');

	// -- if the Next Month is greater than 12, we going to the next year. 
	if($month > 12)
	{
	  $year = $year + 1;
	  $month = 1;
	}
	
	// -- Get the days in a month.
	$daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year );
	
	// -- Ensure the days dont get confused.
	if($day > $daysInMonth)
	{
	  $day = $daysInMonth;
	}
	
	// -- if the month is less than 10 add 0.
	if($month < 10)
	{
	   $month = "0".$month;
	}

	$PaymentDueDate = $year.'-'.$month.'-'.$day;
	
	global $scheduleddate;
	$scheduleddate = $PaymentDueDate;
	
	return $PaymentDueDate;
}
// ------------------------------------------------- //
// -- 				Get the Payments
// -- 				20.05.2017 .
// -- IMS Data System
// ------------------------------------------------ //
function GetPayments($customerid,$ApplicationId,$repaymentAmount)
{
// -- Global eStamentPayments variable.
Global $eStamentPayments;

//require 'database.php';
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
$tot_outstanding = 0;
$tot_paid = 0; 

// ----------------------------------------------------------------------------------- //
// 					IMS Amount to be paid and Still to be paid.						   //
// ----------------------------------------------------------------------------------- //				 
			   // -- Get Payment Invoices from common Tab Data.
				$sql =  "select * from common where customer_identification = ?";
				$q = $pdo->prepare($sql);
				$q->execute(array($customerid));
				$dataCommon = $q->fetchAll(PDO::FETCH_ASSOC);
					 
			    $data = null;
				$IntCount = 0;
				
			   // $dataCommon brings all the Invoices related to a client.
			   foreach($dataCommon as $rowCommon)
			   {
			        // -- Index Count.
					 $invoice_id = $rowCommon['id'];
					 
 					 // -- Get Payment Details Data.
					  $sql =  "select * from payment where invoice_id = ? AND notes = ?";
					  $q = $pdo->prepare($sql);
					  $q->execute(array($invoice_id ,$ApplicationId));
					  $data = $q->fetchAll(PDO::FETCH_ASSOC);
					  
				// For each Invoice Get the Payments related to the Loan Application Id			 
					 foreach ($data as $row) 
					  {
					  	$IntCount = $IntCount + 1;
					    $paymentid = $row['id'];
					    $iddocument = $row['FILE'];
					    $path=str_replace(" ", '%20', $iddocument);
						$tot_paid = $tot_paid + $row['amount'];

						// -- Prepare & copy data to the array.
						$eStamentPayments[$IntCount][0] = $row['date'];// -- Date[0]
						$eStamentPayments[$IntCount][1] = 'Payment received -Thank you';// -- Description[1]
						// ensure negative amount we do not add negative sing in front.
						if($row['amount']> 0)
						{
						  $eStamentPayments[$IntCount][2] = '- '. number_format($row['amount'],2);// -- Transaction Value[2]
						}
						else
						{
						  $eStamentPayments[$IntCount][2] = number_format($row['amount'],2);// -- Transaction Value[2]
						}
						$eStamentPayments[$IntCount][3] = '';// -- Outstanding value [3]						
					   }
					   		  
				}	// End all Customer Invoices
				// -- Get the curreny 
			    // -- $currency = $_SESSION['currency'];
			
			  if($data != null)
				{
					     $tot_outstanding = $repaymentAmount - $tot_paid;
					    /*echo '<table class="table">';
						echo "<tr class='bg-danger'><td><b>Total Repayment Amount</b></td><td><b>$currency".number_format($repaymentAmount,2)."<b></td></tr>";
					    echo "<tr class='bg-success'><td><b>Total Paid Amount</b></td><td><b>$currency".number_format($tot_paid,2)."</b></td></tr>";
						echo "<tr  class='bg-info'><td><b>Total Outstanding Amount</b></td><td><b>$currency".number_format($tot_outstanding,2)."</b></td></tr>";
						echo '</table>';*/
				}
							   Database::disconnect();
			  return $tot_paid;
			  
// ----------------------------------------------------------------------------------- //
// 					END - IMS Amount to be paid and Still to be paid.				   //
// ----------------------------------------------------------------------------------- //	
}

// -- Ge the Charges Descriptions
function GetCharges()
{
   // ----- Global - Payment Allocated Charges, description ----------------- //
	$indexInside = 0;
	$dataCharges = '';
	global $ArrayCharges;

	// -- Database Connections.
	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

	// --------------------- Charges ------------------- //
	$sql = 'SELECT * FROM charge';
	$dataCharges = $pdo->query($sql);						   		

	foreach($dataCharges as $rowCharge)
	{
		  $ArrayCharges [$indexInside][0] = $rowCharge['chargeid'];
		  $ArrayCharges [$indexInside][1] = $rowCharge['chargename'];
		  $indexInside = $indexInside + 1;
	}	
}

// -- Get the charge description(via chargeid)
function GetCharge($chargeid)
{
  // -- Global Charges.
	global $ArrayCharges; 
	
  // -- Charge description.
  $ChargeName = '';
  
  // -- Get the Charge Name
	for ($j=0;$j<sizeof($ArrayCharges);$j++)
	{
		if($chargeid == $ArrayCharges[$j][0])
		{
		   $ChargeName =$ArrayCharges[$j][1];
		}
	}

  return $ChargeName;	
}


?>