<?php 
require 'database.php';
$dbname = "eloan";
// -- Database Declarations and config:  
$path = $_POST['path'];
$backupfilename = $_POST['backupfilename'];

restore_db($dbname,$backupfilename);

/* Restore db from a backup file OR just a table */
function restore_db($dbname,$backupfilename)
{
	try
	{
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$tableName = '';
		
		// -- Read the SQL Backup file to Restore.
		$restorefile = fopen($backupfilename, "r") or die("Unable to open file!");
		$sql = fread($restorefile,filesize($backupfilename));
		fclose($restorefile);
		$q = $pdo->prepare($sql);
		$q->execute();	
		echo 'Database '.$dbname.' was restored successfully. On '.date('Y-m-d');
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}	
}
?>