<?php
////////////////////////////////////////////////////////////////////////
if(!function_exists('getbaseurl'))
{
	function getbaseurl()
	{
		$url = '';
		// -- Require https
			if ($_SERVER['HTTPS'] != "on")
			{
				$url = "https://";
			}
			else
			{
				$url = "https://";
			}

			$url2 = $_SERVER['REQUEST_URI']; //returns the current URL
			$parts = explode('/',$url2);
			$dir = $_SERVER['SERVER_NAME'];
			for ($i = 0; $i < count($parts) - 1; $i++) {
			 $dir .= $parts[$i] . "/";
			}
			return $url.$dir;
	}
}
if(!function_exists('getprotocol'))
{
	function getprotocol()
    {
		$url = '';
		if ($_SERVER['HTTPS'] != "on")
			{
				$url = "http://";
			}
			else
			{
				$url = "http://";
			}
		return $url;
	}
}
// -- Database connection -- //
if(!function_exists('filerootpath'))
{
	function filerootpath()
	{
		 return $_SERVER["DOCUMENT_ROOT"].dirname($_SERVER["PHP_SELF"]);
	}
}
$filerootpath = str_replace('content','',filerootpath());
require  $filerootpath.'/database.php';
////////////////////////////////////////////////////////////////////////
// -----------------| Get the Total Account Balance |-----------------//
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_all_paid'))
{
	function get_all_paid($dataFilter)
	{
		$srUserCode = '';
		if(!empty($dataFilter['IntUserCode']))
		{$srUserCode = 'benefiaryusercode = ? AND';}
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl = 'payments';
		$sqlHeader = "SELECT benefiaryusercode as IntUserCode,SUM(Amount) as Total,'TotalPaid' as Description FROM $tbl";
		$sql = $sqlHeader." WHERE $srUserCode Status_Description NOT in ('pending','Stopped Payment','Deactivated')";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['IntUserCode']));
		$data = $q->fetchAll(PDO::FETCH_ASSOC);
	    return $data;
	}
}
if(!function_exists('get_all_unpaid'))
{
	function get_all_unpaid($dataFilter)
	{
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$data = null;
		$tbl_2  = 'collection';
		$sqlHeader = "SELECT IntUserCode,SUM(Amount) as Total,'TotalUNPaid' as Description FROM $tbl_2";

		/* All Reconcilaition for eCashMeUp Collection Cloud Solution */
		$sql = $sqlHeader." WHERE IntUserCode = ? AND
		Status_Description NOT in ('pending','Submitted','Stopped Payment','Deactivated','Transaction Successful')";
		$q = $pdo->prepare($sql);
		$q->execute(array($dataFilter['IntUserCode']));
		$data = $q->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}
}
if(!function_exists('get_all_retainers'))
{
	function get_all_retainers($dataFilter)
	{

	}
}
if(!function_exists('get_all_income'))
{
	function get_all_income($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'collection';
			$sqlHeader = "SELECT IntUserCode,SUM(Amount) as Total,'TotalIncome' as Description FROM $tbl_2";

			/* All Reconcilaition for eCashMeUp Collection Cloud Solution*/
			$sql = $sqlHeader." WHERE IntUserCode = ? AND
			Status_Description NOT in ('pending','Stopped Payment','Deactivated')";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['IntUserCode']));
			$data = $q->fetchAll(PDO::FETCH_ASSOC);
			return $data;
	}
}

if(!function_exists('get_all_unsettled'))
{
	function get_all_unsettled($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'collection';
			$sqlHeader = "SELECT IntUserCode,SUM(Amount) as Total,'TotalIncome' as Description FROM $tbl_2";

			/* All Reconcilaition for eCashMeUp Collection Cloud Solution*/
			$sql = $sqlHeader." WHERE IntUserCode = ? AND
			Status_Description in ('Submitted')";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['IntUserCode']));
			$data = $q->fetchAll(PDO::FETCH_ASSOC);
			return $data;
	}
}

if(!function_exists('get_all_charges'))
{
	function get_all_charges($dataFilter)
	{
		//--['UserCode'][Total][Description]
		$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data      = null;
			$dataTemp  = null;
			$tbl       = 'client_retainer';
			$sqlHeader = "select IntUserCode,invoices from $tbl";
			$sql = $sqlHeader." WHERE IntUserCode = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['IntUserCode']));
			$data = $q->fetchAll(PDO::FETCH_ASSOC);
			$dataInvoice[] = get_invoice_reconcilations($dataFilter,$data);
			return $dataInvoice;
	}
}
if(!function_exists('get_all_client'))
{
	function get_available_balance_all_client($dataFilter)
	{

	}
}
////////////////////////////////////////////////////////////////////////
if(!function_exists('get_collection_reconcilations'))
{
	function get_collection_reconcilations($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl_2  = 'collection';
			$sqlHeader = "SELECT DATE_FORMAT(Action_Date, '%Y-%m') as Month,DATE(Action_Date) DateOnly,SUM(Amount) as TotalAmount,UserCode,IntUserCode,Status_Description,Status_Code,0.00 as TotalPayOut,0.00 as holdvalue
, '' as percentage,0.00 as charges FROM collection";

			if($dataFilter['role'] == 'admin')
			{
				$srUserCode = '';
				if(!empty($dataFilter['srUserCode']))
				{$srUserCode = 'IntUserCode = ? AND';}
/* All Reconcilaition for eCashMeUp Collection Cloud Solution*/
$sql = $sqlHeader." WHERE $srUserCode DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ? and Status_Description NOT in ('pending','Stopped Payment','Deactivated')
GROUP BY Month,Status_Description,IntUserCode,DateOnly";
				$q = $pdo->prepare($sql);
				if(empty($srUserCode))
				{
					$q->execute(array($dataFilter['month'],$dataFilter['tomonth']));
				}
				else
				{
					$q->execute(array($dataFilter['srUserCode'],$dataFilter['month'],$dataFilter['tomonth']));
				}
				$q->execute();
			}
			else
			{
/* All Reconcilaition per Imnternal User Code */
$sql = $sqlHeader." WHERE IntUserCode = ? AND DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ?  and Status_Description NOT in ('pending','Stopped Payment','Deactivated')
GROUP BY Month,Status_Description,IntUserCode,DateOnly";
				$q = $pdo->prepare($sql);
				$q->execute(array($dataFilter['IntUserCode'],$dataFilter['month'],$dataFilter['tomonth']));
			}

			$data = $q->fetchAll(PDO::FETCH_ASSOC);
//echo 	$sql."<br/>";
//ECHO $dataFilter['role'];
			return $data;
	}
}

// -- IMS Invoices
// -- Invoice Reconcilations
if(!function_exists('get_invoice_reconcilations'))
{
	function get_invoice_reconcilations($dataFilter,$retainer)
	{
		$data = null;
		$invoicesArr = null;
		$WebServiceURL = '';
		$sum = 0;
		$id = '';
		//print_r($retainer);
		foreach($retainer as $row)
		{
			$invoicesArr = explode(",", $row['invoices']);
			for($i=0;$i<count($invoicesArr);$i++)
			{
				if(strtolower($_SERVER['SERVER_NAME']) == 'localhost')
				{
				   $WebServiceURL = getprotocol()."localhost/ims/webservices/ws_invoices.php?invoiceid=".$invoicesArr[$i];
			    }
				else
				{
					$WebServiceURL = getprotocol()."www.vogsphere.co.za/ims/webservices/ws_invoices.php?invoiceid=".$invoicesArr[$i];
				}
				$sxml = simplexml_load_file($WebServiceURL);
				foreach($sxml->invoice as $invoice)
				{
					$DateOnly = '';
					$time = '';
					$id = $invoice->id;
					$str = "  ";
					$id = trim($id);
				 if (strlen($id) > 0)
				  {
					$newformat = '';
					if(isset($row['DateOnly']))
					{$time = strtotime($row['DateOnly']);
					$DateOnly = $row['DateOnly'];
					}

					if(!empty($time))
					{$newformat = date('Y-m',$time);}

					$net_amount = (float)$invoice->net_amount;
					$invArr['Month'] 	    = $newformat;
					$invArr['DateOnly'] 	= DATE($invoice->issue_date);
					$invArr['TotalAmount']  = '0.00';
					$invArr['amount'] 	    = '0.00';
					$invArr['IntUserCode'] 	= $row['IntUserCode'];
					$invArr['Status_Description'] = 'Invoice - '.$invoice->id;
					$invArr['Status_Code'] 	   = '';
					$invArr['TotalPayOut'] 	   = '0.00';
					$invArr['holdvalue'] 	   = '0.00';
					$invArr['percentage'] 	   = '';
					$sum = $net_amount ;	// -- $sum +
				  }
				}
				if (strlen($id) > 0)
				{
					// -- print_r($invArr);
					if(!empty($invArr))
					{
							$invArr['charges'] = number_format($sum ,2);
							$data[] = $invArr;
					}
				}
			}
	   }

		return $data;
   }
}
// -- Invoice Reconcilations
if(!function_exists('get_client_retainers'))
{
	function get_client_retainers($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$dataTemp = null;
			$tbl  = 'client_retainer';
$sqlHeader = "select DATE_FORMAT(Action_Date, '%Y-%m') as Month,DATE(Action_Date) DateOnly,0.00 as TotalAmount,IntUserCode,'Security' as Status_Description,'' as Status_Code,
0.00 as TotalPayOut,holdvalue, percentage ,0.00 as charges,invoices from $tbl";
			if($dataFilter['role'] == 'admin')
			{
				$srUserCode = '';
				if(!empty($dataFilter['srUserCode']))
				{$srUserCode = 'IntUserCode = ? AND';}

				// -- Get client_retainers.
				$sql = $sqlHeader." where $srUserCode DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ? ORDER BY Action_Date ASC";
				$q = $pdo->prepare($sql);
				if(empty($srUserCode))
				{
					$q->execute(array($dataFilter['month'],$dataFilter['tomonth']));
				}
				else
				{
					$q->execute(array($dataFilter['srUserCode'],$dataFilter['month'],$dataFilter['tomonth']));
				}
				$data = $q->fetchAll(PDO::FETCH_ASSOC);
				// -- Get all charges/invoices as well.
				$dataInvoice = get_invoice_reconcilations($dataFilter,$data);

				if(!empty($dataInvoice))
				{
					$data = array_merge($data,$dataInvoice);
				}
			}
			else
			{
				// -- Get client_retainers
			    $sql = $sqlHeader." where IntUserCode = ? AND DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ?  ORDER BY Action_Date ASC";
				$q = $pdo->prepare($sql);
				$q->execute(array($dataFilter['IntUserCode'],$dataFilter['month'],$dataFilter['tomonth']));
				$data = $q->fetchAll(PDO::FETCH_ASSOC);
				// -- Get all charges/invoices as well..
				$dataInvoice = get_invoice_reconcilations($dataFilter,$data);
				if(!empty($dataInvoice))
				{$data = array_merge($data,$dataInvoice);}

			}
			return $data;
	}
}
// -- Payment Reconcilations
if(!function_exists('get_payment_reconcilations'))
{
	function get_payment_reconcilations($dataFilter)
	{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$data = null;
			$tbl  = 'payments';
$sqlHeader = "SELECT DATE_FORMAT(Action_Date, '%Y-%m') as Month,DATE(Action_Date) DateOnly,0.00 as TotalAmount,benefiaryusercode as IntUserCode, 'Payment Successful' as Status_Description,Status_Code,Amount as TotalPayOut,
0.00 as holdvalue, '' as percentage,0.00 as charges FROM $tbl";
//SUM(Amount) as TotalPayOut
// GROUP BY DateOnly,Status_Description,IntUserCode  order by Action_Date
//GROUP BY DateOnly,Status_Description  order by Action_Date
			// - Quesions: Paid Via FNB,onhold
			if($dataFilter['role'] == 'admin')
			{
				$srUserCode = '';
				if(!empty($dataFilter['srUserCode']))
				{$srUserCode = 'benefiaryusercode = ? AND';}
/* All Reconcilaition for eCashMeUp payments Cloud Solution*/
$sql = $sqlHeader." WHERE $srUserCode DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ? and Status_Description NOT in ('pending','Stopped Payment','Deactivated')
";//GROUP BY Month";
				$q = $pdo->prepare($sql);
				if(empty($srUserCode))
				{
					$q->execute(array($dataFilter['month'],$dataFilter['tomonth']));
				}
				else
				{
					$q->execute(array($dataFilter['srUserCode'],$dataFilter['month'],$dataFilter['tomonth']));
				}
				$q->execute();
			}
			else
			{
/* All Reconcilaition per Imnternal User Code */
$sql = $sqlHeader." WHERE benefiaryusercode = ? AND DATE_FORMAT(Action_Date, '%Y-%m')  BETWEEN ? AND ?  and Status_Description NOT in ('pending','Stopped Payment','Deactivated')
";//GROUP BY Month";
				$q = $pdo->prepare($sql);
				$q->execute(array($dataFilter['IntUserCode'],$dataFilter['month'],$dataFilter['tomonth']));				//echo $sql;
			}
			//echo "<br/>"; echo $sql;
			$data = $q->fetchAll(PDO::FETCH_ASSOC);
			//print_r($data );
//echo 	$sql;
//ECHO $dataFilter['role'];
			return $data;
	}
}
////////////////////////////////////////////////////////////////////////
// -- Declarations -- //
$UserCode 			= null;
$IntUserCode 		= null;
$param 				= '';
$benefiaryid 		= '';
$userid 			= '';
$role 				= '';
$results 			= null;
$payments			= null;
$reconArray			= null;
$dataFilter			= null;
$month = '';
$month = date('1970-01');
$month = substr($month,0,7);
$tomonth = '';
$tomonth = date('9999-12');
$retainer = null;
////////////////////////////////////////////////////////////////////////
/// --- Data from POST -- //
if(!empty($_POST))
{
	//$month				= getpost('month');
	//$tomonth			= getpost('tomonth');
	$srUserCode			= getpost('srUserCode');
}
////////////////////////////////////////////////////////////////////////
// -- Data From SESSIONS
// -- User Code.
if(getsession('UserCode')){$UserCode = getsession('UserCode');}
// -- Int User Code.
if(getsession('IntUserCode')){$IntUserCode = getsession('IntUserCode');}
// -- Logged User Details.
if(getsession('username')){$userid = getsession('username');}
// -- Logged User Role
if(getsession('role')){$role = getsession('role');}
////////////////////////////////////////////////////////////////////////
// -- From Single Point Access
$param = '';
if(isset($_GET['param']))
{
	$param = $_GET['param'];
	$params = explode("|", $param);
	//print_r($params);
	if(empty($IntUserCode)){$IntUserCode = $params[0]; }
	if(empty($UserCode)){$UserCode  = $params[1];}
	if(empty($userid)){$userid 		= $params[2];}
	if(empty($role)){$role			= $params[3];}

	if(isset($params[4]))
	//{$month 						= $params[4];}
	if(isset($params[5]))
	{$tomonth 						= $params[5];}
	if(isset($params[6]))
	{$srUserCode 				    = $params[6];}
}
//print($param);
//echo $IntUserCode.' - '.$UserCode.' - '.$userid.' - '.$role.' - '.$month ;

////////////////////////////////////////////////////////////////////////
//$list = getpost('list');
$list = getpost('list');
/*$userid = $list['createdby'];
$list['createdon'] = date('Y-m-d');
$list['createdat'] = date('h:m:s');
$list['Status_Code'] = '1';
$list['Status_Description'] = 'pending';
/*
$dataFilter['IntUserCode'] 	= $list['IntUserCode'];
$dataFilter['UserCode']		= $list['UserCode'];
$dataFilter['userid'] 		= $list['userid'];
$dataFilter['role'] 		= $list['role'];
$dataFilter['month'] 		= $list['month'];
$dataFilter['tomonth'] 		= $list['tomonth'];*/
//print_r($list);
if(empty($list))
{
$dataFilter['IntUserCode'] 	= $IntUserCode;
$dataFilter['UserCode']		= $UserCode;
$dataFilter['userid'] 		= $userid;
$dataFilter['role'] 		= $role;
$dataFilter['month'] 		= $month;
$dataFilter['tomonth'] 		= $tomonth;
$dataFilter['srUserCode'] 	= $srUserCode;
// -- collection->payments->retainers->invoices
$results 			= get_collection_reconcilations($dataFilter);
$payments 			= get_payment_reconcilations($dataFilter);
$retainer           = get_client_retainers($dataFilter);
//if(!empty($_POST))
//{
	//$month				= getpost('month');
	//$tomonth			= getpost('tomonth');
////////////////////////////////////////////////////////////////////////
/*$dataFilter['IntUserCode'] 	= $IntUserCode;
$dataFilter['UserCode']		= $UserCode;
$dataFilter['userid'] 		= $userid;
$dataFilter['role'] 		= $role;
$dataFilter['month'] 		= $month;
$dataFilter['tomonth'] 		= $tomonth;
$results 			= get_collection_reconcilations($dataFilter);
$payments 			= get_payment_reconcilations($dataFilter);
$retainer           = get_client_retainers($dataFilter);*/
//}
}
else
{
 $results = json_encode($results);
 $results 			= get_collection_reconcilations($list);
 $payments 			= get_payment_reconcilations($dataFilter);
 $retainer          = get_client_retainers($dataFilter);
}
if(!empty($payments))
{
	$results = array_merge($results,$payments);
}
if(!empty($retainer))
{
		$results = array_merge($results,$retainer);
}
//print("<br/>");
 //$results = json_encode($results);

//print_r($results);
//echo $tomonth	;
$month = date('Y-m');

?>
