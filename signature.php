<?php
$basepath = 'database.php';
require $basepath;


// ---------------------------- SIGNED DOCUMENT ----------------------//
if(!function_exists('review_uploaded_doc'))
{
	function review_uploaded_doc($data)
	{
		$html = "<p>".
		$data['userid']." has uploaded a document to review and sign.</p>";
		$html = $html."<a href=".$data['url'].">REVIEW DOCUMENT</a>";
		return $html; 
	}
}

if(!function_exists('page_count'))
{
	function page_count($data)
	{
		// -- Multiple pages
		// https://stackoverflow.com/questions/25898678/fpdi-with-multiple-pages/25914022
		ob_start();

		$rootpath = $_SERVER['DOCUMENT_ROOT'];

		// Logo are update on page tcpdf_autoconfig.php (Under root directory)
		// Include the main TCPDF library (search for installation path).
		require_once('tcpdf_include.php');
		require_once "FPDI/fpdi.php";
		
		// initiate FPDI		
		$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'px', 'LETTER' ); //FPDI extends TCPDF

		// set the source file
		$pages = $pdf->setSourceFile($data['file']);
		ob_end_flush();
		return $pages;
	}
}

if(!function_exists('save_signature_into_doc'))
{
	function save_signature_into_doc($data,$option)
	{
		// -- Multiple pages
		// https://stackoverflow.com/questions/25898678/fpdi-with-multiple-pages/25914022
		ob_start();

		$rootpath = $_SERVER['DOCUMENT_ROOT'];

		// Logo are update on page tcpdf_autoconfig.php (Under root directory)
		// Include the main TCPDF library (search for installation path).
		require_once('tcpdf_include.php');
		require_once "FPDI/fpdi.php";
		$width  = 709;
		$height = 919;

		// initiate FPDI		
		$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'pt', array(709,919));//'A4' ); //FPDI extends TCPDF

		// set the source file
		$pages = $pdf->setSourceFile($data['file']);
		
		// -- Read all the pages and add them.
		for ($i = 1; $i <= $pages; $i++)
		{
			// add a page
			// $pdf->AddPage();
			// import page 
			$templateId = $pdf->importPage($i);
			
			// get the size of the imported page
			$size = $pdf->getTemplateSize($templateId);
			//print_r($size);
					$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
$pdf->SetMargins(0, 0, 0);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

			// create a page (landscape or portrait depending on the imported page size)
			if ($size['w'] > $size['h']) {
				$pdf->AddPage('L', array($size['w'], $size['h']));
			} else {//$size['w']$size['h']
				$pdf->AddPage('P', array($width,$height ));
			}
			
			// use the imported page as template.
			$pdf->useTemplate($templateId);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetTextColor(255, 0, 0);
			
			// -- Position the signature into the correct PDF positions.
			// -- Depending which page.
			$x = 0;
			$y = 0;
			//$pdf->Write(0, $x.",".$y." page : ".$i);
				$json = $data['signature'];
								//	print_r($json);

				foreach ($json as $item) 
				{
					if($item['position'])
					{
						$pos = explode("|",$item['position']);
						if($i == $pos[2])
						{
							$pdf->SetXY($pos[0], $pos[1]);
						    $x  = $pos[0];
						    $y  = $pos[1];
							$html = $pos[3];
							//$pdf->Write(0, $x.",".$y." page : ".$i);//$item['esignature']
							//$pdf->writeHTML($html, true, false, true, false, '');
							$html = str_replace ('px', 'pt', $html);

							$pdf->writeHTMLCell(120, 30, $x, $y, $html, 0, 0, 0, false, '', true);
							   // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
							

						}
					}
				//$pdf->Image($item['esignature'], $x , $y, 40, 40, '', '', '', false, 300, '', 
				//false, false, 1, false, false, false);
				}					
		}
		$basepath = str_replace('signature.php',$data['file'],$_SERVER['PHP_SELF']);
		$basepath = $rootpath.$basepath;
		//echo PDF_MARGIN_BOTTOM; = 25
		//echo PDF_MARGIN_TOP; = 45
		//echo PDF_MARGIN_LEFT;=15
		//echo PDF_MARGIN_RIGHT;=15
		//echo PDF_MARGIN_FOOTER;=10
		//echo "<br/>";
				//echo PDF_MARGIN_HEADER;=15

		$pdf->Output($basepath,$option); //"F"
		//$pdf->Output($basepath,"I"); //"F"
		ob_end_flush();
	}
}

if(!function_exists('generate_doc_url'))
{
	function generate_doc_url($data)
	{
		
	}
}

if(!function_exists('db_insert_signed_doc'))
{
	function db_insert_signed_doc($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sqlQuestionMarks[] = '(?,?,?,?,?,?,?,?,?,?)';
			try
			{
				$sql = "INSERT INTO user_signed_doc
				(
				signeddocfile,
				signeddoctype,
				userid,
				usersignfile,
				signeddocformat,
				createdon,
				createdat,
				createdby,
				status,
				url)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();				
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			//echo $id;
			return $id;
	}
}

if(!function_exists('show_uploaded_doc'))
{
	function show_uploaded_doc($data)
	{
		$showdoc = '';
		$showdoc = "<iframe src='".$data[0]."' width='100%' height='500px'>";
		return $showdoc;
	}
}

if(!function_exists('db_get_signed_doc'))
{
	function db_get_signed_doc($data)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'user_signed_doc';
			$sql = "select * from $tbl_2 where signeddocid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($data['signeddocid']));
			$dataobj = $q->fetch(PDO::FETCH_ASSOC);
			return $dataobj;
	}
}

if(!function_exists('db_get_signed_docs'))
{
	function db_get_signed_docs($data)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'user_signed_doc';
			$sql = "select * from $tbl_2 where userid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($data['userid']));
			$dataobj = $q->fetchAll();
			return $dataobj;
	}
}

if(!function_exists('db_signed_doc'))
{
	function db_signed_doc($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sqlQuestionMarks[] = '(?,?,?,?,?,?)';
			try
			{
				$sql = "INSERT INTO user_signature
				(
				userid,
				usersignfile,
				signstatus,
				createdby,
				createdon,
				createdat)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();				
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			return $id;
	}
}
// --------------- SIGNATURE -----------------------------------------//
if(!function_exists('db_get_signatures'))
{
	function db_get_signatures($data)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'user_signature';
			$sql = "select * from $tbl_2 where userid = ? ORDER BY usersignid DESC"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($data['userid']));
			$dataobj = $q->fetchAll();
			return $dataobj;

	}
}
if(!function_exists('db_get_signature'))
{
	function db_get_signature($data)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'user_signature';
			$sql 	= "select * from $tbl_2 where userid = ? order by createdon desc, createdat desc"; 		
			$q 		= $pdo->prepare($sql);
			$q->execute(array($data['usersignid']));
			$dataobj = $q->fetch(PDO::FETCH_ASSOC);
			return $dataobj;
	}
}
if(!function_exists('db_insert_signature'))
{
	function db_insert_signature($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sqlQuestionMarks[] = '(?,?,?,?,?,?)';
			try
			{
				$sql = "INSERT INTO user_signature
				(
				userid,
				usersignfile,
				signstatus,
				createdon,
				createdat,
				createdby)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();				
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			//echo $id;
			return $id;
	}
}

// -- Signature Functions Executions
// -- GET functions
$file = '';
$esignature = '';
$einitials = '';
$edate = '';
$elocation = '';

if ( !empty($_GET['file'])) 
{
	$file = $_GET['file'];
	$data[] = $file;
	echo show_uploaded_doc($data);
}
// -- POST Functions
if(!empty($_POST))
{
	if(isset($_POST['esignature']))
	{
		$esignature 	= $_POST['esignature'];
		print_r($esignature);
		$einitials 		= $_POST['einitials'];
		$edate 			= $_POST['edate']; 		
		$elocation 		= $_POST['elocation'];
		$file	  		= $_POST['file'];
		$data['file'] = $file;		
		$data['signature'] = $esignature;
		$show = "F";
		save_signature_into_doc($data,$show);
     }		
}
elseif ( !empty($_GET['x']))
{
$data['file'] = './esignature/files/certificate.pdf';//c4cd464de9ab566839089d6afb31bfdb.pdf';
$dataArry["esignature"] = './doc_signs/338b0ee550085ce82fdc77e3d4d9607d.png';
$html = '<img src="./doc_signs/2f86941756d101517bbdcc97a131de60.png" id="ball0" style="border:1px solid;padding:1pt;background-color: rgba(0, 0, 0, 0.3);">';
$dataArry["position"] = "0|0|1|$html";//ymax = 720pt,xmax=600pt
$dataArry1[] = $dataArry;

$html = '<img src="./doc_signs/2f86941756d101517bbdcc97a131de60.png" id="ball0" style="border:1px solid;padding:1pt;background-color: rgba(0, 0, 0, 0.3);">';
$dataArry["position"] = "115|0|1|$html";//ymax = 720pt,xmax=600pt
$dataArry1[] = $dataArry;

$html = '<img src="./doc_signs/2f86941756d101517bbdcc97a131de60.png" id="ball0" style="border:1px solid;padding:1pt;background-color: rgba(0, 0, 0, 0.3);">';
$dataArry["position"] = "230|0|1|$html";//ymax = 720pt,xmax=600pt
$dataArry1[] = $dataArry;

$html = '<img src="./doc_signs/2f86941756d101517bbdcc97a131de60.png" id="ball0" style="border:1px solid;padding:1pt;background-color: rgba(0, 0, 0, 0.3);">';
$dataArry["position"] = "345|0|1|$html";//ymax = 720pt,xmax=600pt
$dataArry1[] = $dataArry;

$html = '<img src="./doc_signs/2f86941756d101517bbdcc97a131de60.png" id="ball0" style="border:1px solid;padding:1pt;background-color: rgba(0, 0, 0, 0.3);">';
$dataArry["position"] = "492|750|1|$html";//ymax = 720pt,xmax=600pt
$dataArry1[] = $dataArry;

//print_r($dataArry);
$data['signature'] = $dataArry1;
$show = "I";
save_signature_into_doc($data,$show);		
}

?>