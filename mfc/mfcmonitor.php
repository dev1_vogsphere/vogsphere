<?php
$servername = 'localhost';
$username = 'root';
$password = 'Mulu$2625';

try {
  $pdo = new PDO("mysql:host=$servername;dbname=mfc", $username, $password);
  // set the PDO error mode to exception
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}

$sql = 'SELECT * FROM mfc where sampledatetime between  DATE_ADD(now(), INTERVAL -7 DAY) and now() order by sampledatetime desc limit 2000';
$datafrequency = $pdo->query($sql);

?>


<!DOCTYPE html>   
<html lang="en">   
<head>   
<meta charset="utf-8">   
<title>MFC Monitor</title>   
<meta name="description" content="Bootstrap.">  
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>  
<body style="margin:20px auto">  
<div class="container">
<div class="row header" style="text-align:center;color:green">
<h3>Microbial Fuel Cells Monitor</h3>
</br>
</br>
</div>

<table id="myTable" class="table" data-show-refresh="true"  data-auto-refresh="true" data-pagination="true" table-striped table-bordered table-responsive table-hover" >  
        <thead>  
          <tr>  
            <th>Media</th>  
            <th>Sampling(Time)</th>
            <th>Room Temp</th>
            <th>Room Humidity</th>
            <th>MFC Temp</th>
            <th>MFC Humidity</th>
            <th>Resistance(ohms)</th> 
            <th>Voltage (V)</th>  
            <th>Power (uW)</th>  
          </tr>  
        </thead>  
      
          <?php
	        	if(!empty($datafrequency ))
	          	{
			        	
              foreach($datafrequency as $row)
			      	{

                $resitorvalue=$row["resistor1"]+$row["resistor2"];
              //  echo $value;
                echo '<tr><td width="10%">'.$row["media"].'</td>';
                echo '<td width="15%">'.$row["sampledatetime"].'</td>';
                echo '<td width="10%">'.$row["roomtemp"].'</td>';
                echo '<td width="10%">'.$row["roomhumidity"].'</td>';
                echo '<td width="10%">'.$row["mfctemp"].'</td>';
                echo '<td width="10%">'.$row["mfchumidity"].'</td>';
                echo '<td width="5%">'.$resitorvalue.'</td>';
                echo '<td width="15%">'.$row["voltage"].'</td>';
                echo '<td width="15%">'.round((((pow($row["voltage"],2)/$resitorvalue))*1000000),3).'</td>';
                echo '</tr>';
				      }
	          	}	
          ?> 
      </table>  
	  </div>
</body>  
<script>
$(document).ready(function(){
    $('#myTable').dataTable({
        order: [[1, 'desc']],
    });
});
</script>
</html>  
