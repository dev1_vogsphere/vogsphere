<!DOCTYPE html>
<html>
<body>
<h1>NOTIFICATION OF PAYMENT</h1>
<p>To Whom It May Concern:</p>
<p>[Companyname] hereby confirms that the following payment instruction has been received:</p>
<hr/>
<table class="table table-borderless">
    <thead>
      <tr>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Date Actioned</td>
        <td>:[Action_Date]</td>
      </tr>
      <tr>
        <td>Time Actioned </td>
        <td>:[createdat]</td>
      </tr>
	  <tr>
        <td>Trace ID</td>
        <td>:[payment]</td>
      </tr>	
	  <br/>	  
	  <tr>
        <th><b>Payer Details</b></th>
        <th></th>
      </tr>	  
	  <tr>
        <td>Payment From</td>
        <td>:[Description]</td>
      </tr>
	  <tr>
        <td>Currency/Amount</td>
        <td>:[currency][Amount]</td>
      </tr>	
	  <br/>	  	  
	  <tr>
        <th><b>Payee Details</b></th>
        <th></th>
      </tr>	
	  <tr>
        <td>Account Number</td>
        <td>:[Account_Number]</td>
      </tr>
	  <tr>
        <td>Account Holder Name</td>
        <td>:[Account_Holder]</td>
      </tr>	 
	  <tr>
        <td>Bank</td>
        <td>:[bankname]</td>
      </tr>
	  <tr>
        <td>Branch Code</td>
        <td>:[Branch_Code]</td>
      </tr>		  
	  <tr>
        <td>Reference</td>
        <td>:[Bank_Reference]</td>
      </tr>	 
 	  
    </tbody>
  </table>
<h1>END OF NOTIFICATION</h1>

<hr>

<p>To confirm this Payment Notification,you may email us at : [email] or call us at : [phone].</p>
<p>Our customer(payer) has requested [Companyname] to send this notification of payment to you. Should you have any queries regarding the contents of this notice, please contact the payer. [Companyname] does not guarantee or warrant the accuracy and the integrity of information and data transmitted electronically and we accept no liability whatsoever for any loss, expense, claim or damage, whether direct,indirect or consequential, arising from the transmission of the Information and data.
</p>

<hr>
  
[Disclaimer]
</body>
</html>