<?php 
require 'database.php';
// -- BOC 27.01.2017
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // -- Read all the Loan Type Interest Rate Package.
  // -- Loan Types (27.01.2017) - All Records
  $sql = 'SELECT * FROM loantype ORDER BY loantypeid DESC';
  $data = $pdo->query($sql);						   						 

  // -- Loan Types (27.01.2017)
	foreach ($data as $row) 
	{
		if($row['loantypeid'] == 1) // -- 27.01.2017
		{
			$_SESSION['personalloan'] = $row['percentage'];
			$_SESSION['loantypedesc'] = $row['loantypedesc'];
			$_SESSION['loantypeid'] = $row['loantypeid'];// -- Loan Type ID
		}
	}
	$personalloan = $_SESSION['personalloan'];
	$loantype = $_SESSION['loantypedesc'];
// -- Loan Type ID    
	$loantypeid = $_SESSION['loantypeid'];

	Database::disconnect();
// -- EOC 27.01.2017

// -- BOC 27.01.2017
$personalloan = $_SESSION['personalloan'];
$loantype = $_SESSION['loantypedesc'];
$currency = $_SESSION['currency'];// -- 10.02.2017
// -- EOC 27.01.2017

// --------------------------------- BOC 27.01.2017 ----------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //
// -- Database Declarations and config:  
  $pdo = Database::connect();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// --------------------- BOC 2017.04.15 ------------------------- //
//1. --------------------- Bank and Branch code ------------------- //
  $sql = 'SELECT * FROM bank ORDER BY bankname';
  $dataBanks = $pdo->query($sql);						   						 

  $sql = 'SELECT * FROM bankbranch ORDER BY bankid';
  $dataBankBranches = $pdo->query($sql);						   						 

//2. ---------------------- BOC - Provinces ------------------------------- //
// 15.04.2017 - 
  $sql = 'SELECT * FROM province';
  $dataProvince = $pdo->query($sql);						   						 

//3. ---------------------- BOC - Account Types ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM accounttype';
 $dataAccountType = $pdo->query($sql);						   						 
// ---------------------- EOC - Account Types ------------------------------- //  

//4. ---------------------- BOC - Payment Method ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentmethod';
 $dataPaymentMethod = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Method ------------------------------- //  

//5. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM paymentfrequency';
 $dataPaymentFrequency = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  

//6. ---------------------- BOC - Payment Frequency ------------------------------- //
// 15.04.2017 - 
 $sql = 'SELECT * FROM loantype';
 $dataLoanType = $pdo->query($sql);						   						 
// ---------------------- EOC - Payment Frequency ------------------------------- //  
	Database::disconnect();	
// BOC -- 15.04.2017 -- Updated.
$BankName  = '';
$BranchCode = '';
// EOC -- 15.04.2017 -- Updated.	
// --------------------- EOC 2017.04.15 ----------------------------------------------------------- //
//   					Update  Details Dynamic Table 												//
// ------------------------------------------------------------------------------------------------ //

// For Send E-mail
$email = "";
$FirstName = "";
$LastName = "";
		
// ------------ Start Send an Email ----------------//
function SendEmail()
{
require_once('class/class.phpmailer.php');

try{
			
			$mail=new PHPMailer();
			$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
			$mail->SetFrom("info@vogsphere.co.za","e-Loan - Vogsphere (PTY) LTD");
		    $mail->AddReplyTo("info@vogsphere.co.za","info");
			
			/* $mail=new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug=1;
			$mail->SMTPAuth=true;
			$mail->SMTPSecure="tls";
			$mail->Host="smtp.vogsphere.co.za";
			$mail->Port= 587;//465;
			$mail->Username="vogspesw";
			$mail->Password="fCAnzmz9";
			$mail->SetFrom("info@vogsphere.co.za","e-Loan - Vogsphere (PTY) LTD"); */

			//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
			$mail->Subject=  "Your Vogsphere e-Loan Documentation";
			//$mail->Body= $email_text;//"This is the HTML message body <b>in bold!</b>";
			// Enable output buffering
			ob_start();
			include 'text_emailWelcomeCustomer.php';//'text_emailWelcomeCustomer.php'; //execute the file as php
			$body = ob_get_clean();			
						
			$fullname = $FirstName.' '.$LastName;

			// print $body;
			$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
			$mail->AddAddress($email, $fullname);

			if($mail->Send()) 
			{
			printf("<p class='status'>Your e-Loan application is successfully registered. An e-mail is sent your Inbox!</p>\n");
			}
			else 
			{
			  // echo 'mail not sent';
			printf("<p class='status'>Your e-Loan application was successfully registered. but your e-mail address seems to be invalid!</p>\n");

			}
			
			// Admin E-mail
			$adminemail 	= "info@vogsphere.co.za";
			$AdminFirstName = 'E-loan';
			$AdminLastName 	= 'Admin';
			// clear addresses of all types
			$mail->ClearAddresses();  // each AddAddress add to list
			$mail->ClearCCs();
			$mail->ClearBCCs();
			
// -- send to Eloan Administrator
			$mail->Subject=  "New Vogsphere e-Loan Application for approval";
			ob_start();
			include 'text_emailNotifyAdmin.php';// -- Admin 
			$body = ob_get_clean();						
			$fullname = $AdminFirstName.' '.$AdminLastName;
			
			// print $body;
			$mail->MsgHTML($body );
			$mail->AddAddress($adminemail, $fullname);
			if($mail->Send()) 
			{
			  // nothing
			}
		}
	catch(phpmailerException $e)
	{
		echo $e->errorMessage();
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}	
}
// ------------ End Send an Email ----------------//


// -- Generate Captcha Image
function Image($value)
{
 $generateImage = '';
 if (!empty($_POST)) 
 {
 if (isset($_POST['Apply']))
   {$generateImage = 'No';}
 }
 
 if( $generateImage == '')
 {
$string = '';
for ($i = 0; $i < 5; $i++) {
    $string .= chr(rand(97, 122));
}

$_SESSION['captcha'] = $string; //store the captcha
$_SESSION['value'] = $value;
$dir = 'fonts/';
$image = imagecreatetruecolor(165, 50); //custom image size
$font = "PlAGuEdEaTH.ttf"; // custom font style
$color = imagecolorallocate($image, 113, 193, 217); // custom color
$white = imagecolorallocate($image, 255, 255, 255); // custom background color
imagefilledrectangle($image,0,0,399,99,$white);
imagettftext($image, 30, 0, 10, 40, $color, $dir.$font, $_SESSION['captcha']);

// Enable output buffering
ob_start();
imagepng($image);
// Capture the output
$imagedata = ob_get_contents();
// Clear the output buffer
ob_end_clean();
$_SESSION['image'] = $imagedata;
}
else
{
$imagedata = $_SESSION['image'];
}

return $imagedata;
}
// -- End - Generate Captcha Image

$password1 = '';
$idnumber  = '';
$tbl_user="user"; // Table name 
$tbl_customer ="customers"; // Table name
$tbl_loanapp ="loanapp"; // Table name
$db_eloan = "vogspesw_siwapp_v2";
$captcha = '';
$modise = '';
	 	//echo "<h1>Debugging</h1>";Modise debug.

 if (!empty($_POST)) 
	{
	// Captcha declaration
	$modise = $_SESSION['captcha'];
	$captchaError = null;
	// --Captcha
	$captcha = $_POST['captcha'];
	
		// keep track validation errors - 		// Customer Details
		$idnumberError = null;
		$TitleError = null;
		$FirstNameError = null; 
		$LastNameError = null;
		$phoneError = null;
		$emailError = null;
		$StreetError = null;
		$SuburbError = null;
		$CityError = null;
		$StateError = null;
		$PostCodeError = null;
		$DobError = null;
		$SuretyError = null;
		$firstpaymentError = null;
		$password1Error = null;
		$password2Error = null;


				
		// keep track validation errors - 		// Loan Details
		$MonthlyExpenditureError = null; 
		$ReqLoadValueError = null; 
		$DateAccptError = null; 
		$LoanDurationError = null; 
		$StaffIdError = null; 
		$InterestRateError = null; 
		$ExecApprovalError = null; 
		$MonthlyIncomeError = null; 

		// ------------------------- Declarations - Banking Details ------------------------------------- //
		$AccountHolderError = null;
		$AccountTypeError = null;
		$AccountNumberError = null;
		$BranchCodeError = null;
		$BankNameError = null;
		// -------------------------------- Banking Details ------------------------------------------ //
		
		// keep track post values
		// Customer Details
		$Title = $_POST['Title'];
		$idnumber = $_POST['idnumber'];
		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$Street = $_POST['Street'];
		$Suburb = $_POST['Suburb'];
		$City = $_POST['City'];
		$State = $_POST['State'];
		$PostCode = $_POST['PostCode'];
		$Dob		= $_POST['Dob'];
		$Surety		= $_POST['Surety'];
		$firstpayment = $_POST['FirstPaymentDate'];
		$loantypeid = 1;
		$StaffId = 1;
		
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
		//{
		 // $password1 = $_POST['password1'];
		//}
		$monthlypayment = $_POST['monthlypayment'];

		// Loan Details
		$MonthlyExpenditure = $_POST['MonthlyExpenditure'];
		$ReqLoadValue = $_POST['ReqLoadValue'];
		$DateAccpt  = date('Y-m-d');
		$LoanDuration  = $_POST['LoanDuration'];
		$InterestRate = $_POST['InterestRate'];
		$ExecApproval  = "PEN";
		$MonthlyIncome  = $_POST['MonthlyIncome'];
		$Repayment = $_POST['Repayment'];
		$paymentmethod = $_POST['paymentmethod'];
		$FirstPaymentDate = $_POST['FirstPaymentDate'];
		$lastPaymentDate = $_POST['lastPaymentDate'];
		$loantype = $_POST['loantype'];
		$paymentfrequency = $_POST['paymentfrequency'];

		$TotalAssets = 0;
		$LoanValue = 0;
		
		$months31 = array(1,3,5,7,8,10,12);
		$months30 = array(4,6,9,11);
		$monthsFeb = array(2);

		// ------------------------- $_POST - Banking Details ------------------------------------- //
		$AccountHolder = $_POST['AccountHolder'];
		$AccountType = $_POST['AccountType'];
		$AccountNumber = $_POST['AccountNumber'];
		$BranchCode = $_POST['BranchCode'];
		$BankName = $_POST['BankName'];
		// -------------------------------- Banking Details ------------------------------------------ //
	 	//echo "<h1>Debugging  After Posting</h1>";//Modise debug.

// validate input - 		// Customer Details
		$valid = true;
		if (empty($Title)) { $TitleError = 'Please enter Title'; $valid = false;}
		
		if (empty($idnumber)) { $idnumberError = 'Please enter ID/Passport number'; $valid = false;}
		
// SA ID Number have to be 13 digits, so check the length
		if ( strlen($idnumber) != 13 ) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}
		
// Is Numeric 
		if ( !is_Numeric($idnumber)) { $idnumberError = 'ID number does not appear to be authentic - input not a valid number'; $valid = false;}

		
// Check first 6 digits as a date.
// Month(1 - 12)
		if (substr($idnumber,2,2) > 12) 
		{ 
			$idnumberError = 'ID number does not appear to be authentic - input not a valid month'; $valid = false;
		}
		else
		{

		// Day from 1 - 30
			if (in_array(substr($idnumber,2,2),$months30)) 
			{ 
			   
		       // Day 
			   if (substr($idnumber,4,2) > 30)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}
		// Day from 1 - 31	
			else if (in_array(substr($idnumber,2,2),$months31)) 

			{
				// Day 
			   if (substr($idnumber,4,2) > 31)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
			}		
		// Day from 1 - 28		
			else if (in_array(substr($idnumber,2,2),$monthsFeb)) 
			{  $idString = ''.$idnumber;
			$extract = substr($idString,0,2);
				//echo "<h1>Debugging  After Posting, Check ID number Feb $extract</h1>";//Modise debug.

				// Leap Year	
				$yearValidate = (int)$extract;
				//echo "<h1>Debugging  After Posting, Check ID number Feb $yearValidate</h1>";//Modise debug.

				$leapyear = ($yearValidate % 4 == 0);

				if($leapyear == true)
				{
					// Day 
			   if (substr($idnumber,4,2) > 29)
			   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
				else
				{ // Day 
					   if (substr($idnumber,4,2) > 28)
					   {$idnumberError = 'ID number does not appear to be authentic - input not a valid day'; $valid = false;}
				}
			}			
		}

		// ------------------------- Validate Input - Banking Details ------------------------------------- //
		if (empty($AccountHolder)) { $AccountHolderError = 'Please enter Account Holder'; $valid = false;}
		if (empty($BranchCode)) { $BranchCodeError = 'Please enter Branch Code'; $valid = false;}
		if (empty($AccountNumber)) { $AccountNumberError = 'Please enter Account Number'; $valid = false;}
		// -------------------------------- Banking Details ------------------------------------------ //
				
		if (empty($FirstName)) { $FirstNameError = 'Please enter FirstName'; $valid = false;}
		
		if (empty($LastName)) { $LastNameError = 'Please enter LastName'; $valid = false;}
		
		// Phone number must be characters
		if (!is_Numeric($phone)) { $phoneError = 'Please enter valid phone numbers - digits only'; $valid = false;}

		// 10 digits
		if (strlen($phone) > 10) { $phoneError = 'Please enter valid phone numbers - 10 digits'; $valid = false;}
		
		if (empty($phone)) { $phoneError = 'Please enter phone numbers'; $valid = false;}
		
		// validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$emailError = "Invalid email format"; $valid = false;}
		
		if (empty($email)) { $emailError = 'Please enter e-mail'; $valid = false;}
		
		if (empty($Street)) { $StreetError = 'Please enter Street'; $valid = false;}
		
		if (empty($Suburb)) { $SuburbError = 'Please enter Suburb'; $valid = false;}
		
		if (empty($City)) { $CityError = 'Please enter City/Town'; $valid = false;}
		
		if (empty($State)) { $StateError = 'Please enter province'; $valid = false;}
		
		// is Number
				if (!is_Numeric($PostCode)) { $PostCodeError = 'Please enter valid Postal Code - numbers only'; $valid = false;}

		// Is 4 numbers
		if (strlen($PostCode) > 4) { $PostCodeError = 'Please enter valid Postal Code - 4 numbers'; $valid = false;}
		
		if (empty($PostCode)) { $PostCodeError = 'Please enter Postal Code'; $valid = false;}
		
		if (empty($Dob)) { $DobError = 'Please enter/select Date of birth'; $valid = false;}
		
		if (empty($Surety)) { $SuretyError = 'Please enter Surety'; $valid = false;}
		
		if (empty($firstpayment)) { $firstpaymentError = 'Please enter/select first payment date'; $valid = false;}

		// MonthlyIncome is numeric.
		if (!is_Numeric($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter a number for Monthly Income'; $valid = false;}

				// validate input - 		// Loan Details
		if (empty($MonthlyIncome)) { $MonthlyIncomeError = 'Please enter Monthly Income'; $valid = false;}
		
		
		if (empty($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter MonthlyExpenditure'; $valid = false;}
		
		// MonthlyExpenditure is numeric.
		if (!is_Numeric($MonthlyExpenditure)) { $MonthlyExpenditureError = 'Please enter a number for Monthly Expenditure'.is_float($MonthlyExpenditure); $valid = false;}

		// ReqLoadValue is numeric.
		if (!is_Numeric($ReqLoadValue)) { $ReqLoadValueError = 'Please enter a number for Required Loan Value'; $valid = false;}

		if (empty($ReqLoadValue)) { $ReqLoadValueError = 'Please enter Required Loan Value'; $valid = false;}
		
		//if (empty($ExecApproval)) { $nameError = 'Please enter status'; $valid = false;}
		if (empty($DateAccpt)) { $DateAccptError = 'Please enter Date Accpt'; $valid = false;}
				
		// Loan duration is numeric.
		if (!is_Numeric($LoanDuration)) { $LoanDurationError = 'Please enter a number for Loan Duration'; $valid = false;}

		if (empty($LoanDuration)) { $LoanDurationError = 'Please enter Loan Duration'; $valid = false;}

		if (empty($InterestRate)) { $InterestRateError = 'Please enter Interest Rate'; $valid = false;}
		
		// Captcha Error Validation.
		if (empty($captcha)) { $captchaError = 'Please enter verification code'; $valid = false;}
		if ($captcha != $modise ) { $captchaError = 'Verication code incorrect, please try again.'; $valid = false;}


		// -- Validate Password Only if not loggedin
		if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
			 // nothing
			}
			else
			{
			
			$password1 = $_POST['password1'];
			//$password2 = $_POST['password2'];
			
			if (empty($password1)) { $password1Error = 'Please enter Password'; $valid = false;}
			//if (empty($password2)) { $password2Error = 'Please confirm password'; $valid = false;}
			
			//if($password1 != $password2) {$password1Error = 'Password does not match'; $valid = false; }

			}
	
		// 	refresh Image
		if (!isset($_POST['Apply'])) // If refresh button is pressed just to generate Image. 
		{
		  $valid = false;
		}
		// Insert e-Loan Application Data.
		if ($valid) 
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			// ---------------- User ---------------
			//User - UserID & E-mail Duplicate Validation
			$query = "SELECT * FROM $tbl_user WHERE userid = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($idnumber));

			if ($q->rowCount() >= 1)
			{$idnumberError = 'ID number already exists.'; $valid = false;}
			
			// User - E-mail.
			$query = "SELECT * FROM $tbl_user WHERE email = ?";
			$q = $pdo->prepare($query);
			$q->execute(array($email));
			if ($q->rowCount() >= 1)
			{$emailError = 'E-mail Address already in use.'; $valid = false;}
			
			if ($valid) 
			{
			
				// -- Validate Password Only if not loggedin
		//if (!isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
			{
				// Insert User			
				$sql = "INSERT INTO $tbl_user(userid,password,email,role) VALUES (?,?,?,'Customer')";
				$q = $pdo->prepare($sql);
				$q->execute(array($idnumber,$password1,$email));
			}
			
			// Customer - UserID & E-mail Duplicate Validation
			
			$sql = "INSERT INTO $tbl_customer (CustomerId,Title,FirstName,LastName,Street,Suburb,City,State,PostCode,Dob,phone)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$Title,$FirstName,$LastName,$Street,$Suburb,$City,$State,$PostCode,$Dob,$phone));

			// Insert Application Loan	
			$sql = "INSERT INTO $tbl_loanapp(CustomerId,loantypeid,MonthlyIncome,MonthlyExpenditure,TotalAssets,ReqLoadValue,ExecApproval,";
			$sql = $sql."DateAccpt,StaffId,InterestRate,LoanDuration,LoanValue,surety,Repayment,paymentmethod,FirstPaymentDate,lastPaymentDate,monthlypayment,paymentfrequency,accountholdername,bankname,accountnumber,branchcode,accounttype)";
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; //24
			$q = $pdo->prepare($sql);
			$q->execute(array($idnumber,$loantypeid,$MonthlyIncome,$MonthlyExpenditure,$TotalAssets,$ReqLoadValue,$ExecApproval,$DateAccpt,$StaffId,$InterestRate,$LoanDuration,$LoanValue,$Surety,$Repayment,$paymentmethod,$FirstPaymentDate,$lastPaymentDate,$monthlypayment,$paymentfrequency,
			$AccountHolder,$BankName,$AccountNumber,$BranchCode,$AccountType));//24
			
			Database::disconnect();
			
			// --------------------------- Session Declarations -------------------------//
			$_SESSION['Title'] = $Title;
			$_SESSION['FirstName'] = $FirstName;
			$_SESSION['LastName'] = $LastName;
			$_SESSION['password'] = $password1;
												
			// Id number
			$_SESSION['idnumber']  = $idnumber;
			$clientname = $Title.' '.$FirstName.' '.$LastName ;
			$suretyname = $clientname;
			$name = $suretyname;		
			// Phone numbers
			$_SESSION['phone'] = $phone;
			// Email
			$_SESSION['email'] = $email ;
			// Street
			$_SESSION['Street'] = $Street;
			// Suburb
			$_SESSION['Suburb'] = $Suburb;
			// City
			$_SESSION['City'] = $City;
			// State
			$_SESSION['State'] = $State;
			// PostCode
			$_SESSION['PostCode'] = $PostCode;
			// Dob
			$_SESSION['Dob'] = $Dob;
			// phone
			$_SESSION['phone'] = $phone;	
			//------------------------------------------------------------------- 
			//           				LOAN APP DATA							-
			//-------------------------------------------------------------------			
			$_SESSION['loandate'] = $DateAccpt;// "01-08-2016";
			$_SESSION['loanamount'] = $ReqLoadValue ;// "R2000";
			$_SESSION['loanpaymentamount'] = $Repayment  ; //"R2400";
			$_SESSION['loanpaymentInterest'] = $InterestRate ;// = "20%";
			$_SESSION['FirstPaymentDate'] = $FirstPaymentDate;// = ;//'01-08-2016';
			$_SESSION['LastPaymentDate'] = $lastPaymentDate;// = '01-08-2016';
			$_SESSION['applicationdate'] = $DateAccpt;// = '01-08-2016';
			$_SESSION['status'] = $ExecApproval;// = '01-08-2016';
			$_SESSION['monthlypayment'] = $monthlypayment;// Monthly Payment Amount 
			$_SESSION['LoanDuration'] = $LoanDuration;// Loan Duration
 
 			// ------------------------- $_SESSION - Banking Details ------------------------------------- //
			$_SESSION['AccountHolder'] = $AccountHolder;
			$_SESSION['AccountType'] = $AccountType;
			$_SESSION['AccountNumber'] = $AccountNumber ;
			$_SESSION['BranchCode'] = $BranchCode;
			$_SESSION['BankName'] = $BankName;
		   // ------------------------------- $_SESSION Banking Details ------------------------------------------ //

		// --------------------------- End Session Declarations ---------------------//
		
		// --------------------------- SendEmail to the clients to inform them about the e-loan application -----//
			SendEmail();	
			
			}
		}
}
?>
<div class="container background-white">
                    <div class="row margin-vert-30">
                        <!-- Register Box -->
                        <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                              <form METHOD="post" class="signup-page" action="applicationForm"> 
                                <div class="signup-header">
                                    <h2>e-Loan Application</h2>
									<p>Applying is really quick & then we use your personal info to check your Identity and carry out a 	credit score.
									You’re applying for a small unsecured short term loan, intended to address short term financial needs.
									</p>
									<p>NOTE: * = Required Field</p>
                                    <p>Already a member? Click
                                        <a href="customerLogin">HERE</a>to login to your account.</p>
                                </div>
								
								<!-- Start - Accordion - Alternative  -->
																	 
                                <label>Title</label>
								<SELECT  id="Title" class="form-control" name="Title" size="1">
									<OPTION value="Mr">Mr</OPTION>
									<OPTION value="Mrs">Mrs</OPTION>
									<OPTION value="Miss">Miss</OPTION>
									<OPTION value="Ms">Ms</OPTION>
									<OPTION value="Dr">Dr</OPTION>
								</SELECT>
								<br/>
                                <!-- <input class="form-control margin-bottom-20" type="text"> -->

								<!-- ID number  -->
								<div class="control-group <?php echo !empty($idnumberError)?'error':'';?>">
									 <label >ID/Passport Number
										<span class="color-red">*</span>
									</label> 
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-20"  id="idnumber" name="idnumber" type="text"  placeholder="ID number" value="<?php echo !empty($idnumber)?$idnumber:'';?>"  onchange="CreateUserID()">
									<?php if (!empty($idnumberError)): ?>
										<span class="help-inline"><?php echo $idnumberError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								
								<!-- End ID number -->
								<div class="control-group <?php echo !empty($FirstNameError)?'error':'';?>">
									<label class="control-label">First Name</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="FirstName" type="text"  placeholder="FirstName" value="<?php echo !empty($FirstName)?$FirstName:'';?>">
										<?php if (!empty($FirstNameError)): ?>
										<span class="help-inline"><?php echo $FirstNameError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Last Name -->
								
								<div class="control-group <?php echo !empty($LastNameError)?'error':'';?>">
									 <label>Last Name
										<span class="color-red">*</span>
										
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="LastName" type="text"  placeholder="LastName" value="<?php echo !empty($LastName)?$LastName:'';?>">
									<?php if (!empty($LastNameError)): ?>
										<span class="help-inline"><?php echo $LastNameError; ?></span>
									<?php endif; ?>	
									</div>
								</div>
								
								<!-- Contact number -->
								<div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
									<label class="control-label">Contact number
									    <span class="color-red">*</span>
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="phone" type="text"  placeholder="phone number" value="<?php echo !empty($phone)?$phone:'';?>">
										<?php if (!empty($phoneError)): ?>
										<span class="help-inline"><?php echo $phoneError;?></span>
										<?php endif; ?>
									</div>
								</div>
								<!-- E-mail -->
								<div class="control-group <?php echo !empty($emailError)?'error':'';?>">
									<label class="control-label">E mail
									    <span class="color-red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px"  class="form-control margin-bottom-10" id="email" name="email" type="text"  placeholder="E-mail" value="<?php echo !empty($email)?$email:'';?>">
										<?php if (!empty($emailError)): ?>
										<span class="help-inline"><?php echo $emailError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- Street -->
								<div class="control-group <?php echo !empty($StreetError)?'error':'';?>">
									<label class="control-label">Street</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="Street" name="Street" type="text"  placeholder="Street" value="<?php echo !empty($Street)?$Street:'';?>">
										<?php if (!empty($StreetError)): ?>
										<span class="help-inline"><?php echo $StreetError;?></span>
										<?php endif; ?>
									</div>
								</div>
														
								<div class="control-group <?php echo !empty($SuburbError)?'error':'';?>">
										<label class="control-label">Suburb</label>
										<div class="controls">
							<input style="height:30px" class="form-control margin-bottom-10" id="Suburb" name="Suburb" type="text"  placeholder="Suburb" value="<?php echo !empty($Suburb)?$Suburb:'';?>">
											<?php if (!empty($SuburbError)): ?>
											<span class="help-inline"><?php echo $SuburbError;?></span>
											<?php endif; ?>
										</div>
								</div>	
								
								<div class="control-group <?php echo !empty($CityError)?'error':'';?>">
									<label class="control-label">City/Town/Township</label>   
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" id="City" name="City" type="text"  placeholder="City" value="<?php echo !empty($City)?$City:'';?>">
										<?php if (!empty($CityError)): ?>
										<span class="help-inline"><?php echo $CityError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<label>Province</label>
                                <SELECT class="form-control" id="State" name="State" size="1">
									<OPTION value="EC">Eastern Cape</OPTION>
									<OPTION value="FS">Free State</OPTION>
									<OPTION value="GP">Gauteng</OPTION>
									<OPTION value="LP">Limpopo</OPTION>
									<OPTION value="MP">Mpumalanga</OPTION>
									<OPTION value="NW">North West</OPTION>
									<OPTION value="NC">Northern Cape</OPTION>
									<OPTION value="WC">Western Cape</OPTION>
								</SELECT>
								<br/>
								<div class="control-group <?php echo !empty($PostCodeError)?'error':'';?>">
									<label class="control-label">Postal Code
										<span class="color-red">*</span>
									</label>
									<div class="controls">
										<input style="height:30px" class="form-control margin-bottom-10" name="PostCode" type="text"  placeholder="Postal Code" value="<?php echo !empty($PostCode)?$PostCode:'';?>">
										<?php if (!empty($PostCodeError)): ?>
										<span class="help-inline"><?php echo $PostCodeError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<div class="control-group <?php echo !empty($DobError)?'error':'';?>">
								<label class="control-label">Date of birth
									<span class="color-red">*</span>
								</label>
									<div class="controls">
									<input style="height:30px" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="Dob" type="Date" placeholder="yyyy-mm-dd" value="<?php echo !empty($Dob)?$Dob:'';?>">
									<?php if (!empty($DobError)): ?>
									<span class="help-inline"><?php echo $DobError;?></span>
									<?php endif; ?>
									</div>
								</div>
								
								     <!-- </div> -->
<!------------------------------------------------------------ Banking Details --------------------------------------------------------->
							<div class="tab-pane fade in" id="sample-3b">
								<div class="panel-heading">
									<h2 class="panel-title">
                                         <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                                                Banking  Details
                                         </a>
                                    </h2>		
								</div>
								
							<!-- Account Holder Name -->
							<label>Account Holder Name</label>
								<div class="control-group <?php echo !empty($AccountHolderError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountHolder" name="AccountHolder" type="text"  placeholder="Account Holder Name" value="<?php echo !empty($AccountHolder)?$AccountHolder:'';?>" />
									
									<?php if (!empty($AccountHolderError)): ?>
										<span class="help-inline"><?php echo $AccountHolderError;?></span>
										<?php endif; ?>
									</div>	
								</div>	
							<!-- Bank Name -->
							<label>Bank Name</label>
								<div class="control-group <?php echo !empty($BankNameError)?'error':'';?>">
									<div class="controls">
										<div class="controls">
										  <SELECT class="form-control" id="BankName" name="BankName" size="1">
											<OPTION value="ABSA">ABSA</OPTION>
											<OPTION value="capitec">Capitec</OPTION>
											<OPTION value="StandardBank">Standard Bank</OPTION>
											<OPTION value="fnb">First National Bank</OPTION>
											<OPTION value="nedbank">NedBank</OPTION>
										  </SELECT>
										</div>	
								</div>
								</div>	
							<!-- Account Number  -->
							<label>Account Number
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($AccountNumberError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="AccountNumber" name="AccountNumber" type="text"  placeholder="Account Number e.g 201511" value="<?php echo !empty($AccountNumber)?$AccountNumber:'';?>" />
									
									<?php if (!empty($AccountNumberError)): ?>
										<span class="help-inline"><?php echo $AccountNumberError;?></span>
										<?php endif; ?>
									</div>	
								</div>
								
							<!-- Account Type  -->
							<label>Account Type
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($AccountTypeError)?'error':'';?>">
									<div class="controls">
										 <SELECT class="form-control" id="AccountType" name="AccountType" size="1">
											<OPTION value="Cheque">Cheque</OPTION>
											<OPTION value="Savings">Savings</OPTION>
											<OPTION value="Current">Current</OPTION>
										</SELECT>
									</div>	
								</div>
								
							<!-- Branch Code  -->
							<label class="control-label">Branch Code
							<span class="color-red">*</span></label>
								<div class="control-group <?php echo !empty($BranchCodeError)?'error':'';?>">
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="BranchCode" name="BranchCode" type="text"  placeholder="Branch Code e.g 632005" value="<?php echo !empty($BranchCode)?$BranchCode:'';?>" />
									
									<?php if (!empty($BranchCodeError)): ?>
										<span class="help-inline"><?php echo $BranchCodeError;?></span>
										<?php endif; ?>
									</div>	
								</div>
							</div>	
<!------------------------------------------------------------ End Banking Details --------------------------------------------------------->
									<!-- Heading 2 -->
								<div class="panel-heading">
											
											
											<h2 class="panel-title">
                                            <a class="accordion-toggle" href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                                                Loan Application Details
                                            </a>
                                            </h2>
											
								</div>
								
								<label>Loan Type</label>
                                <SELECT class="form-control" id="loantype" name="loantype" size="1" onChange="valueInterest(this);">
									<!-------------------- BOC 2017.04.15 - dataLoanType --->	
											<?php
											$loantypeid = '';
											$loantypeSelect = $loantype;
											$loantypedesc = '';
											$loantypevalue = 0;
											$loantypevalue = $row['percentage'];
											
											foreach($dataLoanType as $row)
											{
											  // -- Loan Type of a selected Bank
												$loantypeid = $row['loantypeid'];
												$loantypedesc = $row['loantypedesc'];						
												
												// -- Select the Selected Bank 
											  if($loantypeid == $loantype)
											   {
													echo "<OPTION value=$loantypeid selected>$loantypedesc</OPTION>";
											   }
											  else
											   {
												   echo "<OPTION value=$loantypeid>$loantypedesc</OPTION>";
											   }
											}
											
								
											if(empty($dataLoanType))
											{
												echo "<OPTION value=0>No Loan Type</OPTION>";
											}
											?>
								<!-------------------- EOC 2017.04.15 - dataLoanType --->
								</SELECT>
								
								<br>
								<div class = "input-group">
								<label>Interest rate (%)
                                </label>
                                <!-- update 27.01.2017 personal loan interest rate, Id="InterestRate" -->
                                <input style="height:30px" readonly value="<?php echo $personalloan;?>" id="InterestRate" 
								name="InterestRate" class="form-control margin-bottom-10" type="text">
								</div> 
								
								<br>
								
								<label>Monthly Income (e.g. 20000.00)
								
								</label>
								<div class="control-group <?php echo !empty($MonthlyIncomeError)?'error':'';?>">
									<div class = "input-group">
										<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyIncome" name="MonthlyIncome" type="text"  placeholder="Monthly Income(eg 20000.00)" value="<?php echo !empty($MonthlyIncome)?$MonthlyIncome:0.00;?>">
									</div>
									<?php if (!empty($MonthlyIncomeError)): ?>
										<span class="help-inline"><?php echo $MonthlyIncomeError;?></span>
										<?php endif; ?>
								</div>

								<br>

								<label class="control-label">Monthly Expenditure(e.g. 20000.00)
									
								</label>

								<div class="control-group <?php echo !empty($MonthlyExpenditureError)?'error':'';?>">
									<div class = "input-group">
										<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="MonthlyExpenditure"  name="MonthlyExpenditure" type="text"  placeholder="Monthly Expenditure(e.g. 20000.00)" value="<?php echo !empty($MonthlyExpenditure)?$MonthlyExpenditure:0.00;?>"/>
										
									</div>
									<?php if (!empty($MonthlyExpenditureError)): ?>
									<span class="help-inline"><?php echo $MonthlyExpenditureError;?></span>
									<?php endif; ?>
								</div>
								
								<br>
								
								<label class="control-label">Required Loan Value(e.g. 20000.00)
								
								
								
								</label>
								<div class="control-group <?php echo !empty($ReqLoadValueError)?'error':'';?>">
									<div class = "input-group">
									<span class = "input-group-addon">R</span>
										<input  style="height:30px" class="form-control margin-bottom-10" id="ReqLoadValue" name="ReqLoadValue" type="text"  placeholder="Required Loan Value(e.g. 20000.00)" value="<?php echo !empty($ReqLoadValue)?$ReqLoadValue:0.00;?>" onchange="CalculateRepayment()">
										
									</div>
									<?php if (!empty($ReqLoadValueError)): ?>
										<span class="help-inline"><?php echo $ReqLoadValueError;?></span>
								<?php endif; ?>
								</div>
									
								<!--<div class = "input-group">
								  <span class = "input-group-addon">R</span>				
                                <input placeholder="Monthly Disposable Income (e.g. 20000.00)" name="MonthlyDispIncome" class="form-control margin-bottom-10" type="text">
								</div> -->

								<!-- <label>Liabilities(eg 20000.00)
                                    <span class="color-red">*</span>
                                </label>
                                <input name="Liabilities" class="form-control margin-bottom-10" type="text">
								-->
								<br>
								<div class="control-group <?php echo !empty($SuretyError)?'error':'';?>">
									<label class="control-label">Surety Assets(e.g. Golf GTi , iPhone 5)</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" id="Surety" name="Surety" type="text"  placeholder="Surety Assets(e.g. Golf GTi , iPhone 5)" value="<?php echo !empty($Surety)?$Surety:'';?>">
										<?php if (!empty($SuretyError)): ?>
										<span class="help-inline"><?php echo $SuretyError;?></span>
										<?php endif; ?>
									</div>
								</div>
															
								<!--<label>Liquidity (eg 20000.00)
                                    <span class="color-red">*</span>
                                </label>
                                <input name="Liquidity" class="form-control margin-bottom-10" type="text">
								-->
								<!-- Start Loan Duration -->
								<div class="control-group <?php echo !empty($LoanDurationError)?'error':'';?>">
									<label class="control-label">Loan Duration (Please enter as number of months - e.g. 36)
									<span class="color-red">*</span>
									</label>
									<div class="controls">
										<input  style="height:30px" class="form-control margin-bottom-10" name="LoanDuration" id="LoanDuration"  type="text"  placeholder="Loan Duration" value="<?php echo !empty($LoanDuration)?$LoanDuration:0.00;?>" onchange="LastPaymentDate()">
										<?php if (!empty($LoanDurationError)): ?>
										<span class="help-inline"><?php echo $LoanDurationError;?></span>
										<?php endif; ?>
									</div>
								</div>
								
								<!-- End Loan Duration -->
								
								 <label>Total Repayment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  style="height:30px" readonly id="Repayment" name="Repayment" value="<?php echo !empty($Repayment)?$Repayment:0.00;?>" class="form-control margin-bottom-10" type="text">
							    </div>
								<br>
								
								<label>Monthly Payment Amount
                                     </label>
								 <div class = "input-group">
									<span class = "input-group-addon">R</span>	  
                                     <input  style="height:30px" readonly id="monthlypayment" name="monthlypayment" value="<?php echo !empty($monthlypayment)?$monthlypayment:0.00;?>" class="form-control margin-bottom-10" type="text">
							    </div>
								<br>
								
								<label>Frequency of payment</label>
                                <SELECT class="form-control" id="paymentfrequency" name="paymentfrequency" size="1">
									<OPTION value="MON">Monthly</OPTION>
								</SELECT>
									<br>
								<label>Payment method</label>
                                <SELECT class="form-control" id="paymentmethod" name="paymentmethod" size="1">
									<OPTION value="EFT">EFT</OPTION>
									<OPTION value="EFT">Debit Order</OPTION>
								</SELECT>
								<br>
								
							   <div class="control-group <?php echo !empty($firstpaymentError)?'error':'';?>">
								<label class="control-label">First Payment Date
									<span class="color-red">*</span>
								</label>
									<div class="controls">
									<input  style="height:30px" id="FirstPaymentDate" class="form-control margin-bottom-10" class="form-control margin-bottom-10" name="FirstPaymentDate" placeholder="yyyy-mm-dd" type="Date"  value="<?php echo !empty($firstpayment)?$firstpayment:'';?>" onchange="LastPaymentDate()">
									<?php if (!empty($firstpaymentError)): ?>
									<span class="help-inline"><?php echo $firstpaymentError;?></span>
									<?php endif; ?>
									</div>
								</div>
							   
							   
							   <label>Last Payment Date 
							   </label>
                               <input  style="height:30px" readonly id="lastPaymentDate" value="<?php echo !empty($lastPaymentDate)?$lastPaymentDate:'';?>" name="lastPaymentDate" placeholder="yyyy-mm-dd" class="form-control margin-bottom-10" type="date" />
					
								<!-- End - Accordion - Alternative -->
								<?php 
								$classname = "control-group error";
								$html = "";
								
								//session_start();  
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
                                       // echo "<a href='applicationForm.php' class='fa-gears'>Apply for e-loan</a>";
									}
									else
									{									
								echo "<div class='panel-heading'>
											<h2 class='panel-title'>
                                            <a class='accordion-toggle' href='#collapse-Two' data-parent='#accordion' data-toggle='collapse'>
												Register a new account
                                            </a>
                                            </h2>
								      </div>

									  <div class='input-group margin-bottom-20'>
                                        <span class='input-group-addon'>
                                            <i class='fa fa-user'></i>
                                        </span>
                                        <input  style='height:30px' readonly id='Userid' value='$idnumber' name='Username' placeholder='ID number' class='form-control' type='text'>
									 </div>
									 
									 <label>Password
                                            <span class='color-red'>*</span>
                                     </label>";
									 if(!empty($password1Error))
									 { 
									 echo "<div class='control-group error'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password'>
									 <span class='help-inline'>$password1Error</span></div>";
									 }
									 else
									 {
									  echo "<div class='control-group'><input style='height:30px' id='password1' name='password1' class='form-control margin-bottom-20' type='password' value=$password1></div>";
									 }	
									// -- New Captcha
									 echo "<label class='' for='captcha'>Please enter the verification code shown below.</label>";
									 echo "<div id='captcha-wrap'>
											<input type='image' src='assets/img/refresh.jpg' alt='refresh captcha' id='refresh-captcha' /><img src='data:image/png;base64,".base64_encode(Image('refresh'))."' alt='' id='captcha' /></div>";
									 if(!empty($captchaError))
									 {
									 echo "<div class='control-group error'>
											<input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
											<span class='help-inline'>$captchaError</span></div>";
									 }
									 else
									 {
									 $code = '';
									 echo "<div class='control-group'>
									 <input style='height:30px' class='form-control margin-bottom-20' id='captcha' name='captcha' type='text' placeholder='Verification Code' value=$captcha>
									</div>";
									 }
									 // -- End Captcha
					
								}
								?>
								<!-- Capture 
								<input type="hidden" id="captchaId" name="captchaId" value="" />
								<img id="siimage" src="" alt="captcha image" />
								<input type="text" name="captcha_code" value="" />-->
								<!-- Register End here -->
                                <hr>
                                <div class='row'>
                                    <div class='col-lg-8'>
                                        <label class='checkbox'>
                                            <input type='checkbox'>I read the
                                            <a href='#'>Terms and Conditions</a>
                                        </label>
                                    </div>
                                    <div class='col-lg-4 text-right'>
                                        <button class='btn btn-primary' id="Apply" name="Apply" type='submit'>Apply</button>
                                    </div>
                                </div>
							 </form> 	
						</div>	
                   </div>	
				</div>	
<?php  
// -- Database Connections  

//require 'database.php'; 
if(isset($_POST['Apply']))  
{  
	 $pdo = Database::connectDB(); 
	 $userid = $_POST['idnumber']; 
	 $password = $_POST['password1']; 
	 $check_user = "SELECT * FROM $tbl_user Inner Join $tbl_customer on $tbl_user.userid = $tbl_customer.customerid  WHERE userid='$userid' and password='$password'";
	 
	 mysql_select_db("$db_eloan",$pdo)  or die(mysql_error());
	 $result=mysql_query($check_user,$pdo) or die(mysql_error());

// Mysql_num_row is counting table row
	$count = mysql_num_rows($result);
	
	if($count >= 1)
    {  
	$row = mysql_fetch_array($result);
  			$_SESSION['loggedin'] = true;
			$_SESSION['username'] = $userid;
			$_SESSION['password'] = $password;//here session is used and value of $user_email store in $_SESSION.  
			
			// Title
			$_SESSION['Title'] = $row['Title'];	
			// First name
			$_SESSION['FirstName'] = $row['FirstName'];
			// Last Name
			$_SESSION['LastName'] = $row['LastName'];
			// Phone numbers
			$_SESSION['phone'] = $row['phone'];
			// Email
			$_SESSION['email'] = $row['email'];
			// Role
			$_SESSION['role'] = $row['role'];
			// Street
			$_SESSION['Street'] = $row['Street'];

			// Suburb
			$_SESSION['Suburb'] = $row['Suburb'];

			// City
			$_SESSION['City'] = $row['City'];
			// State
			$_SESSION['State'] = $row['State'];
			
			// PostCode
			$_SESSION['PostCode'] = $row['PostCode'];
			
			// Dob
			$_SESSION['Dob'] = $row['Dob'];
			
			// phone
			$_SESSION['phone'] = $row['phone'];
			
			
     echo "<script>window.open('index','_self')</script>";  

    }  
    /* else  
    {  
          echo "<script>alert('Email or password is incorrect!')</script>";  
*/
		  
    }   */
} 
?>  				  				
<script>
// -- 15.07.2017 - Banking Details ---
 // -- Enter to capture comments   ---
 function valueselect(sel) 
 {
		var bankid = sel.options[sel.selectedIndex].value;      
		var selectedbranchcode = document.getElementById('BranchCode').value;
		
		jQuery(document).ready(function(){
		jQuery.ajax({  type: "POST",
					   url:"SelectBranchCodes.php",
				       data:{bankid:bankid,branchcode:selectedbranchcode},
						success: function(data)
						 {	
						// alert(data);
							jQuery("#BranchCode").html(data);
						 }
					});
				});				  
			  return false;
  }
  
  // -- Different Loan Types.
 function valueInterest(sel) 
 {
  alert("Modise");
		var interest = sel.options[sel.selectedIndex].getAttribute('data-vogsInt');   
alert(interest);			
		document.getElementById('InterestRate').value = interest;
 }		
// -- EOC Different Loan Types.
// -- 15.07.2017 - Banking Details ---  
</script> 				  