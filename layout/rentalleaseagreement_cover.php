<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<h2 style="text-align: center;">Agreement of the Lease</h2>
<h2 style="text-align: center;">Memorandum of agreement entered Into by and between</h2>
<h2 style="text-align: center;">Intelli Property Management (Pty) Ltd</h2>
<h2 style="text-align: center;">of</h2>
<h2 style="text-align: center;">Clearwater Office Park</h2>
<h2 style="text-align: center;">Strubens Valley</h2>
<h2 style="text-align: center;">Johannesburg</h2>
<h2 style="text-align: center;">1735</h2>
<h2 style="text-align: center;">(“The Lessor”)</h2>
<h2 style="text-align: center;">and</h2>
<h2 style="text-align: center;">[title][firstname][lastname]</h2>
<h2 style="text-align: center;">Id Number: [customerId]</h2>
<h2 style="text-align: center;">for</h2>
<h2 style="text-align: center;">[rentalpropertyname]</h2>
<h2 style="text-align: center;">[street]</h2>
<h2 style="text-align: center;">[suburb]</h2>
<h2 style="text-align: center;">[postalcode]</h2>
