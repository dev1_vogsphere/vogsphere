<h3>RULES</h3>
<ol>
	<li>Smoking, Drugs and Alcohol
		<ul>
			<li>Our Properties are smoke free. Smoking, drinking or the taking of drugs is strictly prohibited within your room or in the common areas of the building, except for designated areas inside the buildings.
			</li>
			<li>Consumption and storage of alcohol is not allowed in your    room or in the building. Selling, consumption and storage of     any prescription drugs or narcotics is completely prohibited in   our Properties and in the Rooms.
			</li>
			<li>Should alcohol or any form of drugs be found on your person or in your Rooms, we reserve the right to immediately terminate the lease agreement and take whatever legal action that may be necessary against you.
			</li>
		</ul>
	</li>

<li>Consideration For Others
<ul>
	<li>Be considerate to your fellow residents.  Disorderly behaviour that results in the disturbance of other tenants may result in the termination of the lease agreement. In the  event  this  happens  you  will  be  liable  for  all  charges  set  out  in  the  lease agreement.
	</li>
	<li>Playing of music or other sounds at unreasonable times is strictly prohibited.  INTELLI PROPERTY MANAGEMENT reserves  the  right  to  confiscate  such  equipment,  which  shall  be  returned  to  the tenant at the end of the year.
	</li>
</ul>
<li>Medical Conditions and Disabilities
<ul>
	<li>	Should you suffer from any medical condition   or disability,  we have attached medical emergency numbers for you to call.
	</li>
</ul>
<li>Security and Safety
<ul>
	<li>We cannot be held responsible for any losses that you may incur whilst residing in our Property. In light of this, you are advised to please keep your room locked at all times and ensure that the windows are fully closed when you are not present in your 
	room.
	</li>
	<li>
	The key for your room must never be given or lent to any other person. Always keep 
	your keys in a safe place wherever you are, we will not be held responsible for any theft or burglary involving your possessions.
	</li>
	<li>Should your keys be lost you will be held responsible for the cost of and issuing of replacement of new keys (approximately R300).
	</li>
	<li>Tampering with any security features is a serious offence and is strictly forbidden. Should you tamper with the anything in any way, we reserve our right to press charges against you and have you arrested. You will also be liable for the cost of replacements.
	</li>
	 <li>The use of candles, oil lamps, incense or anything that produces a naked flame is a potential fire hazard and is strictly forbidden to be used in your Rooms or any part of the Property.
	</li>
	<li>We reserve the right to search your bags and your visitors at any time and confiscate any drugs, alcohol, firearms, weapons and any illegal substances.
	</li>
</ul>
</li>
<li>Care of the Property, Common Areas and Units
<ul>
	<li>You must keep your room clean and tidy at all times.
	</li>
	<li>The  furniture  provided  in your  room  may  not  be  removed  from  your  room  or the property  without  our prior consent. You will be charged for the full cost of repairs should any damage occur as a result of this.
	</li>
	<li>Dustbins will be provided in the kitchen, please throw all stale, unfinished and waste food into the dustbins provided in the kitchen.
	</li>
	<li>All rooms will be inspected at least twice a month. Any damage found to the room or furniture upon inspection will be billed directly to you on a month to month basis. You will ensure that prior to vacating the room for weekends, vacations or otherwise, that your room is cleaned and that any form of food or drink is stored away or thrown out and that pots, pans, cups and plates are cleaned. We reserve the right to clean your room in your absence and hold you accountable for costs incurred and we will not be responsible for any losses.
	</li>
	<li>Throw all rubbish in the dustbins provided.
	</li>
	<li>Keep the kitchens and bathrooms clean.
	</li>
	<li>Clothes or other items may not be hung from windows or balconies of the property, doing
	so will result in a fine imposed by the Administration Office.
	</li>
</ul>
</li>

<li>Water and Electricity usage
<ul>
	<li>Included  in your rent is an amount  for water.  A prepaid electricity amount is not allocated and must therefore be bought by the tenants residing there. Please make sure you use electricity wisely. Please switch of all lights and appliances while not in use, and ensure that all stove plates and ovens are turned OFF when not in use. 
	<li>No visitors are allowed to spend electricity of other tenants without arranging with other tenants about your visitor. Should a tenant complain about the usage of the electricity by your visitor it will affect your electricity
	</li>
	<li>Water should be used sparingly and any leaks should be reported to the INTELLI PROPERTY MANAGEMENT Caller Number of 0657269473.
	</li>
	<li>Cooking in your rooms is strictly forbidden.
	</li>
	<li>Ironing on your beds is strictly prohibited.
	</li>
</ul>
</li>
<li>Visitors
<ul>
	<li>	Visitors are welcome provided they observe the INTELLI PROPERTY MANAGEMENT Tenants RULES and the lease agreement.
	</li>
	<li>No visitors are allowed to remain in the building without you being present. You will be held wholly responsible and financially liable for any damages caused by your visitor.
	</li>
	<li>Sleeping overnight is limited to two days and electricity shall be compensated accordingly, should we find out that you allowed  a visitor to sleep more than those days you will be charged a penalty fee of R200 per visitor per night.
	</li>
</ul>
</li>
<li>General
<ul>
	<li>No meetings or demonstrations or any advertising of any organisation will be permitted without prior approval from INTELLI PROPERTY MANAGEMENT.
	</li>
	<li>Pamphlets  or  any  form  of  communications  must  not  be  handed  out  without  prior consent from INTELLI PROPERTY MANAGEMENT.
	</li>
	<p style="color:red">FAILURE TO OBSERVE THESE RULES WILL CONSTITUTE BREACH OF THE LEASE AGREEMENT AND MAY RESULT IN TERMINATION OF THE LEASE WITHOUT COMPENSATION.</p>
</ul>
</li>
</ol>	



