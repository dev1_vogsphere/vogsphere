<style>
th{font-weight: bold;}
</style>
<table class="table table-borderless">

<tr><th colspan="3"></th></tr>
<br/>
	  <tr>
        <th colspan="3">THE FOLLOWING INFORMATION IS INCORPORATED INTO THE CONDITIONS OF THE LEASE</th>
	  </tr>
    <tbody>
      <tr>
        <td>LEASE</td>
        <td>PERIOD</td>
		<td>[leasedurarion] Months</td>
      </tr>
      <tr>
		<td></td>
        <td>START DATE</td>
        <td>[desiredate]</td>
      </tr>
      <tr>
		<td></td>
	    <td>END DATE</td>
        <td>[enddate]</td>
      </tr>
	  <tr>
        <td>RENTAL</td>
        <td>RENTAL AMOUNT</td>
		<td>R[rentalpropertyprice]</td>
      </tr>
	 <!-- <tr>
		<td></td>
	    <td>TOTAL</td>
        <td>R[total]</td>
      </tr>-->
	  <tr>
		<td>DEPOSIT</td>
	    <td>RENTAL DEPOSIT</td>
        <td>R[rentalpropertyprice]</td>
      </tr>
	  <tr>
		<td>RENTAL INCREASE DATE</td>
	    <td>[increaserate]</td>
        <td></td>
      </tr>
	  <tr>
		<td>ESCALATION RATE</td>
	    <td>[rate]%</td>
        <td></td>
      </tr>
	  <tr>
		<th colspan="3">PURPOSES FOR WHICH THE LEASED PROPERTY WILL BE USED</th>
      </tr>
	  <tr>
		<td>TYPE OF BUSINESS</td>
	    <td>[typeofbusiness]</td>
        <td></td>
      </tr>
	  <tr>
		<td>SPECIFIC INCLUSIONS</td>
	    <td>[specialinclusions]</td>
        <td></td>
      </tr>
	  <tr>
		<td>LEASE COSTS</td>
	    <td>R[leasecost]</td>
        <td></td>
      </tr>
	  <tr>
		<td>PENALTY FEES</td>
	    <td>R[penaltyfees]</td>
        <td></td>
      </tr>
	  <tr>
		<th colspan="3">ACCOUNT DETAILS</th>
      </tr>
	  <tr>
		<td>ACCOUNT NAME</td>
	    <td>[accountholdername]</td>
        <td></td>
      </tr>
	  <tr>
		<td>ACCOUNT TYPE</td>
	    <td>[accounttype]</td>
        <td></td>
      </tr>
	  <tr>
		<td>ACOUNT NUMBER</td>
	    <td>[accountnumber]</td>
        <td></td>
      </tr>
	  <tr>
		<td>BANK NAME</td>
	    <td>[bankname]</td>
        <td></td>
      </tr>
	  <tr>
		<td>BRANCH CODE</td>
	    <td>[branchcode]</td>
        <td></td>
      </tr>
	  <tr>
		<th colspan="3">RESIDENTIAL ADDRESS</th>
      </tr>
	  <tr>
		<td>PREVIOUS RESIDENTIAL ADDRESS</td>
	    <td colspan="2">[previousaddress]</td>
      </tr>
	  <tr>
		<td>ALTERNATIVE ADDRESS</td>
	    <td colspan="2">[alternativeaddress]</td>
      </tr>
	  <tr>
		<td>PRESENT  ADDRESS</td>
	    <td colspan="2">[presentaddress]</td>
      </tr>
    </tbody>
</table>
