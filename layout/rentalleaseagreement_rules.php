<li><b>RULES</b>
	<ol>
		<li>INTELLI PROPERTY MANAGEMENT has imposed RULES relating to the occupants of the Apartments/Rooms/Houses in the Apartment Block which rules are aimed at protecting the safe and equal enjoyment of the use of the Apartment and/or Room and/or House and the Apartment Block or any place of residence.</div>
		<li>By signing this Agreement the Tenant agrees to be bound by the RULES relating to the  Apartment/Room/House  and  the  Apartment  Block  and  agrees  that  he/she  as well  as his/her guest will at all times obey the RULES.</div>
		<li>Failure to comply with the RULES will constitute a breach of this Agreement and should INTELLI PROPERTY MANAGEMENT cancel this Agreement due to the Tenant’s breach of the RULES, the Tenant will forfeit any rental paid for the month as well as his/her deposit.</div>
		<li>The Tenant undertakes not to breach any of the terms, conditions or restrictions of the township in which the Apartment/Room/House is situated or by-laws of the local authority or any of the terms and conditions of INTELLI PROPERTY MANAGEMENT’s title deed for the Apartment/Room/House.</div>
	</ol>
  </li>
<li><b>MAINTENANCE & REPAIR Tenant</b>
	<ol>
	<li>The Tenant will maintain the interior of the Apartment and/or Room and/or House including but not limited to windows, doors, accessories, fixtures and fittings in a good condition.   If the Tenant fails to maintain the Apartment/Room/House  INTELLI PROPERTY MANAGEMENT will be entitled, without prejudice to any of its other rights or remedies, to effect the necessary maintenance, repairs or replacements and to recover the cost thereof from the Tenant on demand.
	</li>
	<li>On the termination of this agreement, the Tenant will return the Apartment/Room/House and all parts thereof (including all keys) to INTELLI PROPERTY MANAGEMENT in a good order
        and condition, fair wear and tear excepted.
	</li>
	<li>The Tenant will not be entitled to drive nails or screws into the walls or ceilings of 
	the Apartment and/or Room.
	</li>
	<li>The Tenant will not make any alterations or additions to the Apartment and/or Room or to any installation therein without INTELLI PROPERTY MANAGEMENT’s written consent. If INTELLI PROPERTY MANAGEMENT does consent to any alterations and additions being made to the Apartment and/or Room the alterations and additions will belong to INTELLI PROPERTY MANAGEMENT and the Tenant will not be entitled to remove them from the Apartment and/or Room.   The Tenant will not under any circumstances have any claim against  INTELLI PROPERTY MANAGEMENT  for  compensation  for  any  improvement  to  the  Apartment  and/or Room or have a right of retention in respect of any improvements.
	</li>
	<li>The Tenant will not bring any goods,  furniture  or effects  in to the Apartment  and/or Room  and/or  the  Apartment  Block  which  may  by  their  nature  increase  the  rate  of insurance premiums payable by INTELLI PROPERTY MANAGEMENT or vitiate the fire 
	insurance policy held by INTELLI PROPERTY MANAGEMENT or which may be impregnated by any wood borer, termite, or any other wood destroying insect of any kind.
	</li>
	<li>The Tenant undertakes not  to in any  manner  whatsoever  interfere  with  the  present electrical  installations  in  the  Apartment  and/or  Room  and/or  in  the  Apartment  Block without  INTELLI PROPERTY MANAGEMENT’s  consent.  The Tenant will under no circumstances   connect electric lamps, motor or heaters not specifically 
	designed for use for the electric current supplied to the Apartment/Room/House.
	</li>
	<li>Within  (7)  days  after  having  taken  possession  of  the  Apartment  and/or  Room,  the Tenant will notify INTELLI PROPERTY MANAGEMENT in writing of any repairs that need to be done to the Apartment and/or Room or if any part thereof, including any missing, damaged or out of order lock, key, door, window, accessory, fixture or fitting.  If the Tenant does not give such notice, INTELLI PROPERTY MANAGEMENT will be entitled to accept that the Apartment and Room and all parts thereof were intact, in place, and in good order and condition when the Tenant took possession of the Apartment/Room/House. SHOULD THE PLACE LOOK OTHERWISE ON TERMINATION OF THE CONTRACT, THE DEPOSIT SHALL BE UTILISED FOR THE REPAIR OF THE PLACE.
	</li>
	<h3>INTELLI PROPERTY MANAGEMENT</h3>
	<li>INTELLI PROPERTY MANAGEMENT will be responsible for the maintenance of the Apartment Block except for those parts of the Apartment Block which are the responsibility of tenants or the local authority. INTELLI PROPERTY MANAGEMENT’ obligations in this respect will include the maintenance  and repair of the structure of the Apartment Block, all systems, works and installations contained therein, the roofs, the exterior walls, the lifts, the grounds and gardens, and all other parts of the common areas.
	</li>
	</ol>
</li>
<li>
<b>INTELLI PROPERTY MANAGEMENT’ ACCESS</b>
<p>INTELLI PROPERTY MANAGEMENT’s representatives, agents, servants and contractors are entitled to at all reasonable times enter   the Apartment   Block   in  order   to  inspect   and  to  carry   out  any  necessary   repairs, replacements,  or other works, or to 
perform any other lawful function in the bona fide interests of INTELLI PROPERTY MANAGEMENT, the Tenant or any of the occupiers of the Apartment Block.   INTELLI PROPERTY MANAGEMENT will ensure that this right is exercised with due  regard  for,  and  a  minimum  of  interference   with,  the  beneficial enjoyment of the Tenant and the other occupants of the Apartment Block.</p>
</li>
<li><b>DESTRUCTION</b>
<p>Should the Apartment Block in which the Apartment/Room/House  is situated  be destroyed  by fire or through  any other cause during the period of this agreement  and become  untenable,  then this agreement   will  be  terminated.   Should   only the Apartment/Room/House   be damaged   or partially destroyed by fire or through any other cause while the Apartment Block remains tenantable then this agreement will not be terminated, and the Tenant will continue to pay the rent payable</p>
</li>
<li>
<b>ACCESS AND SECURITY</b>
<p>INTELLI PROPERTY MANAGEMENT  will  be  entitled  to  install  such  access  security  to  the  Apartment  and/or  Room  and/or Apartment  and/or House   it may in its sole discretion  decide and the Tenant will be obliged to comply with any procedures or rules relating to that security.</p>
</li>
<li>
<b>INDEMNITY</b>
<p>The Tenant will have no claim for damages against INTELLI PROPERTY MANAGEMENT howsoever and whenever arising because of negligence or gross negligence on the part of INTELLI PROPERTY MANAGEMENT or its directors, representatives, agents, servants, contractors and invitees.</p>
</li>

<li><b>CANCELLATION OF LEASE</b>
	<ol>
		<li>Should the Tenant default in any payment due under this Agreement or be in breach of any of the other terms of this agreement, INTELLI PROPERTY MANAGEMENT will be entitled to cancel this agreement without notice and with immediate effect, to repossess  the  Apartment/Room/House   and to recover all damages for the default or breach  and the cancellation  of this Agreement from the Tenant.
		</li>
		<li>Clause 15.1 will not be construed as excluding the ordinary lawful 
		consequences  of a breach of this Agreement by either party (save any such consequences  as are expressly excluded by any of the other provisions of this Agreement)  and in particular any right of cancellation  of this lease  on the ground  of a material  breach  going to the root of this Agreement.
		</li>	
		<li>
		Should INTELLI PROPERTY MANAGEMENT consult with and/or instruct its attorneys with regard to any breach of this Agreement by the Tenant, the Tenant will be liable for all the fees and disbursements of the said attorneys as between attorney and own-client whether or not steps for relief are initiated.
		</li>
	</ol>
</li>	
<li><b>DOMICILIUM</b>
	<ol>
		<li>The  parties  choose  as  their  domicilia  citandi  et  executandi  (their  address  for  legal notices)  the  addresses  mentioned  below,  but  such  address  of  either  party  may  be changed by written notice from such party to the other party with effect from the date of receipt or deemed receipt by the latter of such notice:
			Correspondence to be directed to the Managing  Agent which  for the purposes  of this agreement  will  be  INTELLI PROPERTY MANAGEMENT  ,  whose  contact  details  are  as follows:
			INTELLI PROPERTY MANAGEMENT (PTY) LTD Reg: 2020/468800/07
			Clearwater Office Park, Strubens Valley, Johannesburg, 1735
			Tel:  0510121012	 
			Email Address:  info@intellipropertymanagement.com
		</li>
	</ol>
<h3>THE TENANT:</h3> <p>The Apartment/Room/House.</p>	
</li>

<li><b>DPARENT OR GUARDIAN TO SIGN AS SURETY AND CO-PRINCIPAL DEBTOR</b>
	<ol>
		<li>The Surety  by  his/her  signature  do  hereby  binds  him/herself  jointly  and  severally  as surety and co-principal debtor with the Tenant for all of the Tenant’s  obligations arising under this agreement.
		</li>
		<li>The Surety and Co-principal Debtor choose as his/her domicilia citandi et executandi 
		for all purposes under this agreement the address specified in the Schedule.
		</li>
	</ol>
</li>

<li><b>DEPARTURE FROM RESIDENCE</b>
	<ol>
		<li>The Tenant hereby agrees that should he/she remain in occupation after termination or expiry of this Agreement, they shall be liable for occupational rent at the standard  
			rate applied by INTELLI PROPERTY MANAGEMENT for accommodation at the Apartment  Block /House for each  month  or part thereof that the Tenant remains in occupation.
		</li>
		<li>The Tenant agrees that it is his/her duty to remove all his/her own property from the INTELLI PROPERTY MANAGEMENT premises on termination of the Lease. The Tenant will have no claim against the Lessor for any property left behind or abandoned at the Apartment Block.
		</li>
	</ol>
</li>	


<li><b>GENERAL</b>
	<ol>
		<p>If  any  provision  of  this  Agreement  is  determined  to  be  void  or  unenforceable  (“the offending provision”), the other terms and provisions of this agreement will remain in full force and effect as if this Agreement had been executed without the offending provision appearing herein.
			This Agreement  contains all the terms and conditions  of the agreement  entered into by INTELLI PROPERTY MANAGEMENT  and  the  Tenant/Surety  and  they  acknowledge  and  agree  that  any representations,  warranties, undertakings or promises whatsoever which may have been made  by  them  other  than  those  contained  herein  will  not  be  binding  or  enforceable against either party, and the terms of this agreement cannot be varied otherwise than in writing and signed by both parties.
			INTELLI PROPERTY MANAGEMENT will not be regarded as having waived, or be precluded from exercising, any right under or arising from this Agreement by reason of the INTELLI PROPERTY MANAGEMENT having at any time granted any extension  of time for, or having  shown  any indulgence  to the Tenant/Surety  with reference  to any  payment  or  performance  hereunder,  or  having  failed  to enforce,  or delayed in the enforcement of, any right of action against the Tenant/Surety.
			If you provide the details of a friend that referred you, the nominated friend stands a chance to win a prize. You may only nominate one friend per lease agreement.  If we 
			are, or a third party supplier is, unable to contact a winner within 30 days or if the winner is unable  to collect  the prize within  60 days, the winner  will forfeit the prize and INTELLI PROPERTY MANAGEMENT (PTY) LTD reserves  the right to re-draw  a new winner  under  the same  conditions.  The terms and conditions that apply to all competitions can be located on the INTELLI PROPERTY MANAGEMENT Tenants. 
			WEBSITE: www.intellipropertymanagement.com
			EMAIL: info@intellipropertymanagement.com
		</p>
	</ol>
</li>
</ol>





