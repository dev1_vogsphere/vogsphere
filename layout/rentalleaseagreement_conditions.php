<style>
ol.nested-counter-list {
  counter-reset: item;
}

ol.nested-counter-list li {
  display: block;
}

ol.nested-counter-list li::before {
  content: counters(item, ".") ". ";
  counter-increment: item;
  font-weight: bold;
}

ol.nested-counter-list ol {
  counter-reset: item;
}
</style>  
<style>
.heading{font-weight: bold;}
</style>
<br/>	  
<ol class="nested-counter-list" >
  <li><b>DEFINITIONS</b>
		<p>In this agreement</p>		
		<ol>
			<li>Annexures means these Terms and Conditions, the RULES and the Debit Order Authority.</li>
			<li>Apartment means the apartment described in the Lease Schedule, which Apartment is situated in the Apartment Block.</li>
			<li>Apartment Block means the apartment building described in the Lease Schedule</li>
			<li>Deposit Amount means the amount stated in the Lease Schedule.</li>
			<li>Lease means the Schedule, these Terms and Conditions and all Annexures to this Agreement.</li>
			<li>Lease End Date means the date stated the Lease Schedule.</li>
			<li>Lease Start Date means the date specified in the Lease Schedule.</li>
			<li>Lease Schedule means the document attached to this agreement marked as the Application and Lease Schedule</li>
			<li>Room refers to the bedroom described in the Lease Schedule.</li>
			<li>Tenant refers a male/female person whose name and details appear in the Lease Schedule.</li>			
		</ol>
  </li>
  <li><b>RECITAL</b> 
      <ol>
		<li>The Tenant is either a student or a working person</li> 
		<li>INTELLI PROPERTY MANAGEMENT is the owner AND/OR agent of the Apartment/Room/House.</li>
		<li>The tenant submitted an application to hire the Apartment/Room/House from INTELLI PROPERTY MANAGEMENT which application has been approved by INTELLI PROPERTY MANAGEMENT and is incorporated into this Agreement as the Lease Schedule.</li>	  
	  </ol>
  </li>
  <h3>NOW THEREFORE IT IS HEREBY AGREED AS FOLLOWS:</h3>
  <li>LETTING AND HIRING, DURATION AND RENTAL
	  <p>INTELLI PROPERTY MANAGEMENT hereby lets, and the Tenant hereby hires, the Apartment/Room/House for the period stipulated in the Lease Schedule.</p>	
  </li>
  <li><b>PAYMENT</b>
	<ol>
		<li>The monthly rental payable by the Tenant to INTELLI PROPERTY MANAGEMENT will be the amount
         stipulated in the Lease Schedule.
		</li>
		<li>The rental will be payable in advance, on the first day of each and every month without any  deduction  for  any  reason  whatsoever,  directly  into  INTELLI PROPERTY MANAGEMENT’s  bank  account  or  it’s elected  manager  and  administrator  using  the payment  method  selected  in the Lease Schedule.
		</li>
		<li>Should the Tenant fail to pay the rental on the first day of the month he/she will be liable for interest on the rental amount at a rate equal to 5% (Five Percent) above the prime interest rate per annum charged by FNB BANK LIMITED from time to time, calculated from the date when the rental was due until the rental is paid.
		</li>
		<li>The Tenant  acknowledges  that INTELLI PROPERTY MANAGEMENT has subscribed  to a collection  system  and that he/she  may  be required  to sign  a debit  order  authority  consenting  to INTELLI PROPERTY MANAGEMENT  debiting his/her  bank account  on the first of every month for the rental. If INTELLI PROPERTY MANAGEMENT is not able to debit the Tenant’s bank account on the first day of the month the Tenant consents to INTELLI PROPERTY MANAGEMENT submitting a debit order for processing on any day thereafter.
		</li>
		<li>Should the Tenant make any cash payments of rent, the Tenant shall be obliged to pay R100 charged by INTELLI PROPERTY MANAGEMENT. The Tenant is advised to use a DEBIT ORDER or an ELECTRONIC FUNDS TRANSFER to minimise the costs of bank charges.
		</li>
	</ol>
  </li>
  <li><b>DEPOSIT</b>
	 <ol>
		<li>The Tenant deposit option and he/she received confirmation  from INTELLI PROPERTY MANAGEMENT that his/her Application  for Tenant Accommodation  had been approved.
		</li>
		<li>INTELLI PROPERTY MANAGEMENT will hold the deposit as a security deposit until after the Tenant has vacated the Apartment/Room/House and the Tenant  has discharged  all of his/her obligations  to INTELLI PROPERTY MANAGEMENT in terms of this agreement.
		</li>
		<li>The deposit will be refunded to the Tenant within 25 business days from the Lease End Date provided that the Tenant has discharged all of his/her obligations to INTELLI PROPERTY MANAGEMENT.
	    </li>
		<li>INTELLI PROPERTY MANAGEMENT will be entitled to apply the whole or a portion of the deposit towards payment of the rent, key replacements, remote control replacements, repairs or any other of the Tenant’s liabilities in terms of this Agreement.
		</li>
		<li>If INTELLI PROPERTY MANAGEMENT  applies  any portion  of the deposit  at any time during the course  of the lease agreement,  except  for the month in which the lease agreement  comes  to an end, the Tenant will immediately reinstate the deposit to its original amount.
		</li>
		<li>The Tenant will not be entitled to set off any rent or other amount payable to PAT-
		NINI PROPERTIES against the deposit.
		</li>
	 </ol>
  </li>
  <li><b>TERMINATION</b>
	<ol>
		<li>The Application for Tenant Accommodation Form and all representations and promises contained   therein forms part of this Agreement. 	The Tenant warrants   that the information given by him/her in the application is true and correct. If INTELLI PROPERTY MANAGEMENT at any stage discovers that the information is false, it will be entitled to terminate this Agreement by giving a Tenant 10 day’s written notice.
		</li>
		<li>The  Tenant  agrees  that  should  he/she  at  any  stage  during  the  duration  of  this Agreement  terminate  his/her  studies  and not be enrolled  and registered  as a full time tenant  at  any  Institution  of  Higher  Learning  for  a  period  in  excess  of  30  days,  this Agreement  will be cancelled  with immediate  effect  and the Tenant  will be obliged  to vacate the Apartment/Room/House  at the last day of the relevant month.
		</li>
		<li>The  Tenant  agrees  that  should  he/she  at  any  stage  during  the  duration  of  this Agreement  wish to terminate  this Agreement,  the Tenant is required to give INTELLI PROPERTY MANAGEMENT 60 days notification in writing of said termination, FAILURE TO DO SO SHALL RESULT IN DEPOSIT BEING FORFEITED.
		</li>
	</ol>
  </li>
  <li><b>USE</b>
	<p>
		The Apartment/Room/House will be used for residential purposes only and may not be used for any other purpose whatsoever and nobody except for the Tenant will occupy the Apartment/Room/House on a permanent basis.
	</p>
  </li>
  <li><b>CESSION</b>
	<p>The Tenant will not cede or assign this Agreement or any portion thereof to any other person, nor will the Tenant sub-let the Apartment/Room/House or any portion thereof.</p>
  </li>

