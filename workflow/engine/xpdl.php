<?php
 /**
 * class.xpdl.php
 * ideas : https://github.com/nshong/processmaker/blob/master/workflow/engine/classes/class.xpdl.php
 *		   https://hotexamples.com/examples/-/Xpdl/-/php-xpdl-class-examples.html	
 *   function xmdlProcess ( $sProUid = '')
 
 * web interface sample
 * https://support.salarium.com/portal/kb/articles/how-can-i-create-approval-workflows
 **/ 
// -- Declarations
$rootpath = $_SERVER['DOCUMENT_ROOT'];
$basepath = str_replace('workflow/engine/xpdl.php','database.php',$_SERVER['PHP_SELF']);
$basepath = $rootpath.$basepath;
require $basepath;

class Xpdl //\\ extends processes
{
 
 public static $xpdl_filename 	   = '';
 public static $workflow_containter  =  '';
 
 // -- Activities
 public static $activitySets =  array();
 
 // -- Activity
 public static $activity 	   = ''; // name, id
 
 // -- Performers
 public static $performers   = array();
 
 // -- Performer
 public static $performer    = '';//

// -- Transitions
public static $Transitions = array();

// -- Transition
public static $Transition = '';

 // -- Read the xpdl file.
 public static $xmlFile = 'C:/xampp/htdocs/ecashmeup/workflow/engine/eCashMeUp_WEngine.xpdl';//'http://localhost/ecashmeup/workflow/engine/eCashMeUp_WEngine.xpdl';

 /*$xml = simplexml_load_file($xmlString);
//echo $xml;//->bbb->cccc->dddd['Id'];
//echo $xml->bbb->cccc->eeee['name'];
print_r($xml);
$count = 0;
	foreach($xml as $message)
	{
		print_r($message);
		$count =  $count + 1;
	}
	echo $count;
	
*/


/* or...........
foreach ($xml->bbb->cccc as $element) {
  foreach($element as $key => $val) {
   echo "{$key}: {$val}";
  }
}*/

 // -- Display
 // -- table 
 // -- Workflow Name
 
 
 /* Activity name, Performers name(s), Next Activity.
 
 /**
  * Get the process Data from a filename .XPDL
  * @param  array $pmFilename
  * @return void
  */
  function getProcessDataXpdl ( $pmFilename  )
  {
    if (! file_exists($pmFilename) )
      throw ( new Exception ( 'Unable to read uploaded file, please check permissions. '));
    if (! filesize($pmFilename) >= 9 )
      throw ( new Exception ( 'Uploaded file is corrupted, please check the file before continue. '));
    clearstatcache();
    $idProcess        = 0;
    $nameProcess      = '';
    $sw               = 0;
    $file             = new DOMDocument();
    @$file->load($pmFilename, LIBXML_DTDLOAD);
    $root             = $file->documentElement;
    $node             = $root->firstChild;
    $numberTasks      = 0;
    $numberLanes      = 0;
    $endArray         = array();
    $posEnd           = 0;
    $startArray       = array();
    $posStart         = 0;
    $isEnd            = 0;
    $isStart          = 0;
    $posRoutes        = 0;
    $sequencial       = 0;
    $posT             = 0;
    $output           = '';
    $arrayRoutes      = array();
    $arrayLanes       = array();
    $routeTransitions = array ();
    $start            = 0;
    $end              = 0;
    $arrayScheduler   = array();
    $arrayMessages    = array();
	
	
	// -- oData -- Arrays with data from XPDL
	$oData = new oData();
	$oData->inputs            = array();
    $oData->outputs           = array();
    $oData->dynaforms         = array();
    $oData->steps             = array();
    $oData->taskusers         = array();
    //$oData->groupwfs          = $this->getGroupwfRows( $oData->taskusers );
    $oData->steptriggers      = array();
    $oData->dbconnections     = array();
    $oData->reportTables      = array();
    $oData->reportTablesVars  = array();
    $oData->stepSupervisor    = array();
    $oData->objectPermissions = array();
    $oData->subProcess        = array();

    $numberCount              = 0;
    $oData->lanes = array();
    $numberLanes = 0;
	
	$oData->caseTracker       = array();
    $oData->caseTrackerObject = array();
    $oData->stage             = array();
    $oData->fieldCondition    = array();
	$oData->event             = array();
    $oData->triggers            = array();
    $oData->caseScheduler     = array();
    $oData->dynaformFiles     = array();
	
    while ($node) {
      if ($node->nodeType == XML_ELEMENT_NODE) {
        $name    = $node->nodeName;
        $content = $node->firstChild;
        if($content != array()){
          $output  = $content->nodeValue;
        }
        if(strpos($name,'Pools')!== false){
          while ($content) {
            if ($content->nodeType == XML_ELEMENT_NODE) {
              $nameChild    = $content->nodeName;
              $contentChild = $content->firstChild;
              if(strpos($nameChild,'Pool')!== false){
                $namePool       = $content->getAttribute('Name');
                $idPool         = str_replace('-','',$content->getAttribute('Process'));
                $process['ID']  = $idPool;
                $process['NAME']= $namePool;
                $process['DESCRIPTION'] = 'aaa';
				$oData->process = $this->createProcess($process);
              }
            }
            $content = $content->nextSibling;
          }
        }
        if(strpos($name,'Artifacts')!== false){
          while ($content) {
            if ($content->nodeType == XML_ELEMENT_NODE) {
              $nameChild    = $content->nodeName;
              $contentChild = $content->firstChild;
              $idActivity   = '';
              if(strpos($nameChild,'Artifact')!== false){
                $artifactType = $content->getAttribute('ArtifactType');
                if($artifactType == 'Annotation'){
                  $textAnnotation      = $content->getAttribute('TextAnnotation');
                  $idAnnotation        = str_replace('-','',$content->getAttribute('Id'));
                  $idActivity          = str_replace('-','',$content->getAttribute('ActivitySetId'));
                  $lanes               = $this->findCoordinates($contentChild);
                  $lanes['TEXT']       = $textAnnotation;
                  $lanes['ID_PROCESS'] = $idPool;
                  $lanes['ID_LANE']    = $idAnnotation;
                  if($idActivity != ''){
                    $lanes['ID_PROCESS'] = $idActivity;
                  }
                  $arrayLanes[$numberLanes] = $this->createLanes($lanes);
                  $numberLanes              = $numberLanes +1;
                }
              }
            }
            $content = $content->nextSibling;
          }
        }
        if(strpos($name,'WorkflowProcesses')!== false){
          while ($content) {
            if ($content->nodeType == XML_ELEMENT_NODE) {
              $nameChild    = $content->nodeName;
              $contentChild = $content->firstChild;
              if(strpos($nameChild,'WorkflowProcess')!== false){
                $nameWorkflow = $content->getAttribute('Name');
                $idProcess    = $content->getAttribute('Id');
                $idProcess    = trim(str_replace('-','',$idProcess));
                $subProcesses = array();
                if($nameWorkflow == $namePool and $idProcess == $idPool){
                  $idWorkflow = $idProcess;
                  while ($contentChild) {
                    if ($contentChild->nodeType == XML_ELEMENT_NODE){
                      $nameNode    = $contentChild->nodeName;
                      $contentNode = $contentChild->firstChild;
                      if(strpos($nameNode,'ActivitySets')!== false){
                        $activitySet 		= $this->createSubProcesses($contentNode,$arrayLanes);
                        $subProcesses 		= $activitySet['SUB_PROCESS'];
                        $arraySubProcesses  = $activitySet['SUBPROCESSES'];
                      }
                      if(strpos($nameNode,'Activities')!== false){
                        $activities     = $this->createActivities($contentNode,$idProcess,$subProcesses);
                        $oData->tasks   = $activities['TASKS'];
                        $startArray     = $activities['START'];
                        $endArray       = $activities['END'];
                        $arrayRoutes    = $activities['ROUTES'];
                        $arrayScheduler = $activities['SCHEDULER'];
                        $arrayMessages  = $activities['MESSAGES'];
                      }
					  
					 if(strpos($nameNode,'DataFields')!== false)
					 {
                        $datafields     	= $this->createDataFields($contentNode,$idProcess,$subProcesses);
                        $oData->datafields  = $datafields['DataFields'];
					   /* 
						$oData->tasks   = $activities['TASKS'];
                        $startArray     = $activities['START'];
                        $endArray       = $activities['END'];
                        $arrayRoutes    = $activities['ROUTES'];
                        $arrayScheduler = $activities['SCHEDULER'];
                        $arrayMessages  = $activities['MESSAGES'];
						*/
						//print_r($datafields);
                      }
					  
                      if(strpos($nameNode,'Transitions')!== false){
                        $transitions      = $this->createTransitions($contentNode,$oData->tasks,$arrayRoutes,$endArray,$startArray,$idProcess,$arrayScheduler,$arrayMessages);
                        $oData->routes    = $transitions['ROUTES'];
                        $oData->tasks     = $transitions['TASKS'];
                        $routeTransitions = $transitions['TRANSITIONS'];
                        $numberRoutes     = $transitions['NUMBER'];
                        $taskHidden       = $transitions['TASKHIDDEN'];
                        $arrayScheduler   = $transitions['SCHEDULER'];
                        $arrayMessages    = $transitions['MESSAGES'];
                      }
                    }
                    $contentChild = $contentChild->nextSibling;
                  }
                }
              }
            }
            $content = $content->nextSibling;
          }
        }
      }
      $node = $node->nextSibling;
	  if(isset($name))
	  {/*echo $name.'<br/>'; special echo skeleton*/}
    }

    foreach($arrayLanes as $key => $value) {
      if($value['PRO_UID'] == $idProcess){
        $oData->lanes[$numberLanes] = $value;
        $numberLanes = $numberLanes + 1;
      }
    }
	$numberSubProcess         = 0;
    $arraySubProcess          = $subProcesses;
    $numberSubProcess         = isset($arraySubProcesses) && is_array($arraySubProcesses) ? sizeof($arraySubProcesses) : 0;
    $numberCount              = 0;

     foreach($subProcesses as $key => $value) {
      foreach($oData->tasks as $keyTask => $valueTask) {
        if($value['ID_PROCESS'] == $valueTask['TAS_UID']){
          $fields['TASK_PARENT']   = $valueTask['TAS_UID'];
          $idSubProcess            = $valueTask['TAS_UID'];
          $findTask = 0;
          $newTasks = $this->getTaskRows($idSubProcess);
          foreach ($newTasks as $keyLane => $val ) {
            if($val['TAS_START']==='TRUE' and $findTask == 0){
              $findTask = 1;
              $value['TASK_START']=$val['TAS_UID'];
            }
          }
          $fields['PROCESS_PARENT']= $idProcess;
          $fields['TAS_UID']= $value['TASK_START'];
          $oData->subProcess[$numberSubProcess]= $this->createSubProcess($fields,$arraySubProcess);
          $numberSubProcess                    = $numberSubProcess + 1;
        }
      }
    }
    //$oData->event             = $this->createEventMessages($arrayMessages,$idProcess);
    //$oData->caseScheduler     = $this->createScheduler($arrayScheduler,$idProcess);
    $numberTransitions=sizeof($routeTransitions);
    if($numberTransitions > 0){
      $routesArray   = $this->createGateways($routeTransitions,$endArray,$oData->routes,$numberRoutes,$idProcess,$taskHidden);
      $oData->routes = $routesArray;
    }
    //print_r($oData);die;
    //print_r($arrayMessages);die;
    return $oData;
  }
  /**
  * This function creates DataFields according the content of the node
  * @param  object $contentNode
  * @param  string $idProcess
  * @param  string $DataFieldSet
  * @return array $result
  */
  function createDataFields($contentNode,$idProcess,$DataFieldSet)
  {
	$result 	= array();
	$startArray = array();
	
	$posStart = 0;
	while ($contentNode)
	{
		if ($contentNode->nodeType == XML_ELEMENT_NODE)
		{
			$nameDataField    = $contentNode->nodeName;
			$contentDataField = $contentNode->firstChild;
			if(strpos($nameDataField,'DataField')!== false)
			{
				$idActivity = $contentNode->getAttribute('Id');
				$idActivity = trim(str_replace('-','',$idActivity));
				$name       = htmlentities($contentNode->getAttribute('Name'));
				$result     = $this->createDataField($contentDataField);				
				$result['DATA_ID']   = $idActivity;								
				$result['DATA_NAME'] = $name;
				$startArray[$posStart] = $result;
                $posStart= $posStart+1;
			}
		}
		      $contentNode = $contentNode->nextSibling;
	}
	$result['DataFields'] = $startArray;
	return $result;
  }
  /**
  * This function creates activities according the content of the node
  * @param  object $contentNode
  * @param  string $idProcess
  * @param  string $ActivitySet
  * @return array $result
  */
  function createActivities($contentNode,$idProcess,$activitySet)
  {
    $result         = array();
    $numberTasks    = 0;
    $posRoutes      = 0;
    $posEnd         = 0;
    $posStart       = 0;
    $arrayRoutes    = array();
    $endArray       = array();
    $endArray       = array();
    $startArray     = array();
    $arrayScheduler = array();
    $scheduler      = array();
    $message        = array();
    $arrayMessages  = array();
    while ($contentNode){
      if ($contentNode->nodeType == XML_ELEMENT_NODE)
	  {
        $nameActivity    = $contentNode->nodeName;
        $contentActivity = $contentNode->firstChild;
        if(strpos($nameActivity,'Activity')!== false)
		{
          $idActivity          = $contentNode->getAttribute('Id');
          $idActivity          = trim(str_replace('-','',$idActivity));
          $name                = htmlentities($contentNode->getAttribute('Name'));
          $result              = $this->createActivity($contentActivity);
          $result['ID_PROCESS']= $idProcess;
          $result['ID_TASK']   = $idActivity;
          $result['TAS_TITLE'] = $name;
          $result['TASK_TYPE'] = 'NORMAL';
		  
          foreach ($activitySet as $key => $value) 
		  {
            if($value['ID_PROCESS'] == $idActivity)
			{
              $result['TASK_TYPE']= 'SUBPROCESS';
            }
          }
          if($result['EVENT'] =='' and $result['ROUTE'] =='')
		  {
            $arrayTasks[$numberTasks]= $this->createTask($result);
            $numberTasks             = $numberTasks +1;
          }
          else
		  {
            if($result['EVENT'] !='' and $result['TRIGGER']== 'None')
			{
              if($result['EVENT'] =='END')
			  {
                $endArray[$posEnd] = $result;
                $posEnd = $posEnd+1;
              }
              if($result['EVENT'] =='START')
			  {
                $startArray[$posStart] = $result;
                $posStart= $posStart+1;
              }
            }
            else{
              if($result['TRIGGER']== 'Timer'){
                $scheduler['ID']       = $idActivity;
                $scheduler['NAME']     = $name;
                if($result['TIME'] == '' or $result['TIME'] == Null){
                  $scheduler['TIME']   = date("D M j G:i:s T Y");
                }
                else{
                  $scheduler['TIME']   = $result['TIME'];
                }
                $scheduler['TYPE_TIME']= $result['TYPE_TIME'];
                $arrayScheduler[]      = $scheduler;
              }
               if($result['TRIGGER']== 'Message'){
                $message['ID']           = $idActivity;
                $message['NAME']         = $name;
                $message['TYPE_MESSAGE'] = $result['MESSAGE'];
                $arrayMessages[]         = $message;
              }
            }
            if($result['ROUTE'] !=''){
              $position                                 = $this->findCoordinates($contentActivity);
              $arrayRoutes[$posRoutes]['ID']            = $idActivity;
              $arrayRoutes[$posRoutes]['ROUTE']         = $result['ROUTE'];
			  if(isset($result['TYPE_DISCRIMINATOR']))
			  {
              $arrayRoutes[$posRoutes]['TYPE_DIS']      = $result['TYPE_DISCRIMINATOR'];
			}
			else
			{
				$arrayRoutes[$posRoutes]['TYPE_DIS'] = '';
			}
			
			if(isset($result['CONDITION_DISCRIMINATOR']))
			{
              $arrayRoutes[$posRoutes]['CONDITION_DIS'] = $result['CONDITION_DISCRIMINATOR'];
			}
			else
			{
				$arrayRoutes[$posRoutes]['CONDITION_DIS'] = '';
			}
              $arrayRoutes[$posRoutes]['X']             = $position['X'];
              $arrayRoutes[$posRoutes]['Y']             = $position['Y'];
              $posRoutes    = $posRoutes + 1;
            }
          }
        }
      }
      $contentNode = $contentNode->nextSibling;
    }
    $result['TASKS']    = $arrayTasks;
    $result['END']      = $endArray;
    $result['START']    = $startArray;
    $result['ROUTES']   = $arrayRoutes;
    $result['SCHEDULER']= $arrayScheduler;
    $result['MESSAGES'] = $arrayMessages;
    return $result;
  }
  /**
  * This function creates an array for the task that will be created according to the data given in an array
  * @param  array $fields
  * @return array $task
  */
   function createTask($fields)
  {
    $task = array();
    $task['PRO_UID']                  = $fields['ID_PROCESS'];
    $task['TAS_UID']                  = $fields['ID_TASK'];
    $task['TAS_TYPE']                 = $fields['TASK_TYPE'];
    /*$task['TAS_DURATION']             = 1;
    $task['TAS_DELAY_TYPE']           ='';
    $task['TAS_TEMPORIZER']           = 0;
    $task['TAS_TYPE_DAY']             ='' ;
    $task['TAS_TIMEUNIT']             = 'DAYS';
    $task['TAS_ALERT']                = 'FALSE';
    $task['TAS_PRIORITY_VARIABLE']    = '@@SYS_CASE_PRIORITY';
    $task['TAS_ASSIGN_TYPE']          = 'BALANCED';
    $task['TAS_ASSIGN_VARIABLE']      = '@@SYS_NEXT_USER_TO_BE_ASSIGNED';
    $task['TAS_ASSIGN_LOCATION']      = 'FALSE';
    $task['TAS_ASSIGN_LOCATION_ADHOC']= 'FALSE';
    $task['TAS_TRANSFER_FLY']         = 'FALSE';
    $task['TAS_LAST_ASSIGNED']        = '00000000000000000000000000000001';
    $task['TAS_USER']                 = '0';
    $task['TAS_CAN_UPLOAD']           = 'FALSE';
    $task['TAS_VIEW_UPLOAD']          = 'FALSE';
    $task['TAS_VIEW_ADDITIONAL_DOCUMENTATION']= 'FALSE';
    $task['TAS_CAN_CANCEL']           = 'FALSE';
    $task['TAS_OWNER_APP']            = 'FALSE';
    $task['STG_UID']                  = '';
    $task['TAS_CAN_PAUSE']            = 'FALSE';
    $task['TAS_CAN_SEND_MESSAGE']     = 'TRUE';
    $task['TAS_CAN_DELETE_DOCS']      = 'FALSE';
    $task['TAS_SELF_SERVICE']         = 'FALSE';
    $task['TAS_START']                = 'FALSE';
    $task['TAS_TO_LAST_USER']         = 'FALSE';
    $task['TAS_SEND_LAST_EMAIL']      = 'FALSE';
    $task['TAS_DERIVATION']           = 'NORMAL';
    $task['TAS_POSX']                 = $fields['X'];
    $task['TAS_POSY']                 = $fields['Y'];
    $task['TAS_COLOR']                = '';
    $task['TAS_DEF_MESSAGE']          =  '';
    $task['TAS_DEF_PROC_CODE']        = '';
    $task['TAS_DEF_DESCRIPTION']      = '';*/
    $task['TAS_TITLE']                = $fields['TAS_TITLE'];
    $task['TAS_DESCRIPTION']          = $fields['DESCRIPTION'];
    $task['TAS_DEF_TITLE']            = '';
	$task['TAS_PERFORMER']            = $fields['PERFORMER'];
	$task['TAS_SCRIPT']               = $fields['script'];
	if(!empty($fields['Priority']))
	{
		$task['TAS_PRIORITY']             = $fields['Priority'];
	}
	else
	{
		oData::$task_priority  = oData::$task_priority  + 1;
		$task['TAS_PRIORITY']             = oData::$task_priority;
	}
	if(isset($fields['CONDITION_DISCRIMINATOR']))
	{$task['TAS_FIELDVALUE_DISCRIMINATOR'] = $fields['CONDITION_DISCRIMINATOR'];}
	else
	{
		$task['TAS_FIELDVALUE_DISCRIMINATOR']  = '';
	}

	if(!empty($task['TAS_FIELDVALUE_DISCRIMINATOR']))
	{
			$task['TAS_FIELDNAME_DISCRIMINATOR'] = 'condition';
	}
	else
	{
		$task['TAS_FIELDNAME_DISCRIMINATOR'] = '';
	}
	//print_r($fields);
    return $task;
  }
  
  /**
  * this function creates a array according to the data given in a node
  * @param  object $contentNode
  * @return array
  */
  function createActivity($contentNode)
  {
    $coordinates   = array();
    $event         = '';
    $typeRoute     = '';
    $markerVisible = '';
    $typePM        = '';
    $isRoute       = 0;
    $description   = '';
    $isSubProcess  = 0 ;
    $trigger       = '';
    $time          = '';
    $typeTime      = '';
    $isMessage     = 0;
	$isPerformer   = 0;
    $performer     = '';
	$Taskscript    = '';;
	$isTaskscript  = 0;
	$isPriority = 0;
	$Priority = '';
	
    while ($contentNode) {
      if ($contentNode->nodeType == XML_ELEMENT_NODE) {
        $nameChild    = $contentNode->nodeName;
        $contentChild = $contentNode->firstChild;
        if(strpos($nameChild,'Description')!== false){
          $description = $contentNode->nodeValue;
        }
        if(strpos($nameChild,'ExtendedAttributes')!== false){
          $result    = $this->createExtended($contentChild);
        }
        if(strpos($nameChild,'BlockActivity')!== false){
          $isSubProcess = 1;
          $idSubProcess = trim(str_replace('-','',$contentNode->getAttribute('ActivitySetId')));
        }
        if(strpos($nameChild,'Event')!== false)
		{
          while ($contentChild) 
		  {
            if ($contentChild->nodeType == XML_ELEMENT_NODE) 
			{
              $nameInfo    = $contentChild->nodeName;
              $contentInfo = $contentChild->firstChild;
              if(strpos($nameInfo,'StartEvent')!== false){
                $event   = 'START';
                $trigger =  $contentChild->getAttribute('Trigger');
              }
              if(strpos($nameInfo,'EndEvent')!== false){
                $event   = 'END';
                $trigger =  $contentChild->getAttribute('Trigger');
                if($trigger == ''){
                  $trigger = 'None';
                }
              }
              if(strpos($nameInfo,'IntermediateEvent')!== false){
                $event = 'INTERMEDIATE';
                $trigger =  $contentChild->getAttribute('Trigger');
              }
            }
            $contentChild = $contentChild->nextSibling;
          }
          if($trigger != 'None'){
            if($trigger == 'Timer'){
              while ($contentInfo) {
                $nameTrigger    = $contentInfo->nodeName;
                $contentTrigger = $contentInfo->firstChild;
                if(strpos($nameTrigger,'TriggerTimer')!== false){
                  $time      = $contentInfo->getAttribute('TimeCycle');
                  $typeTime  = 'TimeCycle';
                  if($time == ''){
                    $time      = $contentInfo->getAttribute('TimeDate');
                    $typeTime  = 'TimeDate';
                  }
                }
                $contentInfo = $contentInfo->nextSibling;
              }
            }
            if($trigger == 'Message'){
              $typeMessage = '';
              $isMessage   = 1;
              while ($contentInfo) {
                $nameTrigger    = $contentInfo->nodeName;
                $contentTrigger = $contentInfo->firstChild;
                if(strpos($nameTrigger,'TriggerResultMessage')!== false){
                  $typeMessage  = $contentInfo->getAttribute('CatchThrow');
                }
                $contentInfo = $contentInfo->nextSibling;
              }
            }
          }
        }

        if(strpos($nameChild,'Route')!== false){
          $typePM        = '';
          $typeRoute     = $contentNode->getAttribute('GatewayType');
          $markerVisible = $contentNode->getAttribute('MarkerVisible');
          if($markerVisible !=''){
            $coordinates['ROUTE'] = $markerVisible;
          }
          if($typeRoute !=''){
            $coordinates['ROUTE'] = $typeRoute;
          }
          if($typeRoute =='' and $markerVisible ==''){
            $coordinates['ROUTE'] = 'EVALUATE';
          }
          switch($coordinates['ROUTE'])
		  {
                  case 'AND':
                    $typePM =  'PARALLEL';
                    break;
                  case 'true':
                    $typePM =  'EVALUATE';
                    break;
                  case 'EVALUATE':
                    $typePM = 'EVALUATE';
                  break;
                  case 'OR':
                    $typePM = 'PARALLEL-BY-EVALUATION';
                  break;
                  case 'Complex':
                    $typePM = 'DISCRIMINATOR';
					break;
				  case 'Exclusive':
				        $typePM = 'DISCRIMINATOR';
                  break;
          }
          $isRoute = 1;
        }
        if(strpos($nameChild,'NodeGraphicsInfos')!== false)
		{
          $coordinates                            = $this->findCoordinates($contentNode);
          $coordinates['EVENT']                   = $event;
          $coordinates['TRIGGER']                 = $trigger;
          $coordinates['TIME']                    = $time;
          $coordinates['TYPE_TIME']               = $typeTime;
          $coordinates['DESCRIPTION']             = $description;
		  if(isset($result['TYPE']))
          $coordinates['TYPE_DISCRIMINATOR']      = $result['TYPE'];
	  
		  if(isset($result['CONDITION']))
          $coordinates['CONDITION_DISCRIMINATOR'] = $result['CONDITION'];
	  
          if($isRoute == 1)
		  {
            $coordinates['ROUTE']= $typePM;
          }
          else{
            $coordinates['ROUTE']='';
          }
          if($isMessage == 1){
            $coordinates['MESSAGE']= $typeMessage;
          }
          else{
            $coordinates['MESSAGE']='';
          }
		  
		  if($isPerformer == 1){
            $coordinates['PERFORMER']= $performer;
          }
          else{
            $coordinates['PERFORMER']='';
          }
		  
		  if($isTaskscript == 1){
            $coordinates['script']= $Taskscript;
          }
          else{
            $coordinates['script']='';
          }
		  //print_r($coordinates);
		  
		  if($isPriority == 1)
		  {
            $coordinates['Priority']= $Priority;
          }
          else{
            $coordinates['Priority']='';
          }
        }
		
		if(strpos($nameChild,'Performers')!== false)
		{
          $performers = $this->findTaskPerformer($contentNode);
		  $performer   = $performers;
		  $isPerformer = 1;
		}
		
		if(strpos($nameChild,'Implementation')!== false)
		{
          $Taskscripts = $this->findTaskScript($contentNode);
		  if(isset($Taskscripts['script']))
		  {$Taskscript = $Taskscripts['script'];}
		  $isTaskscript = 1;
		}
	
		if(strpos($nameChild,'Priority')!== false)
		{
          $Priority = $contentNode->nodeValue;
		  $isPriority = 1;
        }
      }
      $contentNode = $contentNode->nextSibling;
    }
    return $coordinates;
  }
  
  /**
  * This function find the coordinates of a object
  * @param object $contentNode
  * @return array
  */
  function findCoordinates($contentNode)
  {
    $coordinates = array();
    while ($contentNode) {
      if ($contentNode->nodeType == XML_ELEMENT_NODE) {
        $nameChild    = $contentNode->nodeName;
        $contentChild = $contentNode->firstChild;
        if(strpos($nameChild,'NodeGraphicsInfos')!== false){
          while ($contentChild) {
            if ($contentChild->nodeType == XML_ELEMENT_NODE) {
              $nameInfo    = $contentChild->nodeName;
              $contentInfo = $contentChild->firstChild;
              if(strpos($nameInfo,'NodeGraphicsInfo')!== false){
                $coordinates['Height']      = $contentChild->getAttribute('Height');
                $coordinates['Width']       = $contentChild->getAttribute('Width');
                $coordinates['BorderColor'] = $contentChild->getAttribute('BorderColor');
                $coordinates['FillColor']   = $contentChild->getAttribute('FillColor');
                while ($contentInfo) {
                  $nameCoordinate    = $contentInfo->nodeName;
                  $contentCoordinate = $contentInfo->firstChild;

                  if(strpos($nameCoordinate,'Coordinates')!== false){
                    $coordinates['X'] = $contentInfo->getAttribute('XCoordinate');
                    $coordinates['Y'] = $contentInfo->getAttribute('YCoordinate');
                  }
                  $contentInfo = $contentInfo->nextSibling;
                }

              }
            }
            $contentChild = $contentChild->nextSibling;
          }
        }
      }
      $contentNode = $contentNode->nextSibling;
    }
    return $coordinates;
  }
  
  /**
  * This function find the performer of a object
  * @param object $contentNode
  * @return array
  */
  function findTaskPerformer($contentNode)
  {
    $performer = array();
    while ($contentNode) {
      if ($contentNode->nodeType == XML_ELEMENT_NODE) {
        $nameChild    = $contentNode->nodeName;
        $contentChild = $contentNode->firstChild;
        if(strpos($nameChild,'Performers')!== false){
          while ($contentChild) {
            if ($contentChild->nodeType == XML_ELEMENT_NODE) {
              $nameInfo    = $contentChild->nodeName;
              $contentInfo = $contentChild->firstChild;
              if(strpos($nameInfo,'Performer')!== false){
                while ($contentInfo) {
                  $namePerformer    = $contentInfo->nodeName;
                  $contentPerformer = $contentInfo->firstChild;
				  $performer['PERFORMER'] = $contentNode->nodeValue;	
                  $contentInfo = $contentInfo->nextSibling;
                }
              }
            }
            $contentChild = $contentChild->nextSibling;
          }
        }
      }
      $contentNode = $contentNode->nextSibling;
    }
    return $performer;
  }
  
  /**
  * This function find the script that will execute a Task Functionality
  * @param object $contentNode
  * @return array
    xpdl:Implementation>
	<xpdl:Task>
		<xpdl:TaskScript>
			<xpdl:Script>reviewdebitorder</xpdl:Script>
  */
  function findTaskScript($contentNode)
  {
    $script = array();
    while ($contentNode) 
	{
      if ($contentNode->nodeType == XML_ELEMENT_NODE) 
	  {
        $nameChild    = $contentNode->nodeName;
        $contentChild = $contentNode->firstChild;
       if(strpos($nameChild,'Implementation')!== false)// -- Implementation
		{ 
          while ($contentChild) 
		  {
           if ($contentChild->nodeType == XML_ELEMENT_NODE) 
			{ 
              $nameTask    = $contentChild->nodeName;
              $contentTask = $contentChild->firstChild;
			  //echo $nameTask;
             if(strpos($nameTask,'Task')!== false)// -- Task
               {  while ($contentTask) 
				  {
					if ($contentTask->nodeType == XML_ELEMENT_NODE) 
				   	{ 
                  $nameTaskScript    = $contentTask->nodeName;
                  $contentTaskScript = $contentTask->firstChild;
				  //echo $nameTaskScript;
					   if(strpos($nameTaskScript,'TaskScript')!== false) // -- TaskScript
					   {
						while ($contentTaskScript) 
						{
						 if ($contentTaskScript->nodeType == XML_ELEMENT_NODE) 
						 {
						  $nameScript    = $contentTaskScript->nodeName;
						  $contentScript = $contentTaskScript->firstChild;
						  				  //echo $nameScript;
						   if(strpos($nameScript,'Script')!== false) // -- Script
						   {
							   if(isset($contentScript->nodeValue))
							   {$script['script'] = $contentScript->nodeValue;}							  
						   }
						} 
						$contentTaskScript = $contentTaskScript->nextSibling;
					  }	
                    }
				  }	
				 $contentTask = $contentTask->nextSibling;
              }
            }
          }
		   $contentChild = $contentChild->nextSibling;
        }
      }
    }
	$contentNode = $contentNode->nextSibling;
  }
      return $script;
  }
  
  /**
  * This function return the type and the condition of a discriminator
  * @param  object $contentNode
  * @return array
  */
  function createExtended($contentNode)
  {
    $result = array();
    $result['TYPE']= '';
    $result['CONDITION'] = '';
    $contentExtended = $contentNode;
    while ($contentExtended) {
      if ($contentExtended->nodeType == XML_ELEMENT_NODE) {
        $nameChildExtended    = $contentExtended->nodeName;
        $contentChildExtended = $contentExtended->firstChild;
        if(strpos($nameChildExtended,'ExtendedAttribute')!== false){
           $name = $contentExtended->getAttribute('Name');
           if($name == 'option'){
             $result['TYPE']      = $contentExtended->getAttribute('Value');
           }
           if($name == 'condition'){
             $result['CONDITION'] = $contentExtended->getAttribute('Value');
           }
        }
      }
      $contentExtended = $contentExtended->nextSibling;
    }
	//print_r($result);
    return $result;
  }
  
  /**
  * this function creates a array according to the data given in a node
  * @param  object $contentNode
  * @return array
  */
  function createDataField($contentNode)
  { 
		$InitialValue = '';
		$datatype = '';
		$DataField  = array();
		
	   while ($contentNode) 
	   {
		  if ($contentNode->nodeType == XML_ELEMENT_NODE) 
		  {
			$nameChild    = $contentNode->nodeName;
			$contentChild = $contentNode->firstChild;

		 if(strpos($nameChild,'DataType')!== false)
		 {
			while ($contentChild) 
			{
		 	if ($contentChild->nodeType == XML_ELEMENT_NODE) 
			{
			  $nameInfo    = $contentChild->nodeName;
			  $contentInfo = $contentChild->firstChild;
			  if(strpos($nameInfo,'BasicType')!== false)
				{
					$datatype =  $contentChild->getAttribute('Type');
				    $DataField['DATA_TYPE'] = $datatype;
					$DataField['DATA_VALUE'] = '';
				}
			}
			  	         $contentChild = $contentChild->nextSibling;

			}
		 } 			
		if(strpos($nameChild,'InitialValue')!== false)
		{
			  $InitialValue = $contentNode->nodeValue;
			  $DataField['DATA_VALUE'] = $InitialValue;
		}
	   }
	         $contentNode = $contentNode->nextSibling;
	}
  
	/*
	$fields
	$datacontainer = array();
    $datacontainer['PRO_UID']           = $fields['ID'];
	$datacontainer['FIELD_NAME']        = $fields['Name'];
	$datacontainer['FIELD_TYPE']        = $fields['BasicType'];
	$datacontainer['FIELD_VALUE']       = $fields['InitialValue'];*/
	return $DataField;
  }
  
  /**
  * this function creates an array for the process that will be created according to the data given in an array
  * @param  array $fields
  * @return array $process
  */
  function createProcess($fields)
  { $process = array();
    $process['PRO_UID']           = $fields['ID'];
    $process['PRO_PARENT']        = $fields['ID'];
    $process['PRO_TIME']          = 1;
    $process['PRO_TIMEUNIT']      = 'DAYS';
    $process['PRO_STATUS']        = 'ACTIVE';
    $process['PRO_TYPE_DAY']      = '';
    $process['PRO_TYPE']          = 'NORMAL';
    $process['PRO_ASSIGNMENT']    = 'FALSE';
    $process['PRO_SHOW_MAP']      = 0;
    $process['PRO_SHOW_MESSAGE']  = 0;
    $process['PRO_SHOW_DELEGATE'] = 0;
    $process['PRO_SHOW_DYNAFORM'] = 0;
    $process['PRO_CATEGORY']      = '';
    $process['PRO_SUB_CATEGORY']  = '';
    $process['PRO_INDUSTRY']      = 0;
    $process['PRO_UPDATE_DATE']   = date("D M j G:i:s T Y");
    $process['PRO_CREATE_DATE']   = date("D M j G:i:s T Y");
    $process['PRO_CREATE_USER']   = 00000000000000000000000000000001;
    $process['PRO_HEIGHT']        = 5000;
    $process['PRO_WIDTH']         = 10000;
    $process['PRO_TITLE_X']       = 0;
    $process['PRO_TITLE_Y']       = 0;
    $process['PRO_DEBUG']         = 0;
    $process['PRO_TITLE']         = $fields['NAME'];
    $process['PRO_DESCRIPTION']   = $fields['DESCRIPTION'];
    return $process;
  }

/**
  * This function creates transitions according the content of the node
  * @param  object $contentNode
  * @param  array $dataTask
  * @param  array $arrayRoutes
  * @param  array $endArray
  * @param  array $startArray
  * @param  string $idProcess
  * @return array $result
  */

  function createTransitions($contentNode,$dataTasks,$arrayRoutes,$endArray,$startArray,$idProcess,$schedulerArray,$messages)
  {
    $numberRoutes     = 0;
    $posT             = 0;
    $output           = '';
    $routeTransitions = array();
    $taskHidden       = array();
    $countHidden      = 1;
    $dataRoutes       = array();
    $newFrom          = 0;
    $isScheduler      = 0;
    $isMessage        = 0;
    while ($contentNode){
      $isEnd   = 0;
      $isStart = 0;
      $tasks = $dataTasks;
      if ($contentNode->nodeType == XML_ELEMENT_NODE){
        $nameTransition    = $contentNode->nodeName;
        $contentTransition = $contentNode->firstChild;
        if(strpos($nameTransition,'Transition')!== false){
          $idTransition        = $contentNode->getAttribute('Id');
          $idTransition        = trim(str_replace('-','',$idTransition));
          $from                = trim(str_replace('-','',$contentNode->getAttribute('From')));
          $to                  = trim(str_replace('-','',$contentNode->getAttribute('To')));
          $routes['ROU_UID']   = $idTransition;
          $routes['ID_PROCESS']= $idProcess;
          $routes['FROM']      = $from;
          $routes['TO']        = $to;
          $isScheduler         = 0;
          $isMessage           = 0;
          foreach ($startArray as $startBase => $value){
            if($value['ID_TASK']==$from){
              foreach($tasks as $tasksValue=> $taskId){
                if($to==$taskId['TAS_UID']){
                  $taskId['TAS_START']='TRUE';
                  $dataTasks[$tasksValue]['TAS_START']='TRUE';
                  $isStart = 1;
                }
              }
            }
          }
          foreach ($schedulerArray as $startBase => $value){
            if($value['ID']==$from){
              $isScheduler                           = 1;
              $schedulerArray[$startBase]['ID_TASK'] = $to;
              foreach($tasks as $tasksValue=> $taskId){
                if($to==$taskId['TAS_UID']){
                  $taskId['TAS_START']='TRUE';
                  $dataTasks[$tasksValue]['TAS_START']='TRUE';
                  $isStart = 1;
                }
              }
            }
          }
          foreach ($messages as $startBase => $value){
            if($value['ID']==$from){
              $isMessage = 1;
              $messages[$startBase]['ID_TASK'] = $to;
            }
            if($value['ID']==$to){
              $isMessage = 1;
              $messages[$startBase]['ID_TASK'] = $from;
            }
          }
          $sequencial = 0;
          $findRoute  = 0;
          foreach ($arrayRoutes as $routeBase => $value){
            //change for task hidden
            if($value['ID']==$from){
              if($findRoute == 0){
                $typeT      = 'FROM';
                $valueRoute = $value['ID'];
                $typeRoute  = $value['ROUTE'];
                $sequencial = 1;
                $conditionD = $value['CONDITION_DIS'];
                $typeD      = $value['TYPE_DIS'];
                $findRoute  = 1;
              }
              else{
                $contendHidden = $contentTransition;
                $fieldsXY                                 = $this->findCoordinatesTransition($to,$arrayRoutes);
                $hidden['ID_TASK']                        = G::generateUniqueID();
                $hidden['ID_PROCESS']                     = $idProcess;
                $hidden['TAS_TITLE']                      = '';
                $hidden['TASK_TYPE']                      = 'HIDDEN';
                $hidden['DESCRIPTION']                    = $countHidden;
                $hidden['X']                              = $fieldsXY['X'];
                $hidden['Y']                              = $fieldsXY['Y'];
                $taskHidden[]                             = $hidden;
                $dataTasks[]                              = $this->createTask($hidden);
                $countHidden                              = $countHidden + 1;
                $routeTransitions[$posT]['ID']            = G::generateUniqueID();
                $routeTransitions[$posT]['ROUTE']         = $from;
                $routeTransitions[$posT]['FROM']          = $from;
                $routeTransitions[$posT]['TO']            = $hidden['ID_TASK'];
                $routeTransitions[$posT]['TOORFROM']      = 'FROM';
                $routeTransitions[$posT]['ROU_TYPE']      = $value['ROUTE'];
                $routeTransitions[$posT]['CONDITION']     = $output;
                $routeTransitions[$posT]['CONDITION_DIS'] = $conditionD;
                $routeTransitions[$posT]['TYPE_DIS']      = $typeD;
                $output                                   = '';
                $posT                                     = $posT + 1;
                $from                                     = $hidden['ID_TASK'];
              }
            }
            if($value['ID']==$to){
              if($findRoute == 0){
                $typeT      = 'TO';
                $valueRoute = $value['ID'];
                $typeRoute  = $value['ROUTE'];
                $sequencial = 1;
                $conditionD = $value['CONDITION_DIS'];
                $typeD      = $value['TYPE_DIS'];
                $findRoute  = 1;
              }
              else{

                $contendHidden = $contentTransition;
                $fieldsXY                                 = $this->findCoordinatesTransition($to,$arrayRoutes);
                $hidden['ID_TASK']                        = G::generateUniqueID();
                $hidden['ID_PROCESS']                     = $idProcess;
                $hidden['TAS_TITLE']                      = '';
                $hidden['TASK_TYPE']                      = 'HIDDEN';
                $hidden['DESCRIPTION']                    = $countHidden;
                $hidden['X']                              = $fieldsXY['X'];
                $hidden['Y']                              = $fieldsXY['Y'];
                $taskHidden[]                             = $hidden;
                $dataTasks[]                              = $this->createTask($hidden);
                $countHidden                              = $countHidden + 1;
                $routeTransitions[$posT]['ID']            = G::generateUniqueID();
                $routeTransitions[$posT]['ROUTE']         = $from;
                $routeTransitions[$posT]['FROM']          = $from;
                $routeTransitions[$posT]['TO']            = $hidden['ID_TASK'];
                $routeTransitions[$posT]['TOORFROM']      = 'TO';
                $routeTransitions[$posT]['ROU_TYPE']      = $typeRoute;
                $routeTransitions[$posT]['CONDITION']     = $output;
                $routeTransitions[$posT]['CONDITION_DIS'] = $conditionD;
                $routeTransitions[$posT]['TYPE_DIS']      = $typeD;
                $output                                   = '';
                $posT                                     = $posT + 1;
                $from                                     = $hidden['ID_TASK'];
                $conditionD                               = $value['CONDITION_DIS'];
                $typeRoute                                = $value['ROUTE'];
                $typeT                                    = 'TO';
                $valueRoute                               = $value['ID'];
                $sequencial                               = 1;
                $conditionD                               = $value['CONDITION_DIS'];
                $typeD                                    = $value['TYPE_DIS'];
              }
            }
          }
          if($sequencial == 0){
            foreach ($endArray as $endBase => $value){
              if($value['ID_TASK']==$to){
                foreach($tasks as $tasksValue=> $taskId){
                  if($from==$taskId['TAS_UID']){
                    $routes['TO'] = '-1';
                    $dataRoutes[$numberRoutes]= $this->createSEQUENTIALRoute($routes);
                    $numberRoutes = $numberRoutes +1;
                    $isEnd        = 1;
                  }
                }
              }
            }
          }
          if($sequencial == 1){
            while ($contentTransition) {
              $nameCondition    = $contentTransition->nodeName;
              $contentCondition = $contentTransition->firstChild;
              if(strpos($nameCondition,'Condition')!== false){
                while ($contentCondition) {
                  $nameExpression    = $contentCondition->nodeName;
                  $contentCondition1 = '';
                  if(strpos($nameExpression,'Expression')!== false){
                    $contentCondition1 = $contentCondition->firstChild;
                    if($contentCondition1 != array()){
                      $output  =  $contentCondition1->nodeValue;
                    }
                  }
                  $contentCondition = $contentCondition->nextSibling;
                }
              }
              $contentTransition = $contentTransition->nextSibling;
            }
            $routeTransitions[$posT]['ID']            = $idTransition;
            $routeTransitions[$posT]['ROUTE']         = $valueRoute;
            $routeTransitions[$posT]['FROM']          = $from;
            $routeTransitions[$posT]['TO']            = $to;
            $routeTransitions[$posT]['TOORFROM']      = $typeT;
            $routeTransitions[$posT]['ROU_TYPE']      = $typeRoute;
            $routeTransitions[$posT]['CONDITION']     = $output;
            $routeTransitions[$posT]['CONDITION_DIS'] = $conditionD;
            $routeTransitions[$posT]['TYPE_DIS']      = $typeD;
            $output                                   = '';
            $posT                                     = $posT + 1;
            $sequencial                               = 1;
          }
          if($isEnd==0 and $isStart == 0 and $sequencial == 0 and $isScheduler == 0 and $isMessage == 0){
            $dataRoutes[$numberRoutes]= $this->createSEQUENTIALRoute($routes);
            $numberRoutes = $numberRoutes +1;
          }
        }
      }
      $contentNode = $contentNode->nextSibling;
    }
    $routes = $routeTransitions;
    foreach($routeTransitions as $key => $id){
      $findTo   = 0;
      $findFrom = 0;
      foreach ($dataTasks as $keyHidden => $value){
        if($id['FROM']== $value['TAS_UID']){
          $findFrom= 1;
        }
        if($id['TO']== $value['TAS_UID']){
          $findTo = 1;
        }
      }
      if($findTo == 0){
        foreach($routes as $keyR => $idR){
          if($id['TO'] == $idR['ROUTE']){
            $routeTransitions[$key]['ROU_TYPE'] = $idR['ROU_TYPE'];
            $routeTransitions[$key]['ROUTE']    = $id['TO'];
            $routeTransitions[$key]['TOORFROM'] = 'TO';
          }
        }
      }
      if($findFrom == 0){
        foreach($routes as $keyR => $idR){
          if($id['FROM'] == $idR['ROUTE']){
            $routeTransitions[$key]['ROU_TYPE'] = $idR['ROU_TYPE'];
            $routeTransitions[$key]['ROUTE']    = $id['FROM'];
            $routeTransitions[$key]['TOORFROM'] = 'FROM';
          }
        }
      }
    }
    $result['ROUTES']     = $dataRoutes;
    $result['TRANSITIONS']= $routeTransitions;
    $result['TASKS']      = $dataTasks;
    $result['NUMBER']     = $numberRoutes;
    $result['TASKHIDDEN'] = $taskHidden;
    $result['SCHEDULER']  = $schedulerArray;
    $result['MESSAGES']   = $messages;
    return $result;
  }  
  
  /**
  * This function creates an array for the route that will be created according to the data given in an array
  * @param  array $fields
  * @return array $route
  */
  function createSEQUENTIALRoute($fields)
  {
    $route = array();
    $route['ROU_UID']          = $fields['ROU_UID'];
    $route['ROU_PARENT']       =  0;
    $route['PRO_UID']          = $fields['ID_PROCESS'];
    $route['TAS_UID']          = $fields['FROM'];
    $route['ROU_NEXT_TASK']    = $fields['TO'];
    $route['ROU_CASE']         =  1;
    $route['ROU_TYPE']         =  'SEQUENTIAL';
    $route['ROU_CONDITION']    =  '' ;
    $route['ROU_TO_LAST_USER'] =  'FALSE';
    $route['ROU_OPTIONAL']     =  'FALSE';
    $route['ROU_SEND_EMAIL']   =  'TRUE';
    $route['ROU_SOURCEANCHOR'] =  1;
    $route['ROU_TARGETANCHOR'] =  0;
	//print('--------------------------------------------------------fields');
	//print_r($fields);
    return $route;
  }
  
/**
  * This function creates an array for the routes that are not sequential that will be created according to the data given in an array
  * @param  array $routeTransitions
  * @param  array $endArray
  * @param  array $dataRoutes
  * @param  array $numberRoutes
  * @param  string $idProcess
  * @return array $dataRoutes
  */
  function createGateways($routeTransitions,$endArray,$dataRoutes,$numberRoutes,$idProcess,$taskHidden)
  {
    $valueCase                = '';
    $value                    = 1;
    $valueC                   = 1;
    $aux                      = $routeTransitions;
    foreach ($routeTransitions as $key => $row) {
      $aux[$key]  = $row['ROUTE'];
    }
    array_multisort($aux,SORT_ASC,$routeTransitions);
    $routeArray = $this->verifyRoutes($routeTransitions,$endArray,$taskHidden);
    $routeArray = $this->sortArray($routeArray);
    foreach($routeArray as $valRoutes => $value){
      $isEventEnd = 0;
      foreach ($endArray as $endBase => $valueEnd){
        if($valueEnd['ID_TASK']==$value['TO']){
          $isEventEnd = 1;
        }
      }
      $dataRoutes[$numberRoutes]= array();
      $dataRoutes[$numberRoutes]['ROU_UID']    = $value['ID'];
      $dataRoutes[$numberRoutes]['ROU_PARENT'] =  0;
      $dataRoutes[$numberRoutes]['PRO_UID']    =  $idProcess;
      $dataRoutes[$numberRoutes]['TAS_UID']    =  $value['FROM'];

      if($isEventEnd == 0){
        $dataRoutes[$numberRoutes]['ROU_NEXT_TASK'] = $value['TO'];
      }
      else{
        $dataRoutes[$numberRoutes]['ROU_NEXT_TASK'] = '-1';
      }
      if($valueCase == $value['FROM']){
        $valueC = $valueC + 1;
      }
      else{
        $valueC    = 1;
        $valueCase = $value['FROM'];
      }
      if($valueCase == ''){
        $valueC    = 1;
        $valueCase = $value['FROM'];
      }
      $dataRoutes[$numberRoutes]['ROU_CASE']         = $valueC;
      $dataRoutes[$numberRoutes]['ROU_TYPE']         = $value['ROU_TYPE'];
      if($value['ROU_TYPE'] ==='DISCRIMINATOR'){
        $dataRoutes[$numberRoutes]['ROU_CONDITION']    = $value['CONDITION_DIS'];
        $dataRoutes[$numberRoutes]['ROU_OPTIONAL']     = $value['TYPE_DIS'];
      }
      if($value['ROU_TYPE'] !=='DISCRIMINATOR'){
        $dataRoutes[$numberRoutes]['ROU_CONDITION']    = $value['CONDITION'];
        $dataRoutes[$numberRoutes]['ROU_OPTIONAL']     = 'FALSE';
      }
      $dataRoutes[$numberRoutes]['ROU_TO_LAST_USER'] = 'FALSE';
      $dataRoutes[$numberRoutes]['ROU_SEND_EMAIL']   = 'TRUE';
      $dataRoutes[$numberRoutes]['ROU_SOURCEANCHOR'] = 1;
      $dataRoutes[$numberRoutes]['ROU_TARGETANCHOR'] = 0;
      $numberRoutes = $numberRoutes + 1;
    }
    return $dataRoutes;
  }  
  /**
  * This function sort a array
  * @param  array $fields
  * @return array sorted
  */
  function sortArray($fields)
  {
    $aux = $fields ;
    foreach ($fields as $key => $row) {
      $aux[$key]  = $row['FROM'];
    }
    array_multisort($aux,SORT_ASC,$fields);
    return $fields;
  }

  /**
  * This functions verify the routes and removes the routes that are repeated
  * @param  array $routeTransitions
  * @param  array $endArray
  * @return array
  */
  function verifyRoutes ($routeTransitions,$endArray,$taskHidden)
  { $findFirst   = 0;
    $firstRoute  = '';
    $taskTo      = '';
    $taskFrom    = '';
    $routeArrayT = $routeTransitions;
    $findHidden  = 0;
    foreach ($routeTransitions as $valRoute => $value){
      $findHidden = 0;
      if($value['ROUTE'] == $firstRoute){
        if($value['TOORFROM'] == 'TO'){
          $taskFrom = $value['FROM'];
        }
        if($value['TOORFROM'] == 'FROM'){
          $taskTo  = $value['TO'];
        }
        if($taskFrom != ''){
          foreach ($routeArrayT as $valRoutes => $values){
            $isEventEnd = 0;
            foreach ($endArray as $endBase => $valueEnd){
              if($valueEnd==$values['TO']){
                $isEventEnd = 1;
              }
            }
            if($values['ROUTE'] == $value['ROUTE'] and $values['TO'] != $value['ROUTE'] and $isEventEnd == 0 and $findHidden == 0){
              $taskFrom = $values['TO'];
            }
            else{
              if($values['ROUTE'] == $value['ROUTE'] and $values['TO'] == $value['ROUTE'] and $isEventEnd == 0){
                foreach ($taskHidden as $idHidden => $valueHidden){
                  if($valueHidden['ID_TASK'] == $values['TO']){
                    $taskFrom  = $valueHidden['ID_TASK'];
                    $findHidden= 1;
                  }
                }
              }
            }
          }
          $routeTransitions[$valRoute]['TO']=$taskFrom;
          $taskFrom = '';
        }
        if($taskTo != ''){
          foreach ($routeArrayT as $valRoutes => $values){
            if($values['ROUTE'] == $value['ROUTE'] and $values['FROM'] != $value['ROUTE']  and $findHidden == 0 ){
              $taskTo = $values['FROM'];
            }
          }
          $routeTransitions[$valRoute]['FROM']=$taskTo;
          $taskTo = '';
        }
      }
      else{
        $firstRoute = $value['ROUTE'];
        $taskToE = '';
        $taskFromE = '';
        if($value['TOORFROM'] == 'TO'){
          $taskFromE = $value['FROM'];
        }
        if($value['TOORFROM'] == 'FROM'){
          $taskToE  = $value['TO'];
        }
        if($taskFromE != ''){
          $findHidden = 0;
          foreach ($routeArrayT as $valRoutes => $values){
            $isEventEnd = 0;
            foreach ($endArray as $endBase => $valueEnd){
              if($valueEnd==$values['TO']){
                $isEventEnd = 1;
              }
            }
            if($values['ROUTE'] == $value['ROUTE'] and $values['TO'] != $value['ROUTE'] and $isEventEnd == 0 and $findHidden == 0){
              $taskFromE = $values['TO'];
            }
            else{
              if($values['ROUTE'] == $value['ROUTE'] and $values['TO'] == $value['ROUTE'] and $isEventEnd == 0){
                foreach ($taskHidden as $idHidden => $valueHidden){
                  if($valueHidden['ID_TASK'] == $values['TO']){
                    $taskFromE  = $valueHidden['ID_TASK'];
                    $findHidden = 1;
                  }
                }
              }
            }
          }
          $routeTransitions[$valRoute]['TO']=$taskFromE;
          $taskFromE = '';
        }
        if($taskToE != ''){
          foreach ($routeArrayT as $valRoutes => $values){
            if($values['ROUTE'] == $value['ROUTE'] and $values['FROM'] != $value['ROUTE'] and $findHidden == 0){
              $taskToE = $values['FROM'];
            }
          }
          $routeTransitions[$valRoute]['FROM']=$taskToE;
          $taskToE = '';
        }
      }
    }
    $firstRoute  = 0;
    $cont        = 0;
    $routeChange = $routeTransitions;
    foreach ($routeTransitions as $valRoutes => $value){
      $route     = $value['ROUTE'];
      $type      = $value['ROU_TYPE'];
      $countFrom = 0;
      $countTo   = 0;
      foreach ($routeChange as $valRoutes2 => $values){
        if($value['ROUTE'] == $values['ROUTE'] and $values['TOORFROM'] == 'TO'){
          $countTo = $countTo + 1;
        }
        if($value['ROUTE'] == $values['ROUTE'] and $values['TOORFROM'] == 'FROM'){
          $countFrom = $countFrom + 1;
        }
      }
      if($type == 'PARALLEL'){
        if($countTo > $countFrom){
          $routeTransitions[$valRoutes]['ROU_TYPE'] = 'SEC-JOIN';
        }
      }
    }
    $routeArrayT2 = $routeTransitions;
    $routeArrayT1 = $routeTransitions;
    foreach ($routeArrayT1 as $valRoutes => $value){
      if($firstRoute == 0){
        $taskFirst = $value['ROUTE'];
      }
      if($taskFirst == $value['ROUTE']){
        if($firstRoute == 0){
          foreach ($routeArrayT2 as $valRoutes2 => $values){
            if($values['ROUTE'] == $taskFirst and $values['FROM'] == $value['FROM'] and $values['TO'] == $value['TO'] and $values['ID'] != $value['ID']){
              unset($routeArrayT2[$valRoutes2]);
              $firstRoute = 1;
            }
          }
        }
      }
      else{
        $firstRoute = 0;
      }
    }
    return $routeArrayT2;
  }
  
  /**
  * This function creates an array for the lane that will be created according to the data given in an array
  * @param  array $lanes
  * @return array $lane
  */
  function createLanesPM($array,$idProcess)
  {
    $arrayLanes = array();
    $field      = array();
    foreach ($array as $key=> $value){
      $field['ID_LANE']    = $value['0'];
      $field['ID_PROCESS'] = $idProcess;
      $field['X']          = $value['2'];
      $field['Y']          = $value['3'];
      $field['WIDTH']      = $value['4'];
      $field['HEIGHT']     = $value['5'];
      $field['TEXT']       = $value['6'];
      $arrayLanes[]= $this->createLanes($field);
    }
    return $arrayLanes;
  }
  
  /**
  * This function creates an array for the lane that will be created according to the data given in an array
  * @param  array $lanes
  * @return array $lane
  */
  function createLanes($lanes)
  {
    $lane = array();
    $lane['SWI_UID']   = $lanes['ID_LANE'];
    $lane['PRO_UID']   = $lanes['ID_PROCESS'];
    $lane['SWI_TYPE']  = 'TEXT';
    $lane['SWI_X']     = $lanes['X'];
    $lane['SWI_Y']     = $lanes['Y'];
    $lane['SWI_TEXT']  = $lanes['TEXT'];
    if(isset($lanes['WIDTH'])){
	$lane['SWI_WIDTH'] = $lanes['WIDTH'];}
	
	if(isset($lanes['HEIGHT']))
    {$lane['SWI_HEIGHT']= $lanes['HEIGHT'];}

    return $lane;
  }

}

if(!function_exists('sendemailbpm'))
{
	function sendemailbpm($json_datafields,$task,$oData )
	{
	// -- get_process_previous_executed_task
	$previous_task = get_process_previous_executed_task($task['EXEC_ID'],$task['TRIGGER_ID']);
	//print('------ PREVIOUS tasks -----');
	//print_r($previous_task);
	// -- Depending on the condition values.
	$condition 	   = '';
	$emailtemplate = '';
	$emailData = array();
	
	// -- Search for data from PROC_JSON_VARIABLES.	
	$datafields    = extract_json_datafields_to_array($json_datafields);
	$tablename     = $datafields['tablename'];
	$tablekey      = $datafields['tablekey'];
	$tablekeyvalue = $datafields['tablekeyvalue'];
	$condition 	   = $datafields['condition'];
	$emailtemplate = $datafields['emailtemplate'];
	$mailto		   = $datafields['mailto'];
	$usercode	   = $datafields['usercode'];
	$workflow_initiator = $datafields['workflow_initiator'];
	// -- clientid
	$datafields['clientid']   = $datafields['tablekeyvalue'];			
    // -- customerid
	$datafields['customerid'] = $datafields['currentprocessor'];			
	//print_r($datafields);
	// -- Status of previous_task task.ONLY execute if its COMPLETED.
	if(isset($previous_task['TAS_EXC_STATUS']))
	{
	  if($previous_task['TAS_EXC_STATUS'] == 'COMPLETED' )
	  {
		// -- Approved
		if($condition == 'approved')
		{	
			// -- Email the bank/supplier/client e.t.c.
			if(!empty($mailto))
			{
				auto_email_bpm($datafields);
			}
			$worklist_id = get_process_execution_tasks_by_triggerid_task($task['TRIGGER_ID'],$datafields['currenttask']);
			$databasename  	   = $datafields['databasename'];//'ecashpdq_paycollections';
			// -- Ulock table for changes
			db_update_table_lockedforchanges($databasename,$datafields);

			// -- Notify workflow initiator
			$datafields = null;
			$datafields['WORKLIST_ID']   = $worklist_id;
			$datafields['customerid']    = $workflow_initiator;			
			$datafields['emailtemplate'] = 'approved_task_content';
			auto_email_bpm($datafields);
		}
		// -- Rejected 	
		elseif($condition == 'rejected')
		{
			// -- Restore Staged Data back into database.
			$sqlstagestatement = $datafields['sqlstagestatement'];
			$json_to_array 	   = json_decode($datafields['sqlstagedata'],true);
			$databasename  	   = $datafields['databasename'];//'ecashpdq_paycollections';
			//db_auto_restore_staged_data($databasename,$data,$sql)
			db_auto_restore_staged_data($databasename,$json_to_array,$sqlstagestatement);
			// -- Ulock table for changes
			db_update_table_lockedforchanges($databasename,$datafields);
			
			$worklist_id = get_process_execution_tasks_by_triggerid_task($task['TRIGGER_ID'],$datafields['currenttask']);
			// -- Notify workflow initiator
			$datafields = null;
			$datafields['WORKLIST_ID']   = $worklist_id;		
			$datafields['customerid']    = $workflow_initiator;			
			$datafields['emailtemplate'] = 'rejected_task_content';
			auto_email_bpm($datafields);
		}
		else
		{
			$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
			$result['WORKLIST_USER']	 = $task['processor'];
			$result['TAS_EXEC_USER'] 	 = $task['processor'];
			$result['TAS_EXC_STATUS'] 	 = 'COMPLETED';//'IN PROGRESS';//'COMPLETED';//'ERROR';
			$result['TAS_EXEC_DATE']     = date('Y-m-d');
			$result['TAS_EXEC_TIME']     = date('h:m:s');
			$result['TAS_EXEC_MESSAGE']  = 'no email was send';
			$result['TAS_EXEC_CONTENT']  = '';
		}
		// -- Approved/Rejected condition met.
		if(!empty($condition))
		{
			$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
			$result['WORKLIST_USER']	 = $task['processor'];
			$result['TAS_EXEC_USER'] 	 = $task['processor'];
			$result['TAS_EXC_STATUS'] 	 = 'COMPLETED';//'IN PROGRESS';//'COMPLETED';//'ERROR';
			$result['TAS_EXEC_DATE']     = date('Y-m-d');
			$result['TAS_EXEC_TIME']     = date('h:m:s');
			$result['TAS_EXEC_MESSAGE']  = $condition.' email was sent to '.$mailto.'.';
			$result['TAS_EXEC_CONTENT']  = '';
		}
	  }
	  else
	  {
		$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
		$result['WORKLIST_USER']	 = $task['processor'];
		$result['TAS_EXEC_USER'] 	 = $task['processor'];
		$result['TAS_EXC_STATUS'] 	 = 'READY';//'IN PROGRESS';//'COMPLETED';//'ERROR'//'READY';
		$result['TAS_EXEC_DATE']     = date('Y-m-d');
		$result['TAS_EXEC_TIME']     = date('h:m:s');
		$result['TAS_EXEC_MESSAGE']  = '';
		$result['TAS_EXEC_CONTENT']  = '';
	  }
	}
		return $result;
   }
}
// -- Custom taskscript
if(!function_exists('moveprocessedfiles'))
{
function moveprocessedfiles($json_datafields,$task,$oData )
{	
	$path = "workflow/engine/";
	$path = $path.'xpdl.php';
	
	// -- Declarations
	$usercode 	  		= '';
	$role 		  		= $task['TAS_PERFORMER'];
	$triggerid    		= $task['TRIGGER_ID'];
	$worklistuser 		= $task['processor'];
	$colspan 			= 2;
	$result 			= array();
	$result['TAS_ID']   = $task['TAS_ID'];
	$result['EXEC_ID']	= $task['EXEC_ID'];
// -- get_process_previous_executed_task
	//$previous_task = get_process_previous_executed_task($EXEC_ID,$TRIGGER_ID);

	// -- Dynamically generated page content.
	$tablename = '';
	$tablekey = '';
	$tablekeyvalue = '';
	
 	// -- Search for data from PROC_JSON_VARIABLES.	
	$datafields    = extract_json_datafields_to_array($json_datafields);
	$tablename     = $datafields['tablename'];
	$tablekey      = $datafields['tablekey'];
	$tablekeyvalue = $datafields['tablekeyvalue'];
	$condition 	   = $datafields['condition'];
	$emailtemplate = $datafields['emailtemplate'];
	$mailto		   = $datafields['mailto'];
	$usercode	   = $datafields['usercode'];
	$fields	   	   = $datafields['fields'];
	
	$result['tablename'] 	 = $datafields['tablename'];
	$result['tablekeyvalue'] = $datafields['tablekeyvalue'];
	$result['tablekey'] 	 = $datafields['tablekey'];
	$result['oData'] 		 = $oData;
	$result['TAS_NEXT']  	 = $task['TAS_NEXT'];
	$result['PROC_NAME']  	 = $task['PROC_NAME'];
	$result['workflow_initiator'] 	 = $datafields['workflow_initiator'];
	$result['fields'] 	 	= $fields;

 // -- Check if the role exist & users to approve.
	$role = 'paycollection';

 // -- Get list of approving agents.
	$taskusers = get_process_agents_by_role_usercode($usercode,$role);
	//--print_r($taskusers);
  if(!empty($role))
  {
	foreach($taskusers as $row)
	{	
		$insertQuery 	  = null;
		$insertQueryMarks = null;
		$html 			  = '';	
		$worklistuser 				 = $row['CustomerId'];
		// -- GENERATE_TASK_WORKLIST_CONTENT
		$result['TAS_PERFORMER']	 = $task['TAS_PERFORMER'];
		$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
		$result['WORKLIST_USER']	 = $row['CustomerId'];
		$result['TAS_EXEC_USER'] 	 = '';
		$result['TAS_EXC_STATUS'] 	 = 'IN PROGRESS';//'IN PROGRESS';//'COMPLETED';//'ERROR';
		$result['TAS_EXEC_DATE']     = date('Y-m-d');
		$result['TAS_EXEC_TIME']     = date('h:m:s');
		$result['TAS_EXEC_MESSAGE']  = 'Please review the content in task content & approve or reject';
		// -- Call generate_task_worklist_content
		$html = generate_task_worklist_content($result);
		// -- Also Email task content 
		
		$result['TAS_EXEC_CONTENT']  = $html;
	}
  // -- Role without approver - Default to administrator to approve '9901019999999';	
	if(empty($html))
	{
		$insertQuery 	  = null;
		$insertQueryMarks = null;
		$html 			  = '';	
		$worklistuser 				 = '9901019999999';
		// -- GENERATE_TASK_WORKLIST_CONTENT
		$result['TAS_PERFORMER']	 = $task['TAS_PERFORMER'];
		$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
		$result['WORKLIST_USER']	 = $task['processor'];
		$result['TAS_EXEC_USER'] 	 = '9901019999999';
		$result['TAS_EXC_STATUS'] 	 = 'IN PROGRESS';//'IN PROGRESS';//'COMPLETED';//'ERROR';
		$result['TAS_EXEC_DATE']     = date('Y-m-d');
		$result['TAS_EXEC_TIME']     = date('h:m:s');
		$result['TAS_EXEC_MESSAGE']  = 'No users found for '.$role.' .Please review the content in task content & approve or reject';
		// -- Call generate_task_worklist_content
		$html = generate_task_worklist_content($result);
		$result['TAS_EXEC_CONTENT']  = $html;
	}
  }
  // -- no role but have task script.
	else
	{
		$result['TRIGGER_ID']	 	 = $task['TRIGGER_ID'];
		$result['WORKLIST_USER']	 = $task['processor'];
		$result['TAS_EXEC_USER'] 	 = $task['processor'];
		$result['TAS_EXC_STATUS'] 	 = 'COMPLETED';//'IN PROGRESS';//'COMPLETED';//'ERROR';
		$result['TAS_EXEC_DATE']     = date('Y-m-d');
		$result['TAS_EXEC_TIME']     = date('h:m:s');
		$result['TAS_EXEC_MESSAGE']  = '';
		$result['TAS_EXEC_CONTENT']  = '';
    }
	return $result;
}
}
// -- Extract json_datafields into flat array.
if(!function_exists('extract_json_datafields_to_array'))
{
	function extract_json_datafields_to_array($json_datafields)
	{
		$datafields = array();
		$json = json_decode($json_datafields);
		foreach ($json as $item) 
		{
			if ($item->DATA_ID == "tablename") 
			{
				$datafields['tablename'] = $item->DATA_VALUE;
			}
				if ($item->DATA_ID == "tablekey") 
			{
				$datafields['tablekey'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "tablekeyvalue") 
			{
				$datafields['tablekeyvalue'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "condition") 
			{
				$datafields['condition'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "emailtemplate") 
			{
				$datafields['emailtemplate'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "mailto") 
			{
				$datafields['mailto'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "usercode") 
			{
				$datafields['usercode'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "currentprocessor") 
			{
				$datafields['currentprocessor'] = $item->DATA_VALUE;
			}
			if ($item->DATA_ID == "workflow_initiator") 
			{
				$datafields['workflow_initiator'] = $item->DATA_VALUE;
			}
			
			// -- Staged Data
			if ($item->DATA_ID == "sqlstagestatement") 
			{
				$datafields['sqlstagestatement'] = $item->DATA_VALUE;
			}

			if ($item->DATA_ID == "sqlstagedata") 
			{
				$datafields['sqlstagedata'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "currenttask") 
			{
				$datafields['currenttask'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "databasename") 
			{
				$datafields['databasename'] = $item->DATA_VALUE;
			}
			
			if ($item->DATA_ID == "fields") 
			{
				$datafields['fields'] = $item->DATA_VALUE;
			}
		}
		
		// -- Isset Checker
		if(!isset($datafields['tablekey'])){$datafields['tablekey'] = '';}
		if(!isset($datafields['tablekeyvalue'])){$datafields['tablekeyvalue'] = '';}
		if(!isset($datafields['condition'])){$datafields['condition'] = '';}
		if(!isset($datafields['emailtemplate'])){$datafields['emailtemplate'] = '';}
		if(!isset($datafields['mailto'])){$datafields['mailto'] = '';}
		if(!isset($datafields['usercode'])){$datafields['usercode'] = '';}
		if(!isset($datafields['workflow_initiator'])){$datafields['workflow_initiator'] = '';}
		if(!isset($datafields['sqlstagestatement'])){$datafields['sqlstagestatement'] = '';}
		if(!isset($datafields['sqlstagedata'])){$datafields['sqlstagedata'] = '';}
		if(!isset($datafields['currenttask'])){$datafields['currenttask'] = '';}
		if(!isset($datafields['databasename'])){$datafields['databasename'] = '';}
		if(!isset($datafields['fields'])){$datafields['fields'] = '';}

		return $datafields;
	}
}

// Begin -- function scripts
function reviewdebitorder($json_datafields,$task,$oData )
{	
	$results = array();
	$actionsContent = array();
	$tablename = '';
	$tablekey = '';
	$tablekeyvalue = '';
	
	// -- Search for data from PROC_JSON_VARIABLES.	
	$datafields    = extract_json_datafields_to_array($json_datafields);
	$tablename     = $datafields['tablename'];
	$tablekey      = $datafields['tablekey'];;
	$tablekeyvalue = $datafields['tablekeyvalue'];
	$fields 	   =  $datafields['fields'];
	
	$table		 	 = array();
	$table['name']   = $tablename;
	$table['fields'] = $fields;
	
	// -- Determine the performers [Agents] by group & usercode
	// -- Go to database & get the record.
	$results['TYPE'] 			= '';
	$results['PRO_CREATE_USER'] = '';
	$results['PRO_CREATE_DATE'] = '';
	$results['MESSAGE'] 		= '';
	$results['oData'] 		    = $oData;

	// -- Generate a dynamic Page table record with approve & reject button [if task DISCRIMINATOR] 
	$actions  = getactions($results);
	$feedback = getheader($actions,$table);
	for($int=0;$int<count($actions);$int++)
	{
		$actionsContent[] = '?value='.$int;
	}
	$fieldnamesObj = $feedback['fielname'];
	echo $feedback['html'].getdata($tablekeyvalue,$fieldnamesObj,$actionsContent,$tablename,$tablekey);

	return $results;
}

function getactions($task)
{
	// -- Get Business PROCESS 
	$process = get_process_definition($task['PROC_NAME']);
	$xmlFile = $process['FILE_NAME'];
	$actions = array();
	$taskDefinition = null;
	
	if(!empty($xmlFile))
	{
		$oProcess = new Xpdl();
		$oData = $oProcess->getProcessDataXpdl($xmlFile);
	}
	//print_r($oData->routes);
	
	for($i=0;$i<count($oData->routes);$i++)
	{
		if($oData->routes[$i]['TAS_UID'] == $task['TAS_NEXT'])
		{
				$taskDefinition = get_process_task_defintion_by_name($oData->routes[$i]['ROU_NEXT_TASK']);
				$actions[] = $taskDefinition['TAS_CONDITION_VALUE'];				
		}
	}
	return $actions;
}
function getheader($actions,$table)
{
// -- Header
$name 	   = $table['name'];
$fields    = $table['fields'];
$fieldsArray = array();
$fieldsArray = explode(",",$fields);
//print_r($fieldsArray );

$html = '';
$queryFields = null;
$queryFields[] = 'column_name';
$whereArray = null;
$whereArray[] = "table_name = '{$name}'";
$dbnameobj = 'ecashpdq_paycollections';
$tablename = 'information_schema.columns';
$fielnames = array();
$fielname = array();
$index = 0; $fieldnamesObj = NULL;
						$fieldnamesObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
						$fieldnamesObj1 = $fieldnamesObj;
						//print_r($fieldnamesObj);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//									DISPLAY QUERY															  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*$html = '<table class="table table-bordered table-responsive-md table-striped text-center">';*/

	
						$html = "<table class='table table-striped table-bordered' style='white-space: nowrap;font-size: 10px;border: 1px solid black;'>";
						
						// -- Header.
							$html = $html."<thead><tr>";  
							// -- Actions
							for($i=0;$i<count($actions);$i++)
							{
								$html = $html. "<td valign='top' style='border: 1px solid black;'>" .$actions[$i]. "</td>";
							}
						foreach($fieldnamesObj as $row)
						{ 
						  //print_r($row ['COLUMN_NAME']);
						// -- search the fields in the list.
						$column_name = '';
						if(isset($row ['COLUMN_NAME'])){$column_name = $row ['COLUMN_NAME'];}
						elseif(isset($row ['column_name'])){$column_name = $row ['column_name'];}

						 if(empty($fieldsArray[0]))
						 {	
						    $fielname[$index]['column_name'] = $column_name;//$row ['column_name'];					 
							foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
							$html = $html. "<td valign='top' style='border: 1px solid black;'>" . nl2br($row[$key]) . "</td>"; 
							$index = $index + 1;
						 }
						 else
						 {
							 $col = array_search($column_name,$fieldsArray);
							 if(is_numeric($col))
							 {
								$fielname[$index]['column_name'] = $column_name;//$row ['column_name'];					 
								foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
								$html = $html. "<td valign='top' style='border: 1px solid black;'>" . nl2br($row[$key]) . "</td>"; 
								$index = $index + 1;
							 }
						 }
						 
							//$fielnames['COLUMN_NAME'] = $fielname;
						}
																			$html = $html. "</tr></thead>"; 

						//$html = $html. "</table>"; 
						$fielnames['html'] = $html;
						$fielnames['fielname'] = $fielname;
						return $fielnames ;//$html;
						
}

function getdata($id,$fieldnamesObj,$actionsContent,$tbl_name,$tbl_key )
{
	//echo $tbl_name.' - '.$tbl_key.'  '.$id;
// -- Row Items
$html = '';
$queryFields = null;

	$rowCount = count($fieldnamesObj);
	//echo $rowCount;
	foreach($fieldnamesObj as $row1)
	{ 
					$queryFields[] = $row1['column_name'];
	}
$whereArray = null;
$whereArray[] = "{$tbl_key} = '{$id}'";
$dbnameobj = 'ecashpdq_paycollections';
$tablename = $tbl_name;
$fieldvaluesObj = search_dynamic($dbnameobj,$tablename,$whereArray,$queryFields);
$arr = array();
$arrV = array();
$count = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//									DISPLAY QUERY															  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$html = $html.  "<tr>";
							// -- Actions
							for($i=0;$i<count($actionsContent);$i++)
							{ $rejectstyle = 
						"style = 'color: #fff;text-shadow: 0 -1px 0 rgba(0,0,0,0.25);background-color: #da4f49;background-image: linear-gradient(to bottom,#ee5f5b,#bd362f);background-repeat: repeat-x;border-color: rgba(0,0,0,0.1)';";
								$approvestyle = "style = 'color: #fff;text-shadow: 0 -1px 0 rgba(0,0,0,0.25);background-color: #49afcd;
								background-image: linear-gradient(to bottom,#5bc0de,#2f96b4);background-repeat: repeat-x;
								border-color: rgba(0,0,0,0.1)';";
								
								if($actionsContent[$i]['NAME'] == 'rejected')
								{
										$html = $html. "<td valign='top' style='border: 1px solid black;'><a class='ls-modal' $rejectstyle href=".$actionsContent[$i]['URL'].">".$actionsContent[$i]['NAME']."</a></td>";
								}
								else
								{
									$html = $html. "<td valign='top' style='border: 1px solid black;'><a class='ls-modal' $approvestyle href=".$actionsContent[$i]['URL'].">".$actionsContent[$i]['NAME']."</a></td>";
								}
							}
//print($row1['COLUMN_NAME']);						
		foreach($fieldvaluesObj as $row)
		{ 
		//print_r($queryFields);
		//echo $count;
			foreach($row AS $key => $value) 
			{ 
				//$row[$key] = stripslashes($value);  
				//$html = $html.  "<td valign='top' style='border: 1px solid black;'>" .$row[1] . "</td>";		
				//break;
			}
			foreach($fieldnamesObj as $row1)
			{ 
				//$queryFields[] = $row1['COLUMN_NAME'];
				$html = $html.  "<td valign='top' style='border: 1px solid black;'>" .$row[$count] . "</td>";		
//echo $row[$count].'-';	
			$count = $count + 1;			
			}
	
		}
    	
	$html = $html.  "</tr>"; 
	$html = $html.  "</table>"; 
	return $html;
}
// End -- function scripts

class oData
{
	public $inputs            = array();
    public $outputs           = array();
    public $dynaforms         = array();
    public $steps             = array();
    public $taskusers         = array();
    public $groupwfs        	 = array();
    public $steptriggers      = array();
    public $dbconnections     = array();
    public $reportTables      = array();
    public $reportTablesVars  = array();
    public $stepSupervisor    = array();
    public $objectPermissions = array();
    public $subProcess        = array();
    public $lanes 			 = array();
	public $caseTracker       = array();
    public $caseTrackerObject = array();
    public $stage             = array();
    public $fieldCondition    = array();
	public $event             = array();
    public $triggers          = array();
    public $caseScheduler     = array();
    public $dynaformFiles     = array();
	public $datafields 		  = array();
	public static $task_priority = 0; 
	public $routes    		  = array();
    public $tasks			  = array();
	
	  function __construct() 
	  {
		//$this->name = $name;
	  }
}

function cmp($a,$b)
{	$col = 'TAS_PRIORITY';
    return strcmp($a[$col],$b[$col]);
}

if(!function_exists('trgger_process_event'))
{
	function trigger_process_event($event_name,$data)
	{

		// -- Current processor.
		$currentTask = array();
		
		// -- Get Business EVENT
		$event = get_process_event($event_name);
		
		// -- Get Business PROCESS 
		$process = get_process_definition($event['EV_PROCNAME']);
		$xmlFile = $process['FILE_NAME'];
		
		// -- processing XPDL.
		$oProcess = new Xpdl();
		$oData    = new oData();
		$oData->datafields = json_decode($process['PROC_JSON_VARIABLES'],true);
		//$oData = $oProcess->getProcessDataXpdl($xmlFile);
		//print_r($oData->datafields);
		
		// -- Pass Data to the workflow/process & keep them.
		$json_array = array();
		for($int=0;$int<count($data);$int++)
		{
			for($i=0;$i<count($oData->datafields);$i++)
			{
						if($oData->datafields[$i]['DATA_ID'] == $data[$int]['DATA_ID'])
						{
							$oData->datafields[$i]['DATA_VALUE'] = $data[$int]['DATA_VALUE'];
							
							// -- Get current TASK.
							if($data[$int]['DATA_ID'] == 'currenttaskstatus')
							{
								$currentTask['status'] 	  = $data[$int]['DATA_VALUE'];

							}
							
							if($data[$int]['DATA_ID'] == 'currenttask')
							{
								$currentTask['task'] 	  = $data[$int]['DATA_VALUE'];
							}
							
							if($data[$int]['DATA_ID'] == 'currentprocessor')
							{
								$currentTask['processor'] = $data[$int]['DATA_VALUE'];
							}
						}
				   $json_array[] = $oData->datafields[$i]; 						
			}
		}
		if(!empty($json_array))
		{
			$oData->datafields = $json_array;
		}
		//print('<br/>');
		//print_r($oData->datafields);

		// -- Insert Event Trigger		
		$insertQuery[] = $process['PROC_ID'];
		$insertQuery[] = $process['PROC_NAME'];	
		$insertQuery[] = $event['EV_NAME'];
		$insertQuery[] = $currentTask['processor'];
		$insertQuery[] = date('Y-m-d');
		$insertQuery[] = date('h:m:s');
		$trigger_id    = db_insert_business_process_event_trigger($insertQuery);
		
		// -- Get process TASKS
		$Definitions = get_process_tasks($process['PROC_ID']);
		
		// -- EXECUTIONS-Create records
		$Executions = process_execution_pdo($Definitions,$oData,$currentTask,$trigger_id,'');
	}
}
		
if(!function_exists('trigger_process_event_action'))
{
	function trigger_process_event_action($trigger_id,$data)
	{

		// -- Current processor.
		$currentTask = array();
		$json_array = array();

		// -- Get Triggered Business EVENT
		$event = get_triggered_event($trigger_id);
		
		// -- Get Process Execution Tasks by trigger_id
		$processingTasks = get_process_execution_tasks_by_triggerid($trigger_id);
		//$Executions 	 = $processingTasks;
		
		// -- oData -- Arrays with data from XPDL
		$oData = new oData();
		$currentTask['triggerid'] = $trigger_id;
		
		// -- Get the Current Task Details from Action.
		for($int=0;$int<count($data);$int++)
		{
			// -- Get current TASK Status.
			if($data[$int]['DATA_ID'] == 'currenttaskstatus')
			{
				$currentTask['status'] 	  = $data[$int]['DATA_VALUE'];

			}
			// -- Get current Task. 
			if($data[$int]['DATA_ID'] == 'currenttask')
			{
				$currentTask['task'] 	  = $data[$int]['DATA_VALUE'];
			}
			// -- Get current task processor.
			if($data[$int]['DATA_ID'] == 'currentprocessor')
			{
				$currentTask['processor'] = $data[$int]['DATA_VALUE'];
			}
			
			// -- Get condition value.
			if($data[$int]['DATA_ID'] == 'condition')
			{
				$currentTask['condition'] = $data[$int]['DATA_VALUE'];
			}
		}
		// -- Get executed task from database to check if click url multiple times.
		$executedtask = get_process_executed_task($currentTask);
		//print_r($executedtask);
		if($executedtask['TAS_EXC_STATUS'] != 'COMPLETED')
		{
		foreach($processingTasks as $Definitions)
		{
			//$Definitions['TAS_ID'] = //$trigger_id;
			if($currentTask['task'] == $Definitions['TAS_ID'])
			{
				// -- Update status, processor,PROC_JSON_VARIABLES.
				$updateQueryMarks = null;
				$updateQueryMarks[] = ' PROC_JSON_VARIABLES = ?,TAS_CONDITION_VARIABLE = ?,TAS_CONDITION_VALUE = ?,TAS_EXEC_USER = ?,TAS_EXC_STATUS = ?';
			
				$json_datafields_encoded = $Definitions['PROC_JSON_VARIABLES'];
				$json_decoded = json_decode($json_datafields_encoded);
				foreach ($json_decoded as $item) 
				{
					if ($item->DATA_ID == "currenttask") 
					{
						$item->DATA_VALUE = $currentTask['task'];
					}
					
					if ($item->DATA_ID == "currenttaskstatus") 
					{
						$item->DATA_VALUE = $currentTask['status'];
					}

					if ($item->DATA_ID == "currentprocessor") 
					{
						$item->DATA_VALUE = $currentTask['processor'];
					}
					
					if ($item->DATA_ID == "currentprocessor") 
					{
						$item->DATA_VALUE = $currentTask['processor'];
					}
					
					if ($item->DATA_ID == "condition") 
					{
						$item->DATA_VALUE = $currentTask['condition'];
					}
				   $json_array[] = $item; 
				}
		
				 $json_datafields_encoded  = json_encode($json_array);
				 $updatedtQuery = null;
				 $updatedtQuery[] = $json_datafields_encoded;	 
				 $updatedtQuery[] = 'condition';
				 $updatedtQuery[] = $currentTask['condition'];
				 $updatedtQuery[] = $currentTask['processor'];
				 $updatedtQuery[] = $currentTask['status'];				 
				 $updatedtQuery[] = $Definitions['EXEC_ID'];
				 db_update_business_process_execution($updatedtQuery,$updateQueryMarks,'');
			}
		}
		
		// -- Get Process Execution Tasks by trigger_id after an update.
		$Definitions = get_process_execution_tasks_by_triggerid($trigger_id);
		$oData->datafields = $json_array;
		//print_r($oData->datafields);
		//print('<br/>');
			//	print('<br/>');

		// -- EXECUTIONS
		$Executions = process_execution_pdo($Definitions,$oData,$currentTask,$trigger_id,'UPDATE');
		}
		else
		{
			echo '<p>Task has already been actioned by '.$executedtask['TAS_EXEC_USER'] .' on '
			. $executedtask['TAS_EXEC_TIME'].' at '. $executedtask['TAS_EXEC_TIME'].'</p>';
		}
	}
}		
if(!function_exists('get_process_event'))
{
	function get_process_event($EV_NAME)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'process_event';
			$sql = "select * from $tbl_2 where EV_NAME = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($EV_NAME));
			$data = $q->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
}

if(!function_exists('get_process_worklist_by_exec_id'))
{
	function get_process_worklist_by_exec_id($exec_id)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'process_worklist';
			$sql = "select * from $tbl_2 where EXEC_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($exec_id));
			$data = $q->fetchAll();
		return $data;
	}
}

if(!function_exists('get_triggered_event'))
{
	function get_triggered_event($triggerid)
	{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$tbl_2  = 'process_event_trigger';
			$sql = "select * from $tbl_2 where triggerid = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($triggerid));
			$data = $q->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
}

if(!function_exists('get_process_definition'))
{
	function get_process_definition($PROC_NAME)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_definition';
			$sql = "select * from $tbl_2 where PROC_NAME = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($PROC_NAME));
			$data = $q->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
}

if(!function_exists('get_process_definition_by_id'))
{
	function get_process_definition_by_id($PROC_ID)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_definition';
			$sql = "select * from $tbl_2 where PROC_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($PROC_ID));
			$data = $q->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
}

if(!function_exists('generate_task_worklist_content'))
{
	function generate_task_worklist_content($data)
	{
	$path = "workflow/engine/";
	$path = $path.'xpdl.php';
	$html = '';	
	$actionsContent = array();
	$insertQuery 	= array();
	
	// -- Get the base url
	$baseurl = getbaseurl();
	$path = $baseurl.$path;
	
	// -- WORKLIST TASK CONTENT
	if(isset($data['TAS_PERFORMER']))
	{

// -- Generate a dynamic Page table record with approve & reject button [if task DISCRIMINATOR] 
	$actions  = getactions($data);
	$table		 	 = array();
	$table['name']   = $data['tablename'];
	$table['fields'] = $data['fields'];
	
	$feedback = getheader($actions,$table);
	for($int=0;$int<count($actions);$int++)
	{
		$url =	$path."?currentprocessor=".$data['WORKLIST_USER']."&currenttask=".$data['TAS_ID'].
		"&currenttaskstatus=COMPLETED&condition=".$actions[$int]."&triggerid=".$data['TRIGGER_ID'];
		$url = htmlentities($url, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$actionsContent[$int]['URL']  = $url;
		$actionsContent[$int]['NAME'] = $actions[$int];
	}
	$fieldnamesObj = $feedback['fielname'];
	$html = $feedback['html'].getdata($data['tablekeyvalue'],$fieldnamesObj,$actionsContent,
	$data['tablename'],$data['tablekey']);	
	}
	
	$data['TAS_EXEC_CONTENT'] = $html;
	// -- WORKLIST	
	// -- insert into database worklist.
	$insertQuery[] = $data['EXEC_ID'];			// -- EXEC_ID
	$insertQuery[] = $data['TAS_ID'];  			// -- Task ID
	if(isset($data['TASK_EXEC_USER']))
	{
		$insertQuery[] = $data['TASK_EXEC_USER'];	// -- TASK_EXEC_USER	
	}
	else
	{
		$insertQuery[] = '';// -- TASK_EXEC_USER	
	}
	$insertQuery[] = $data['TAS_EXEC_CONTENT'];
	$insertQuery[] = $data['TAS_EXEC_MESSAGE'];
	$insertQuery[] = $data['TAS_EXC_STATUS'];
	$insertQuery[] = $data['WORKLIST_USER'];
	$insertQuery[] = date('Y-m-d');
	$insertQuery[] = date('h:m:s');
	
	//print_r();
	
	$insertQueryMarks[] = '(?,?,?,?,?,?,?,?,?)';
	$WORKLIST_ID = db_insert_business_process_worklist($insertQuery,$insertQueryMarks);
	
	// -- Email approver worklist as well	
		$datafields['WORKLIST_ID']   = $WORKLIST_ID;
		$datafields['customerid']    = $data['WORKLIST_USER'];
		$datafields['emailtemplate'] = 'email_task_content'; 
		auto_email_bpm($datafields);

		return $html;
	}
}

if(!function_exists('get_process_agents_by_role_usercode'))
{
	function get_process_agents_by_role_usercode($usercode,$role)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_1  = 'customer';
			$tbl_2  = 'user';
			$isapprover = 'X';
			// 1. -- UserCode & role
			$sql = "select * from $tbl_1 inner join $tbl_2 ON  $tbl_1.CustomerId = $tbl_2.userid 
			where usercode = ? AND role = ? AND approver = ?"; 			
			$q = $pdo->prepare($sql );
			$q->execute(array($usercode,$role,$isapprover));
			$data = $q->fetchAll();
			
			if($q->rowCount() == 0)
			{
				// 2. -- DebiCheckUserCode & role
				$sql = "select * from $tbl_1 inner join $tbl_2 ON  $tbl_1.CustomerId = $tbl_2.userid 
				where debicheckusercode = ? AND role = ? AND approver = ?";			
				$q = $pdo->prepare($sql );
				$q->execute(array($usercode,$role,$isapprover));
				$data = $q->fetchAll();
			}
			
		return $data;
	}
}

if(!function_exists('get_process_tasks'))
{
	function get_process_tasks($PROC_ID)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_task_definition';
			$sql = "select * from $tbl_2 where PROC_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($PROC_ID));
			$data = $q->fetchAll();
			return $data;
	}
}

if(!function_exists('get_process_task_defintion_by_name'))
{
	function get_process_task_defintion_by_name($taskname)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_task_definition';
			$sql = "select * from $tbl_2 where TAS_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($taskname));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}

if(!function_exists('get_process_previous_executed_task'))
{
	function get_process_previous_executed_task($EXEC_ID,$TRIGGER_ID)
	{		
			$PREV_EXEC_ID = $EXEC_ID - 1;
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_execution';
			$sql = "select * from $tbl_2 where EXEC_ID = ? AND TRIGGER_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($PREV_EXEC_ID,$TRIGGER_ID));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}

if(!function_exists('get_process_executed_task'))
{
	function get_process_executed_task($data)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_execution';
			$sql = "select * from $tbl_2 where TAS_ID = ? AND TRIGGER_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($data['task'],$data['triggerid']));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
	}
}

if(!function_exists('get_process_execution_tasks_by_triggerid'))
{
	function get_process_execution_tasks_by_triggerid($triggerid)
	{		
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_execution';
			$sql = "select * from $tbl_2 where TRIGGER_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($triggerid));
			$data = $q->fetchAll();
			return $data;
	}
}

if(!function_exists('get_process_execution_tasks_by_triggerid_task'))
{
	function get_process_execution_tasks_by_triggerid_task($triggerid,$taskname)
	{		
			$worklist_id = 0;
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$tbl_2  = 'process_execution';
			$sql = "select * from $tbl_2 where TAS_ID = ? AND TRIGGER_ID = ?"; 		
			$q = $pdo->prepare($sql);
			$q->execute(array($taskname,$triggerid));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			if(isset($data['EXEC_ID']))
			{
				$worklist = get_process_worklist_by_exec_id($data['EXEC_ID']);
				foreach($worklist as $row)
				{
					$worklist_id = $row['WORKLIST_ID'];
				}
			}	
echo $worklist_id;			
			return $worklist_id;
	}
}

if(!function_exists('process_definition'))
{
	function process_definition($oData,$xmlFile,$user)
	{
		$output = array();

$arguments = array();
$processingTasks = array();
$processingTask = array();
$actions = array();
$isError = 0; // -- if there is isError > 0

// -- Read Data from work flow/process
$currenttask 		  = '';
$currenttaskstatus 	  = '';
$currentprocessor 	  = '';
$messagelog 	 	  = '';
$message	 	 	  = '';
$db_proc_id 		  = 0;
$db_proc_name		  = '';

/* -- Pass Data to the workflow/process
for($int=0;$int<count($TestData);$int++)
{
	for($i=0;$i<count($oData->datafields);$i++)
	{
				if($oData->datafields[$i]['DATA_ID'] == $TestData[$int]['DATA_ID'])
				{
					$oData->datafields[$i]['DATA_VALUE'] = $TestData[$int]['DATA_VALUE'];
				}
	}
}
*/

$json_datafields = json_encode($oData->datafields);
// -- Read the parameters values
// -- Search for : TableName,ID,Usercode in $json_datafields.	
	$json = json_decode($json_datafields);
	foreach ($json as $item) 
	{
		if ($item->DATA_ID == "currenttask") 
		{
			$currenttask = $item->DATA_VALUE;
		}
		
		if ($item->DATA_ID == "currenttaskstatus") 
		{
			$currenttaskstatus = $item->DATA_VALUE;
		}

		if ($item->DATA_ID == "currentprocessor") 
		{
			$currentprocessor = $item->DATA_VALUE;
		}

		if ($item->DATA_ID == "message") 
		{
			$messagelog = $item->DATA_VALUE;
		}		
	}
		
// ------------------------------------------------------------  //
echo '<div class="row">';
echo '<div class="table-responsive">';
echo '<h3>Process Definition</h3>';
echo '<br/>';
echo '<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
echo '<tr><th>Workflow Name</th>';
echo '<th>File Name</th>';
echo '<th>Workflow Parameters</th></tr>';
echo '<tr>';
echo '<td>'.$oData->routes[0]['PRO_UID'].'</td>';
echo '<td>'.$xmlFile.'</td>';
echo '<td>'.json_encode($oData->datafields).'</td>';
echo '</tr>';
echo '</table>';
// -- Insert into database.
$insertQuery[] = $oData->routes[0]['PRO_UID'];
$insertQuery[] = $xmlFile;
$insertQuery[] = '0.0';
$insertQuery[] = 'APPROVAL';
$insertQuery[] = json_encode($oData->datafields);
$insertQuery[] = 'Active';
$insertQuery[] = $user;
$insertQuery[] = date('Y-m-d');
$insertQuery[] = date('h:m:s');
$db_proc_id    = db_insert_business_process($insertQuery);
$db_proc_name  = $oData->routes[0]['PRO_UID']; 

//print_r($oData->tasks);

// -- PROCESS TASK DEFITIONS	- COMBING ROUTES & TASKS IN ONE ARRAY - PROCTASKSDEF
for($i=0;$i<count($oData->routes);$i++)
{	$performer = '';
	$fieldname = '';
	$fieldvalue = '';
	$function		= '';
	$priority		= 0;
	$task = $oData->routes[$i]['TAS_UID'];
	// -- Task
	for($j=0;$j<count($oData->tasks);$j++)
	{
		if($oData->routes[$i]['TAS_UID'] == $oData->tasks[$j]['TAS_UID'])
		{
			$processingTask[$i]['TAS_UID'] 		 = $oData->routes[$i]['TAS_UID'];
			$processingTask[$i]['ROU_TYPE'] 	 = $oData->routes[$i]['ROU_TYPE'];
			$processingTask[$i]['ROU_NEXT_TASK'] = $oData->routes[$i]['ROU_NEXT_TASK'];
			$processingTask[$i]['PROC_ID'] 		 	   = $db_proc_id;
			$processingTask[$i]['PROC_NAME'] 		 	   = $db_proc_id;

			if(isset($oData->tasks[$j]['TAS_FIELDNAME_DISCRIMINATOR']))
			{
				$fieldname = $oData->tasks[$j]['TAS_FIELDNAME_DISCRIMINATOR'];
				$processingTask[$i]['TAS_CONDITION_VARIABLE'] = $fieldname;							
			}
			else
			{
				$processingTask[$i]['TAS_CONDITION_VARIABLE'] = 'CVAL';
			}
			
			if(isset($oData->tasks[$j]['TAS_FIELDVALUE_DISCRIMINATOR']))
			{
				$fieldvalue = $oData->tasks[$j]['TAS_FIELDVALUE_DISCRIMINATOR'];
				$actions[] = $fieldvalue;
				$processingTask[$i]['TAS_CONDITION_VALUE'] = $fieldvalue;				
			}
			else
			{
				$processingTask[$i]['TAS_CONDITION_VALUE'] = 'CVAR';				
			}
			
			if(isset($oData->tasks[$j]['TAS_PERFORMER']['PERFORMER']))
			{
				$performer = $oData->tasks[$j]['TAS_PERFORMER']['PERFORMER'];
				$processingTask[$i]['TAS_PERFORMER'] = $performer;				
			}
			else
			{
				$processingTask[$i]['TAS_PERFORMER'] = '';
			}
			
			if(isset($oData->tasks[$j]['TAS_SCRIPT']))
			{
				$function = $oData->tasks[$j]['TAS_SCRIPT'];
				$processingTask[$i]['TAS_SCRIPT'] = $function;
			}
			else
			{
				$processingTask[$i]['TAS_SCRIPT'] = '';				
			}
			
			if(isset($oData->tasks[$j]['TAS_PRIORITY']))
			{
				$priority = $oData->tasks[$j]['TAS_PRIORITY'];
				$processingTask[$i]['TAS_PRIORITY'] = $priority;
			}
			else
			{
				$processingTask[$i]['TAS_PRIORITY'] = '';				
			}
			
			$processingTask[$i]['PROC_JSON_VARIABLES'] = $json_datafields;
		}
	}
	$processingTasks['PRO_ACTIONS'] = $actions;
	$processingTasks['PRO_TASKS'] = $processingTask;
}
// -- sort by priority.
usort($processingTasks['PRO_TASKS'], "cmp");
//print_r($processingTasks['PRO_TASKS']);

// -- PROCESS DEFITIONS - DISPLAYING 
$insertQuery = null;
echo '<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
echo '<tr>
<th>TaskName</th>
<th>TaskType</th>
<th>Performer</th>
<th>Next Task</th>
<th>Task condition variable</th>
<th>Task condition value</th>
<th>Task Function Script</th>
<th>Task Priority</th>
</tr>';
$Definitions = $processingTasks['PRO_TASKS'];
//print_r($Definitions);

$insertQueryMarks = null;
for($i=0;$i<count($Definitions);$i++)
{
	$TAS_CONDITION_VARIABLE = '';
	$TAS_CONDITION_VALUE = '';
	$TAS_PERFORMER = '';
	$TAS_SCRIPT    = '';
	echo '</tr>';
	echo '<td>'.$Definitions[$i]['TAS_UID'].'</td>';
	echo '<td>'.$Definitions[$i]['ROU_TYPE'].'</td>';
	if(isset($Definitions[$i]['TAS_PERFORMER']))
	{echo '<td>'.$Definitions[$i]['TAS_PERFORMER'].'</td>';
		$TAS_PERFORMER = $Definitions[$i]['TAS_PERFORMER'] ;
	}
	else
	{echo '<td></td>';}

	echo '<td>'.$Definitions[$i]['ROU_NEXT_TASK']  .'</td>';
	//echo '<td>'.$Definitions[$i]['PROC_JSON_VARIABLES'].'</td>';

	if(isset($Definitions[$i]['TAS_CONDITION_VARIABLE']))
	{
		echo '<td>'.$Definitions[$i]['TAS_CONDITION_VARIABLE'].'</td>';
		$TAS_CONDITION_VARIABLE = $Definitions[$i]['TAS_CONDITION_VARIABLE'];		
	}
	else
	{echo '<td></td>';}

	
	if(isset($Definitions[$i]['TAS_CONDITION_VALUE']))
	{
		echo '<td>'.$Definitions[$i]['TAS_CONDITION_VALUE'].'</td>';
		$TAS_CONDITION_VALUE = $Definitions[$i]['TAS_CONDITION_VALUE'];
	}
	else
	{
		echo '<td></td>';
	}
	echo '<td>'.$Definitions[$i]['TAS_SCRIPT'].'</td>';
	echo '<td>'.$Definitions[$i]['TAS_PRIORITY'].'</td>';
	echo'</tr>';
	
	// -- Build Database Insert Query 
	$insertQuery[] = $db_proc_id;
	$insertQuery[] = $Definitions[$i]['TAS_UID'];
	$insertQuery[] = $Definitions[$i]['TAS_UID'];	
	$insertQuery[] = $Definitions[$i]['ROU_NEXT_TASK'];
	$insertQuery[] = $Definitions[$i]['ROU_TYPE'];
	$insertQuery[] = $TAS_CONDITION_VARIABLE;
	$insertQuery[] = $TAS_CONDITION_VALUE;
	$insertQuery[] = $TAS_PERFORMER;
	$insertQuery[] = $Definitions[$i]['TAS_SCRIPT'];
	$insertQuery[] = $Definitions[$i]['TAS_PRIORITY'];	
	$insertQuery[] = $user;
	$insertQuery[] = date('Y-m-d');
	$insertQuery[] = date('h:m:s');
	$insertQueryMarks[] = '(?,?,?,?,?,?,?,?,?,?,?,?,?)';		
}
echo '</table>';
echo '</div>';
echo '</div>';
// -- Insert process tasks into  database.
db_insert_business_process_tasks($insertQuery,$insertQueryMarks);

// ------------------------------------------------------------  // 
		return $Definitions;
	}
}

if(!function_exists('process_execution_pdo'))
{
	function process_execution_pdo($processingTasks,$oData,$current,$trigger_id,$operation)
	{
		$output = array();
		// -- Get Business Process Defintion once.
		$isBusinessProcess = 0;
		$processdef        = null;
		$Executions = array();
		$isError = 0;
		$TAS_SCRIPT_TEMP = '';
		
//$Definitions = $processingTasks;//['PRO_TASKS'];
foreach($processingTasks as $Definitions)
{
	$TAS_SCRIPT_TEMP = '';
	// -- get business process definition
	if($isBusinessProcess == 0)	
	{
		$processdef = get_process_definition_by_id($Definitions['PROC_ID']);
		$isBusinessProcess = $isBusinessProcess + 1;
	}
	$Definitions['TRIGGER_ID'] = $trigger_id;
	$Definitions['PROC_NAME'] = $processdef['PROC_NAME'];
// -- Exception if there is task script.
	$message = '';
	$result = array();
	$task = null;
	$task['TAS_ID'] 	= $Definitions['TAS_ID'];	
	$task['TRIGGER_ID'] = $trigger_id;	
	$task['processor'] 	= $current['processor'];
	$task['TAS_NEXT'] 	= $Definitions['TAS_NEXT'];	
	$task['PROC_NAME'] 	= $Definitions['PROC_NAME'];
	
	if(isset($Definitions['TAS_PERFORMER']))
	{
		$task['TAS_PERFORMER'] = $Definitions['TAS_PERFORMER']; 	
	}
	else
	{
		$task['TAS_PERFORMER'] = '';
	}
	// -- Check if work flow current task is same one being processed, 
	// -- if so check the status, if its complete don't execute the task script
	if($Definitions['TAS_ID'] == $current['task'])
	{
		if($current['status'] == 'COMPLETED')
		{
			if(isset($Definitions['TAS_SCRIPT']))
			{ $TAS_SCRIPT_TEMP = $Definitions['TAS_SCRIPT'];}
			$Definitions['TAS_SCRIPT'] = '';
			$message 					   = 'Task has been '.$current['condition'].' by '.$current['processor'];
		}
	}
	// -- condition 
	//echo $Definitions['TAS_SCRIPT'].'===========================';
	if(!empty($Definitions['TAS_SCRIPT']))
	{
	// 1. statically by hard-code, 
	if(isset($Definitions['PROC_JSON_VARIABLES']))
	{
		//print('IF');
		//if(empty($Definitions['PROC_JSON_VARIABLES']))
		{
					//print_r($oData->datafields);

			$Definitions['PROC_JSON_VARIABLES'] = json_encode($oData->datafields);//$processdef['PROC_JSON_VARIABLES'];
		}		
		$arguments[0] = $Definitions['PROC_JSON_VARIABLES'];//$json_datafields;
	}
	else
	{	//print('ELSE');
		//print_r($oData->datafields);
		$json_datafields = json_encode($oData->datafields);
		$arguments[0] = $json_datafields;//$processdef['PROC_JSON_VARIABLES'];//$json_datafields;
		$Definitions['PROC_JSON_VARIABLES'] = $json_datafields;//$processdef['PROC_JSON_VARIABLES'];
	}
	
	if(empty($operation)) // -- Create
	{
		// -- Insert business process execution
		$Definitions['EXEC_ID'] = build_business_process_execution_pdo($Definitions);
	}
	$task['EXEC_ID'] = $Definitions['EXEC_ID'];
	$arguments[1] = $task; // my $to
	$arguments[2] = $oData;

	// -- Dynamic Calling function
	$result = call_user_func_array($Definitions['TAS_SCRIPT'], $arguments);
    $Definitions['TAS_EXEC_USER'] 	 = $result['TAS_EXEC_USER'] 	 ;
	$Definitions['TAS_EXC_STATUS']   = $result['TAS_EXC_STATUS'] 	 ;
	$Definitions['TAS_EXEC_DATE']    = $result['TAS_EXEC_DATE']     ;
	$Definitions['TAS_EXEC_TIME']    = $result['TAS_EXEC_TIME']     ;
	$Definitions['TAS_EXEC_MESSAGE'] = $result['TAS_EXEC_MESSAGE']  ;
	$Definitions['TAS_EXEC_CONTENT'] = $result['TAS_EXEC_CONTENT']  ;
		
	if($Definitions['TAS_EXC_STATUS'] == 'ERROR' || $Definitions['TAS_EXC_STATUS'] == 'IN PROGRESS')
	{
		$isError = 1;
	}
		
	 // -- Update Values from Task Script 
	 $updateQueryMarks = null;
	 $updatedtQuery    = null;
	 $updateQueryMarks[] = ' TAS_EXEC_USER = ?,TAS_EXC_STATUS = ?,TAS_EXEC_MESSAGE = ?,TAS_EXEC_CONTENT = ?';
	 $updatedtQuery[] = $Definitions['TAS_EXEC_USER'];	 
	 $updatedtQuery[] = $Definitions['TAS_EXC_STATUS'];
	 $updatedtQuery[] = $Definitions['TAS_EXEC_MESSAGE'];
	 $updatedtQuery[] = $Definitions['TAS_EXEC_CONTENT'];
	 $updatedtQuery[] = $Definitions['EXEC_ID'];
	 db_update_business_process_execution($updatedtQuery,$updateQueryMarks,'Yes');

    }
	else
	{
	    // -- Restore Task Script Name
		if($Definitions['TAS_ID'] == $current['task'])
		{
			if($current['status'] == 'COMPLETED')
			{
				$Definitions['TAS_SCRIPT'] = $TAS_SCRIPT_TEMP;
			}
		}
		
	   if($isError != 1)
	   {
		$Definitions['TAS_EXC_STATUS'] 	 = 'COMPLETED';
		$Definitions['TAS_EXEC_DATE']     = date('Y-m-d');
		$Definitions['TAS_EXEC_TIME']     = date('h:m:s');
		$Definitions['TAS_EXEC_USER'] 	 = '';
		}
		else
		{
		 $Definitions['TAS_EXC_STATUS']    = 'READY';
		 $Definitions['TAS_EXEC_DATE']     = date('Y-m-d');
		 $Definitions['TAS_EXEC_TIME']     = date('h:m:s');
		}
		$Definitions['TAS_EXEC_USER'] 	  = $current['processor'];
		$Definitions['TAS_EXEC_MESSAGE']  = $message;
		$Definitions['TAS_EXEC_CONTENT']  = '';
	    
		if(empty($operation)) // -- Create
		{		
			// -- Insert business process execution
			$Definitions['EXEC_ID'] = build_business_process_execution_pdo($Definitions);
		}
		else			    // -- Update
		{
			
			// -- Update Values from Task Script 
			 $updateQueryMarks = null;
			 $updatedtQuery    = null;
			 $updateQueryMarks[] = ' TAS_EXEC_USER = ?,TAS_EXC_STATUS = ?,TAS_EXEC_MESSAGE = ?,TAS_EXEC_CONTENT = ?';
			 $updatedtQuery[] = $Definitions['TAS_EXEC_USER'];	 
			 $updatedtQuery[] = $Definitions['TAS_EXC_STATUS'];
			 $updatedtQuery[] = $Definitions['TAS_EXEC_MESSAGE'];
			 $updatedtQuery[] = $Definitions['TAS_EXEC_CONTENT'];
			 $updatedtQuery[] = $Definitions['EXEC_ID'];
			 db_update_business_process_execution($updatedtQuery,$updateQueryMarks,'Yes');
		}
	}
	$Executions[] = $Definitions;
}
// -- PROCESS EXECUTION DISPLAY
//echo  $_SERVER['DOCUMENT_ROOT'];
//include $_SERVER['DOCUMENT_ROOT'].'/ecashmeup/bank.php';
if(!empty($operation)) // -- Ony show when an action has been peformed by user
{
		echo '<div class="row">';
		echo '<div class="table-responsive">';
		echo '<h3>Proccess Default Executions</h3>';
		echo '<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
		echo '<tr>
		<th>TaskName</th>
		<th>Task User</th>
		<th>Task Status</th>
		<th>Task Date</th>
		<th>Task Time</th>
		<th>Task Message log</th>
		<th>Task Content</th>
		</tr>';
		//$Executions = $Definitions;//$processingTasks['PRO_TASKS'];
		$insertQueryMarks = null;
		$insertQuery	  = null;
		for($i=0;$i<count($Executions);$i++)
		{
				echo '<tr><td>'.$Executions[$i]['TAS_ID'].'</td>'; 
				echo '<td>'.$Executions[$i]['TAS_EXEC_USER'].'</td>'; 
				echo '<td>'.$Executions[$i]['TAS_EXC_STATUS'].'</td>'; 
				echo '<td>'.$Executions[$i]['TAS_EXEC_DATE'].'</td>';   
				echo '<td>'.$Executions[$i]['TAS_EXEC_TIME'].'</td>';
				echo '<td>'.$Executions[$i]['TAS_EXEC_MESSAGE'].'</td>';
				echo '<td>'.$Executions[$i]['TAS_EXEC_CONTENT'].'</td></tr>';
		} 	
		echo '</table>';
		echo '</div>';
		echo '</div>';
	}
// --------------------------------------------------------------------------- //
		$output = $Executions; 
		return $output;
	}
}

if(!function_exists('process_execution_array'))
{
	function process_execution_array($processingTasks,$oData,$current)
	{
		$output = array();
// --------------------------------------------------------------------------- //
// -- PROCESS EXECUTION - CALLING TASK SCRIPT
/* -- Pass Data to the workflow/process
for($int=0;$int<count($TestData);$int++)
{
	for($i=0;$i<count($oData->datafields);$i++)
	{
				if($oData->datafields[$i]['DATA_ID'] == $TestData[$int]['DATA_ID'])
				{
					$oData->datafields[$i]['DATA_VALUE'] = $TestData[$int]['DATA_VALUE'];
				}
	}
}
*/
$Definitions = $processingTasks;//['PRO_TASKS'];
for($i=0;$i<count($Definitions);$i++)
{
// -- Exception if there is task script.
	$message = '';
	$result = array();
	$task = null;
	$task['TAS_UID'] = $Definitions[$i]['TAS_UID'];			
	if(isset($Definitions[$i]['TAS_PERFORMER']))
	{
		$task['TAS_PERFORMER'] = $Definitions[$i]['TAS_PERFORMER']; 	
	}
	else
	{
		$task['TAS_PERFORMER'] = '';
	}
	// -- Check if work flow current task is same one being processed, 
	// -- if so check the status, if its complete don't execute the task script
	if($Definitions[$i]['TAS_UID'] == $current['task'])
	{
		if($current['taskstatus'] == 'COMPLETED')
		{
			$Definitions[$i]['TAS_SCRIPT'] = '';
			$message 					   = $messagelog;
		}
	}
	
	if(!empty($Definitions[$i]['TAS_SCRIPT']))
	{
	// 1. statically by hard-code, 
	$arguments[0] = $Definitions[$i]['PROC_JSON_VARIABLES'];//$json_datafields; // my $something
	// -- Insert business process execution
	$Definitions[$i]['EXEC_ID'] = build_business_process_execution($Definitions);
	$task['EXEC_ID'] = $Definitions[$i]['EXEC_ID'];
	$arguments[1] = $task; // my $to
	$arguments[2] = $oData;

	// -- Dynamic Calling function
	$result = call_user_func_array($Definitions[$i]['TAS_SCRIPT'], $arguments);
    $Definitions[$i]['TAS_EXEC_USER'] 	  = $result['TAS_EXEC_USER'] 	 ;
	$Definitions[$i]['TAS_EXC_STATUS']  = $result['TAS_EXC_STATUS'] 	 ;
	$Definitions[$i]['TAS_EXEC_DATE']    = $result['TAS_EXEC_DATE']     ;
	$Definitions[$i]['TAS_EXEC_TIME']    = $result['TAS_EXEC_TIME']     ;
	$Definitions[$i]['TAS_EXEC_MESSAGE'] = $result['TAS_EXEC_MESSAGE']  ;
	$Definitions[$i]['TAS_EXEC_CONTENT'] = $result['TAS_EXEC_CONTENT']  ;
		
	if($Definitions[$i]['TAS_EXC_STATUS'] == 'ERROR' || $Definitions[$i]['TAS_EXC_STATUS'] == 'IN PROGRESS')
	{
		$isError = 1;
	}
		
	 // -- Update Values from Task Script 
	 $updateQueryMarks = null;
	 $updatedtQuery    = null;
	 $updateQueryMarks[] = ' TAS_EXEC_USER = ?,TAS_EXC_STATUS = ?,TAS_EXEC_MESSAGE = ?,TAS_EXEC_CONTENT = ?';
	 $updatedtQuery[] = $Definitions[$i]['TAS_EXEC_USER'];	 
	 $updatedtQuery[] = $Definitions[$i]['TAS_EXC_STATUS'];
	 $updatedtQuery[] = $Definitions[$i]['TAS_EXEC_MESSAGE'];
	 $updatedtQuery[] = $Definitions[$i]['TAS_EXEC_CONTENT'];
	 $updatedtQuery[] = $Definitions[$i]['EXEC_ID'];
	 db_update_business_process_execution($updatedtQuery,$updateQueryMarks,'Yes');

    }
	else
	{
	   if($isError != 1)
	   {
		$Definitions[$i]['TAS_EXC_STATUS'] 	 = 'COMPLETED';
		$Definitions[$i]['TAS_EXEC_DATE']     = date('Y-m-d');
		$Definitions[$i]['TAS_EXEC_TIME']     = date('h:m:s');
		$Definitions[$i]['TAS_EXEC_USER'] 	 = '';

		}
		else
		{
		 $Definitions[$i]['TAS_EXC_STATUS']    = 'READY';
		 $Definitions[$i]['TAS_EXEC_DATE']     = date('Y-m-d');
		 $Definitions[$i]['TAS_EXEC_TIME']     = date('h:m:s');
		}
		$Definitions[$i]['TAS_EXEC_USER'] 	  = $current['processor'];
		$Definitions[$i]['TAS_EXEC_MESSAGE']  = '';
		$Definitions[$i]['TAS_EXEC_CONTENT']  = $message;
			
		// -- Insert business process execution
		$Definitions[$i]['EXEC_ID'] = build_business_process_execution($Definitions);
	}
}
// -- PROCESS EXECUTION DISPLAY
echo '<div class="row">';
echo '<div class="table-responsive">';
echo '<h3>Proccess Default Executions</h3>';
echo '<table class="table table-striped table-bordered" style="white-space: nowrap;font-size: 10px;">';
echo '<tr>
<th>TaskName</th>
<th>Task User</th>
<th>Task Status</th>
<th>Task Date</th>
<th>Task Time</th>
<th>Task Message log</th>
<th>Task Content</th>
</tr>';
$Executions = $Definitions;//$processingTasks['PRO_TASKS'];
$insertQueryMarks = null;
$insertQuery	  = null;
for($i=0;$i<count($Executions);$i++)
{
		echo '<tr><td>'.$Executions[$i]['TAS_UID'].'</td>'; 
		echo '<td>'.$Executions[$i]['TAS_EXEC_USER'].'</td>'; 
		echo '<td>'.$Executions[$i]['TAS_EXC_STATUS'].'</td>'; 
		echo '<td>'.$Executions[$i]['TAS_EXEC_DATE'].'</td>';   
		echo '<td>'.$Executions[$i]['TAS_EXEC_TIME'].'</td>';
		echo '<td>'.$Executions[$i]['TAS_EXEC_MESSAGE'].'</td>';
		echo '<td>'.$Executions[$i]['TAS_EXEC_CONTENT'].'</td></tr>';
} 	
echo '</table>';
echo '</div>';
echo '</div>';
// --------------------------------------------------------------------------- //
		$output = $Executions; 
		return $output;
	}
}
if(!function_exists('db_insert_business_process'))
{
	function db_insert_business_process($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// -- Process DEFITIONS
		 $sqlQuestionMarks[] = '(?,?,?,?,?,?,?,?,?)';
		 //$pdo->beginTransaction();
			try
			{
				$sql = "INSERT INTO process_definition
						(
						PROC_NAME,
						FILE_NAME,
						VERSION,
						PROC_CATEGORY,
						PROC_JSON_VARIABLES,
						PROC_STATUS,
						CREATEDBY,
						CREATEDON,
						CREATEDAT)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();		
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			return $id;
	}
}

if(!function_exists('db_insert_business_process_event_trigger'))
{
	function db_insert_business_process_event_trigger($data)
	{
		// -- Connect to the database.
		$id  = 0;
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// -- Process DEFITIONS
		 $sqlQuestionMarks[] = '(?,?,?,?,?,?)';
		 //$pdo->beginTransaction();
			try
			{
				$sql = "INSERT INTO process_event_trigger
				(
				PROC_ID,
				PROC_NAME,
				EV_NAME,
				CREATEDBY,
				CREATEDON,
				CREATEDAT)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();		
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			return $id;
	}
}





if(!function_exists('db_insert_business_process_tasks'))
{
	function db_insert_business_process_tasks($data,$sqlQuestionMarks)
	{
		// -- Process Task DEFITIONS
// -- Connect to the database.
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		 $pdo->beginTransaction();
			try
			{
				$sql = "INSERT INTO process_task_definition(
						PROC_ID,
						TAS_ID,
						TAS_NAME,
						TAS_NEXT,
						TAS_TYPE,
						TAS_CONDITION_VARIABLE,
						TAS_CONDITION_VALUE,
						TAS_PERFORMER,
						TAS_SCRIPT,
						TAS_PRIORITY,
						CREATEDBY,
						CREATEDON,
						CREATEDAT)
						VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
			}					
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
			$pdo->commit();
}
}
if(!function_exists('db_insert_business_process_worklist'))
{
	function db_insert_business_process_worklist($data,$sqlQuestionMarks)
	{
		 // -- Process Worklist
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		 $id  = 0;
			try
			{
				$sql = "INSERT INTO process_worklist(
				EXEC_ID,
				TAS_ID,
				TAS_EXEC_USER,
				TAS_EXEC_CONTENT,
				TAS_EXEC_MESSAGE,
				TAS_EXC_STATUS,
				WORKLIST_USER,
				WORKLIST_DATE,
				WORKLIST_TIME)
				VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);					
				$q->execute($data);
				$id = $pdo->lastInsertId();	
			}					
			catch (PDOException $e)
			{
				echo $e->getMessage();
			}
			return $id;
   }
}
if(!function_exists('build_business_process_execution_pdo'))
{
	function build_business_process_execution_pdo($Executions)
	{
		$id = 0;
		//print_r($Executions);
	// -- Build Database Insert Query 
	$insertQuery = array();
	$i = 0;
	$insertQuery[] = $Executions['PROC_ID'];
	$insertQuery[] = $Executions['PROC_NAME'];
	$insertQuery[] = $Executions['PROC_JSON_VARIABLES'];
	$insertQuery[] = $Executions['TAS_ID'];
	$insertQuery[] = $Executions['TAS_ID'];	
	$insertQuery[] = $Executions['TAS_NEXT'];
	$insertQuery[] = $Executions['TAS_TYPE'];
	if(isset($Executions['TAS_CONDITION_VARIABLE'])){$insertQuery[] = $Executions['TAS_CONDITION_VARIABLE'];}
	else{$insertQuery[] = $Executions['TAS_CONDITION_VARIABLE'] = '';}
	
	if(isset($Executions['TAS_CONDITION_VALUE']))
	{
		$insertQuery[] =  $Executions['TAS_CONDITION_VALUE'];
	}
	else
	{
		$insertQuery[] = '';
	}
	
	if(isset($Executions['TAS_EXEC_USER']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_USER'] = '';
	}
	else
	{
		$insertQuery[] = '';
	}
	
	
	if(isset($Executions['TAS_EXC_STATUS']))
	{
		$insertQuery[] = $Executions['TAS_EXC_STATUS'];
	}
		else
	{
		$insertQuery[] = '';
	}
	
	
	if(isset($Executions['TAS_EXEC_DATE']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_DATE'];
	}
	else
	{
		$insertQuery[] = date('Y-m-d');
	}
	
	 
	if(isset($Executions['TAS_EXEC_TIME']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_TIME'];
	}	
	else
	{
		$insertQuery[] = date('h:m:s');
	}
	
	if(isset($Executions['TAS_EXEC_MESSAGE']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_MESSAGE'];
	}else
	{
		$insertQuery[] = '';
	}
	
	if(isset($Executions['TAS_EXEC_CONTENT']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_CONTENT'];
	}else
	{
		$insertQuery[] = '';
	}	
	$insertQuery[] = '';//EVENT_ID
	$insertQuery[] = '';//EVENT_NAME
	
	if(isset($Executions['TAS_EXEC_USER']))
	{
		$insertQuery[] = $Executions['TAS_EXEC_USER'];
	}
	else
	{
		$insertQuery[] = '';
	}
	
	$insertQuery[] = date('Y-m-d');
	$insertQuery[] = date('h:m:s');
	$insertQuery[] = $Executions['TRIGGER_ID'];
	$insertQuery[] = $Executions['TAS_SCRIPT'];

	$insertQueryMarks[] = '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';		
	// -- Insert process tasks into  database.
	$id = db_insert_business_process_execution($insertQuery,$insertQueryMarks);
	return $id;
	}
}

if(!function_exists('build_business_process_execution'))
{
	function build_business_process_execution($Executions)
	{
		$id = 0;
		//print_r($Executions);
	// -- Build Database Insert Query 
	$i = 0;
	$insertQuery[] = $Executions[$i]['PROC_ID'];
	$insertQuery[] = $Executions[$i]['PROC_NAME'];
	$insertQuery[] = $Executions[$i]['PROC_JSON_VARIABLES'];
	$insertQuery[] = $Executions[$i]['TAS_UID'];
	$insertQuery[] = $Executions[$i]['TAS_UID'];	
	$insertQuery[] = $Executions[$i]['ROU_NEXT_TASK'];
	$insertQuery[] = $Executions[$i]['ROU_TYPE'];
$insertQuery[] = isset($Executions[$i]['TAS_CONDITION_VARIABLE']) ?: $Executions[$i]['TAS_CONDITION_VARIABLE'] = '';
	$insertQuery[] = isset($Executions[$i]['TAS_CONDITION_VALUE']) ?: $Executions[$i]['TAS_CONDITION_VALUE'] = '';
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_USER'])?:$Executions[$i]['TAS_EXEC_USER'] = '';
	$insertQuery[] = isset($Executions[$i]['TAS_EXC_STATUS'])?:$Executions[$i]['TAS_EXC_STATUS'] = '';
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_DATE'])?:$Executions[$i]['TAS_EXEC_DATE'] = date('Y-m-d');
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_TIME'])?:$Executions[$i]['TAS_EXEC_TIME'] = date('h:m:s');
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_MESSAGE'])?:$Executions[$i]['TAS_EXEC_MESSAGE'] = '';
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_CONTENT'])?:$Executions[$i]['TAS_EXEC_CONTENT'] = '';	
	$insertQuery[] = '';//EVENT_ID
	$insertQuery[] = '';//EVENT_NAME
	$insertQuery[] = isset($Executions[$i]['TAS_EXEC_USER'])?:$Executions[$i]['TAS_EXEC_USER'] = '';	
	$insertQuery[] = date('Y-m-d');
	$insertQuery[] = date('h:m:s');
	$insertQueryMarks[] = '(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';		
	// -- Insert process tasks into  database.
	$id = db_insert_business_process_execution($insertQuery,$insertQueryMarks);
	return $id;
	}
}

if(!function_exists('db_insert_business_process_execution'))
{
	function db_insert_business_process_execution($data,$sqlQuestionMarks)
	{
		// -- Process Task Executions
		// -- Connect to the database.
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		 //$pdo->beginTransaction();
		 $id = 0;
			try
			{
				$sql = "INSERT INTO process_execution
				(
				PROC_ID,
				PROC_NAME,
				PROC_JSON_VARIABLES,
				TAS_ID,
				TAS_NAME,
				TAS_NEXT,
				TAS_TYPE,
				TAS_CONDITION_VARIABLE,
				TAS_CONDITION_VALUE,
				TAS_EXEC_USER,
				TAS_EXC_STATUS,
				TAS_EXEC_DATE,
				TAS_EXEC_TIME,
				TAS_EXEC_MESSAGE,
				TAS_EXEC_CONTENT,
				EVENT_ID,
				EVENT_NAME,
				CREATEDBY,
				CREATEDON,
				CREATEDAT,
				TRIGGER_ID,
				TAS_SCRIPT)
				VALUES ".implode(',', $sqlQuestionMarks);
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				$id = $pdo->lastInsertId();	
				$pdo->commit();
			}					
			catch (PDOException $e)
			{
				//echo $e->getMessage();
			}
			
			return $id;
}
}
// -- Restore Staged Data
if(!function_exists('db_auto_restore_staged_data'))
{
	function db_auto_restore_staged_data($databasename,$data,$sql)
	{
		 $pdo = Database::connectGeneric($databasename);
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		 $q   = $pdo->prepare($sql);
		 $q->execute($data);
	}
}

// -- Unlock - Update table for lock changes.
if(!function_exists('db_update_table_lockedforchanges'))
{
	function db_update_table_lockedforchanges($databasename,$data)
	{
		 $pdo = Database::connectGeneric($databasename);
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		 $sql = "UPDATE {$data['tablename']} SET lockedforchanges = ? WHERE {$data['tablekey']} = ?";
		 $q   = $pdo->prepare($sql);		 
		 $q->execute(array('',$data['tablekeyvalue']));
	}
}

if(!function_exists('db_update_business_process_execution'))
{
	function db_update_business_process_execution($data,$sqlQuestionMarks,$worklistupdate)
	{
		// -- Process Task Executions
		// -- Connect to the database.
		 $pdo = Database::connect();
		 $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
			{
				$sql = "UPDATE process_execution
				SET
				".implode(',', $sqlQuestionMarks).
				"WHERE EXEC_ID = ?";
				$q   = $pdo->prepare($sql);
				$q->execute($data);
				//print_r($data);
				// -- Check if we need to updates worklist as well
				//if(!empty($worklistupdate))
				{
					// -- get worklist id that might require to be updated.
					$worklist = get_process_worklist_by_exec_id($data[4]);
					foreach($worklist as $row)
					{
						// -- update worklist status.//,TAS_EXEC_CONTENT = ?//$data[3]
						$sql = "UPDATE process_worklist SET TAS_EXEC_USER = ?,TAS_EXEC_MESSAGE = ?,TAS_EXC_STATUS = ? WHERE WORKLIST_ID = ?";
						$q   = $pdo->prepare($sql);				
						$q->execute(array($data[0],$data[2],$data[1],$row['WORKLIST_ID']));
					}
				}
				// -- Update Task Message
				/*else
				{
					// -- get worklist id that might require to be updated.
					$worklist = get_process_worklist_by_exec_id($data[4]);
					foreach($worklist as $row)
					{
						// -- update worklist status.//,TAS_EXEC_CONTENT = ?//$data[3]
						$sql = "UPDATE process_worklist SET TAS_EXEC_USER = ?,TAS_EXEC_MESSAGE = ?,TAS_EXC_STATUS = ? WHERE WORKLIST_ID = ?";
						$q   = $pdo->prepare($sql);				
						$q->execute(array($data[0],$data[3],$data[1],$row['WORKLIST_ID']));
					}
				}*/
			}							
}
}

// -- Loading of Process Definitions and also first process execution. 
/* -- Work flow/Process Source File(s) 
$TestData = array();
$Test	  = array();
$xpdlFile0 = 'C:/xampp/htdocs/ecashmeup/workflow/engine/eCashMeUp_WEngine.xpdl';
$xpdlFile1 = 'C:/xampp/htdocs/ecashmeup/workflow/engine/leave_request.xpdl';
$xpdlFile2 = 'C:/xampp/htdocs/ecashmeup/workflow/engine/MoveProcessed_files.xpdl';
// -- Initial Data
$TestData[0]['DATA_ID']  = 'usercode';
$TestData[0]['DATA_VALUE'] = 'UFET';

$TestData[1]['DATA_ID']  = 'tablename';
$TestData[1]['DATA_VALUE'] = 'collection';

$TestData[2]['DATA_ID']  = 'workflow_initiator';
$TestData[2]['DATA_VALUE'] = '12121212';

$TestData[3]['DATA_ID']  = 'tablekeyvalue';
$TestData[3]['DATA_VALUE'] = '17';*/

// --- Actioned Performed : Condition & Current Task 
$currenttask 		= '';
$currenttaskstatus  = '';
$condition			= '';
$currentprocessor   = '';
$messagelog = '';
$user = getsession('username');

if(empty($_POST))
{
	//$Test = $TestData;
	//process_task_definition($xpdlFile0,$user);
}

if(isset($_GET['currentprocessor']))
{
	$currentprocessor = $_GET['currentprocessor'];
}
//-- Triggered Event Task to be updated.
if(isset($_GET['triggerid']))
{
	if(isset($_GET['currenttask']))
	{
	$currenttask = $_GET['currenttask'];
	//echo "TEST HERE - ".$currenttask;
	if(isset($_GET['currenttaskstatus']))
	{
		$currenttaskstatus = $_GET['currenttaskstatus'];
	}
		 if(isset($_GET['condition']))
		 {
			$condition = $_GET['condition'];

			  if($condition == 'Approved')
			  {
					 $messagelog = 'Approved';
			  }	
			  elseif($condition == 'Rejected')
			  {
					 $messagelog = 'Rejected';
			  }		
		 }

	// -- Task Data
	$data[0]['DATA_ID']    = 'currentprocessor';
	$data[0]['DATA_VALUE'] = $currentprocessor;

	$data[1]['DATA_ID']    = 'currenttask';
	$data[1]['DATA_VALUE'] = $currenttask;//'determine_reviewers';

	$data[2]['DATA_ID']    = 'currenttaskstatus';
	$data[2]['DATA_VALUE'] = $currenttaskstatus;//'COMPLETED';

	$data[3]['DATA_ID']    = 'condition';
	$data[3]['DATA_VALUE'] = $condition;

	$data[4]['DATA_ID']    = 'message';
	$data[4]['DATA_VALUE'] = $messagelog;

// -- Update Triggered Event Tasks
	 trigger_process_event_action($_GET['triggerid'],$data);
	}
}

function process_task_definition($xmlFile,$user)
{
	
// -- Testing
$oProcess = new Xpdl();
$oData = $oProcess->getProcessDataXpdl($xmlFile);

// -- PROCESS DEFITIONS	- WORKFLOW/PROCESS PROCDEF
$Definitions = process_definition($oData,$xmlFile,$user);

// -- PROCESS EXECUTION
$currentTask['task'] 	  = '';
$currentTask['processor'] = '';
$currentTask['status'] 	  = '';

//$Executions = process_execution($Definitions,$oData,$currentTask);

echo '<HR></HR>';
echo '<BR/>';	

}

// -- PROC_ID - be able to get filename
// -- FileName

 ?>