<div name="confirm_ffms" id="confirm_ffms">
<style>
.font-bold {
  font-weight: bold;
}
</style>
<div class="controls">
<div class="container-fluid" style="border:1px solid #ccc ">
<img name="logo" id="logo" src="" />
<div class="row">
		<div>	
				<label name="Reference_1Lbl" id="Reference_1Lbl"></label>
				<table class="table table-borderless" width="100%">
					<tbody>
					<tr>
						<td>Date:</td>	
						<td><label name="member_applicationdate_Lbl" id="member_applicationdate_Lbl"></label></td>		
					</tr>    
					<tr>
						<td>Name:</td>	
						<td><label name="member_name_Lbl" id="member_name_Lbl"></label></td>		
					</tr>    
					<tr>
						<td>Province:</td>	
						<td><label name="member_province_Lbl" id="member_province_Lbl"></label></td>		
					</tr>    
					<tr>
						<td>Contact No:</td>	
						<td><label name="member_contact_Lbl" id="member_contact_Lbl"></label></td>		
					</tr>    	
					</tbody>
				</table>					
			    <p><label name="ffms_header" id="ffms_header" /></p>
				<p>A summary of your current Burial Service Benefit is as follows:</p>
				<table class="table table-primary" width="100%" id="main_member" name="main_member">
					<tbody>
					<tr><th colspan="3">Principal Member</th></tr>
					<<tr>
						<th>Surname and Name</th>	
						<th>ID No</th>	
						<th>Premium</th>
					</tr>   
					</tbody>
				</table>					
<table class="table table-primary" width="100%" id="spouse" name="spouse">
					<tbody>
					<tr><th colspan="3">Spouse</th></tr>
					<tr>
						<th>Surname and Name</th>	
						<th>ID No</th>	
						<th>Date Joined</th>
					</tr>    
					</tbody>
				</table>	
<table class="table table-primary" width="100%" id="Children" name="Children">
					<tbody>
					<tr><th colspan="3">Children</th></tr>
					<tr>
						<th>Surname and Name</th>	
						<th>ID No</th>	
						<th>Date Joined</th>	
						<?php echo $tableChildrenConfirm ;?>														
					</tr>    
					</tbody>
				</table>
<table class="table table-primary" width="100%" id="Extended" name="Extended">
					<tbody>
					<tr><th colspan="5">Extended Family Members</th></tr>
					<tr>
						<th>Surname and Name</th>	
						<th>ID No</th>	
						<th>Waiting Period</th>	
						<th>Benefits</th>							
						<th>Date Joined</th>
						<?php echo $tableExtendedFamilyMembersConfirm ;?>																				
					</tr>    
					</tbody>
				</table>
<table class="table table-primary" width="100%" id="benefiary" name="benefiary">
				<tbody>
					<tr><th colspan="2">Beneficiaries </th></tr>
					<tr><th>Surname and Name</th><th>ID No</th></tr>
				</tbody>																									
</table>
<?php if(isset($tableBeneficiariesConfirm)){echo $tableBeneficiariesConfirm;}?>			
<br />		
		<p>
					<label name="ffms_footer" id="ffms_footer" />		
		
		</p>		</div>	
</div>	
	
</div>
</div>                                                                                     
</div> 
