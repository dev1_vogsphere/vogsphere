<?php
//-------------------------------------------------------------------
//							eproofofpayment
//-------------------------------------------------------------------
$tableDetails  = '';
//-------------------------------------------------------------------
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
	require 'database.php';
////////////////////////////////////////////////////////////////////////
//				Functions
////////////////////////////////////////////////////////////////////////

if(!function_exists('mapping_data'))
{
function mapping_data($html,$dataComp,$dataPayment )
{
$html = str_replace("[Companyname]",$dataComp['name'],$html); 
$html = str_replace("[currency]",$dataComp['currency'],$html); 
$html = str_replace("[Disclaimer]",ReplacePeriodWithSpace(trim_input($dataComp['disclaimer'])),$html); 
$html = str_replace("[Action_Date]",$dataPayment['createdon'],$html); 
$html = str_replace("[createdat]",$dataPayment['createdat'],$html); 
$html = str_replace("[Amount]",$dataPayment['Amount'],$html); 
$html = str_replace("[Account_Number]",$dataPayment['Account_Number'],$html); 
$html = str_replace("[Account_Holder]",$dataPayment['Account_Holder'],$html); 
$html = str_replace("[Branch_Code]",$dataPayment['Branch_Code'],$html); 
$html = str_replace("[bankname]",$dataPayment['bankname'],$html); 
$html = str_replace("[Bank_Reference]",$dataPayment['Bank_Reference'],$html); 
$html = str_replace("[payment]",$dataPayment['collectionid'],$html); 
$html = str_replace("[Description]",$dataPayment['Description'],$html); 
$html = str_replace("[phone]",$dataComp['phone'],$html); 
$html = str_replace("[email]",$dataComp['email'],$html); 
$html = str_replace("[website]",$dataComp['website'],$html); 

return $html;
}}

if(!function_exists('get_globalsettings'))
{
	function get_globalsettings()
	{
		$data = null;
		try
		{
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "select * from globalsettings";
			$q = $pdo->prepare($sql);
			$q->execute();
			$data = $q->fetch(PDO::FETCH_ASSOC);
			Database::disconnect();
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $data;
	}
}
if(!function_exists('get_payment'))
{
	function get_payment($dataFilter)
	{
		$data = null;
		try
		{
			$pdo = Database::connectDB();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "select * from payments where collectionid = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($dataFilter['collectionid']));
			$data = $q->fetch(PDO::FETCH_ASSOC);
			
			// -- Get Bank Name from code
			if(isset($data['Branch_Code']))
			{
				$data['bankname'] = Getbanknamefrombrachcode($data['Branch_Code']);
			}else{$data['bankname'] = '';}
			// -- Get Payer Name from Client Table.
			if(isset($data['IntUserCode']))
			{
				$dataClient = Getclient($data['IntUserCode']);
				$data['Description'] = $dataClient['Description'];
			}else{$data['Description'] = '';}				
			Database::disconnect();
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
		}
		return $data;
	}
}
// -- To avoid : Redeclaring trim.
if (!function_exists('trim_input'))
{
	// -- Trim Input
	function trim_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);

	  return $data;
	}
}

if(!function_exists('isurlencode'))
{
function isurlencode($param)
{
	//if ( urlencode(base64_encode(base64_decode(urldecode($param)))) === $param)
	if (preg_match("@^[a-zA-Z0-9%+-_]*$@", $param)) //{ // variable is urlencoded. }
	{
		return true;
	}
	else
	{
		return false;
	}
}
}
if(!function_exists('put_sessionsvalues'))
{
function put_sessionsvalues($dataComp)
{
	// =========================================== Store Values for e-mail eStatement =======
						$space = '';
						/*$_SESSION['invTitle'] 		  = $dataComp['Title'];
						$_SESSION['invFirstName']     = $dataComp['FirstName'];
						$_SESSION['invLastName']      = $dataComp['LastName'];
						$_SESSION['invidnumber']      = $dataComp['idnumber'];
						$_SESSION['invAccountNumber'] = $dataComp['ApplicationId'];
						$_SESSION['invInvoiceNumber'] = $dataComp['ApplicationId'];
						$_SESSION['invDueDate'] 	  = $dataComp['PaymentDueDate'];
						$_SESSION['invAmountDue'] 	  = $dataComp['tot_outstanding'];
						$_SESSION['invCurrency'] 	  = $dataComp['currency'];
						$_SESSION['invEmail'] 		  = $dataComp['email'];
						*/
						$_SESSION['GlobalCompany']  = $dataComp['name'];
						$_SESSION['Globalphone']    = $dataComp['phone'];
						$_SESSION['Globalfax']      = $dataComp['fax'];
						$_SESSION['Globalemail']    = $dataComp['email'];
						$_SESSION['Globalwebsite']  = $dataComp['website'];
						$_SESSION['Globallogo']     = $dataComp['logo'];
						$_SESSION['GloballogoTemp'] = $dataComp['logo'];
						$_SESSION['Globalcurrency'] = $dataComp['currency'];
						$_SESSION['Globallegalterms'] = $dataComp['legaltext'];
						$_SESSION['Globalstreet'] = $dataComp['street'];
						$_SESSION['Globalsuburb'] = $dataComp['suburb'];
						$_SESSION['Globalcity'] = $dataComp['city'];
						$_SESSION['Globalstate'] = $dataComp['province'];
						$_SESSION['Globalpostcode'] = $dataComp['postcode'];
						$_SESSION['Globalregistrationnumber'] = $dataComp['registrationnumber'];

						// -- Email Server Hosting
						$_SESSION['Globalmailhost']  = $dataComp['mailhost'];
						$_SESSION['Globalport'] = $dataComp['port'];
						$_SESSION['Globalmailusername'] = $dataComp['username'];
						$_SESSION['Globalmailpassword'] = $dataComp['password'];
						$_SESSION['GlobalSMTPSecure'] = $dataComp['SMTPSecure'];
						$_SESSION['Globaldev'] =  $dataComp['dev'];
						$_SESSION['Globalprod'] = $dataComp['prod'];
					// -- EOC Master Session Values --- //
// =========================================== ECO Store Values ==========================
}
}
////////////////////////////////////////////////////////////////////////	
//						Declarations								  //				
////////////////////////////////////////////////////////////////////////
$emailFlag = '';	
$paymentid = '';
$dataPayment = null;
$dataPayment['createdon'] = '';
$dataPayment['createdat'] = '';
$dataPayment['collectionid'] = '';
$dataPayment['Amount'] = '';

$dataPayment['Account_Holder'] = '';
$dataPayment['Account_Number'] = '';
$dataPayment['Branch_Code'] = '';
$dataPayment['Bank_Reference'] = '';
$title = '';//'proof of payment';
// -- Read data fro below 
$dataPayment['bankname'] = ''; 
$dataPayment['Description'] = '';
$dataPayment['collectionid'] = '';
////////////////////////////////////////////////////////////////////////
//						POST and GET parameters						  //			
////////////////////////////////////////////////////////////////////////
// ================================ send an e-mail ====================
if( !empty($_GET['email']))
{
	$emailFlag = $_REQUEST['email'];
}
else
{
	if(isset($_POST['email']))
	{
		$emailFlag = $_POST['email'];
	}
}
// ================================ End Send an e-mail ================
if (!empty($_GET['paymentid']))
{   $paymentid = $_REQUEST['paymentid'];
	if(isurlencode($paymentid)){$paymentid = base64_decode(urldecode($paymentid));}
}
else{if(isset($_POST['paymentid'])){
    $paymentid = $_POST['paymentid'];
	if(isurlencode($paymentid)){$paymentid = base64_decode(urldecode($paymentid));}
	}
}
////////////////////////////////////////////////////////////////////////
//						Location									  //	
//////////////////////////////////////////////////////////////////////// 	
	if ( null==$paymentid )
	 {
		 //header("Location: index");
	 }
	 else 
	   {
		$dataFilter['collectionid'] = $paymentid;
		$dataPayment = get_payment($dataFilter);
		if(!isset($dataPayment['collectionid']))
		{
			//header("Location: index");
		}
	}
//-------------------------------------------------------------------
//           				COMPANY DATA							-
//-------------------------------------------------------------------
	$dataComp = get_globalsettings();
	put_sessionsvalues($dataComp );	

//-------------------------------------------------------------------
//           				CUSTOMER DATA							-
//-------------------------------------------------------------------

// ------------------------------------------------------------------
//							CONTRACT OPTIONAL DATA					 -
// ------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////
// Logo are update on page tcpdf_autoconfig.php (Under root directory)
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
require_once "FPDI/fpdi.php";
////////////////////////////////////////////////////////////////////////
$pdf = new FPDI( PDF_PAGE_ORIENTATION, 'mm', 'LANDSCAPE' ); //FPDI extends TCPDFLETTER
//$companyphone = $fax;
$address =  $dataComp['name'].','.$dataComp['street'].','.
$dataComp['suburb'].','.$dataComp['postcode'].','.
$dataComp['city'].','.$dataComp['phone'].'/'.$dataComp['fax'].
','.$dataComp['email'];
/**
 * Protect PDF from being printed, copied or modified. In order to being viewed, the user needs
 * to provide "ID number" as password and ID number as master password as well.
 */
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);
//$pdf->SetProtection(array('copy','modify'), $customerid, $customerid, 0, null);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor( $dataComp['name']);
$pdf->SetTitle( $dataComp['name']);
$pdf->SetHeaderTitle('Header Title');
$pdf->SetSubject($dataComp['name']." - $title");
$pdf->SetKeywords('Invoice, eStatement');
//\t\t\t\t\t
$tab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
$gtab = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

$pdf_title = $gtab. $dataComp['name'];//.' - '.$title;

define ('PDF_HEADER_STRING2',//$tab.$dataComp['name']."\n"
							 $tab.$dataComp['street']."\n"
							 .$tab.$dataComp['suburb']."\n"
							 .$tab.$dataComp['postcode']."\n"
							 .$tab.$dataComp['city']."\n"
							 .$tab.$dataComp['phone']."\n"
							 .$tab.$dataComp['email']);

define ('PDF_HEADER_LOGO_CUSTOM', $dataComp['logo']);
define ('PDF_HEADER_LOGO_WIDTH_CUSTOM', 90);

// set default header data PDF_HEADER_STRING // -- array(0,64,255) rgb color:
$pdf->SetHeaderData(PDF_HEADER_LOGO_CUSTOM, PDF_HEADER_LOGO_WIDTH_CUSTOM, $pdf_title, PDF_HEADER_STRING2, array(0,0,0), array(0,0,0));

$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

////////////////////////////////////////////////////////////////////////
// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = file_get_contents('eproofofpayment_html.php');
$html = mapping_data($html,$dataComp,$dataPayment);
////////////////////////////////////////////////////////////////////////

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$ApplicationId = '2';
// ---------------------------------------------------------
//$pdf->AddPage();
$sw_bookmark = 0;
$pages = "";
$eStatement = $title.'_'.$dataPayment['collectionid'];
// Close and output PDF document
// This method has several options, check the source code documentation for more information.

$fileName = $eStatement.'.pdf';
$_SESSION['invFileName'] = $fileName;

//============================================================
if(empty($emailFlag))
{
$pdf->Output($fileName, 'I');
//echo $html;
}
else // Send an e-mail
{
 $fileatt = $pdf->Output($fileName, 'S');
 SendEmail($fileatt);
}
//============================================================+
// 	SEND AN eInvoice to client(s) -
//============================================================+
// ------------ Start Send an Email ----------------//
function SendEmail($att)
{


		require_once('class/class.phpmailer.php');
		try
		{
					// -- Email Server Hosting -- //
						$Globalmailhost = $_SESSION['Globalmailhost'];
						$Globalport = $_SESSION['Globalport'];
						$Globalmailusername = $_SESSION['Globalmailusername'];
						$Globalmailpassword = $_SESSION['Globalmailpassword'];
						$GlobalSMTPSecure = $_SESSION['GlobalSMTPSecure'];
						$Globaldev = $_SESSION['Globaldev'];
						$Globalprod = $_SESSION['Globalprod'];
						$adminemail = $_SESSION['Globalemail'];
						$companyname = $_SESSION['GlobalCompany'];
						// -- Email Server Hosting -- //

					/*	Production - Live System */
						if($Globalprod == 'p')
						{
							$mail=new PHPMailer();
							$mail->IsSendmail(); // telling the class to use SendMail transport (/usr/sbin/sendmail)
							$mail->SetFrom($adminemail,$companyname);
							$mail->AddReplyTo($adminemail,$companyname);
						}
					/*	Development - Testing System */
						else
						{
								$mail=new PHPMailer();
								$mail->IsSMTP();
								$mail->SMTPDebug=1;
								$mail->SMTPAuth=true;
								$mail->SMTPSecure= $GlobalSMTPSecure;//"tls";//
								$mail->Host=$Globalmailhost;//"smtp.vogsphere.co.za";//
								$mail->Port= $Globalport;//587;//
								$mail->Username=$Globalmailusername;//"vogspesw";//
								$mail->Password=$Globalmailpassword;//"fCAnzmz9";//
						}
								$dateForm = date('F')."  ".date('Y');

								$mail->SetFrom($adminemail,$companyname);

								$mail->AddStringAttachment($att, 'statement.pdf');

								//$email_text = file_get_contents('content/text_emailWelcomeLetter.html');
								$mail->Subject=  $companyname." statement of account :".$dateForm;//$_SESSION['invAccountNumber'];
								// Enable output buffering
								$_SESSION['invDateForm'] = $dateForm;

								ob_start();
								include 'content/text_emailInvoice.php';//execute the file as php
								$body = ob_get_clean();
								$fullname = $FirstName.' '.$LastName;
								$email = $_SESSION['invEmail'];

								// print $body;
								$mail->MsgHTML($body );//file_get_contents('content/text_emailWelcomeLetter.html'));
								$mail->AddAddress($email, $fullname);
								$mail->AddBCC($adminemail, $companyname);


								if($mail->Send())
								{
						$successMessage = "An e-mail has been sent to ".$email;
						// -- Somehow the code below stops the buffer and returns called jQuery  code page, on the eStatement.php.
						// -- Mystery code. This code is mysterous do not understand at this point.25.06.2017
						//echo "<script>alert('".$successMessage."')</script>";
							include 'success.php';//execute the file as php

								}
								else
								{
								  // echo 'mail not sent';
								$successMessage = "Your user name is valid but your e-mail address seems to be invalid!";

								}
								
			}
			catch(phpmailerException $e)
			{
				$successMessage = $e->errorMessage();
			}
			catch(Exception $e)
			{
				$successMessage = $e->getMessage();
			}
}
if(!function_exists('trim_input'))
{
// -- To avoid : Redeclaring trim.
	// -- Trim Input
	function trim_input($data)
	{
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);

	  return $data;
	}
}


// -- BOC Check if parameters are Encoded & Decoded. 17.09.2017.
if(!function_exists('isurlencode'))
{
function isurlencode($param)
{
	//if ( urlencode(base64_encode(base64_decode(urldecode($param)))) === $param)
	if (preg_match("@^[a-zA-Z0-9%+-_]*$@", $param)) //{ // variable is urlencoded. }
	{
		return true;
	}
	else
	{
		return false;
	}
}
}
// -- EOC Check if parameters are Encoded & Decoded. 17.09.2017.

//============================================================+
// END OF FILE
//============================================================+
?>
