<?php
require 'database.php';

if (!function_exists('updatecollections2'))
{
	function updatecollections2($id,$arrayCollections)
	{
	 try
	  {
		//Global $pdo;
		$pdo = Database::connectDB();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$sql = "UPDATE mandates
		SET
		tracking_cancellation_indicator					= ?,  	
		cancellation_reason                  			= ?
		WHERE reference = ?";
		$q = $pdo->prepare($sql);

		$q->execute(array(
		$arrayCollections['tracking_cancellation_indicator'],				
		$arrayCollections['cancellation_reason'],                  		
		$id));
		Database::disconnect();
		}
	  catch (Exception $e)
	  {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	  }
	}
}

$reference 			 = getpost('id_'); 		
$list		         = getpost('list');			

// -- Update Collection.
$message = '';

// -- Function.
$message = updatecollections2($reference,$list);
if(empty($message))
{
	 $message = $reference.' was updated successfully.';
}

echo $message;
//print_r($list );	
?>