<?php  
session_start();  

?>  
<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title>E-loan - Vogsphere e-Loans</title>
       <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- Favicon -->
        <link href="assets/img/Icon_vogs.ico" rel="shortcut icon">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.css" rel="stylesheet">
		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 

				<!-- Jquery --
<script src="assets/js/jquery.min.js"></script> -->

        <script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/custom.js"></script>
		
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Template CSS -->
        <link rel="stylesheet" href="assets/css/animate.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/nexus.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/custom.css" rel="stylesheet">
		<style type="text/css">
a{ text-decoration: none; color: #333}
h1{ font-size: 1.9em; margin: 10px 0}
p{ margin: 8px 0}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-font-smoothing: antialiased;
	-moz-font-smoothing: antialiased;
	-o-font-smoothing: antialiased;
	font-smoothing: antialiased;
	text-rendering: optimizeLegibility;
}
body{
	font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
	text-transform: inherit;
	color: #333;
	background: #e7edee;
	width: 100%;
	line-height: 18px;
}
.wrap{
	width: 500px;
	margin: 15px auto;
	padding: 20px 25px;
	background: white;
	border: 2px solid #DBDBDB;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	overflow: hidden;
	text-align: center;
}
.status{
	/*display: none;*/
	padding: 8px 35px 8px 14px;
	margin: 20px 0;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}
input[type="submit"] {
	cursor:pointer;
	width:100%;
	border:none;
	background:#991D57;
	background-image:linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-moz-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	background-image:-webkit-linear-gradient(bottom, #8C1C50 0%, #991D57 52%);
	color:#FFF;
	font-weight: bold;
	margin: 20px 0;
	padding: 10px;
	border-radius:5px;
}
input[type="submit"]:hover {
	background-image:linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-moz-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	background-image:-webkit-linear-gradient(bottom, #9C215A 0%, #A82767 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
}
input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}
</style>

<script>
  //webshim.setOptions('basePath', '/js-webshim/minified/shims/');

  //request the features you need:
 // webshim.polyfill('es5 mediaelement forms');
  
  $(function(){
    // use all implemented API-features on DOM-ready
  });
</script>

        <!-- Google Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="body-bg">
            <!-- Phone/Email -->
            <div id="pre-header" class="background-gray-lighter">
                <div class="container no-padding">
                    <div class="row hidden-xs">
                        <div class="col-sm-6 padding-vert-5">
						<?php
								//session_start();  
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
                           echo "<strong>Phone:</strong>&nbsp;".$_SESSION['phone'].""; 
									}
						?>
                        </div>
                        <div class="col-sm-6 text-right padding-vert-5">
						<?php
								//session_start();  
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
										echo "<strong>Welcome ".$_SESSION['Title']." ".$_SESSION['FirstName']." ".$_SESSION['LastName']."</strong>";
										echo "<strong>&nbsp;&nbsp;Email:</strong>&nbsp;".$_SESSION['email']."";
									} else 
									{
										echo "<strong>Email:</strong>&nbsp;info@vogsphere.co.za";
								    }
									?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Phone/Email -->
            <!-- Header -->
			<div style="height:190px" id="header">
               <!-- <div style="border:solid" class="container"> -->

                   <div  class="row"> 
                        <!-- Logo -->

                        <div  class="logo">
                            <a  href="index.php" title="">
								<img  src="assets/img/logo-theone.png" alt="Logo" >

                                <!-- <img src="assets/img/logo.png" alt="Logo" /> -->
                            </a>
                        </div>
						
                        <!-- End Logo -->
                   </div>
               <!-- </div> -->
            </div>
            <!-- End Header -->
            <!-- Top Menu -->
		    <?php include_once('menu.php');?> 
            <!-- End Top Menu -->
            <!-- === END HEADER === -->
            <!-- === BEGIN CONTENT === -->
            <div id="content" class="bottom-border-shadow">
                <?php include_once($page_content) ;?>
            </div>
            <!-- === END CONTENT === -->
            <!-- === BEGIN FOOTER === -->
            <div id="base">
                <div class="container bottom-border padding-vert-30">
                    <div class="row">
                        <!-- Disclaimer
                        <div class="col-md-4">
                            <h3 class="class margin-bottom-10">Disclaimer</h3>
                            <p>All stock images on this template demo are for presentation purposes only, intended to represent a live site and are not included with the template or in any of the Joomla51 club membership plans.</p>
                            <p>Most of the images used here are available from
                                <a href="http://www.shutterstock.com/" target="_blank">shutterstock.com</a>. Links are provided if you wish to purchase them from their copyright owners.</p>
                        </div>  -->
                        <!-- End Disclaimer -->
                        <!-- Contact Details -->
                        <div class="col-md-4 margin-bottom-20">
                            <h3 class="margin-bottom-10">Contact Details</h3>
                            <p>
                                <span class="fa-phone">Telephone:</span>079-340-1754
                                <br>
                                <span class="fa-envelope">Email:</span>
                                <a href="mailto:info@example.com">info@vogsphere.co.za</a>
                                <br>
                                <span class="fa-link">Website:</span>
                                <a href="http://www.example.com">www.vogsphere.co.za</a>
                            </p>
                            <p>43 Mulder street 31 SEOUL,
                                <br>Centurion,
                                <br>Gauteng,
                                <br>0158</p>
                        </div>
                        <!-- End Contact Details -->
                        <!-- Sample Menu -->
                        <div class="col-md-4 margin-bottom-20">
                            <h3 class="margin-bottom-10">e-Loans Menu</h3>
                            <ul class="menu">
                                <li>
                                    <a class="fa fa-home fa-fw" href="index.php">Home</a>
                                </li>
                                <li>
									<a class="fa-signal" href="applicationForm.php">Apply for Loan</a>
                                </li>
								<?php
								//session_start();  
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
										echo "<li><a id='Login' name='Login' href='Logout.php' class='fa-gears'>Logout</a></li>";

									} else 
									{
										echo "<li><a href='customerLogin.php' class='fa-gears' target='_blank'>e-loan login</a></li>";
										// echo "Please log in first to see this page.";
								    }
									?>
								
                                <!-- <li>
								<a href="customerLogin.php" class="fa-gears">e-loan login</a>
                                </li> -->
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <!-- End Sample Menu -->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <div id="footer" class="background-grey">
                <div class="container">
                    <div class="row">
                        <!-- Footer Menu -->
                        <div id="footermenu" class="col-md-8">
                            <ul class="list-unstyled list-inline">
                                <li>
                                    <a href="index.php" target="_blank">Home</a>
                                </li>
                                <li>
                                    <a href="applicationForm.php" target="_blank">Apply for Loan</a>
                                </li>
								<?php
								//session_start();  
									if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) 
									{
										echo "<li><a id='Login' name='Login' href='Logout.php' class='fa-gears'>Logout</a></li>";

									} else 
									{
										echo "<li><a href='customerLogin.php' class='fa-gears' target='_blank'>e-loan login</a></li>";
										// echo "Please log in first to see this page.";
								    }
									?>
								
                               <!-- <li>
                                    <a href="customerLogin.php" target="_blank">e-loan login</a>
                                </li> -->
                            </ul>
                        </div>
                        <!-- End Footer Menu -->
                        <!-- Copyright -->
                        <div id="copyright" class="col-md-4">
                            <p class="pull-right">(c) 2016 Vogsphere</p>
                        </div>
                        <!-- End Copyright -->
                    </div>
                </div>
            </div>
            <!-- End Footer -->
            <!-- JS --
            <script type="text/javascript" src="assets/js/jquery.min.js" type="text/javascript"></script> 
            <script type="text/javascript" src="assets/js/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="assets/js/scripts.js"></script>
            <!-- Isotope - Portfolio Sorting 
            <script type="text/javascript" src="assets/js/jquery.isotope.js" type="text/javascript"></script>
            <!-- Mobile Menu - Slicknav 
            <script type="text/javascript" src="assets/js/jquery.slicknav.js" type="text/javascript"></script>
            <!-- Animate on Scroll
            <script type="text/javascript" src="assets/js/jquery.visible.js" charset="utf-8"></script>
            <!-- Sticky Div 
            <script type="text/javascript" src="assets/js/jquery.sticky.js" charset="utf-8"></script> -->
            <!-- Slimbox2-->
            <script type="text/javascript" src="assets/js/slimbox2.js" charset="utf-8"></script>
            <!-- Modernizr -->
            <script src="assets/js/modernizr.custom.js" type="text/javascript"></script>
			

            <!-- End JS -->
    </body>
</html>
<!-- === END FOOTER === -->