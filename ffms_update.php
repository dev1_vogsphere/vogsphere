<?php
//-------------------------------------------------------------------
//							eInvoice
//-------------------------------------------------------------------
$tableDetails  = '';
//-------------------------------------------------------------------
//           				DATABASE CONNECTION DATA    			-
//-------------------------------------------------------------------
//$filerootpath = filerootpath();
//require $filerootpath.'/database.php';
require  $filerootpath.'/database_ffms.php';
//-------------------------------------------------------------------
//           				DECLARATIONS			    			-
//-------------------------------------------------------------------	
	$emailFlag = '';
	$tenantid = '';
	$customerid = '';
//-------------------------------------------------------------------
//           				INPUT PARAMETERS		    			-
//-------------------------------------------------------------------
// -- Check if variables are already set $a is set
	if (!isset($emailFlag)) {$emailFlag = '';}
	if (!isset($tenantid)) {$tenantid = '';}
	if (!isset($customerid)) {$customerid = '';}
// ================================ send an e-mail ==================
if ( !empty($_GET['email']))
	{
		$emailFlag = $_REQUEST['email'];
	}
	else
	{
	// -- Send Multiple Emails
	 if(isset($_POST['email']))
	 {
	    $emailFlag = $_POST['email'];
	 }
	}
// ================================ End Send an e-mail ===============

// ================================ BOC - tenantid =========================
//$tenantid = 'UFRF';
if ( !empty($_GET['tenantid']))
	{
		$tenantid = $_REQUEST['tenantid'];
	}
	else
	{
	// -- From POST operation.
	 if(isset($_POST['tenantid']))
	 {
	    $tenantid = $_POST['tenantid'];
	 }
	}
	//echo $tenantid;
// ================================ EOC - tenantid =========================
//$customerid = '84080355450866'; //'620103554586';//
	if ( !empty($_GET['customerid']))
	{
		$customerid = $_REQUEST['customerid'];
		/*if(isurlencode($customerid))
		{
			$customerid = base64_decode(urldecode($customerid));
		}*/
	}
	else
	{
		if(isset($_POST['customerid']))
		  {
		  	$customerid = $_POST['customerid'];
			/*if(isurlencode($customerid))
			{
				$customerid = base64_decode(urldecode($customerid));
			}*/
		  }
	}
	
	
//-------------------------------------------------------------------
//           				Declarations				    		-
//-------------------------------------------------------------------
	$dataMember = null;
	$dataMemberPolicy = null;
	$SpouseData = null;
	$ChildrenData = null;
	$tablePrincipalMember = '';
	
	if ( null==$tenantid )
	    {
			//header("Location: custAuthenticate.php");
		}
	 else {
//-------------------------------------------------------------------
//           				Principal Member DATA		    		-
//-------------------------------------------------------------------
		$input['customerid']       = $customerid;
		$main_member_data          = get_main_member_data($input);
		$dataPackage 		       = GET_MEMBER_PACKAGE_POLICY($main_member_data);
		$input['Member_Policy_id'] = $main_member_data['Member_Policy_id'];
		$addressMember             = get_main_member_address($main_member_data);
		$input['member_province']  = $addressMember ['Line_2'];
		$input['member_debitdate'] = get_debicheck_data($input)['collection_date_text'];
		$input['Benefits']          = 'R'.number_format($dataPackage['Benefits'],2);
		$input['PackageOption_id']  = $dataPackage['PackageOption_id'];
		$dataPremium 		        = GET_premium($input);
		$input['premium']	        = 'R'.number_format($dataPremium['Premium_amount'],2); 
		$tablePrincipalMember       = get_principal_data($input);
		$member_name     = $main_member_data['First_name'].' '.$main_member_data['Surname'];
		$member_province = $input['member_province'];
		$member_ContactNo = $addressMember['Cellphone'];
//-------------------------------------------------------------------
//           				Spouse Member DATA		    			-
//-------------------------------------------------------------------			
		//--$input['Spouse_id'] = $customerid;
		$tableSpouseMember  = get_spouse_data($main_member_data);
//-------------------------------------------------------------------
//           				Children DATA(Extended_TypeId = '3')    -
//-------------------------------------------------------------------		
		$tableChildren 				= get_dependent_update_data($input);
		$tableChildrenConfirm 		= get_dependent_3_data($input);
//-------------------------------------------------------------------
//           				Extended Family (Extended_TypeId = '4') -
//-------------------------------------------------------------------		
	    $tableExtendedFamilyMembers        = get_extended_members_update_data($input);
	    $tableExtendedFamilyMembersConfirm = get_extended_members_4_data($input);
//-------------------------------------------------------------------
//           				Beneficiaries (Extended_TypeId = '2') -
//-------------------------------------------------------------------
		$tableBeneficiaries 		= get_beneficiary_update_data($input);
		$tableBeneficiariesConfirm	= get_beneficiary_2_data($input); 
		//if(!isset($_SESSION['tableBeneficiariesConfirm']))
		//{$_SESSION['tableBeneficiariesConfirm'] = $tableBeneficiariesConfirm;}
//-------------------------------------------------------------------
		
		$tbl_member = 'member';
		$tbl_member_policy = 'member_policy';
		$tbl_address = 'address';
////////////////////////////////////////////////////////////////////////
// -- Determine the SQL statement to use..if both spouse and member_bank_account..
// -- then use a bigger SQL..
		$ffms_spouse 			  = get_ffms_spouse($main_member_data);
		$ffms_member_bank_account = get_ffms_member_bank_account($main_member_data);		
		if(isset($ffms_spouse['spouse_id']) && isset($ffms_member_bank_account['Main_member_id'])){ 
		$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium,spouse where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
		AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id AND $tbl_member.spouse_id = spouse.spouse_id
		AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id LIMIT 1";
		//echo $sql."1";
		}elseif(isset($ffms_spouse['spouse_id'])){
		$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,source_of_income,member_income,policy,packageoption,premium,spouse where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id
		AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id AND $tbl_member.spouse_id = spouse.spouse_id
		AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id LIMIT 1";
		//echo $sql."2";;		
		}elseif(isset($ffms_member_bank_account['Main_member_id'])){
		$sql =  "select * from $tbl_member, $tbl_member_policy,$tbl_address,member_bank_account,source_of_income,member_income,policy,packageoption,premium where $tbl_member.Member_id = ? AND Member_Policy_id = ? AND $tbl_address.Address_id = $tbl_member.Address_id AND $tbl_member.Member_id = member_bank_account.Main_member_id
		AND source_of_income.Source_of_income_id = member_income.Source_of_income_id AND member_income.Member_id = member.Member_id
		AND member_policy.Policy_id = policy.Policy_id AND packageoption.PackageOption_id = policy.PackageOption_id AND premium.PackageOption_id = packageoption.PackageOption_id LIMIT 1";
		//echo $sql."3";;		
		}
////////////////////////////////////////////////////////////////////////			
		$pdo_ffms = db_connect::db_connection();			
		$pdo_ffms->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$q = $pdo_ffms->prepare($sql);
		$q->execute(array($customerid,$ApplicationId));
		$dataMembership = $q->fetch(PDO::FETCH_ASSOC);

	    $pdo1 = Database::connect();			
		$pdo1->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = 'SELECT * FROM bankbranch WHERE bankid = ? AND branchcode = ?';
		$q = $pdo1->prepare($sql);
		$q->execute(array($dataMembership['Bank'],$dataMembership['Branch_code']));
		$dataBankBranches = $q->fetch(PDO::FETCH_ASSOC);		
	}
?>